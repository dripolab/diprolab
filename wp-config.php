<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'diprolab');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ap~]Ews!Kpn?zERxBy7-rT0O.pMQC;mqf@yHp.%RwQ n[Ut}K?Y,e^Ct[hS]s*ax');
define('SECURE_AUTH_KEY',  'tqm*@Dp[(^IZ=CPkK.xP)!zzG/2kYx-<((%kO Iis,cj_Ujx)+7K2H1.w*6>q Xa');
define('LOGGED_IN_KEY',    '# #&q%I<4t7Nh~LYJu_.@NA&*xbR,7H13^jYTw*WkU5=su+$GH`g6A?CGulp<Xsw');
define('NONCE_KEY',        ']-``MU}+~(m*3f:G~G6!W#$X ~XuInPU-{LxGQRqUl^%T=:q$K$,*&oYNz>-011B');
define('AUTH_SALT',        '8|VT9Juhe4Qn/LtL])7zj(k1r_L.t@)]i.niN^x08+ 0Hubw|?pR>YJp @MgjfH$');
define('SECURE_AUTH_SALT', 'mWT4krW)^$=QxUApYXTYIJz4nq,<2]!NUPSmnL|kF+Z#[4X;CmPL*-w,{mmcGv-5');
define('LOGGED_IN_SALT',   '8Hz|A$:~9$!*$81gM#W-$3qtSjM.PN2@dZXJ]41dY{!&sqSI|5<`o7~N.|b$tv[r');
define('NONCE_SALT',       'gab vZvd!?]Y0.*v_{w(04Z-Sp}+J?[Q7]yMYzwT~f,:f{sKE,jq9!G<#+.?25U)');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
