<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 */

get_header(); ?>

<?php medicale_mascot_get_title_area_parts(); ?>
<?php medicale_mascot_get_page(); ?>


<?php get_footer();
