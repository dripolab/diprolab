<?php
/**
 * The template for displaying archive for portfolio
 */

get_header(); ?>

<?php medicale_mascot_get_title_area_parts(); ?>
<?php medicale_mascot_get_portfolio(); ?>

<?php get_footer();
