<?php
/**
 * The template for displaying all single posts and attachments of Gallery CPT
 *
 */

get_header(); ?>


<?php medicale_mascot_get_title_area_parts(); ?>
<?php medicale_mascot_get_gallery_single(); ?>


<?php get_footer();
