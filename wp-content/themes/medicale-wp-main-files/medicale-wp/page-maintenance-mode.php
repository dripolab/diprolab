<?php
/**
 * The template for displaying Maintenance Mode Page
 *
 * This is the template that displays Maintenance Mode0 page by default.
 *
 */
add_filter( 'medicale_mascot_filter_show_header', 'medicale_mascot_return_false' );
add_filter( 'medicale_mascot_filter_show_footer', 'medicale_mascot_return_false' );
	
//change the page title
if( medicale_mascot_get_redux_option( 'maintenance-mode-settings-title' ) != '' ) {
	add_filter('pre_get_document_title', 'medicale_mascot_change_the_title');
	function medicale_mascot_change_the_title() {
		return medicale_mascot_get_redux_option( 'maintenance-mode-settings-title' );
	}
}

get_header();
?>

<?php
if ( medicale_mascot_core_plugin_installed() ) {
	mascot_core_get_maintenance_mode_parts();
}
?>

<?php get_footer();
