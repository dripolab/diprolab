<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Cannot access pages directly.' );
}
?>

<div class="wrap about-wrap mascot-admin-tpl-wrapper">
	<?php echo medicale_mascot_get_template_part( 'admin/admin-tpl/mascot-header' ); ?>
	<?php echo medicale_mascot_get_template_part( 'admin/admin-tpl/mascot-tabs' ); ?>

	<?php 
		if ( medicale_mascot_core_plugin_installed() ) {
			mascot_core_theme_admin_menu_system_status();
		}
	?>
</div>