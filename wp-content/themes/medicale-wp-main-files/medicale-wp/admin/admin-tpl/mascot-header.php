<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Cannot access pages directly.' );
}
$wp_get_theme = wp_get_theme( get_template() );
$theme_name = $wp_get_theme->get('Name');
?>

	<h1><?php esc_html_e( 'Welcome to', 'medicale-wp' ); ?> <?php echo $theme_name ?></h1>
	<div class="about-text">
		<?php echo $theme_name ?> <?php esc_html_e( 'theme is installed successfully and you are ready to go live! Please install premium & required plugins and import demo content to build your awesome website! Also please activate this product and get latest updates. Happy Developing!', 'medicale-wp' ); ?> 
	</div>
	<div class="mascot-theme-logo"><span class="mascot-theme-version">Version <?php echo $wp_get_theme->get('Version')?></span></div>
