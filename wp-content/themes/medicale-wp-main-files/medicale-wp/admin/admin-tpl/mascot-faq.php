<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Cannot access pages directly.' );
}
?>

<div class="wrap about-wrap mascot-admin-tpl-wrapper">
	<?php echo medicale_mascot_get_template_part( 'admin/admin-tpl/mascot-header' ); ?>
	<?php echo medicale_mascot_get_template_part( 'admin/admin-tpl/mascot-tabs' ); ?>


<div class="feature-section">
	<div class="mascot-faq-tab">
		<div class="heading">Mascot Core Plugin</div>
		<div class="content">
		<pre>
For proper theme functioning, the "<strong>Mascot Core</strong>" plugin is required.

Installation Instructions:
1. From your WordPress dashboard visit Appearance > Install Plugins.
2. Search for 'Mascot Core' and install the plugin.
3. Now activate it.
4. Once the plugin is activated you will find full features of the theme including Custom Post Types, Theme Options, Post Meta etc.

		</pre>
		</div>
	</div>

<hr>
	<div class="mascot-faq-tab">
		<div class="heading">One Click Demo Import</div>
		<div class="content">
		<pre>
Import your demo content, widgets and theme settings with one click. It's as simple as clicking one button! To import demo data, make sure that 'One Click Demo Import' plugin is installed and active.

Installation Process 1:
1. From your WordPress dashboard visit Appearance > Install Plugins.
2. Search for 'One Click Demo Import' and install/activate the plugin.

Installation Process 2:
1. From your WordPress dashboard visit 'Plugins > Add New'.
2. Search for 'One Click Demo Import' and install/activate the plugin.


Demo Import Instructions:
1. Once the plugin is activated you will find the actual import page in: Appearance -> Import Demo Data.

See the <a target="_blank" href="https://wordpress.org/plugins/one-click-demo-import/screenshots/">Screenshots Tab</a> for more details.
		</pre>
		</div>
	</div>

<hr>

	<div class="mascot-faq-tab">
		<div class="heading">Regenerate Thumbnails</div>
		<div class="content">
		<pre>
Regenerate Thumbnails allows you to regenerate the thumbnails for your image attachments. This is very handy if you've changed any of your thumbnail dimensions (via Settings -> Media) after previously uploading images or have changed to a theme with different featured post image dimensions.

You can either regenerate the thumbnails for all image uploads, individual image uploads, or specific multiple image uploads.

See the <a target="_blank" href="https://wordpress.org/extend/plugins/regenerate-thumbnails/screenshots/">Screenshots Tab</a> for more details.

Installation and Usage Method:
1. Go to your admin area and select Plugins -&gt; Add new from the menu.
2. Search for 'Regenerate Thumbnails.
3. Click install.
4. Click activate.
5. Go to Tools > Regen. Thumbnails
6. Click "Regenerate All Thumbnails" button and let the process to finish till it reaches 100 percent.
		</pre>
		</div>
	</div>

<hr>

	<div class="mascot-faq-tab">
		<div class="heading">Flush Rewrite Rules</div>
		<div class="content">
		<pre>
Remove rewrite rules and then recreate rewrite rules. This method can be used to refresh WordPress' rewrite rule cache. Generally, this should be used after programmatically adding one or more custom rewrite rules.

When it is necessary?
-> Usually needs to flush rewrite rules for new custom post types.
-> You should only flush rules on activation or deactivation of a plugin or theme.
-> If you get 404 Page not found error on some pages/posts that already exist.


To flush WordPress rewrite rules or permalinks from the Dashboard:
1: In the main menu find <a href="<?php echo admin_url( 'options-permalink.php' ); ?>" target="_blank">Settings > Permalinks</a>.
2: Scroll down if needed and click 'Save Changes.
3: Rewrite rules and permalinks are flushed.
		</pre>
		</div>
	</div>

<hr>


</div>