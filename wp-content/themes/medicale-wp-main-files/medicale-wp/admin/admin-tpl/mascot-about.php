<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Cannot access pages directly.' );
}
$wp_get_theme = wp_get_theme( get_template() );
$theme_name = $wp_get_theme->get('Name');
?>

<div class="wrap about-wrap mascot-admin-tpl-wrapper">
	<?php echo medicale_mascot_get_template_part( 'admin/admin-tpl/mascot-header' ); ?>
	<?php echo medicale_mascot_get_template_part( 'admin/admin-tpl/mascot-tabs' ); ?>

	<div class="about-wrapper">
		<?php //if ( medicale_mascot_get_url_params( 'theme-activated' ) == 'true' ) : ?>
		<div class="welcome-message">
			<h3>Thank you for choosing <?php echo $theme_name; ?>! <br><small>For proper theme functioning, the "Mascot Core" plugin is must required.</small></h3>

			<h4>Installation Instructions for "Mascot Core" plugin:</h4>
			<ul>
				<li>1. From your WordPress dashboard visit Appearance > Install Plugins or you can click <a target="_blank" href="<?php echo esc_url( admin_url( 'themes.php?page=tgmpa-install-plugins' ) ); ?>">here</a>.</li>
				<li>2. Search for 'Mascot Core' and install the plugin.</li>
				<li>3. Now activate it.</li>
				<li>4. Once the plugin is activated you will find full features of the theme including Custom Post Types, Theme Options, Post Meta etc.</li>
			</ul>
		</div>
		<?php //endif; ?>

	</div>
</div>