<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Cannot access pages directly.' );
}
?>

<div class="wrap about-wrap mascot-admin-tpl-wrapper">
	<?php echo medicale_mascot_get_template_part( 'admin/admin-tpl/mascot-header' ); ?>
	<?php echo medicale_mascot_get_template_part( 'admin/admin-tpl/mascot-tabs' ); ?>

	<div class="feature-section three-col">
		<div class="col">
			<div class="mascot-faq-tab">
				<div class="heading">Documentation</div>
				<div class="content">
					Goto the following link to get documentation on this theme. <br><br>
					<a class="button button-default" target="_blank" href="http://docs.kodesolution.info/wp/">Online Documentation >></a>
				</div>
			</div>
		</div><!-- 
		<div class="col">
			<div class="mascot-faq-tab">
				<div class="heading">Submit A Ticket</div>
				<div class="content">
					We offer excellent support through our advanced ticket system. Make sure to register your purchase first to access our support services and other resources.
				</div>
			</div>
		</div>
		<div class="col">
			<div class="mascot-faq-tab">
				<div class="heading">Video Tutorials</div>
				<div class="content">
					We offer excellent support through our advanced ticket system. Make sure to register your purchase first to access our support services and other resources.
				</div>
			</div>
		</div>
		
		
		<div class="col">
			<div class="mascot-faq-tab">
				<div class="heading">Knowledgebase</div>
				<div class="content">
					We offer excellent support through our advanced ticket system. Make sure to register your purchase first to access our support services and other resources.
				</div>
			</div>
		</div>
		<div class="col">
			<div class="mascot-faq-tab">
				<div class="heading">Community Forum</div>
				<div class="content">
					We offer excellent support through our advanced ticket system. Make sure to register your purchase first to access our support services and other resources.
				</div>
			</div>
		</div>
		<div class="col">
			<div class="mascot-faq-tab">
				<div class="heading">Facebook Group</div>
				<div class="content">
					We offer excellent support through our advanced ticket system. Make sure to register your purchase first to access our support services and other resources.
				</div>
			</div>
		</div> -->

	</div>
</div>