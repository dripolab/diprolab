<?php
/**
 * The template for displaying search results
 *
 */

get_header(); ?>


<?php medicale_mascot_get_title_area_parts(); ?>
<?php medicale_mascot_get_search_page(); ?>


<?php get_footer();
