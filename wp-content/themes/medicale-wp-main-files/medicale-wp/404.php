<?php
/**
 * The template for displaying 404 pages (not found)
 *
 */
$header_return_true_false = ( medicale_mascot_get_redux_option( '404-page-settings-show-header' ) == true ) ? 'medicale_mascot_return_true' : 'medicale_mascot_return_false';
add_filter( 'medicale_mascot_filter_show_header', $header_return_true_false );

$footer_return_true_false = ( medicale_mascot_get_redux_option( '404-page-settings-show-footer' ) == true ) ? 'medicale_mascot_return_true' : 'medicale_mascot_return_false';
add_filter( 'medicale_mascot_filter_show_footer', $footer_return_true_false );

get_header();
?>

<?php
	medicale_mascot_get_404_parts();
?>

<?php get_footer();
