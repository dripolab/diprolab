<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "main-content" div.
 *
 */

?><?php
// This is your option name where all the Redux data is stored.
global $medicale_mascot_redux_theme_opt;
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php
	/**
	 * medicale_mascot_body_tag_start hook.
	 *
	 */
	do_action( 'medicale_mascot_body_tag_start' );
?>
<div id="wrapper">
	<?php
		/**
		 * medicale_mascot_wrapper_start hook.
		 *
		 */
		do_action( 'medicale_mascot_wrapper_start' );
	?>
	<?php medicale_mascot_get_page_preloader(); ?>

	<?php if( apply_filters('medicale_mascot_filter_show_header', true) ): ?>
	<?php medicale_mascot_get_header_parts(); ?>
	<?php endif; ?>

	<?php
		/**
		 * medicale_mascot_before_main_content hook.
		 *
		 */
		do_action( 'medicale_mascot_before_main_content' );
	?>
	<div class="main-content">
	<?php
		/**
		 * medicale_mascot_main_content_start hook.
		 *
		 */
		do_action( 'medicale_mascot_main_content_start' );
	?>
	