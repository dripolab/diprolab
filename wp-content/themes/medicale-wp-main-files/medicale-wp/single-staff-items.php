<?php
/**
 * The template for displaying all single posts and attachments of Staff CPT
 *
 */

get_header(); ?>


<?php medicale_mascot_get_title_area_parts(); ?>
<?php medicale_mascot_get_staff_details(); ?>


<?php get_footer();
