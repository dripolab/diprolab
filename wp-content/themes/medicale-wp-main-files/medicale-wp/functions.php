<?php
/**
 * ThemeMascot functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 */

global $medicale_mascot_theme_info;
$medicale_mascot_theme_info = wp_get_theme();

if (!function_exists('medicale_mascot_core_plugin_installed')) {
	/**
	 * Core Plugin installed?
	 */
	function medicale_mascot_core_plugin_installed() {
		return defined( 'MASCOT_CORE_VERSION' );
	}
}

/* VARIABLE DEFINITIONS
================================================== */
define( 'AUTHOR', 'ThemeMascot' );
define( 'MASCOT_FRAMEWORK_VERSION', '1.0' );
define( 'MASCOT_TEMPLATE_URI', get_template_directory_uri() );
define( 'MASCOT_CHILD_THEME_URI', get_stylesheet_directory_uri() );
define( 'MASCOT_TEMPLATE_DIR', get_template_directory() );
define( 'MASCOT_STYLESHEET_DIR', get_stylesheet_directory() );

define( 'MASCOT_ASSETS_URI', MASCOT_TEMPLATE_URI . '/assets' );
define( 'MASCOT_ASSETS_DIR', MASCOT_TEMPLATE_DIR . '/assets' );

define( 'MASCOT_ADMIN_ASSETS_URI', MASCOT_TEMPLATE_URI . '/admin/assets' );
define( 'MASCOT_ADMIN_ASSETS_DIR', MASCOT_TEMPLATE_DIR . '/admin/assets' );

define( 'MASCOT_FRAMEWORK_FOLDER', 'mascot-framework' );
define( 'MASCOT_FRAMEWORK_URI', MASCOT_TEMPLATE_URI . '/'. MASCOT_FRAMEWORK_FOLDER );
define( 'MASCOT_FRAMEWORK_DIR', MASCOT_TEMPLATE_DIR . '/'. MASCOT_FRAMEWORK_FOLDER );

define( 'MASCOT_LANG_DIR', MASCOT_TEMPLATE_DIR . '/languages' );

define( 'MASCOT_THEME_NAME', $medicale_mascot_theme_info->get( 'Name' ) );
define( 'MASCOT_THEME_SHORT', strtolower($medicale_mascot_theme_info->get( 'Name' )) );
define( 'MASCOT_THEME_VERSION', $medicale_mascot_theme_info->get( 'Version' ) );


/* Initial Actions
================================================== */
add_action( 'after_setup_theme', 		'medicale_mascot_action_after_setup_theme' );
add_action( 'wp_enqueue_scripts', 		'medicale_mascot_action_wp_enqueue_scripts' );
add_action( 'widgets_init', 			'medicale_mascot_action_widgets_init' );
add_action( 'wp_head', 					'medicale_mascot_action_wp_head',1 );
add_action( 'wp_head', 					'medicale_mascot_action_wp_head_at_the_end',100 );

//admin actions
add_action( 'admin_enqueue_scripts',	'medicale_mascot_action_theme_admin_enqueue_scripts' );

add_action( 'wp_footer', 				'medicale_mascot_action_wp_footer' );


/* MASCOT FRAMEWORK
================================================== */
require_once( MASCOT_FRAMEWORK_DIR . '/mascot-framework.php' );



if(!function_exists('medicale_mascot_action_after_setup_theme')) {
	/**
	 * After Setup Theme
	 */
	function medicale_mascot_action_after_setup_theme() {
		//Theme Support
		global $supported_post_formats;
		$supported_post_formats = array( 'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video' );
		
		//This feature enables Post Formats support for this theme
		add_theme_support( 'post-formats', $supported_post_formats );

		//This feature enables Automatic Feed Links for post and comment in the head
		add_theme_support( 'automatic-feed-links' );

		//This feature enables Post Thumbnails support for this theme
		add_theme_support( 'post-thumbnails' );

		//Woocommerce theme suport
		add_theme_support( 'woocommerce' );

		// Custom Backgrounds
		add_theme_support( 'custom-background', array(
			'default-color' => 'fff',
		) );

		//This feature enables plugins and themes to manage the document title tag. This should be used in place of wp_title() function
		add_theme_support( 'title-tag' );

		//This feature allows the use of HTML5 markup for the search forms, comment forms, comment lists, gallery, and caption
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

		// add excerpt support for pages
		add_post_type_support( 'page', 'excerpt' );

		//Thumbnail Sizes
		set_post_thumbnail_size( 672, 372, true );
		add_image_size( 'medicale_mascot_featured_image', 1100 );

		add_image_size( 'medicale_mascot_square', 550, 550, true );
		add_image_size( 'medicale_mascot_square_small', 150, 150, true );
		add_image_size( 'medicale_mascot_square_extra_small', 64, 64, true );

		add_image_size( 'medicale_mascot_landscape', 800, 600, true );
		add_image_size( 'medicale_mascot_portrait', 600, 800, true );
		add_image_size( 'medicale_mascot_large_width', 1600, 600, true );
		add_image_size( 'medicale_mascot_large_height', 800, 1200, true );
		add_image_size( 'medicale_mascot_large_width_height', 1600, 1200, true );

		//Content Width
		if ( ! isset( $content_width ) ) $content_width = 1140;

		//Theme Textdomain
		load_theme_textdomain( 'medicale-wp', get_template_directory() . '/languages' );

		//Register Nav Menus
		register_nav_menus( array(
			'primary' 					=> esc_html__( 'Primary Navigation Menu', 'medicale-wp' ),
			'page-404-helpful-links' 	=> esc_html__( 'Page 404 Helpful Links', 'medicale-wp' ),
			'column1-header-top-nav' 	=> esc_html__( 'Column 1 - Header Top Navigation', 'medicale-wp' ),
			'column2-header-top-nav' 	=> esc_html__( 'Column 2 - Header Top Navigation', 'medicale-wp' ),
			'column1-footer-nav' 		=> esc_html__( 'Columns 1 - Footer Bottom Navigation', 'medicale-wp' ),
			'column2-footer-nav' 		=> esc_html__( 'Columns 2 - Footer Bottom Navigation', 'medicale-wp' ),
			'column3-footer-nav' 		=> esc_html__( 'Columns 3 - Footer Bottom Navigation', 'medicale-wp' ),
		) );
	}
}


if(!function_exists('medicale_mascot_action_wp_enqueue_scripts')) {
	/**
	 * Enqueue Script/Style
	 */
	function medicale_mascot_action_wp_enqueue_scripts() {
		wp_enqueue_script( 'jquery-ui-core');
		wp_enqueue_script( 'jquery-ui-tabs');
		wp_enqueue_script( 'jquery-ui-accordion');
		
		wp_enqueue_style( 'wp-mediaelement' );
		wp_enqueue_script( 'wp-mediaelement' );

		if( !is_admin() ){

			/**
			 * Enque Style
			 */
			wp_enqueue_style( 'bootstrap', MASCOT_TEMPLATE_URI . '/assets/css/bootstrap.min.css' );
			wp_enqueue_style( 'animate', MASCOT_TEMPLATE_URI . '/assets/css/animate.min.css' );

			wp_enqueue_style( 'mascot-preloader', MASCOT_TEMPLATE_URI . '/assets/css/preloader.css' );
			wp_enqueue_style( 'mascot-custom-bootstrap-margin-padding', MASCOT_TEMPLATE_URI . '/assets/css/custom-bootstrap-margin-padding.css' );
			wp_enqueue_style( 'mascot-menuzord-megamenu', MASCOT_TEMPLATE_URI . '/assets/js/plugins/menuzord/css/menuzord.css' );
			wp_enqueue_style( 'mascot-menuzord-megamenu-animations', MASCOT_TEMPLATE_URI . '/assets/js/plugins/menuzord/css/menuzord-animations.css' );



			/**
			 * Enque Fonts
			 */
			//font-awesome icons
			wp_enqueue_style( 'font-awesome', MASCOT_TEMPLATE_URI . '/assets/css/font-awesome.min.css' );
			wp_enqueue_style( 'font-awesome-animation', MASCOT_TEMPLATE_URI . '/assets/css/font-awesome-animation.min.css' );

			//iconfonts
			wp_enqueue_style( 'pe-icon-7-stroke', MASCOT_TEMPLATE_URI . '/assets/css/pe-icon-7-stroke.css' );
			wp_enqueue_style( 'elegant-icons', MASCOT_TEMPLATE_URI . '/assets/css/elegant-icons.css' );
			wp_enqueue_style( 'ion-icons', MASCOT_TEMPLATE_URI . '/assets/css/ionicons.css' );

			//Theme Related Icon
			wp_enqueue_style( 'flaticon-set-medical', MASCOT_TEMPLATE_URI . '/assets/css/flaticon-set-medical.css' );


			//google fonts
			wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700,800|Playfair+Display:400,400italic,700,700italic', null, null );



			/**
			 * Enque Script
			 */
			wp_enqueue_script( 'bootstrap', MASCOT_TEMPLATE_URI . '/assets/js/plugins/bootstrap.min.js', array('jquery'), false, true );
			wp_enqueue_script( 'mascot-jquery-plugin-collection', MASCOT_TEMPLATE_URI . '/assets/js/jquery-plugin-collection.js', array('jquery'), false, true );
			wp_enqueue_script( 'mascot-menuzord-megamenu', MASCOT_TEMPLATE_URI . '/assets/js/plugins/menuzord/js/menuzord.js', array('jquery'), false, true );

			//external plugins single file:
			wp_enqueue_script( 'appear', MASCOT_TEMPLATE_URI . '/assets/js/plugins/jquery.appear.js', array('jquery'), false, true );
			wp_enqueue_script( 'imagesloaded', MASCOT_TEMPLATE_URI . '/assets/js/plugins/imagesloaded.pkgd.min.js', array('jquery'), false, true );
			wp_enqueue_script( 'infinite-scroll', MASCOT_TEMPLATE_URI . '/assets/js/plugins/infinite-scroll.pkgd.min.js', array('jquery'), false, true );
			wp_enqueue_script( 'isotope', MASCOT_TEMPLATE_URI . '/assets/js/plugins/isotope.pkgd.min.js', array('jquery'), false, true );
			wp_enqueue_script( 'scrolltofixed', MASCOT_TEMPLATE_URI . '/assets/js/plugins/jquery-scrolltofixed-min.js', array('jquery'), false, true );
			wp_enqueue_script( 'animateNumbers', MASCOT_TEMPLATE_URI . '/assets/js/plugins/jquery.animateNumbers.min.js', array('jquery'), false, true );
			wp_enqueue_script( 'appear', MASCOT_TEMPLATE_URI . '/assets/js/plugins/jquery.appear.js', array('jquery'), false, true );
			wp_enqueue_script( 'countdown', MASCOT_TEMPLATE_URI . '/assets/js/plugins/jquery.countdown.min.js', array('jquery'), false, true );
			wp_enqueue_script( 'easing', MASCOT_TEMPLATE_URI . '/assets/js/plugins/jquery.easing.min.js', array('jquery'), false, true );
			wp_enqueue_script( 'fitvids', MASCOT_TEMPLATE_URI . '/assets/js/plugins/jquery.fitvids.js', array('jquery'), false, true );
			wp_enqueue_script( 'localScroll', MASCOT_TEMPLATE_URI . '/assets/js/plugins/jquery.localScroll.min.js', array('jquery'), false, true );
			wp_enqueue_script( 'parallax', MASCOT_TEMPLATE_URI . '/assets/js/plugins/jquery.parallax-1.1.3.js', array('jquery'), false, true );
			wp_enqueue_script( 'scrollTo', MASCOT_TEMPLATE_URI . '/assets/js/plugins/jquery.scrollTo.min.js', array('jquery'), false, true );
			wp_enqueue_script( 'sticky', MASCOT_TEMPLATE_URI . '/assets/js/plugins/jquery.sticky.js', array('jquery'), false, true );
			wp_enqueue_script( 'touchSwipe', MASCOT_TEMPLATE_URI . '/assets/js/plugins/jquery.touchSwipe.min.js', array('jquery'), false, true );
			wp_enqueue_script( 'ScrollMagic', MASCOT_TEMPLATE_URI . '/assets/js/plugins/ScrollMagic.min.js', array('jquery'), false, true );
			wp_enqueue_script( 'typed', MASCOT_TEMPLATE_URI . '/assets/js/plugins/typed.min.js', array('jquery'), false, true );
			wp_enqueue_script( 'wow', MASCOT_TEMPLATE_URI . '/assets/js/plugins/wow.min.js', array('jquery'), false, true );


			//external plugins js & css:
			wp_enqueue_script( 'bxslider', MASCOT_TEMPLATE_URI . '/assets/js/plugins/bxslider/jquery.bxslider.min.js', array('jquery'), false, true );
			wp_enqueue_style( 'bxslider', MASCOT_TEMPLATE_URI . '/assets/js/plugins/bxslider/jquery.bxslider.min.css' );
			
			wp_enqueue_script( 'flexslider', MASCOT_TEMPLATE_URI . '/assets/js/plugins/flexslider/jquery.flexslider-min.js', array('jquery'), false, true );
			wp_enqueue_style( 'flexslider', MASCOT_TEMPLATE_URI . '/assets/js/plugins/flexslider/flexslider.css' );
			
			wp_enqueue_script( 'flipclock', MASCOT_TEMPLATE_URI . '/assets/js/plugins/flipclock/flipclock.min.js', array('jquery'), false, true );
			wp_enqueue_style( 'flipclock', MASCOT_TEMPLATE_URI . '/assets/js/plugins/flipclock/flipclock.css' );
			
			wp_enqueue_script( 'owl-carousel', MASCOT_TEMPLATE_URI . '/assets/js/plugins/owl-carousel/owl.carousel.min.js', array('jquery'), false, true );
			wp_enqueue_style( 'owl-carousel', MASCOT_TEMPLATE_URI . '/assets/js/plugins/owl-carousel/assets/owl.carousel.min.css' );
			
			wp_enqueue_script( 'magnific-popup', MASCOT_TEMPLATE_URI . '/assets/js/plugins/magnific-popup/jquery.magnific-popup.min.js', array('jquery'), false, true );
			wp_enqueue_style( 'magnific-popup', MASCOT_TEMPLATE_URI . '/assets/js/plugins/magnific-popup/magnific-popup.css' );
			
			wp_enqueue_script( 'prettyphoto', MASCOT_TEMPLATE_URI . '/assets/js/plugins/prettyphoto/js/jquery.prettyPhoto.js', array('jquery'), false, true );
			wp_enqueue_style( 'prettyphoto', MASCOT_TEMPLATE_URI . '/assets/js/plugins/prettyphoto/css/prettyPhoto.css' );

			//TwentyTwenty Before After Slider
			wp_enqueue_script( 'twentytwenty-event-move', MASCOT_TEMPLATE_URI . '/assets/js/plugins/twentytwenty/jquery.event.move.js', array('jquery'), false, true );
			wp_enqueue_script( 'twentytwenty', MASCOT_TEMPLATE_URI . '/assets/js/plugins/twentytwenty/jquery.twentytwenty.js', array('jquery'), false, true );
			wp_enqueue_style( 'twentytwenty', MASCOT_TEMPLATE_URI . '/assets/js/plugins/twentytwenty/twentytwenty.css' );


		
			//Register Google Map Scripts
			$google_maps_api_key = medicale_mascot_get_redux_option( 'theme-api-settings-gmaps-api-key' );
			if( $google_maps_api_key ) {
				wp_enqueue_script( 'google-maps-api', 'http://maps.google.com/maps/api/js?key=' . $google_maps_api_key, array(), false, true );
			} else {
				wp_enqueue_script( 'google-maps-api', 'http://maps.google.com/maps/api/js', array(), false, true );
			}
			wp_enqueue_script( 'google-maps-init', MASCOT_TEMPLATE_URI . '/assets/js/google-map-init.js', array('jquery'), false, true );


			//Theme Custom JS
			wp_enqueue_script( 'mascot-custom', MASCOT_TEMPLATE_URI . '/assets/js/custom.js', array('jquery'), false, true );

			
			//used when needed:
			wp_register_script( 'swiper', MASCOT_TEMPLATE_URI . '/assets/js/plugins/swiper-slider/js/swiper.jquery.min.js', array('jquery'), false, true );
			wp_register_style( 'swiper', MASCOT_TEMPLATE_URI . '/assets/js/plugins/swiper-slider/css/swiper.min.css' );

			wp_register_script( 'menuFullpage', MASCOT_TEMPLATE_URI . '/assets/js/plugins/menuFullpage/menuFullpage.min.js', array('jquery'), false, true );
			wp_register_style( 'menuFullpage', MASCOT_TEMPLATE_URI . '/assets/js/plugins/menuFullpage/menuFullpage.css' );

			wp_register_script( 'fullPage', MASCOT_TEMPLATE_URI . '/assets/js/plugins/slider-fullPage/jquery.fullPage.min.js', array('jquery'), false, true );
			wp_register_style( 'fullPage', MASCOT_TEMPLATE_URI . '/assets/js/plugins/slider-fullPage/jquery.fullPage.css' );
			
			wp_register_script( 'multiscroll', MASCOT_TEMPLATE_URI . '/assets/js/plugins/slider-multiscroll/jquery.multiscroll.min.js', array('jquery'), false, true );
			wp_register_style( 'multiscroll', MASCOT_TEMPLATE_URI . '/assets/js/plugins/slider-multiscroll/jquery.multiscroll.css' );
			
			wp_register_script( 'pagepiling', MASCOT_TEMPLATE_URI . '/assets/js/plugins/slider-pagepiling/jquery.pagepiling.min.js', array('jquery'), false, true );
			wp_register_style( 'pagepiling', MASCOT_TEMPLATE_URI . '/assets/js/plugins/slider-pagepiling/jquery.pagepiling.css' );




			/**
			 * Enque Styles At the End of the queue
			 */
			wp_enqueue_style( 'mascot-utility-classes', MASCOT_TEMPLATE_URI . '/assets/css/utility-classes.css' );
			wp_enqueue_style( 'mascot-style-main', MASCOT_TEMPLATE_URI . '/assets/css/style-main.css', array(), MASCOT_THEME_VERSION );
			wp_enqueue_style( 'mascot-responsive', MASCOT_TEMPLATE_URI . '/assets/css/responsive.css' );



			//Theme Color
			if( !empty(medicale_mascot_get_redux_option( 'theme-color-settings-theme-color-type' )) && medicale_mascot_get_redux_option( 'theme-color-settings-theme-color-type' ) == 'predefined' ) {
				//Primary Theme Color
				$choosed_primary_theme_color = !empty( medicale_mascot_get_redux_option( 'theme-color-settings-primary-theme-color' ) ) ? medicale_mascot_get_redux_option( 'theme-color-settings-primary-theme-color' ) : '';
				
				wp_enqueue_style( 'mascot-primary-theme-color', MASCOT_TEMPLATE_URI . '/assets/css/colors/'. $choosed_primary_theme_color );
			} else if( !empty(medicale_mascot_get_redux_option( 'theme-color-settings-theme-color-type' )) && medicale_mascot_get_redux_option( 'theme-color-settings-theme-color-type' ) == 'custom' ) {
				//Custom Theme Color
				if ( !is_multisite() ) {
					if ( file_exists( MASCOT_ASSETS_DIR . '/css/colors/custom-theme-color.css' ) ) {
						wp_enqueue_style( 'mascot-primary-theme-color', MASCOT_TEMPLATE_URI . '/assets/css/colors/custom-theme-color.css' );
					}
				} else {
					if ( file_exists( MASCOT_ASSETS_DIR . '/css/colors/custom-theme-color-msid-' . medicale_mascot_get_multisite_blog_id() . '.css' ) ) {
						wp_enqueue_style( 'mascot-primary-theme-color', MASCOT_TEMPLATE_URI . '/assets/css/colors/custom-theme-color-msid-' . medicale_mascot_get_multisite_blog_id() . '.css' );
					}
				}

			} else {
				wp_enqueue_style( 'mascot-primary-theme-color', MASCOT_TEMPLATE_URI . '/assets/css/colors/theme-skin-blue.css' );
			}

			//Dynamic Style
			if ( !is_multisite() ) {
				if ( file_exists( MASCOT_ASSETS_DIR . '/css/dynamic-style.css' ) ) {
					wp_enqueue_style( 'mascot-dynamic-style', MASCOT_TEMPLATE_URI . '/assets/css/dynamic-style.css' );
				}
			} else {
				if ( file_exists( MASCOT_ASSETS_DIR . '/css/dynamic-style-msid-' . medicale_mascot_get_multisite_blog_id() . '.css' ) ) {
					wp_enqueue_style( 'mascot-dynamic-style', MASCOT_TEMPLATE_URI . '/assets/css/dynamic-style-msid-' . medicale_mascot_get_multisite_blog_id() . '.css' );
				}
			}


			//Enqueue comment-reply.js 
			if ( is_singular() && comments_open() && get_option('thread_comments') ) {
				wp_enqueue_script( 'comment-reply' );
			}


			if( is_rtl() ) {
				wp_enqueue_style( 'mascot-bootstrap-rtl', MASCOT_TEMPLATE_URI . '/assets/css/bootstrap-rtl.min.css' );

				wp_deregister_style( 'mascot-menuzord-megamenu');
				wp_enqueue_style( 'mascot-menuzord-megamenu-rtl', MASCOT_TEMPLATE_URI . '/assets/js/plugins/menuzord/css/menuzord-rtl.css' );

				wp_deregister_style( 'mascot-style-main');
				wp_enqueue_style( 'mascot-style-main-rtl', MASCOT_TEMPLATE_URI . '/assets/css/style-main-rtl.css', array(), MASCOT_THEME_VERSION );

				wp_enqueue_style( 'mascot-style-main-rtl-extra', MASCOT_TEMPLATE_URI . '/assets/css/style-main-rtl-extra.css' );
			}

		}
	}
}

if(!function_exists('medicale_mascot_action_theme_admin_enqueue_scripts')) {
	/**
	 * Add Admin Scripts
	 */
	function medicale_mascot_action_theme_admin_enqueue_scripts() {
		wp_enqueue_style( 'font-awesome', MASCOT_TEMPLATE_URI . '/assets/css/font-awesome.min.css' );
		wp_enqueue_style( 'pe-icon-7-stroke', MASCOT_TEMPLATE_URI . '/assets/css/pe-icon-7-stroke.css' );
		wp_enqueue_style( 'ion-icons', MASCOT_TEMPLATE_URI . '/assets/css/ionicons.css' );
		wp_enqueue_style( 'flaticon-set-medical', MASCOT_TEMPLATE_URI . '/assets/css/flaticon-set-medical.css' );
			
		wp_enqueue_style( 'mascot-custom-admin', MASCOT_TEMPLATE_URI . '/admin/assets/css/custom-admin.css' );

		wp_enqueue_script( 'mascot-admin-js', MASCOT_TEMPLATE_URI . '/admin/assets/js/admin.js', array('jquery'), null, true );
	}
}
