<?php

/**
 * Predefine demo imports
 */
function medicale_mascot_ocdi_import_files() {
  return array(
      array(
          'import_file_name'             => esc_html__( 'Demo Import - Medicale', 'medicale-wp' ),
          'categories'                   => array( 'Medicale' ),
          'local_import_file'            => trailingslashit( get_template_directory() ) . 'mascot-framework/tgm/demo-content/medicale-healthmedicalresponsivewordpresstheme.wordpress.2017-08-27.xml',
          'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'mascot-framework/tgm/demo-content/kodesolution.net-demo-wp-medicale-wp-widgets.wie',
          /*'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'mascot-framework/tgm/demo-content/customizer.dat',*/
          'local_import_redux'           => array(
              array(
                  'file_path'   => trailingslashit( get_template_directory() ) . 'mascot-framework/tgm/demo-content/redux_options_medicale_mascot_redux_theme_opt_backup_27-08-2017.json',
                  'option_name' => 'medicale_mascot_redux_theme_opt',
              ),
          ),
          'import_preview_image_url'     => 'http://www.your_domain.com/ocdi/preview_import_image1.jpg',
          'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the slider separately.', 'medicale-wp' ),
          'preview_url'                  => 'http://thememascot.net/demo/wp/medicale/',
      ),
  );
}
add_filter( 'pt-ocdi/import_files', 'medicale_mascot_ocdi_import_files' );


/**
 * Automatically assign “Front page”, “Posts page” and menu locations after the importer is done
 */
function medicale_mascot_ocdi_after_import_setup() {
    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Top Primary Nav', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Index Multipage Layout1' );
    $blog_page_id  = get_page_by_title( 'Blog Classic Right Sidebar' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'medicale_mascot_ocdi_after_import_setup' );



/**
 * Change the location, title and other parameters of the plugin page
 */
function medicale_mascot_ocdi_plugin_page_setup( $default_settings ) {
    $default_settings['parent_slug'] = 'themes.php';
    $default_settings['page_title']  = esc_html__( 'One Click Demo Import - ThemeMascot' , 'medicale-wp' );
    $default_settings['menu_title']  = esc_html__( 'Import Demo Data' , 'medicale-wp' );
    $default_settings['capability']  = 'import';
    $default_settings['menu_slug']   = 'tm-one-click-demo-import';

    return $default_settings;
}
add_filter( 'pt-ocdi/plugin_page_setup', 'medicale_mascot_ocdi_plugin_page_setup' );


add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );