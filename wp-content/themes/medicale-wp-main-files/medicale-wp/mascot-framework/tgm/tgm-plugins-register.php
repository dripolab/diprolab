<?php
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for parent theme SampleTGM for publication on ThemeForest
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

/**
 * Include the TGM_Plugin_Activation class.
 *
 * Depending on your implementation, you may want to change the include call:
 *
 * Parent Theme:
 * require_once get_template_directory() . '/path/to/class-tgm-plugin-activation.php';
 *
 * Child Theme:
 * require_once get_stylesheet_directory() . '/path/to/class-tgm-plugin-activation.php';
 *
 * Plugin:
 * require_once dirname( __FILE__ ) . '/path/to/class-tgm-plugin-activation.php';
 */
require_once MASCOT_FRAMEWORK_DIR . '/tgm/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'medicale_mascot_register_required_plugins' );

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
 */
function medicale_mascot_register_required_plugins() {
	$plugins_path = MASCOT_FRAMEWORK_DIR . '/tgm/plugins';
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(
		array(
			'name'         	=> 'Mascot Core - Medicale WP',
			'slug'         	=> 'mascot-core-medicale-wp',
			'source'       	=> $plugins_path . '/mascot-core-medicale-wp-08-21-17-v2.zip',
			'required'     	=> true,
			'version'      	=> '1.0'
		),
		array(
			'name'         	=> 'Mascot Twitter Feed',
			'slug'         	=> 'mascot-twitter-feed',
			'source'       	=> $plugins_path . '/mascot-twitter-feed-08-12-17-v2.zip',
			'required'     	=> true,
			'version'      	=> '1.0'
		),
		array(
			'name'         	=> 'One Click Demo Import',
			'slug'         	=> 'one-click-demo-import',
			'required'     	=> true,
		),
		array(
			'name'         	=> 'WPBakery Visual Composer',
			'slug'         	=> 'js_composer',
			'source'       	=> $plugins_path . '/js_composer-5.2.1.zip',
			'required'     	=> true,
			'external_url' 	=> 'http://vc.wpbakery.com',
			'version'      	=> '5.2.1'
		),
		array(
			'name'			=> 'Contact Form 7',
			'slug'			=> 'contact-form-7',
			'required'		=> true
		),
		array(
			'name'			=> 'Contact Form 7 Datepicker',
			'slug'			=> 'contact-form-7-datepicker',
			'required'		=> true
		),
		array(
			'name'         	=> 'Revolution Slider',
			'slug'         	=> 'revslider',
			'source'       	=> $plugins_path . '/revslider-5.4.5.1.zip',
			'required'     	=> true,
			'external_url' 	=> 'http://revolution.themepunch.com/',
			'version'      	=> '5.4.5.1'
		),
		array(
			'name'         	=> 'Ultimate Addons for Visual Composer',
			'slug'         	=> 'Ultimate_VC_Addons',
			'source'       	=> $plugins_path . '/Ultimate_VC_Addons-3.16.15.zip',
			'required'     	=> true,
			'external_url' 	=> 'https://brainstormforce.com/demos/ultimate/',
			'version'      	=> '3.16.15'
		),
		array(
			'name'         	=> 'Layer Slider',
			'slug'         	=> 'LayerSlider',
			'source'       	=> $plugins_path . '/layersliderwp-6.5.7.installable.zip',
			'required'     	=> false,
			'external_url' 	=> 'https://brainstormforce.com/demos/ultimate/',
			'version'      	=> '6.5.7'
		),
		array(
			'name'         	=> 'Timetable Responsive Schedule For WordPress',
			'slug'         	=> 'timetable',
			'source'       	=> $plugins_path . '/timetable-v3.9.zip',
			'required'     	=> true,
			'external_url' 	=> 'http://codecanyon.net/item/timetable-responsive-schedule-for-wordpress/7010836',
			'version'      	=> 'v3.9'
		),
		array(
			'name'         	=> 'Booked - Appointment Booking for WordPress',
			'slug'         	=> 'booked',
			'source'       	=> $plugins_path . '/booked-v2.04.zip',
			'required'     	=> true,
			'external_url' 	=> 'http://codecanyon.net/item/timetable-responsive-schedule-for-wordpress/7010836',
			'version'      	=> '2.0.4'
		),
		array(
			'name'         	=> 'Essential Grid WordPress Plugin',
			'slug'         	=> 'essential-grid',
			'source'       	=> $plugins_path . '/essential-grid-2.1.6.1.zip',
			'required'     	=> false,
			'external_url' 	=> 'https://codecanyon.net/item/essential-grid-wordpress-plugin/7563340',
			'version'      	=> '2.1.6.1'
		),



		array(
			'name'      	=> 'WooCommerce',
			'slug'      	=> 'woocommerce',
			'required'  	=> true
		),
		array(
			'name'			=> 'YITH WooCommerce Quick View',
			'slug'			=> 'yith-woocommerce-quick-view',
			'required'		=> false
		),
		array(
			'name'			=> 'YITH WooCommerce Compare',
			'slug'			=> 'yith-woocommerce-compare',
			'required'		=> false
		),
		array(
			'name'			=> 'YITH WooCommerce Wishlist',
			'slug'			=> 'yith-woocommerce-wishlist',
			'required'		=> false
		),
		array(
			'name'          => 'Envato Market',
			'slug'          => 'envato-market',
			'source'        => 'https://github.com/envato/wp-envato-market/blob/gh-pages/dist/envato-market.zip?raw=true',
			'required'      => true,
			'version'       => '1.0.0-beta',
			'external_url'  => 'https://github.com/envato/wp-envato-market',
		),
		array(
			'name'			=> 'MailChimp for WordPress Lite',
			'slug'			=> 'mailchimp-for-wp',
			'required'		=> false
		),
		array(
			'name'			=> 'TinyMCE Advanced',
			'slug'			=> 'tinymce-advanced',
			'required'		=> false
		),
		array(
			'name'			=> 'Breadcrumb NavXT',
			'slug'			=> 'breadcrumb-navxt',
			'required'		=> false
		),


		/*

		array(
			'name'     => 'Regenerate Thumbnails',
			'slug'     => 'regenerate-thumbnails',
			'required' => false
		),
		array(
			'name'     => 'Force Regenerate Thumbnails',
			'slug'     => 'force-regenerate-thumbnails',
			'required' => false
		),
		array(
			'name'     => 'Recent Tweets Widget',
			'slug'     => 'recent-tweets-widget',
			'required' => false
		),
		array(
			'name'     => 'Instagram Feed',
			'slug'     => 'instagram-feed',
			'required' => false
		),
		array(
			'name'     => 'Flickr Badges Widget',
			'slug'     => 'flickr-badges-widget',
			'required' => false
		),
		array(
			'name'     => 'AddToAny Share Buttons',
			'slug'     => 'add-to-any',
			'required' => false
		)*/



	);

	$theme_obj     = wp_get_theme();
	$theme_name    = $theme_obj->get( 'Name' );
	$theme_version = $theme_obj->get( 'Version' );
	
	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
	$config = array(
		'id'           => 'medicale-wp',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '<div class="notice notice-info is-dismissible" style="margin-top: 20px;">
			<p><strong>Theme Name: </strong>' . $theme_name . '</p>
			<p><strong>Current Version: </strong>' . $theme_version . '</p>
			<p><strong>Note from Author ' . AUTHOR . ':</strong> To ensure the latest version of <em>theme required</em> plugins, you must have the latest version of <strong>' . $theme_name . '</strong></p>
			<p style="font-size: 12px; color: #999;">If any of theme required plugins has released new update after the date that we released ' . "{$theme_name} {$theme_version}" . ', we will include the latest version of that plugin in the next update.</p>
		</div>',                      // Message to output right before the plugins table.

		/*
		'strings'      => array(
			'page_title'                      => esc_html__( 'Install Required Plugins', 'medicale-wp' ),
			'menu_title'                      => esc_html__( 'Install Plugins', 'medicale-wp' ),
			/* translators: %s: plugin name. * /
			'installing'                      => esc_html__( 'Installing Plugin: %s', 'medicale-wp' ),
			/* translators: %s: plugin name. * /
			'updating'                        => esc_html__( 'Updating Plugin: %s', 'medicale-wp' ),
			'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'medicale-wp' ),
			'notice_can_install_required'     => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'medicale-wp'
			),
			'notice_can_install_recommended'  => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme recommends the following plugin: %1$s.',
				'This theme recommends the following plugins: %1$s.',
				'medicale-wp'
			),
			'notice_ask_to_update'            => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
				'medicale-wp'
			),
			'notice_ask_to_update_maybe'      => _n_noop(
				/* translators: 1: plugin name(s). * /
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'medicale-wp'
			),
			'notice_can_activate_required'    => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'medicale-wp'
			),
			'notice_can_activate_recommended' => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'medicale-wp'
			),
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'medicale-wp'
			),
			'update_link'						=> _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'medicale-wp'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'medicale-wp'
			),
			'return'                          => esc_html__( 'Return to Required Plugins Installer', 'medicale-wp' ),
			'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'medicale-wp' ),
			'activated_successfully'          => esc_html__( 'The following plugin was activated successfully:', 'medicale-wp' ),
			/* translators: 1: plugin name. * /
			'plugin_already_active'           => esc_html__( 'No action taken. Plugin %1$s was already active.', 'medicale-wp' ),
			/* translators: 1: plugin name. * /
			'plugin_needs_higher_version'     => esc_html__( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'medicale-wp' ),
			/* translators: 1: dashboard link. * /
			'complete'                        => esc_html__( 'All plugins installed and activated successfully. %1$s', 'medicale-wp' ),
			'dismiss'                         => esc_html__( 'Dismiss this notice', 'medicale-wp' ),
			'notice_cannot_install_activate'  => esc_html__( 'There are one or more required or recommended plugins to install, update or activate.', 'medicale-wp' ),
			'contact_admin'                   => esc_html__( 'Please contact the administrator of this site for help.', 'medicale-wp' ),

			'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
		),
		*/
	);

	tgmpa( $plugins, $config );
}
