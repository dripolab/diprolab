<?php
/*
Plugin Name: Sweet Custom Menu
Plugin URL: http://remicorson.com/sweet-custom-menu
Description: A little plugin to add attributes to WordPress menus
Version: 0.1
Author: Remi Corson
Author URI: http://remicorson.com
Contributors: corsonr
Text Domain: rc_scm
Domain Path: languages
*/

class Medicale_Mascot_RC_Sweet_Custom_Menu {

	/*--------------------------------------------*
	 * Constructor
	 *--------------------------------------------*/

	/**
	 * Initializes the plugin by setting localization, filters, and administration functions.
	 */
	function __construct() {

		// load the plugin translation files
		//add_action( 'init', array( $this, 'textdomain' ) );
		
		// add custom menu fields to menu
		add_filter( 'wp_setup_nav_menu_item', array( $this, 'rc_scm_add_custom_nav_fields' ) );

		// save menu custom fields
		add_action( 'wp_update_nav_menu_item', array( $this, 'rc_scm_update_custom_nav_fields'), 10, 3 );
		
		// edit menu walker
		add_filter( 'wp_edit_nav_menu_walker', array( $this, 'rc_scm_edit_walker'), 10, 2 );

	} // end constructor
	
	
	/**
	 * Load the plugin's text domain
	 *
	 * @since 1.0
	 *
	 * @return void
	 */
	public function textdomain() {
	}
	
	/**
	 * Add custom fields to $item nav object
	 * in order to be used in custom Walker
	 *
	 * @access      public
	 * @since       1.0 
	 * @return      void
	*/
	function rc_scm_add_custom_nav_fields( $menu_item ) {

		$menu_item->mascot_dropdownposition = get_post_meta( $menu_item->ID, '_menu_item_mascot_dropdownposition', true );
		$menu_item->mascot_subtitle = get_post_meta( $menu_item->ID, '_menu_item_mascot_subtitle', true );
		$menu_item->mascot_menuicon = get_post_meta( $menu_item->ID, '_menu_item_mascot_menuicon', true );

		if( medicale_mascot_get_redux_option( 'header-menu-megamenu-enable-megamenu' ) == 1 ) {
			if ( ! $menu_item->menu_item_parent ) {
				$menu_item->mascot_megamenu_status = get_post_meta( $menu_item->ID, '_menu_item_mascot_megamenu_status', true );
				$menu_item->mascot_megamenu_containerwidth = get_post_meta( $menu_item->ID, '_menu_item_mascot_megamenu_containerwidth', true );
			} else {
				$menu_item->mascot_megamenu_gridcolumnwidth = get_post_meta( $menu_item->ID, '_menu_item_mascot_megamenu_gridcolumnwidth', true );
				$menu_item->mascot_megamenu_title = get_post_meta( $menu_item->ID, '_menu_item_mascot_megamenu_title', true );
				$menu_item->mascot_megamenu_widgetarea = get_post_meta( $menu_item->ID, '_menu_item_mascot_megamenu_widgetarea', true );
			}
			$menu_item->mascot_megamenu_bgimage = get_post_meta( $menu_item->ID, '_menu_item_mascot_megamenu_bgimage', true );
		}
		return $menu_item;
		
	}
	
	/**
	 * Save menu custom fields
	 *
	 * @access      public
	 * @since       1.0 
	 * @return      void
	*/
	function rc_scm_update_custom_nav_fields( $menu_id, $menu_item_db_id, $args ) {

		// Check if element is properly sent
		if ( ! isset( $_REQUEST[ 'menu-item-mascot-dropdownposition' ][ $menu_item_db_id ] ) ) {
			$_REQUEST[ 'menu-item-mascot-dropdownposition' ][ $menu_item_db_id ] = '';
		}
		$dropdownposition_value = $_REQUEST[ 'menu-item-mascot-dropdownposition' ][ $menu_item_db_id ];
		update_post_meta( $menu_item_db_id, '_menu_item_mascot_dropdownposition', $dropdownposition_value );

		if ( isset( $_REQUEST['menu-item-mascot-subtitle'] ) && is_array( $_REQUEST['menu-item-mascot-subtitle'] ) ) {
				$subtitle_value = $_REQUEST['menu-item-mascot-subtitle'][$menu_item_db_id];
				update_post_meta( $menu_item_db_id, '_menu_item_mascot_subtitle', $subtitle_value );
		}
		if ( isset( $_REQUEST['menu-item-mascot-menuicon'] ) && is_array( $_REQUEST['menu-item-mascot-menuicon'] ) ) {
				$subtitle_value = $_REQUEST['menu-item-mascot-menuicon'][$menu_item_db_id];
				update_post_meta( $menu_item_db_id, '_menu_item_mascot_menuicon', $subtitle_value );
		}


		if( medicale_mascot_get_redux_option( 'header-menu-megamenu-enable-megamenu' ) == 1 ) {
			$field_name_suffix = array( 'gridcolumnwidth', 'title', 'widgetarea', 'bgimage' );
			if ( ! $args['menu-item-parent-id'] ) {
				$field_name_suffix = array( 'status', 'containerwidth', 'bgimage' );
			}
			foreach ( $field_name_suffix as $key ) {
				if ( ! isset( $_REQUEST[ 'menu-item-mascot-megamenu-' . $key ][ $menu_item_db_id ] ) ) {
					$_REQUEST[ 'menu-item-mascot-megamenu-' . $key ][ $menu_item_db_id ] = '';
				}
				$value = $_REQUEST[ 'menu-item-mascot-megamenu-' . $key ][ $menu_item_db_id ];
				update_post_meta( $menu_item_db_id, '_menu_item_mascot_megamenu_' . $key, $value );
			}
		}
	}
	
	/**
	 * Define new Walker edit
	 *
	 * @access      public
	 * @since       1.0 
	 * @return      void
	*/
	function rc_scm_edit_walker($walker,$menu_id) {
	
		return 'Medicale_Mascot_Walker_Nav_Menu_Edit_Custom';
		
	}

}

// instantiate plugin's class
$GLOBALS['sweet_custom_menu'] = new Medicale_Mascot_RC_Sweet_Custom_Menu();


include_once( 'edit_custom_walker.php' );
include_once( 'custom_walker.php' );