<?php
/**
 *  /!\ This is a copy of Walker_Nav_Menu_Edit class in core
 * 
 * Create HTML list of nav menu input items.
 *
 * @package WordPress
 * @since 3.0.0
 * @uses Walker_Nav_Menu
 */




// Add the mega menu custom fields to the menu fields.
add_action( 'medicale_mascot_wp_nav_megamenu_item_custom_fields', 'medicale_mascot_wp_add_megamenu_fields', 20, 4 );

/**
 * Adds the menu markup.
 *
 * @param string $item_id The ID of the menu item.
 * @param object $item    The menu item object.
 * @param int    $depth   The depth of the current item in the menu.
 * @param array  $args    Menu arguments.
 */
function medicale_mascot_wp_add_megamenu_fields( $item_id, $item, $depth, $args ) {
	?>
	<div class="clear"></div>
	<div class="mascot-mega-menu-options">
		<p class="field-megamenu-status description description-wide">
			<label for="edit-menu-item-mascot-megamenu-status-<?php echo $item_id; ?>">
				<input type="checkbox" id="edit-menu-item-mascot-megamenu-status-<?php echo $item_id; ?>" class="widefat code edit-menu-item-mascot-megamenu-status" name="menu-item-mascot-megamenu-status[<?php echo $item_id; ?>]" value="enabled" <?php checked( $item->mascot_megamenu_status, 'enabled' ); ?> />
				<strong><?php esc_attr_e( 'Enable Mega Menu (only for main menu)', 'medicale-wp' ); ?></strong>
			</label>
		</p>
		<p class="field-megamenu-containerwidth description description-wide">
			<label for="edit-menu-item-mascot-megamenu-containerwidth-<?php echo $item_id; ?>">
				<?php esc_attr_e( 'Mega Menu Container Width', 'medicale-wp' ); ?>
				<select id="edit-menu-item-mascot-megamenu-containerwidth-<?php echo $item_id; ?>" class="widefat code edit-menu-item-mascot-megamenu-containerwidth" name="menu-item-mascot-megamenu-containerwidth[<?php echo $item_id; ?>]">
					<option value="megamenu-fullwidth" <?php selected( $item->mascot_megamenu_containerwidth, 'megamenu-fullwidth' ); ?>><?php esc_attr_e( 'Fullwidth', 'medicale-wp' ); ?></option>
					<option value="megamenu-three-quarter-width" <?php selected( $item->mascot_megamenu_containerwidth, 'megamenu-three-quarter-width' ); ?>><?php esc_attr_e( 'Three Quarter Width', 'medicale-wp' ); ?></option>
					<option value="megamenu-half-width" <?php selected( $item->mascot_megamenu_containerwidth, 'megamenu-half-width' ); ?>><?php esc_attr_e( 'Half Width', 'medicale-wp' ); ?></option>
					<option value="megamenu-quarter-width" <?php selected( $item->mascot_megamenu_containerwidth, 'megamenu-quarter-width' ); ?>><?php esc_attr_e( 'Quarter Width', 'medicale-wp' ); ?></option>
				</select>
			</label>
		</p>
		<p class="field-megamenu-gridcolumnwidth description description-wide">
			<label for="edit-menu-item-mascot-megamenu-gridcolumnwidth-<?php echo $item_id; ?>">
				<?php esc_attr_e( 'Mega Menu Column Width in Grid system', 'medicale-wp' ); ?>
				<select id="edit-menu-item-mascot-megamenu-gridcolumnwidth-<?php echo $item_id; ?>" class="widefat code edit-menu-item-mascot-megamenu-gridcolumnwidth" name="menu-item-mascot-megamenu-gridcolumnwidth[<?php echo $item_id; ?>]">
					<option value="12" <?php selected( $item->mascot_megamenu_gridcolumnwidth, 'auto' ); ?>><?php esc_attr_e( 'Auto', 'medicale-wp' ); ?></option>
					<option value="1" <?php selected( $item->mascot_megamenu_gridcolumnwidth, '1' ); ?>>1</option>
					<option value="2" <?php selected( $item->mascot_megamenu_gridcolumnwidth, '2' ); ?>>2</option>
					<option value="3" <?php selected( $item->mascot_megamenu_gridcolumnwidth, '3' ); ?>>3</option>
					<option value="4" <?php selected( $item->mascot_megamenu_gridcolumnwidth, '4' ); ?>>4</option>
					<option value="5" <?php selected( $item->mascot_megamenu_gridcolumnwidth, '5' ); ?>>5</option>
					<option value="6" <?php selected( $item->mascot_megamenu_gridcolumnwidth, '6' ); ?>>6</option>
					<option value="7" <?php selected( $item->mascot_megamenu_gridcolumnwidth, '7' ); ?>>7</option>
					<option value="8" <?php selected( $item->mascot_megamenu_gridcolumnwidth, '8' ); ?>>8</option>
					<option value="9" <?php selected( $item->mascot_megamenu_gridcolumnwidth, '9' ); ?>>9</option>
					<option value="10" <?php selected( $item->mascot_megamenu_gridcolumnwidth, '10' ); ?>>10</option>
					<option value="11" <?php selected( $item->mascot_megamenu_gridcolumnwidth, '11' ); ?>>11</option>
					<option value="12" <?php selected( $item->mascot_megamenu_gridcolumnwidth, '12' ); ?>>12</option>
				</select>
			</label>
		</p>
		<p class="field-megamenu-title description description-wide">
			<label for="edit-menu-item-mascot-megamenu-title-<?php echo $item_id; ?>">
				<input type="checkbox" id="edit-menu-item-mascot-megamenu-title-<?php echo $item_id; ?>" class="widefat code edit-menu-item-mascot-megamenu-title" name="menu-item-mascot-megamenu-title[<?php echo $item_id; ?>]" value="disabled" <?php checked( $item->mascot_megamenu_title, 'disabled' ); ?> />
				<?php esc_attr_e( 'Disable Mega Menu Column Title', 'medicale-wp' ); ?>
			</label>
		</p>
		<p class="field-megamenu-widgetarea description description-wide">
			<label for="edit-menu-item-mascot-megamenu-widgetarea-<?php echo $item_id; ?>">
				<?php esc_attr_e( 'Mega Menu Widget Area', 'medicale-wp' ); ?>
				<select id="edit-menu-item-mascot-megamenu-widgetarea-<?php echo $item_id; ?>" class="widefat code edit-menu-item-mascot-megamenu-widgetarea" name="menu-item-mascot-megamenu-widgetarea[<?php echo $item_id; ?>]">
					<option value="0"><?php esc_attr_e( 'Select Widget Area', 'medicale-wp' ); ?></option>
					<?php global $wp_registered_sidebars; ?>
					<?php if ( ! empty( $wp_registered_sidebars ) && is_array( $wp_registered_sidebars ) ) : ?>
						<?php foreach ( $wp_registered_sidebars as $sidebar ) : ?>
							<option value="<?php echo $sidebar['id']; ?>" <?php selected( $item->mascot_megamenu_widgetarea, $sidebar['id'] ); ?>><?php echo $sidebar['name']; ?></option>
						<?php endforeach; ?>
					<?php endif; ?>
				</select>
			</label>
		</p>

		<p class="field-megamenu-bgimage description description-wide">
			<a  id="mascot-media-upload-<?php echo $item_id; ?>" class="mascot-open-media button button-primary mascot-megamenu-upload-bgimage" data-target="#edit-menu-item-mascot-megamenu-bgimage-<?php echo $item_id; ?>" data-preview=".mascot-megamenu-bgimage-image" data-frame="select" data-state="wpc_widgets_insert_single" data-fetch="url" data-title="Insert Image" data-button="Insert" data-class="media-frame tm-widget-custom-uploader" title="Add Media"><?php esc_attr_e( 'Set bgimage', 'medicale-wp' ); ?></a>

			<label for="edit-menu-item-mascot-megamenu-bgimage-<?php echo $item_id; ?>">
				<input type="hidden" id="edit-menu-item-mascot-megamenu-bgimage-<?php echo $item_id; ?>" class="mascot-new-media-image widefat code edit-menu-item-mascot-megamenu-bgimage" name="menu-item-mascot-megamenu-bgimage[<?php echo $item_id; ?>]" value="<?php echo $item->mascot_megamenu_bgimage; ?>" />
				<img src="<?php echo $item->mascot_megamenu_bgimage; ?>" id="mascot-media-img-<?php echo $item_id; ?>" class="mascot-megamenu-bgimage-image" style="<?php echo ( trim( $item->mascot_megamenu_bgimage ) ) ? 'display:inline;' : ''; ?>" />

				<a href="#" id="mascot-media-remove-<?php echo $item_id; ?>" data-target="#edit-menu-item-mascot-megamenu-bgimage-<?php echo $item_id; ?>" data-preview=".mascot-megamenu-bgimage-image" class="remove-mascot-megamenu-bgimage" style="<?php echo ( trim( $item->mascot_megamenu_bgimage ) ) ? 'display:inline;' : ''; ?>"><?php esc_attr_e( 'Remove Image', 'medicale-wp' ); ?></a>


			</label>
		</p>
	</div><!-- .mascot-mega-menu-options-->
	<?php
}



class Medicale_Mascot_Walker_Nav_Menu_Edit_Custom extends Walker_Nav_Menu  {
	/**
	 * Starts the list before the elements are added.
	 *
	 * @since 3.0.0
	 *
	 * @see Walker::start_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of wp_nav_menu() arguments.
	 */
	function start_lvl( &$output, $depth = 0, $args = array() ) {
	}

	/**
	 * Ends the list of after the elements are added.
	 *
	 * @since 3.0.0
	 *
	 * @see Walker::end_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of wp_nav_menu() arguments.
	 */
	function end_lvl( &$output, $depth = 0, $args = array() ) {
	}
	
	/**
	 * Starts the element output.
	 *
	 * @since 3.0.0
	 * @since 4.4.0 The {@see 'nav_menu_item_args'} filter was added.
	 *
	 * @see Walker::start_el()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item   Menu item data object.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of wp_nav_menu() arguments.
	 * @param int    $id     Current item ID.
	 */
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $_wp_nav_menu_max_depth, $wp_registered_sidebars;
		$_wp_nav_menu_max_depth = $depth > $_wp_nav_menu_max_depth ? $depth : $_wp_nav_menu_max_depth;

		ob_start();
		$item_id = esc_attr( $item->ID );
		$removed_args = array(
			'action',
			'customlink-tab',
			'edit-menu-item',
			'menu-item',
			'page-tab',
			'_wpnonce',
		);

		$original_title = '';
		if ( 'taxonomy' == $item->type ) {
			$original_title = get_term_field( 'name', $item->object_id, $item->object, 'raw' );
			if ( is_wp_error( $original_title ) ) {
				$original_title = false;
			}
		} elseif ( 'post_type' == $item->type ) {
			$original_object = get_post( $item->object_id );
			$original_title  = get_the_title( $original_object->ID );
		}

		$classes = array(
			'menu-item menu-item-depth-' . $depth,
			'menu-item-' . esc_attr( $item->object ),
			'menu-item-edit-' . ( ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? 'active' : 'inactive' ),
		);

		$title = $item->title;

		if ( ! empty( $item->_invalid ) ) {
			$classes[] = 'menu-item-invalid';
			/* translators: %s: title of menu item which is invalid */
			$title = sprintf( esc_html__( '%s (Invalid)', 'medicale-wp' ), $item->title );
		} elseif ( isset( $item->post_status ) && 'draft' == $item->post_status ) {
			$classes[] = 'pending';
			/* translators: %s: title of menu item in draft status */
			$title = sprintf( esc_html__( '%s (Pending)', 'medicale-wp' ), $item->title );
		}

		$title = ( ! isset( $item->label ) || '' == $item->label ) ? $title : $item->label;

		$submenu_text = '';
		if ( 0 == $depth ) {
			$submenu_text = 'style="display:none;"';
		}

		?>
		<li id="menu-item-<?php echo $item_id; ?>" class="<?php echo implode( ' ', $classes ); ?>">
			<dl class="menu-item-bar">
				<dt class="menu-item-handle">
					<span class="item-title"><span class="menu-item-title"><?php echo esc_html( $title ); ?></span> <span class="is-submenu" <?php echo $submenu_text; ?>><?php esc_attr_e( 'sub item' , 'medicale-wp' ); ?></span></span>
					<span class="item-controls">
						<span class="item-type"><?php echo esc_html( $item->type_label ); ?></span>
						<span class="item-order hide-if-js">
							<?php
							$url1 = wp_nonce_url(
								add_query_arg(
									array( 'action' => 'move-up-menu-item', 'menu-item' => $item_id ),
									remove_query_arg( $removed_args, admin_url( 'nav-menus.php' ) )
								),
								'move-menu_item'
							);
							$url2 = wp_nonce_url(
								add_query_arg(
									array( 'action' => 'move-down-menu-item', 'menu-item' => $item_id ),
									remove_query_arg( $removed_args, admin_url( 'nav-menus.php' ) )
								),
								'move-menu_item'
							);
							if ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) {
								$url3 = admin_url( 'nav-menus.php' );
							} else {
								$url3 = add_query_arg(
									'edit-menu-item', $item_id, remove_query_arg(
										$removed_args,
										admin_url( 'nav-menus.php#menu-item-settings-' . $item_id )
									)
								);
							}
							?>
							<a href="<?php echo esc_url( $url1 ); ?>" class="item-move-up"><abbr title="<?php esc_attr_e( 'Move up', 'medicale-wp' ); ?>">&#8593;</abbr></a>
							|
							<a href="<?php echo esc_url( $url2 ); ?>" class="item-move-down"><abbr title="<?php esc_attr_e( 'Move down', 'medicale-wp' ); ?>">&#8595;</abbr></a>
						</span>
						<a class="item-edit" id="edit-<?php echo $item_id; ?>" title="<?php esc_attr_e( 'Edit Menu Item', 'medicale-wp' ); ?>" href="<?php echo esc_url( $url3 ); ?>"><?php esc_attr_e( 'Edit Menu Item', 'medicale-wp' ); ?></a>
					</span>
				</dt>
			</dl>
			<!-- .menu-item-bar-->
	

			<div class="menu-item-settings wp-clearfix" id="menu-item-settings-<?php echo $item_id; ?>">
				<?php if( 'custom' == $item->type ) : ?>
					<p class="field-url description description-wide">
						<label for="edit-menu-item-url-<?php echo $item_id; ?>">
							<?php esc_html_e( 'URL', 'medicale-wp' ); ?><br />
							<input type="text" id="edit-menu-item-url-<?php echo $item_id; ?>" class="widefat code edit-menu-item-url" name="menu-item-url[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->url ); ?>" />
						</label>
					</p>
				<?php endif; ?>
				<p class="description description-thin">
					<label for="edit-menu-item-title-<?php echo $item_id; ?>">
						<?php esc_html_e( 'Navigation Label', 'medicale-wp' ); ?><br />
						<input type="text" id="edit-menu-item-title-<?php echo $item_id; ?>" class="widefat edit-menu-item-title" name="menu-item-title[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->title ); ?>" />
					</label>
				</p>
				<p class="description description-thin">
					<label for="edit-menu-item-attr-title-<?php echo $item_id; ?>">
						<?php esc_html_e( 'Title Attribute', 'medicale-wp' ); ?><br />
						<input type="text" id="edit-menu-item-attr-title-<?php echo $item_id; ?>" class="widefat edit-menu-item-attr-title" name="menu-item-attr-title[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->post_excerpt ); ?>" />
					</label>
				</p>
				<p class="field-link-target description">
					<label for="edit-menu-item-target-<?php echo $item_id; ?>">
						<input type="checkbox" id="edit-menu-item-target-<?php echo $item_id; ?>" value="_blank" name="menu-item-target[<?php echo $item_id; ?>]"<?php checked( $item->target, '_blank' ); ?> />
						<?php esc_html_e( 'Open link in a new window/tab', 'medicale-wp' ); ?>
					</label>
				</p>
				<p class="field-css-classes description description-thin">
					<label for="edit-menu-item-classes-<?php echo $item_id; ?>">
						<?php esc_html_e( 'CSS Classes (optional)', 'medicale-wp' ); ?><br />
						<input type="text" id="edit-menu-item-classes-<?php echo $item_id; ?>" class="widefat code edit-menu-item-classes" name="menu-item-classes[<?php echo $item_id; ?>]" value="<?php echo esc_attr( implode(' ', $item->classes ) ); ?>" />
					</label>
				</p>
				<p class="field-xfn description description-thin">
					<label for="edit-menu-item-xfn-<?php echo $item_id; ?>">
						<?php esc_html_e( 'Link Relationship (XFN)', 'medicale-wp' ); ?><br />
						<input type="text" id="edit-menu-item-xfn-<?php echo $item_id; ?>" class="widefat code edit-menu-item-xfn" name="menu-item-xfn[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->xfn ); ?>" />
					</label>
				</p>
				<p class="field-description description description-wide">
					<label for="edit-menu-item-description-<?php echo $item_id; ?>">
						<?php esc_html_e( 'Description', 'medicale-wp' ); ?><br />
						<textarea id="edit-menu-item-description-<?php echo $item_id; ?>" class="widefat edit-menu-item-description" rows="3" cols="20" name="menu-item-description[<?php echo $item_id; ?>]"><?php echo esc_html( $item->description ); // textarea_escaped ?></textarea>
						<span class="description"><?php esc_html_e('The description will be displayed in the menu if the current theme supports it.', 'medicale-wp'); ?></span>
					</label>
				</p>
				<?php
					/* New fields insertion starts here */
				?>
				<p class="field-dropdownposition description description-wide">
					<label for="edit-menu-item-mascot-dropdownposition-<?php echo $item_id; ?>">
						<input type="checkbox" id="edit-menu-item-mascot-dropdownposition-<?php echo $item_id; ?>" class="widefat code edit-menu-item-mascot-dropdownposition" name="menu-item-mascot-dropdownposition[<?php echo $item_id; ?>]" value="left" <?php checked( $item->mascot_dropdownposition, 'left' ); ?> />
						<strong><?php esc_attr_e( 'Show dropdown items left (default right)', 'medicale-wp' ); ?></strong>
					</label>
				</p>
				<p class="field-custom description description-wide">
					<label for="edit-menu-item-mascot-subtitle-<?php echo $item_id; ?>">
					<?php esc_html_e( 'Subtitle', 'medicale-wp' ); ?>
					<br />
					<input type="text" id="edit-menu-item-mascot-subtitle-<?php echo $item_id; ?>" class="widefat code edit-menu-item-custom" name="menu-item-mascot-subtitle[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->mascot_subtitle ); ?>" />
					</label>
				</p>
				<p class="field-custom description description-wide">
					<label for="edit-menu-item-mascot-menuicon-<?php echo $item_id; ?>">
					<?php esc_html_e( 'Menu Icon (use full font awesome name. Example: fa fa-home)', 'medicale-wp' ); ?>
					<br />
					<input type="text" id="edit-menu-item-mascot-menuicon-<?php echo $item_id; ?>" class="widefat code edit-menu-item-custom" name="menu-item-mascot-menuicon[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->mascot_menuicon ); ?>" />
					<?php esc_html_e( 'Collect your own icon from here: <a target="_blank" href="http://fontawesome.io/icons/">FontAwesome</a>.', 'medicale-wp' ); ?>
					</label>
				</p>
				<?php 
					if( medicale_mascot_get_redux_option( 'header-menu-megamenu-enable-megamenu' ) ) {
						do_action( 'medicale_mascot_wp_nav_megamenu_item_custom_fields', $item_id, $item, $depth, $args );
					}
				?>
				<?php
					/* New fields insertion ends here */
				?>
				<fieldset class="field-move hide-if-no-js description description-wide">
					<span class="field-move-visual-label" aria-hidden="true">Move</span>
					<button type="button" class="button-link menus-move menus-move-up" data-dir="up" aria-label="Move up one">Up one</button>
					<button type="button" class="button-link menus-move menus-move-down" data-dir="down" aria-label="Move down one">Down one</button>
					<button type="button" class="button-link menus-move menus-move-left" data-dir="left" aria-label="Move out from under Home">Out from under Home</button>
					<button type="button" class="button-link menus-move menus-move-right" data-dir="right"></button>
					<button type="button" class="button-link menus-move menus-move-top" data-dir="top">To the top</button>
				</fieldset>
				<div class="menu-item-actions description-wide submitbox">
					<?php if( 'custom' != $item->type && $original_title !== false ) : ?>
					<p class="link-to-original"> <?php printf( esc_html__('Original: %s', 'medicale-wp'), '<a href="' . esc_attr( $item->url ) . '">' . esc_html( $original_title ) . '</a>' ); ?> </p>
					<?php endif; ?>
					<a class="item-delete submitdelete deletion" id="delete-<?php echo $item_id; ?>" href="<?php
						echo wp_nonce_url(
							add_query_arg(
								array(
									'action' => 'delete-menu-item',
									'menu-item' => $item_id,
								),
								remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
							),
							'delete-menu_item_' . $item_id
						); ?>">
					<?php esc_html_e('Remove', 'medicale-wp'); ?>
					</a> <span class="meta-sep"> | </span> <a class="item-cancel submitcancel" id="cancel-<?php echo $item_id; ?>" href="<?php echo esc_url( add_query_arg( array('edit-menu-item' => $item_id, 'cancel' => time()), remove_query_arg( $removed_args, admin_url( 'nav-menus.php' ) ) ) );
						?>#menu-item-settings-<?php echo $item_id; ?>">
					<?php esc_html_e('Cancel', 'medicale-wp'); ?>
					</a> 
				</div>
				<input class="menu-item-data-db-id" name="menu-item-db-id[<?php echo $item_id; ?>]" value="<?php echo $item_id; ?>" type="hidden">
				<input class="menu-item-data-object-id" name="menu-item-object-id[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->object_id ); ?>" type="hidden">
				<input class="menu-item-data-object" name="menu-item-object[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->object ); ?>" type="hidden">
				<input class="menu-item-data-parent-id" name="menu-item-parent-id[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->menu_item_parent ); ?>" type="hidden">
				<input class="menu-item-data-position" name="menu-item-position[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->menu_order ); ?>" type="hidden">
				<input class="menu-item-data-type" name="menu-item-type[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->type ); ?>" type="hidden">
			</div>
			<!-- .menu-item-settings-->

				<ul class="menu-item-transport"></ul>
				
		<?php
		$output .= ob_get_clean();
	}
}


