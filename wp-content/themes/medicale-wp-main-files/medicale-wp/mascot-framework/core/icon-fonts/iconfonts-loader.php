<?php

//load lib
require_once MASCOT_FRAMEWORK_DIR . '/core/icon-fonts/lib/interface-iconfonts.php';

//load iconfonts
require_once MASCOT_FRAMEWORK_DIR . '/core/icon-fonts/icons-fontawesome.php';
require_once MASCOT_FRAMEWORK_DIR . '/core/icon-fonts/icons-sevenstroke.php';
require_once MASCOT_FRAMEWORK_DIR . '/core/icon-fonts/icons-icomoon.php';
require_once MASCOT_FRAMEWORK_DIR . '/core/icon-fonts/icons-ionicons.php';
require_once MASCOT_FRAMEWORK_DIR . '/core/icon-fonts/icons-elegant.php';
require_once MASCOT_FRAMEWORK_DIR . '/core/icon-fonts/icons-medical-flaticons.php';

//reg iconfonts
require_once MASCOT_FRAMEWORK_DIR . '/core/icon-fonts/reg-icon-fonts.php';

//functions
require_once MASCOT_FRAMEWORK_DIR . '/core/icon-fonts/iconfonts-functions.php';