<?php


if(!function_exists('medicale_mascot_get_vc_icon_pack_array')) {
	/**
	 * Makes and return iconpack dropdown fields for visual composer with dependency
	 *
	 */
	function medicale_mascot_get_vc_icon_pack_array( $icon_pack_dependency = array(), $icon_pack_grounp = "" ) {

		global $medicale_mascot_icon_font_packs;
		$icon_pack_collection = $medicale_mascot_icon_font_packs->getIconFontPackKeys();

		$icon_pack_parent = array(
			'type'			=> 'dropdown',
			'heading'		=> esc_html__( 'Choose Icon Pack', 'medicale-wp' ),
			'param_name'	=> "icon_pack",
			'value'			=> array_merge(
				array( '' => '' ),
				$icon_pack_collection
			),
			'save_always'	=> true,
			'admin_label'   => true,
		);

		if( !empty( $icon_pack_dependency ) ) {
			$icon_pack_parent["dependency"] = $icon_pack_dependency;
		}

		if( $icon_pack_grounp != "" ) {
			$icon_pack_parent["group"] = $icon_pack_grounp;
		}

		$icon_pack_icons = array();
		$icon_pack_icons[] = $icon_pack_parent;
		if(is_array($icon_pack_collection) && count($icon_pack_collection)) {
			foreach($icon_pack_collection as $key => $pack_name) {
				$child_icon_pack = array(
					'type'			=> 'dropdown',
					'heading'		=> 'Icon',
					'param_name'	=> $pack_name,
					'value'			=> array_flip( $medicale_mascot_icon_font_packs->getIconFontPackByKey( $pack_name )->getIconList() ),
					'dependency'	=> array('element' => 'icon_pack', 'value' =>array( $pack_name )),
					'save_always'	=> true
				);
				
				if( $icon_pack_grounp != "" ) {
					$child_icon_pack["group"] = $icon_pack_grounp;
				}

				$icon_pack_icons[] = $child_icon_pack;
			}
		}

		return $icon_pack_icons;
	}
}



if(!function_exists('medicale_mascot_get_metabox_icon_pack_array')) {
	/**
	 * Makes and return iconpack dropdown fields for metabox with dependency
	 *
	 */
	function medicale_mascot_get_metabox_icon_pack_array( $icon_pack_field_id = "choose_iconpack", $icon_pack_visible = array(), $icon_pack_tab = "" ) {
		global $medicale_mascot_icon_font_packs;
		$icon_pack_key_name_array = $medicale_mascot_icon_font_packs->getIconFontPackNamesAarray();

		//icon pack parent array
		$icon_pack_parent = array(
			'id'	=> $icon_pack_field_id,
			'name'	=> esc_html__( 'Choose Icon Pack', 'medicale-wp' ),
			'type'	=> 'select',
			'options' => $icon_pack_key_name_array,
		);

		if( !empty( $icon_pack_visible ) ) {
			$icon_pack_parent["visible"] = $icon_pack_visible;
		}

		if( $icon_pack_tab != "" ) {
			$icon_pack_parent["tab"] = $icon_pack_tab;
		}

		$icon_pack_icons = array();
		$icon_pack_icons[] = $icon_pack_parent;

		if(is_array($icon_pack_key_name_array) && count($icon_pack_key_name_array)) {
			foreach($icon_pack_key_name_array as $key => $name) {
				$child_icon_pack = array(
					'id'		=> $icon_pack_field_id . '_' . $key,
					'name'		=> $name,
					'type'		=> 'select',
					'options'	=> $medicale_mascot_icon_font_packs->getIconFontPackByKey( $key )->getIconList(),
					'desc'		=> sprintf( esc_html__( 'See full list of icons from %1$shere%2$s', 'medicale-wp' ), '<a target="_blank" href="' . esc_url( 'http://docs.kodesolution.info/icons/' ) . '">', '</a>' ),
					'visible'	=> array( $icon_pack_field_id, '=', $key ),
				);

				if( $icon_pack_tab != "" ) {
					$child_icon_pack["tab"] = $icon_pack_tab;
				}
				$icon_pack_icons[] = $child_icon_pack;
			}
		}
		return $icon_pack_icons;
	}
}