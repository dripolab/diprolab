<?php
namespace MascotMedicaleNamespace\Framework\IconFonts\Lib;

/**
 * interface Medicale_Mascot_Interface_IconFonts
 * @package MascotMedicaleNamespace\Framework\IconFonts\Lib;
 */
interface Medicale_Mascot_Interface_IconFonts {

	/**
	 * Returns Icon List
	 */
	public function getIconList();

	/**
	 * Check if the font package has Social Icons
	 */
	public function hasSocialIcons();
}