<?php

use MascotMedicaleNamespace\Framework\IconFonts\Lib;
use MascotMedicaleNamespace\Framework\IconFonts\FontAwesome\Medicale_Mascot_Icons_Fontawesome;
use MascotMedicaleNamespace\Framework\IconFonts\SevenStroke\Medicale_Mascot_Icons_SevenStroke;
use MascotMedicaleNamespace\Framework\IconFonts\IcoMoon\Medicale_Mascot_Icons_IcoMoon;
use MascotMedicaleNamespace\Framework\IconFonts\IonIcons\Medicale_Mascot_Icons_IonIcons;
use MascotMedicaleNamespace\Framework\IconFonts\Elegant\Medicale_Mascot_Icons_Elegant;
use MascotMedicaleNamespace\Framework\IconFonts\MedicalFlatIcons\Medicale_Mascot_Icons_MedicalFlatIcons;

/**
 * class RegIconFonts
 */
class RegIconFonts {
	/**
	 * @var Singleton The reference to *Singleton* instance of this class
	 */
	private static $instance;

	/**
	 * @var array
	 */
	private $allIconFontPacks = array();
	
	/**
	 * Returns the *Singleton* instance of this class.
	 *
	 * @return Singleton The *Singleton* instance.
	 */
	public static function getInstance()
	{
		if (null === static::$instance) {
			static::$instance = new static();
		}
		
		return static::$instance;
	}

	/**
	 * Protected constructor to prevent creating a new instance of the
	 * *Singleton* via the `new` operator from outside of this class.
	 */
	protected function __construct()
	{
		$this->allIconFontPacksToReg();
	}

	/**
	 * Private clone method to prevent cloning of the instance of the
	 * *Singleton* instance.
	 *
	 * @return void
	 */
	private function __clone()
	{
	}

	/**
	 * Private unserialize method to prevent unserializing of the *Singleton*
	 * instance.
	 *
	 * @return void
	 */
	private function __wakeup()
	{
	}

	/**
	 * Add each Icon Font Pack to array
	 */
	public function addIconFontPack($key, $name, $value) {
		$this->allIconFontPacks[$key]['name'] = $name;
		$this->allIconFontPacks[$key]['value'] = $value;
	}

	/**
	 * List of all Icon Font Packs to register
	 */
	private function allIconFontPacksToReg() {
		$this->addIconFontPack('font_awesome', 'FontAwesome',  new Medicale_Mascot_Icons_Fontawesome());
		$this->addIconFontPack('medicale_flaticons', 'Flaticon Medical', new Medicale_Mascot_Icons_MedicalFlatIcons());
		$this->addIconFontPack('elegant', 'Elegant Icons', new Medicale_Mascot_Icons_Elegant());
		$this->addIconFontPack('icomoon', 'IcoMoon', new Medicale_Mascot_Icons_IcoMoon());
		$this->addIconFontPack('ionicons', 'Ion Icons', new Medicale_Mascot_Icons_IonIcons());
		$this->addIconFontPack('seven_stroke', '7 Stroke Icons', new Medicale_Mascot_Icons_SevenStroke());
	}

	/**
	 * Get Icon Font Pack by Key
	 */
	public function getIconFontPackByKey($key) {
		if(array_key_exists($key, $this->allIconFontPacks)) {
			return $this->allIconFontPacks[$key]['value'];
		}
		return false;
	}

	/**
	 * Get key of all Icon Font Packs 
	 */
	public function getIconFontPackKeys() {
		return array_keys($this->allIconFontPacks);
	}

	/**
	 * Get Icon Font Pack Names Array
	 */
	public function getIconFontPackNamesAarray() {
		$font_array = array();

		foreach($this->allIconFontPacks as $key => $icon_pack) {
			$font_array[$key] = $icon_pack['name'];
		}
		return $font_array;
	}
}

//create global variable $medicale_mascot_icon_font_packs
if ( ! function_exists( 'medicale_mascot_activate_theme_icon_packs' ) ) {
	function medicale_mascot_activate_theme_icon_packs() {
		global $medicale_mascot_icon_font_packs;
		$medicale_mascot_icon_font_packs = RegIconFonts::getInstance();
	}
	add_action('after_setup_theme', 'medicale_mascot_activate_theme_icon_packs');
}

if( !function_exists( 'medicale_mascot_icon_font_packs' ) ) {
	/**
	 * Returns instance of RegIconFonts class
	 */
	function medicale_mascot_icon_font_packs() {
		return RegIconFonts::getInstance();
	}
}