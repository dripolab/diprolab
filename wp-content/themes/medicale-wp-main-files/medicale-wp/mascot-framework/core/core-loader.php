<?php

//extend visual composer
if ( class_exists( 'Vc_Manager' ) ) {
	function medicale_mascot_extend_visual_composer() {
		require_once( MASCOT_FRAMEWORK_DIR . '/core/wpbakery/vc-init.php' );
	}
	add_action( 'init', 'medicale_mascot_extend_visual_composer', 20 );
}

//extend visual composer
if ( ! function_exists( 'medicale_mascot_extend_visual_composer_icons' ) ) {
	function medicale_mascot_extend_visual_composer_icons() {
		//iconfonts loader
		require_once( MASCOT_FRAMEWORK_DIR . '/core/icon-fonts/iconfonts-loader.php' );
	}
	add_action( 'after_setup_theme', 'medicale_mascot_extend_visual_composer_icons', 0 );
}

//CPT loader
require_once( MASCOT_FRAMEWORK_DIR . '/core/custom-post-types/cpt-loader.php' );

//shortcode loader
require_once( MASCOT_FRAMEWORK_DIR . '/core/shortcodes/shortcodes-loader.php' );

//widgets loader
require_once( MASCOT_FRAMEWORK_DIR . '/core/widgets/widgets-loader.php' );

//template parts loader
require_once( MASCOT_FRAMEWORK_DIR . '/core/blocks/blocks-loader.php' );

//woocommerce loader
require_once( MASCOT_FRAMEWORK_DIR . '/core/woocommerce/loader.php' );