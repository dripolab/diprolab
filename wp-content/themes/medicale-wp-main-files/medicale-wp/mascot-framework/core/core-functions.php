<?php	
use MASCOTCORE\CPT\Portfolio\CPT_Portfolio;
/*
*
*	Core Functions
*	---------------------------------------
*	Mascot Framework v1.0
* 	Copyright ThemeMascot 2017 - http://www.thememascot.com
*
*/

// Null Funcion
function medicale_mascot_null_function() {}

if(!function_exists('rwmb_meta')) {
	/**
	 * For fallback when metabox is not defined
	 */
	function rwmb_meta() {
		return false;
	}
}

if(!function_exists('medicale_mascot_get_blocks_template_part')) {
	/**
	 * Load a blocks template part into a template
	 *
	 * @param string $slug The slug name for the generic template.
	 * @param string $name The name of the specialised template.
	 * @param string $folder The name of the specialised folder.
	 * @param array $params array of parameters to pass to the template.
	 */
	function medicale_mascot_get_blocks_template_part( $slug, $name = null, $folder, $params = array() ) {

		$template_path = MASCOT_FRAMEWORK_FOLDER . '/core/blocks/' . $folder . '/' . $slug;

		return medicale_mascot_get_template_part( $template_path, $name, $params );

	}
}

if(!function_exists('medicale_mascot_get_shortcode_template_part')) {
	/**
	 * Load a shortcode template part into a template
	 *
	 * @param string $slug The slug name for the generic template.
	 * @param string $name The name of the specialised template.
	 * @param string $folder The name of the specialised folder.
	 * @param array $params array of parameters to pass to the template.
	 * @param boolean $shortcode_ob_start only for shortcodes to get HTML string.
	 */
	function medicale_mascot_get_shortcode_template_part( $slug, $name = null, $folder, $params = array(), $shortcode_ob_start ) {

		$template_path = MASCOT_FRAMEWORK_FOLDER . '/core/shortcodes/parts/' . $folder . '/' . $slug;

		return medicale_mascot_get_template_part( $template_path, $name, $params, $shortcode_ob_start );

	}
}

if(!function_exists('medicale_mascot_get_cpt_template_part')) {
	/**
	 * Load a cpt template part into a template
	 *
	 * @param string $slug The slug name for the generic template.
	 * @param string $name The name of the specialised template.
	 * @param string $folder The name of the specialised folder.
	 * @param array $params array of parameters to pass to the template.
	 * @param boolean $shortcode_ob_start only for shortcodes to get HTML string.
	 */
	function medicale_mascot_get_cpt_template_part( $slug, $name = null, $folder, $params = array(), $shortcode_ob_start ) {

		$template_path = MASCOT_FRAMEWORK_FOLDER . '/core/custom-post-types/' . $folder . '/' . $slug;

		return medicale_mascot_get_template_part( $template_path, $name, $params, $shortcode_ob_start );

	}
}

if(!function_exists('medicale_mascot_get_widget_template_part')) {
	/**
	 * Load a widget template part into a template
	 *
	 * @param string $slug The slug name for the generic template.
	 * @param string $name The name of the specialised template.
	 * @param string $folder The name of the specialised folder.
	 * @param array $params array of parameters to pass to the template.
	 * @param boolean $widget_ob_start only for widget to get HTML string.
	 */
	function medicale_mascot_get_widget_template_part( $slug, $name = null, $folder, $params = array(), $widget_ob_start ) {

		$template_path = MASCOT_FRAMEWORK_FOLDER . '/core/widgets/parts/' . $folder . '/' . $slug;

		return medicale_mascot_get_template_part( $template_path, $name, $params, $widget_ob_start );

	}
}


if(!function_exists('medicale_mascot_get_woocommerce_template_part')) {
	/**
	 * Load a woocommerce template part into a template
	 *
	 * @param string $slug The slug name for the generic template.
	 * @param string $name The name of the specialised template.
	 * @param string $folder The name of the specialised folder.
	 * @param array $params array of parameters to pass to the template.
	 */
	function medicale_mascot_get_woocommerce_template_part( $slug, $name = null, $folder, $params = array() ) {

		$template_path = MASCOT_FRAMEWORK_FOLDER . '/core/woocommerce/' . $folder . '/' . $slug;

		return medicale_mascot_get_template_part( $template_path, $name, $params );

	}
}



if(!function_exists('medicale_mascot_get_template_part')) {
	/**
	 * Load a template part into a template
	 *
	 * @param string $template_path path of the specialised template.
	 * @param string $name The name of the specialised template.
	 * @param array $params array of parameters to pass to the template.
	 * @param boolean $shortcode_ob_start only for shortcodes to get HTML string.
	 */
	function medicale_mascot_get_template_part( $template_path, $name = null, $params = array(), $shortcode_ob_start = false ) {

		$output_html = '';

		if( is_array($params) && count($params) ) {
			extract($params);
		}

		$templates = array();
		$name = (string) $name;
		if ( '' !== $name )
			$templates[] = "{$template_path}-{$name}.php";

		$templates[] = "{$template_path}.php";

		$located = medicale_mascot_locate_template($templates);

		if($located) {
			if( $shortcode_ob_start ) {
				ob_start();
				include($located);
				$output_html = ob_get_clean();
			} else {
				include($located);
			}
		}

		return $output_html;
	}
}

if(!function_exists('medicale_mascot_locate_template')) {
	/**
	 * Retrieve the name of the highest priority template file that exists.
	 *
	 * Searches in the MASCOT_STYLESHEET_DIR before MASCOT_TEMPLATE_DIR
	 * so that themes which inherit from a parent theme can just overload one file.
	 *
	 * @param string|array $template_names Template file(s) to search for, in order.
	 * @return string The template filename if one is located.
	 */
	function medicale_mascot_locate_template($template_names) {
		$located = '';
		foreach ( (array) $template_names as $template_name ) {
			if ( !$template_name ) {
				continue;
			}
			if ( file_exists(MASCOT_STYLESHEET_DIR . '/' . $template_name)) {
				$located = MASCOT_STYLESHEET_DIR . '/' . $template_name;
				break;
			} elseif ( file_exists(MASCOT_TEMPLATE_DIR . '/' . $template_name) ) {
				$located = MASCOT_TEMPLATE_DIR . '/' . $template_name;
				break;
			}
		}
		return $located;
	}
}


if(!function_exists('medicale_mascot_dynamic_css_generator')) {
	/**
	 * Dynamic CSS generator based on selectors & declarations
	 *
	 * @param array,string $selector The selector points to the HTML element you want to style
	 * @param array $declaration The declaration block contains one or more declarations separated by semicolons.
	 *
	 * @return string
	 */
	function medicale_mascot_dynamic_css_generator($selector, $declaration) {

		$generated_css = '';

		if( !empty( $selector ) && ( is_array( $declaration ) && count( $declaration ) ) ) {

			if( is_array( $selector ) && count( $selector ) ) {
				$generated_css .= implode(', ', $selector);
			} else {
				$generated_css .= $selector;
			}

			$generated_css .= ' {';
			foreach( $declaration as $property => $value ) {
				if( $property !== '' ) {
					$generated_css .= $property.': '.esc_attr($value).';';
				}
			}

			$generated_css .= '}';
		}

		return $generated_css;
	}
}

if(!function_exists('medicale_mascot_redux_option_field_typography')) {
	/**
	 * Redux Option Field Typography
	 * @return bool
	 */
	function medicale_mascot_redux_option_field_typography( $var_name = '' ) {
		global $medicale_mascot_redux_theme_opt;
		$declaration = array();

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		$redux_opt = $medicale_mascot_redux_theme_opt[$var_name];

		if( $var_name != '' && $redux_opt !== '' ) {
			if( isset( $redux_opt['font-family'] ) && $redux_opt['font-family'] != "" ) {
				$declaration['font-family'] = $redux_opt['font-family'];
			}
			if( isset( $redux_opt['font-weight'] ) && $redux_opt['font-weight'] != "" ) {
				$declaration['font-weight'] = $redux_opt['font-weight'];
			}
			if( isset( $redux_opt['font-style'] ) && $redux_opt['font-style'] != "" ) {
				$declaration['font-style'] = $redux_opt['font-style'];
			}

			if( isset( $redux_opt['text-align'] ) && $redux_opt['text-align'] != "" ) {
				$declaration['text-align'] = $redux_opt['text-align'];
			}
			if( isset( $redux_opt['text-transform'] ) && $redux_opt['text-transform'] != "" ) {
				$declaration['text-transform'] = $redux_opt['text-transform'];
			}
			if( isset( $redux_opt['font-size'] ) && $redux_opt['font-size'] != "" ) {
				$declaration['font-size'] = $redux_opt['font-size'];
			}

			if( isset( $redux_opt['line-height'] ) && $redux_opt['line-height'] != "" ) {
				$declaration['line-height'] = $redux_opt['line-height'];
			}
			if( isset( $redux_opt['word-spacing'] ) && $redux_opt['word-spacing'] != "" ) {
				$declaration['word-spacing'] = $redux_opt['word-spacing'];
			}
			if( isset( $redux_opt['letter-spacing'] ) && $redux_opt['letter-spacing'] != "" ) {
				$declaration['letter-spacing'] = $redux_opt['letter-spacing'];
			}

			if( isset( $redux_opt['color'] ) && $redux_opt['color'] != "" ) {
				$declaration['color'] = $redux_opt['color'];
			}
		}

		return $declaration;
	}
}

if(!function_exists('medicale_mascot_redux_option_field_background')) {
	/**
	 * Redux Option Field Background
	 * @return bool
	 */
	function medicale_mascot_redux_option_field_background( $var_name = '' ) {
		global $medicale_mascot_redux_theme_opt;
		$declaration = array();

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}
		
		$redux_opt = $medicale_mascot_redux_theme_opt[$var_name];

		if( $var_name != '' && $redux_opt !== '' ) {
			// Background color
			if( isset( $redux_opt['background-color'] ) && $redux_opt['background-color'] != "" ) {
				$declaration['background-color'] = $redux_opt['background-color'];
			}

			// Background image options
			if( isset( $redux_opt['background-repeat'] ) &&  $redux_opt['background-repeat'] != "" ) {
				$declaration['background-repeat'] = $redux_opt['background-repeat'];
			}
			if( isset( $redux_opt['background-size'] ) &&  $redux_opt['background-size'] != "" ) {
				$declaration['background-size'] = $redux_opt['background-size'];
			}
			if( isset( $redux_opt['background-attachment'] ) &&  $redux_opt['background-attachment'] != "" ) {
				$declaration['background-attachment'] = $redux_opt['background-attachment'];
			}
			if( isset( $redux_opt['background-position'] ) &&  $redux_opt['background-position'] != "" ) {
				$declaration['background-position'] = $redux_opt['background-position'];
			}

			// Background image
			if( isset( $redux_opt['background-image'] ) &&  $redux_opt['background-image'] != "" ) {
				$declaration['background-image'] = 'url('.$redux_opt['background-image'].')';
			}
		}

		return $declaration;
	}
}

if(!function_exists('medicale_mascot_is_woocommerce_installed')) {
	/**
	 * Function that checks if woocommerce is installed
	 * @return bool
	 */
	function medicale_mascot_is_woocommerce_installed() {
		return function_exists('is_woocommerce');
	}
}

if(!function_exists('medicale_mascot_visual_composer_installed')) {
	/**
	 * Function that checks if visual composer installed
	 * @return bool
	 */
	function medicale_mascot_visual_composer_installed() {
		//is Visual Composer installed?
		if(class_exists('WPBakeryVisualComposerAbstract')) {
			return true;
		}

		return false;
	}
}

if(!function_exists('medicale_mascot_seo_plugin_installed')) {
	/**
	 * Function that checks if popular seo plugins are installed
	 * @return bool
	 */
	function medicale_mascot_seo_plugin_installed() {
		//is 'YOAST' or 'All in One SEO' installed?
		if(defined('WPSEO_VERSION') || class_exists('All_in_One_SEO_Pack')) {
			return true;
		}

		return false;
	}
}

if(!function_exists('medicale_mascot_contact_form_7_installed')) {
	/**
	 * Function that checks if contact form 7 installed
	 * @return bool
	 */
	function medicale_mascot_contact_form_7_installed() {
		//is Contact Form 7 installed?
		if(defined('WPCF7_VERSION')) {
			return true;
		}

		return false;
	}
}

if(!function_exists('medicale_mascot_is_wpml_installed')) {
	/**
	 * Function that checks if WPML plugin is installed
	 * @return bool
	 *
	 * @version 0.1
	 */
	function medicale_mascot_is_wpml_installed() {
		return defined('ICL_SITEPRESS_VERSION');
	}
}


if(!function_exists('medicale_mascot_return_false')) {
	/**
	 * return true false by add_filter and apply_filters
	 */
	function medicale_mascot_return_false( ) {
		return false;
	}

}

if(!function_exists('medicale_mascot_return_true')) {
	/**
	 * return true
	 */
	function medicale_mascot_return_true( ) {
		return true;
	}
}


if(!function_exists('medicale_mascot_get_url_params')) {
	/**
	 * retrieve values of parameters passing through the URL
	 */
	function medicale_mascot_get_url_params( $param ) {
		return isset( $_GET[ $param ] ) ? $_GET[ $param ] : ( isset( $_REQUEST[ $param ] ) ? $_REQUEST[ $param ] : '' );
	}
}


if(!function_exists('medicale_mascot_get_url_params')) {
	/**
	 * retrieve POST data
	 */
	function medicale_mascot_get_post_params( $param ) {
		return isset( $_POST[ $param ] ) ? $_POST[ $param ] : ( isset( $_REQUEST[ $param ] ) ? $_REQUEST[ $param ] : '' );
	}
}

if(!function_exists('medicale_mascot_get_page_id')) {
	/**
	 * retrieve page ID
	 */
	function medicale_mascot_get_page_id() {
		if( class_exists( 'WooCommerce' ) && (is_shop() || is_product()) ) {
			return get_option( 'woocommerce_shop_page_id' );
		}

		if( (is_home() && is_front_page()) || is_archive() || is_search() || is_404() ) {
			return -1;
		}

		return get_queried_object_id();
	}
}

if(!function_exists('medicale_mascot_show_comments')) {
	/**
	 * Return Comments HTML
	 *
	 */
	function medicale_mascot_show_comments() {
		if (! is_attachment() ) {
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
		}
	}
}

if(!function_exists('medicale_mascot_category_list_array_for_vc')) {
	/**
	 * Return category list array for VC
	 */
	function medicale_mascot_category_list_array_for_vc( $taxonomy ) {
		$list_categories = array(
			esc_html__( 'All', 'medicale-wp' ) => ''
		);
		$terms = get_terms( $taxonomy );

		if ( $terms && !is_wp_error( $terms ) ) :
			foreach ( $terms as $term ) {
				$list_categories[ $term->name ] = $term->slug;
			}
		endif;

		return $list_categories;
	}
}

if(!function_exists('medicale_mascot_category_list_array')) {
	/**
	 * Return category list array
	 */
	function medicale_mascot_category_list_array( $taxonomy ) {
		$list_categories = array(
			'' => esc_html__( 'All', 'medicale-wp' )
		);
		$terms = get_terms( $taxonomy );

		if ( $terms && !is_wp_error( $terms ) ) :
			foreach ( $terms as $term ) {
				$list_categories[ $term->slug ] = $term->name;
			}
		endif;

		return $list_categories;
	}
}


if(!function_exists('medicale_mascot_output_array_list')) {
	/**
	 * Output list array
	 * to show use: medicale_mascot_output_array_list( $vc_map['params'] );
	 */
	function medicale_mascot_output_array_list( $list_array ) {
		//Outputs $args to make it easy
		foreach ( $list_array as $eachparam ) {
			echo "'".$eachparam['param_name']."' => '', <br>";
		}
		
	}
}



if(!function_exists('medicale_mascot_get_image_dimensions')) {
	/**
	 * Output Image Dimensions
	 */
	function medicale_mascot_get_image_dimensions( $orientation = "landscape", $ratio = "16:9", $width = 450, $height = 0 ) {
		switch ( $ratio ) {
			case "16:9":
				if ( $orientation == "landscape" )
				$height = (int) (( $width / 16 ) * 9);
				else
				$height = (int) (( $width / 9 ) * 16);
			break;

			case "4:3":
				if ( $orientation == "landscape" )
				$height = (int) (( $width / 4 ) * 3);
				else
				$height = (int) (( $width / 3 ) * 4);
			break;

			case "3:2":
				if ( $orientation == "landscape" )
				$height = (int) (( $width / 3 ) * 2);
				else
				$height = (int) (( $width / 2 ) * 3);
			break;

			case "1:1";
				$height = (int) ( $width );
			break;
		}

		return array ( 'width' => $width, 'height' => $height );
	}
}

if(!function_exists('medicale_mascot_startsWith')) {
	/**
	 * Functions that would take a string and return true if it starts with the specified character/string
	 */
	function medicale_mascot_startsWith($haystack, $needle)
	{
		$length = strlen($needle);
		return (substr($haystack, 0, $length) === $needle);
	}
}

if(!function_exists('medicale_mascot_endsWith')) {
	/**
	 * Functions that would take a string and return true if it ends with the specified character/string
	 */
	function medicale_mascot_endsWith($haystack, $needle)
	{
		$length = strlen($needle);
		if ($length == 0) {
			return true;
		}

		return (substr($haystack, -$length) === $needle);
	}
}

if(!function_exists('medicale_mascot_remove_suffix')) {
	/**
	 * Remove Suffix from String
	 */
	function medicale_mascot_remove_suffix( $string, $suffix )
	{
		if( $string != '' && medicale_mascot_endsWith($string, $suffix) ) {
			$string = substr($string, 0 , (strpos($string, $suffix)));
		}
		return $string;
	}
}


if(!function_exists('medicale_mascot_get_inline_attributes')) {
	/**
	 * Get inline attributes and it's properties
	 */
	function medicale_mascot_get_inline_attributes( $values, $attribute, $glue = '' ) {
		if( $values != '' ) {
			if( is_array( $values ) && count( $values ) ) {
				$properties = implode( $glue, $values );
			} elseif( $values !== '' ) {
				$properties = $values;
			}

			return $attribute . '="' . esc_attr($properties) . '"';
		}
		return '';
	}
}


if(!function_exists('medicale_mascot_get_inline_css')) {
	/**
	 * Get inline CSS
	 */
	function medicale_mascot_get_inline_css( $values ) {
		return medicale_mascot_get_inline_attributes( $values, 'style', $glue = ';' );
	}
}


if(!function_exists('medicale_mascot_get_inline_classes')) {
	/**
	 * Get inline classes
	 */
	function medicale_mascot_get_inline_classes( $values ) {
		return medicale_mascot_get_inline_attributes( $values, 'class', $glue = ' ' );
	}
}


if(!function_exists('medicale_mascot_metabox_get_image_advanced_field_url')) {
	/**
	 * Get Full URL of image_advanced metabox field
	 */
	function medicale_mascot_metabox_get_image_advanced_field_url( $image_field_array ) {
		$first_key = key( $image_field_array );
		return $image_field_array[$first_key]['full_url'];
	}
}


if(!function_exists('medicale_mascot_metabox_get_file_advanced_field_url')) {
	/**
	 * Get Full URL of file_advanced metabox field
	 */
	function medicale_mascot_metabox_get_file_advanced_field_url( $image_field_array ) {
		$first_key = key( $image_field_array );
		return $image_field_array[$first_key]['url'];
	}
}



if(!function_exists('medicale_mascot_get_sidebar')) {
	/**
	 * Get Sidebar
	 */
	function medicale_mascot_get_sidebar( $sidebar_position ) {
		$sidebar_id = 'default-sidebar';

		//Choose Sidebar for different page type
		if ( is_front_page() && is_home() ) {
			// Default homepage
			$sidebar_id = medicale_mascot_blog_archive_get_sidebar( $sidebar_position );
		} elseif ( is_front_page() ) {
			// static homepage
			$sidebar_id = medicale_mascot_page_get_sidebar( $sidebar_position );
		} elseif ( is_home() ) {
			// blog page
			$sidebar_id = medicale_mascot_blog_archive_get_sidebar( $sidebar_position );

		} else if ( is_single() ) {
			// single page
			if ( is_singular( 'post' ) ) {
				$sidebar_id = medicale_mascot_blog_single_get_sidebar( $sidebar_position );
			} else if ( is_singular( 'portfolio' ) ) {
				$sidebar_id = medicale_mascot_portfolio_single_get_sidebar( $sidebar_position );
			}

		} else if ( is_search() || is_archive() ) {
			//if custom post type archive
			if ( is_post_type_archive( 'portfolio' ) ) {
				$sidebar_id = medicale_mascot_portfolio_get_sidebar( $sidebar_position );
			} else {
				// search or archive page
				$sidebar_id = medicale_mascot_blog_archive_get_sidebar( $sidebar_position );
			}

		} else if ( medicale_mascot_is_woocommerce_installed() && is_woocommerce() ) {
			//woocommerce page
		} else {
			//everyting else
			$sidebar_id = medicale_mascot_page_get_sidebar( $sidebar_position );
		}

		return $sidebar_id;
	}
}


if(!function_exists('medicale_mascot_page_get_sidebar')) {
	/**
	 * Get Sidebar for page
	 */
	function medicale_mascot_page_get_sidebar( $sidebar_position ) {
		$current_page_id = medicale_mascot_get_page_id();
		$sidebar_id = 'default-sidebar';


		//Page Sidebar Layout
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_sidebar_layout", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$page_layout = $temp_meta_value;
		} else {
			$page_layout = medicale_mascot_get_redux_option( 'page-settings-sidebar-layout' );
		}


		//If both sidebar then
		if( $page_layout == 'both-sidebar-25-50-25' ) {
			
			//Sidebar 2 Position
			//check if meta value is provided for this page or then get it from theme options
			$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_sidebar_two_position", '', $current_page_id );
			if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
				$sidebar_two_position = $temp_meta_value;
			} else {
				$sidebar_two_position = medicale_mascot_get_redux_option( 'page-settings-sidebar-layout-sidebar-two-position' );
			}

			if( $sidebar_two_position == $sidebar_position ) {
				//Sidebar Two
				//check if meta value is provided for this page or then get it from theme options
				$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_sidebar_two", '', $current_page_id );
				if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
					$sidebar_id = $temp_meta_value;
				} else {
					$sidebar_id = medicale_mascot_get_redux_option( 'page-settings-sidebar-layout-sidebar-two' );
				}
			} else {
				//Sidebar Default
				//check if meta value is provided for this page or then get it from theme options
				$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_sidebar_default", '', $current_page_id );
				if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
					$sidebar_id = $temp_meta_value;
				} else {
					$sidebar_id = medicale_mascot_get_redux_option( 'page-settings-sidebar-layout-sidebar-default' );
				}

			}

		} else {

			//Sidebar Default
			//check if meta value is provided for this page or then get it from theme options
			$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_sidebar_default", '', $current_page_id );
			if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
				$sidebar_id = $temp_meta_value;
			} else {
				$sidebar_id = medicale_mascot_get_redux_option( 'page-settings-sidebar-layout-sidebar-default' );
			}

		}

		return $sidebar_id;
	}
}


if(!function_exists('medicale_mascot_blog_archive_get_sidebar')) {
	/**
	 * Get Sidebar for Blog Archive
	 */
	function medicale_mascot_blog_archive_get_sidebar( $sidebar_position ) {
		$current_page_id = medicale_mascot_get_page_id();
		$sidebar_id = 'default-sidebar';

		$page_layout = medicale_mascot_get_redux_option( 'blog-settings-sidebar-layout' );

		//If both sidebar then
		if( $page_layout == 'both-sidebar-25-50-25' ) {
			//Sidebar 2 Position
			$sidebar_two_position = medicale_mascot_get_redux_option( 'blog-settings-sidebar-layout-sidebar-two-position' );
			if( $sidebar_two_position == $sidebar_position ) {
				//Sidebar Two
				$sidebar_id = medicale_mascot_get_redux_option( 'blog-settings-sidebar-layout-sidebar-two' );
			} else {
				//Sidebar Default
				$sidebar_id = medicale_mascot_get_redux_option( 'blog-settings-sidebar-layout-sidebar-default' );
			}
		} else {
			//Sidebar Default
			$sidebar_id = medicale_mascot_get_redux_option( 'blog-settings-sidebar-layout-sidebar-default', 'default-sidebar' );
		}

		return $sidebar_id;
	}
}


if(!function_exists('medicale_mascot_blog_single_get_sidebar')) {
	/**
	 * Get Sidebar for Blog Single
	 */
	function medicale_mascot_blog_single_get_sidebar( $sidebar_position ) {
		$current_page_id = medicale_mascot_get_page_id();
		$sidebar_id = 'default-sidebar';

		$page_layout = medicale_mascot_get_redux_option( 'blog-single-post-settings-sidebar-layout' );

		//If both sidebar then
		if( $page_layout == 'both-sidebar-25-50-25' ) {
			//Sidebar 2 Position
			$sidebar_two_position = medicale_mascot_get_redux_option( 'blog-single-post-settings-sidebar-layout-sidebar-two-position' );
			if( $sidebar_two_position == $sidebar_position ) {
				//Sidebar Two
				$sidebar_id = medicale_mascot_get_redux_option( 'blog-single-post-settings-sidebar-layout-sidebar-two' );
			} else {
				//Sidebar Default
				$sidebar_id = medicale_mascot_get_redux_option( 'blog-single-post-settings-sidebar-layout-sidebar-default' );
			}
		} else {
			//Sidebar Default
			$sidebar_id = medicale_mascot_get_redux_option( 'blog-single-post-settings-sidebar-layout-sidebar-default', 'default-sidebar' );
		}

		return $sidebar_id;
	}
}


if(!function_exists('medicale_mascot_portfolio_get_sidebar')) {
	/**
	 * Get Sidebar for portfolio
	 */
	function medicale_mascot_portfolio_get_sidebar( $sidebar_position ) {
		$current_page_id = medicale_mascot_get_page_id();
		$sidebar_id = 'default-sidebar';

		$page_layout = medicale_mascot_get_redux_option( 'portfolio-settings-sidebar-layout' );

		//If both sidebar then
		if( $page_layout == 'both-sidebar-25-50-25' ) {
			//Sidebar 2 Position
			$sidebar_two_position = medicale_mascot_get_redux_option( 'portfolio-settings-sidebar-layout-sidebar-two-position' );
			if( $sidebar_two_position == $sidebar_position ) {
				//Sidebar Two
				$sidebar_id = medicale_mascot_get_redux_option( 'portfolio-settings-sidebar-layout-sidebar-two' );
			} else {
				//Sidebar Default
				$sidebar_id = medicale_mascot_get_redux_option( 'portfolio-settings-sidebar-layout-sidebar-default' );
			}
		} else {
			//Sidebar Default
			$sidebar_id = medicale_mascot_get_redux_option( 'portfolio-settings-sidebar-layout-sidebar-default' );
		}

		return $sidebar_id;
	}
}


if(!function_exists('medicale_mascot_portfolio_single_get_sidebar')) {
	/**
	 * Get Sidebar for Portfolio Single
	 */
	function medicale_mascot_portfolio_single_get_sidebar( $sidebar_position ) {
		$current_page_id = medicale_mascot_get_page_id();
		$sidebar_id = 'default-sidebar';

		$page_layout = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-sidebar-layout' );

		//If both sidebar then
		if( $page_layout == 'both-sidebar-25-50-25' ) {
			//Sidebar 2 Position
			$sidebar_two_position = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-sidebar-layout-sidebar-two-position' );
			if( $sidebar_two_position == $sidebar_position ) {
				//Sidebar Two
				$sidebar_id = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-sidebar-layout-sidebar-two' );
			} else {
				//Sidebar Default
				$sidebar_id = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-sidebar-layout-sidebar-default' );
			}
		} else {
			//Sidebar Default
			$sidebar_id = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-sidebar-layout-sidebar-default' );
		}

		return $sidebar_id;
	}
}

if(!function_exists('medicale_mascot_metabox_opt_val_is_empty')) {
	/**
	 * Check if metabox field option value is empty
	 */
	function medicale_mascot_metabox_opt_val_is_empty( $option_value ) {
		if ( ( is_array($option_value) && empty($option_value) ) || ( !is_array($option_value) && $option_value == '' ) ) {
			return true;
		} else {
			return false;
		}
	}
}


if(!function_exists('medicale_mascot_variable_val_is_empty')) {
	/**
	 * Check if variable value is empty
	 */
	function medicale_mascot_variable_val_is_empty( $variable ) {
		if ( ( is_array($variable) && empty($variable) ) || ( !is_array($variable) && $variable == '' ) ) {
			return true;
		} else {
			return false;
		}
	}
}

if(!function_exists('medicale_mascot_is_css_folder_writable')) {
	/**
	 * Checks if css folder writable
	 */
	function medicale_mascot_is_css_folder_writable() {
		$css_dir = MASCOT_ASSETS_DIR . '/css';
		return is_writable( $css_dir );
	}
}

if(!function_exists('medicale_mascot_is_css_colors_folder_writable')) {
	/**
	 * Checks if css colors folder writable
	 */
	function medicale_mascot_is_css_colors_folder_writable() {
		$css_dir = MASCOT_ASSETS_DIR . '/css/colors';
		return is_writable( $css_dir );
	}
}


if(!function_exists('medicale_mascot_generate_dynamic_css')) {
	/**
	 * Gets content of dynamic assets files and puts that in static file
	 */
	function medicale_mascot_generate_dynamic_css() {
		global $wp_filesystem;
		WP_Filesystem();

		if ( medicale_mascot_is_css_folder_writable() ) {
			$css_dir = MASCOT_ASSETS_DIR . '/css/';
			ob_start();
			include_once $css_dir . 'dynamic-style.php';
			$css = ob_get_clean();
			if ( is_multisite() ) {
				$wp_filesystem->put_contents( $css_dir . 'dynamic-style-msid-' . medicale_mascot_get_multisite_blog_id() . '.css', $css );
			} else {
				$wp_filesystem->put_contents( $css_dir . 'dynamic-style.css', $css );
			}
		}
	}
}


if(!function_exists('medicale_mascot_generate_css_for_custom_theme_color_from_less')) {
	/**
	 * Generates css custom theme color from Less dynamically when a user presses the "Save Settings" button at Redux Framework theme options
	 */
	function medicale_mascot_generate_css_for_custom_theme_color_from_less() {
		if ( medicale_mascot_core_plugin_installed() && class_exists( 'Less_Parser' ) ) {
			global $wp_filesystem;
			WP_Filesystem();
			
			if ( medicale_mascot_is_css_colors_folder_writable() ) {
				$less_dir = MASCOT_ASSETS_DIR . '/less/';
				$css_colors_dir = MASCOT_ASSETS_DIR . '/css/colors/';
				$options = array( 'compress'=>true );
				$parser = new Less_Parser( $options );
				$parser->parse( '@theme-color: ' . medicale_mascot_get_redux_option( 'theme-color-settings-custom-theme-color' ) . ';' );
				$parser->parseFile( $less_dir . 'less-theme-color/custom-theme-color.less' );
				$css = $parser->getCss();

				if ( is_multisite() ) {
					$wp_filesystem->put_contents( $css_colors_dir . 'custom-theme-color-msid-' . medicale_mascot_get_multisite_blog_id() . '.css', $css);
				} else {
					$wp_filesystem->put_contents( $css_colors_dir . 'custom-theme-color.css', $css);
				}
			}
		}
	}
}


if (!function_exists('medicale_mascot_get_multisite_blog_id')) {
	/**
	 * Check is multisite and return blog id
	 *
	 * @return int
	 */
	function medicale_mascot_get_multisite_blog_id() {
		if(is_multisite()){
			return get_blog_details()->blog_id;
		}
	}
}


if(!function_exists('medicale_mascot_posts_per_page_for_different_post_types')) {
	/**
	 * WordPress Posts Per Page for Different Custom Post Type
	 */
	function medicale_mascot_posts_per_page_for_different_post_types( $query ) {
		if ( !is_admin() && $query->is_main_query() ) {
					
			if( class_exists('MASCOTCORE\CPT\Portfolio\CPT_Portfolio') ) {
				//Post Type: Portfolio
				$portfolio_cpt_class = CPT_Portfolio::Instance();
				if( is_post_type_archive( $portfolio_cpt_class->ptKey ) || is_tax( $portfolio_cpt_class->ptTaxKey ) || is_tax( $portfolio_cpt_class->ptTagTaxKey ) ) {
					$items_per_page = medicale_mascot_get_redux_option( 'portfolio-layout-settings-items-per-page' );
					$query->set( 'posts_per_page', $items_per_page );
				}
			}
			
		}
	}
	add_action( 'pre_get_posts', 'medicale_mascot_posts_per_page_for_different_post_types' );
}


if(!function_exists('medicale_mascot_get_redux_option')) {
	/**
	 * Retuns Redux Theme Option
	 */
	function medicale_mascot_get_redux_option( $id, $fallback = false, $param = false ) {
		global $medicale_mascot_redux_theme_opt;

		if ( $fallback == false ) $fallback = '';

		$output = ( isset( $medicale_mascot_redux_theme_opt[$id] ) && $medicale_mascot_redux_theme_opt[$id] !== '' ) ? $medicale_mascot_redux_theme_opt[$id] : $fallback;

		if ( !empty( $medicale_mascot_redux_theme_opt[$id] ) && $param ) {
			$output = $medicale_mascot_redux_theme_opt[$id][$param];
		}
		return $output;
	}
}



if(!function_exists('medicale_mascot_render_pagination_html')) {
	/**
	 * Function that renders and returns Pagination HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_render_pagination_html() {
		global $wp_query;

		$big = 999999999; // need an unlikely integer
		$pages = paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $wp_query->max_num_pages,
			'prev_next' => false,
			'type'  => 'array',
			'prev_next'   => true,
			'prev_text'	=> esc_html__( '&laquo;', 'medicale-wp' ),
			'next_text'	=> esc_html__( '&raquo;', 'medicale-wp'),
		) );
		$output = '';

		if ( is_array( $pages ) ) {
			$paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var( 'paged' );

			$output .=  '<ul class="pagination">';
			foreach ( $pages as $page ) {
				$output .= "<li>$page</li>";
			}
			$output .= '</ul>';

			// Create an instance of DOMDocument 
			$dom = new \DOMDocument();

			// Populate $dom with $output, making sure to handle UTF-8, otherwise
			// problems will occur with UTF-8 characters.
			$dom->loadHTML( mb_convert_encoding( $output, 'HTML-ENTITIES', 'UTF-8' ) );

			// Create an instance of DOMXpath and all elements with the class 'page-numbers' 
			$xpath = new \DOMXpath( $dom );

			// http://stackoverflow.com/a/26126336/3059883
			$page_numbers = $xpath->query( "//*[contains(concat(' ', normalize-space(@class), ' '), ' page-numbers ')]" );

			// Iterate over the $page_numbers node...
			foreach ( $page_numbers as $page_numbers_item ) {

				// Add class="mynewclass" to the <li> when its child contains the current item.
				$page_numbers_item_classes = explode( ' ', $page_numbers_item->attributes->item(0)->value );
				if ( in_array( 'current', $page_numbers_item_classes ) ) {			
					$list_item_attr_class = $dom->createAttribute( 'class' );
					$list_item_attr_class->value = 'active';
					$page_numbers_item->parentNode->appendChild( $list_item_attr_class );
				}

				// Replace the class 'current' with 'active'
				$page_numbers_item->attributes->item(0)->value = str_replace( 
								'current',
								'active',
								$page_numbers_item->attributes->item(0)->value );

				// Replace the class 'page-numbers' with 'page-link'
				$page_numbers_item->attributes->item(0)->value = str_replace( 
								'page-numbers',
								'page-link',
								$page_numbers_item->attributes->item(0)->value );
			}

			// Save the updated HTML and output it.
			$output = $dom->saveHTML();
		}

		return $output;
	}
}


if(!function_exists('medicale_mascot_sl_get_simple_likes_button')) {
	/**
	 * WordPress Post Like System
	 */
	function medicale_mascot_sl_get_simple_likes_button( $post_id ) {
		if ( medicale_mascot_core_plugin_installed() && function_exists( 'mascot_core_sl_get_simple_likes_button' ) ) {
			return mascot_core_sl_get_simple_likes_button( $post_id );
		}
	}
}