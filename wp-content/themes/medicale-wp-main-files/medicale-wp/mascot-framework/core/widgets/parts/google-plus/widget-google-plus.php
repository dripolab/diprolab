<?php

/*
 * Adds Medicale_Mascot_Widget_GooglePlus widget.
 */
class Medicale_Mascot_Widget_GooglePlus extends Medicale_Mascot_Widget_Loader {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->widgetOptions = array( 
			'classname'		=> 'widget-google-plus clearfix',
			'description'	=> esc_html__( 'The widget lets you easily displays Google Plus.', 'medicale-wp' ),
		);
		parent::__construct( 'tm_widget_google_plus', esc_html__( '(TM) Google +', 'medicale-wp' ), $this->widgetOptions );
		$this->getFormFields();
	}

	
	/**
	 * Get form fields of the widget.
	 */
	protected function getFormFields() {
		$this->formFields = array(
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Widget Title:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	=> esc_html__( 'Google Plus', 'medicale-wp' ),
			),
			array(
				'id'		=> 'custom_css_class',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Custom CSS Class:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'To style particular content element', 'medicale-wp' ),
			),
			array(
				'id'		=> 'badge',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Badge Types', 'medicale-wp' ),
				'options'	=> array(
					'g-community'   => esc_html__( 'Communities', 'medicale-wp' ),
					'g-person'		=> esc_html__( 'Profiles', 'medicale-wp' ),
					'g-page'		=> esc_html__( 'Pages', 'medicale-wp' ),
				)
			),
			array(
				'id'		=> 'url',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Google + Page URL:', 'medicale-wp' ),
				'default'	=> 'https://plus.google.com/communities/105484898574579059070',
			),
			array(
				'id'		=> 'layout',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Layout', 'medicale-wp' ),
				'options'	=> array(
					''			=> esc_html__( 'Portrait', 'medicale-wp' ),
					'landscape' => esc_html__( 'Landscape', 'medicale-wp' ),
				)
			),
			array(
				'id'		=> 'theme',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Color Theme', 'medicale-wp' ),
				'options'	=> array(
					''	 => esc_html__( 'Light', 'medicale-wp' ),
					'dark' => esc_html__( 'Dark', 'medicale-wp' ),
				)
			),
			array(
				'id'		=> 'width',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Width:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'The pixel width of the embed. Default 300', 'medicale-wp' ),
			),
			array(
				'id'		=> 'showcoverphoto',
				'type'		=> 'checkbox',
				'title'		=> esc_html__( 'Show Cover Photo', 'medicale-wp' ),
				'desc'		=> '',
				'value'	=> 'true',
				'default'	=> 'checked',
			),
		);
	}



	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args	 Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, widget_ob_start)
		echo $html = medicale_mascot_get_widget_template_part( 'google-plus', null, 'google-plus/tpl', $instance, true );

		echo $args['after_widget'];
	}
}