<?php
use MASCOTCORE\CPT\Gallery\CPT_Gallery;

/*
 * Adds Medicale_Mascot_Widget_GalleryImages widget.
 */
class Medicale_Mascot_Widget_GalleryImages extends Medicale_Mascot_Widget_Loader {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->widgetOptions = array( 
			'classname'		=> 'widget-gallery-images clearfix',
			'description'	=> esc_html__( 'The widget lets you easily add gallery images.', 'medicale-wp' ),
		);
		parent::__construct( 'tm_widget_gallery_images', esc_html__( '(TM) Gallery Images', 'medicale-wp' ), $this->widgetOptions );
		$this->getFormFields();
	}

	
	/**
	 * Get form fields of the widget.
	 */
	protected function getFormFields() {
		$new_cpt_class = CPT_Gallery::Instance();
		$categories_array = medicale_mascot_category_list_array( $new_cpt_class->ptTaxKey );
		$orderby_parameters_list = medicale_mascot_orderby_parameters_list();
		$this->formFields = array(
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Widget Title:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	=> esc_html__( 'Gallery Images', 'medicale-wp' ),
			),
			array(
				'id'		=> 'custom_css_class',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Custom CSS Class:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'To style particular content element', 'medicale-wp' ),
			),
			array(
				'id'		=> 'gallery_category',				
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Gallery Category', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> $categories_array
			),
			array(
				'id'		=> 'order_by',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Order By:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'date'			=> esc_html__( 'Date', 'medicale-wp' ),
					'title'		=> esc_html__( 'Title', 'medicale-wp' ),
					'comment_count' => esc_html__( 'Number of Comments', 'medicale-wp' )
				)
			),
			array(
				'id'		=> 'order',				
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Order:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'DESC' => esc_html__( 'DESC', 'medicale-wp' ),
					'ASC'  => esc_html__( 'ASC', 'medicale-wp' )
				)
			),
			array(
				'id'		=> 'items_count',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Number of Items to Show', 'medicale-wp' ),
				'desc'		=> '',
			),
		);
	}



	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args	 Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		//get gallery instance
		$new_cpt_class = CPT_Gallery::Instance();
		//default posts per page
		$posts_per_page = ( $instance['items_count'] == '' ) ? -1 : $instance['items_count'];
		//query args
		$query_args = array(
			'post_type' => $new_cpt_class->ptKey,
			'orderby' => $instance['order_by'],
			'order' => $instance['order'],
			'posts_per_page' => $posts_per_page,
		);

		$the_query = new \WP_Query( $query_args );
		$instance['the_query'] = $the_query;


		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, widget_ob_start)
		echo $html = medicale_mascot_get_widget_template_part( 'gallery', null, 'gallery/tpl', $instance, true );

		echo $args['after_widget'];
	}
}