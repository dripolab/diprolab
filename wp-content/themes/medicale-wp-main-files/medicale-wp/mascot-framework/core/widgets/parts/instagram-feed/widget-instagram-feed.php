<?php

/*
 * Adds Medicale_Mascot_Widget_InstagramFeed widget.
 */
class Medicale_Mascot_Widget_InstagramFeed extends Medicale_Mascot_Widget_Loader {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->widgetOptions = array( 
			'classname'		=> 'widget-instagram-feed clearfix',
			'description'	=> esc_html__( 'The widget lets you easily displays instagram feed as list or carousel.', 'medicale-wp' ),
		);
		parent::__construct( 'tm_widget_instagram_feed', esc_html__( '(TM) Instagram Feed', 'medicale-wp' ), $this->widgetOptions );
		$this->getFormFields();
	}

	
	/**
	 * Get form fields of the widget.
	 */
	protected function getFormFields() {
		$this->formFields = array(
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Widget Title:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	=> esc_html__( 'Instagram Feed', 'medicale-wp' ),
			),
			array(
				'id'		=> 'custom_css_class',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Custom CSS Class:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'To style particular content element', 'medicale-wp' ),
			),
			array(
				'id'		=> 'userid',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Instagram User ID:', 'medicale-wp' ),
				'desc'		=> sprintf( esc_html__( 'Put your instagram UserId here. Collect your own UserId from %1$shere%2$s.', 'medicale-wp' ), '<a target="_blank" href="' . esc_url( 'https://smashballoon.com/instagram-feed/find-instagram-user-id/' ) . '">', '</a>' ),
				'default'	=> '3450544574',
			),
			array(
				'id'		=> 'accesstoken',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Instagram Access Token:', 'medicale-wp' ),
				'desc'		=> sprintf( esc_html__( 'Put your instagram Access Token here. Collect Your own Instagram Access Token from %1$shere%2$s.', 'medicale-wp' ), '<a target="_blank" href="' . esc_url( 'http://instagram.pixelunion.net/' ) . '">', '</a>' ),
				'default'	=> '3450544574.1677ed0.7d0725a565914415b40a4953c17bcdc9',
			),
			array(
				'id'		=> 'count',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Number of Images to Show:', 'medicale-wp' ),
				'desc'		=> '',
			),
			array(
				'id'		=> 'feed_type',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Feed Type:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'grid'		=> esc_html__( 'Grid', 'medicale-wp' ),
					'carousel'   => esc_html__( 'Carousel', 'medicale-wp' ),
				)
			),
			array(
				'id'		=> 'image_resolution',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Image Resolution:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'thumbnail' => esc_html__( 'Thumbnail (150x150)', 'medicale-wp' ),
					'low_resolution' => esc_html__( 'Low Resolution (320x320)', 'medicale-wp' ),
					'standard_resolution' => esc_html__( 'Standard Resolution (640x640)', 'medicale-wp' ),
				)
			),
		);
	}



	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args	 Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, widget_ob_start)
		echo $html = medicale_mascot_get_widget_template_part( 'instagram-feed' . '-' . $instance['feed_type'], null, 'instagram-feed/tpl', $instance, true );

		echo $args['after_widget'];
	}
}