<?php

/*
 * Adds Medicale_Mascot_Widget_OpeningHours widget.
 */
class Medicale_Mascot_Widget_OpeningHours extends Medicale_Mascot_Widget_Loader {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->widgetOptions = array( 
			'classname'		=> 'widget-opening-hours clearfix',
			'description'	=> esc_html__( 'The widget lets you easily display Opening Hours.', 'medicale-wp' ),
		);
		parent::__construct( 'tm_widget_opening_hours', esc_html__( '(TM) Opening Hours', 'medicale-wp' ), $this->widgetOptions );
		$this->getFormFields();
	}

	
	/**
	 * Get form fields of the widget.
	 */
	protected function getFormFields() {

		$this->formFields = array(
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Widget Title:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	=> esc_html__( 'Opening Hours', 'medicale-wp' ),
			),
			array(
				'id'		=> 'custom_css_class',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Custom CSS Class:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'To style particular content element', 'medicale-wp' ),
			),
			array(
				'id'		=> 'border_color',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Border Color:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'border-light'  => 'Border Light',
					'border-dark'   => 'Border Dark',
				)
			),


			//Monday
			array(
				'id'		=> 'day_monday',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Monday:', 'medicale-wp' ),
				'default'	=> esc_html__( '9:00 - 17:00', 'medicale-wp' ),
			),


			//Tuesday
			array(
				'id'		=> 'day_tuesday',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Tuesday:', 'medicale-wp' ),
				'default'	=> esc_html__( '9:00 - 17:00', 'medicale-wp' ),
			),


			//Wednesday
			array(
				'id'		=> 'day_wednesday',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Wednesday:', 'medicale-wp' ),
				'default'	=> esc_html__( '9:00 - 17:00', 'medicale-wp' ),
			),


			//Thursday
			array(
				'id'		=> 'day_thursday',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Thursday:', 'medicale-wp' ),
				'default'	=> esc_html__( '9:00 - 17:00', 'medicale-wp' ),
			),


			//Friday
			array(
				'id'		=> 'day_friday',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Friday:', 'medicale-wp' ),
				'default'	=> esc_html__( '9:30 - 16:00', 'medicale-wp' ),
			),


			//Saturday
			array(
				'id'		=> 'day_saturday',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Saturday:', 'medicale-wp' ),
				'default'	=> esc_html__( '10:00 - 15:00', 'medicale-wp' ),
			),


			//Sunday
			array(
				'id'		=> 'day_sunday',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Sunday:', 'medicale-wp' ),
				'default'	=> esc_html__( 'Closed', 'medicale-wp' ),
			),
		);
	}



	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args	 Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, widget_ob_start)
		echo $html = medicale_mascot_get_widget_template_part( 'opening-hours', null, 'opening-hours/tpl', $instance, true );

		echo $args['after_widget'];
	}
}