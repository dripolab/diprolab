<!-- Place this tag in your head or just before your close body tag. -->
<script src="https://apis.google.com/js/platform.js" async defer></script>

<!-- Place this tag where you want the widget to render. -->
<div class="<?php echo $badge;?>" data-width="<?php echo ($width) ? $width : '300';?>" data-href="<?php echo $url;?>" data-theme="<?php echo $theme;?>" data-layout="<?php echo $layout;?>" data-showcoverphoto="<?php echo ($showcoverphoto) ? $showcoverphoto : 'false';?>" data-rel="publisher"></div>