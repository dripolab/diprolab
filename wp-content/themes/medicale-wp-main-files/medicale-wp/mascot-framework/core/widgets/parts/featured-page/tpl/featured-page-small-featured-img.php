  <div class="page <?php echo $custom_css_class;?>">
	<?php
		$thumb_url = '';
		$thumb_id = get_post_thumbnail_id( $choose_page ); 
		$full_image_url = wp_get_attachment_image_src( $thumb_id, 'full' );
		$full_image_url = $full_image_url[0];
		
		//if has post thumbnail
		if( $thumb_id ) {
		//create resized image
		$resized_image = medicale_mascot_matthewruddy_image_resize( $full_image_url, 100, 80, true );
		if( !is_array( $resized_image ) || !$resized_image['url'] || $resized_image['url'] == '' ) {
			$resized_image['url'] = '';
		}
		$thumb_url = $resized_image['url'];
		}
	?>  
	<a class="thumb pull-left flip mr-10" href="<?php the_permalink( $choose_page );?>"><img src="<?php echo $thumb_url;?>" alt=""></a>
	<div class="page-content">
		<h5><a href="<?php the_permalink( $choose_page );?>"><?php echo get_the_title( $choose_page ); ?></a></h5>
		<p><?php echo get_the_excerpt( $choose_page ); ?></p>
	</div>
  </div>