<?php
  $random_number = wp_rand( 111111, 999999 );
?>

<?php if ( $the_query->have_posts() ) : ?>

  <!-- Portfolio Gallery Grid -->
  <div id="widget-grid-<?php echo $random_number.'-'.get_the_ID();?>" class="gallery-isotope grid-3 gutter <?php echo $custom_css_class;?> clearfix">
	<!-- the loop -->
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	<?php
		$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
		$full_image_url = $full_image_url[0];

		$gallery_images = rwmb_meta( 'medicale_mascot_' . "cpt_gallery_images", 'size=full' );
		//if has no post thumbnail
		if( !has_post_thumbnail( get_the_ID() ) ) {
		if ( !empty( $gallery_images ) ) {
			$first_image_key = key($gallery_images);
			$full_image_url = $gallery_images[$first_image_key]['url'];
		}
		}

		//thumb image
		$thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medicale_mascot_square_small' );
		$thumb_url = $thumb_url[0];

		// if has no post thubnail or gallery images then add placeholder image
		if( empty( $thumb_url ) ) {
		$thumb_url = array();
		$full_image_url = $thumb_url['url'] = "http://placehold.it/250x250?text=Image Not Found!";
		}
	?>
	<!-- Portfolio Item Start -->
	<div class="gallery-item box-hover-effect">
		<div class="effect-wrapper">
		<div class="thumb">
			<img src="<?php echo $thumb_url;?>" alt="<?php the_title();?>" width="250">
		</div>
		<div class="overlay-shade"></div>
		<a data-rel="prettyPhoto[gallery-widget-<?php echo $random_number;?>]" title="<?php the_title();?>" href="<?php echo $full_image_url;?>" class="hover-link"></a>
		</div>
	</div>
	<!-- Portfolio Item End -->
	<?php endwhile; ?>
	<!-- end of the loop -->
  </div>
  <!-- End Portfolio Gallery Grid -->
  <?php wp_reset_postdata(); ?>

<?php else : ?>
  <p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'medicale-wp' ); ?></p>
<?php endif; ?>