<?php

/*
 * Adds Medicale_Mascot_Widget_IconBox widget.
 */
class Medicale_Mascot_Widget_IconBox extends Medicale_Mascot_Widget_Loader {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->widgetOptions = array( 
			'classname'		=> 'widget-icon-box clearfix',
			'description'	=> esc_html__( 'The widget lets you easily add Icon Box.', 'medicale-wp' ),
		);
		parent::__construct( 'tm_widget_icon_box', esc_html__( '(TM) Icon Box', 'medicale-wp' ), $this->widgetOptions );
		$this->getFormFields();
	}

	
	/**
	 * Get form fields of the widget.
	 */
	protected function getFormFields() {
		$this->formFields = array(
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Widget Title:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	=> esc_html__( 'Icon Box', 'medicale-wp' ),
			),
			array(
				'id'		=> 'custom_css_class',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Custom CSS Class:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'To style particular content element', 'medicale-wp' ),
			),
			array(
				'id'		=> 'icon_box_title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Title', 'medicale-wp' ),
				'desc'		=> '',
			),
			array(
				'id'		=> 'title_tag',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Title Tag:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'h5' => 'h5',
					'h2' => 'h2',
					'h3' => 'h3',
					'h4' => 'h4',
					'h6' => 'h6',
				)
			),
			array(
				'id'		=> 'paragraph',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Paragraph', 'medicale-wp' ),
				'desc'		=> '',
			),
			array(
				'id'		=> 'hyperlink',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Hyperlink', 'medicale-wp' ),
				'desc'		=> '',
			),
			array(
				'id'		=> 'target',
				'type'		=> 'checkbox',
				'title'		=> esc_html__( 'Open Link in New Tab', 'medicale-wp' ),
				'desc'		=> '',
				'value'	=> 'on',
			),
			array(
				'id'		=> 'icon',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Icon', 'medicale-wp' ),
				'desc'		=> sprintf( esc_html__( 'Example: fa fa-user. Collect your own icon from %1$sFontAwesome%2$s.', 'medicale-wp' ), '<a target="_blank" href="' . esc_url( 'http://fontawesome.io/icons/' ) . '">', '</a>' ),
			),

			array(
				'id'		=> 'icon_size',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Icon Size:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					''  => 'Default',
					'icon-xs' => 'Extra Small',
					'icon-sm' => 'Small',
					'icon-lg' => 'Large',
					'icon-xl' => 'Extra Large',
				)
			),
			array(
				'id'		=> 'icon_color',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Icon Color:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'icon-gray' => 'Gray',
					''			=> 'Default',
					'icon-dark' => 'Dark',
				)
			),

			array(
				'id'		=> 'icon_style',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Icon Style:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'icon-rounded' => 'Rounded',
					''			=> 'Default',
					'icon-circled' => 'Circled',
				)
			),
			array(
				'id'		=> 'icon_border_style',
				'type'		=> 'checkbox',
				'title'		=> esc_html__( 'Make Icon Area Bordered?', 'medicale-wp' ),
				'desc'		=> '',
				'value'	=> 'on',
			),
			array(
				'id'		=> 'icon_theme_colored',
				'type'		=> 'checkbox',
				'title'		=> esc_html__( 'Make Icon Theme Colored?', 'medicale-wp' ),
				'desc'		=> '',
				'value'	=> 'on',
			),
		);
	}



	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args	 Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, widget_ob_start)
		echo $html = medicale_mascot_get_widget_template_part( 'icon-box', null, 'icon-box/tpl', $instance, true );

		echo $args['after_widget'];
	}
}