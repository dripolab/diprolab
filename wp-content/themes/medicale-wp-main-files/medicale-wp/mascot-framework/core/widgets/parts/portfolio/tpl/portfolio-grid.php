<?php if ( $the_query->have_posts() ) : ?>

  <!-- Portfolio Grid -->
  <div id="grid" class="gallery-isotope grid-3 gutter clearfix <?php echo $custom_css_class;?>">
	<!-- the loop -->
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	<?php
		$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
		$full_image_url = $full_image_url[0];

		$portfolio_images = rwmb_meta( 'medicale_mascot_' . "portfolio_metabox_portfolio_gallery_images", 'size=full' );
		//if has no post thumbnail
		if( !has_post_thumbnail( get_the_ID() ) ) {
		if ( !empty( $portfolio_images ) ) {
			$first_image_key = key($portfolio_images);
			echo $full_image_url = $portfolio_images[$first_image_key]['url'];
		}
		}

		//thumb image
		$thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medicale_mascot_square_small' );
		$thumb_url = $thumb_url[0];

		// if has no post thubnail or gallery images then add placeholder image
		if( empty( $thumb_url ) ) {
		$thumb_url = array();
		$full_image_url = $thumb_url['url'] = "http://placehold.it/250x250?text=Image Not Found!";
		}
	?>
	<!-- Portfolio Item Start -->
	<div class="gallery-item box-hover-effect">
		<div class="effect-wrapper">
		<div class="thumb">
			<img src="<?php echo $thumb_url;?>" alt="<?php the_title();?>">
		</div>
		<div class="overlay-shade"></div>
		<div class="icons-holder icons-holder-middle">
			<div class="icons-holder-inner">
			<div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
				<a href="<?php the_title();?>"><i class="fa fa-link"></i></a>
			</div>
			</div>
		</div>
		<a title="<?php the_title();?>" href="<?php the_permalink();?>" class="hover-link"></a>
		</div>
	</div>
	<!-- Portfolio Item End -->
	<?php endwhile; ?>
	<!-- end of the loop -->
  </div>
  <!-- End Portfolio Grid -->
  <?php wp_reset_postdata(); ?>

<?php else : ?>
  <p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'medicale-wp' ); ?></p>
<?php endif; ?>