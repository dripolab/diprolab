<?php

/*
 * Adds Medicale_Mascot_Widget_PinterestPin widget.
 */
class Medicale_Mascot_Widget_PinterestPin extends Medicale_Mascot_Widget_Loader {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->widgetOptions = array( 
			'classname'		=> 'widget-pinterest-pin clearfix',
			'description'	=> esc_html__( 'The Widget lets you easily embed Pinterest Pin on your site.', 'medicale-wp' ),
		);
		parent::__construct( 'tm_widget_pinterest_pin', esc_html__( '(TM) Pinterest Pin', 'medicale-wp' ), $this->widgetOptions );
		$this->getFormFields();
	}

	
	/**
	 * Get form fields of the widget.
	 */
	protected function getFormFields() {
		$this->formFields = array(
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Widget Title:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	=> esc_html__( 'Pinterest Pin', 'medicale-wp' ),
			),
			array(
				'id'		=> 'pin_url',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Pin URL:', 'medicale-wp' ),
				'default'	=> 'https://www.pinterest.com/pin/99360735500167749/',
			),
			array(
				'id'		=> 'pin_size',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Pin Size:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'small'	=> esc_html__( 'Small', 'medicale-wp' ),
					'medium'   => esc_html__( 'Medium', 'medicale-wp' ),
					'large'	=> esc_html__( 'Large', 'medicale-wp' ),
				)
			),
			array(
				'id'		=> 'hide_description',
				'type'		=> 'checkbox',
				'title'		=> esc_html__( 'Hide description:', 'medicale-wp' ),
				'desc'		=> '',
				'value'	=> 'true',
			),
		);
	}



	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args	 Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		//Register Pinterest Script
		wp_register_script( 'pinterest-pinit-js', '//assets.pinterest.com/js/pinit.js', array('jquery'), null, true );
		wp_enqueue_script( array( 'pinterest-pinit-js' ) );


		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, widget_ob_start)
		echo $html = medicale_mascot_get_widget_template_part( 'pinterest-pin', null, 'pinterest-pin/tpl', $instance, true );

		echo $args['after_widget'];
	}
}