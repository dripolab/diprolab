<?php
if( $board_size != 'custom' ) {
	$board_size_array = explode('-', $board_size);
	$board_width 	= $board_size_array[0];
	$board_height 	= $board_size_array[1];
	$image_width 	= $board_size_array[2];
}
?>
<a data-pin-do="embedBoard" data-pin-board-width="<?php echo $board_width;?>" data-pin-scale-height="<?php echo $board_height;?>" data-pin-scale-width="<?php echo $image_width;?>" href="<?php echo $board_url;?>"></a>