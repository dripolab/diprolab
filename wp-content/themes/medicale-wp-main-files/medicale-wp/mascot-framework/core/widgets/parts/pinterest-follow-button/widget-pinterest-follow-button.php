<?php

/*
 * Adds Medicale_Mascot_Widget_PinterestFollowButton widget.
 */
class Medicale_Mascot_Widget_PinterestFollowButton extends Medicale_Mascot_Widget_Loader {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->widgetOptions = array( 
			'classname'		=> 'widget-pinterest-follow-button clearfix',
			'description'	=> esc_html__( 'The Widget lets you easily embed Follow Button on your site.', 'medicale-wp' ),
		);
		parent::__construct( 'tm_widget_pinterest_follow_button', esc_html__( '(TM) Pinterest Follow Button', 'medicale-wp' ), $this->widgetOptions );
		$this->getFormFields();
	}

	
	/**
	 * Get form fields of the widget.
	 */
	protected function getFormFields() {
		$this->formFields = array(
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Widget Title:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	=> esc_html__( 'Pinterest Follow Button', 'medicale-wp' ),
			),
			array(
				'id'		=> 'user_url',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Pinterest User URL:', 'medicale-wp' ),
				'default'	=> 'https://www.pinterest.com/pinterest/',
			),
			array(
				'id'		=> 'full_name',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Full Name:', 'medicale-wp' ),
				'default'	 => esc_html__( 'Pinterest', 'medicale-wp' ),
			),
		);
	}



	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args	 Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		//Register Pinterest Script
		wp_register_script( 'pinterest-pinit-js', '//assets.pinterest.com/js/pinit.js', array('jquery'), null, true );
		wp_enqueue_script( array( 'pinterest-pinit-js' ) );


		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, widget_ob_start)
		echo $html = medicale_mascot_get_widget_template_part( 'pinterest-follow-button', null, 'pinterest-follow-button/tpl', $instance, true );

		echo $args['after_widget'];
	}
}