<div class="contact-info-style1 <?php echo $custom_css_class;?>">
  <ul>
	<?php if(!empty($name)): ?>
	<li><i class="fa fa-user-circle-o"></i> <?php echo $name;?></li>
	<?php endif; ?>

	<?php if(!empty($company)): ?>
	<li><i class="fa fa-university"></i> <?php echo $company;?></li>
	<?php endif; ?>

	<?php if(!empty($phone)): ?>
	<li><i class="fa fa-phone-square"></i> <?php echo $phone;?></li>
	<?php endif; ?>

	<?php if(!empty($fax)): ?>
	<li><i class="fa fa-fax"></i> <?php echo $fax;?></li>
	<?php endif; ?>

	<?php if(!empty($email)): ?>
	<li><i class="fa fa-envelope"></i> <?php echo $email;?></li>
	<?php endif; ?>

	<?php if(!empty($website)): ?>
	<li><i class="fa fa-globe"></i> <?php echo $website;?></li>
	<?php endif; ?>

	<?php if(!empty($skype)): ?>
	<li><i class="fa fa-skype"></i> <?php echo $skype;?></li>
	<?php endif; ?>

	<?php if(!empty($address)): ?>
	<li><i class="fa fa-map-o"></i> <?php echo $address;?></li>
	<?php endif; ?>

  </ul>
</div>