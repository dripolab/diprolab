<?php

/*
 * Adds Medicale_Mascot_Widget_PinterestBoard widget.
 */
class Medicale_Mascot_Widget_PinterestBoard extends Medicale_Mascot_Widget_Loader {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->widgetOptions = array( 
			'classname'		=> 'widget-pinterest-board clearfix',
			'description'	=> esc_html__( 'The Widget lets you easily embed Pinterest Board on your site.', 'medicale-wp' ),
		);
		parent::__construct( 'tm_widget_pinterest_board', esc_html__( '(TM) Pinterest Board', 'medicale-wp' ), $this->widgetOptions );
		$this->getFormFields();
	}

	
	/**
	 * Get form fields of the widget.
	 */
	protected function getFormFields() {
		$this->formFields = array(
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Widget Title:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	=> esc_html__( 'Pinterest Board', 'medicale-wp' ),
			),
			array(
				'id'		=> 'board_url',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Pinterest Board URL:', 'medicale-wp' ),
				'default'	=> 'https://www.pinterest.com/pinterest/official-news/',
			),
			array(
				'id'		=> 'board_size',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Board Size:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'400-240-80'	=> esc_html__( 'Square', 'medicale-wp' ),
					'150-800-60'	=> esc_html__( 'Sidebar', 'medicale-wp' ),
					'900-120-115'   => esc_html__( 'Header', 'medicale-wp' ),
					'custom'		=> esc_html__( 'Custom', 'medicale-wp' ),
				)
			),
			array(
				'id'		=> 'board_custom_description',
				'type'		=> 'description',
				'title'		=> esc_html__( 'The following values will be used only for custom "Board Size" selected from above dropdown menu.', 'medicale-wp' ),
			),
			array(
				'id'		=> 'board_custom_line',
				'type'		=> 'line',
			),
			array(
				'id'		=> 'image_width',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Image Width:', 'medicale-wp' ),
				'default'	=> '80',
			),
			array(
				'id'		=> 'board_height',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Board Height:', 'medicale-wp' ),
				'default'	=> '240',
			),
			array(
				'id'		=> 'board_width',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Board Width:', 'medicale-wp' ),
				'default'	=> '400',
			),
		);
	}



	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args	 Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		//Register Pinterest Script
		wp_register_script( 'pinterest-pinit-js', '//assets.pinterest.com/js/pinit.js', array('jquery'), null, true );
		wp_enqueue_script( array( 'pinterest-pinit-js' ) );


		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, widget_ob_start)
		echo $html = medicale_mascot_get_widget_template_part( 'pinterest-board', null, 'pinterest-board/tpl', $instance, true );

		echo $args['after_widget'];
	}
}