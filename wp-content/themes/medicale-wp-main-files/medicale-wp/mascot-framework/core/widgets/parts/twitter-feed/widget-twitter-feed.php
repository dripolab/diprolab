<?php

/*
 * Adds Medicale_Mascot_Widget_TwitterFeed widget.
 */
class Medicale_Mascot_Widget_TwitterFeed extends Medicale_Mascot_Widget_Loader {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->widgetOptions = array( 
			'classname'		=> 'widget-twitter-feed clearfix',
			'description'	=> esc_html__( 'The widget lets you easily displays Twitter Feed as list or carousel.', 'medicale-wp' ),
		);
		parent::__construct( 'tm_widget_twitter_feed', esc_html__( '(TM) Twitter Feed', 'medicale-wp' ), $this->widgetOptions );
		$this->getFormFields();
	}

	
	/**
	 * Get form fields of the widget.
	 */
	protected function getFormFields() {
		$this->formFields = array(
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Widget Title:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	=> esc_html__( 'Twitter Feed', 'medicale-wp' ),
			),
			array(
				'id'		=> 'custom_css_class',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Custom CSS Class:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'To style particular content element', 'medicale-wp' ),
			),
			array(
				'id'		=> 'userid',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Twitter Username:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'Put Your Twitter Username Here. Default: "Envato".', 'medicale-wp' ),
				'default'	=> 'Envato',
			),
			array(
				'id'		=> 'count',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Set Tweets Count:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'Number of tweets you want to display. Default: "3".', 'medicale-wp' ),
				'default'	=> '3',
			),
			array(
				'id'		=> 'feed_type',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Feed Type:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'list'		=> esc_html__( 'List', 'medicale-wp' ),
					'carousel'   => esc_html__( 'Carousel', 'medicale-wp' ),
				)
			),
		);
	}



	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args	 Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, widget_ob_start)
		echo $html = medicale_mascot_get_widget_template_part( 'twitter-feed' . '-' . $instance['feed_type'], null, 'twitter-feed/tpl', $instance, true );

		echo $args['after_widget'];
	}
}