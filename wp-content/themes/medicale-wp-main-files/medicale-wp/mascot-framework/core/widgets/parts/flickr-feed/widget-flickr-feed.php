<?php

/*
 * Adds Medicale_Mascot_Widget_FlickrFeed widget.
 */
class Medicale_Mascot_Widget_FlickrFeed extends Medicale_Mascot_Widget_Loader {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->widgetOptions = array( 
			'classname'		=> 'widget-flickr-feed clearfix',
			'description'	=> esc_html__( 'The widget lets you easily add flickr feed.', 'medicale-wp' ),
		);
		parent::__construct( 'tm_widget_flickr_feed', esc_html__( '(TM) Flickr Feed', 'medicale-wp' ), $this->widgetOptions );
		$this->getFormFields();
	}

	
	/**
	 * Get form fields of the widget.
	 */
	protected function getFormFields() {
		$this->formFields = array(
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Widget Title:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	 => esc_html__( 'Flickr Feed', 'medicale-wp' ),
			),
			array(
				'id'		=> 'custom_css_class',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Custom CSS Class:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'To style particular content element', 'medicale-wp' ),
			),
			array(
				'id'		=> 'userid',
				'type'		=> 'text',
				'title'		=> esc_html__( 'User Id', 'medicale-wp' ),
				'desc'		=> sprintf( esc_html__( 'Example: 52617155@N08. Collect your userid from %1$shere%2$s.', 'medicale-wp' ), '<a target="_blank" href="' . esc_url( 'http://idgettr.com/' ) . '">', '</a>' ),
			),
			array(
				'id'		=> 'count',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Number of Images', 'medicale-wp' ),
				'desc'		=> '',
			),
			array(
				'id'		=> 'size',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Image Size', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					's' => esc_html__( 'Small Square Box', 'medicale-wp' ),
					't' => esc_html__( 'Thumbnail Size', 'medicale-wp' ),
					'm' => esc_html__( 'Medium Size', 'medicale-wp' ),
				)
			),
			array(
				'id'		=> 'orderby',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Order By', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'latest' => esc_html__( 'Latest', 'medicale-wp' ),
					'random' => esc_html__( 'Random', 'medicale-wp' ),
				)
			)
		);
	}



	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args	 Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, widget_ob_start)
		echo $html = medicale_mascot_get_widget_template_part( 'flickr-feed', null, 'flickr-feed/tpl', $instance, true );

		echo $args['after_widget'];
	}
}