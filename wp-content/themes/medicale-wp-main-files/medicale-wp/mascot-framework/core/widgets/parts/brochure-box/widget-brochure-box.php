<?php

/*
 * Adds Medicale_Mascot_Widget_BrochureBox widget.
 */
class Medicale_Mascot_Widget_BrochureBox extends Medicale_Mascot_Widget_Loader {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->widgetOptions = array( 
			'classname'		=> 'widget-brochure-box clearfix',
			'description'	=> esc_html__( 'A widget that displays Brochure Box with download link.', 'medicale-wp' ),
		);
		parent::__construct( 'tm_widget_brochure_box', esc_html__( '(TM) Brochure Box', 'medicale-wp' ), $this->widgetOptions );
		$this->getFormFields();

		add_action('admin_enqueue_scripts', 'medicale_mascot_widget_brochure_enque_media_library_for_admin' );
	}
	
	/**
	 * Get form fields of the widget.
	 */
	protected function getFormFields() {
		$this->formFields = array(
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Widget Title:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	=> esc_html__( 'Brochure Box', 'medicale-wp' ),
			),
			array(
				'id'		=> 'custom_css_class',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Custom CSS Class:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'To style particular content element', 'medicale-wp' ),
			),
			array(
				'id'		=> 'visual_style',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Visual Style', 'medicale-wp' ),
				'desc'		=> 'Please choose one from different predefined styles.',
				'options'	=> array(
					'default'			=> esc_html__( 'Default', 'medicale-wp' ),
					'classic'			=> esc_html__( 'Classic', 'medicale-wp' ),
				)
			),
			array(
				'id'		=> 'brochure_box_theme_colored',
				'type'		=> 'checkbox',
				'title'		=> esc_html__( 'Use Theme Colore', 'medicale-wp' ),
				'desc'		=> '',
				'value'	=> 'brochure-box-theme-colored',
			),
			array(
				'id'		=> 'file_url',
				'type'		=> 'media_upload',
				'title'		=> esc_html__( 'Brochure URL:', 'medicale-wp' ),
			),
			array(
				'id'		=> 'text',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Brochure Text:', 'medicale-wp' ),
			),
			array(
				'id'		=> 'icon',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Brochure Icon:', 'medicale-wp' ),
				'desc'		=> sprintf( esc_html__( 'Example: fa fa-download. Collect your own icon from %1$sFontAwesome%2$s.', 'medicale-wp' ), '<a target="_blank" href="' . esc_url( 'http://fontawesome.io/icons/' ) . '">', '</a>' ),
			),
			array(
				'id'		=> 'icon_list',				
				'type'		=> 'icon_list',
				'title'		=> esc_html__( 'Or select any icon from here:', 'medicale-wp' ),
				'desc'		=> '',
				'class'	=> 'fa-lg',
				'target'   => 'icon', //targeted text field
				'options'	=> medicale_mascot_icon_font_packs()->getIconFontPackByKey('font_awesome')->getFileTypeIconList()
			),
			array(
				'id'		=> 'target',
				'type'		=> 'checkbox',
				'title'		=> esc_html__( 'Open Link in New Tab', 'medicale-wp' ),
				'desc'		=> '',
				'value'	=> 'on',
			),
		);
	}



	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args	 Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, widget_ob_start)
		echo $html = medicale_mascot_get_widget_template_part( 'brochure-box', $instance['visual_style'], 'brochure-box/tpl', $instance, true );

		echo $args['after_widget'];
	}
}


function medicale_mascot_widget_brochure_enque_media_library_for_admin( $hook ) {
	if ( $hook == 'widgets.php' ) {
		wp_enqueue_media();
	}
}