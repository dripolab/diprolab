<?php if ( $the_query->have_posts() ) : ?>
  <div class="<?php echo $custom_css_class;?>">
	<div class="owl-carousel owl-theme owl-carousel-1col" data-dots="true">
		<!-- the loop -->
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<div class="item">
			<div class="testimonial sm-text-center pt-10">
			<?php if ( $show_thumb == 'true' ) : ?>
			<div class="thumb pull-left flip mb-0 mr-0 pr-20 sm-pull-none">
				<?php echo get_the_post_thumbnail( get_the_ID(), array( 64, 64 ), array( 'class' => 'img-circle' ) );?>
			</div>
			<?php endif; ?>
			<div class="<?php if ( $show_thumb == 'false' ) : ?>ml-100 ml-sm-0<?php endif; ?>">
				<?php if ( $show_testimonial_text == 'true' ) : ?>
				<p class="author-text"><?php echo rwmb_meta( 'medicale_mascot_' . "cpt_author_text" );?></p>
				<?php endif; ?>
				<p class="author mt-20">
				<?php if ( $show_author_name == 'true' ) : ?>
				- <span class="author-name"><?php echo rwmb_meta( 'medicale_mascot_' . "cpt_author_name" );?>,</span> 
				<?php endif; ?>
				<small>
					<em>
					<?php if ( $show_author_job_position == 'true' ) : ?>
					<?php echo rwmb_meta( 'medicale_mascot_' . "cpt_author_job_position" );?> 
					<?php endif; ?>
					<?php if ( $show_author_company == 'true' ) : ?>
					<a href="<?php echo rwmb_meta( 'medicale_mascot_' . "cpt_author_company_URL" );?>">
						<?php echo rwmb_meta( 'medicale_mascot_' . "cpt_author_company" );?>
					</a>
					<?php endif; ?>
					</em>
				</small>
				</p>
			</div>
			</div>
		</div>
		<?php endwhile; ?>
		<!-- end of the loop -->
	</div>
  </div>
  <?php wp_reset_postdata(); ?>

<?php else : ?>
  <p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'medicale-wp' ); ?></p>
<?php endif; ?>