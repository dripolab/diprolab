<?php
  $random_number = wp_rand( 111111, 999999 );
?>
<div id="instafeed-<?php echo $random_number;?>" class="instagram-feed-carousel <?php echo $custom_css_class;?>" data-userid="<?php echo $userid;?>" data-accesstoken="<?php echo $accesstoken;?>" data-limit="<?php echo $count;?>" data-resolution="<?php echo $image_resolution;?>"></div>