<ul class="styled-icons <?php echo $icon_size;?> <?php echo $icon_color;?> <?php if( $icon_border_style ) { echo 'icon-bordered'; }?> <?php echo $icon_style;?> <?php if( $icon_theme_colored ) { echo 'icon-theme-colored'; }?> <?php echo $custom_css_class;?>">
	<?php 
	if( $social_links ): foreach( $social_links as $key => $value ) {
		if( !empty( medicale_mascot_get_redux_option( 'social-links-url-'.$key ) ) ) :
	 ?>
	<li><a href="<?php echo medicale_mascot_get_redux_option( 'social-links-url-'.$key ); ?>" target="<?php echo medicale_mascot_get_redux_option( 'social-links-open-in-window' ); ?>"><i class="fa fa-<?php echo $key; ?>"></i></a></li>
	<?php endif; } endif; ?>	
</ul>