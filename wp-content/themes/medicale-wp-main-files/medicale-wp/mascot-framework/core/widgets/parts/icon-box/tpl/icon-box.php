
<div class="icon-box icon-left <?php echo $custom_css_class;?><?php if( $icon_theme_colored ) { echo 'iconbox-theme-colored'; }?>">
  <a class="icon <?php echo $icon_size;?> <?php echo $icon_color;?> <?php if( $icon_border_style ) { echo 'icon-bordered'; }?> <?php echo $icon_style;?> pull-left flip" <?php if($target) echo 'target="_blank"';?> href="<?php echo $hyperlink;?>">
	<i class="<?php echo $icon;?>"></i>
  </a>
  <<?php echo $title_tag;?> class="icon-box-title"><a <?php if($target) echo 'target="_blank"';?> href="<?php echo $hyperlink;?>"><?php echo $icon_box_title;?></a></<?php echo $title_tag;?>>
  <div class="content"><?php echo $paragraph;?></div>
</div>