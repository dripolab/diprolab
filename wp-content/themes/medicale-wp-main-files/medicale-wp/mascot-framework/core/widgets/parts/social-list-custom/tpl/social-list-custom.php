<ul class="styled-icons <?php echo $icon_size;?> <?php echo $icon_color;?> <?php if( $icon_border_style ) { echo 'icon-bordered'; }?> <?php echo $icon_style;?> <?php if( $icon_theme_colored ) { echo 'icon-theme-colored'; }?> <?php echo $custom_css_class;?>">
	<?php 
		for ($i=1; $i <= 15 ; $i++) { 
		$link = 'link_'.$i;
		$link_network = 'link_'.$i.'_network';
		if( !empty( $$link ) ) :
	 ?>
	<li><a href="<?php echo $$link; ?>" ><i class="fa <?php echo $$link_network; ?>"></i></a></li>
	<?php endif; } ?>
</ul>