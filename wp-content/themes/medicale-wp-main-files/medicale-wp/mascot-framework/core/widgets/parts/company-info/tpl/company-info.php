<div class="company-info <?php echo $custom_css_class;?>">
	<?php if(!empty($company_logo)): ?>
	<img class="mt-10 mb-20" alt="" src="<?php echo $company_logo;?>">
	<?php endif; ?>

	<?php if(!empty($company)): ?>
	<p><?php echo $company;?></p>
	<?php endif; ?>

	<ul>
		<?php if(!empty($address)): ?>
		<li><i class="fa fa-map-o"></i> <span><?php echo $address;?></span></li>
		<?php endif; ?>

		<?php if(!empty($phone)): ?>
		<li><i class="fa fa-phone"></i> <?php echo $phone;?></li>
		<?php endif; ?>

		<?php if(!empty($fax)): ?>
		<li><i class="fa fa-fax"></i> <?php echo $fax;?></li>
		<?php endif; ?>

		<?php if(!empty($email)): ?>
		<li><i class="fa fa-envelope"></i> <?php echo $email;?></li>
		<?php endif; ?>

		<?php if(!empty($website)): ?>
		<li><i class="fa fa-globe"></i> <?php echo $website;?></li>
		<?php endif; ?>
	</ul>
</div>