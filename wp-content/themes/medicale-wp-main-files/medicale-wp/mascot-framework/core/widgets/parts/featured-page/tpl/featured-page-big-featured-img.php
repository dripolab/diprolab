  <div class="page <?php echo $custom_css_class;?>">
	<?php echo get_the_post_thumbnail( $choose_page ); ?>
	<div class="page-content">
		<h4><a href="<?php the_permalink( $choose_page );?>"><?php echo get_the_title( $choose_page ); ?></a></h4>
		<p><?php echo get_the_excerpt( $choose_page ); ?></p>
		<a class="text-theme-colored" href="<?php the_permalink( $choose_page );?>">Read more</a>
	</div>
  </div>