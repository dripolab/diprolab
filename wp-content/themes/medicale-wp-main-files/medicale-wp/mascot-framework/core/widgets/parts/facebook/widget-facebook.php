<?php

/*
 * Adds Medicale_Mascot_Widget_Facebook widget.
 */
class Medicale_Mascot_Widget_Facebook extends Medicale_Mascot_Widget_Loader {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->widgetOptions = array( 
			'classname'		=> 'widget-facebook-page clearfix',
			'description'	=> esc_html__( 'The Page plugin lets you easily embed and promote any Facebook Page on your website. Just like on Facebook, your visitors can like and share the Page without leaving your site.', 'medicale-wp' ),
		);
		parent::__construct( 'tm_widget_facebook', esc_html__( '(TM) Facebook Page', 'medicale-wp' ), $this->widgetOptions );
		$this->getFormFields();
	}

	
	/**
	 * Get form fields of the widget.
	 */
	protected function getFormFields() {
		$this->formFields = array(
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Widget Title:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	=> esc_html__( 'Facebook Page', 'medicale-wp' ),
			),
			array(
				'id'		=> 'url',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Facebook page URL:', 'medicale-wp' ),
				'default'	=> 'https://www.facebook.com/facebook',
			),
			array(
				'id'		=> 'width',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Width:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'The pixel width of the embed (Min. 180 to Max. 500). Default 340', 'medicale-wp' ),
			),
			array(
				'id'		=> 'height',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Height:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'The pixel height of the embed (Min. 70). Default 500', 'medicale-wp' ),
			),
			array(
				'id'		=> 'small_header',
				'type'		=> 'checkbox',
				'title'		=> esc_html__( 'Use Small Header', 'medicale-wp' ),
				'desc'		=> '',
				'value'	=> 'true',
			),
			array(
				'id'		=> 'hide_cover',
				'type'		=> 'checkbox',
				'title'		=> esc_html__( 'Hide Cover Photo', 'medicale-wp' ),
				'desc'		=> '',
				'value'	=> 'true',
			),
			array(
				'id'		=> 'show_facepile',
				'type'		=> 'checkbox',
				'title'		=> esc_html__( 'Show Friend\'s Faces', 'medicale-wp' ),
				'desc'		=> '',
				'value'	=> 'true',
				'default'	=> 'checked',
			),
		);
	}



	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args	 Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, widget_ob_start)
		echo $html = medicale_mascot_get_widget_template_part( 'facebook-page-box', null, 'facebook/tpl', $instance, true );

		echo $args['after_widget'];
	}
}