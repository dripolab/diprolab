<?php

/*
 * Adds Medicale_Mascot_Widget_SocialList widget.
 */
class Medicale_Mascot_Widget_SocialList extends Medicale_Mascot_Widget_Loader {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->widgetOptions = array( 
			'classname'		=> 'widget-social-icons clearfix',
			'description'	=> esc_html__( 'The widget lets you easily display social icons which are configured at Theme Options > Social Links.', 'medicale-wp' ),
		);
		parent::__construct( 'tm_widget_social_list', esc_html__( '(TM) Social List', 'medicale-wp' ), $this->widgetOptions );
		$this->getFormFields();
	}

	
	/**
	 * Get form fields of the widget.
	 */
	protected function getFormFields() {
		$this->formFields = array(
			array(
				'id'		=> 'desc',
				'type'		=> 'description',
				'title'		=> $this->widgetOptions['description'],
			),
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Widget Title:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	=> esc_html__( 'Social List', 'medicale-wp' ),
			),
			array(
				'id'		=> 'custom_css_class',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Custom CSS Class:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'To style particular content element', 'medicale-wp' ),
			),

			array(
				'id'		=> 'icon_size',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Icon Size:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					''  => 'Default',
					'icon-xs' => 'Extra Small',
					'icon-sm' => 'Small',
					'icon-lg' => 'Large',
					'icon-xl' => 'Extra Large',
				)
			),
			array(
				'id'		=> 'icon_color',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Icon Color:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'icon-dark' => 'Dark',
					''			=> 'Default',
					'icon-gray' => 'Gray',
				)
			),

			array(
				'id'		=> 'icon_style',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Icon Style:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'icon-rounded' => 'Rounded',
					''			=> 'Default',
					'icon-circled' => 'Circled',
				)
			),

			
			array(
				'id'		=> 'icon_border_style',
				'type'		=> 'checkbox',
				'title'		=> esc_html__( 'Make Icon Area Bordered?', 'medicale-wp' ),
				'desc'		=> '',
				'value'	=> 'on',
			),
			array(
				'id'		=> 'icon_theme_colored',
				'type'		=> 'checkbox',
				'title'		=> esc_html__( 'Make Icon Theme Colored?', 'medicale-wp' ),
				'desc'		=> '',
				'value'	=> 'on',
			),
		);
	}



	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args	 Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		//Enabled social links
		$instance['social_links'] = medicale_mascot_get_redux_option( 'social-links-ordering', false, 'Enabled' );

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, widget_ob_start)
		echo $html = medicale_mascot_get_widget_template_part( 'social-list', null, 'social-list/tpl', $instance, true );

		echo $args['after_widget'];
	}
}