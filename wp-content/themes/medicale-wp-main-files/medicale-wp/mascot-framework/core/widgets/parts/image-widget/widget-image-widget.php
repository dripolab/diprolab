<?php

/*
 * Adds Medicale_Mascot_Widget_ImageWidget widget.
 */
class Medicale_Mascot_Widget_ImageWidget extends Medicale_Mascot_Widget_Loader {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->widgetOptions = array( 
			'classname'		=> 'widget-image-widget clearfix',
			'description'	=> esc_html__( 'A widget that displays Image with description.', 'medicale-wp' ),
		);
		parent::__construct( 'tm_widget_image_widget', esc_html__( '(TM) Image Widget', 'medicale-wp' ), $this->widgetOptions );
		$this->getFormFields();
	}

	
	/**
	 * Get form fields of the widget.
	 */
	protected function getFormFields() {
		$this->formFields = array(
			array(
				'id'		=> 'desc',
				'type'		=> 'description',
				'title'		=> $this->widgetOptions['description'],
			),
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Widget Title:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	=> '',
			),
			array(
				'id'		=> 'custom_css_class',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Custom CSS Class:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'To style particular content element', 'medicale-wp' ),
			),
			array(
				'id'		=> 'uploaded_image',
				'type'		=> 'media_upload',
				'title'		=> esc_html__( 'Image URL:', 'medicale-wp' ),
			),
			array(
				'id'		=> 'image_alignment',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Image Alignment:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'text-left flip'	 => esc_html__( 'Left', 'medicale-wp' ),
					'text-center'   => esc_html__( 'Center', 'medicale-wp' ),
					'text-right flip'	=> esc_html__( 'Right', 'medicale-wp' )
				)
			),
			array(
				'id'		=> 'description',
				'type'		=> 'textarea',
				'title'		=> esc_html__( 'Description', 'medicale-wp' ),
				'desc'		=> '',
			),
		);
	}



	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args	 Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, widget_ob_start)
		echo $html = medicale_mascot_get_widget_template_part( 'image-widget', null, 'image-widget/tpl', $instance, true );

		echo $args['after_widget'];
	}
}