<?php

/*
 * Adds Medicale_Mascot_Widget_ContactInfo widget.
 */
class Medicale_Mascot_Widget_ContactInfo extends Medicale_Mascot_Widget_Loader {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->widgetOptions = array( 
			'classname'		=> 'widget-contact-info clearfix',
			'description'	=> esc_html__( 'A widget that displays contact info in different styles.', 'medicale-wp' ),
		);
		parent::__construct( 'tm_widget_contact_info', esc_html__( '(TM) Contact Info', 'medicale-wp' ), $this->widgetOptions );
		$this->getFormFields();
	}

	
	/**
	 * Get form fields of the widget.
	 */
	protected function getFormFields() {
		$this->formFields = array(
			array(
				'id'		=> 'desc',
				'type'		=> 'description',
				'title'		=> $this->widgetOptions['description'],
			),
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Widget Title:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	=> esc_html__( 'Contact Info', 'medicale-wp' ),
			),
			array(
				'id'		=> 'custom_css_class',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Custom CSS Class:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'To style particular content element', 'medicale-wp' ),
			),
			array(
				'id'		=> 'name',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Name', 'medicale-wp' ),
				'desc'		=> '',
			),
			array(
				'id'		=> 'company',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Company Name', 'medicale-wp' ),
				'desc'		=> '',
			),
			array(
				'id'		=> 'phone',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Phone', 'medicale-wp' ),
				'desc'		=> '',
			),
			array(
				'id'		=> 'fax',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Fax', 'medicale-wp' ),
				'desc'		=> '',
			),
			array(
				'id'		=> 'email',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Email', 'medicale-wp' ),
				'desc'		=> '',
			),
			array(
				'id'		=> 'website',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Website', 'medicale-wp' ),
				'desc'		=> '',
			),
			array(
				'id'		=> 'skype',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Skype', 'medicale-wp' ),
				'desc'		=> '',
			),
			array(
				'id'		=> 'address',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Address', 'medicale-wp' ),
				'desc'		=> '',
			),
			array(
				'id'		=> 'contact_info_style',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Contact Info Style', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'style1' => esc_html__( 'Style 1', 'medicale-wp' ),
					'style2' => esc_html__( 'Style 2', 'medicale-wp' ),
				)
			),
		);
	}



	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args	 Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, widget_ob_start)
		echo $html = medicale_mascot_get_widget_template_part( 'contact-info-' . $instance['contact_info_style'], null, 'contact-info/tpl', $instance, true );

		echo $args['after_widget'];
	}
}