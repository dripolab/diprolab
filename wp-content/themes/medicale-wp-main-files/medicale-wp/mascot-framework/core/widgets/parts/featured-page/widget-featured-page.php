<?php

/*
 * Adds Medicale_Mascot_Widget_FeaturedPage widget.
 */
class Medicale_Mascot_Widget_FeaturedPage extends Medicale_Mascot_Widget_Loader {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->widgetOptions = array( 
			'classname'		=> 'widget-featured-page clearfix',
			'description'	=> esc_html__( 'A widget that displays Featured Page.', 'medicale-wp' ),
		);
		parent::__construct( 'tm_widget_featured_page', esc_html__( '(TM) Featured Page', 'medicale-wp' ), $this->widgetOptions );
		$this->getFormFields();
	}

	
	/**
	 * Get form fields of the widget.
	 */
	protected function getFormFields() {
		$this->formFields = array(
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Widget Title:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	=> esc_html__( 'Featured Page', 'medicale-wp' ),
			),
			array(
				'id'		=> 'custom_css_class',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Custom CSS Class:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'To style particular content element', 'medicale-wp' ),
			),
			array(
				'id'		=> 'choose_page',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Choose Page:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> medicale_mascot_get_page_list_array()
			),
			array(
				'id'		=> 'presentation_style',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Presentation Style:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'big-featured-img'	=> esc_html__( 'Big Featured Image', 'medicale-wp' ),
					'small-featured-img'  => esc_html__( 'Small Featured Image', 'medicale-wp' ),
				)
			),
		);
	}



	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args	 Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		$the_page = get_page( $instance['choose_page'] );
		$instance['the_page'] = $the_page;

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, widget_ob_start)
		echo $html = medicale_mascot_get_widget_template_part( 'featured-page-' . $instance['presentation_style'], null, 'featured-page/tpl', $instance, true );

		echo $args['after_widget'];
	}
}