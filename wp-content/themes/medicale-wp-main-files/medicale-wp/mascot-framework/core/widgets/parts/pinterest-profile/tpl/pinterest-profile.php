<?php
if( $size != 'custom' ) {
	$board_size_array = explode('-', $size);
	$board_width 	= $board_size_array[0];
	$board_height 	= $board_size_array[1];
	$image_width 	= $board_size_array[2];
}
?>
<a data-pin-do="embedUser" data-pin-board-width="<?php echo $board_width;?>" data-pin-scale-height="<?php echo $board_height;?>" data-pin-scale-width="<?php echo $image_width;?>" href="<?php echo $user_url;?>"></a>