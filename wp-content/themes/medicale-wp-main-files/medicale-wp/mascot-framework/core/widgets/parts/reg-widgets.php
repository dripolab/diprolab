<?php
// Block direct requests
if ( !defined('ABSPATH') ) die('-1');


if(!function_exists('medicale_mascot_register_widgets')) {
	/**
	 * Register all widgets
	 */
	function medicale_mascot_register_widgets() {
		$widget_list = array(
			'Medicale_Mascot_Widget_BlogList',
			'Medicale_Mascot_Widget_ContactInfo',
			'Medicale_Mascot_Widget_CompanyInfo',
			'Medicale_Mascot_Widget_Facebook',
			'Medicale_Mascot_Widget_FlickrFeed',
			'Medicale_Mascot_Widget_GoogleMap',
			'Medicale_Mascot_Widget_GooglePlus',
			'Medicale_Mascot_Widget_InstagramFeed',
			'Medicale_Mascot_Widget_OpeningHours',
			'Medicale_Mascot_Widget_OpeningHoursCompressed',
			'Medicale_Mascot_Widget_SocialList',
			'Medicale_Mascot_Widget_SocialListCustom',
			'Medicale_Mascot_Widget_PinterestFollowButton',
			'Medicale_Mascot_Widget_PinterestPin',
			'Medicale_Mascot_Widget_PinterestBoard',
			'Medicale_Mascot_Widget_PinterestProfile',
			'Medicale_Mascot_Widget_FeaturedPage',
			'Medicale_Mascot_Widget_BrochureBox',
			'Medicale_Mascot_Widget_IconBox',
			'Medicale_Mascot_Widget_HorizontalRow',
			'Medicale_Mascot_Widget_EmptySpace',
			'Medicale_Mascot_Widget_ImageWidget',
		);

		//if twitter feed plugin installed
		if( defined( 'MASCOT_TWITTER_FEED_VERSION' ) ) {
			$widget_list[] = 'Medicale_Mascot_Widget_TwitterFeed';
		}

		//apply filter
		if( has_filter('mascot_core_register_widgets_add_widgets') ) {
			$widget_list = apply_filters('mascot_core_register_widgets_add_widgets', $widget_list);
		}

		foreach( $widget_list as $each_widget ) {
			register_widget( $each_widget );
		}

	}
	/* Register the widget */
	add_action('widgets_init', 'medicale_mascot_register_widgets');
}
