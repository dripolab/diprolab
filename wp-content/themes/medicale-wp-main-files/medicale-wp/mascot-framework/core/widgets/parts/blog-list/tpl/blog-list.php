
<?php if ( $the_query->have_posts() ) : ?>
  <div class="<?php echo $custom_css_class;?>">
	<!-- the loop -->
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	<article class="post media-post clearfix">
		<?php if( !$disable_thumb ): ?>
		<a class="post-thumb" href="<?php the_permalink();?>"><?php the_post_thumbnail( 'medicale_mascot_square_extra_small' ); ?></a>
		<?php endif; ?>
		<div class="post-right">
		<<?php echo $post_title_tag;?> class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></<?php echo $post_title_tag;?>>
		<?php medicale_mascot_posted_on_date();?>
		</div>
	</article>
	<?php endwhile; ?>
	<!-- end of the loop -->
  </div>
  <?php wp_reset_postdata(); ?>

<?php else : ?>
  <p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'medicale-wp' ); ?></p>
<?php endif; ?>