<ul class="opening-hours <?php echo $border_color?>">
	<?php 
		for ($i=1; $i <= 7 ; $i++) { 
			$day = 'day_'.$i;
			$day_time = 'day_'.$i.'_time';
			if( !empty( $$day ) ) :
	 ?>
			<li class="clearfix">
				<span><?php echo $$day; ?></span>
				<div class="value"><?php echo $$day_time; ?></div>
			</li>
	<?php 
			endif; 
		}
	?>
</ul>
