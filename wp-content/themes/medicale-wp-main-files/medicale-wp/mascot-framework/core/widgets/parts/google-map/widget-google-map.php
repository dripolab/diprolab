<?php

/*
 * Adds Medicale_Mascot_Widget_GoogleMap widget.
 */
class Medicale_Mascot_Widget_GoogleMap extends Medicale_Mascot_Widget_Loader {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->widgetOptions = array( 
			'classname'		=> 'widget-gallery-images clearfix',
			'description'	=> esc_html__( 'The widget lets you easily displays Google Map.', 'medicale-wp' ),
		);
		parent::__construct( 'tm_widget_google_map', esc_html__( '(TM) Google Map', 'medicale-wp' ), $this->widgetOptions );
		$this->getFormFields();
	}

	
	/**
	 * Get form fields of the widget.
	 */
	protected function getFormFields() {
		$this->formFields = array(
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Widget Title:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	=> esc_html__( 'Google Map', 'medicale-wp' ),
			),
			array(
				'id'		=> 'lat',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Latitude:', 'medicale-wp' ),
				'desc'		=> sprintf( esc_html__( 'Collect your Latitude from %1$shere%2$s.', 'medicale-wp' ), '<a target="_blank" href="' . esc_url( 'http://www.latlong.net/' ) . '">', '</a>' ),
			),
			array(
				'id'		=> 'long',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Longitude:', 'medicale-wp' ),
				'desc'		=> sprintf( esc_html__( 'Collect your Longitude from %1$shere%2$s.', 'medicale-wp' ), '<a target="_blank" href="' . esc_url( 'http://www.latlong.net/' ) . '">', '</a>' ),
			),
			array(
				'id'		=> 'zoom',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Zoom Level:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'Zoom levels between 0 (the lowest zoom level, in which the entire world can be seen on one map) and 21+ (down to streets and individual buildings)', 'medicale-wp' ),
				'default'	=> esc_html__( '14', 'medicale-wp' ),
			),
			array(
				'id'		=> 'height',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Height of the Map Canvas:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'Default: 300', 'medicale-wp' ),
			),
			array(
				'id'		=> 'marker_url',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Custom Pin Marker URL:', 'medicale-wp' ),
				'desc'		=> '',
			),
			array(
				'id'		=> 'marker_text_title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Title Text for Marker Popup:', 'medicale-wp' ),
				'default'	=> esc_html__( 'Envato', 'medicale-wp' ),
				'desc'		=> '',
			),
			array(
				'id'		=> 'marker_text_desc',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Short Description for Marker Popup:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	=> esc_html__( 'The world\'s leading marketplace and community for creative assets and creative people.', 'medicale-wp' ),
			),
			array(
				'id'		=> 'mapstyle',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Google Map Style', 'medicale-wp' ),
				'desc'		=> 'Please choose one from different predefined map styles.',
				'options'	=> array(
					'default'	=> esc_html__( 'Default', 'medicale-wp' ),
					'style1'	 => esc_html__( 'Style 1', 'medicale-wp' ),
					'style2'	 => esc_html__( 'Style 2', 'medicale-wp' ),
					'style3'	 => esc_html__( 'Style 3', 'medicale-wp' ),
					'style4'	 => esc_html__( 'Style 4', 'medicale-wp' ),
					'style5'	 => esc_html__( 'Style 5', 'medicale-wp' ),
					'style6'	 => esc_html__( 'Style 6', 'medicale-wp' ),
					'style7'	 => esc_html__( 'Style 7', 'medicale-wp' ),
					'style8'	 => esc_html__( 'Style 8', 'medicale-wp' ),
					'style9'	 => esc_html__( 'Style 9', 'medicale-wp' ),
					'dark'		=> esc_html__( 'Dark', 'medicale-wp' ),
					'greyscale1' => esc_html__( 'Greyscale 1', 'medicale-wp' ),
					'greyscale2' => esc_html__( 'Greyscale 2', 'medicale-wp' ),
				)
			),
		);
	}



	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args	 Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		if( $instance['lat'] == '' ) $instance['lat'] = '-37.817314';
		if( $instance['long'] == '' ) $instance['long'] = '144.955431';
		if( $instance['zoom'] == '' ) $instance['zoom'] = '10';
		if( $instance['height'] == '' ) $instance['height'] = '300';
		if( $instance['marker_url'] == '' ) $instance['marker_url'] = MASCOT_ASSETS_URI . '/images/map-marker1.png';


		//Enque Google Map Scripts
		wp_enqueue_script( array( 'google-maps-api', 'google-maps-init' ) );
		

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, widget_ob_start)
		echo $html = medicale_mascot_get_widget_template_part( 'google-map', null, 'google-map/tpl', $instance, true );

		echo $args['after_widget'];
	}
}