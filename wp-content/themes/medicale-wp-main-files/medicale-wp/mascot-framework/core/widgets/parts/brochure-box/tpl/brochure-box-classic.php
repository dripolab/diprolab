<a class="brochure-box <?php echo $visual_style;?> <?php echo $brochure_box_theme_colored;?> <?php echo $custom_css_class;?>" <?php if($target) echo 'target="_blank"';?> href="<?php echo $file_url;?>">
  <h5 class="text"><?php echo $text;?></h5>
  <i class="brochure-icon <?php echo $icon;?>"></i>
</a>