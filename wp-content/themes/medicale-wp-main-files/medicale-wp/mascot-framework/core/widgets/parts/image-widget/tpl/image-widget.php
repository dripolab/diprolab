<?php if(!empty($uploaded_image)): ?>
<div class="thumb <?php echo $image_alignment;?>">
	<img alt="" src="<?php echo $uploaded_image;?>">
</div>
<?php endif; ?>

<?php if(!empty($description)): ?>
<p><?php echo $description;?></p>
<?php endif; ?>