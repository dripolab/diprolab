<?php

/*
 * Adds Medicale_Mascot_Widget_SocialListCustom widget.
 */
class Medicale_Mascot_Widget_SocialListCustom extends Medicale_Mascot_Widget_Loader {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->widgetOptions = array( 
			'classname'		=> 'widget-social-list-custom clearfix',
			'description'	=> esc_html__( 'The widget lets you easily display social icons.', 'medicale-wp' ),
		);
		parent::__construct( 'tm_widget_social_list_custom', esc_html__( '(TM) Social List (Custom)', 'medicale-wp' ), $this->widgetOptions );
		$this->getFormFields();
	}

	
	/**
	 * Get form fields of the widget.
	 */
	protected function getFormFields() {
		$social_array = array(
			'fa-dribbble'  => esc_html__( 'Dribble', 'medicale-wp' ),
			'fa-facebook'  => esc_html__( 'Facebook', 'medicale-wp' ),
			'fa-flickr'  => esc_html__( 'Flickr', 'medicale-wp' ),
			'fa-google-plus'  => esc_html__( 'Google Plus', 'medicale-wp' ),
			'fa-instagram'  => esc_html__( 'Instagram', 'medicale-wp' ),

			'fa-linkedin'  => esc_html__( 'Linkedin', 'medicale-wp' ),
			'fa-pinterest'  => esc_html__( 'Pinterest', 'medicale-wp' ),
			'fa-rss'  => esc_html__( 'RSS', 'medicale-wp' ),
			'fa-skype'  => esc_html__( 'Skype', 'medicale-wp' ),
			'fa-tumblr'  => esc_html__( 'Tumblr', 'medicale-wp' ),
			
			'fa-twitter'  => esc_html__( 'Twitter', 'medicale-wp' ),
			'fa-vimeo-square'  => esc_html__( 'Vimeo', 'medicale-wp' ),
			'fa-vine'  => esc_html__( 'Vine', 'medicale-wp' ),
			'fa-wordpress'  => esc_html__( 'Wordpress', 'medicale-wp' ),
			'fa-youtube'  => esc_html__( 'Youtube', 'medicale-wp' ),
		);

		$this->formFields = array(
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Widget Title:', 'medicale-wp' ),
				'desc'		=> '',
				'default'	=> esc_html__( 'Social List', 'medicale-wp' ),
			),
			array(
				'id'		=> 'custom_css_class',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Custom CSS Class:', 'medicale-wp' ),
				'desc'		=> esc_html__( 'To style particular content element', 'medicale-wp' ),
			),


			array(
				'id'		=> 'icon_size',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Icon Size:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					''  => 'Default',
					'icon-xs' => 'Extra Small',
					'icon-sm' => 'Small',
					'icon-lg' => 'Large',
					'icon-xl' => 'Extra Large',
				)
			),
			array(
				'id'		=> 'icon_color',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Icon Color:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'icon-dark' => 'Dark',
					''			=> 'Default',
					'icon-gray' => 'Gray',
				)
			),

			array(
				'id'		=> 'icon_style',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Icon Style:', 'medicale-wp' ),
				'desc'		=> '',
				'options'	=> array(
					'icon-rounded' => 'Rounded',
					''			=> 'Default',
					'icon-circled' => 'Circled',
				)
			),

			
			array(
				'id'		=> 'icon_border_style',
				'type'		=> 'checkbox',
				'title'		=> esc_html__( 'Make Icon Area Bordered?', 'medicale-wp' ),
				'desc'		=> '',
				'value'	=> 'on',
			),
			array(
				'id'		=> 'icon_theme_colored',
				'type'		=> 'checkbox',
				'title'		=> esc_html__( 'Make Icon Theme Colored?', 'medicale-wp' ),
				'desc'		=> '',
				'value'	=> 'on',
			),
			array(
				'id'		=> 'target',
				'type'		=> 'checkbox',
				'title'		=> esc_html__( 'Open Link in New Tab', 'medicale-wp' ),
				'desc'		=> '',
				'value'	=> 'on',
			),

			

			//link 1
			array(
				'id'		=> 'link_1',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Link 1:', 'medicale-wp' ),
				'desc'	=> '',
			),
			array(
				'id'		=> 'link_1_network',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Choose Social Network:', 'medicale-wp' ),
				'desc'		=> '',
				'width'	=> 'auto',
				'options'	=> $social_array
			),
			array(
				'id'		=> 'link_1_line',
				'type'		=> 'line',
			),

			

			//link 2
			array(
				'id'		=> 'link_2',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Link 2:', 'medicale-wp' ),
				'desc'	=> '',
			),
			array(
				'id'		=> 'link_2_network',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Choose Social Network:', 'medicale-wp' ),
				'desc'		=> '',
				'width'	=> 'auto',
				'options'	=> $social_array
			),
			array(
				'id'		=> 'link_2_line',
				'type'		=> 'line',
			),

			

			//link 3
			array(
				'id'		=> 'link_3',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Link 3:', 'medicale-wp' ),
				'desc'	=> '',
			),
			array(
				'id'		=> 'link_3_network',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Choose Social Network:', 'medicale-wp' ),
				'desc'		=> '',
				'width'	=> 'auto',
				'options'	=> $social_array
			),
			array(
				'id'		=> 'link_3_line',
				'type'		=> 'line',
			),

			

			//link 4
			array(
				'id'		=> 'link_4',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Link 4:', 'medicale-wp' ),
				'desc'	=> '',
			),
			array(
				'id'		=> 'link_4_network',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Choose Social Network:', 'medicale-wp' ),
				'desc'		=> '',
				'width'	=> 'auto',
				'options'	=> $social_array
			),
			array(
				'id'		=> 'link_4_line',
				'type'		=> 'line',
			),

			

			//link 5
			array(
				'id'		=> 'link_5',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Link 5:', 'medicale-wp' ),
				'desc'	=> '',
			),
			array(
				'id'		=> 'link_5_network',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Choose Social Network:', 'medicale-wp' ),
				'desc'		=> '',
				'width'	=> 'auto',
				'options'	=> $social_array
			),
			array(
				'id'		=> 'link_5_line',
				'type'		=> 'line',
			),

			

			//link 6
			array(
				'id'		=> 'link_6',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Link 6:', 'medicale-wp' ),
				'desc'	=> '',
			),
			array(
				'id'		=> 'link_6_network',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Choose Social Network:', 'medicale-wp' ),
				'desc'		=> '',
				'width'	=> 'auto',
				'options'	=> $social_array
			),
			array(
				'id'		=> 'link_6_line',
				'type'		=> 'line',
			),

			

			//link 7
			array(
				'id'		=> 'link_7',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Link 7:', 'medicale-wp' ),
				'desc'	=> '',
			),
			array(
				'id'		=> 'link_7_network',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Choose Social Network:', 'medicale-wp' ),
				'desc'		=> '',
				'width'	=> 'auto',
				'options'	=> $social_array
			),
			array(
				'id'		=> 'link_7_line',
				'type'		=> 'line',
			),

			

			//link 8
			array(
				'id'		=> 'link_8',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Link 8:', 'medicale-wp' ),
				'desc'	=> '',
			),
			array(
				'id'		=> 'link_8_network',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Choose Social Network:', 'medicale-wp' ),
				'desc'		=> '',
				'width'	=> 'auto',
				'options'	=> $social_array
			),
			array(
				'id'		=> 'link_8_line',
				'type'		=> 'line',
			),

			

			//link 9
			array(
				'id'		=> 'link_9',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Link 9:', 'medicale-wp' ),
				'desc'	=> '',
			),
			array(
				'id'		=> 'link_9_network',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Choose Social Network:', 'medicale-wp' ),
				'desc'		=> '',
				'width'	=> 'auto',
				'options'	=> $social_array
			),
			array(
				'id'		=> 'link_9_line',
				'type'		=> 'line',
			),

			

			//link 10
			array(
				'id'		=> 'link_10',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Link 10:', 'medicale-wp' ),
				'desc'	=> '',
			),
			array(
				'id'		=> 'link_10_network',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Choose Social Network:', 'medicale-wp' ),
				'desc'		=> '',
				'width'	=> 'auto',
				'options'	=> $social_array
			),
			array(
				'id'		=> 'link_10_line',
				'type'		=> 'line',
			),

			

			//link 11
			array(
				'id'		=> 'link_11',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Link 11:', 'medicale-wp' ),
				'desc'	=> '',
			),
			array(
				'id'		=> 'link_11_network',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Choose Social Network:', 'medicale-wp' ),
				'desc'		=> '',
				'width'	=> 'auto',
				'options'	=> $social_array
			),
			array(
				'id'		=> 'link_11_line',
				'type'		=> 'line',
			),

			

			//link 12
			array(
				'id'		=> 'link_12',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Link 12:', 'medicale-wp' ),
				'desc'	=> '',
			),
			array(
				'id'		=> 'link_12_network',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Choose Social Network:', 'medicale-wp' ),
				'desc'		=> '',
				'width'	=> 'auto',
				'options'	=> $social_array
			),
			array(
				'id'		=> 'link_12_line',
				'type'		=> 'line',
			),

			

			//link 13
			array(
				'id'		=> 'link_13',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Link 13:', 'medicale-wp' ),
				'desc'	=> '',
			),
			array(
				'id'		=> 'link_13_network',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Choose Social Network:', 'medicale-wp' ),
				'desc'		=> '',
				'width'	=> 'auto',
				'options'	=> $social_array
			),
			array(
				'id'		=> 'link_13_line',
				'type'		=> 'line',
			),

			

			//link 14
			array(
				'id'		=> 'link_14',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Link 14:', 'medicale-wp' ),
				'desc'	=> '',
			),
			array(
				'id'		=> 'link_14_network',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Choose Social Network:', 'medicale-wp' ),
				'desc'		=> '',
				'width'	=> 'auto',
				'options'	=> $social_array
			),
			array(
				'id'		=> 'link_14_line',
				'type'		=> 'line',
			),

			

			//link 15
			array(
				'id'		=> 'link_15',
				'type'		=> 'text',
				'title'		=> esc_html__( 'Link 15:', 'medicale-wp' ),
				'desc'	=> '',
			),
			array(
				'id'		=> 'link_15_network',
				'type'		=> 'dropdown',
				'title'		=> esc_html__( 'Choose Social Network:', 'medicale-wp' ),
				'desc'		=> '',
				'width'	=> 'auto',
				'options'	=> $social_array
			),
			array(
				'id'		=> 'link_15_line',
				'type'		=> 'line',
			),
		);
	}



	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args	 Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, widget_ob_start)
		echo $html = medicale_mascot_get_widget_template_part( 'social-list-custom', null, 'social-list-custom/tpl', $instance, true );

		echo $args['after_widget'];
	}
}