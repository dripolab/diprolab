<?php if( defined( 'MASCOT_TWITTER_FEED_VERSION' ) ) { ?>
<?php $random_number = wp_rand( 111111, 999999 ); ?>
<div id="twitter-feed-<?php echo $random_number;?>" data-username="<?php echo $userid;?>" data-count="<?php echo ($count) ? $count : 3;?>" data-ajaxurl="<?php echo esc_attr( admin_url( 'admin-ajax.php' ) ); ?>" class="twitter-feed twitter-feed-list <?php echo $custom_css_class;?> clearfix"></div>
<?php } else {
sprintf( esc_html__( 'Please install %1$s"Mascot Twitter Feed"%1$s Plugin to enable this feature.', 'medicale-wp' ), '<strong>', '</strong>');
} ?>