<?php

//load lib
require_once MASCOT_FRAMEWORK_DIR . '/core/widgets/lib/abstract-widgets.php';

/* Loads all widgets located in widgets folder
================================================== */
if( !function_exists('medicale_mascot_load_all_widgets') ) {
	function medicale_mascot_load_all_widgets() {
		foreach( glob(MASCOT_FRAMEWORK_DIR.'/core/widgets/parts/*/loader.php') as $each_sc_loader ) {
			require_once $each_sc_loader;
		}
		require_once MASCOT_FRAMEWORK_DIR . '/core/widgets/parts/reg-widgets.php';
	}
	add_action('medicale_mascot_before_custom_action', 'medicale_mascot_load_all_widgets');
}