<?php
// Remove WooCommerce Styles all in one line
add_filter( 'woocommerce_enqueue_styles', '__return_false' );


remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

if (!function_exists('medicale_mascot_shop_archive_before_main_content_page_title')) {
	/**
	 * show page title
	 */
	function medicale_mascot_shop_archive_before_main_content_page_title() {
		medicale_mascot_get_title_area_parts();
	}
	add_action( 'woocommerce_before_main_content', 'medicale_mascot_shop_archive_before_main_content_page_title', 10 );
}



if (!function_exists('medicale_mascot_shop_archive_wrapper_start')) {
	/**
	 * custom before main content
	 */
	function medicale_mascot_shop_archive_wrapper_start() {
		$container = false;

		if( is_product() ) {
			$container = medicale_mascot_get_redux_option( 'shop-single-product-settings-fullwidth' );
		} else if ( is_shop() || is_product_category() || is_product_tag() ) {
			$container = medicale_mascot_get_redux_option( 'shop-archive-settings-fullwidth' );
		}

		if( $container ) {
			$container = 'container-fluid';
		} else {
			$container = 'container';
		}

		echo '<section><div class="' . $container . '">';
	}
	add_action( 'woocommerce_before_main_content', 'medicale_mascot_shop_archive_wrapper_start', 10 );
}



if (!function_exists('medicale_mascot_shop_archive_wrapper_end')) {
	/**
	 * custom after main content
	 */
	function medicale_mascot_shop_archive_wrapper_end() {
		echo '</div></section>';
	}
	add_action( 'woocommerce_after_main_content', 'medicale_mascot_shop_archive_wrapper_end', 10 );
}



if (!function_exists('medicale_mascot_woo_hide_page_title')) {
	/**
	 * remove page title
	 */
	function medicale_mascot_woo_hide_page_title() {
		return false;
	}
	add_filter( 'woocommerce_show_page_title' , 'medicale_mascot_woo_hide_page_title' );
}



if (!function_exists('medicale_mascot_shop_archive_sidebar_position_before')) {
	/**
	 * shop sidebar
	 */
	function medicale_mascot_shop_archive_sidebar_position_before() {
		$sidebar_position = medicale_mascot_get_redux_option( 'shop-archive-settings-sidebar-position' );
		switch ( $sidebar_position ) {
			case 'left':
			case 'right':
				# code...
			?>
			<div class="row">
				<div class="col-md-9 main-content-area<?php echo ( $sidebar_position == 'left' ) ? ' md-pull-right' : ''; ?>">
			<?php
				break;
			
			default:
				# code...
			?>
			<div class="row">
				<div class="col-md-12 main-content-area">
			<?php
				break;
		}
	}
	add_action( 'woocommerce_before_shop_loop', 'medicale_mascot_shop_archive_sidebar_position_before', 1 );
}



if (!function_exists('medicale_mascot_shop_archive_sidebar_position_after')) {
	/**
	 * shop sidebar
	 */
	function medicale_mascot_shop_archive_sidebar_position_after() {
		$sidebar_position = medicale_mascot_get_redux_option( 'shop-archive-settings-sidebar-position' );
		switch ( $sidebar_position ) {
			case 'left':
			case 'right':
				# code...
			?>
				</div>
				<div class="col-md-3">
					<?php dynamic_sidebar( 'shop_sidebar' ); ?>
				</div>
			</div>
			<?php
				break;
			
			default:
				# code...
			?>
				</div>
			</div>
			<?php
				break;
		}
	}
	add_action( 'woocommerce_after_shop_loop', 'medicale_mascot_shop_archive_sidebar_position_after', 30 );
}




if (!function_exists('medicale_mascot_shop_archive_loop_shop_products_per_row')) {
	/**
	 * Number of products Per Row
	 */
	function medicale_mascot_shop_archive_loop_shop_products_per_row() {
		$products_per_row = medicale_mascot_get_redux_option( 'shop-archive-settings-products-per-row', 4 );

		return $products_per_row;
	}
	add_filter( 'loop_shop_per_page', 'medicale_mascot_shop_archive_loop_shop_products_per_row' );
}




if (!function_exists('medicale_mascot_shop_archive_loop_shop_products_per_page')) {
	/**
	 * Number of products displayed per page
	 */
	function medicale_mascot_shop_archive_loop_shop_products_per_page() {
		$products_per_page = medicale_mascot_get_redux_option( 'shop-archive-settings-products-per-page', '10' );

		return $products_per_page;
	}
	add_filter( 'loop_shop_per_page', 'medicale_mascot_shop_archive_loop_shop_products_per_page' );
}


if (!function_exists('medicale_mascot_woocommerce_product_per_page_select')) {
	/**
	 * Add a Products Per Page Dropdown
	 */
	function medicale_mascot_woocommerce_product_per_page_select() {
		$per_page = filter_input(INPUT_GET, 'perpage', FILTER_SANITIZE_NUMBER_INT);	 
		echo '<div class="woocommerce-perpage">';
		echo '<select onchange="if (this.value) window.location.href=this.value" class="perpage">';

		$products_per_page = medicale_mascot_get_redux_option( 'shop-archive-settings-products-per-page', '10' );
		$orderby_options = array(
			$products_per_page => esc_html__( 'Select Products Per Page', 'medicale-wp' ),
		);

		$dropdown_options = medicale_mascot_get_redux_option( 'shop-archive-settings-products-per-page-dropdown-options', false );
		$dropdown_options = explode( ' ', $dropdown_options );
		foreach ($dropdown_options as $value) {
			if( !empty($value) ) {
				$orderby_options[ $value ] = $value . esc_html__( ' Products Per Page', 'medicale-wp' );
			}
		}


		foreach( $orderby_options as $value => $label ) {
			if( !empty($value) ) {
				echo "<option ".selected( $per_page, $value )." value='?perpage=$value'>$label</option>";
			}
		}
		echo '</select>';
		echo '</div>';
	}
	add_action( 'woocommerce_before_shop_loop', 'medicale_mascot_woocommerce_product_per_page_select', 25 );
}


if (!function_exists('medicale_mascot_woocommerce_product_per_page_select_query')) {
	/**
	 * Add a Products Per Page Dropdown Query
	 */
	function medicale_mascot_woocommerce_product_per_page_select_query( $query ) {
		$per_page = filter_input(INPUT_GET, 'perpage', FILTER_SANITIZE_NUMBER_INT);
		if( $query->is_main_query() && !is_admin() && is_post_type_archive( 'product' ) ) {
			$query->set( 'posts_per_page', $per_page );
		}
	}
	add_action( 'pre_get_posts', 'medicale_mascot_woocommerce_product_per_page_select_query' );
}


if ( ! function_exists( 'medicale_mascot_get_shop_isotope_holder_ID' ) ) {
	/**
	 * Returns Shop Isotope Holder ID
	 *
	 */
	function medicale_mascot_get_shop_isotope_holder_ID() {
		$random_number = wp_rand( 111111, 999999 );
		$holder_id = 'isotope-holder-' . $random_number;
		return $holder_id;
	}
}


if ( ! function_exists( 'medicale_mascot_get_shop_catalog_layout' ) ) {
	/**
	 * Returns Shop Catalog Layout Type
	 *
	 */
	function medicale_mascot_get_shop_catalog_layout() {
		$params = array();

		$params['shop_catalog_layout'] = medicale_mascot_get_redux_option( 'shop-layout-settings-select-shop-catalog-layout', 'default' );


		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_woocommerce_template_part( 'shop-catalog-layout', $params['shop_catalog_layout'], 'tpl/catalog-layout', $params );

		return $html;
	}
	add_action( 'woocommerce_init', 'medicale_mascot_get_shop_catalog_layout', 1000 );
}


if (!function_exists('medicale_mascot_shop_archive_register_shop_sidebar')) {
	/**
	 * Register Shop Sidebar
	 */
	function medicale_mascot_shop_archive_register_shop_sidebar() {
		$title_line_bottom_class = '';

		if( medicale_mascot_get_redux_option( 'sidebar-settings-sidebar-title-show-line-bottom' ) ) {
			$title_line_bottom_class = 'widget-title-line-bottom';
		}
		
		// Page Default Sidebar
		register_sidebar( array(
			'name'			=> esc_html__( 'Shop Sidebar', 'medicale-wp' ),
			'id'			=> 'shop_sidebar',
			'description'   => esc_html__( 'This is a default sidebar for page. Widgets in this area will be shown on sidebar of page. Drag and drop your widgets here.', 'medicale-wp' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h5 class="widget-title ' . $title_line_bottom_class . '">',
			'after_title'   => '</h5>',
		) );
	}
	add_action( 'widgets_init', 'medicale_mascot_shop_archive_register_shop_sidebar', 1000 );
}


if (!function_exists('medicale_mascot_woocommerce_checkout_form_field_args')) {
	/**
	 * Add Bootstrap form control class to woocommerce address fields
	 */
	function medicale_mascot_woocommerce_checkout_form_field_args($args, $key, $value) {
		$args['input_class'] = array( 'form-control' );
		return $args;
	}
	add_filter('woocommerce_form_field_args',  'medicale_mascot_woocommerce_checkout_form_field_args',10,3);
}



if (!function_exists('medicale_mascot_woocommerce_breadcrumbs')) {
	/**
	 * Customize the WooCommerce breadcrumb
	 */
	function medicale_mascot_woocommerce_breadcrumbs() {
		return array(
			'delimiter'   => ' <i class="fa fa-angle-right"></i> ',
			'wrap_before' => '<nav class="woocommerce-breadcrumb" itemprop="breadcrumb">',
			'wrap_after'  => '</nav>',
			'before'		=> '',
			'after'		=> '',
			'home'		=> _x( 'Home', 'breadcrumb', 'medicale-wp' ),
		);
	}
	add_filter( 'woocommerce_breadcrumb_defaults', 'medicale_mascot_woocommerce_breadcrumbs' );
}


/**
 * Override WooCommerce Function - Get the product thumbnail for the loop.
 */
function woocommerce_template_loop_product_thumbnail() {
	$products_thumb_type = medicale_mascot_get_redux_option( 'shop-archive-settings-products-thumb-type', 'image-swap' );

	switch ( $products_thumb_type ) {
		case 'image-featured':
			# code...
			echo woocommerce_get_product_thumbnail();

			break;

		case 'image-swap':
			# code...
			echo woocommerce_get_product_thumbnail();
			
			global $product;
			$shop_catalog = wc_get_image_size( 'shop_catalog' );

			$attachment_ids = $product->get_gallery_image_ids();
			if( !empty($attachment_ids) ) {
				$first_gallery_image = $attachment_ids[0];
			} else {
				//otherwise get featured image
				$first_gallery_image = get_post_thumbnail_id();
			}
			
			if( !empty($first_gallery_image) ) {
				$image_link = wp_get_attachment_url( $first_gallery_image );
				$resized_image = medicale_mascot_matthewruddy_image_resize( $image_link, $shop_catalog['width'], $shop_catalog['height'], true );
				echo '<img src="'.$resized_image['url'].'" alt="'.get_the_title().'" class="product-hover-image" title="'.get_the_title().'">';
			}

			break;

		case 'image-gallery':
			# code...
			global $product;
			$shop_catalog = wc_get_image_size( 'shop_catalog' );
			$output = '';

			$attachment_id = get_post_thumbnail_id();
			$attachment_ids = $product->get_gallery_image_ids();
			$attachment_ids[] = $attachment_id;
			$attachment_ids = array_unique($attachment_ids);

			if( !empty($attachment_ids) ) {
				$output .= '<div class="owl-carousel-1col" data-dots="false" data-nav="true">';
				foreach( $attachment_ids as $attachment_id ) {
					$output .= '<div class="item">';
					$image_link = wp_get_attachment_url( $attachment_id );
					$resized_image = medicale_mascot_matthewruddy_image_resize( $image_link, $shop_catalog['width'], $shop_catalog['height'], true );
					$output .= '<img src="'.$resized_image['url'].'" alt="'.get_the_title().'" class="product-hover-image" title="'.get_the_title().'">';
					$output .= '</div>';
				}
				$output .= '</div>';
			}
			echo $output;

			break;
		
		default:
			# code...
			break;
	}
}







/** ===============================
 * Product Single
 * ================================ */

if (!function_exists('medicale_mascot_shop_single_product_gallery_add_theme_support')) {
	/**
	 * Single Product Gallery Add Theme Support 
	 */
	function medicale_mascot_shop_single_product_gallery_add_theme_support() {
		$single_product_catalog_layout = medicale_mascot_get_redux_option( 'shop-single-product-settings-select-single-catalog-layout' );
		if( $single_product_catalog_layout == 'image-with-thumb' ) {
			if( medicale_mascot_get_redux_option( 'shop-single-product-settings-enable-gallery-slider' ) ) {
				add_theme_support( 'wc-product-gallery-slider' );
			}
		}
		if( medicale_mascot_get_redux_option( 'shop-single-product-settings-enable-gallery-lightbox' ) ) {
			add_theme_support( 'wc-product-gallery-lightbox' );
		}
		if( medicale_mascot_get_redux_option( 'shop-single-product-settings-enable-gallery-zoom' ) ) {
			add_theme_support( 'wc-product-gallery-zoom' );
		}

		/**/
	}
	add_action( 'woocommerce_init', 'medicale_mascot_shop_single_product_gallery_add_theme_support' );
}



//review gravatar size
function woocommerce_review_display_gravatar( $comment ) {
	echo get_avatar( $comment, apply_filters( 'woocommerce_review_gravatar_size', '60' ), '', false, array( 'class' => 'media-object img-thumbnail' ) );
}
/**
* Change number of related products output
*/
add_filter( 'woocommerce_output_related_products_args', 'medicale_mascot_shop_single_product_related_products_args' );
function medicale_mascot_shop_single_product_related_products_args( $args ) {
	$args['posts_per_page'] = medicale_mascot_get_redux_option( 'shop-single-product-settings-related-products-count', 8 ); // 4 related products
	$args['columns'] = medicale_mascot_get_redux_option( 'shop-single-product-settings-related-products-per-row', 4 ); // arranged in 2 columns
	return $args;
}


if (!function_exists('medicale_mascot_shop_single_product_sidebar_position_before')) {
	/**
	 * Shop Single Product Sidebar Before
	 */
	function medicale_mascot_shop_single_product_sidebar_position_before() {
		$sidebar_position = medicale_mascot_get_redux_option( 'shop-single-product-settings-sidebar-position' );
		switch ( $sidebar_position ) {
			case 'left':
			case 'right':
				# code...
			?>
			<div class="row">
				<div class="col-md-9 main-content-area<?php echo ( $sidebar_position == 'left' ) ? ' md-pull-right' : ''; ?>">
			<?php
				break;
			
			default:
				# code...
			?>
			<div class="row">
				<div class="col-md-12 main-content-area">
			<?php
				break;
		}
	}
	add_action( 'woocommerce_before_single_product', 'medicale_mascot_shop_single_product_sidebar_position_before', 1 );
}


if (!function_exists('medicale_mascot_shop_single_product_sidebar_position_after')) {
	/**
	 * Shop Single Product Sidebar After
	 */
	function medicale_mascot_shop_single_product_sidebar_position_after() {
		$sidebar_position = medicale_mascot_get_redux_option( 'shop-single-product-settings-sidebar-position' );
		switch ( $sidebar_position ) {
			case 'left':
			case 'right':
				# code...
			?>
				</div>
				<div class="col-md-3">
					<?php dynamic_sidebar( 'shop_sidebar' ); ?>
				</div>
			</div>
			<?php
				break;
			
			default:
				# code...
			?>
				</div>
			</div>
			<?php
				break;
		}
	}
	add_action( 'woocommerce_after_single_product', 'medicale_mascot_shop_single_product_sidebar_position_after' );
}



if (!function_exists('medicale_mascot_shop_single_product_images_column_width_before')) {
	/**
	 * Shop Single Product Images Column Width
	 */
	function medicale_mascot_shop_single_product_images_column_width_before() {
		$images_column_width = medicale_mascot_get_redux_option( 'shop-single-product-settings-product-images-column-width' );
		$images_align = medicale_mascot_get_redux_option( 'shop-single-product-settings-product-images-align' );
		?>
		<div class="row">
			<div class="col-sm-<?php echo $images_column_width;?><?php echo ( $images_align == 'right' ) ? ' md-pull-right' : ''; ?>">
		<?php
	}
	add_action( 'woocommerce_before_single_product_summary', 'medicale_mascot_shop_single_product_images_column_width_before', 1 );
}

if (!function_exists('medicale_mascot_shop_single_product_images_column_width_middle')) {
	/**
	 * Shop Single Product Images Column Width
	 */
	function medicale_mascot_shop_single_product_images_column_width_middle() {
		$images_column_width = medicale_mascot_get_redux_option( 'shop-single-product-settings-product-images-column-width' );
		?>
			</div>
			<div class="col-sm-<?php echo 12-$images_column_width;?>">
		<?php
	}
	add_action( 'woocommerce_before_single_product_summary', 'medicale_mascot_shop_single_product_images_column_width_middle', 30 );
}

if (!function_exists('medicale_mascot_shop_single_product_images_column_width_after')) {
	/**
	 * Shop Single Product Images Column Width
	 */
	function medicale_mascot_shop_single_product_images_column_width_after() {
		?>
			</div>
		</div>
		<?php
	}
	add_action( 'woocommerce_after_single_product_summary', 'medicale_mascot_shop_single_product_images_column_width_after', 1 );
}


if (!function_exists('medicale_mascot_shop_single_product_enable_product_meta')) {
	/**
	 * Enable Product Meta
	 */
	function medicale_mascot_shop_single_product_enable_product_meta() {
		$single_product_enable_product_meta = medicale_mascot_get_redux_option( 'shop-single-product-settings-enable-product-meta' );
		if( !$single_product_enable_product_meta ) {
			remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
		}
	}
	add_action( 'woocommerce_init', 'medicale_mascot_shop_single_product_enable_product_meta' );
}


if (!function_exists('medicale_mascot_shop_single_product_enable_product_sharing')) {
	/**
	 * Enable Product Sharing
	 */
	function medicale_mascot_shop_single_product_enable_product_sharing() {
		$single_product_enable_sharing = medicale_mascot_get_redux_option( 'shop-single-product-settings-enable-sharing' );
		if( $single_product_enable_sharing ) {
			add_action( 'woocommerce_single_product_summary', 'medicale_mascot_get_social_share_links', 49 );
		}
	}
	add_action( 'woocommerce_init', 'medicale_mascot_shop_single_product_enable_product_sharing' );
}


/**
 * Show the subcategory title in the product loop.
 *
 * @param object $category
 */
function woocommerce_template_loop_category_title( $category ) {
	?>
	<h4 class="woocommerce-loop-category__title">
		<?php
			echo $category->name;

			if ( $category->count > 0 ) {
				echo apply_filters( 'woocommerce_subcategory_count_html', ' <mark class="count">(' . $category->count . ')</mark>', $category );
			}
		?>
	</h4>
	<?php
}