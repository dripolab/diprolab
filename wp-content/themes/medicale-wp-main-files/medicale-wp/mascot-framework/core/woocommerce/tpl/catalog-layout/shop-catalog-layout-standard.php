<?php

//removed product link
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

//add before shop loop item
add_action( 'woocommerce_before_shop_loop_item', 'medicale_mascot_catalog_standard_before_shop_loop_item', 11 );
if ( ! function_exists( 'medicale_mascot_catalog_standard_before_shop_loop_item' ) ) {
	/**
	 * Add some HTML codes
	 *
	 */
	function medicale_mascot_catalog_standard_before_shop_loop_item() {
	?>
	<div class="effect-wrapper">
	<?php
	}
}

//add before shop loop item
add_action( 'woocommerce_before_shop_loop_item', 'medicale_mascot_catalog_standard_before_shop_loop_item_two', 13 );
if ( ! function_exists( 'medicale_mascot_catalog_standard_before_shop_loop_item_two' ) ) {
	/**
	 * Add some HTML codes
	 *
	 */
	function medicale_mascot_catalog_standard_before_shop_loop_item_two() {
		$products_thumb_type = medicale_mascot_get_redux_option( 'shop-archive-settings-products-thumb-type', 'image-swap' );
	?>
		<div class="thumb <?php echo $products_thumb_type;?>">
	<?php
	}
}

//add before shop loop item
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
add_action( 'woocommerce_before_shop_loop_item', 'woocommerce_show_product_loop_sale_flash', 12 );


//add before shop loop item title
add_action( 'woocommerce_before_shop_loop_item_title', 'medicale_mascot_catalog_standard_before_shop_loop_item_title', 11 );
if ( ! function_exists( 'medicale_mascot_catalog_standard_before_shop_loop_item_title' ) ) {
	/**
	 * Add some HTML codes
	 *
	 */
	function medicale_mascot_catalog_standard_before_shop_loop_item_title() {
	?>
		</div>
		<div class="overlay-shade shade-white"></div>
		<div class="text-holder text-holder-top-left">
			<h5 class="woocommerce-loop-product__title product-title"><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h5>
			<span class="product-categories"><?php echo wc_get_product_category_list( get_the_ID() ); ?></span>
			<div><?php woocommerce_template_loop_rating(); ?></div>
			<div><?php woocommerce_template_loop_price(); ?></div>
		</div>
		<div class="text-holder text-holder-bottom-left">
			<?php woocommerce_template_loop_add_to_cart() ?>
		</div>
		<div class="text-holder text-holder-bottom-right">
	<?php
	}
}




//add shop loop item title
remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );

//add after shop loop item title
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );


//add after shop loop item
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
add_action( 'woocommerce_after_shop_loop_item', 'medicale_mascot_catalog_standard_after_shop_loop_item', 50 );
if ( ! function_exists( 'medicale_mascot_catalog_standard_after_shop_loop_item' ) ) {
	/**
	 * Add some HTML codes
	 *
	 */
	function medicale_mascot_catalog_standard_after_shop_loop_item() {
	?>
		</div>
	</div>
	<?php
	}
}
