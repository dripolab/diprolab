<?php	
/*
*
*	Custom Visual Composer Scripts
*	---------------------------------------
*	Mascot Framework
* 	Copyright ThemeMascot 2017 - http://www.thememascot.com
*
*/

//Disable Frontend Editor of VC
vc_disable_frontend();


//remove unnecessary elements
//vc_remove_element( 'vc_gmaps' );


/* Font Awesome Icons Param
================================================== */
function mascot_init_icons_param( $settings, $value ) {
 return 'null';
}
//add_shortcode_param( 'init_icons', 'mascot_init_icons_param', get_template_directory_uri().'/mascot-framework/wpbakery/assets/icon-init.js' );


/* Add extra parameters to VC Row
================================================== */
vc_remove_param( "vc_row", "el_id" );
vc_remove_param( "vc_row", "el_class" );

/*vc_remove_param( "vc_row", "full_width" );
vc_remove_param( "vc_row", "full_height" );
vc_remove_param( "vc_row", "equal_height" );
vc_remove_param( "vc_row", "video_bg" );
vc_remove_param( "vc_row", "video_bg_url" );
vc_remove_param( "vc_row", "columns_placement" );
vc_remove_param( "vc_row", "content_placement" ); 
vc_remove_param( "vc_row", "video_bg_parallax" );
vc_remove_param( "vc_row", "parallax" );
vc_remove_param( "vc_row", "parallax_image" );
vc_remove_param( "vc_row", "parallax_speed_video" );
vc_remove_param( "vc_row", "parallax_speed_bg" );*/

vc_add_param( "vc_row", array(
	"type" => "textfield",
	"heading" => esc_html__( "Row ID", 'medicale-wp' ),
	"param_name" => "el_id",
	"value" => "",
	"description" => esc_html__( 'Enter row ID (Note: make sure it is unique and valid according to <a href="http://www.w3schools.com/tags/att_global_id.asp" target="_blank">w3c specification</a>).', 'medicale-wp' ),
	"weight" => "99"
));

vc_add_param( "vc_row", array(
	"type" => "textfield",
	"heading" => esc_html__( "Extra class name", 'medicale-wp' ),
	"param_name" => "el_class",
	"value" => "",
	"description" => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'medicale-wp' ),
	"weight" => "99"
));

vc_add_param( "vc_row", array(
	"type" => 'dropdown',
	"heading" => esc_html__( "Container width", 'medicale-wp' ) ,
	"param_name" => "container_width",
	"description" => esc_html__( "Define the width of the container.", 'medicale-wp' ) ,
	"value" => Array(
		'Container' => 'grid',
		'Full Width' => 'full-width'
	) ,
	"weight" => "99"
));

vc_add_param( "vc_row", array(
	"type" => 'dropdown',
	"heading" => esc_html__( "Content Aligment", 'medicale-wp' ) ,
	"param_name" => "content_alignment",
	"description" => esc_html__( "Define the width of the container.", 'medicale-wp' ) ,
	"value" => Array(
		'Default' => '',
		'Left' => 'left',
		'Center' => 'center',
		'Right' => 'right'
	) ,
	"weight" => "99"
));

vc_add_param( "vc_row", array(
	"type" => 'dropdown',
	"heading" => esc_html__( "Add Background Image Overlay", 'medicale-wp' ) ,
	"param_name" => "add_bg_overlay",
	"description" => esc_html__( "If you want to add overlay on background image then choose yes.", 'medicale-wp' ) ,
	"value" => Array(
		'No' 	=> 'no',
		'Yes' 	=> 'yes',
	) ,
	'group'  => 'Bg Image Overlay',
));

vc_add_param( "vc_row", array(
	"type" => 'dropdown',
	"heading" => esc_html__( "Background Overlay Color", 'medicale-wp' ) ,
	"param_name" => "bg_overlay_color",
	"description" => esc_html__( "If you want to add overlay on background image then choose yes.", 'medicale-wp' ) ,
	"value" => Array(
		'Dark' 		=> 'dark',
		'White' 	=> 'white',
		'Theme Color Overlay' 	=> 'theme-colored',
	) ,
	'dependency'	=> array('element' => 'add_bg_overlay', 'value' => 'yes'),
	'group'  => 'Bg Image Overlay',
));

vc_add_param( "vc_row", array(
	"type" => 'dropdown',
	"heading" => esc_html__( "Background Overlay Color Opacity", 'medicale-wp' ) ,
	"param_name" => "bg_overlay_color_opacity",
	"value" => Array(
		'1' 	=> '1',
		'2' 	=> '2',
		'3' 	=> '3',
		'4' 	=> '4',
		'5' 	=> '5',
		'6' 	=> '6',
		'7' 	=> '7',
		'8' 	=> '8',
		'9' 	=> '9',
	) ,
	'std'  => '7',
	'dependency'	=> array('element' => 'add_bg_overlay', 'value' => 'yes'),
	'group'  => 'Bg Image Overlay',
));


vc_add_param( "vc_row_inner", array(
	"type" => 'dropdown',
	"heading" => esc_html__( "Container width", 'medicale-wp' ) ,
	"param_name" => "container_width",
	"description" => esc_html__( "Define the width of the container.", 'medicale-wp' ) ,
	"value" => Array(
		'Container' => 'grid',
		'Full Width' => 'full-width'
	) ,
	"weight" => "99"
));