<?php	
/*
*
*	Custom Visual Composer Variables
*	---------------------------------------
*	Mascot Framework
* 	Copyright ThemeMascot 2017 - http://www.thememascot.com
*
*/

//Predefined colors
$colors_arr = array(
  esc_html__( 'Grey', 'medicale-wp' ) => 'wpb_button',
  esc_html__( 'Blue', 'medicale-wp' ) => 'btn-primary',
  esc_html__( 'Turquoise', 'medicale-wp' ) => 'btn-info',
  esc_html__( 'Green', 'medicale-wp' ) => 'btn-success',
  esc_html__( 'Orange', 'medicale-wp' ) => 'btn-warning',
  esc_html__( 'Red', 'medicale-wp' ) => 'btn-danger',
  esc_html__( 'Black', 'medicale-wp' ) => "btn-inverse"
);
