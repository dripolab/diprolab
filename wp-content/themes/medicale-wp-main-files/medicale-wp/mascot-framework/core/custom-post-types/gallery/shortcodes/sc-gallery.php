<?php

if(!function_exists('mascot_core_cpt_sc_gallery_vc_map_modifier')) {
	/**
	 * Shortcodes vc_map modifier
	 */
	function mascot_core_cpt_sc_gallery_vc_map_modifier( $shortcode_base ) {
		//vc_remove_param( $shortcode_base, "display_type" );
		//vc_add_param( $shortcode_base, $attributes );
		//vc_add_params( $shortcode_base, $attributes );
	}
}



if(!function_exists('mascot_core_cpt_sc_gallery_render')) {
	/**
	 * Renders shortcodes HTML
	 */
	function mascot_core_cpt_sc_gallery_render( $attr, $content = null, $class_instance ) {

		$new_cpt_class = $class_instance;
		$args = array(
			'custom_css_class' => '',
			'gallery_type' => '',
			'columns' => '4',
			'gallery_image_orientation' => 'landscape',
			'gallery_image_aspect_ratio' => '16:9',

			'show_navigation' => 'true',
			'show_bullets' => 'true',
			'animation_speed' => '4000',

			'total_items' => '-1',
			'selected_category' => '',
			'order_by' => 'date',
			'order' => 'DESC',

			'gutter' => 'gutter',
			'gallery_hover_effect' => '',
			'gallery_overlay_color_on_image' => '',
			'show_gradient_effect_on_image' => 'false',
			'show_gallery_filter' => 'false',
			'show_gallery_title' => 'true',
			'show_gallery_overlay_zoomlink_icons' => 'true',
			'show_gallery_description'	=> 'false',
			'show_gallery_date' => 'false',
			'show_selected_category' => 'true'
		);
		$params = shortcode_atts($args, $attr);

		//query args
		$args = array(
			'post_type' => $new_cpt_class->ptKey,
			'orderby' => $params['order_by'],
			'order' => $params['order'],
			'posts_per_page' => $params['total_items'],
		);

		//if category selected
		if( $params['selected_category'] ) {
			$cat_array = explode(',', $params['selected_category']);
			$args['tax_query'] = array(
				array(
					'taxonomy' => $new_cpt_class->ptTaxKey,
					'field'	=> 'slug',
					'terms'	=> $cat_array,
				)
			);
		}

		$the_query = new \WP_Query( $args );
		$params['the_query'] = $the_query;
		
		//ptTaxKey
		$params['ptTaxKey'] = $new_cpt_class->ptTaxKey;

		//get image dimension
		$params['image_dimension'] = medicale_mascot_get_image_dimensions( $params['gallery_image_orientation'], $params['gallery_image_aspect_ratio'] );

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, shortcode_ob_start)
		$html = medicale_mascot_get_cpt_template_part( 'gallery', $params['gallery_type'], 'gallery/shortcodes/tpl', $params, true );
		
		return $html;
	}
}