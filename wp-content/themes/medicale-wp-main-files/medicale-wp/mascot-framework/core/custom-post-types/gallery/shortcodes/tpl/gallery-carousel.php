
<?php if ( $the_query->have_posts() ) : ?>
	<div class="<?php echo $custom_css_class;?>">
		<div class="owl-carousel owl-theme owl-carousel-<?php echo $columns;?>col" <?php if ( $show_navigation == 'true' ) : ?> data-nav="<?php echo $show_navigation;?>"<?php endif; ?> <?php if ( $show_bullets == 'true' ) : ?> data-dots="<?php echo $show_bullets;?>"<?php endif; ?> <?php if ( $animation_speed != '' ) : ?> data-duration="<?php echo $animation_speed;?>"<?php endif; ?>>
			<!-- the loop -->
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<?php
				$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
				$full_image_url = $full_image_url[0];

				$gallery_images = rwmb_meta( 'medicale_mascot_' . "cpt_gallery_images", 'size=full' );
				//if has no post thumbnail
				if( !has_post_thumbnail( get_the_ID() ) ) {
					if ( !empty( $gallery_images ) ) {
						$first_image_key = key($gallery_images);
						$full_image_url = $gallery_images[$first_image_key]['url'];
					}
				}

				//create resized image
				$resized_image = medicale_mascot_matthewruddy_image_resize( $full_image_url, $image_dimension['width'], $image_dimension['height'], true );

				// if has no post thubnail or gallery images then add placeholder image
				if( !is_array( $resized_image ) || !$resized_image['url'] || $resized_image['url'] == '' ) {
					$resized_image = array();
					$full_image_url = $resized_image['url'] = "http://placehold.it/".$image_dimension['width']."x".$image_dimension['height']."?text=Image Not Found!";
				}

				$term_slugs_list = wp_get_post_terms( get_the_ID(), $ptTaxKey, array("fields" => "slugs") );
				$term_slugs_list_string = implode( $term_slugs_list, ' ' );
				$term_names_list = wp_get_post_terms( get_the_ID(), $ptTaxKey, array("fields" => "names") );
				$term_names_list_string = implode( $term_names_list, ', ' );
			?>
			<!-- Portfolio Item Start -->
			<div class="box-hover-effect <?php echo $gallery_hover_effect;?> <?php echo $term_slugs_list_string;?>">
				<div class="effect-wrapper">
					<div class="thumb <?php echo ( $show_gradient_effect_on_image == 'true' ) ? 'gradient-effect' : '';?>">
						<img src="<?php echo $resized_image['url'];?>" alt="<?php the_title();?>" width="<?php echo $image_dimension['width'];?>">
						<div class="hidden">
							<?php
							if ( !empty( $gallery_images ) ) {
								foreach ( $gallery_images as $each_gallery_image ) {
								?>
								<a data-rel="prettyPhoto[gallery<?php echo get_the_ID();?>]" title="<?php echo $each_gallery_image['alt'];?>" href="<?php echo $each_gallery_image['url'];?>"></a>
								<?php
								}
							}
							?>
						</div>
					</div>
					<div class="overlay-shade <?php echo $gallery_overlay_color_on_image;?>"></div>
					<div class="text-holder">
					<?php if ( $show_gallery_title == 'true' ) : ?>
					<div class="title"><?php the_title();?></div>
					<?php endif; ?>
					<?php if ( $show_gallery_description == 'true' ) : ?>
					<div class="description"><?php the_excerpt();?></div>
					<?php endif; ?>
					<?php if ( $show_selected_category == 'true' ) : ?>
					<div class="category"><?php echo $term_names_list_string;?></div>
					<?php endif; ?>
					<?php if ( $show_gallery_date == 'true' ) : ?>
					<div class="date"><?php echo get_the_date(); ?></div>
					<?php endif; ?>
					</div>
					<?php if ( $show_gallery_overlay_zoomlink_icons == 'true' ) : ?>
					<div class="icons-holder icons-holder-middle">
						<div class="icons-holder-inner">
							<div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
								<a data-rel="prettyPhoto[gallery<?php echo get_the_ID();?>]" title="<?php the_title();?>" href="<?php echo $full_image_url;?>"><i class="fa fa-link"></i></a>
							</div>
						</div>
					</div>
					<?php endif; ?>
					<a data-rel="prettyPhoto[gallery<?php echo get_the_ID();?>]" title="<?php the_title();?>" href="<?php echo $full_image_url;?>" class="hover-link"></a>
				</div>
			</div>
			<!-- Portfolio Item End -->
			<?php endwhile; ?>
			<!-- end of the loop -->
		</div>
	</div>
  <?php wp_reset_postdata(); ?>

<?php else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'medicale-wp' ); ?></p>
<?php endif; ?>