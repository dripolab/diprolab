<section>
  <div class="<?php echo $container_type; ?>">
  	<div class="main-content-area">
		<?php
			if ( have_posts() ) :
			// Start the Loop.
			while ( have_posts() ) : the_post();
				medicale_mascot_get_single_post_title();
				the_content();
			endwhile;
			endif;
		?>

		<!-- Portfolio Gallery Grid -->
		<div id="grid" class="gallery-isotope masonry grid-3 gutter clearfix">
			<div class="gallery-item gallery-item-sizer"></div>
			<!-- the loop -->
			<?php  while ( have_posts() ) : the_post(); ?>
			<?php
				$image_dimension = medicale_mascot_get_image_dimensions( 'landscape', '16:9' );
				$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
				$full_image_url = $full_image_url[0];

				$gallery_images = rwmb_meta( 'medicale_mascot_' . "cpt_gallery_images", 'size=full' );
				//if has no post thumbnail
				if( !has_post_thumbnail( get_the_ID() ) ) {
					if ( !empty( $gallery_images ) ) {
						$first_image_key = key($gallery_images);
						$full_image_url = $gallery_images[$first_image_key]['url'];
					}
				}


				// if has no post thubnail or gallery images then add placeholder image
				if( $full_image_url == '' ) {
					$full_image_url = "http://placehold.it/".$image_dimension['width']."x".$image_dimension['height']."?text=Image Not Found!";
				}
			?>
			<!-- Portfolio Item Start -->
			<div class="gallery-item box-hover-effect">
				<div class="effect-wrapper">
					<div class="thumb">
						<img src="<?php echo $full_image_url;?>" alt="<?php the_title();?>">
					</div>
					<div class="overlay-shade"></div>
					<div class="icons-holder icons-holder-middle">
						<div class="icons-holder-inner">
							<a data-rel="prettyPhoto[gallery<?php echo get_the_ID();?>]" title="<?php the_title();?>" href="<?php echo $full_image_url;?>"><i class="pe-7s-plus font-40 text-white"></i></a>
						</div>
					</div>
				</div>
			</div>
			<!-- Portfolio Item End -->

			<?php
				if ( !empty( $gallery_images ) ) {
				foreach ( $gallery_images as $each_gallery_image ) {
			?>
			<!-- Portfolio Item Start -->
			<div class="gallery-item box-hover-effect">
				<div class="effect-wrapper">
					<div class="thumb">
						<img src="<?php echo $each_gallery_image['url'];?>" alt="<?php the_title();?>" width="<?php echo $image_dimension['width'];?>">
					</div>
					<div class="overlay-shade"></div>
					<div class="icons-holder icons-holder-middle">
						<div class="icons-holder-inner">
							<a data-rel="prettyPhoto[gallery<?php echo get_the_ID();?>]" title="<?php the_title();?>" href="<?php echo $each_gallery_image['url'];?>"><i class="pe-7s-plus font-40 text-white"></i></a>
						</div>
					</div>
				</div>
			</div>
			<!-- Portfolio Item End -->
			<?php 
					}
				}
				endwhile;
			?>
			<!-- end of the loop -->
		</div>
		<!-- End Portfolio Gallery Grid -->

	<?php
		if( medicale_mascot_get_redux_option( 'cpt-settings-faq-show-related-posts', true ) ) {
			$posts_count = medicale_mascot_get_redux_option( 'cpt-settings-faq-show-related-posts-count', 3 );
			echo '<hr class="hr-divider">';
			medicale_mascot_get_blog_single_related_posts( medicale_mascot_get_page_id(), $posts_count );
		}
	?>
	</div>
  </div>
</section> 