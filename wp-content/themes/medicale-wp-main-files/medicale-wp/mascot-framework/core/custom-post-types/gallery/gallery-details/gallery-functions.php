<?php


if(!function_exists('medicale_mascot_get_gallery_single')) {
	/**
	 * Function that Renders gallery Details HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_gallery_single( $container_type = 'container' ) {
		$params = array();

		$params['container_type'] = $container_type;

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_cpt_template_part( 'gallery-parts', null, 'gallery/gallery-details/tpl', $params, false );
		
		return $html;
	}
}
