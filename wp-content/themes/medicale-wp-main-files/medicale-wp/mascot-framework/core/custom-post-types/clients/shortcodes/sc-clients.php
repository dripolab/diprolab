<?php

if(!function_exists('mascot_core_cpt_sc_clients_vc_map_modifier')) {
	/**
	 * Shortcodes vc_map modifier
	 */
	function mascot_core_cpt_sc_clients_vc_map_modifier( $shortcode_base ) {
		//vc_remove_param( $shortcode_base, "display_type" );
		//vc_add_param( $shortcode_base, $attributes );
		//vc_add_params( $shortcode_base, $attributes );
	}
}



if(!function_exists('mascot_core_cpt_sc_clients_render')) {
	/**
	 * Renders shortcodes HTML
	 */
	function mascot_core_cpt_sc_clients_render( $attr, $content = null, $class_instance ) {

		$new_cpt_class = $class_instance;
		$args = array(
			'custom_css_class' => '',
			'clients_type' => '',
			'columns' => '6',

			'show_navigation' => 'true',
			'show_bullets' => 'true',
			'animation_speed' => '4000',

			'total_items' => '-1',
			'selected_category' => '',
			'order_by' => 'date',
			'order' => 'DESC',

			'show_client_name' => 'false',
			'show_client_description'	=> 'false',
			'show_client_logo' => 'true',
			'link_client_url' => 'true',
			'open_links_in' => '_blank',
		);
		$params = shortcode_atts($args, $attr);

		//query args
		$args = array(
			'post_type' => $new_cpt_class->ptKey,
			'orderby' => $params['order_by'],
			'order' => $params['order'],
			'posts_per_page' => $params['total_items'],
		);

		//if category selected
		if( $params['selected_category'] ) {
			$cat_array = explode(',', $params['selected_category']);
			$args['tax_query'] = array(
				array(
					'taxonomy' => $new_cpt_class->ptTaxKey,
					'field'	=> 'slug',
					'terms'	=> $cat_array,
				)
			);
		}

		//if order by client_author_name selected
		if( $params['order_by'] == 'client_name' ) {
			$args['meta_key'] = 'medicale_mascot_' . 'client_name';
			$args['orderby']  = 'meta_value';
		}

		$the_query = new \WP_Query( $args );
		$params['the_query'] = $the_query;

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, shortcode_ob_start)
		$html = medicale_mascot_get_cpt_template_part( 'clients', $params['clients_type'], 'clients/shortcodes/tpl', $params, true );
		
		return $html;
	}
}