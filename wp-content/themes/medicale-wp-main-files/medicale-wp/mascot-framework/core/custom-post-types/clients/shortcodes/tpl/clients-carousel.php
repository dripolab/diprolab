
<?php if ( $the_query->have_posts() ) : ?>
  <div class="<?php echo $custom_css_class;?>">
	<div class="owl-carousel owl-theme owl-carousel-<?php echo $columns;?>col" <?php if ( $show_navigation == 'true' ) : ?> data-nav="<?php echo $show_navigation;?>"<?php endif; ?> <?php if ( $show_bullets == 'true' ) : ?> data-dots="<?php echo $show_bullets;?>"<?php endif; ?> <?php if ( $animation_speed != '' ) : ?> data-duration="<?php echo $animation_speed;?>"<?php endif; ?>>
		<!-- the loop -->
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<div class="item">
			<?php if ( $show_client_logo == 'true' ) : ?>
			<?php if ( $link_client_url == 'true' ) : ?>
			<a target="<?php echo $open_links_in;?>" href="<?php echo ( rwmb_meta( 'medicale_mascot_' . "cpt_client_url" ) == '' ) ? '#' : rwmb_meta( 'medicale_mascot_' . "cpt_client_url" );?>">
			<?php endif; ?>
			<?php
			$images = rwmb_meta( 'medicale_mascot_' . "cpt_client_logo" );
			if ( !empty( $images ) ) {
				foreach ( $images as $image ) {
				echo "<img src='{$image['url']}' alt='{$image['alt']}' />";
				}
			}
			?>
			<?php if ( $link_client_url == 'true' ) : ?>
			</a>
			<?php endif; ?>
			<?php endif; ?>
			<?php if ( $show_client_name == 'true' ) : ?>
			<h6 class="client-name"><?php echo rwmb_meta( 'medicale_mascot_' . "cpt_client_name" );?></h6>
			<?php endif; ?>
			<?php if ( $show_client_description == 'true' ) : ?>
			<p class="client-description"><?php echo rwmb_meta( 'medicale_mascot_' . "cpt_client_description" );?></p>
			<?php endif; ?>
		</div>
		<?php endwhile; ?>
		<!-- end of the loop -->
	</div>
  </div>
  <?php wp_reset_postdata(); ?>

<?php else : ?>
  <p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'medicale-wp' ); ?></p>
<?php endif; ?>