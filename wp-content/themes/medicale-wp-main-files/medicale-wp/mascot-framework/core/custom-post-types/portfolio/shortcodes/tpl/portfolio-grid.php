<?php if ( $the_query->have_posts() ) : ?>
  
  <?php if ( $show_portfolio_filter == 'true' ) : ?>
  <?php
	$portfolio_filters = array();
	while ( $the_query->have_posts() ) : 
		$the_query->the_post();
		//Returns All Term Items for $ptTaxKey
		$term_list = wp_get_post_terms(get_the_ID(), $ptTaxKey, array("fields" => "all"));
		if ( !empty ( $term_list) ) {
			foreach($term_list as $term) {
				$portfolio_filters[$term->slug] = $term->name;
			}
		}
	endwhile;
	wp_reset_postdata();
	$portfolio_filters = array_unique($portfolio_filters);
  ?>
  <!-- Portfolio Filter -->
  <div class="gallery-isotope-filter style-boxed" data-link-with="<?php echo $holder_id ?>">
	<a href="#" class="active" data-filter="*">All</a>
	<?php if ( !empty($portfolio_filters) ) { foreach ( $portfolio_filters as $slug=>$name ) { ?>
	<a href="#<?php echo $slug;?>" class="" data-filter=".<?php echo $slug;?>"><?php echo $name;?></a>
	<?php } } ?>
  </div>
  <!-- End Portfolio Filter -->
  <?php endif; ?>

  <!-- Portfolio Grid -->
  <div id="<?php echo $holder_id ?>" class="gallery-isotope grid-<?php echo $columns;?> <?php echo $gutter;?> clearfix <?php echo $custom_css_class;?>">
	<!-- the loop -->
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	<?php
		$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
		$full_image_url = $full_image_url[0];

		$portfolio_images = rwmb_meta( 'medicale_mascot_' . "portfolio_metabox_portfolio_gallery_images", 'size=full' );
		//if has no post thumbnail
		if( !has_post_thumbnail( get_the_ID() ) ) {
			if ( !empty( $portfolio_images ) ) {
				$first_image_key = key($portfolio_images);
				echo $full_image_url = $portfolio_images[$first_image_key]['url'];
			}
		}

		//create resized image
		$resized_image = medicale_mascot_matthewruddy_image_resize( $full_image_url, $image_dimension['width'], $image_dimension['height'], true );

		// if has no post thubnail or portfolio images then add placeholder image
		if( !is_array( $resized_image ) || !$resized_image['url'] || $resized_image['url'] == '' ) {
			$resized_image = array();
			$full_image_url = $resized_image['url'] = "http://placehold.it/".$image_dimension['width']."x".$image_dimension['height']."?text=Image Not Found!";
		}

		$term_slugs_list = wp_get_post_terms( get_the_ID(), $ptTaxKey, array("fields" => "slugs") );
		$term_slugs_list_string = implode( $term_slugs_list, ' ' );
		$term_names_list = wp_get_post_terms( get_the_ID(), $ptTaxKey, array("fields" => "names") );
		$term_names_list_string = implode( $term_names_list, ', ' );
	?>
	<!-- Portfolio Item Start -->
	<div class="gallery-item box-hover-effect <?php echo $portfolio_hover_effect;?> <?php echo $term_slugs_list_string;?>">
		<div class="effect-wrapper">
			<div class="thumb <?php echo ( $show_gradient_effect_on_image == 'true' ) ? 'gradient-effect' : '';?>">
				<img src="<?php echo $resized_image['url'];?>" alt="<?php the_title();?>" width="<?php echo $image_dimension['width'];?>">
			</div>
			<div class="overlay-shade <?php echo $portfolio_overlay_color_on_image;?>"></div>
			<div class="text-holder">
				<?php if ( $show_portfolio_title == 'true' ) : ?>
				<div class="title"><?php the_title();?></div>
				<?php endif; ?>
				<?php if ( $show_portfolio_description == 'true' ) : ?>
				<div class="description"><?php the_excerpt();?></div>
				<?php endif; ?>
				<?php if ( $show_selected_category == 'true' ) : ?>
				<div class="category"><?php echo $term_names_list_string;?></div>
				<?php endif; ?>
				<?php if ( $show_portfolio_date == 'true' ) : ?>
				<div class="date"><?php echo get_the_date(); ?></div>
				<?php endif; ?>
			</div>
			<?php if ( $show_portfolio_overlay_zoomlink_icons == 'true' ) : ?>
			<div class="icons-holder icons-holder-middle">
				<div class="icons-holder-inner">
					<div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
						<a title="<?php the_title();?>" href="<?php the_permalink();?>"><i class="fa fa-link"></i></a>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<a title="<?php the_title();?>" href="<?php the_permalink();?>" class="hover-link"></a>
		</div>
	</div>
	<!-- Portfolio Item End -->
	<?php endwhile; ?>
	<!-- end of the loop -->
  </div>
  <!-- End Portfolio Grid -->
  <?php wp_reset_postdata(); ?>

<?php else : ?>
  <p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'medicale-wp' ); ?></p>
<?php endif; ?>