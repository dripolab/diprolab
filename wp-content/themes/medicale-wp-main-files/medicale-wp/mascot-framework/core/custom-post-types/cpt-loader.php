<?php

/* Loads all CPT shortcodes located in custom-post-types folder
================================================== */
if( !function_exists('medicale_mascot_load_all_cpt_shortcodes') ) {
	function medicale_mascot_load_all_cpt_shortcodes() {
		foreach( glob( MASCOT_FRAMEWORK_DIR.'/core/custom-post-types/*/loader.php' ) as $each_cpt_sc_loader ) {
			require_once $each_cpt_sc_loader;
		}
	}
	add_action('medicale_mascot_before_custom_action', 'medicale_mascot_load_all_cpt_shortcodes');
}
