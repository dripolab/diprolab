
		<div class="departments-item">
			<?php if ( $show_department_thumb == 'true' ) : ?>
			<a href="<?php the_permalink();?>">
			<?php echo get_the_post_thumbnail( get_the_ID(), $feature_thumb_image_size, array( 'class' => 'img-fullwidth' ) );?>
			</a>
			<?php endif; ?>
			<div class="details">
			<?php if ( $show_department_title == 'true' ) : ?>
			<h4 class="title">
				<a href="<?php the_permalink();?>">
				<?php the_title();?>
				</a>
			</h4>
			<?php endif; ?>
			<?php if ( $show_department_description == 'true' ) : ?>
			<p><?php echo wp_trim_words( strip_shortcodes( get_the_excerpt() ), $department_description_excerpt_length, '...' )?></p>
			<?php endif; ?>
			<?php if ( $show_department_read_more_button == 'true' ) : ?>
			<a href="<?php the_permalink();?>" class="btn-read-more">Read more</a>
			<?php endif; ?>
			</div>
		</div>