<div class="thumb">
<?php if ( has_post_thumbnail() ) { ?>
	<?php the_post_thumbnail( 'medicale_mascot_featured_image' ); ?>
<?php } ?>
</div>
<?php
	the_content();
?>