<?php


if(!function_exists('medicale_mascot_get_departments_details')) {
	/**
	 * Function that Renders departments Details HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_departments_details( $container_type = 'container' ) {
		$params = array();

		$params['container_type'] = $container_type;

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_cpt_template_part( 'departments-parts', null, 'departments/departments-details/tpl', $params, false );
		
		return $html;
	}
}

if (!function_exists('medicale_mascot_get_departments_sidebar_layout')) {
	/**
	 * Return departments Layout HTML
	 */
	function medicale_mascot_get_departments_sidebar_layout( $page_layout = null ) {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();

		$params['page_layout'] = 'layout1';

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_cpt_template_part( 'departments-layout1', null, 'departments/departments-details/tpl/layout', $params, false );
		
		return $html;
	}
}

if ( ! function_exists( 'medicale_mascot_get_departments_layout1_sidebar' ) ) {
	/**
	 * Returns departments Sidebar
	 *
	 */
	function medicale_mascot_get_departments_layout1_sidebar() {
		$params = array();
		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_cpt_template_part( 'departments-layout1-sidebar', null, 'departments/departments-details/tpl/parts', $params, false );
		
		return $html;
	}
}

if ( ! function_exists( 'medicale_mascot_get_departments_layout1_content' ) ) {
	/**
	 * Returns departments Content
	 *
	 */
	function medicale_mascot_get_departments_layout1_content() {
		$params = array();
		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_cpt_template_part( 'departments-layout1-content', null, 'departments/departments-details/tpl/parts', $params, false );
		
		return $html;
	}
}