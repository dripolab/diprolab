<?php
$current_page_id = medicale_mascot_get_page_id();
use MASCOTCORE\CPT\Departments\CPT_Departments;
if( class_exists('MASCOTCORE\CPT\Departments\CPT_Departments') ) {
	$departments_cpt_class = CPT_Departments::Instance();
	$query = new WP_Query( array( 'post_type' => $departments_cpt_class->ptKey ) );
?>
<div class="list-group list-departments">
  <div class="list-group-item list-group-item-heading-parent active">
	<h4 class="list-group-item-heading list-group-item-title"><?php esc_html_e( 'Departments', 'medicale-wp' ) ?></h4>
  </div>
<?php
	while ( $query->have_posts() ) :
		$query->the_post();
?>
  <a href="<?php the_permalink(); ?>" class="list-group-item <?php if( $current_page_id == get_the_ID() ) echo "active" ?>"><?php the_title(); ?></a>
<?php
	endwhile;
	wp_reset_postdata();
?>
</div>
<?php
}
?>