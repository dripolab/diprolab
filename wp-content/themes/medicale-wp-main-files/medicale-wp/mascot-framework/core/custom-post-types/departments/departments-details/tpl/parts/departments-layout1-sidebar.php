<?php
$current_page_id = medicale_mascot_get_page_id();
?>

<?php medicale_mascot_get_cpt_template_part( 'list-departments', null, 'departments/departments-details/tpl/parts', $params, false );?>


<ul class="list-group">
  <li class="list-group-item active">
	<h4 class="list-group-item-heading list-group-item-title"><?php esc_html_e( 'Contacts', 'medicale-wp' ) ?></h4>
  </li>

  <?php if ( !empty( rwmb_meta( 'medicale_mascot_' . "cpt_department_address", '', $current_page_id ) ) ) { ?>
  <li class="list-group-item">
	<h5 class="list-group-item-heading"><?php esc_html_e( 'Address', 'medicale-wp' ) ?></h5>
	<p class="list-group-item-text"><?php echo $department_address = rwmb_meta( 'medicale_mascot_' . "cpt_department_address", '', $current_page_id ) ?></p>
	
	<a class="btn btn-dark btn-theme-colored btn-sm mt-10" target="_blank" href="<?php echo $department_address = "https://www.google.com/maps/place/" . str_replace(' ', '+', $department_address); ?>"><?php esc_html_e( 'Get Direction', 'medicale-wp' ) ?> <i class="fa fa-arrow-circle-right"></i></a>
  </li>
  <?php } ?>

  <?php if ( !empty( rwmb_meta( 'medicale_mascot_' . "cpt_department_phone", '', $current_page_id ) ) ) { ?>
  <li class="list-group-item">
	<h5 class="list-group-item-heading"><?php esc_html_e( 'Phone', 'medicale-wp' ) ?></h5>
	<p class="list-group-item-text"><i class="fa fa-phone"></i> <?php echo rwmb_meta( 'medicale_mascot_' . "cpt_department_phone", '', $current_page_id ) ?></p>
  </li>
  <?php } ?>

  <?php if ( !empty( rwmb_meta( 'medicale_mascot_' . "cpt_department_fax", '', $current_page_id ) ) ) { ?>
  <li class="list-group-item">
	<h5 class="list-group-item-heading">Fax</h5>
	<p class="list-group-item-text"><i class="fa fa-fax"></i> <?php echo rwmb_meta( 'medicale_mascot_' . "cpt_department_fax", '', $current_page_id ) ?></p>
  </li>
  <?php } ?>

  <?php if ( !empty( rwmb_meta( 'medicale_mascot_' . "cpt_department_email", '', $current_page_id ) ) ) { ?>
  <li class="list-group-item">
	<h5 class="list-group-item-heading"><?php esc_html_e( 'Email', 'medicale-wp' ) ?></h5>
	<p class="list-group-item-text"><i class="fa fa-envelope-o"></i> <?php echo rwmb_meta( 'medicale_mascot_' . "cpt_department_email", '', $current_page_id ) ?></p>
  </li>
  <?php } ?>
</ul>

<?php
//working hours
$working_hours_array = array();
for ($i=1; $i <= 7 ; $i++) { 
  $day = rwmb_meta( 'medicale_mascot_' . 'cpt_department_opening_hours_day_'.$i, '', $current_page_id );
  $day_time = rwmb_meta( 'medicale_mascot_' . 'cpt_department_opening_hours_day_'.$i.'_time', '', $current_page_id );
  if( !empty( $day ) ) :
	$working_hours_array[$day] = $day_time;
  endif;
}
if( !empty( $working_hours_array ) ) :
?>
<ul class="list-group">
  <li class="list-group-item active">
	<h4 class="list-group-item-heading list-group-item-title"><?php esc_html_e( 'Working Hours', 'medicale-wp' ) ?></h4>
  </li>
  <li class="list-group-item">
	<div class="list-group-item-text">
  		<ul class="opening-hours">
  			<?php 
			foreach( $working_hours_array as $day => $day_time ) {
  			?>
					<li class="clearfix">
						<span><?php echo $day; ?></span>
						<div class="value"><?php echo $day_time; ?></div>
					</li>
  			<?php 
  				}
  			?>
			</ul>
	</div>
  </li>
</ul>
<?php
endif;
?>