<section>
  <div class="<?php echo $container_type; ?>">
  	<div class="main-content-area">
	<?php
		if ( have_posts() ) :
		// Start the Loop.
		while ( have_posts() ) : the_post();
			medicale_mascot_get_departments_sidebar_layout();
		endwhile;
		endif;
	?>
	</div>
  </div>
</section> 