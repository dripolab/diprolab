<div id="departments-<?php the_ID(); ?>" <?php post_class( 'departments-single-wrapper' ); ?>>
  <div class="row">
	<div class="col-md-4">
		<div class="departments-sidebar">
		<?php
		medicale_mascot_get_departments_layout1_sidebar();
		?>
		
		</div>
	</div>
	<div class="col-md-8">
		<div class="departments-content">
		<?php
		medicale_mascot_get_departments_layout1_content();
		?>
		</div>
	</div>
  </div>
</div>