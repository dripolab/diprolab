<?php

//load Departments CPT Shortcodes
require_once MASCOT_FRAMEWORK_DIR . '/core/custom-post-types/departments/shortcodes/sc-departments.php';
//load Departments Single Page
require_once MASCOT_FRAMEWORK_DIR . '/core/custom-post-types/departments/departments-details/loader.php';