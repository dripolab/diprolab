<?php
  $random_number = wp_rand( 1111, 9999 );
?>
<?php if ( $the_query->have_posts() ) : ?>
  <div class="services-tab border-10px bg-white <?php echo $custom_css_class;?>">
	<ul class="nav nav-tabs">
		<?php $iter = 0; while ( $the_query->have_posts() ) : $the_query->the_post(); $iter++;?>
		<li class="<?php echo ( $iter == 1) ? 'active': '';?>">
		<a href="#tab<?php echo '-'.$random_number.'-'.get_the_ID();?>" data-toggle="tab">
			<?php if ( $show_department_icon == 'true' ) : ?>
			<?php 
			$image_or_icon_html = '';
			$choose_image_or_icon = rwmb_meta( 'medicale_mascot_' . "cpt_department_tab_choose_image_or_icon" );
			if( $choose_image_or_icon == 'image' ) {
				$tab_image = rwmb_meta( 'medicale_mascot_' . "cpt_department_tab_image" );
				if ( !empty( $tab_image ) ) {
				$img_url = medicale_mascot_metabox_get_image_advanced_field_url( $tab_image );
				$image_or_icon_html = '<img src="'.$img_url.'" alt=""></br>';
				}
			}elseif( $choose_image_or_icon == 'icon' ) {
				$choose_iconpack = rwmb_meta( 'medicale_mascot_' . "cpt_department_tab_iconpack" );
				if( $choose_iconpack != '' ) {
					$icon_name = rwmb_meta( 'medicale_mascot_' . "cpt_department_tab_iconpack" . '_' . $choose_iconpack );
					$image_or_icon_html = '<i class="'.$icon_name.'"></i>';
				}
			}
			?>
			<?php echo $image_or_icon_html; ?>
			<?php endif; ?>
			<?php if ( $show_department_title == 'true' ) : ?>
			<span><?php the_title();?></span>
			<?php endif; ?>
		</a>
		</li>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	</ul>
	<div class="tab-content">
		<?php $iter = 0; while ( $the_query->have_posts() ) : $the_query->the_post(); $iter++;?>
		<div class="tab-pane fade <?php echo ( $iter == 1) ? 'in active': '';?>" id="tab<?php echo '-'.$random_number.'-'.get_the_ID();?>">
		<div class="row">
			<div class="col-md-5">
			<?php if ( $show_department_thumb == 'true' ) : ?>
			<div class="thumb">
				<?php echo get_the_post_thumbnail( get_the_ID(), $feature_thumb_image_size, array( 'class' => 'img-fullwidth' ) );?>
			</div>
			<?php endif; ?>
			</div>
			<div class="col-md-6">
			<div class="service-excerpt">
				<h3 class="sub-title mb-0 mt-15">Speciality</h3>
				<?php if ( $show_department_title == 'true' ) : ?>
				<h1 class="title mt-0"><?php the_title();?></h1>
				<?php endif; ?>
				<?php if ( $show_department_description == 'true' ) : ?>
				<?php the_excerpt();?>
				<?php endif; ?>
			</div>
			<?php if ( $show_department_read_more_button == 'true' ) : ?>
			<a href="<?php the_permalink();?>" class="btn btn-dark btn-theme-colored">Read more</a>
			<?php endif; ?>
			</div>
		</div>
		</div>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	</div>
  </div>

<?php else : ?>
  <p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'medicale-wp' ); ?></p>
<?php endif; ?>