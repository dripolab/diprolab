<?php


if(!function_exists('medicale_mascot_get_features_details')) {
	/**
	 * Function that Renders features Details HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_features_details( $container_type = 'container' ) {
		$params = array();

		$params['container_type'] = $container_type;

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_cpt_template_part( 'features-parts', null, 'features/features-details/tpl', $params, false );
		
		return $html;
	}
}
