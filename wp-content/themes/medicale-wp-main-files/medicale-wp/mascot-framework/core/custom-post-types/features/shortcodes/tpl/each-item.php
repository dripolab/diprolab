
		<div class="features-item">
			<?php if ( $show_feature_thumb == 'true' ) : ?>
			<a href="<?php the_permalink();?>">
				<?php echo get_the_post_thumbnail( get_the_ID(), $feature_thumb_image_size, array( 'class' => 'img-fullwidth' ) );?>
			</a>
			<?php endif; ?>
			<div class="details">
				<?php if ( $show_feature_title == 'true' ) : ?>
				<h4 class="title line-bottom">
					<a href="<?php the_permalink();?>">
					<?php the_title();?>
					</a>
				</h4>
				<?php endif; ?>
				<?php if ( $show_feature_description == 'true' ) : ?>
				<?php if ( has_excerpt() ) { echo get_the_excerpt(); } else { ?>
				<p><?php echo wp_trim_words( strip_shortcodes( get_the_content() ), $excerpt_length, '...' )?></p>
				<?php } ?>
				<?php endif; ?>
				<?php if ( $show_features_read_more_button == 'true' ) : ?>
				<a href="<?php the_permalink();?>" class="btn-read-more">Read more</a>
				<?php endif; ?>
			</div>
		</div>