
			<div class="team-member">
				<div class="box-hover-effect">
					<div class="effect-wrapper">
						<div class="thumb">
							<?php echo get_the_post_thumbnail( get_the_ID(), $feature_thumb_image_size, array( 'class' => 'img-fullwidth' ) );?>
						</div>
						<div class="overlay-shade shade-white"></div>
						<?php if ( $show_social_link == 'true' ) : ?>
						<div class="icons-holder icons-holder-middle">
							<div class="icons-holder-inner">
								<ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
									<?php
									foreach ($social_list_array as $key => $value) {
										$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "cpt_staff_social_".$key );
										if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) ) {
										echo '<li><a target="_blank" href="'.$temp_meta_value.'"><i class="fa fa-'.$key.'"></i></a></li>';
										}
									} 
									?>
									
								</ul>
							</div>
						</div>
						<?php endif; ?>
					</div>
				
					<div class="details">
					<?php if ( $show_name == 'true' ) : ?>
					<h4 class="title"><a href="<?php the_permalink();?>"><?php echo rwmb_meta( 'medicale_mascot_' . "cpt_staff_name" );?></a></h4>
					<?php endif; ?>
					<?php if ( $show_position == 'true' ) : ?>
					<h6 class="sub-title"><?php echo rwmb_meta( 'medicale_mascot_' . "cpt_staff_position" );?></h6>
					<?php endif; ?>
					<?php if ( $show_short_bio == 'true' ) : ?>
					<p class="short-bio"><?php echo wp_trim_words( strip_shortcodes( rwmb_meta( 'medicale_mascot_' . "cpt_staff_short_bio" ) ), $excerpt_length, '...' )?></p>
					<?php endif; ?>

					<ul class="team-contact-info">
						<?php if( !empty( rwmb_meta( 'medicale_mascot_' . "cpt_staff_phone" ) ) ): ?>
						<li><i class="fa fa-phone"></i> <?php echo rwmb_meta( 'medicale_mascot_' . "cpt_staff_phone" );?></li>
						<?php endif; ?>
						<?php if( !empty( rwmb_meta( 'medicale_mascot_' . "cpt_staff_email" ) ) ): ?>
						<li><i class="fa fa-envelope-o"></i> <?php echo rwmb_meta( 'medicale_mascot_' . "cpt_staff_email" );?></li>
						<?php endif; ?>
					</ul>
					</div>
				
					<?php if ( $show_view_details_button == 'true' ) : ?>
					<a class="btn btn-theme-colored btn-sm" href="<?php the_permalink();?>">view details</a>
					<?php endif; ?>
				</div>
			</div>