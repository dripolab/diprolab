
<?php if ( $the_query->have_posts() ) : ?>
  <div class="<?php echo $custom_css_class;?>">
	<div class="owl-carousel owl-theme owl-carousel-<?php echo $columns;?>col owl-dots-bottom-right" <?php if ( $show_navigation == 'true' ) : ?> data-nav="<?php echo $show_navigation;?>"<?php endif; ?> <?php if ( $show_bullets == 'true' ) : ?> data-dots="<?php echo $show_bullets;?>"<?php endif; ?> <?php if ( $animation_speed != '' ) : ?> data-duration="<?php echo $animation_speed;?>"<?php endif; ?>>
		<!-- the loop -->
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<div class="item">
			<div class="team-member">
				<div class="row-fluid">
					<div class="col-md-5">
						<div class="thumb">
							<?php echo get_the_post_thumbnail( get_the_ID(), $feature_thumb_image_size, array( 'class' => 'img-fullwidth' ) );?>
							<?php

							$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
							$full_image_url = $full_image_url[0];
							/* $image_dimension = array();
							$image_dimension['width'] = 280;
							$image_dimension['height'] = 390;

							//create resized image
							$resized_image = medicale_mascot_matthewruddy_image_resize( $full_image_url, $image_dimension['width'], $image_dimension['height'], true );

							// if has no post thubnail or gallery images then add placeholder image
							if( !is_array( $resized_image ) || !$resized_image['url'] || $resized_image['url'] == '' ) {
								$resized_image = array();
								$resized_image['url'] = "http://placehold.it/".$image_dimension['width']."x".$image_dimension['height']."?text=Image Not Found!";
							}*/
							?>
						</div>
					</div>
					<div class="col-md-7">
						<div class="details">
							<?php if ( $show_position == 'true' ) : ?>
							<h6 class="sub-title mb-0"><?php echo rwmb_meta( 'medicale_mascot_' . "cpt_staff_position" );?></h6>
							<?php endif; ?>
							<?php if ( $show_name == 'true' ) : ?>
							<h4 class="title mt-0"><a href="<?php the_permalink();?>"><?php echo rwmb_meta( 'medicale_mascot_' . "cpt_staff_name" );?></a></h4>
							<?php endif; ?>
							<?php if ( $show_short_bio == 'true' ) : ?>
							<p class="short-bio"><?php echo rwmb_meta( 'medicale_mascot_' . "cpt_staff_short_bio" );?></p>
							<?php endif; ?>

							<ul class="team-contact-info">
							<?php if( !empty( rwmb_meta( 'medicale_mascot_' . "cpt_staff_phone" ) ) ): ?>
							<li><i class="fa fa-phone"></i> <?php echo rwmb_meta( 'medicale_mascot_' . "cpt_staff_phone" );?></li>
							<?php endif; ?>
							<?php if( !empty( rwmb_meta( 'medicale_mascot_' . "cpt_staff_email" ) ) ): ?>
							<li><i class="fa fa-envelope-o"></i> <?php echo rwmb_meta( 'medicale_mascot_' . "cpt_staff_email" );?></li>
							<?php endif; ?>
							</ul>
						</div>


					<?php if ( $show_view_details_button == 'true' ) : ?>
					<a class="btn-read-more mt-15" href="<?php the_permalink();?>">View Details</a>
					<?php endif; ?>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<?php endwhile; ?>
		<!-- end of the loop -->
	</div>
  </div>
  <?php wp_reset_postdata(); ?>


<?php else : ?>
  <p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'medicale-wp' ); ?></p>
<?php endif; ?>