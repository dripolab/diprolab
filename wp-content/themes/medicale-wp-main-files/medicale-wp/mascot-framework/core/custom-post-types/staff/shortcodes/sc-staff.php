<?php

if(!function_exists('mascot_core_cpt_sc_staff_vc_map_modifier')) {
	/**
	 * Shortcodes vc_map modifier
	 */
	function mascot_core_cpt_sc_staff_vc_map_modifier( $shortcode_base ) {
		//vc_remove_param( $shortcode_base, "display_type" );
		//vc_add_param( $shortcode_base, $attributes );
		//vc_add_params( $shortcode_base, $attributes );
	}
}



if(!function_exists('mascot_core_cpt_sc_staff_render')) {
	/**
	 * Renders shortcodes HTML
	 */
	function mascot_core_cpt_sc_staff_render( $attr, $content = null, $class_instance ) {

		$new_cpt_class = $class_instance;
		$args = array(
			'custom_css_class' => '',
			'staff_type' => '',
			'columns' => '4',

			'show_navigation' => 'true',
			'show_bullets' => 'true',
			'animation_speed' => '4000',

			'total_items' => '-1',
			'selected_category' => '',
			'order_by' => 'date',
			'order' => 'DESC',

			'excerpt_length' => '15',
			'feature_thumb_image_size' => 'post-thumbnail',
			'show_name' => 'true',
			'show_position' => 'true',
			'show_short_bio' => 'true',
			'show_short_bio_length' => '20',
			'show_social_link' => 'true',
			'show_view_details_button' => 'true'
		);
		$params = shortcode_atts($args, $attr);

		//query args
		$args = array(
			'post_type' => $new_cpt_class->ptKey,
			'orderby' => $params['order_by'],
			'order' => $params['order'],
			'posts_per_page' => $params['total_items'],
		);

		//if category selected
		if( $params['selected_category'] ) {
			$cat_array = explode(',', $params['selected_category']);
			$args['tax_query'] = array(
				array(
					'taxonomy' => $new_cpt_class->ptTaxKey,
					'field'	=> 'slug',
					'terms'	=> $cat_array,
				)
			);
		}

		//if order by staff_name selected
		if( $params['order_by'] == 'staff_name' ) {
			$args['meta_key'] = 'medicale_mascot_' . 'cpt_staff_name';
			$args['orderby']  = 'meta_value';
		}

		$the_query = new \WP_Query( $args );
		$params['the_query'] = $the_query;

		$params['social_list_array'] = $new_cpt_class->social_list;

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, shortcode_ob_start)
		$html = medicale_mascot_get_cpt_template_part( 'staff', $params['staff_type'], 'staff/shortcodes/tpl', $params, true );
		
		return $html;
	}
}