<?php
$current_page_id = medicale_mascot_get_page_id();
?>
<ul class="list-group">

  <?php if ( has_post_thumbnail() ) { ?>
  <li class="list-group-item p-0 mb-0">
	<?php the_post_thumbnail( 'medicale_mascot_square' ); ?>
  </li>
  <?php } ?>

  <li class="list-group-item active mb-0">
	<h4 class="list-group-item-heading"><?php echo rwmb_meta( 'medicale_mascot_' . "cpt_staff_name", '', $current_page_id ) ?></h4>
  </li>

  <?php medicale_mascot_get_cpt_template_part( 'social-info', null, 'staff/staff-details/tpl/parts', $params, false ); ?>

</ul>

<?php if ( !empty( rwmb_meta( 'medicale_mascot_' . "cpt_staff_appointment_url", '', $current_page_id ) ) ) { ?>
<div class="list-group">
  <a href="<?php echo rwmb_meta( 'medicale_mascot_' . "cpt_staff_appointment_url", '', $current_page_id ) ?>" target="_blank" class="list-group-item active">
	<h5 class="list-group-item-heading"><?php esc_html_e( 'Request an Appointment URL', 'medicale-wp' ) ?> <i class="fa fa-arrow-circle-right"></i></h5>
  </a>
</div>
<?php } ?>

<ul class="list-group">

  <?php if ( !empty( rwmb_meta( 'medicale_mascot_' . "cpt_staff_speciality", '', $current_page_id ) ) ) { ?>
  <li class="list-group-item">
	<h5 class="list-group-item-heading"><?php esc_html_e( 'Speciality', 'medicale-wp' ) ?></h5>
	<p class="list-group-item-text"><?php echo rwmb_meta( 'medicale_mascot_' . "cpt_staff_speciality", '', $current_page_id ) ?></p>
  </li>
  <?php } ?>

  <?php if ( !empty( rwmb_meta( 'medicale_mascot_' . "cpt_staff_short_bio", '', $current_page_id ) ) ) { ?>
  <li class="list-group-item">
	<h5 class="list-group-item-heading"><?php esc_html_e( 'Short Bio', 'medicale-wp' ) ?></h5>
	<p class="list-group-item-text"><?php echo rwmb_meta( 'medicale_mascot_' . "cpt_staff_short_bio", '', $current_page_id ) ?></p>
  </li>
  <?php } ?>
</ul>

<ul class="list-group">
  <li class="list-group-item active">
	<h5 class="list-group-item-heading list-group-item-title"><?php esc_html_e( 'Contacts', 'medicale-wp' ) ?></h5>
  </li>
  <?php if ( !empty( rwmb_meta( 'medicale_mascot_' . "cpt_staff_address", '', $current_page_id ) ) ) { ?>
  <li class="list-group-item">
	<h5 class="list-group-item-heading"><?php esc_html_e( 'Address', 'medicale-wp' ) ?></h5>
	<p class="list-group-item-text"><?php echo $staff_address = rwmb_meta( 'medicale_mascot_' . "cpt_staff_address", '', $current_page_id ) ?></p>
	
	<a class="btn btn-dark btn-theme-colored btn-sm mt-10" target="_blank" href="<?php echo $staff_address = "https://www.google.com/maps/place/" . str_replace(' ', '+', $staff_address); ?>"><?php esc_html_e( 'Get Direction', 'medicale-wp' ) ?> <i class="fa fa-arrow-circle-right"></i></a>
  </li>
  <?php } ?>

  <?php if ( !empty( rwmb_meta( 'medicale_mascot_' . "cpt_staff_phone", '', $current_page_id ) ) ) { ?>
  <li class="list-group-item">
	<h5 class="list-group-item-heading"><?php esc_html_e( 'Phone', 'medicale-wp' ) ?></h5>
	<p class="list-group-item-text"><i class="fa fa-phone"></i> <?php echo rwmb_meta( 'medicale_mascot_' . "cpt_staff_phone", '', $current_page_id ) ?></p>
  </li>
  <?php } ?>

  <?php if ( !empty( rwmb_meta( 'medicale_mascot_' . "cpt_staff_fax", '', $current_page_id ) ) ) { ?>
  <li class="list-group-item">
	<h5 class="list-group-item-heading">Fax</h5>
	<p class="list-group-item-text"><i class="fa fa-fax"></i> <?php echo rwmb_meta( 'medicale_mascot_' . "cpt_staff_fax", '', $current_page_id ) ?></p>
  </li>
  <?php } ?>

  <?php if ( !empty( rwmb_meta( 'medicale_mascot_' . "cpt_staff_email", '', $current_page_id ) ) ) { ?>
  <li class="list-group-item">
	<h5 class="list-group-item-heading"><?php esc_html_e( 'Email', 'medicale-wp' ) ?></h5>
	<p class="list-group-item-text"><i class="fa fa-envelope-o"></i> <?php echo rwmb_meta( 'medicale_mascot_' . "cpt_staff_email", '', $current_page_id ) ?></p>
  </li>
  <?php } ?>
</ul>

<?php
//working hours
$working_hours_array = array();
for ($i=1; $i <= 7 ; $i++) { 
  $day = rwmb_meta( 'medicale_mascot_' . 'cpt_staff_opening_hours_day_'.$i, '', $current_page_id );
  $day_time = rwmb_meta( 'medicale_mascot_' . 'cpt_staff_opening_hours_day_'.$i.'_time', '', $current_page_id );
  if( !empty( $day ) ) :
	$working_hours_array[$day] = $day_time;
  endif;
}
if( !empty( $working_hours_array ) ) :
?>
<ul class="list-group">
  <li class="list-group-item active">
	<h4 class="list-group-item-heading list-group-item-title"><?php esc_html_e( 'Working Hours', 'medicale-wp' ) ?></h4>
  </li>
  <li class="list-group-item">
	<div class="list-group-item-text">
  		<ul class="opening-hours">
  			<?php 
			foreach( $working_hours_array as $day => $day_time ) {
  			?>
					<li class="clearfix">
						<span><?php echo $day; ?></span>
						<div class="value"><?php echo $day_time; ?></div>
					</li>
  			<?php 
  				}
  			?>
			</ul>
	</div>
  </li>
</ul>
<?php
endif;
?>