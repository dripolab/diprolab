<?php


if(!function_exists('medicale_mascot_get_staff_details')) {
	/**
	 * Function that Renders Staff Details HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_staff_details( $container_type = 'container' ) {
		$params = array();

		$params['container_type'] = $container_type;

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_cpt_template_part( 'staff-parts', null, 'staff/staff-details/tpl', $params, false );
		
		return $html;
	}
}

if (!function_exists('medicale_mascot_get_staff_sidebar_layout')) {
	/**
	 * Return Staff Layout HTML
	 */
	function medicale_mascot_get_staff_sidebar_layout( $page_layout = null ) {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();

		$params['page_layout'] = 'layout1';

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_cpt_template_part( 'staff-layout1', null, 'staff/staff-details/tpl/layout', $params, false );
		
		return $html;
	}
}

if ( ! function_exists( 'medicale_mascot_get_staff_layout1_sidebar' ) ) {
	/**
	 * Returns Staff Sidebar
	 *
	 */
	function medicale_mascot_get_staff_layout1_sidebar() {
		$params = array();
		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_cpt_template_part( 'staff-layout1-sidebar', null, 'staff/staff-details/tpl/parts', $params, false );
		
		return $html;
	}
}

if ( ! function_exists( 'medicale_mascot_get_staff_layout1_content' ) ) {
	/**
	 * Returns Staff Content
	 *
	 */
	function medicale_mascot_get_staff_layout1_content() {
		$params = array();
		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_cpt_template_part( 'staff-layout1-content', null, 'staff/staff-details/tpl/parts', $params, false );
		
		return $html;
	}
}