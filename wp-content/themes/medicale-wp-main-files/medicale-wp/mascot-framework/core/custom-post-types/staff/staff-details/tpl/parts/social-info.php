<?php
use MASCOTCORE\CPT\Staff\CPT_Staff;
if( class_exists('MASCOTCORE\CPT\Staff\CPT_Staff') ) {
  $staff_cpt_class = CPT_Staff::Instance();
  $social_links = $staff_cpt_class->socialList();
  $current_page_id = medicale_mascot_get_page_id();

  $link_array = array();
  if( $social_links ): foreach( $social_links as $key => $value ) {
	$current_link = rwmb_meta( 'medicale_mascot_' . "cpt_staff_social_".$key, '', $current_page_id );
	if( isset( $current_link ) && $current_link != '' ) :
		$link_array[$key] = $current_link;
	endif; 
  } endif;

  if( !empty( $link_array ) ) :
?>
  <li class="list-group-item">
	<ul class="styled-icons icon-gray icon-circled icon-sm icon-theme-colored pl-0">
		<?php 
		foreach( $link_array as $key => $url ) {
		?>
		<li><a href="<?php echo $url; ?>" target="_blank"><i class="fa fa-<?php echo $key; ?>"></i></a></li>
		<?php } ?>  
	</ul>
  </li>
<?php
  endif;
}
?>