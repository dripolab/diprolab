<div id="staff-<?php the_ID(); ?>" <?php post_class( 'staff-single-wrapper' ); ?>>
  <div class="row">
	<div class="col-md-4">
		<div class="staff-sidebar">
		<?php
		medicale_mascot_get_staff_layout1_sidebar();
		?>
		
		</div>
	</div>
	<div class="col-md-8">
		<div class="staff-content">
		<?php
		medicale_mascot_get_staff_layout1_content();
		?>
		</div>
	</div>
  </div>
</div>