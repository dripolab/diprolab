
		<div class="services-item icon-box left media p-0">
			<?php if ( $show_service_image == 'true' ) : ?>
			<a class="icon media-left pull-left flip" <?php if ( $link_service_url == 'true' ) : ?> target="<?php echo $open_links_in;?>" href="<?php the_permalink();?>"<?php endif; ?>>
			<?php
			$image_or_icon_html = '';
			$choose_image_or_icon = rwmb_meta( 'medicale_mascot_' . "cpt_services_choose_image_or_icon" );
			if( $choose_image_or_icon == 'image' ) {
				$tab_image = rwmb_meta( 'medicale_mascot_' . "cpt_services_image" );
				if ( !empty( $tab_image ) ) {
				$img_url = medicale_mascot_metabox_get_image_advanced_field_url( $tab_image );
				$image_or_icon_html = '<img src="'.$img_url.'" alt=""></br>';
				}
			}elseif( $choose_image_or_icon == 'icon' ) {
				$choose_iconpack = rwmb_meta( 'medicale_mascot_' . "cpt_service_iconpack" );
				if( $choose_iconpack != '' ) {
					$icon_name = rwmb_meta( 'medicale_mascot_' . "cpt_service_iconpack" . '_' . $choose_iconpack );
					$inlin_style = "font-size: " . rwmb_meta( 'medicale_mascot_' . "cpt_services_icon_font_size" ) . '; ';
					$inlin_style .= "color: " . rwmb_meta( 'medicale_mascot_' . "cpt_services_icon_color" ) . '; ';
					$image_or_icon_html = '<i class="'.$icon_name.'" style="'.$inlin_style.'"></i>';
				}
			}
			?>
			<?php echo $image_or_icon_html; ?>
			</a>
			<?php endif; ?>
			<div class="media-body">
			<?php if ( $show_service_title == 'true' ) : ?>
			<h5 class="title media-heading heading">
				<a <?php if ( $link_service_url == 'true' ) : ?> target="<?php echo $open_links_in;?>" href="<?php the_permalink();?>"<?php endif; ?>>
				<?php the_title();?>
				</a>
			</h5>
			<?php endif; ?>
			<?php if ( $show_service_description == 'true' ) : ?>
			<?php if ( has_excerpt() ) { echo get_the_excerpt(); } else { ?>
			<p><?php echo wp_trim_words( strip_shortcodes( get_the_content() ), $excerpt_length, '...' )?></p>
			<?php } ?>
			<?php endif; ?>
			</div>
		</div>