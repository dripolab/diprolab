<section>
  <div class="<?php echo $container_type; ?>">
  	<div class="main-content-area">
	<?php
		if ( have_posts() ) :
		// Start the Loop.
		while ( have_posts() ) : the_post();
	?>
		<div class='mb-15'>
			<?php
			$image_or_icon_html = '';
			$choose_image_or_icon = rwmb_meta( 'medicale_mascot_' . "cpt_services_choose_image_or_icon" );
			if( $choose_image_or_icon == 'image' ) {
				$tab_image = rwmb_meta( 'medicale_mascot_' . "cpt_services_image" );
				if ( !empty( $tab_image ) ) {
				$img_url = medicale_mascot_metabox_get_image_advanced_field_url( $tab_image );
				$image_or_icon_html = '<img src="'.$img_url.'" alt=""></br>';
				}
			}elseif( $choose_image_or_icon == 'icon' ) {
				$choose_iconpack = rwmb_meta( 'medicale_mascot_' . "cpt_service_iconpack" );
				if( $choose_iconpack != '' ) {
					$icon_name = rwmb_meta( 'medicale_mascot_' . "cpt_service_iconpack" . '_' . $choose_iconpack );
					$inlin_style = "font-size: " . rwmb_meta( 'medicale_mascot_' . "cpt_services_icon_font_size" ) . '; ';
					$inlin_style .= "color: " . rwmb_meta( 'medicale_mascot_' . "cpt_services_icon_color" ) . '; ';
					$image_or_icon_html = '<i class="'.$icon_name.'" style="'.$inlin_style.'"></i>';
				}
			}
			?>
			<?php echo $image_or_icon_html; ?>
		</div>

	<?php
			medicale_mascot_get_single_post_title();
			the_content();
		endwhile;
		endif;
	?>
	<?php
		if( medicale_mascot_get_redux_option( 'cpt-settings-services-show-related-posts', true ) ) {
			$posts_count = medicale_mascot_get_redux_option( 'cpt-settings-services-show-related-posts-count', 3 );
			echo '<hr class="hr-divider">';
			medicale_mascot_get_blog_single_related_posts( medicale_mascot_get_page_id(), $posts_count );
		}
	?>
	</div>
  </div>
</section> 