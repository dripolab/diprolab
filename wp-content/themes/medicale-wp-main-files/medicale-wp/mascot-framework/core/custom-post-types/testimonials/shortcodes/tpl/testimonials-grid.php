<?php
if( $columns == 1 ) {
  $col_layout = 12;
  $col_layout_class = "col-md-12";
  $equal_height_class = "";
} else if( $columns == 2 ) {
  $col_layout = 6;
  $col_layout_class = "col-sm-6 col-md-6";
  $equal_height_class = "equal-height";
} else if( $columns == 3 ) {
  $col_layout = 4;
  $col_layout_class = "col-sm-6 col-md-4";
  $equal_height_class = "equal-height";
} else {
  $col_layout = 3;
  $col_layout_class = "col-sm-6 col-md-3";
  $equal_height_class = "equal-height";
}
?>

<?php if ( $the_query->have_posts() ) : ?>
  <div class="row <?php echo $custom_css_class;?> <?php echo $equal_height_class;?>">
	<!-- the loop -->
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<div class="<?php echo $col_layout_class;?>">
		<div class="testimonial sm-text-center pt-10">
			<?php if ( $show_thumb == 'true' ) : ?>
			<div class="thumb mb-15">
			<?php echo get_the_post_thumbnail( get_the_ID(), array( 64, 64 ), array( 'class' => 'img-circle' ) );?>
			</div>
			<?php endif; ?>
			<div>
			<?php if ( $show_testimonial_text == 'true' ) : ?>
			<p class="author-text"><?php echo rwmb_meta( 'medicale_mascot_' . "cpt_author_text" );?></p>
			<?php endif; ?>
			<p class="author mt-20">
				<?php if ( $show_author_name == 'true' ) : ?>
				- <span class="author-name"><?php echo rwmb_meta( 'medicale_mascot_' . "cpt_author_name" );?>,</span> 
				<?php endif; ?>
				<small>
				<em>
					<?php if ( $show_author_job_position == 'true' ) : ?>
					<?php echo rwmb_meta( 'medicale_mascot_' . "cpt_author_job_position" );?> 
					<?php endif; ?>
					<?php if ( $show_author_company == 'true' ) : ?>
					<a href="<?php echo rwmb_meta( 'medicale_mascot_' . "cpt_author_company_URL" );?>">
					<?php echo rwmb_meta( 'medicale_mascot_' . "cpt_author_company" );?>
					</a>
					<?php endif; ?>
				</em>
				</small>
			</p>
			</div>
		</div>
		</div>
	<?php endwhile; ?>
	<!-- end of the loop -->
  </div>
  <?php wp_reset_postdata(); ?>

<?php else : ?>
  <p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'medicale-wp' ); ?></p>
<?php endif; ?>