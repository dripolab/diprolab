<?php

if(!function_exists('mascot_core_cpt_sc_faq_vc_map_modifier')) {
	/**
	 * Shortcodes vc_map modifier
	 */
	function mascot_core_cpt_sc_faq_vc_map_modifier( $shortcode_base ) {
		//vc_remove_param( $shortcode_base, "display_type" );
		//vc_add_param( $shortcode_base, $attributes );
		//vc_add_params( $shortcode_base, $attributes );
	}
}



if(!function_exists('mascot_core_cpt_sc_faq_render')) {
	/**
	 * Renders shortcodes HTML
	 */
	function mascot_core_cpt_sc_faq_render( $attr, $content = null, $class_instance ) {

		$new_cpt_class = $class_instance;
		$args = array(
			'custom_css_class' => '',
			'faq_type' => '',
			'total_items' => '-1',
			'selected_category' => '',
			'order_by' => 'date',
			'order' => 'DESC',
		);
		$params = shortcode_atts($args, $attr);

		//query args
		$args = array(
			'post_type' => $new_cpt_class->ptKey,
			'orderby' => $params['order_by'],
			'order' => $params['order'],
			'posts_per_page' => $params['total_items'],
		);

		//if category selected
		if( $params['selected_category'] ) {
			$cat_array = explode(',', $params['selected_category']);
			$args['tax_query'] = array(
				array(
					'taxonomy' => $new_cpt_class->ptTaxKey,
					'field'	=> 'slug',
					'terms'	=> $cat_array,
				)
			);
		}

		$the_query = new \WP_Query( $args );
		$params['the_query'] = $the_query;

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, shortcode_ob_start)
		$html = medicale_mascot_get_cpt_template_part( 'faq', $params['faq_type'], 'faq/shortcodes/tpl', $params, true );
		
		return $html;
	}
}