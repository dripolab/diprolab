<section>
  <div class="<?php echo $container_type; ?>">
  	<div class="main-content-area">
	<?php
		if ( have_posts() ) :
		// Start the Loop.
		while ( have_posts() ) : the_post();
			medicale_mascot_get_single_post_title();
			the_content();
		endwhile;
		endif;
	?>
	<?php
		if( medicale_mascot_get_redux_option( 'cpt-settings-faq-show-related-posts', true ) ) {
			$posts_count = medicale_mascot_get_redux_option( 'cpt-settings-faq-show-related-posts-count', 3 );
			echo '<hr class="hr-divider">';
			medicale_mascot_get_blog_single_related_posts( medicale_mascot_get_page_id(), $posts_count );
		}
	?>
	</div>
  </div>
</section> 