<?php
  $random_number = wp_rand( 111111, 999999 );
?>
<?php if ( $the_query->have_posts() ) : ?>
  <div id="accordion-<?php echo $random_number;?>" class="panel-group accordion <?php echo $custom_css_class;?>">
	<?php $iter = 0; while ( $the_query->have_posts() ) : $the_query->the_post(); $iter++;?>
	<div class="panel">
		<div class="panel-title"> <a data-parent="#accordion-<?php echo $random_number;?>" data-toggle="collapse" href="#accordion-<?php echo $random_number.'-'.get_the_ID();?>" class="<?php echo ( $iter == 1) ? 'active': '';?>" aria-expanded="true">
		<span class="open-sub"></span> <strong><?php the_title();?></strong></a>
		</div>
		<div id="accordion-<?php echo $random_number.'-'.get_the_ID();?>" class="panel-collapse collapse <?php echo ( $iter == 1) ? 'in': '';?>" role="tablist" aria-expanded="true">
		<div class="panel-content">
			<?php the_content();?>
		</div>
		</div>
	</div>
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
  </div>

<?php else : ?>
  <p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'medicale-wp' ); ?></p>
<?php endif; ?>

