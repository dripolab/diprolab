<?php
  $random_number = wp_rand( 111111, 999999 );
?>
<?php if ( $the_query->have_posts() ) : ?>
  <div class="<?php echo $custom_css_class;?>">
	<div class="list-group faq-titles">
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<a href="#faq-<?php echo $random_number.'-'.get_the_ID();?>" class="list-group-item smooth-scroll-to-target">
		<?php the_title();?>
		</a>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	</div>
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	<div id="faq-<?php echo $random_number.'-'.get_the_ID();?>" class="faq-details mb-50">
		<h3><?php the_title();?></h3>
		<hr>
		<?php the_content();?>
	</div>
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
  </div>

<?php else : ?>
  <p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'medicale-wp' ); ?></p>
<?php endif; ?>