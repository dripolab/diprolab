
		<div class="pricing-table style1 bg-white border-10px <?php echo $text_alignment;?> <?php if ( rwmb_meta( 'medicale_mascot_' . "cpt_pricing_featured" ) ) echo "featured";?>">
			<div class="p-40">
			<?php if ( rwmb_meta( 'medicale_mascot_' . "cpt_pricing_show_image_or_icon" ) == true ) : ?>
				<?php if ( rwmb_meta( 'medicale_mascot_' . "cpt_pricing_choose_image_or_icon" ) == 'image' ) { ?>
				<?php
				$images = rwmb_meta( 'medicale_mascot_' . "cpt_pricing_image" );
				if ( !empty( $images ) ) {
					foreach ( $images as $image ) {
					echo "<img src='{$image['url']}' alt='{$image['alt']}' />";
					}
				}
				?>
				<?php } else if ( rwmb_meta( 'medicale_mascot_' . "cpt_pricing_choose_image_or_icon" ) == 'icon' ) { ?>
				<div class="icon-box iconbox-theme-colored">
				<a class="icon icon-dark icon-circled icon-border-effect effect-circled <?php echo ( rwmb_meta( 'medicale_mascot_' . "cpt_pricing_icon_size" ) == '' ) ? '' : 'icon-'.rwmb_meta( 'medicale_mascot_' . "cpt_pricing_icon_size" );?>" href="#">
					<i class="pe-7s-users"></i>
				</a>
				</div>
				<?php } ?>
			<?php endif; ?>
			<?php if ( $show_pricing_title == 'true' ) : ?>
			<h3 class="title"><?php echo rwmb_meta( 'medicale_mascot_' . "cpt_pricing_title" );?></h3>
			<?php endif; ?>
			<?php if ( $show_pricing_subtitle == 'true' ) : ?>
			<p class="subtitle"><?php echo rwmb_meta( 'medicale_mascot_' . "cpt_pricing_sub_title" );?></p>
			<?php endif; ?>
			<?php if ( $show_price == 'true' ) : ?>
			<h3 class="price text-theme-colored mb-10">
				<?php if ( $show_currency == 'true' ) : ?>
				<span class="currency"><?php echo ( rwmb_meta( 'medicale_mascot_' . "cpt_pricing_currency" ) == '' ) ? '$' : rwmb_meta( 'medicale_mascot_' . "cpt_pricing_currency" );?></span>
				<?php endif; ?>
				<?php echo ( rwmb_meta( 'medicale_mascot_' . "cpt_pricing_price" ) == '' ) ? '99' : rwmb_meta( 'medicale_mascot_' . "cpt_pricing_price" );?>
			</h3>
			<?php endif; ?>
			<?php if ( $show_period == 'true' ) : ?>
			<h5 class="period"><?php echo rwmb_meta( 'medicale_mascot_' . "cpt_pricing_price_period" );?></h5>
			<?php endif; ?>
			<?php if ( $show_pricing_feature_list == 'true' ) : ?>
			<div class="feature-list">
				<?php
				$feature_list = rwmb_meta( 'medicale_mascot_' . "cpt_pricing_content_feature_list" );;
				$feature_list = preg_replace('#^<\/p>|<p>$#', '', $feature_list); // delete p tag before and after content
				echo do_shortcode( $feature_list );  
				?>
			</div>
			<?php endif; ?>
			<?php if ( $show_button == 'true' ) : ?>
			<a target="<?php echo rwmb_meta( 'medicale_mascot_' . "cpt_pricing_button_link_target" );?>" href="<?php echo ( rwmb_meta( 'medicale_mascot_' . "cpt_pricing_button_link" ) == '' ) ? '#' : rwmb_meta( 'medicale_mascot_' . "cpt_pricing_button_link" );?>" class="btn btn-colored btn-theme-colored text-uppercase"><?php echo ( rwmb_meta( 'medicale_mascot_' . "cpt_pricing_button_text" ) == '' ) ? 'Select Plan' : rwmb_meta( 'medicale_mascot_' . "cpt_pricing_button_text" );?></a>
			<?php endif; ?>
			</div>
		</div>