<?php


if(!function_exists('medicale_mascot_get_page_preloader')) {
	/**
	 * Function that Renders page preloader HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_page_preloader() {
		$params = array();

		$params['page_preloader'] = medicale_mascot_get_redux_option( 'general-settings-enable-page-preloader' );

		if( !$params['page_preloader'] ) {
			return;
		}
		
		$params['page_show_disable_button'] = medicale_mascot_get_redux_option( 'general-settings-page-preloader-show-disable-button' );
		$params['page_show_disable_button_text'] = medicale_mascot_get_redux_option( 'general-settings-page-preloader-show-disable-button-text' );
		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'preloader', null, 'page-preloader/tpl', $params );
		
		return $html;
	}
}


if(!function_exists('medicale_mascot_get_page_preloader_type')) {
	/**
	 * Function that Renders page preloader Type HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_page_preloader_type() {
		$params = array();
		$html = '';

		$params['page_preloader_type'] = medicale_mascot_get_redux_option( 'general-settings-page-preloader-type' );
		$params['page_preloader_type_css'] = medicale_mascot_get_redux_option( 'general-settings-page-preloader-type-css' );
		$params['page_preloader_type_gif'] = medicale_mascot_get_redux_option( 'general-settings-page-preloader-type-gif' );
		
		if( $params['page_preloader_type'] == 'css-preloader' ) {
			//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
			$html = medicale_mascot_get_blocks_template_part( $params['page_preloader_type_css'], null, 'page-preloader/tpl/'.$params['page_preloader_type'], $params );
		} else if( $params['page_preloader_type'] == 'gif-preloader' ) {
			//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
			$html = medicale_mascot_get_blocks_template_part( 'preloader-gif', null, 'page-preloader/tpl/'.$params['page_preloader_type'], $params );
		}
		
		return $html;
	}
}