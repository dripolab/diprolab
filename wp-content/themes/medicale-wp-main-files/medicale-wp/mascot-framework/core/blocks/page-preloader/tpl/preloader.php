
	<!-- preloader -->
	<div id="preloader">
		<?php do_action( 'medicale_mascot_page_preloader_start' ); ?>
	<div id="spinner">
		<?php medicale_mascot_get_page_preloader_type();?>
	</div>

		<?php if ( $page_show_disable_button ) { ?>
	<div id="disable-preloader" class="btn btn-default btn-sm"><?php echo esc_html( $page_show_disable_button_text );?></div>
		<?php } ?>
		<?php do_action( 'medicale_mascot_page_preloader_end' ); ?>
	</div>