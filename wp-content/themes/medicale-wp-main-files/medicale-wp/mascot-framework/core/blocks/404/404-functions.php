<?php


if(!function_exists('medicale_mascot_get_404_parts')) {
	/**
	 * Function that Renders Coming Soon Page HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_404_parts() {
		$params = array();
		$section_classes_array = array();
		$params['section_classes'] = '';

		//Text Alignment
		$params['text_align'] = medicale_mascot_get_redux_option( '404-page-settings-text-align', 'text-center' );
		
		//Add Background Overlay
		if( medicale_mascot_get_redux_option( '404-page-settings-bg-layer-overlay-status' ) ) {
			$section_classes_array[] = 'layer-overlay overlay-'.medicale_mascot_get_redux_option( '404-page-settings-bg-layer-overlay-color' ) .'-'.medicale_mascot_get_redux_option( '404-page-settings-bg-layer-overlay' );
		}

		//make array into string
		if( is_array( $section_classes_array ) && count( $section_classes_array ) ) {
			$params['section_classes'] = esc_attr(implode(' ', $section_classes_array));
		}

		$params['page_title'] = medicale_mascot_get_redux_option( '404-page-settings-title', '404' );
		$params['page_content'] = medicale_mascot_get_redux_option( '404-page-settings-content', 'Page not found' );


		//fullscreen if not show header footer
		if( medicale_mascot_get_redux_option( '404-page-settings-show-header' ) == true || medicale_mascot_get_redux_option( '404-page-settings-show-footer' ) == true ) {
			$params['fullscreen'] = 'page-404-wrapper-padding';
		} else {
			$params['fullscreen'] = 'fullscreen';
		}

		//Search Box
		$params['show_search_box'] = medicale_mascot_get_redux_option( '404-page-settings-show-search-box' );
		$params['search_box_heading'] = medicale_mascot_get_redux_option( '404-page-settings-search-box-heading' );
		$params['search_box_paragraph'] = medicale_mascot_get_redux_option( '404-page-settings-search-box-paragraph' );

		//Helpful Links
		$params['show_helpful_links'] = medicale_mascot_get_redux_option( '404-page-settings-show-helpful-links' );
		$params['helpful_links_heading'] = medicale_mascot_get_redux_option( '404-page-settings-helpful-links-heading' );
		$params['helpful_links_nav'] = 'page-404-helpful-links';

		//Show Social Links
		$params['show_social_links'] = medicale_mascot_get_redux_option( '404-page-settings-show-social-links' );

		//Back Button Label
		$params['show_back_to_home_button'] = medicale_mascot_get_redux_option( '404-page-settings-show-back-to-home-button', true );
		$params['back_to_home_button_label'] = medicale_mascot_get_redux_option( '404-page-settings-back-to-home-button-label', esc_html__( "Back to Homes", 'medicale-wp' ) );

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'template', medicale_mascot_get_redux_option( '404-page-settings-layout', 'simple' ), '404/tpl', $params );
		
		return $html;
	}
}