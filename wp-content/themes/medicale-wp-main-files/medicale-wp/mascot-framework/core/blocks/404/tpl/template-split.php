<section class="page-404-wrapper <?php echo $fullscreen;?> <?php echo $section_classes;?>">
	<div class="display-table">
		<div class="display-table-cell">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="page-404-main-content">

							<?php do_action( 'medicale_mascot_page_404_content_start' ); ?>

							<h1 class="title"><?php echo $page_title;?></h1>
							<div class="content">
								<?php echo wpautop( do_shortcode( $page_content ) ); ?> 
							</div>
							<?php
								if( $show_back_to_home_button ) {
									medicale_mascot_get_blocks_template_part( 'back-to-home-button', null, '404/tpl', $params );
								}
							?>

							<?php do_action( 'medicale_mascot_page_404_content_end' ); ?>

						</div>
					</div>
					<div class="col-md-5">
						<?php
							if( $show_search_box ) {
								medicale_mascot_get_blocks_template_part( 'search-box', null, '404/tpl', $params );
							}
						?>
						<?php
							if( $show_helpful_links ) {
								medicale_mascot_get_blocks_template_part( 'helpful-links', null, '404/tpl', $params );
							}
						?>
					</div>
				</div>
				<div class="row <?php echo $text_align;?> mt-30">
					<div class="col-md-12">
					<?php
						if( $show_social_links ) {
							medicale_mascot_get_blocks_template_part( 'social-links', null, '404/tpl', $params );
						}
					?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>