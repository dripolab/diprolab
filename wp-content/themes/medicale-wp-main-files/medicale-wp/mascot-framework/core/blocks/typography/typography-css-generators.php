<?php

if (!function_exists('medicale_mascot_typography_primary_body')) {
	/**
	 * Generate CSS codes for body, p Typography
	 */
	function medicale_mascot_typography_primary_body() {
		$var_name = 'typography-primary-body';
		$declaration = array();
		$selector = array(
			'body',
			'p',
		);
		$declaration = medicale_mascot_redux_option_field_typography( $var_name );
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_typography_primary_body');
}

if (!function_exists('medicale_mascot_typography_primary_link_color')) {
	/**
	 * Generate CSS codes for Primary Link Color
	 */
	function medicale_mascot_typography_primary_link_color() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'typography-primary-body-link-color';
		$declaration = array();
		$selector = array(
			'a',
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		$declaration['color'] = $medicale_mascot_redux_theme_opt[$var_name];
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_typography_primary_link_color');
}



//For H1
if (!function_exists('medicale_mascot_typography_h1')) {
	/**
	 * Generate CSS codes for H1 Typography
	 */
	function medicale_mascot_typography_h1() {
		$var_name = 'typography-h1';
		$declaration = array();
		$selector = array(
			'h1, .h1'
		);
		$declaration = medicale_mascot_redux_option_field_typography( $var_name );
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_typography_h1');
}

if (!function_exists('medicale_mascot_typography_h1_margin_top_bottom')) {
	/**
	 * Generate CSS codes for H1 Margin Top & Bottom
	 */
	function medicale_mascot_typography_h1_margin_top_bottom() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'typography-h1-margin-top-bottom';
		$declaration = array();
		$selector = array(
			'h1, .h1'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		if( $medicale_mascot_redux_theme_opt[$var_name]['margin-top'] != "" ) {
			$declaration['margin-top'] = $medicale_mascot_redux_theme_opt[$var_name]['margin-top'];
		}
		if( $medicale_mascot_redux_theme_opt[$var_name]['margin-bottom'] != "" ) {
			$declaration['margin-bottom'] = $medicale_mascot_redux_theme_opt[$var_name]['margin-bottom'];
		}
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_typography_h1_margin_top_bottom');
}


//For H2
if (!function_exists('medicale_mascot_typography_h2')) {
	/**
	 * Generate CSS codes for H2 Typography
	 */
	function medicale_mascot_typography_h2() {
		$var_name = 'typography-h2';
		$declaration = array();
		$selector = array(
			'h2, .h2'
		);
		$declaration = medicale_mascot_redux_option_field_typography( $var_name );
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_typography_h2');
}

if (!function_exists('medicale_mascot_typography_h2_margin_top_bottom')) {
	/**
	 * Generate CSS codes for H2 Margin Top & Bottom
	 */
	function medicale_mascot_typography_h2_margin_top_bottom() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'typography-h2-margin-top-bottom';
		$declaration = array();
		$selector = array(
			'h2, .h2'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		if( $medicale_mascot_redux_theme_opt[$var_name]['margin-top'] != "" ) {
			$declaration['margin-top'] = $medicale_mascot_redux_theme_opt[$var_name]['margin-top'];
		}
		if( $medicale_mascot_redux_theme_opt[$var_name]['margin-bottom'] != "" ) {
			$declaration['margin-bottom'] = $medicale_mascot_redux_theme_opt[$var_name]['margin-bottom'];
		}
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_typography_h2_margin_top_bottom');
}


//For H3
if (!function_exists('medicale_mascot_typography_h3')) {
	/**
	 * Generate CSS codes for H3 Typography
	 */
	function medicale_mascot_typography_h3() {
		$var_name = 'typography-h3';
		$declaration = array();
		$selector = array(
			'h3, .h3'
		);
		$declaration = medicale_mascot_redux_option_field_typography( $var_name );
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_typography_h3');
}

if (!function_exists('medicale_mascot_typography_h3_margin_top_bottom')) {
	/**
	 * Generate CSS codes for H3 Margin Top & Bottom
	 */
	function medicale_mascot_typography_h3_margin_top_bottom() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'typography-h3-margin-top-bottom';
		$declaration = array();
		$selector = array(
			'h3, .h3'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		if( $medicale_mascot_redux_theme_opt[$var_name]['margin-top'] != "" ) {
			$declaration['margin-top'] = $medicale_mascot_redux_theme_opt[$var_name]['margin-top'];
		}
		if( $medicale_mascot_redux_theme_opt[$var_name]['margin-bottom'] != "" ) {
			$declaration['margin-bottom'] = $medicale_mascot_redux_theme_opt[$var_name]['margin-bottom'];
		}
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_typography_h3_margin_top_bottom');
}


//For H4
if (!function_exists('medicale_mascot_typography_h4')) {
	/**
	 * Generate CSS codes for H4 Typography
	 */
	function medicale_mascot_typography_h4() {
		$var_name = 'typography-h4';
		$declaration = array();
		$selector = array(
			'h4, .h4'
		);
		$declaration = medicale_mascot_redux_option_field_typography( $var_name );
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_typography_h4');
}

if (!function_exists('medicale_mascot_typography_h4_margin_top_bottom')) {
	/**
	 * Generate CSS codes for H4 Margin Top & Bottom
	 */
	function medicale_mascot_typography_h4_margin_top_bottom() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'typography-h4-margin-top-bottom';
		$declaration = array();
		$selector = array(
			'h4, .h4'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		if( $medicale_mascot_redux_theme_opt[$var_name]['margin-top'] != "" ) {
			$declaration['margin-top'] = $medicale_mascot_redux_theme_opt[$var_name]['margin-top'];
		}
		if( $medicale_mascot_redux_theme_opt[$var_name]['margin-bottom'] != "" ) {
			$declaration['margin-bottom'] = $medicale_mascot_redux_theme_opt[$var_name]['margin-bottom'];
		}
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_typography_h4_margin_top_bottom');
}


//For H5
if (!function_exists('medicale_mascot_typography_h5')) {
	/**
	 * Generate CSS codes for H5 Typography
	 */
	function medicale_mascot_typography_h5() {
		$var_name = 'typography-h5';
		$declaration = array();
		$selector = array(
			'h5, .h5'
		);
		$declaration = medicale_mascot_redux_option_field_typography( $var_name );
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_typography_h5');
}

if (!function_exists('medicale_mascot_typography_h5_margin_top_bottom')) {
	/**
	 * Generate CSS codes for H5 Margin Top & Bottom
	 */
	function medicale_mascot_typography_h5_margin_top_bottom() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'typography-h5-margin-top-bottom';
		$declaration = array();
		$selector = array(
			'h5, .h5'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		if( $medicale_mascot_redux_theme_opt[$var_name]['margin-top'] != "" ) {
			$declaration['margin-top'] = $medicale_mascot_redux_theme_opt[$var_name]['margin-top'];
		}
		if( $medicale_mascot_redux_theme_opt[$var_name]['margin-bottom'] != "" ) {
			$declaration['margin-bottom'] = $medicale_mascot_redux_theme_opt[$var_name]['margin-bottom'];
		}
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_typography_h5_margin_top_bottom');
}


//For H6
if (!function_exists('medicale_mascot_typography_h6')) {
	/**
	 * Generate CSS codes for H6 Typography
	 */
	function medicale_mascot_typography_h6() {
		$var_name = 'typography-h6';
		$declaration = array();
		$selector = array(
			'h6, .h6'
		);
		$declaration = medicale_mascot_redux_option_field_typography( $var_name );
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_typography_h6');
}

if (!function_exists('medicale_mascot_typography_h6_margin_top_bottom')) {
	/**
	 * Generate CSS codes for H6 Margin Top & Bottom
	 */
	function medicale_mascot_typography_h6_margin_top_bottom() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'typography-h6-margin-top-bottom';
		$declaration = array();
		$selector = array(
			'h6, .h6'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		if( $medicale_mascot_redux_theme_opt[$var_name]['margin-top'] != "" ) {
			$declaration['margin-top'] = $medicale_mascot_redux_theme_opt[$var_name]['margin-top'];
		}
		if( $medicale_mascot_redux_theme_opt[$var_name]['margin-bottom'] != "" ) {
			$declaration['margin-bottom'] = $medicale_mascot_redux_theme_opt[$var_name]['margin-bottom'];
		}
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_typography_h6_margin_top_bottom');
}