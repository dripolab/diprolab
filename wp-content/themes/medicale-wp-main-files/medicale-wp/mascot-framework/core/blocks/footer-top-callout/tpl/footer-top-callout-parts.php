<section id="footer-top-callout-wrap">
	<div class="container <?php echo $callout_text_align; ?>">
		<div class="media">
			<?php if( $left_font_icon != "" ): ?>
			<div class="media-<?php echo $left_font_icon_position; ?> callout-icon">
				<i class="<?php echo $left_font_icon; ?>"></i>
			</div>
			<?php endif; ?>
			<div class="media-body <?php echo $button_position; ?>">
				<div class="callout-content"><?php echo wpautop( do_shortcode( $callout_text ) ); ?></div>
				<?php if( $button_visibility ): ?>
				<div class="callout-button">
					<a target="<?php echo $button_target; ?>" class="btn btn-theme-colored btn-xl btn-flat" href="<?php echo $button_link; ?>"><?php echo $button_text; ?></a>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>