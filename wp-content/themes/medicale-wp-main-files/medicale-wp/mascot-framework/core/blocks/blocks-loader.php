<?php

/* Loads all blocks located in blocks folder
================================================== */
if( !function_exists('medicale_mascot_load_all_template_parts') ) {
	function medicale_mascot_load_all_template_parts() {
		foreach( glob(MASCOT_FRAMEWORK_DIR.'/core/blocks/*/loader.php') as $each_template_part_loader ) {
			require_once $each_template_part_loader;
		}
	}
	add_action('medicale_mascot_before_custom_action', 'medicale_mascot_load_all_template_parts');
}