
<?php if ( $enabled_social_networks ) : ?>
	<div class="social-share-icons">
		<h5 class="title"><?php echo esc_html( $sharing_heading );?></h5>
		<?php
			if( $social_icon_type == 'icon' ) {
				medicale_mascot_get_blocks_template_part( 'icon', null, 'social-share/tpl', $params );
			} else if ( $social_icon_type == 'text' ) {
				medicale_mascot_get_blocks_template_part( 'text', null, 'social-share/tpl', $params );
			} else if ( $social_icon_type == 'icon-brand' ) {
				medicale_mascot_get_blocks_template_part( 'icon-brand', null, 'social-share/tpl', $params );
			}
		?>
	</div>
<?php endif; ?>