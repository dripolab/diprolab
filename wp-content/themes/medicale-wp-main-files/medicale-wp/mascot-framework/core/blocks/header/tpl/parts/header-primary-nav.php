
				<?php
				if (has_nav_menu('primary')) {
					$menu_class = 'menuzord-menu';
					$one_page_nav_effect = ( $enable_one_page_nav_scrolling_effect ) ? ' onepage-nav' : '';
					$menu_class = $menu_class . $one_page_nav_effect;

					if( $header_layout_type == 'header-5-mobile-nav' ) {
					$one_page_nav_effect = ( $enable_one_page_nav_scrolling_effect ) ? ' fullscreen-onepage-nav' : '';
					$menu_class = $one_page_nav_effect;
					}

					if( $header_layout_type == 'header-6-side-panel-nav' ) {
					$one_page_nav_effect = ( $enable_one_page_nav_scrolling_effect ) ? ' onepage-nav' : '';
					$menu_class = $one_page_nav_effect;
					}

					if( $custom_primary_nav_menu != '' ) {
					wp_nav_menu(
						array(
						'menu'				=> $custom_primary_nav_menu,
						'theme_location'	=> 'primary',
						'menu_class'		=> $menu_class,
						'menu_id'			=> 'main-nav', 
						'container'		=> '',
						'link_before'		=> '<span>',
						'link_after'		=> '</span>',
						'walker'			=>  new Medicale_Mascot_Nav_Walker
						)
					);
					} else {
					wp_nav_menu(
						array(
						'theme_location'	=> 'primary',
						'menu_class'		=> $menu_class,
						'menu_id'			=> 'main-nav', 
						'container'		=> '',
						'link_before'		=> '<span>',
						'link_after'		=> '</span>',
						'walker'			=>  new Medicale_Mascot_Nav_Walker
						)
					);
					}
				}
				?>