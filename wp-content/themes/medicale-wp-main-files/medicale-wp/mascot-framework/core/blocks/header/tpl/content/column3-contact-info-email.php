<?php
$current_page_id = medicale_mascot_get_page_id();

//Contact Info
if( $header_mid_column3_content_type_from == "metabox" ) {
	$contact_info_phone = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_mid_column3_contact_info_email", '', $current_page_id );
} else {
	$contact_info_phone = medicale_mascot_get_redux_option( 'header-settings-header-mid-column3-contact-info-email' );
	$contact_info_phone[0] = $contact_info_phone['Title'];
	$contact_info_phone[1] = $contact_info_phone['Subtitle'];
}
?>

<?php if( $iconbox_style == 'big-font-icon' ){ ?>
<div class="media element contact-info">
	<div class="media-left">
	<a href="#">
		<i class="fa fa-envelope-o font-icon text-theme-colored sm-display-block"></i>
	</a>
	</div>
	<div class="media-body">
	<a href="#" class="title"><?php echo $contact_info_phone[0]?></a>
	<h5 class="media-heading subtitle"> <?php echo $contact_info_phone[1]?></h5>
	</div>
</div>
<?php } else { ?>
<div class="media element contact-info small-icon">
	<div class="media-body">
	<a href="#" class="title"><i class="fa fa-envelope-o font-icon text-theme-colored sm-display-block"></i> <?php echo $contact_info_phone[0]?></a>
	<h5 class="media-heading subtitle"> <?php echo $contact_info_phone[1]?></h5>
	</div>
</div>
<?php } ?>