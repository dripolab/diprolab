<div id="top-nav-search-form" class="clearfix">
	<form action="<?php echo esc_url(home_url()) ?>" method="GET">
		<input type="text" name="s" value="" placeholder="<?php echo esc_html__('Type and Press Enter...', 'medicale-wp') ?>" autocomplete="off" />
	</form>
	<a href="#" id="close-search-btn"><i class="icon_close"></i></a>
</div>
