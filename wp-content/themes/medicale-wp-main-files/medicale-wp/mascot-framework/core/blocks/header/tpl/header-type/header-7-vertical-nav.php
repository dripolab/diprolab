	<?php if( $show_header_nav_row ): ?>
	<?php do_action( 'medicale_mascot_before_header_nav' ); ?>
	<div class="header-nav">
		<div class="header-nav-wrapper <?php echo $navigation_color_scheme;?>">
		<div class="menuzord-container header-nav-container <?php echo $navigation_color_scheme;?> <?php echo $header_nav_row_bg_theme_colored; ?>" style="<?php echo $header_nav_row_custom_bgcolor; ?>">
			<div class="<?php echo $header_layout_container_class; ?> position-relative">

			<?php
				/**
				* medicale_mascot_header_nav_container_start hook.
				*
				*/
				do_action( 'medicale_mascot_header_nav_container_start' );
			?>

			<nav id="top-primary-nav" class="menuzord <?php echo $navigation_color_scheme;?>" data-effect="<?php echo $navigation_primary_effect;?>" data-animation="<?php echo $navigation_css3_animation;?>" data-align="right">
			
				<?php
				/**
				* medicale_mascot_header_logo hook.
				*
				* @hooked medicale_mascot_get_header_logo
				*/
				do_action( 'medicale_mascot_header_logo' );
				?>
				
				<?php
				/**
				* medicale_mascot_header_primary_nav hook.
				*
				* @hooked medicale_mascot_get_header_primary_nav
				*/
				do_action( 'medicale_mascot_header_primary_nav' );
				?>

			</nav>
			
			<div class="vertical-nav-sidebar-widget-wrapper">
			<?php if ( is_active_sidebar( 'header-vertical-nav-sidebar' ) ) : ?>
				<?php dynamic_sidebar( 'header-vertical-nav-sidebar' ); ?>
			<?php else: ?>
				<h4><?php esc_html_e( 'This is your Vertical Nav Sidebar!', 'medicale-wp' )?></h4>
				<p><?php echo sprintf( esc_html__( 'Please add your sidebar widgets to this section from %1$sAppearance > Widgets%2$s (Vertical Nav Sidebar).', 'medicale-wp' ), '<strong>', '</strong>')?></p>
			<?php endif; ?>
			</div>
			
			<?php
				/**
				* medicale_mascot_header_nav_container_end hook.
				*
				* @hooked medicale_mascot_get_header_search_form
				*/
				do_action( 'medicale_mascot_header_nav_container_end' );
			?>

			</div>
		</div>
		</div>
	</div>
	<?php do_action( 'medicale_mascot_after_header_nav' ); ?>
	<?php endif; ?>