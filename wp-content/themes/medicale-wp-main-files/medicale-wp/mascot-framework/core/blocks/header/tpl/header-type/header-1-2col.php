	<?php
		/**
		* medicale_mascot_header_top_area hook.
		*
		* @hooked medicale_mascot_get_header_top
		*/
		do_action( 'medicale_mascot_header_top_area' );
	?>

	<?php if( $show_header_nav_row ): ?>
	<?php do_action( 'medicale_mascot_before_header_nav' ); ?>
	<div class="header-nav">
		<div class="header-nav-wrapper navbar-scrolltofixed <?php echo $navigation_color_scheme;?>">
		<div class="menuzord-container header-nav-container <?php echo $navigation_color_scheme;?> <?php echo $header_nav_row_bg_theme_colored; ?>" style="<?php echo $header_nav_row_custom_bgcolor; ?>">
			<div class="<?php echo $header_layout_container_class; ?> position-relative">

			<?php
				/**
				* medicale_mascot_header_nav_container_start hook.
				*
				*/
				do_action( 'medicale_mascot_header_nav_container_start' );
			?>

			<nav id="top-primary-nav" class="menuzord <?php echo $navigation_color_scheme;?>" data-effect="<?php echo $navigation_primary_effect;?>" data-animation="<?php echo $navigation_css3_animation;?>" data-align="right">
			
				<?php
				/**
				* medicale_mascot_header_logo hook.
				*
				* @hooked medicale_mascot_get_header_logo
				*/
				do_action( 'medicale_mascot_header_logo' );
				?>

				<ul class="list-inline nav-side-icon-list">
				<?php
				/**
				* medicale_mascot_header_nav_side_icons hook.
				*
				* @hooked medicale_mascot_get_header_search_icon - 10
				* @hooked medicale_mascot_get_header_mini_cart_icon - 15
				* @hooked medicale_mascot_get_header_side_push_panel_sidebar - 20
				* @hooked medicale_mascot_get_header_nav_custom_button - 25
				*/
				do_action( 'medicale_mascot_header_nav_side_icons' );
				?>
				</ul>
				
				<?php
				/**
				* medicale_mascot_header_primary_nav hook.
				*
				* @hooked medicale_mascot_get_header_primary_nav
				*/
				do_action( 'medicale_mascot_header_primary_nav' );
				?>

			</nav>

			<?php
				/**
				* medicale_mascot_header_nav_container_end hook.
				*
				* @hooked medicale_mascot_get_header_search_form
				*/
				do_action( 'medicale_mascot_header_nav_container_end' );
			?>

			</div>
		</div>
		</div>
	</div>
	<?php do_action( 'medicale_mascot_after_header_nav' ); ?>
	<?php endif; ?>