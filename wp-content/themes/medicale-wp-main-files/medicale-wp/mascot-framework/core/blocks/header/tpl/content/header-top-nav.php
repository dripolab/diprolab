<div class="element">
<?php
if ( has_nav_menu( $header_top_nav ) ) {
	$defaults = array(
		'theme_location'  => $header_top_nav,
		'menu'			=> '',
		'container'		=> 'false',
		'menu_class'		=> 'header-top-nav list-inline',
		'menu_id'		=> '',
		'echo'			=> true,
		'before'			=> '',
		'after'			=> '',
		'link_before'	 => '',
		'link_after'		=> '',
		'depth'			=> 0
	);

	if( !empty( $header_top_nav ) || $header_top_nav != '' ) {
		wp_nav_menu( $defaults );
	}

} else {
	echo '<p>'. sprintf( esc_html__( 'Please Create a Menu from %1$shere%2$s & set it\'s Location to %4$s%3$s - Header Top Navigation%5$s', 'medicale-wp' ), '<a target="_blank" href="' . esc_url( admin_url('nav-menus.php') ) . '">', '</a>', $header_top_nav_col_number, '<strong>', '</strong>') . '</p>';
}
?>
</div>