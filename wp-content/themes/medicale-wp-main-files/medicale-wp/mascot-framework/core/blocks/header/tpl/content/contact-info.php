<?php if( !empty( $contact_info_checkbox ) ): ?>
	<ul class="element list-inline contact-info">
		<?php foreach ($contact_info as $key => $value ) : ?>
			<?php if( isset($contact_info_checkbox[$key]) && $contact_info_checkbox[$key] == 1 ): ?>
				<?php medicale_mascot_get_blocks_template_part( 'contact-info', $key, 'header/tpl/content', $params ); ?>
			<?php endif; ?>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>