<?php if( $header_slider_type != 'no-slider' && $header_slider_position == 'above-header' ) : ?>
	<?php medicale_mascot_get_blocks_template_part( 'header-parts-top-slider', null, 'header/tpl', $params ); ?>
	<?php medicale_mascot_get_blocks_template_part( 'header-parts-header', null, 'header/tpl', $params ); ?>
<?php else : ?>
	<?php medicale_mascot_get_blocks_template_part( 'header-parts-header', null, 'header/tpl', $params ); ?>
	<?php medicale_mascot_get_blocks_template_part( 'header-parts-top-slider', null, 'header/tpl', $params ); ?>
<?php endif; ?>