	<!-- Header -->
	<?php
		/**
		* medicale_mascot_before_header hook.
		*
		*/
		do_action( 'medicale_mascot_before_header' );
	?>
	<header id="header" class="header <?php echo 'header-layout-type-'.$header_layout_type;?>">
		<?php
			/**
			* medicale_mascot_header_start hook.
			*
			*/
			do_action( 'medicale_mascot_header_start' );
		?>

		<?php
			medicale_mascot_get_header_layout_type();
		?>

		<?php
			/**
			* medicale_mascot_header_end hook.
			*
			*/
			do_action( 'medicale_mascot_header_end' );
		?>
	</header>
	<?php
		/**
		* medicale_mascot_after_header hook.
		*
		*/
		do_action( 'medicale_mascot_after_header' );
	?>