	<div class="header-middle sm-text-center">
		<div class="<?php echo $header_layout_container_class; ?>">
		<div class="row">
			<div class="col-md-3">
			<?php
				/**
				* medicale_mascot_header_logo hook.
				*
				* @hooked medicale_mascot_get_header_logo
				*/
				do_action( 'medicale_mascot_header_logo' );
			?>
			</div>
			<div class="col-md-9">
			<div class="header-mid-column3-container">
				<?php medicale_mascot_get_header_middle_column3_content(); ?>
			</div>
			</div>
		</div>
		</div>
	</div>