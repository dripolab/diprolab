
	<?php if( $show_header_nav_row ): ?>
	<?php do_action( 'medicale_mascot_before_header_nav' ); ?>
	<div class="header-nav">
		<div class="header-nav-wrapper navbar-scrolltofixed <?php echo $navigation_color_scheme;?>">
		<div class="menuzord-container header-nav-container <?php echo $navigation_color_scheme;?> <?php echo $header_nav_row_bg_theme_colored; ?>" style="<?php echo $header_nav_row_custom_bgcolor; ?>">
			<div class="<?php echo $header_layout_container_class; ?> position-relative">

			<?php
				/**
				* medicale_mascot_header_nav_container_start hook.
				*
				*/
				do_action( 'medicale_mascot_header_nav_container_start' );
			?>

			<nav id="top-primary-nav" class="menuzord <?php echo $navigation_color_scheme;?>" data-effect="<?php echo $navigation_primary_effect;?>" data-animation="<?php echo $navigation_css3_animation;?>" data-align="right">
			
				<?php
				/**
				* medicale_mascot_header_logo hook.
				*
				* @hooked medicale_mascot_get_header_logo
				*/
				do_action( 'medicale_mascot_header_logo' );
				?>
				


				<div id="side-panel-trigger" class="side-panel-trigger pull-right pt-20 pb-10">
				<a href="#">
				<!-- <i class="fa fa-bars font-24 text-gray"></i> -->
					<div class="hamburger-box">
					<div class="hamburger-inner"></div>
					</div>
				</a>
				</div>
				<div class="side-panel-body-overlay"></div>
				<div id="side-panel-container" class="dark">
				<div class="side-panel-wrap">
					<div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon_close side-panel-trigger-icon"></i></a></div>
					<?php
					/**
					* medicale_mascot_header_logo hook.
					*
					* @hooked medicale_mascot_get_header_logo
					*/
					do_action( 'medicale_mascot_header_logo' );
					?>
					<div class="clearfix"></div>
					<?php
					/**
					* medicale_mascot_header_primary_nav hook.
					*
					* @hooked medicale_mascot_get_header_primary_nav
					*/
					do_action( 'medicale_mascot_header_primary_nav' );
					?>
				</div>
				</div>

			</nav>

			<?php
				/**
				* medicale_mascot_header_nav_container_end hook.
				*
				* @hooked medicale_mascot_get_header_search_form
				*/
				do_action( 'medicale_mascot_header_nav_container_end' );
			?>

			</div>
		</div>
		</div>
	</div>
	<?php do_action( 'medicale_mascot_after_header_nav' ); ?>
	<?php endif; ?>