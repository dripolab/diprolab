<li>
	<div class="woocommerce top-nav-mini-cart-icon-container">
		<div class="top-nav-mini-cart-icon-contents">
			<a class="mini-cart-icon" href="<?php echo wc_get_checkout_url(); ?>" title="<?php esc_html_e( 'View your shopping cart', 'medicale-wp' ); ?>"><i class="icon_cart_alt"></i><span class="items-count"><?php echo sprintf (_n( '%d', '%d', WC()->cart->get_cart_contents_count(), 'medicale-wp' ), WC()->cart->get_cart_contents_count() ); ?></span> <span class="cart-quick-info"><?php echo sprintf (_n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), 'medicale-wp' ), WC()->cart->get_cart_contents_count() ); ?> - <?php echo WC()->cart->get_cart_total(); ?></span></a>

			<div class="dropdown-content">
				<?php woocommerce_mini_cart(); ?>
			</div>
		</div>
	</div>
</li>
