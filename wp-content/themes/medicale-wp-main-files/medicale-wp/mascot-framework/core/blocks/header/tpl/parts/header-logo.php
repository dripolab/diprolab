
			<a class="menuzord-brand site-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<?php if( !$use_logo ): ?>
				<?php echo $site_brand; ?>
				<?php else: ?>
					<?php if( !$use_switchable_logo ): ?>
					<?php
						list( $logo_default_width, $logo_default_height ) = getimagesize( $logo_default );
					?>
					<img class="logo-default logo-1x" src="<?php echo $logo_default; ?>" width="<?php echo $logo_default_width; ?>" height="<?php echo $logo_default_height; ?>" alt="">
					<img class="logo-default logo-2x retina" src="<?php echo $logo_default_2x; ?>" width="<?php echo $logo_default_width; ?>" height="<?php echo $logo_default_height; ?>" alt="">
					<?php else: ?>
					<?php
						list( $logo_light_width, $logo_light_height ) = getimagesize( $logo_light );
						list( $logo_dark_width, $logo_dark_height ) = getimagesize( $logo_dark );
					?>
					<img class="logo-light logo-1x" src="<?php echo $logo_light; ?>" width="<?php echo $logo_light_width; ?>" height="<?php echo $logo_light_height; ?>" alt="">
					<img class="logo-light logo-2x retina" src="<?php echo $logo_light_2x; ?>" width="<?php echo $logo_light_width; ?>" height="<?php echo $logo_light_height; ?>" alt="">
					<img class="logo-dark logo-1x" src="<?php echo $logo_dark; ?>" width="<?php echo $logo_dark_width; ?>" height="<?php echo $logo_dark_height; ?>" alt="">
					<img class="logo-dark logo-2x retina" src="<?php echo $logo_dark_2x; ?>" width="<?php echo $logo_dark_width; ?>" height="<?php echo $logo_dark_height; ?>" alt="">
					<?php endif; ?>
				<?php endif; ?>
			</a>