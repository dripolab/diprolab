
	<?php
	/**
	* medicale_mascot_before_top_sliders_container hook.
	*
	*/
	do_action( 'medicale_mascot_before_top_sliders_container' );
	?>
	<div class="top-sliders-container">
		<?php	if ( is_front_page() && is_home() ) {?>
print '<h1>red</h1>';
<?php	} elseif ( is_front_page()){?>
	<link rel="stylesheet" href="<?php bloginfo('template_url')?>/customCode/dist/css/swiper.min.css">
	<style>
	.swiper-slide.swiper-slide-active img{
		width: 100%;
		height: auto;
	}
	</style>

	<!-- Swiper -->
	<div class="swiper-container">
		<div class="swiper-wrapper">
			<?php
				$query = new WP_Query(array('cat' => 85));

				if ($query -> have_posts() ):
			?>
		<?php while ( $query-> have_posts() ) : $query->the_post(); ?>
			<div class="swiper-slide"><?php the_content(); ?></div>
		<?php endwhile; ?>

	<?php endif; ?>

		</div>
		<!-- Add Pagination -->
		<div class="swiper-pagination"></div>
		<!-- Add Navigation -->
		<div class="swiper-button-prev"></div>
		<div class="swiper-button-next"></div>
	</div>

	<!-- Swiper JS -->
	<script src="<?php bloginfo('template_url')?>/customCode/dist/js/swiper.min.js"></script>


	<!-- Include plugin after Swiper -->
	<script>
	$(document).ready(function(){
		var height = $(window).height()-72;
		$('.swiper-container').height(height);
		$('.swiper-wrapper').height(height);
		$('.swiper-slide').height(height);
	});
		/* ========
		Debugger plugin, simple demo plugin to console.log some of callbacks
		======== */
		var myPlugin = {
			name: 'debugger',
			params: {
				debugger: false,
			},
			on: {
				init: function () {
					if (!this.params.debugger) return;
					console.log('init');
				},
				click: function (e) {
					if (!this.params.debugger) return;
					console.log('click');
				},
				tap: function (e) {
					if (!this.params.debugger) return;
					console.log('tap');
				},
				doubleTap: function (e) {
					if (!this.params.debugger) return;
					console.log('doubleTap');
				},
				sliderMove: function (e) {
					if (!this.params.debugger) return;
					console.log('sliderMove');
				},
				slideChange: function () {
					if (!this.params.debugger) return;
					console.log('slideChange', this.previousIndex, '->', this.activeIndex);
				},
				slideChangeTransitionStart: function () {
					if (!this.params.debugger) return;
					console.log('slideChangeTransitionStart');
				},
				slideChangeTransitionEnd: function () {
					if (!this.params.debugger) return;
					console.log('slideChangeTransitionEnd');
				},
				transitionStart: function () {
					if (!this.params.debugger) return;
					console.log('transitionStart');
				},
				transitionEnd: function () {
					if (!this.params.debugger) return;
					console.log('transitionEnd');
				},
				fromEdge: function () {
					if (!this.params.debugger) return;
					console.log('fromEdge');
				},
				reachBeginning: function () {
					if (!this.params.debugger) return;
					console.log('reachBeginning');
				},
				reachEnd: function () {
					if (!this.params.debugger) return;
					console.log('reachEnd');
				},
			},
		};
	</script>

	<script>
		// Install Plugin To Swiper
		Swiper.use(myPlugin);

		// Init Swiper
		var swiper = new Swiper('.swiper-container', {
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			autoplay: {
				delay: 3000,
			},
			// Enable debugger
			debugger: true,
		});
	</script>
<?php	} elseif ( is_home()){?>

<?php	} else {?>


<?php	}?>


	</div>
	<?php
	/**
	* medicale_mascot_after_top_sliders_container hook.
	*
	*/

	do_action( 'medicale_mascot_after_top_sliders_container' );
	?>
