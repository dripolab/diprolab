<div class="element">
<?php 
if ( is_user_logged_in() ){
	$current_user = wp_get_current_user();
	$username = $current_user->display_name;
	echo 'Welcome '.$username.', <a href="'.esc_url( wp_logout_url() ).'">Logout</a>';
} else {
	echo '<a href="'.esc_url( wp_login_url() ).'">Login</a> | <a href="'.esc_url( wp_registration_url() ).'">Register</a> ';
}
?>
</div>