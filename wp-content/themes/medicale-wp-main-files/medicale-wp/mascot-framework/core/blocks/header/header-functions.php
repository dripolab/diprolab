<?php


if(!function_exists('medicale_mascot_get_header_parts')) {
	/**
	 * Function that Renders Header HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_header_parts() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();


		//Header Visibility
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_visibility", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['header_visibility'] = $temp_meta_value;
		} else {
			$params['header_visibility'] = medicale_mascot_get_redux_option( 'header-settings-choose-header-visibility', true );
		}

		if( !$params['header_visibility'] ) {
			return;
		}

		$params['header_slider_type'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_slider_select_slider_type", '', $current_page_id );
		$params['header_slider_position'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_slider_position", '', $current_page_id );


		//Header Layout Type
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_layout_type", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['header_layout_type'] = $temp_meta_value;
		} else {
			$params['header_layout_type'] = medicale_mascot_get_redux_option( 'header-settings-choose-header-layout-type', 'header-1-2col' );
		}
		

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'header-parts', null, 'header/tpl', $params );
		
		return $html;
	}
}


if(!function_exists('medicale_mascot_get_header_layout_type')) {
	/**
	 * Function that Renders Header Layout Type HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_header_layout_type() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();

		/*
		* Header Layout
		*/
		
		//Header Layout Type
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_layout_type", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['header_layout_type'] = $temp_meta_value;
		} else {
			$params['header_layout_type'] = medicale_mascot_get_redux_option( 'header-settings-choose-header-layout-type', 'header-1-2col' );
		}


		//Header Container
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_layout_type_container", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['header_layout_container_class'] = $temp_meta_value;
		} else {
			$params['header_layout_container_class'] = medicale_mascot_get_redux_option( 'header-settings-header-layout-type-container', 'container' );
		}

		
		
		//Header Behaviour
		//check if meta value is provided for this page or then get it from theme options
		/*$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_layout_behaviour", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['header_layout_behaviour'] = $temp_meta_value;
		} else {
			$params['header_layout_behaviour'] = medicale_mascot_get_redux_option( 'header-settings-header-layout-type-behaviour'];
		}*/
		




		/*
		* Header Navigation Row
		*/
		//if Use Theme Color in Background enabled
		$params['header_nav_row_bg_theme_colored'] = '';
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_nav_row_bgcolor_use_themecolor", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			if( $temp_meta_value == "1" ) {
				$params['header_nav_row_bg_theme_colored'] = 'bg-theme-colored';
			}
		} else {
			if( medicale_mascot_get_redux_option( 'header-settings-navigation-bgcolor-use-themecolor' ) ) {
				$params['header_nav_row_bg_theme_colored'] = 'bg-theme-colored';
			}
		}

		//Header Navigation Row Custom Background Color
		$params['header_nav_row_custom_bgcolor'] = '';
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_nav_row_bgcolor_use_themecolor", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value == "0" ) {
			$params['header_nav_row_custom_bgcolor'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_nav_row_custom_bgcolor", '', $current_page_id );
			if( ! medicale_mascot_metabox_opt_val_is_empty( $params['header_nav_row_custom_bgcolor'] ) ) {
				$params['header_nav_row_custom_bgcolor'] = 'background-color: ' . $params['header_nav_row_custom_bgcolor'] . '; ';
			}
		}



		//Navigation Skin
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_navigation_skin", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['navigation_skin'] = $temp_meta_value;
		} else {
			$params['navigation_skin'] = esc_attr( medicale_mascot_get_redux_option( 'header-settings-navigation-skin' ) );
		}

		//register and enque menuzord css skin
		if( $params['navigation_skin'] && $params['navigation_skin'] != 'default' ) {
			wp_register_style( 'mascot-menuzord-navigation-skin', MASCOT_TEMPLATE_URI . '/assets/js/plugins/menuzord/css/skins/menuzord-'.$params['navigation_skin'].'.css', array(), MASCOT_THEME_VERSION );
			wp_enqueue_style( array( 'mascot-menuzord-navigation-skin' ) );
		}


		//Header Navigation Color Scheme
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_navigation_color_scheme", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['navigation_color_scheme'] = $temp_meta_value;
		} else {
			$params['navigation_color_scheme'] = esc_attr( medicale_mascot_get_redux_option( 'header-settings-navigation-color-scheme' ) );
		}


		//Header Navigation Primary Effect
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_navigation_primary_effect", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['navigation_primary_effect'] = $temp_meta_value;
		} else {
			$params['navigation_primary_effect'] = esc_attr( medicale_mascot_get_redux_option( 'header-settings-navigation-primary-effect' ) );
		}

		//Header Navigation CSS3 Animation
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_navigation_css3_animation", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['navigation_css3_animation'] = $temp_meta_value;
		} else {
			$params['navigation_css3_animation'] = esc_attr( medicale_mascot_get_redux_option( 'header-settings-navigation-css3-animation' ) );
		}


		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_show_header_nav_row", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['show_header_nav_row'] = $temp_meta_value;
		} else {
			$params['show_header_nav_row'] = medicale_mascot_get_redux_option( 'header-settings-navigation-show-header-nav-row', true );
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( $params['header_layout_type'], null, 'header/tpl/header-type', $params );

		return $html;
	}
}


if(!function_exists('medicale_mascot_header_mobile_vertical_nav_add_class_to_body')) {
	function medicale_mascot_header_mobile_vertical_nav_add_class_to_body ( $classes ) {
		$current_page_id = medicale_mascot_get_page_id();
		//Header Layout Type
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_layout_type", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$header_layout_type = $temp_meta_value;
		} else {
			$header_layout_type = medicale_mascot_get_redux_option( 'header-settings-choose-header-layout-type', 'header-1-2col' );
		}

		if( $header_layout_type == 'header-5-mobile-nav' ) {
			$classes[] = 'menu-full-page';
		} else if ( $header_layout_type == 'header-7-vertical-nav' ) {
			$classes[] = 'vertical-nav';
		}

		return $classes;
	}
	add_filter( 'body_class', 'medicale_mascot_header_mobile_vertical_nav_add_class_to_body' );
}


if(!function_exists('medicale_mascot_get_header_top')) {
	/**
	 * Function that Renders Header Top HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_header_top() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();

		//Show Header Top
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_show_header_top", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['show_header_top'] = $temp_meta_value;
		} else {
			$params['show_header_top'] = medicale_mascot_get_redux_option( 'header-settings-show-header-top' );
		}
		

		if( !$params['show_header_top'] ) {
			return;
		}
		
		//if Use Theme Color in Background enabled
		$params['header_top_bg_theme_colored'] = '';
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_bgcolor_use_themecolor", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			if( $temp_meta_value == "1" ) {
				$params['header_top_bg_theme_colored'] = 'bg-theme-colored';
			}
		} else {
			if( medicale_mascot_get_redux_option( 'header-settings-header-top-bgcolor-use-themecolor' ) ) {
				$params['header_top_bg_theme_colored'] = 'bg-theme-colored';
			}
		}

		//Header Top Custom Background Color
		$params['header_top_custom_bgcolor'] = '';
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_bgcolor_use_themecolor", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value == "0" ) {
			$params['header_top_custom_bgcolor'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_custom_bgcolor", '', $current_page_id );
			if( ! medicale_mascot_metabox_opt_val_is_empty( $params['header_top_custom_bgcolor'] ) ) {
				$params['header_top_custom_bgcolor'] = 'background-color: ' . $params['header_top_custom_bgcolor'] . '; ';
			}
		}


		//Header Container
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_layout_type_container", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['header_layout_container_class'] = $temp_meta_value;
		} else {
			$params['header_layout_container_class'] = medicale_mascot_get_redux_option( 'header-settings-header-layout-type-container', 'container' );
		}

		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'header-top', null, 'header/tpl/parts', $params );
		
		return $html;
	}
	add_action( 'medicale_mascot_header_top_area', 'medicale_mascot_get_header_top' );
}


if(!function_exists('medicale_mascot_get_header_logo')) {
	/**
	 * Function that Renders Header Logo HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_header_logo() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();

		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_logo_site_brand", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "" ) {
			$params['site_brand'] = trim($temp_meta_value);
		} else {
			$params['site_brand'] = esc_html( medicale_mascot_get_redux_option( 'logo-settings-site-brand', get_bloginfo( 'name' ) ) );
		}


		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_use_logo", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['use_logo'] = $temp_meta_value;
		} else {
			$params['use_logo'] = medicale_mascot_get_redux_option( 'logo-settings-want-to-use-logo', false );
		}


		if( $params['use_logo'] ) {
			//check if meta value is provided for this page or then get it from theme options
			$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_logo_default", '', $current_page_id );
			if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) ) {
				$params['logo_default'] = medicale_mascot_metabox_get_image_advanced_field_url( $temp_meta_value );
			} else {
				$params['logo_default'] = esc_url( medicale_mascot_get_redux_option( 'logo-settings-logo-default', false, 'url' ) );
			}

			//check if meta value is provided for this page or then get it from theme options
			$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_logo_default_2x", '', $current_page_id );
			if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) ) {
				$params['logo_default_2x'] = medicale_mascot_metabox_get_image_advanced_field_url( $temp_meta_value );
			} else {
				$params['logo_default_2x'] = esc_url( medicale_mascot_get_redux_option( 'logo-settings-logo-default-2x', false, 'url' ) );
			}


			//check if meta value is provided for this page or then get it from theme options
			$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_use_switchable_logo", '', $current_page_id );
			if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
				$params['use_switchable_logo'] = $temp_meta_value;
			} else {
				$params['use_switchable_logo'] = medicale_mascot_get_redux_option( 'logo-settings-switchable-logo' );
			}

			

			if( $params['use_switchable_logo'] ) {
				//logo light
				//check if meta value is provided for this page or then get it from theme options
				$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_logo_light", '', $current_page_id );
				if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) ) {
					$params['logo_light'] = medicale_mascot_metabox_get_image_advanced_field_url( $temp_meta_value );
				} else {
					$params['logo_light'] = esc_url( medicale_mascot_get_redux_option( 'logo-settings-logo-light', false, 'url' ) );
				}

				//check if meta value is provided for this page or then get it from theme options
				$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_logo_light_2x", '', $current_page_id );
				if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) ) {
					$params['logo_light_2x'] = medicale_mascot_metabox_get_image_advanced_field_url( $temp_meta_value );
				} else {
					$params['logo_light_2x'] = esc_url( medicale_mascot_get_redux_option( 'logo-settings-logo-light-2x', false, 'url' ) );
				}




				//logo dark
				//check if meta value is provided for this page or then get it from theme options
				$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_logo_dark", '', $current_page_id );
				if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) ) {
					$params['logo_dark'] = medicale_mascot_metabox_get_image_advanced_field_url( $temp_meta_value );
				} else {
					$params['logo_dark'] = esc_url( medicale_mascot_get_redux_option( 'logo-settings-logo-dark', false, 'url' ) );
				}

				//logo dark
				//check if meta value is provided for this page or then get it from theme options
				$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_logo_dark_2x", '', $current_page_id );
				if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) ) {
					$params['logo_dark_2x'] = medicale_mascot_metabox_get_image_advanced_field_url( $temp_meta_value );
				} else {
					$params['logo_dark_2x'] = esc_url( medicale_mascot_get_redux_option( 'logo-settings-logo-dark-2x', false, 'url' ) );
				}

			}

			//check if meta value is provided for this page or then get it from theme options
			$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_logo_maximum_height", '', $current_page_id );
			if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "" ) {
				$params['maximum_logo_height'] = $temp_meta_value;
			} else {
				$params['maximum_logo_height'] = medicale_mascot_get_redux_option( 'logo-settings-maximum-logo-height' );
			}
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'header-logo', null, 'header/tpl/parts', $params );
		
		return $html;
	}
	add_action( 'medicale_mascot_header_logo', 'medicale_mascot_get_header_logo' );
}


if(!function_exists('medicale_mascot_get_header_primary_nav')) {
	/**
	 * Function that Renders Header Primary Nav HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_header_primary_nav() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();
		

		//Header Layout Type
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_layout_type", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['header_layout_type'] = $temp_meta_value;
		} else {
			$params['header_layout_type'] = medicale_mascot_get_redux_option( 'header-settings-choose-header-layout-type', 'header-1-2col' );
		}

		
		//Primary Navigation Menu Only For This Page
		$params['custom_primary_nav_menu'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_navigation_custom_primary_nav_menu", '', $current_page_id );
		$params['enable_one_page_nav_scrolling_effect'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_navigation_enable_one_page_nav_scrolling_effect", '', $current_page_id );

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'header-primary-nav', null, 'header/tpl/parts', $params );
		
		return $html;
	}
	add_action( 'medicale_mascot_header_primary_nav', 'medicale_mascot_get_header_primary_nav' );
}


if (!function_exists('medicale_mascot_switchable_logo_add_class_to_body')) {
	/**
	 * Add classes to body
	 */
	function medicale_mascot_switchable_logo_add_class_to_body ( $classes ) {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();

		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_use_switchable_logo", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['use_switchable_logo'] = $temp_meta_value;
		} else {
			$params['use_switchable_logo'] = medicale_mascot_get_redux_option( 'logo-settings-switchable-logo' );
		}
		
		if( $params['use_switchable_logo'] ) {
			$classes[] = 'switchable-logo';
		}

		return $classes;
	}
	
	add_filter( 'body_class', 'medicale_mascot_switchable_logo_add_class_to_body' );
}


if (!function_exists('medicale_mascot_get_header_top_columns_layout')) {
	/**
	 * Return Header Top Columns Layout HTML
	 */
	function medicale_mascot_get_header_top_columns_layout() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();

		//Header Top Columns Layout
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_columns_layout", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['header_top_columns_layout'] = $temp_meta_value;
		} else {
			$params['header_top_columns_layout'] = medicale_mascot_get_redux_option( 'header-settings-header-top-columns-layout' );
		}



		//Header Top Column 1 - Text Alignment
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column1_text_align", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['header_top_column1_text_alignment'] = $temp_meta_value .' sm-text-center';
		} else {
			$params['header_top_column1_text_alignment'] = esc_attr( medicale_mascot_get_redux_option( 'header-settings-header-top-column1-text-align' ) ) .' sm-text-center';
		}



		//Header Top Column 2 - Text Alignment
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column2_text_align", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['header_top_column2_text_alignment'] = $temp_meta_value .' sm-text-center';
		} else {
			$params['header_top_column2_text_alignment'] = esc_attr( medicale_mascot_get_redux_option( 'header-settings-header-top-column2-text-align' ) ) . ' sm-text-center';
		}


		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'header-top-sidebar-columns-' . $params['header_top_columns_layout'], null, 'header/tpl/sidebar-columns', $params );
		
		return $html;
	}
}


if(!function_exists('medicale_mascot_get_header_menu')) {
	/**
	 * Function that Renders Header Menu HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_header_menu() {
		$params = array();

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'header-'.medicale_mascot_get_redux_option( 'header-settings-choose-header-navigation-type' ), null, 'header/tpl/header-type', $params );
		
		return $html;
	}
}



if (!function_exists('medicale_mascot_get_header_top_column1_content')) {
	/**
	 * Return Header Top Column1 Content HTML
	 */
	function medicale_mascot_get_header_top_column1_content() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();
		$html = '';

		//Header Top Column 1 (Left Widget) - Content Types
		//check if meta value is provided for this page or then get it from theme options
		$params['header_top_column1_content_type_from'] = '';
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column1_content", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value[0] != "inherit" ) {
			$params['header_top_column1_content_type'] = $temp_meta_value;
			$params['header_top_column1_content_type_from'] = 'metabox';
		} else {
			$params['header_top_column1_content_type'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column1-content' );
			$params['header_top_column1_content_type_from'] = 'redux';
		}

		//foreach content type
		foreach ( $params['header_top_column1_content_type'] as $each_content_type ) {
			
			switch ( $each_content_type ) {
				case 'contact-info':
					# code...
					//Contact Info
					if( $params['header_top_column1_content_type_from'] == "metabox" ) {
						$params['contact_info'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column1_contact_info", '', $current_page_id );
						$params['contact_info_checkbox'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column1_contact_info_checkbox", '', $current_page_id );

						//add static key to $params['contact_info']
						$contact_info_array = array();
						$contact_info_array['phone'] = $params['contact_info'][0];
						$contact_info_array['email'] = $params['contact_info'][1];
						$contact_info_array['address'] = $params['contact_info'][2];
						$contact_info_array['opening-hours'] = $params['contact_info'][3];
						$params['contact_info'] = $contact_info_array;

						//add key and set value to 1
						$contact_info_checkbox_array = array();
						foreach ($params['contact_info_checkbox'] as $key => $value) {
							$contact_info_checkbox_array[$value] = 1;
						}
						$params['contact_info_checkbox'] = $contact_info_checkbox_array;

					} else {
						$params['contact_info'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column1-contact-info' );
						$params['contact_info_checkbox'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column1-contact-info-checkbox' );
					}

					break;

				case 'custom-text':
					# code...
					//Custom Text
					if( $params['header_top_column1_content_type_from'] == "metabox" ) {
						$params['custom_text'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column1_custom_text", '', $current_page_id );
					} else {
						$params['custom_text'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column1-custom-text' );
					}
					break;

				case 'custom-button':
					# code...
					//Custom Text
					if( $params['header_top_column1_content_type_from'] == "metabox" ) {
						$params['custom_button'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column1_custom_button", '', $current_page_id );
						$params['custom_button_design_style'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column1_custom_button_design_style", '', $current_page_id );
						$params['custom_button_theme_colored'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column1_custom_button_theme_colored", '', $current_page_id );

						//add static key to $params['custom_button']
						$custom_button_array = array();
						$custom_button_array['Button Title'] = $params['custom_button'][0];
						$custom_button_array['Button Link'] = $params['custom_button'][1];
						$params['custom_button'] = $custom_button_array;
					} else {
						$params['custom_button'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column1-custom-button' );
						$params['custom_button_design_style'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column1-custom-button-design-style' );
						$params['custom_button_theme_colored'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column1-custom-button-theme-colored' );
					}
					break;

				case 'header-top-nav':
					# code...
					//Header Top Navigatin
					$params['header_top_nav_col_number'] = esc_html__( "Columns 1", 'medicale-wp' );
					$params['header_top_nav'] = 'column1-header-top-nav';
					break;

				case 'social-links':
					# code...
					//Enabled social links
					if( $params['header_top_column1_content_type_from'] == "metabox" ) {
						$params['social_links'] = medicale_mascot_get_redux_option( 'social-links-ordering', false, 'Enabled' );
						$params['social_links_icon_color'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column1_social_links_color", '', $current_page_id );
						$params['social_links_icon_style'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column1_social_links_icon_style", '', $current_page_id );
						$params['social_links_icon_border_style'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column1_social_links_icon_border_style", '', $current_page_id );
						$params['social_links_icon_theme_colored'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column1_social_links_icon_theme_colored", '', $current_page_id );
					} else {
						$params['social_links'] = medicale_mascot_get_redux_option( 'social-links-ordering', false, 'Enabled' );
						$params['social_links_icon_color'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column1-social-links-color' );
						$params['social_links_icon_style'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column1-social-links-icon-style' );
						$params['social_links_icon_border_style'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column1-social-links-icon-border-style' );
						$params['social_links_icon_theme_colored'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column1-social-links-theme-colored' );
					}
					break;

				case 'search-box':
					# code...
					break;

				case 'login-register':
					# code...
					break;

				case 'wpml-languages':
					# code...
					break;

				case 'checkout-button':
					# code...
					break;
				
				default:
					# code...
					break;
			}

			//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
			$html .= medicale_mascot_get_blocks_template_part( $each_content_type, null, 'header/tpl/content', $params );

		}//end foreach content type
		
		return $html;
	}
}



if (!function_exists('medicale_mascot_get_header_top_column2_content')) {
	/**
	 * Return Header Top column2 Content HTML
	 */
	function medicale_mascot_get_header_top_column2_content() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();
		$html = '';

		//Header Top Column 2 (Right Widget) - Content Types
		//check if meta value is provided for this page or then get it from theme options
		$params['header_top_column2_content_type_from'] = '';
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column2_content", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value[0] != "inherit" ) {
			$params['header_top_column2_content_type'] = $temp_meta_value;
			$params['header_top_column2_content_type_from'] = 'metabox';
		} else {
			$params['header_top_column2_content_type'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column2-content' );
			$params['header_top_column2_content_type_from'] = 'redux';
		}

		//foreach content type
		foreach ( $params['header_top_column2_content_type'] as $each_content_type ) {
			
			switch ( $each_content_type ) {
				case 'contact-info':
					# code...
					//Contact Info
					if( $params['header_top_column2_content_type_from'] == "metabox" ) {
						$params['contact_info'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column2_contact_info", '', $current_page_id );
						$params['contact_info_checkbox'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column2_contact_info_checkbox", '', $current_page_id );

						//add static key to $params['contact_info']
						$contact_info_array = array();
						$contact_info_array['phone'] = $params['contact_info'][0];
						$contact_info_array['email'] = $params['contact_info'][1];
						$contact_info_array['address'] = $params['contact_info'][2];
						$contact_info_array['opening-hours'] = $params['contact_info'][3];
						$params['contact_info'] = $contact_info_array;

						//add key and set value to 1
						$contact_info_checkbox_array = array();
						foreach ($params['contact_info_checkbox'] as $key => $value) {
							$contact_info_checkbox_array[$value] = 1;
						}
						$params['contact_info_checkbox'] = $contact_info_checkbox_array;

					} else {
						$params['contact_info'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column2-contact-info' );
						$params['contact_info_checkbox'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column2-contact-info-checkbox' );
					}

					break;

				case 'custom-text':
					# code...
					//Custom Text
					if( $params['header_top_column2_content_type_from'] == "metabox" ) {
						$params['custom_text'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column2_custom_text", '', $current_page_id );
					} else {
						$params['custom_text'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column2-custom-text' );
					}
					break;

				case 'custom-button':
					# code...
					//Custom Text
					if( $params['header_top_column2_content_type_from'] == "metabox" ) {
						$params['custom_button'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column2_custom_button", '', $current_page_id );
						$params['custom_button_design_style'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column2_custom_button_design_style", '', $current_page_id );
						$params['custom_button_theme_colored'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column2_custom_button_theme_colored", '', $current_page_id );

						//add static key to $params['custom_button']
						$custom_button_array = array();
						$custom_button_array['Button Title'] = $params['custom_button'][0];
						$custom_button_array['Button Link'] = $params['custom_button'][1];
						$params['custom_button'] = $custom_button_array;
					} else {
						$params['custom_button'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column2-custom-button' );
						$params['custom_button_design_style'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column2-custom-button-design-style' );
						$params['custom_button_theme_colored'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column2-custom-button-theme-colored' );
					}
					break;

				case 'header-top-nav':
					# code...
					//Header Top Navigatin
					$params['header_top_nav_col_number'] = esc_html__( "Columns 2", 'medicale-wp' );
					$params['header_top_nav'] = 'column2-header-top-nav';
					break;

				case 'social-links':
					# code...
					//Enabled social links
					if( $params['header_top_column2_content_type_from'] == "metabox" ) {
						$params['social_links'] = medicale_mascot_get_redux_option( 'social-links-ordering', false, 'Enabled' );
						$params['social_links_icon_color'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column2_social_links_color", '', $current_page_id );
						$params['social_links_icon_style'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column2_social_links_icon_style", '', $current_page_id );
						$params['social_links_icon_border_style'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column2_social_links_icon_border_style", '', $current_page_id );
						$params['social_links_icon_theme_colored'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_top_column2_social_links_icon_theme_colored", '', $current_page_id );
					} else {
						$params['social_links'] = medicale_mascot_get_redux_option( 'social-links-ordering', false, 'Enabled' );
						$params['social_links_icon_color'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column2-social-links-color' );
						$params['social_links_icon_style'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column2-social-links-icon-style' );
						$params['social_links_icon_border_style'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column2-social-links-icon-border-style' );
						$params['social_links_icon_theme_colored'] = medicale_mascot_get_redux_option( 'header-settings-header-top-column2-social-links-theme-colored' );
					}
					break;

				case 'search-box':
					# code...
					break;

				case 'login-register':
					# code...
					break;

				case 'wpml-languages':
					# code...
					break;

				case 'checkout-button':
					# code...
					break;
				
				default:
					# code...
					break;
			}

			//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
			$html .= medicale_mascot_get_blocks_template_part( $each_content_type, null, 'header/tpl/content', $params );

		}//end foreach content type
		
		return $html;
	}
}




if(!function_exists('medicale_mascot_get_header_mid')) {
	/**
	 * Function that Renders Header Mid HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_header_mid() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();

		
		//Header Container
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_layout_type_container", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['header_layout_container_class'] = $temp_meta_value;
		} else {
			$params['header_layout_container_class'] = medicale_mascot_get_redux_option( 'header-settings-header-layout-type-container', 'container' );
		}

		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'header-mid', null, 'header/tpl/parts', $params );
		
		return $html;
	}
	add_action( 'medicale_mascot_header_mid_area', 'medicale_mascot_get_header_mid' );
}


if (!function_exists('medicale_mascot_get_header_middle_column3_content')) {
	/**
	 * Return Header Middle column3 Content HTML
	 */
	function medicale_mascot_get_header_middle_column3_content() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();
		$html = '';

		//Header Top Column 2 (Right Widget) - Content Types
		//check if meta value is provided for this page or then get it from theme options
		$params['header_mid_column3_content_type_from'] = '';
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_mid_column3_content", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value[0] != "inherit" ) {
			$params['header_mid_column3_content_type'] = $temp_meta_value;
			$params['header_mid_column3_content_type_from'] = 'metabox';
		} else {
			$params['header_mid_column3_content_type'] = medicale_mascot_get_redux_option( 'header-settings-header-mid-column3-content' );
			$params['header_mid_column3_content_type_from'] = 'redux';
		}

		//foreach content type
		foreach ( $params['header_mid_column3_content_type'] as $each_content_type ) {
			switch ( $each_content_type ) {
				case 'column3-contact-info':
					# code...
					//Contact Info
					if( $params['header_mid_column3_content_type_from'] == "metabox" ) {
						$params['contact_info_checkbox'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_mid_column3_contact_info_checkbox", '', $current_page_id );

						//contact info array
						$contact_info_array = array();
						$contact_info_array['phone'] = 0;
						$contact_info_array['email'] = 0;
						$contact_info_array['address'] = 0;
						$contact_info_array['opening-hours'] = 0;

						//add key and set value to 1
						foreach ($params['contact_info_checkbox'] as $key => $value) {
							$contact_info_array[$value] = 1;
						}

						$params['contact_info_checkbox'] = $contact_info_array;

					} else {
						$params['contact_info_checkbox'] = medicale_mascot_get_redux_option( 'header-settings-header-mid-column3-contact-info-checkbox' );
					}

					//Icon Box Style
					//check if meta value is provided for this page or then get it from theme options
					$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_mid_column3_contact_info_iconbox_style", '', $current_page_id );
					if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
						$params['iconbox_style'] = $temp_meta_value;
					} else {
						$params['iconbox_style'] = medicale_mascot_get_redux_option( 'header-settings-header-mid-column3-contact-info-iconbox-style' );
					}
					break;

				case 'column3-custom-text':
					# code...
					//Custom Text
					if( $params['header_mid_column3_content_type_from'] == "metabox" ) {
						$params['custom_text'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_mid_column3_custom_text", '', $current_page_id );
					} else {
						$params['custom_text'] = medicale_mascot_get_redux_option( 'header-settings-header-mid-column3-custom-text' );
					}
					break;

				case 'column3-custom-button':
					# code...
					//Custom Text
					if( $params['header_mid_column3_content_type_from'] == "metabox" ) {
						$params['custom_button'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_mid_column3_custom_button", '', $current_page_id );
						$params['custom_button_design_style'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_mid_column3_custom_button_design_style", '', $current_page_id );
						$params['custom_button_theme_colored'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_mid_column3_custom_button_theme_colored", '', $current_page_id );

						//add static key to $params['custom_button']
						$custom_button_array = array();
						$custom_button_array['Button Title'] = $params['custom_button'][0];
						$custom_button_array['Button Link'] = $params['custom_button'][1];
						$params['custom_button'] = $custom_button_array;
					} else {
						$params['custom_button'] = medicale_mascot_get_redux_option( 'header-settings-header-mid-column3-custom-button' );
						$params['custom_button_design_style'] = medicale_mascot_get_redux_option( 'header-settings-header-mid-column3-custom-button-design-style' );
						$params['custom_button_theme_colored'] = medicale_mascot_get_redux_option( 'header-settings-header-mid-column3-custom-button-theme-colored' );
					}
					break;

				case 'column3-social-links':
					# code...
					//Enabled social links
					if( $params['header_mid_column3_content_type_from'] == "metabox" ) {
						$params['social_links'] = medicale_mascot_get_redux_option( 'social-links-ordering', false, 'Enabled' );
						$params['social_links_icon_color'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_mid_column3_social_links_color", '', $current_page_id );
						$params['social_links_icon_style'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_mid_column3_social_links_icon_style", '', $current_page_id );
						$params['social_links_icon_border_style'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_mid_column3_social_links_icon_border_style", '', $current_page_id );
						$params['social_links_icon_theme_colored'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_mid_column3_social_links_icon_theme_colored", '', $current_page_id );
					} else {
						$params['social_links'] = medicale_mascot_get_redux_option( 'social-links-ordering', false, 'Enabled' );
						$params['social_links_icon_color'] = medicale_mascot_get_redux_option( 'header-settings-header-mid-column3-social-links-color' );
						$params['social_links_icon_style'] = medicale_mascot_get_redux_option( 'header-settings-header-mid-column3-social-links-icon-style' );
						$params['social_links_icon_border_style'] = medicale_mascot_get_redux_option( 'header-settings-header-mid-column3-social-links-icon-border-style' );
						$params['social_links_icon_theme_colored'] = medicale_mascot_get_redux_option( 'header-settings-header-mid-column3-social-links-theme-colored' );
					}
					break;

				case 'column3-search-box':
					# code...
					break;
				
				default:
					# code...
					break;
			}

			//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
			$html .= medicale_mascot_get_blocks_template_part( $each_content_type, null, 'header/tpl/content', $params );

		}//end foreach content type
		
		return $html;
	}
}


if (!function_exists('medicale_mascot_get_top_main_slider')) {
	/**
	 * Return Top Main Slider do_shortcode
	 */
	function medicale_mascot_get_top_main_slider() {
		$current_page_id = medicale_mascot_get_page_id();
		$html = '';

		$slider_type = rwmb_meta( 'medicale_mascot_' . "page_metabox_slider_select_slider_type", '', $current_page_id );

		switch ( $slider_type ) {
			case 'rev-slider':
				# code...
				if ( class_exists( 'RevSlider' ) ) {
					$rev_slider_alias = rwmb_meta( 'medicale_mascot_' . "page_metabox_slider_select_rev_slider", '', $current_page_id );
					if( ! medicale_mascot_metabox_opt_val_is_empty( $rev_slider_alias ) && $rev_slider_alias != '0' ) {
						$html = do_shortcode( '[rev_slider alias="'.$rev_slider_alias.'"]' );
					}
					break;
				}
			
			case 'layer-slider':
				# code...
				if ( class_exists( 'LS_Sliders' ) ) {
					$rev_slider_alias = rwmb_meta( 'medicale_mascot_' . "page_metabox_slider_select_layer_slider", '', $current_page_id );
					if( ! medicale_mascot_metabox_opt_val_is_empty( $rev_slider_alias ) && $rev_slider_alias != '0' ) {
						$html = do_shortcode( '[layerslider id="'.$rev_slider_alias.'"]' );
					}
					break;
				}
			
			default:
				# code...
				break;
		}

		echo $html;
	}
}









if (!function_exists('medicale_mascot_register_header_navigation_side_push_panel_sidebar')) {
	/**
	 * Register Side Push Panel Sidebar
	 */
	function medicale_mascot_register_header_navigation_side_push_panel_sidebar() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();


		//Show Side Push Panel
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_nav_row_show_side_push_panel", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['show_side_push_panel'] = $temp_meta_value;
		} else {
			$params['show_side_push_panel'] = medicale_mascot_get_redux_option( 'header-settings-navigation-show-side-push-panel', false );
		}

		if( !$params['show_side_push_panel'] ) {
			return;
		}



		// Side Push Panel Sidebar
		register_sidebar( array(
			'name' => esc_html__( 'Side Push Panel Sidebar', 'medicale-wp' ),
			'id' => 'header-side-push-panel-sidebar',
			'description'	=> esc_html__( 'Widgets in this area will be shown on Side Push Panel section. Drag and drop your widgets here.', 'medicale-wp' ),
			'before_widget' => '<div id="%1$s" class="widget widget-side-push-panel %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h5 class="widget-title">',
			'after_title'   => '</h5>',
		) );
	}
	add_action( 'widgets_init', 'medicale_mascot_register_header_navigation_side_push_panel_sidebar', 1000 );
}


if(!function_exists('medicale_mascot_get_header_side_push_panel_sidebar')) {
	/**
	 * Function that Renders Side Push Panel Sidebar HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_header_side_push_panel_sidebar() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();


		//Show Side Push Panel
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_nav_row_show_side_push_panel", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['show_side_push_panel'] = $temp_meta_value;
		} else {
			$params['show_side_push_panel'] = medicale_mascot_get_redux_option( 'header-settings-navigation-show-side-push-panel', false );
		}

		if( !$params['show_side_push_panel'] ) {
			return;
		}

		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'side-push-panel', null, 'header/tpl/content', $params );
		return $html;
	}
	add_action( 'medicale_mascot_header_nav_side_icons', 'medicale_mascot_get_header_side_push_panel_sidebar', 20 );
}


if (!function_exists('medicale_mascot_header_side_push_panel_sidebar_add_class_to_body')) {
	/**
	 * Add classes to body for Side Push Panel Sidebar
	 */
	function medicale_mascot_header_side_push_panel_sidebar_add_class_to_body( $classes ) {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();


		//Show Side Push Panel
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_nav_row_show_side_push_panel", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['show_side_push_panel'] = $temp_meta_value;
		} else {
			$params['show_side_push_panel'] = medicale_mascot_get_redux_option( 'header-settings-navigation-show-side-push-panel', false );
		}

		
		if( $params['show_side_push_panel'] ) {
			$classes[] = 'has-side-panel side-panel-right';
		}

		return $classes;
	}
	add_filter( 'body_class', 'medicale_mascot_header_side_push_panel_sidebar_add_class_to_body' );
}





if (!function_exists('medicale_mascot_register_header_navigation_vertical_nav_sidebar')) {
	/**
	 * Register Vertical Nav Sidebar
	 */
	function medicale_mascot_register_header_navigation_vertical_nav_sidebar() {
		// Side Push Panel Sidebar
		register_sidebar( array(
			'name' => esc_html__( 'Vertical Nav Sidebar', 'medicale-wp' ),
			'id' => 'header-vertical-nav-sidebar',
			'description'	=> esc_html__( 'Widgets in this area will be shown on Vertical Nav section. Drag and drop your widgets here.', 'medicale-wp' ),
			'before_widget' => '<div id="%1$s" class="widget widget-vertical-nav %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h5 class="widget-title">',
			'after_title'   => '</h5>',
		) );
	}
	add_action( 'widgets_init', 'medicale_mascot_register_header_navigation_vertical_nav_sidebar', 1000 );
}



if(!function_exists('medicale_mascot_get_header_search_icon')) {
	/**
	 * Function that Renders Header Search Icon HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_header_search_icon() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();


		//Show Search Icon
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_nav_row_show_menu_search_icon", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['show_menu_search_icon'] = $temp_meta_value;
		} else {
			$params['show_menu_search_icon'] = medicale_mascot_get_redux_option( 'header-settings-navigation-show-menu-search-icon' );
		}

		if( !$params['show_menu_search_icon'] ) {
			return;
		}

		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'nav-search-icon', null, 'header/tpl/content', $params );
		return $html;
	}
	add_action( 'medicale_mascot_header_nav_side_icons', 'medicale_mascot_get_header_search_icon', 10 );
}

if(!function_exists('medicale_mascot_get_header_search_form')) {
	/**
	 * Function that Renders Header Search Form HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_header_search_form() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();


		//Show Search Icon
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_nav_row_show_menu_search_icon", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['show_menu_search_icon'] = $temp_meta_value;
		} else {
			$params['show_menu_search_icon'] = medicale_mascot_get_redux_option( 'header-settings-navigation-show-menu-search-icon' );
		}

		if( !$params['show_menu_search_icon'] ) {
			return;
		}

		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'nav-search-form', null, 'header/tpl/content', $params );
		return $html;
	}
	add_action( 'medicale_mascot_header_nav_container_end', 'medicale_mascot_get_header_search_form' );
}

if(!function_exists('medicale_mascot_get_header_nav_custom_button')) {
	/**
	 * Function that Renders Header Nav Custom Button HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_header_nav_custom_button() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();


		//Show Custom Button
		//check if meta value is provided for this page or then get it from theme options
		$params['header_nav_custom_button_content_type_from'] = '';
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_nav_row_show_custom_button", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['show_custom_button'] = $temp_meta_value;
			$params['header_nav_custom_button_content_type_from'] = 'metabox';
		} else {
			$params['show_custom_button'] = medicale_mascot_get_redux_option( 'header-settings-navigation-show-custom-button' );
			$params['header_nav_custom_button_content_type_from'] = 'redux';
		}

		if( !$params['show_custom_button'] ) {
			return;
		}


		//Custom Button Info
		if( $params['header_nav_custom_button_content_type_from'] == "metabox" ) {
			$params['custom_button'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_nav_custom_button_info", '', $current_page_id );
			$params['custom_button_design_style'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_nav_custom_button_design_style", '', $current_page_id );
			$params['custom_button_theme_colored'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_nav_custom_button_theme_colored", '', $current_page_id );

			//add static key to $params['custom_button']
			$custom_button_array = array();
			$custom_button_array['Button Title'] = $params['custom_button'][0];
			$custom_button_array['Button Link'] = $params['custom_button'][1];
			$params['custom_button'] = $custom_button_array;
		} else {
			$params['custom_button'] = medicale_mascot_get_redux_option( 'header-settings-navigation-custom-button-info' );
			$params['custom_button_design_style'] = medicale_mascot_get_redux_option( 'header-settings-navigation-custom-button-design-style' );
			$params['custom_button_theme_colored'] = medicale_mascot_get_redux_option( 'header-settings-navigation-custom-button-theme-colored' );
		}

		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'nav-custom-button', null, 'header/tpl/content', $params );
		return $html;
	}
	add_action( 'medicale_mascot_header_nav_side_icons', 'medicale_mascot_get_header_nav_custom_button', 25 );
}








if(!function_exists('medicale_mascot_get_header_mini_cart_icon')) {
	/**
	 * Function that Renders Header Mini Cart Icon HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_header_mini_cart_icon() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();

		if ( !class_exists( 'WooCommerce' ) ) {
			return;
		}
		
		//Show Cart Icon
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_nav_row_show_menu_cart_icon", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['show_menu_cart_icon'] = $temp_meta_value;
		} else {
			$params['show_menu_cart_icon'] = medicale_mascot_get_redux_option( 'header-settings-navigation-show-menu-cart-icon' );
		}

		if( !$params['show_menu_cart_icon'] ) {
			return;
		}

		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'nav-mini-cart-icon', null, 'header/tpl/content', $params );
		return $html;
	}
	add_action( 'medicale_mascot_header_nav_side_icons', 'medicale_mascot_get_header_mini_cart_icon', 15 );
}


if(!function_exists('woocommerce_header_add_to_cart_fragment')) {
	/**
	 * Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
	 * @return HTML
	 */
	function woocommerce_header_add_to_cart_fragment( $fragments ) {
		ob_start();
		?>
		<div class="top-nav-mini-cart-icon-contents">
			<a class="mini-cart-icon" href="<?php echo wc_get_cart_url(); ?>" title="<?php esc_html_e( 'View your shopping cart', 'medicale-wp' ); ?>"><i class="icon_cart_alt"></i><span class="items-count"><?php echo sprintf (_n( '%d', '%d', WC()->cart->get_cart_contents_count(), 'medicale-wp' ), WC()->cart->get_cart_contents_count() ); ?></span> <span class="cart-quick-info"><?php echo sprintf (_n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), 'medicale-wp' ), WC()->cart->get_cart_contents_count() ); ?> - <?php echo WC()->cart->get_cart_total(); ?></span></a>

			<div class="dropdown-content">
				<?php woocommerce_mini_cart(); ?>
			</div>
		</div>

		<?php
		$fragments['div.top-nav-mini-cart-icon-contents'] = ob_get_clean();
		return $fragments;
	}
	add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
}


if(!function_exists('header_nav_row_link_n_icon_color_custom_style')) {
	/**
	 * Header Navigation Link and Cart/Search/Side Push Icon Color
	 * @return HTML
	 */
	function header_nav_row_link_n_icon_color_custom_style()
	{
		$current_page_id = medicale_mascot_get_page_id();
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_header_nav_row_link_n_icon_color", '', $current_page_id );

		if( medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) ) {
			return;
		}
		
        $custom_css = "
			header#header .header-nav .header-nav-container .menuzord-menu > li > a,
			header#header .header-nav .header-nav-container .search-icon,
			header#header .header-nav .header-nav-container .mini-cart-icon {
				color: {$temp_meta_value};
			}
			header#header .header-nav .header-nav-container .hamburger-box .hamburger-inner,
			header#header .header-nav .header-nav-container .hamburger-box .hamburger-inner:before,
			header#header .header-nav .header-nav-container .hamburger-box .hamburger-inner:after {
				background-color: {$temp_meta_value};
			}";
        wp_add_inline_style( 'mascot-style-main', $custom_css );
	}
	add_action( 'wp_enqueue_scripts', 'header_nav_row_link_n_icon_color_custom_style' );
}




