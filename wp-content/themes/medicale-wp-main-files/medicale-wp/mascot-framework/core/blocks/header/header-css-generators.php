<?php


/*Header Top Starts*/
if (!function_exists('medicale_mascot_header_top_padding_top_bottom')) {
	/**
	 * Generate CSS codes for Header Top Padding Top & Bottom
	 */
	function medicale_mascot_header_top_padding_top_bottom() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'header-settings-header-top-padding-top-bottom';
		$declaration = array();
		$selector = array(
			'header#header .header-top .container',
			'header#header .header-top .container-fluid'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		//added padding into the container.
		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		if( $medicale_mascot_redux_theme_opt[$var_name]['padding-top'] != "" ) {
			$declaration['padding-top'] = $medicale_mascot_redux_theme_opt[$var_name]['padding-top'];
		}
		if( $medicale_mascot_redux_theme_opt[$var_name]['padding-bottom'] != "" ) {
			$declaration['padding-bottom'] = $medicale_mascot_redux_theme_opt[$var_name]['padding-bottom'];
		}
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_header_top_padding_top_bottom');
}


if (!function_exists('medicale_mascot_header_top_custom_background_color')) {
	/**
	 * Generate CSS codes for Header Top Custom Background Color
	 */
	function medicale_mascot_header_top_custom_background_color() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'header-settings-header-top-custom-bgcolor';
		$declaration = array();
		$selector = array(
			'header#header .header-top',
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		$declaration['background-color'] = $medicale_mascot_redux_theme_opt[$var_name];
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_header_top_custom_background_color');
}



if (!function_exists('medicale_mascot_header_top_widget_text_typography')) {
	/**
	 * Generate CSS codes for Header Top Widget Text Typography
	 */
	function medicale_mascot_header_top_widget_text_typography() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'header-settings-header-top-widget-text-typography';
		$declaration = array();
		$selector = array(
			'header#header .header-top'
		);

		$declaration = medicale_mascot_redux_option_field_typography( $var_name );
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_header_top_widget_text_typography');
}

if (!function_exists('medicale_mascot_header_top_widget_link_typography')) {
	/**
	 * Generate CSS codes for Header Top Widget Link Typography
	 */
	function medicale_mascot_header_top_widget_link_typography() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'header-settings-header-top-widget-link-typography';
		$declaration = array();
		$selector = array(
			'header#header .header-top a'
		);

		$declaration = medicale_mascot_redux_option_field_typography( $var_name );
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_header_top_widget_link_typography');
}

if (!function_exists('medicale_mascot_header_top_widget_link_hover_color')) {
	/**
	 * Generate CSS codes for Header Top Widget Link Hover Color
	 */
	function medicale_mascot_header_top_widget_link_hover_color() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'header-settings-header-top-widget-link-hover-color';
		$declaration = array();
		$selector = array(
			'header#header .header-top a:hover'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}


		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		$declaration['color'] = $medicale_mascot_redux_theme_opt[$var_name];
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_header_top_widget_link_hover_color');
}

if (!function_exists('medicale_mascot_header_logo_maximum_height')) {
	/**
	 * Generate CSS codes for Maximum logo height
	 */
	function medicale_mascot_header_logo_maximum_height() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'logo-settings-maximum-logo-height';
		$declaration = array();
		$selector = array(
			'header#header .header-nav .site-brand img'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}


		$declaration['max-height'] = $medicale_mascot_redux_theme_opt[$var_name].'px';
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_header_logo_maximum_height');
}


if (!function_exists('medicale_mascot_header_nav_row_custom_background_color')) {
	/**
	 * Generate CSS codes for Header Navigation Row Custom Background Color
	 */
	function medicale_mascot_header_nav_row_custom_background_color() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'header-settings-navigation-custom-bgcolor';
		$declaration = array();
		$selector = array(
			'header#header .header-nav .header-nav-container',
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		$header_layout_type = medicale_mascot_get_redux_option( 'header-settings-choose-header-layout-type' );
		if( $header_layout_type == 'header-7-vertical-nav' ) {
			$selector = array(
				'body.vertical-nav #header.header',
			);
		}

		$declaration['background-color'] = $medicale_mascot_redux_theme_opt[$var_name];
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_header_nav_row_custom_background_color');
}



if (!function_exists('medicale_mascot_header_nav_row_custom_nav_link_n_icon_color')) {
	/**
	 * Generate CSS codes for Header Navigation Row Link and Cart/Search/Side Push Icon Color
	 */
	function medicale_mascot_header_nav_row_custom_nav_link_n_icon_color() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'header-settings-navigation-custom-navigation-link-n-icon-color';
		$declaration = array();

		$selector = array(
			'header#header .header-nav .header-nav-container .menuzord-menu > li > a',
			'header#header .header-nav .header-nav-container .search-icon',
			'header#header .header-nav .header-nav-container .mini-cart-icon',
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		$declaration['color'] = $medicale_mascot_redux_theme_opt[$var_name];
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);


		
		$header_layout_type = medicale_mascot_get_redux_option( 'header-settings-choose-header-layout-type' );
		if( $header_layout_type == 'header-7-vertical-nav' ) {
			$selector = array(
				'body.vertical-nav #header.header .vertical-nav-sidebar-widget-wrapper .widget',
				'body.vertical-nav #header.header .vertical-nav-sidebar-widget-wrapper .widget-title',
			);
			$declaration['color'] = $medicale_mascot_redux_theme_opt[$var_name];
			echo medicale_mascot_dynamic_css_generator($selector, $declaration);
		}


		//background color
		$selector = array(
			'header#header .header-nav .header-nav-container .hamburger-box .hamburger-inner',
			'header#header .header-nav .header-nav-container .hamburger-box .hamburger-inner:before',
			'header#header .header-nav .header-nav-container .hamburger-box .hamburger-inner:after',
		);
		$declaration['background-color'] = $medicale_mascot_redux_theme_opt[$var_name];
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);

	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_header_nav_row_custom_nav_link_n_icon_color');
}


if (!function_exists('medicale_mascot_header_navigation_vertical_nav_bgimg')) {
	/**
	 * Generate CSS codes for Background Image for Vertical Nav
	 */
	function medicale_mascot_header_navigation_vertical_nav_bgimg() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'header-settings-navigation-vertical-nav-bgimg';
		$declaration = array();
		$selector = array(
			'body.vertical-nav #header.header'
		);

		$declaration = medicale_mascot_redux_option_field_background( $var_name );
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_header_navigation_vertical_nav_bgimg');
}