
	<?php if( $show_header_nav_row ): ?>
	<?php do_action( 'medicale_mascot_before_header_nav' ); ?>
	<!-- fullpage nav --> 
	<a href="#fullpage-nav" class="fullpage-nav-toggle"> <span>Toggle menu</span> </a>
	<nav id="fullpage-nav" class="panel" role="navigation">
		<div class="pt-30">
		<?php
			/**
			* medicale_mascot_header_logo hook.
			*
			* @hooked medicale_mascot_get_header_logo
			*/
			do_action( 'medicale_mascot_header_logo' );
		?>
		</div>
		<?php
		/**
		* medicale_mascot_header_primary_nav hook.
		*
		* @hooked medicale_mascot_get_header_primary_nav
		*/
		do_action( 'medicale_mascot_header_primary_nav' );
		?>
	</nav>
	<!-- end fullpage nav --> 
	<?php do_action( 'medicale_mascot_after_header_nav' ); ?>
	<?php endif; ?>