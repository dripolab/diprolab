<?php

if (!function_exists('medicale_mascot_layout_settings_boxed_layout_padding_top_bottom')) {
	/**
	 * Generate CSS codes for Boxed Layout - Padding Top & Bottom
	 */
	function medicale_mascot_layout_settings_boxed_layout_padding_top_bottom() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'layout-settings-boxed-layout-padding-top-bottom';
		$declaration = array();
		$selector = array(
			'body.boxed-layout',
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		//if Page Layout boxed
		if( medicale_mascot_get_redux_option( 'layout-settings-page-layout' ) == 'boxed' ) {
			$padding_top = $medicale_mascot_redux_theme_opt[$var_name]['padding-top'];
			$padding_bottom = $medicale_mascot_redux_theme_opt[$var_name]['padding-bottom'];

			if( !empty( $padding_top ) && $padding_top != "" ) {
				$padding_top = medicale_mascot_remove_suffix( $padding_top, 'px');
				$declaration['padding-top'] = $padding_top . 'px';
			}
			if( !empty( $padding_bottom ) && $padding_bottom != "" ) {
				$padding_bottom = medicale_mascot_remove_suffix( $padding_bottom, 'px');
				$declaration['padding-bottom'] = $padding_bottom . 'px';
			}
		}

		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_layout_settings_boxed_layout_padding_top_bottom');
}


if (!function_exists('medicale_mascot_boxed_layout_background_color')) {
	/**
	 * Generate CSS codes for Boxed Layout - Background Color
	 */
	function medicale_mascot_boxed_layout_background_color() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'layout-settings-boxed-layout-bg-type-bgcolor';
		$declaration = array();
		$selector = array(
			'body.boxed-layout',
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( medicale_mascot_get_redux_option( 'layout-settings-boxed-layout-bg-type' ) == 'bg-color' ) {
			if( $medicale_mascot_redux_theme_opt[$var_name] != "" ) {
				$declaration['background-color'] = $medicale_mascot_redux_theme_opt[$var_name];
			}
			echo medicale_mascot_dynamic_css_generator($selector, $declaration);
		}
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_boxed_layout_background_color');
}




if (!function_exists('medicale_mascot_boxed_layout_background_pattern')) {
	/**
	 * Generate CSS codes for Boxed Layout - Background Pattern
	 */
	function medicale_mascot_boxed_layout_background_pattern() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'layout-settings-boxed-layout-bg-type-pattern';
		$declaration = array();
		$selector = array(
			'body.boxed-layout',
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( medicale_mascot_get_redux_option( 'layout-settings-boxed-layout-bg-type' ) == 'bg-patter' ) {
			if( $medicale_mascot_redux_theme_opt[$var_name] != "" ) {
				$declaration['background-image'] = 'url('.$medicale_mascot_redux_theme_opt[$var_name].')';
			}
			echo medicale_mascot_dynamic_css_generator($selector, $declaration);
		}
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_boxed_layout_background_pattern');
}


if (!function_exists('medicale_mascot_boxed_layout_bg')) {
	/**
	 * Generate CSS codes for Widget Footer Background
	 */
	function medicale_mascot_boxed_layout_bg() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'layout-settings-boxed-layout-bg-type-bgimg';
		$declaration = array();
		$selector = array(
			'body.boxed-layout',
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( medicale_mascot_get_redux_option( 'layout-settings-boxed-layout-bg-type' ) == 'bg-image' ) {
			$declaration = medicale_mascot_redux_option_field_background( $var_name );
			echo medicale_mascot_dynamic_css_generator($selector, $declaration);
		}
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_boxed_layout_bg');
}

