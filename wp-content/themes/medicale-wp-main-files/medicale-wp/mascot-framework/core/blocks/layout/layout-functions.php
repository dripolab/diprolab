<?php


if (!function_exists('medicale_mascot_layout_settings_add_class_to_body')) {
	/**
	 * Add classes to body
	 */
	function medicale_mascot_layout_settings_add_class_to_body ( $classes ) {
		$current_page_id = medicale_mascot_get_page_id();



		//if Page Layout boxed
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_page_layout", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			if( $temp_meta_value == 'boxed' ) {
				$classes[] = 'boxed-layout';
			}
		} else if( medicale_mascot_get_redux_option( 'layout-settings-page-layout' ) == 'boxed' ) {
			$classes[] = 'boxed-layout';
		}


		//if Container Shadow
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_boxed_layout_container_shadow", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			if( $temp_meta_value ) {
				$classes[] = 'container-shadow';
			}
		} else if( medicale_mascot_get_redux_option( 'layout-settings-boxed-layout-container-shadow' ) ) {
			$classes[] = 'container-shadow';
		}

		
		//Content Width
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_content_width", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			if( $temp_meta_value != 'container-default' ) {
				$classes[] = $temp_meta_value;
			}
		} else if( medicale_mascot_get_redux_option( 'layout-settings-content-width' ) != 'container-default' ) {
			$classes[] = medicale_mascot_get_redux_option( 'layout-settings-content-width' );
		}

		return $classes;
	}
	add_filter( 'body_class', 'medicale_mascot_layout_settings_add_class_to_body' );
}



if (!function_exists('medicale_mascot_layout_settings_add_inline_css_to_body')) {
	/**
	 * Add inline css to body
	 */
	function medicale_mascot_layout_settings_add_inline_css_to_body() {
		$current_page_id = medicale_mascot_get_page_id();

		//Padding Top
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_boxed_layout_padding_top", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) ) {
			$padding_top = medicale_mascot_remove_suffix( $temp_meta_value, 'px');
			if( $padding_top >= 0 ) {
				$custom_css = "
						body.boxed-layout {
								padding-top: {$padding_top}px;
						}";
				wp_add_inline_style( 'mascot-dynamic-style', $custom_css );
			}
		}

		//Padding Bottom
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_boxed_layout_padding_bottom", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) ) {
			$padding_bottom = medicale_mascot_remove_suffix( $temp_meta_value, 'px');
			if( $padding_bottom >= 0 ) {
				$custom_css = "
						body.boxed-layout {
								padding-bottom: {$padding_bottom}px;
						}";
				wp_add_inline_style( 'mascot-dynamic-style', $custom_css );
			}
		}


		//Boxed Layout Background Type
		$params['title_area_bgcolor'] = '';
		$params['title_area_bgimg'] = '';
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_boxed_layout_bg_type", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['boxed_layout_bg_type'] = $temp_meta_value;

			if( $params['boxed_layout_bg_type'] == 'bg-color' ) {

				//Background Color
				$boxed_layout_bg_color = rwmb_meta( 'medicale_mascot_' . "page_metabox_boxed_layout_bg_type_color", '', $current_page_id );
				if( ! medicale_mascot_metabox_opt_val_is_empty( $boxed_layout_bg_color ) ) {
					$custom_css = "
							body.boxed-layout {
									background: {$boxed_layout_bg_color};
							}";
					wp_add_inline_style( 'mascot-dynamic-style', $custom_css );
				}

			} else if ( $params['boxed_layout_bg_type'] == 'bg-pattern' ) {

				//Background Pattern
				$boxed_layout_bg_pattern = rwmb_meta( 'medicale_mascot_' . "page_metabox_boxed_layout_bg_type_pattern", '', $current_page_id );
				if( ! medicale_mascot_metabox_opt_val_is_empty( $boxed_layout_bg_pattern ) ) {
					$custom_css = "
							body.boxed-layout {
									background-color: unset;
									background-image: url($boxed_layout_bg_pattern);
							}";
					wp_add_inline_style( 'mascot-dynamic-style', $custom_css );
				}

			} else if ( $params['boxed_layout_bg_type'] == 'bg-image' ) {

				//Background Image
				$boxed_layout_bg_image = rwmb_meta( 'medicale_mascot_' . "page_metabox_boxed_layout_bg_type_img", '', $current_page_id );
				if( ! medicale_mascot_metabox_opt_val_is_empty( $boxed_layout_bg_image ) ) {
					$boxed_layout_bg_image = medicale_mascot_metabox_get_image_advanced_field_url( $boxed_layout_bg_image );
					$custom_css = "
							body.boxed-layout {
									background-color: unset;
									background-image: url($boxed_layout_bg_image);
							}";
					wp_add_inline_style( 'mascot-dynamic-style', $custom_css );
				}

			}
		}

	}
	add_action( 'wp_enqueue_scripts', 'medicale_mascot_layout_settings_add_inline_css_to_body' );
}