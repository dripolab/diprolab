<!-- Blog Masonry -->
<div id="grid" class="gallery-isotope grid-3 masonry gutter-30 clearfix">
	<div class="gallery-item gallery-item-sizer"></div>
	<?php
	if ( have_posts() ) :
		// Start the Loop.
		while ( have_posts() ) : the_post();
		?>
		<!-- Blog Item Start -->
		<div class="gallery-item">
		<?php
			medicale_mascot_get_blog_post_format( get_post_format() );
		?>
		</div>
		<?php
		endwhile;
	else :
		// If no content, include the "No posts found" template.
		echo "No posts found!";
	endif;
	?>
</div>