<?php
	$link_url = rwmb_meta( 'medicale_mascot_' . 'settings_link_url' );
	$link_target = rwmb_meta( 'medicale_mascot_' . 'settings_link_target' );
?>
<a target="<?php echo $link_target;?>" class="post-link text-center text-white bg-theme-colored display-block" href="<?php echo $link_url;?>">
	<?php echo get_the_title(); ?><br><span><?php echo $link_url;?></span>
</a>