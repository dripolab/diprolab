	<?php
	if ( has_post_thumbnail() ) {
		$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
		$full_image_url = $full_image_url[0];
	?>

	<div class="box-hover-effect">
		<div class="effect-wrapper">
			<div class="thumb">
				<?php the_post_thumbnail( 'medicale_mascot_featured_image' ); ?>
			</div>
			<div class="overlay-shade"></div>
			<div class="icons-holder icons-holder-middle">
				<div class="icons-holder-inner">
					<div class="styled-icons icon-sm icon-dark">
						<a href="<?php echo $full_image_url ?>" data-rel="prettyPhoto[blog-archive-featured-image-prettyphoto-<?php the_ID(); ?>]" title="<?php echo get_the_title(); ?>"><i class="fa fa-picture-o"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	}
	?>
