<?php

add_filter( 'rwmb_meta_boxes', 'medicale_mascot_blog_metaboxes' );

function medicale_mascot_blog_metaboxes( $meta_boxes ) {
	$meta_boxes[] = array(
		'id'		=> 'metabox_post_format_settings_audio',
		'title'		=> esc_html__( 'Audio Settings', 'medicale-wp' ),
		'post_types' => array( 'post' ),
		'context'	=> 'normal',
		'priority'   => 'high',
		'show'   => array(
			// List of post formats. Array. Case insensitive. Optional.
			'post_format' => array( 'Audio' ),
		),
		'fields'	 => array(
			array(
				'id'   => 'medicale_mascot_' . 'settings_audio_soundcloud_link',
				'name' => esc_html__( 'SoundCloud URL', 'medicale-wp' ),
				'desc' => esc_html__( 'Example: https://soundcloud.com/wmnashville/hero-skillet', 'medicale-wp' ),
				'type' => 'textarea',
			),
			array(
				'id'   => 'medicale_mascot_' . 'settings_audio_mp3_link',
				'name' => esc_html__( 'MP3 Audio File URL', 'medicale-wp' ),
				'type' => 'file_input',
			),
			array(
				'id'   => 'medicale_mascot_' . 'settings_audio_ogg_link',
				'name' => esc_html__( 'OGA/OGG Audio File URL', 'medicale-wp' ),
				'type' => 'file_input',
			),
		)
	);

	$meta_boxes[] = array(
		'id'		=> 'metabox_post_format_settings_gallery',
		'title'		=> esc_html__( 'Gallery Settings', 'medicale-wp' ),
		'post_types' => array( 'post' ),
		'context'	=> 'normal',
		'priority'   => 'high',
		'show'   => array(
			// List of post formats. Array. Case insensitive. Optional.
			'post_format' => array( 'Gallery' ),
		),
		'fields' => array(
			array(
				'id'				=> 'medicale_mascot_' . 'settings_gallery_images',
				'name'			=> esc_html__( 'Gallery Images', 'medicale-wp' ),
				'type'			=> 'image_advanced',
				'desc'			=> esc_html__( 'Uploaded images for the gallery will be grouped into a slider', 'medicale-wp' ),
				// Delete image from Media Library when remove it from post meta?
				// Note: it might affect other posts if you use same image for multiple posts
				'force_delete'	 => false,
				// Maximum image uploads
				'max_file_uploads' => 50,
			),
		)
	);

	$meta_boxes[] = array(
		'id'		=> 'metabox_post_format_settings_link',
		'title'		=> esc_html__( 'Link Settings', 'medicale-wp' ),
		'post_types' => array( 'post' ),
		'context'	=> 'normal',
		'priority'   => 'high',
		'show'   => array(
			// List of post formats. Array. Case insensitive. Optional.
			'post_format' => array( 'Link' ),
		),
		'fields' => array(
			array(
				'id'   => 'medicale_mascot_' . 'settings_link_url',
				'name' => esc_html__( 'Link URL', 'medicale-wp' ),
				'type' => 'text',
			),
			array(
				'name' => esc_html__( 'Link Target', 'medicale-wp' ),
				'id'   => 'medicale_mascot_' . 'settings_link_target',
				'type'	=> 'select',
				'options' => array(
					'_blank' => esc_html__( 'Open New Window', 'medicale-wp' ),
					'_self' => esc_html__( 'Open Same Window', 'medicale-wp' ),
				),
				'std'		=> '_blank',
			),
		)
	);

	$meta_boxes[] = array(
		'id'		=> 'metabox_post_format_settings_quote',
		'title'		=> esc_html__( 'Quote Settings', 'medicale-wp' ),
		'post_types' => array( 'post' ),
		'context'	=> 'normal',
		'priority'   => 'high',
		'show'   => array(
			// List of post formats. Array. Case insensitive. Optional.
			'post_format' => array( 'Quote' ),
		),
		'fields' => array(			
			array(
				'id'				=> 'medicale_mascot_' . 'settings_quote_quote',
				'name'			=> esc_html__( 'Quote', 'medicale-wp' ),
				'desc'		=> esc_html__( 'Place your text for the quote', 'medicale-wp' ),
				'type'		=> 'textarea',
				// Default value (optional)
				//'std'		=> esc_html__( '', 'medicale-wp' ),
				// Placeholder
				//'placeholder' => esc_html__( '', 'medicale-wp' ),
				// Number of rows
				'rows'		=> 5,
				// Number of columns
				'cols'		=> 5,
			),
			array(
				'id'   => 'medicale_mascot_' . 'settings_quote_author',
				'name' => esc_html__( 'Quote Author', 'medicale-wp' ),
				'desc' => esc_html__( 'The person who place the quote', 'medicale-wp' ),
				'type' => 'text',
			),
		)
	);

	$meta_boxes[] = array(
		'id'		=> 'metabox_post_format_settings_video',
		'title'		=> esc_html__( 'Video Settings', 'medicale-wp' ),
		'post_types' => array( 'post' ),
		'context'	=> 'normal',
		'priority'   => 'high',
		'show'   => array(
			// List of post formats. Array. Case insensitive. Optional.
			'post_format' => array( 'Video' ),
		),
		'fields'	 => array(
			array(
				'name' => esc_html__( 'Video Type', 'medicale-wp' ),
				'id'   => 'medicale_mascot_' . 'settings_video_type',
				'type'	=> 'radio',
				'options' => array(
					'youtube' => esc_html__( 'Youtube', 'medicale-wp' ),
					'vimeo' => esc_html__( 'Vimeo', 'medicale-wp' ),
					'self_hosted' => esc_html__( 'Self Hosted Video', 'medicale-wp' ),
				),
			),
			array(
				'id'   => 'medicale_mascot_' . 'settings_video_youtube_url',
				'name' => esc_html__( 'Youtube Video URL', 'medicale-wp' ),
				'type' => 'textarea',
				'desc' => esc_html__( 'Example: https://www.youtube.com/watch?v=DEME-d6foS8', 'medicale-wp' ),
				'visible' => array( 'medicale_mascot_' . 'settings_video_type', 'youtube' )
			),
			array(
				'id'   => 'medicale_mascot_' . 'settings_video_vimeo_url',
				'name' => esc_html__( 'Vimeo Video URL', 'medicale-wp' ),
				'type' => 'textarea',
				'desc' => esc_html__( 'Example: https://vimeo.com/217247717', 'medicale-wp' ),
				'visible' => array( 'medicale_mascot_' . 'settings_video_type', 'vimeo' )
			),
			array(
				'id'				=> 'medicale_mascot_' . 'settings_video_self_hosted_video_image',
				'name'			=> esc_html__( 'Video Image', 'medicale-wp' ),
				'type'			=> 'image_advanced',
				// Delete image from Media Library when remove it from post meta?
				// Note: it might affect other posts if you use same image for multiple posts
				'force_delete'	 => false,
				// Maximum image uploads
				'max_file_uploads' => 1,
				'visible' => array( 'medicale_mascot_' . 'settings_video_type', 'self_hosted' )
			),
			array(
				'id'   => 'medicale_mascot_' . 'settings_video_self_hosted_mp4_url',
				'name' => esc_html__( 'MP4 Video URL', 'medicale-wp' ),
				'type' => 'file_input',
				'visible' => array( 'medicale_mascot_' . 'settings_video_type', 'self_hosted' )
			),
			array(
				'id'   => 'medicale_mascot_' . 'settings_video_self_hosted_webm_url',
				'name' => esc_html__( 'WEBM Video URL', 'medicale-wp' ),
				'type' => 'file_input',
				'visible' => array( 'medicale_mascot_' . 'settings_video_type', 'self_hosted' )
			),
			array(
				'id'   => 'medicale_mascot_' . 'settings_video_self_hosted_ogv_url',
				'name' => esc_html__( 'OGV Video URL', 'medicale-wp' ),
				'type' => 'file_input',
				'visible' => array( 'medicale_mascot_' . 'settings_video_type', 'self_hosted' )
			),
		)
	);
	return $meta_boxes;
}