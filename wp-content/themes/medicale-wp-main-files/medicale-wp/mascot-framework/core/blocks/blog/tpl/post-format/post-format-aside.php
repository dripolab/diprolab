<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if( $show_post_featured_image ) { ?>
	<div class="entry-header">
	<?php do_action( 'medicale_mascot_blog_post_entry_header_start' ); ?>
	<?php medicale_mascot_get_post_thumbnail( $post_format ); ?>
	<?php do_action( 'medicale_mascot_blog_post_entry_header_end' ); ?>
	</div>
	<?php } ?>
	<div class="entry-content">
	<?php do_action( 'medicale_mascot_blog_post_entry_content_start' ); ?>
	<?php medicale_mascot_post_meta(); ?>
	<?php medicale_mascot_get_post_title(); ?>
	<div class="post-excerpt">
		<?php medicale_mascot_get_excerpt(); ?>
		<?php medicale_mascot_get_post_wp_link_pages(); ?>
	</div>
	<div class="post-btn-readmore">
		<a href="<?php the_permalink(); ?>" class="btn btn-dark btn-theme-colored"><?php esc_html_e( 'Read more', 'medicale-wp' ); ?></a>
	</div>
	<?php do_action( 'medicale_mascot_blog_post_entry_content_end' ); ?>
	<div class="clearfix"></div>
	</div>
</article>