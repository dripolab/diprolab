<?php
	/**
	* medicale_mascot_before_blog_section hook.
	*
	*/
	do_action( 'medicale_mascot_before_blog_section' );
?>
<section>
	<div class="<?php echo $container_type; ?>">
		<?php
			/**
			* medicale_mascot_blog_container_start hook.
			*
			*/
			do_action( 'medicale_mascot_blog_container_start' );
		?>

		<div class="blog-posts">
			<?php
				medicale_mascot_get_blog_sidebar_layout();
			?>
		</div>

	<?php
		/**
		* medicale_mascot_blog_container_end hook.
		*
		*/
		do_action( 'medicale_mascot_blog_container_end' );
	?>
	</div>
</section>
<?php
	/**
	* medicale_mascot_after_blog_section hook.
	*
	*/
	do_action( 'medicale_mascot_after_blog_section' );
?>
