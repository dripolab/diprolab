<div class="row">
	<div class="col-md-8 main-content-area">

	<?php do_action( 'medicale_mascot_blog_main_content_area_start' ); ?>

	<?php
		medicale_mascot_get_blog_post_layout();
	?>
	<?php
		medicale_mascot_get_pagination();
	?>

	<?php do_action( 'medicale_mascot_blog_main_content_area_end' ); ?>

	</div>
	<div class="col-md-3 sidebar-area sidebar-right">
	<?php get_sidebar( 'right' ); ?>
	</div>
</div>
