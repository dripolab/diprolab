<?php


if(!function_exists('medicale_mascot_get_blog')) {
	/**
	 * Function that Renders Blog HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_blog( $container_type = 'container' ) {
		$params = array();

		$params['container_type'] = $container_type;

		if( medicale_mascot_get_redux_option( 'blog-settings-fullwidth' ) ) {
			$params['container_type'] = 'container-fluid';
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'blog-parts', null, 'blog/tpl', $params );
		
		return $html;
	}
}

if (!function_exists('medicale_mascot_get_blog_sidebar_layout')) {
	/**
	 * Return Blog Sidebar Layout HTML
	 */
	function medicale_mascot_get_blog_sidebar_layout() {
		$params = array();

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'blog', medicale_mascot_get_redux_option( 'blog-settings-sidebar-layout', 'sidebar-right-33' ), 'blog/tpl/sidebar-columns', $params );
		
		return $html;
	}
}

if (!function_exists('medicale_mascot_get_blog_post_layout')) {
	/**
	 * Return Blog Post Layout HTML
	 */
	function medicale_mascot_get_blog_post_layout() {
		$params = array();

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'post', medicale_mascot_get_redux_option( 'blog-settings-archive-page-layout', 'standard-1-col' ), 'blog/tpl/post-layout', $params );
		
		return $html;
	}
}

if (!function_exists('medicale_mascot_get_blog_post_format')) {
	/**
	 * Return Blog Post Format HTML
	 */
	function medicale_mascot_get_blog_post_format( $post_format = '' ) {
		global $supported_post_formats;
		$params = array();

		if( !in_array( $post_format, $supported_post_formats ) ) {
			$post_format = 'standard';
		}
		$params['post_format'] = $post_format;

		$params['show_post_featured_image'] = medicale_mascot_get_redux_option( 'blog-settings-show-post-featured-image', true );

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'post-format', $post_format, 'blog/tpl/post-format', $params );
		
		return $html;
	}
}

if(!function_exists('medicale_mascot_get_post_thumbnail')) {
	/**
	 * Function that Renders Header HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_post_thumbnail( $post_format = '' ) {
		global $supported_post_formats;
		$params = array();

		//format
		if( !in_array( $post_format, $supported_post_formats ) ) {
			$post_format = 'standard';
		}
		$params['post_format'] = $post_format;

		$params['featured_image_height'] = medicale_mascot_get_redux_option( 'blog-single-post-settings-featured-image-height' );

		if ( ( $params['post_format'] == "standard" || $params['post_format'] == "image" ) && !has_post_thumbnail() ) {
			return;
		}
			
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'post-thumb', $post_format, 'blog/tpl/parts', $params );
		
		return $html;
	}
}

if(!function_exists('medicale_mascot_get_post_title')) {
	/**
	 * Function that Renders Post Title HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_post_title() {
		$params = array();

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'post-title', null, 'blog/tpl/parts', $params );
		
		return $html;
	}
}

if(!function_exists('medicale_mascot_get_post_wp_link_pages')) {
	/**
	 * Function that Displays page-links for paginated posts 
	 * @return HTML
	 */
	function medicale_mascot_get_post_wp_link_pages() {
		$params = array();

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'post-link-pages', null, 'blog/tpl/parts', $params );
		
		return $html;
	}
}

if(!function_exists('medicale_mascot_get_pagination')) {
	/**
	 * Function that Renders Pagination HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_pagination() {

		//Show Pagination
		$params['show_pagination'] = medicale_mascot_get_redux_option( 'blog-settings-show-pagination', true );

		if( !$params['show_pagination'] ) {
			return;
		}

		echo medicale_mascot_render_pagination_html();
	}
}

if ( ! function_exists( 'medicale_mascot_post_meta' ) ) {
	/**
	 * Print HTML with meta information for the current post-date/time and author.
	 *
	 */
	function medicale_mascot_post_meta() {
	?>
	<ul class="entry-meta list-inline">
	<?php
		if( medicale_mascot_get_redux_option( 'blog-settings-post-meta', false, 'show-post-by-author' ) ) {
	?>
			<li><i class="fa fa-user-o"></i> <?php medicale_mascot_posted_by();?></li>
	<?php
		} if( medicale_mascot_get_redux_option( 'blog-settings-post-meta', true, 'show-post-date' ) ) {
	?>
			<li><i class="fa fa-calendar-o"></i> <?php medicale_mascot_posted_on();?></li>
	<?php
		} if( has_category() && medicale_mascot_get_redux_option( 'blog-settings-post-meta', true, 'show-post-category' ) ) {
	?>
			<li><i class="fa fa-folder-o"></i> <?php medicale_mascot_post_category();?></li>
	<?php
		} if( comments_open() && medicale_mascot_get_redux_option( 'blog-settings-post-meta', false, 'show-post-comments-count' ) ) {
	?>
			<li><i class="fa fa-commenting-o"></i> <?php medicale_mascot_get_comments_number(); ?></li>
	<?php
		} if( has_tag() && medicale_mascot_get_redux_option( 'blog-settings-post-meta', false, 'show-post-tag' ) ) {
	?>
			<li><i class="fa fa-tags"></i> <?php medicale_mascot_post_tag();?></li>
	<?php
		} if( medicale_mascot_get_redux_option( 'blog-settings-post-meta', false, 'show-post-like-button' ) ) {
	?>
			<li><?php echo medicale_mascot_sl_get_simple_likes_button( get_the_ID() ); ?></li>
	<?php
		}
	?>
	</ul>
	<?php
	}
}

if ( ! function_exists( 'medicale_mascot_posted_on' ) ) {
	/**
	 * Print HTML posted on
	 *
	 */
	function medicale_mascot_posted_on() {
		printf( '<a href="%1$s" rel="bookmark"><time class="entry-date" datetime="%2$s">%3$s</time></a>',
			esc_url( get_permalink() ),
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() )
		);
	}
}

if ( ! function_exists( 'medicale_mascot_posted_on_date' ) ) {
	/**
	 * Print HTML posted on
	 *
	 */
	function medicale_mascot_posted_on_date() {
		printf( '<span class="entry-date"><time class="entry-date" datetime="%1$s">%2$s</time></span>',
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() )
		);
	}
}

if ( ! function_exists( 'medicale_mascot_posted_by' ) ) {
	/**
	 * Print HTML posted by.
	 *
	 */
	function medicale_mascot_posted_by() {
		printf( '<span class="byline">By <span class="author vcard"><a class="url fn n" href="%1$s" rel="author">%2$s</a></span></span>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			get_the_author()
		);
	}
}

if ( ! function_exists( 'medicale_mascot_post_category' ) ) {
	/**
	 * Print HTML post category.
	 *
	 */
	function medicale_mascot_post_category() {
		// Translators: used between list items, there is a space after the comma.
		$categories_list = get_the_category_list( esc_html__( ', ', 'medicale-wp' ) );
		if ( $categories_list ) {
			echo '<span class="categories-links">' . $categories_list . '</span>';
		}
	}
}

if ( ! function_exists( 'medicale_mascot_post_tag' ) ) {
	/**
	 * Print HTML post tag.
	 *
	 */
	function medicale_mascot_post_tag() {
		// Translators: used between list items, there is a space after the comma.
		$tag_list = get_the_tag_list( '', esc_html__( ', ', 'medicale-wp' ) );
		if ( $tag_list ) {
			echo '<span class="tags-links">' . $tag_list . '</span>';
		}
	}
}

if ( ! function_exists( 'medicale_mascot_get_comments_number' ) ) {
	/**
	 * Print HTML get comments number.
	 *
	 */
	function medicale_mascot_get_comments_number() {
		$num_comments = get_comments_number(); // get_comments_number returns only a numeric value
		if ( comments_open() ) {
			if ( $num_comments == 0 ) {
				$comments = esc_html__('No Comments', 'medicale-wp');
			} elseif ( $num_comments > 1 ) {
				$comments = $num_comments . esc_html__(' Comments', 'medicale-wp');
			} else {
				$comments = esc_html__('1 Comment', 'medicale-wp');
			}
			$write_comments = '<a href="' . get_comments_link() .'">'. $comments.'</a>';
		} else {
			$write_comments =  esc_html__('Comments are off for this post.', 'medicale-wp');
		}
		echo $write_comments;
	}
}

if ( ! function_exists( 'medicale_mascot_get_excerpt' ) ) {
	/**
	 * Print HTML get excerpt.
	 *
	 */
	function medicale_mascot_get_excerpt() {
		if ( has_excerpt() ) {
			echo get_the_excerpt();
		} else {
			$blog_excerpt_length = medicale_mascot_get_redux_option( 'blog-settings-excerpt-length', 22 );
			echo '<p>'.wp_trim_words( strip_shortcodes( get_the_content() ), $blog_excerpt_length, '...' ).'</p>';
		}
	}
}

if ( ! function_exists( 'medicale_mascot_related_posts_get_excerpt' ) ) {
	/**
	 * Print HTML Related Posts get excerpt.
	 *
	 */
	function medicale_mascot_related_posts_get_excerpt() {
		if ( has_excerpt() ) {
			echo get_the_excerpt();
		} else {
			$related_posts_excerpt_length = medicale_mascot_get_redux_option( 'blog-single-post-settings-show-related-posts-excerpt-length' );
			echo '<p>'.wp_trim_words( strip_shortcodes( get_the_content() ), $related_posts_excerpt_length, '...' ).'</p>';
		}
	}
}

if ( ! function_exists( 'medicale_mascot_custom_excerpt_length' ) ) {
	/**
	 * Filter the except length to 20 words
	 *
	 */
	function medicale_mascot_custom_excerpt_length( $length ) {
		return 20;
	}
	add_filter( 'excerpt_length', 'medicale_mascot_custom_excerpt_length', 999 );
}

if ( ! function_exists( 'medicale_mascot_wpdocs_excerpt_more' ) ) {
	/**
	 * Filter the excerpt "read more" string.
	 */
	function medicale_mascot_wpdocs_excerpt_more( $more ) {
		return '...';
	}
	add_filter( 'excerpt_more', 'medicale_mascot_wpdocs_excerpt_more' );
}

if (!function_exists('medicale_mascot_register_blog_sidebar')) {
	/**
	 * Register Blog Sidebar
	 */
	function medicale_mascot_register_blog_sidebar() {
		$title_line_bottom_class = '';

		if( medicale_mascot_get_redux_option( 'sidebar-settings-sidebar-title-show-line-bottom' ) ) {
			$title_line_bottom_class = 'widget-title-line-bottom';
		}


		// Blog Default Sidebar
		register_sidebar( array(
			'name'			=> esc_html__( 'Blog Sidebar', 'medicale-wp' ),
			'id'			=> 'default-sidebar',
			'description'   => esc_html__( 'This is a default sidebar for blog. Widgets in this area will be shown on sidebar of blog/single page. Drag and drop your widgets here.', 'medicale-wp' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="widget-title ' . $title_line_bottom_class . '">',
			'after_title'   => '</h4>',
		) );

		// Blog Secondary Sidebar
		register_sidebar( array(
			'name'			=> esc_html__( 'Blog Secondary Sidebar', 'medicale-wp' ),
			'id'			=> 'blog-secondary-sidebar',
			'description'   => esc_html__( 'This is a Secondary sidebar for blog. Widgets in this area will be shown on another sidebar of blog/single page. Drag and drop your widgets here.', 'medicale-wp' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="widget-title ' . $title_line_bottom_class . '">',
			'after_title'   => '</h4>',
		) );
	}
	add_action( 'widgets_init', 'medicale_mascot_register_blog_sidebar', 999 );
}