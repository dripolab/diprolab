<div class="row">
	<div class="col-md-12 main-content-area">

	<?php do_action( 'medicale_mascot_blog_main_content_area_start' ); ?>

	<?php
		medicale_mascot_get_blog_post_layout();
	?>
	<?php
		medicale_mascot_get_pagination();
	?>

	<?php do_action( 'medicale_mascot_blog_main_content_area_end' ); ?>

	</div>
</div>