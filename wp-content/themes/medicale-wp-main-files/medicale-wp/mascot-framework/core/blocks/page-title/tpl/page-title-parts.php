<?php
	/**
	* medicale_mascot_before_page_title hook.
	*
	*/
	do_action( 'medicale_mascot_before_page_title' );
?>
<!-- Section: page-title -->
<section class="page-title <?php echo $title_area_classes;?>"<?php if( $title_area_add_bg_video_status && $title_area_bg_video_type == 'self-hosted' ) { ?> data-vide-bg="
<?php if( $title_area_bg_video_self_hosted_video_mp4_url['url'] != '' ) { echo 'mp4: ' . $title_area_bg_video_self_hosted_video_mp4_url['url']; }?>
<?php if( $title_area_bg_video_self_hosted_video_webm_url['url'] != '' ) { echo ', webm: ' . $title_area_bg_video_self_hosted_video_webm_url['url']; }?>
<?php if( $title_area_bg_video_self_hosted_video_ogv_url['url'] != '' ) { echo ', ogv: ' . $title_area_bg_video_self_hosted_video_ogv_url['url']; }?>
<?php if( $title_area_bg_video_self_hosted_video_poster['url'] != '' ) { echo ', poster: ' . $title_area_bg_video_self_hosted_video_poster['url']; }?>" data-vide-options="loop: true, muted: false, position: 0% 0%"<?php } ?> style="<?php echo $title_area_bgcolor; ?> <?php echo $title_area_bgimg; ?>" >
	<?php
		if( $title_area_add_bg_video_status && $title_area_bg_video_type == 'youtube' ) :
			medicale_mascot_get_title_area_bg_video_youtube();
		endif;
	?>
	<div class="<?php echo $title_area_container_class; ?> <?php echo $title_area_container_height; ?>">
		<?php
			/**
			* medicale_mascot_page_title_start hook.
			*
			*/
			do_action( 'medicale_mascot_page_title_start' );
		?>
		<?php
			medicale_mascot_get_title_area_layout();
		?>
		<?php
			/**
			* medicale_mascot_page_title_end hook.
			*
			*/
			do_action( 'medicale_mascot_page_title_end' );
		?>
	</div>
</section>
<?php
	/**
	* medicale_mascot_after_page_title hook.
	*
	*/
	do_action( 'medicale_mascot_after_page_title' );
?>