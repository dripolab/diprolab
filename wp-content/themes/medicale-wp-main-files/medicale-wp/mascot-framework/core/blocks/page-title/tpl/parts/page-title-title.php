<?php
if(is_home()) {
	$show_blog_title_description = medicale_mascot_get_redux_option( 'blog-other-settings-show-blog-title-description', true );
	if( $show_blog_title_description ) {
		$page_title = medicale_mascot_get_redux_option( 'blog-other-settings-blog-title-text', esc_html__( 'Blog', 'medicale-wp' ) );
		$page_title_desc = medicale_mascot_get_redux_option( 'blog-other-settings-blog-description-text' );
	}
}
?>

<?php if( $animation_effect != '' ): ?>
	<<?php echo $title_tag; ?> class="title wow <?php echo $animation_effect; ?>" style="<?php echo $title_color; ?>" data-wow-delay="<?php echo $animation_duration; ?>"><?php echo $page_title;; ?></<?php echo $title_tag; ?>>
<?php else: ?>
	<<?php echo $title_tag; ?> class="title" style="<?php echo $title_color; ?>"><?php echo $page_title; ?></<?php echo $title_tag; ?>>
<?php endif; ?>

<?php
if( is_home() ) {
	if( $show_blog_title_description && !empty($page_title_desc) ) {
	echo '<p>' . $page_title_desc . '</p>';
	}
}
?>