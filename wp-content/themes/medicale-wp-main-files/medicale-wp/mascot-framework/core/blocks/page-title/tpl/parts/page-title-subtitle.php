<?php if( $subtitle_text != '' ): ?>
	<?php if( $animation_effect != '' ): ?>
		<<?php echo $subtitle_tag; ?> class="subtitle wow <?php echo $animation_effect; ?>" style="<?php echo $subtitle_color; ?>" data-wow-delay="<?php echo $animation_duration; ?>"><?php echo $subtitle_text; ?></<?php echo $subtitle_tag; ?>>
	<?php else: ?>
		<<?php echo $subtitle_tag; ?> class="subtitle" style="<?php echo $subtitle_color; ?>"><?php echo $subtitle_text; ?></<?php echo $subtitle_tag; ?>>
	<?php endif; ?>
<?php endif; ?>