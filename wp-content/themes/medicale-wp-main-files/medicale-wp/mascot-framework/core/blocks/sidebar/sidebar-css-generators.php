<?php

if (!function_exists('medicale_mascot_sidebar_padding')) {
	/**
	 * Generate CSS codes for Sidebar Padding
	 */
	function medicale_mascot_sidebar_padding() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'sidebar-settings-sidebar-padding';
		$declaration = array();
		$selector = array(
			'.sidebar-area'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		//added padding into the container.
		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name]['padding-top'] != "" ) {
			$declaration['padding-top'] = $medicale_mascot_redux_theme_opt[$var_name]['padding-top'];
		}
		if( $medicale_mascot_redux_theme_opt[$var_name]['padding-right'] != "" ) {
			$declaration['padding-right'] = $medicale_mascot_redux_theme_opt[$var_name]['padding-right'];
		}
		if( $medicale_mascot_redux_theme_opt[$var_name]['padding-bottom'] != "" ) {
			$declaration['padding-bottom'] = $medicale_mascot_redux_theme_opt[$var_name]['padding-bottom'];
		}
		if( $medicale_mascot_redux_theme_opt[$var_name]['padding-left'] != "" ) {
			$declaration['padding-left'] = $medicale_mascot_redux_theme_opt[$var_name]['padding-left'];
		}
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_sidebar_padding');
}


if (!function_exists('medicale_mascot_sidebar_bg_color')) {
	/**
	 * Generate CSS codes for Sidebar Background Color
	 */
	function medicale_mascot_sidebar_bg_color() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'sidebar-settings-sidebar-bg-color';
		$declaration = array();
		$selector = array(
			'.sidebar-area'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		$declaration['background-color'] = $medicale_mascot_redux_theme_opt[$var_name];
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_sidebar_bg_color');
}


if (!function_exists('medicale_mascot_sidebar_text_align')) {
	/**
	 * Generate CSS codes for Sidebar Text Alignment
	 */
	function medicale_mascot_sidebar_text_align() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'sidebar-settings-sidebar-text-align';
		$declaration = array();
		$selector = array(
			'.sidebar-area'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		$declaration['text-align'] = $medicale_mascot_redux_theme_opt[$var_name];
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_sidebar_text_align');
}





if (!function_exists('medicale_mascot_sidebar_title_padding')) {
	/**
	 * Generate CSS codes for Sidebar Widget Title Padding
	 */
	function medicale_mascot_sidebar_title_padding() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'sidebar-settings-sidebar-title-padding';
		$declaration = array();
		$selector = array(
			'.sidebar-area .widget .widget-title'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		//added padding into the container.
		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name]['padding-top'] != "" ) {
			$declaration['padding-top'] = $medicale_mascot_redux_theme_opt[$var_name]['padding-top'];
		}
		if( $medicale_mascot_redux_theme_opt[$var_name]['padding-right'] != "" ) {
			$declaration['padding-right'] = $medicale_mascot_redux_theme_opt[$var_name]['padding-right'];
		}
		if( $medicale_mascot_redux_theme_opt[$var_name]['padding-bottom'] != "" ) {
			$declaration['padding-bottom'] = $medicale_mascot_redux_theme_opt[$var_name]['padding-bottom'];
		}
		if( $medicale_mascot_redux_theme_opt[$var_name]['padding-left'] != "" ) {
			$declaration['padding-left'] = $medicale_mascot_redux_theme_opt[$var_name]['padding-left'];
		}
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_sidebar_title_padding');
}


if (!function_exists('medicale_mascot_sidebar_title_bg_color')) {
	/**
	 * Generate CSS codes for Sidebar Widget Title Background Color
	 */
	function medicale_mascot_sidebar_title_bg_color() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'sidebar-settings-sidebar-title-bg-color';
		$declaration = array();
		$selector = array(
			'.sidebar-area .widget .widget-title'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		$declaration['background-color'] = $medicale_mascot_redux_theme_opt[$var_name];
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_sidebar_title_bg_color');
}


if (!function_exists('medicale_mascot_sidebar_title_text_color')) {
	/**
	 * Generate CSS codes for Sidebar Widget Title Text Color
	 */
	function medicale_mascot_sidebar_title_text_color() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'sidebar-settings-sidebar-title-text-color';
		$declaration = array();
		$selector = array(
			'.sidebar-area .widget .widget-title'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		$declaration['color'] = $medicale_mascot_redux_theme_opt[$var_name];
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_sidebar_title_text_color');
}


if (!function_exists('medicale_mascot_sidebar_title_font_size')) {
	/**
	 * Generate CSS codes for Sidebar Widget Title Font Size
	 */
	function medicale_mascot_sidebar_title_font_size() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'sidebar-settings-sidebar-title-font-size';
		$declaration = array();
		$selector = array(
			'.sidebar-area .widget .widget-title'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		$declaration['font-size'] = $medicale_mascot_redux_theme_opt[$var_name] . 'px';
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_sidebar_title_font_size');
}


if (!function_exists('medicale_mascot_sidebar_title_line_bottom_color')) {
	/**
	 * Generate CSS codes for Sidebar Widget Title Line Bottom Color
	 */
	function medicale_mascot_sidebar_title_line_bottom_color() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'sidebar-settings-sidebar-title-line-bottom-color';
		$declaration = array();
		$selector = array(
			'.sidebar-area .widget .widget-title.widget-title-line-bottom:after'
		);

		if( !medicale_mascot_get_redux_option( 'sidebar-settings-sidebar-title-show-line-bottom' ) ) {
			return;
		}

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		$declaration['background-color'] = $medicale_mascot_redux_theme_opt[$var_name];
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_sidebar_title_line_bottom_color');
}