<section>
	<div class="<?php echo $container_type; ?>">
		<div class="row">
			<div class="col-md-8 main-content-area">
				<?php do_action( 'medicale_mascot_search_result_page_main_content_area_start' ); ?>
				<div class="new-search-form mb-40">
					<h3 class="mt-0"><?php esc_html_e( 'New Search', 'medicale-wp' ); ?></h3>
					<p><?php esc_html_e( 'Not happy with the results? Type your search again', 'medicale-wp' ); ?></p>
					<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
						<input type="search" class="form-control search-field" placeholder="<?php echo esc_html__( 'Search &hellip;', 'medicale-wp' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
						<button type="submit" class="search-submit"><?php esc_html_e( 'Search', 'medicale-wp' ); ?></button>
					</form>
				</div>

				<hr class="mb-40">

				<?php
				if ( have_posts() ) :

				// Start the Loop.
				while ( have_posts() ) : the_post();
				?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php
					if ( has_post_thumbnail() ) {
					?>
					<div class="row">
						<div class="col-md-3">
							<div class="entry-header">
							<?php the_post_thumbnail( 'medicale_mascot_featured_image' ); ?>
							</div>
						</div>
						<div class="col-md-9">
							<div class="entry-content">
								<h5 class="entry-title"><?php the_title(); ?></h5>
								<div class="post-excerpt">
									<?php medicale_mascot_get_excerpt(); ?>
								</div>
								<div class="post-btn-readmore">
									<a href="<?php the_permalink(); ?>" class="btn-read-more"><?php esc_html_e( 'Read more', 'medicale-wp' ); ?></a>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<?php
					} else {
					?>
					<div class="entry-content">
						<h5 class="entry-title"><?php the_title(); ?></h5>
						<div class="post-excerpt">
							<?php medicale_mascot_get_excerpt(); ?>
						</div>
						<div class="post-btn-readmore">
							<a href="<?php the_permalink(); ?>" class="btn-read-more"><?php esc_html_e( 'Read more', 'medicale-wp' ); ?></a>
						</div>
						<div class="clearfix"></div>
					</div>
					<?php
					}
					?>
				</article>
				<?php
				endwhile;

				// Previous/next page navigation.
				medicale_mascot_get_pagination();

				else :

				// If no content, include the "No posts found" template.
				?>
				<p><?php esc_html_e( 'Sorry, no results were found for this query', 'medicale-wp' ); ?>!</p>
				<?php
				endif;
				?>
				<?php do_action( 'medicale_mascot_search_result_page_main_content_area_end' ); ?>
			</div>
			<div class="col-md-4 sidebar-area sidebar-right">
				<?php get_sidebar( 'right' ); ?>
			</div>
		</div>
	</div>
</section>