<?php
	/**
	* medicale_mascot_before_portfolio_section hook.
	*
	*/
	do_action( 'medicale_mascot_before_portfolio_section' );
?>
<section>
	<div class="<?php echo $container_type; ?>">
	<?php
		/**
		* medicale_mascot_portfolio_container_start hook.
		*
		*/
		do_action( 'medicale_mascot_portfolio_container_start' );
	?>

	<?php
		medicale_mascot_get_portfolio_sidebar_layout();
	?>

	<div class="pagination-wrapper <?php echo $text_align; ?>">
		<?php
		medicale_mascot_get_pagination();
		?>
	</div>
	
	<?php
		/**
		* medicale_mascot_portfolio_container_end hook.
		*
		*/
		do_action( 'medicale_mascot_portfolio_container_end' );
	?>
	</div>
</section>
<?php
	/**
	* medicale_mascot_after_portfolio_section hook.
	*
	*/
	do_action( 'medicale_mascot_after_portfolio_section' );
?>
