<div class="row">
  <div class="col-md-12 main-content-area">
	<?php do_action( 'medicale_mascot_portfolio_main_content_area_start' ); ?>
	<?php
		medicale_mascot_get_portfolio_content();
	?>
	<?php do_action( 'medicale_mascot_portfolio_main_content_area_end' ); ?>
  </div>
</div>