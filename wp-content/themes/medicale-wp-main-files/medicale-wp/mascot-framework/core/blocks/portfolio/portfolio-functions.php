<?php
use MASCOTCORE\CPT\Portfolio\CPT_Portfolio;

if(!function_exists('medicale_mascot_get_portfolio')) {
	/**
	 * Function that Renders Portfolio HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_portfolio( $container_type = 'container' ) {
		$params = array();

		$params['container_type'] = $container_type;

		if( medicale_mascot_get_redux_option( 'portfolio-layout-settings-fullwidth' ) ) {
			$params['container_type'] = 'container-fluid';
		}

		$params['text_align'] = medicale_mascot_get_redux_option( 'portfolio-layout-settings-text-align' );

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'portfolio-parts', null, 'portfolio/tpl', $params );
		
		return $html;
	}
}

if (!function_exists('medicale_mascot_get_portfolio_sidebar_layout')) {
	/**
	 * Return Portfolio Sidebar Layout HTML
	 */
	function medicale_mascot_get_portfolio_sidebar_layout() {
		$params = array();

		$params['sidebar_layout'] = medicale_mascot_get_redux_option( 'portfolio-settings-sidebar-layout' );

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'portfolio', $params['sidebar_layout'], 'portfolio/tpl/sidebar-columns', $params );
		
		return $html;
	}
}

if ( ! function_exists( 'medicale_mascot_get_portfolio_holder_ID' ) ) {
	/**
	 * Returns Portfolio Holder ID
	 *
	 */
	function medicale_mascot_get_portfolio_holder_ID() {
		$random_number = wp_rand( 111111, 999999 );
		$holder_id = 'portfolio-holder-' . $random_number;
		return $holder_id;
	}
}

if ( ! function_exists( 'medicale_mascot_get_portfolio_content' ) ) {
	/**
	 * Returns Portfolio Content
	 *
	 */
	function medicale_mascot_get_portfolio_content() {
		$params = array();

		$params['holder_id'] = medicale_mascot_get_portfolio_holder_ID();

		medicale_mascot_get_portfolio_top_part( $params['holder_id'] );
		
		$params['portfolio_type'] = medicale_mascot_get_redux_option( 'portfolio-layout-settings-select-portfolio-type' );

		$params['portfolio_layout_mode'] = medicale_mascot_get_redux_option( 'portfolio-layout-settings-select-portfolio-layout-mode' );
		$params['items_per_row'] = medicale_mascot_get_redux_option( 'portfolio-layout-settings-items-per-row' );

		//Reveal Animation Effect
		$params['add_animation_effect'] = medicale_mascot_get_redux_option( 'portfolio-layout-settings-portfolio-items-add-animation-effect' );
		$params['animation_effect'] = '';
		if( $params['add_animation_effect'] ) {
			$params['animation_effect'] = 'wow ' . medicale_mascot_get_redux_option( 'portfolio-layout-settings-portfolio-items-animation-effect' );
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'portfolio-content', null, 'portfolio/tpl/content', $params );
		
		return $html;
	}
}

if ( ! function_exists( 'medicale_mascot_get_portfolio_type' ) ) {
	/**
	 * Returns Portfolio Type
	 *
	 */
	function medicale_mascot_get_portfolio_type() {
		$params = array();

		$params['portfolio_type'] = medicale_mascot_get_redux_option( 'portfolio-layout-settings-select-portfolio-type' );
		$params['showhide_options'] = medicale_mascot_get_redux_option( 'portfolio-layout-settings-portfolio-showhide-options' );


		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'portfolio', $params['portfolio_type'], 'portfolio/tpl/type', $params );
		
		return $html;
	}
}

if(!function_exists('medicale_mascot_get_portfolio_thumb')) {
	/**
	 * Returns Portfolio Thumb
	 * @return HTML
	 */
	function medicale_mascot_get_portfolio_thumb() {
		$params = array();

		$params['thumbnail_orientation'] = medicale_mascot_get_redux_option( 'portfolio-layout-settings-thumbnail-orientation' );
		$params['thumbnail_ratio'] = medicale_mascot_get_redux_option( 'portfolio-layout-settings-thumbnail-ratio' );

		//image dimension
		$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
		$full_image_url = $full_image_url[0];

		$image_dimension = medicale_mascot_get_image_dimensions( $params['thumbnail_orientation'], $params['thumbnail_ratio'] );

		//create resized image
		$resized_image = medicale_mascot_matthewruddy_image_resize( $full_image_url, $image_dimension['width'], $image_dimension['height'], true );

		// if has no post thubnail or gallery images then add placeholder image
		if( !is_array( $resized_image ) || !$resized_image['url'] || $resized_image['url'] == '' ) {
			$resized_image = array();
			$full_image_url = $resized_image['url'] = "http://placehold.it/".$image_dimension['width']."x".$image_dimension['height']."?text=Image Not Found!";
		}
		$params['resized_image'] = $resized_image;
		$params['image_dimension'] = $image_dimension;

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'thumb', null, 'portfolio/tpl/parts', $params );
		
		return $html;
	}
}

if(!function_exists('medicale_mascot_get_portfolio_thumb_lightbox')) {
	/**
	 * Returns Portfolio Thumb Lightbox
	 * @return HTML
	 */
	function medicale_mascot_get_portfolio_thumb_lightbox() {
		$params = array();

		$params['lightbox_type'] = medicale_mascot_get_redux_option( 'portfolio-layout-settings-lightbox-type' );

		//full image url
		$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
		$params['full_image_url'] = $full_image_url[0];

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'lightbox', $params['lightbox_type'], 'portfolio/tpl/lightbox', $params );
		
		return $html;
	}
}

if(!function_exists('medicale_mascot_get_portfolio_hover_link')) {
	/**
	 * Returns Portfolio Hover Link
	 * @return HTML
	 */
	function medicale_mascot_get_portfolio_hover_link() {
		$params = array();

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'hover-link', null, 'portfolio/tpl/parts', $params );
		
		return $html;
	}
}

if(!function_exists('medicale_mascot_get_portfolio_excerpt')) {
	/**
	 * Returns Portfolio Excerpt
	 * @return HTML
	 */
	function medicale_mascot_get_portfolio_excerpt() {
		$params = array();
		$html = '';

		//Display Content Type
		$display_content_type = medicale_mascot_get_redux_option( 'portfolio-layout-settings-display-content-type' );
		$excerpt_length = medicale_mascot_get_redux_option( 'portfolio-layout-settings-excerpt-length' );
		
		switch ( $display_content_type ) {
			case 'full-excerpt':
				# code...
				$html = '<p>'.get_the_excerpt().'</p>';
				break;

			case 'custom-excerpt':
				# code...
				$html = '<p>'.wp_trim_words( strip_shortcodes( get_the_content() ), $excerpt_length, '...' ).'</p>';
				break;
			
			default:
				# code...
				$html = get_the_content();
				break;
		}
		return $html;
	}
}

if(!function_exists('medicale_mascot_get_portfolio_category_taxonomy_terms')) {
	/**
	 * Returns Portfolio Category Taxonomy Terms
	 * @return HTML
	 */
	function medicale_mascot_get_portfolio_category_taxonomy_terms() {
		$new_cpt_class = CPT_Portfolio::Instance();
		$terms = get_the_terms( get_the_ID(), $new_cpt_class->ptTaxKey );
		$out = array();
		if ( ! empty( $terms ) ) {
			foreach ( $terms as $term ) {
				$out[] = sprintf( '<a href="%1$s">%2$s</a>',
					esc_url( get_term_link( $term->slug, $new_cpt_class->ptTaxKey ) ),
					esc_html( $term->name )
				);
			}
		}
		$html = implode( ', ', $out );
		return $html;
	}
}

if(!function_exists('medicale_mascot_get_portfolio_tag_taxonomy_terms')) {
	/**
	 * Returns Portfolio Tag Taxonomy Terms
	 * @return HTML
	 */
	function medicale_mascot_get_portfolio_tag_taxonomy_terms() {
		$new_cpt_class = CPT_Portfolio::Instance();
		$terms = get_the_terms( get_the_ID(), $new_cpt_class->ptTagTaxKey );
		$out = array();
		if ( ! empty( $terms ) ) {
			foreach ( $terms as $term ) {
				$out[] = sprintf( '<a href="%1$s">%2$s</a>',
					esc_url( get_term_link( $term->slug, $new_cpt_class->ptTagTaxKey ) ),
					esc_html( $term->name )
				);
			}
		}
		$html = implode( ', ', $out );
		return $html;
	}
}



if(!function_exists('medicale_mascot_get_portfolio_top_part')) {
	/**
	 * Returns Top Part on Portfolio Container
	 * @return HTML
	 */
	function medicale_mascot_get_portfolio_top_part( $holder_id ) {
		$params = array();

		$params['holder_id'] = $holder_id;

		$params['filter_sorter_position'] = medicale_mascot_get_redux_option( 'portfolio-layout-settings-portfolio-filter-sorter-position' );
		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'top-part', null, 'portfolio/tpl/parts', $params );
		
		return $html;
	}
}



if(!function_exists('medicale_mascot_get_portfolio_cagetory_filters')) {
	/**
	 * Returns Category Filter on Portfolio Items
	 * @return HTML
	 */
	function medicale_mascot_get_portfolio_cagetory_filters( $holder_id ) {
		$params = array();

		$params['holder_id'] = $holder_id;

		$params['show_portfolio_filter'] = medicale_mascot_get_redux_option( 'portfolio-layout-settings-portfolio-filter' );
		$params['portfolio_filter_style'] = medicale_mascot_get_redux_option( 'portfolio-layout-settings-portfolio-filter-style' );

		if( !$params['show_portfolio_filter'] ) {
			return;
		}

		$new_cpt_class = CPT_Portfolio::Instance();
		$portfolio_filters = array();

		// Start the loop.
		while ( have_posts() ) : the_post();
			$term_list = wp_get_post_terms( get_the_ID(), $new_cpt_class->ptTaxKey, array("fields" => "all") );
			if( !empty( $term_list ) ) {
				foreach($term_list as $term_single) {
					$portfolio_filters[ $term_single->slug ] = $term_single->name;
				}
			}
		// End of the loop.
		endwhile;
		$params['portfolio_filters'] = array_unique( $portfolio_filters );

		$params['filter_sorter_position'] = medicale_mascot_get_redux_option( 'portfolio-layout-settings-portfolio-filter-sorter-position' );

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'category-filter', $params['portfolio_filter_style'], 'portfolio/tpl/parts', $params );
		
		return $html;
	}
}


if(!function_exists('medicale_mascot_get_portfolio_sorter')) {
	/**
	 * Returns Sorter on Portfolio Items
	 * @return HTML
	 */
	function medicale_mascot_get_portfolio_sorter( $holder_id ) {
		$params = array();
		
		$params['holder_id'] = $holder_id;

		$params['show_portfolio_sorter'] = medicale_mascot_get_redux_option( 'portfolio-layout-settings-portfolio-sorter' );
		$params['portfolio_sorter_style'] = medicale_mascot_get_redux_option( 'portfolio-layout-settings-portfolio-sorter-style' );

		if( !$params['show_portfolio_sorter'] ) {
			return;
		}

		$params['filter_sorter_position'] = medicale_mascot_get_redux_option( 'portfolio-layout-settings-portfolio-filter-sorter-position' );
		$params['sorter_elements'] = medicale_mascot_get_redux_option( 'portfolio-layout-settings-portfolio-sorter-elements' );

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'portfolio-sorter', $params['portfolio_sorter_style'], 'portfolio/tpl/parts', $params );
		
		return $html;
	}
}



if(!function_exists('medicale_mascot_get_portfolio_post_terms')) {
	/**
	 * Returns Portfolio Post Terms
	 * @return HTML
	 */
	function medicale_mascot_get_portfolio_post_terms( $post_id = null ) {
		$new_cpt_class = CPT_Portfolio::Instance();

		$term_list = wp_get_post_terms( $post_id, $new_cpt_class->ptTaxKey, array("fields" => "slugs") );
		
		return $term_list;
	}
}