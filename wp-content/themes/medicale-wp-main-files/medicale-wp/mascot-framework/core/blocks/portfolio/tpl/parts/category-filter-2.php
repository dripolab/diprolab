
			<!-- Portfolio Filter -->
			<div class="gallery-isotope-filter style-boxed <?php if( $filter_sorter_position == 'text-left' ) echo "pull-left flip sm-pull-none"; ?>" data-link-with="<?php echo $holder_id ?>">
				<a href="#" class="active" data-filter="*">All</a>
				<?php
					foreach ($portfolio_filters as $key => $value) {
						# code...
						echo '<a href="#' . $key . '" data-filter=".' . $key . '">' . $value . '</a>';
					}
				?>
			</div>
			<!-- End Portfolio Filter -->