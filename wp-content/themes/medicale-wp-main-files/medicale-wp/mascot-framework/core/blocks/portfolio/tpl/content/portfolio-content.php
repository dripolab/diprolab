<!-- portfolio Masonry -->
<div id="<?php echo $holder_id ?>" class="gallery-isotope portfolio-archive <?php echo $portfolio_layout_mode; ?> grid-<?php echo $items_per_row; ?> <?php echo 'portfolio-type-' . $portfolio_type; ?> clearfix">
	<?php if( $portfolio_layout_mode == 'masonry' ) { ?>
	<div class="gallery-item gallery-item-sizer"></div>
	<?php } ?>
	<?php
	if ( have_posts() ) :
		// Start the Loop.
		$i = 0;
		$animation_wow_delay_base = 0.15;
		$reveal_delay_attribute = '';
		while ( have_posts() ) : the_post();
			if( $add_animation_effect ) {
				//if( $reveal_delay == 0 ) $reveal_delay = 0.15;
				$reveal_delay = $i % ( $items_per_row * 2 ) * $animation_wow_delay_base;
				$reveal_delay_attribute = 'data-wow-delay="' . $reveal_delay . 's"';
			}
			$term_list = medicale_mascot_get_portfolio_post_terms( get_the_ID() );
			$term_string = implode( ' ', $term_list );
	?>
			<!-- Portfolio Item Start -->
			<div class="gallery-item <?php echo $term_string; ?> portfolio-item box-hover-effect <?php echo 'effect-' . $portfolio_type; ?> <?php echo $animation_effect; ?>" <?php echo $reveal_delay_attribute; ?> data-date="<?php the_time('c'); ?>">
			<?php
					medicale_mascot_get_portfolio_type();
			?>
			</div>
			<!-- Portfolio Item End -->
	<?php
			$i++;
		endwhile;
	else :
		// If no content, include the "No posts found" template.
		echo "No posts found!";
	endif;
	?>

</div>
<!-- Blog Masonry -->