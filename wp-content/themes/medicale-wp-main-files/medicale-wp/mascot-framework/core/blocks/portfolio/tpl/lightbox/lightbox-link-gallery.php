<a href="<?php echo $full_image_url ?>" data-rel="prettyPhoto[gallery-prettyphoto-<?php echo get_the_ID();?>]" title="<?php echo get_the_title(); ?>"><i class="fa fa-picture-o"></i></a>

<?php
	$gallery_images = rwmb_meta( 'medicale_mascot_' . "portfolio_metabox_portfolio_gallery_images", 'size=full' );
?>
<div class="hidden">
	<?php
		if ( !empty( $gallery_images ) ) {
			foreach ( $gallery_images as $each_gallery_image ) {
			?>
			<a data-rel="prettyPhoto[gallery-prettyphoto-<?php echo get_the_ID();?>]" title="<?php echo get_the_title(); ?>" href="<?php echo $each_gallery_image['url'];?>"></a>
			<?php
			}
		}
	?>
</div>