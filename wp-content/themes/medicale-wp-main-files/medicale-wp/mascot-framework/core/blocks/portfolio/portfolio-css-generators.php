<?php


if (!function_exists('medicale_mascot_portfolio_settings_gutter_size')) {
	/**
	 * Generate CSS codes for Portfolio Column Spacing (Gutter Size)
	 */
	function medicale_mascot_portfolio_settings_gutter_size() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'portfolio-layout-settings-gutter-size';
		$declaration = array();
		$selector = array(
			'.gallery-isotope.portfolio-archive .gallery-item'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}

		$column_gutter = $medicale_mascot_redux_theme_opt[$var_name] / 2;

		$declaration['padding'] = $column_gutter . 'px';
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_portfolio_settings_gutter_size');
}
