
	<div class="effect-wrapper">
		<div class="thumb">
		<?php medicale_mascot_get_portfolio_thumb(); ?>
		</div>
		<div class="overlay-shade"></div>
		<div class="text-holder">
			<div class="text-holder-inner">
				<?php if( $showhide_options['show-portfolio-title'] ) { ?>
				<h5 class="title"><?php echo get_the_title(); ?></h5>
				<?php } ?>
				<?php if( $showhide_options['show-portfolio-date'] ) { ?>
				<p class="date"><?php medicale_mascot_posted_on();?></p>
				<?php } ?>
			</div>
		</div>
		<div class="icons-holder icons-holder-top-left">
			<div class="icons-holder-inner">
				<ul class="list-inline">
				<?php if( $showhide_options['show-portfolio-like-button'] ) { ?>
				<li class="like"><?php echo medicale_mascot_sl_get_simple_likes_button( get_the_ID() ); ?></li>
				<?php } ?>
				<?php if( $showhide_options['show-portfolio-comments-count'] ) { ?>
				<li class="comment"><i class="fa fa-commenting-o"></i> <?php echo medicale_mascot_get_comments_number(); ?></li>
				<?php } ?>
				</ul>
			</div>
		</div>
		<div class="icons-holder icons-holder-top-right">
			<div class="icons-holder-inner">
				<div class="styled-icons icon-sm icon-dark">
				<a href="<?php echo esc_url( get_permalink() ) ?>"><i class="fa fa-link"></i></a>
				<?php medicale_mascot_get_portfolio_thumb_lightbox(); ?>
				</div>
			</div>
		</div>
		<?php medicale_mascot_get_portfolio_hover_link(); ?>
	</div>
	<ul class="entry-meta list-inline">
	<?php if( $showhide_options['show-portfolio-category'] ) { ?>
		<li><i class="fa fa-folder-o"></i> <?php echo medicale_mascot_get_portfolio_category_taxonomy_terms(); ?></li>
		<?php } ?>
		<?php if( $showhide_options['show-portfolio-tag'] ) { ?>
		<li><i class="fa fa-tags"></i> <?php echo medicale_mascot_get_portfolio_tag_taxonomy_terms(); ?></li>
	<?php } ?>
	</ul>
	<?php if( $showhide_options['show-portfolio-content'] ) { ?>
	<div class="content"><?php echo medicale_mascot_get_portfolio_excerpt(); ?></div>
	<?php } ?>