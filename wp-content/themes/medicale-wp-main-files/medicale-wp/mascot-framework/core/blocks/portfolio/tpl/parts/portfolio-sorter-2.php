
<div class="dropdown <?php echo ( $filter_sorter_position == 'text-left' ) ? "pull-right flip sm-pull-none" : "display-inline clearfix"; ?>">
	<button class="btn btn-default btn-sm dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
	Sorter
		<span class="caret"></span>
	</button>
	<ul class="dropdown-menu gallery-isotope-sorter" aria-labelledby="dropdownMenu1" data-link-with="<?php echo $holder_id ?>">
		<?php if( $sorter_elements['sorter-date'] ) { ?>
		<li><a href="#" data-sortby="date">Date</a></li>
		<?php } ?>
		<?php if( $sorter_elements['sorter-name'] ) { ?>
		<li><a href="#" data-sortby="name">Name</a></li>
		<?php } ?>
		<?php if( $sorter_elements['sorter-random'] ) { ?>
		<li><a href="#" data-sortby="random">Random</a></li>
		<?php } ?>
		<?php if( $sorter_elements['sorter-original-order'] ) { ?>
		<li><a href="#" data-sortby="original-order">Original Order</a></li>
		<?php } ?>
		<?php if( $sorter_elements['sorter-shuffle'] ) { ?>
		<li><a href="#" data-sortby="shuffle">Shuffle</a></li>
		<?php } ?>
	</ul>
</div>