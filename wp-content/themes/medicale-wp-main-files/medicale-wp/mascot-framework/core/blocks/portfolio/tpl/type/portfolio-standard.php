
	<div class="effect-wrapper">
		<div class="thumb">
		<?php medicale_mascot_get_portfolio_thumb(); ?>
		</div>
		<div class="overlay-shade"></div>
		<div class="icons-holder icons-holder-middle">
			<div class="icons-holder-inner">
				<div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
				<?php medicale_mascot_get_portfolio_thumb_lightbox(); ?>
				</div>
			</div>
		</div>
		<?php if( $showhide_options['show-portfolio-date'] ) { ?>
		<div class="text-holder text-holder-bottom-right">
			<div class="text-holder-inner">
				<p class="date"><?php medicale_mascot_posted_on();?></p>
			</div>
		</div>
		<?php } ?>
		<div class="text-holder text-holder-bottom-left">
			<div class="text-holder-inner">
				<?php if( $showhide_options['show-portfolio-tag'] ) { ?>
				<p class="tag"><?php echo medicale_mascot_get_portfolio_tag_taxonomy_terms();?></p>
				<?php } ?>
			</div>
		</div>
		<?php medicale_mascot_get_portfolio_hover_link(); ?>
	</div>
	<?php if( $showhide_options['show-portfolio-title'] ) { ?>
	<h5 class="title"><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo get_the_title(); ?></a></h5>
	<?php } ?>
	<ul class="entry-meta list-inline">
	<?php if( $showhide_options['show-portfolio-category'] ) { ?>
		<li><i class="fa fa-folder-o"></i> <?php echo medicale_mascot_get_portfolio_category_taxonomy_terms(); ?></li>
		<?php } ?>
		<?php if( $showhide_options['show-portfolio-like-button'] ) { ?>
		<li><?php echo medicale_mascot_sl_get_simple_likes_button( get_the_ID() ); ?></li>
		<?php } ?>
		<?php if( $showhide_options['show-portfolio-comments-count'] ) { ?>
		<li><i class="fa fa-commenting-o"></i> <?php echo medicale_mascot_get_comments_number(); ?></li>
	<?php } ?>
	</ul>
	<?php if( $showhide_options['show-portfolio-content'] ) { ?>
	<div class="content"><?php echo medicale_mascot_get_portfolio_excerpt(); ?></div>
	<?php } ?>