<?php
$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
$full_image_url = $full_image_url[0];
?>
<a class="hover-link" data-lightbox="image" href="<?php echo $full_image_url;?>">View more</a>