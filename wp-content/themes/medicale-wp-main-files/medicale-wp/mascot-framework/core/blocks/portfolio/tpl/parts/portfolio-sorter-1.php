
				<!-- Portfolio Sorter -->
				<div class="gallery-isotope-sorter <?php echo ( $filter_sorter_position == 'text-left' ) ? "pull-right flip sm-pull-none" : "display-inline clearfix"; ?>" role="group" data-link-with="<?php echo $holder_id ?>">
					<div class="btn-group">
						<?php if( $sorter_elements['sorter-date'] ) { ?>
						<a class="btn btn-default btn-sm" data-sortby="date">Date</a>
						<?php } ?>
						<?php if( $sorter_elements['sorter-name'] ) { ?>
						<a class="btn btn-default btn-sm" data-sortby="name">Name</a>
						<?php } ?>
						<?php if( $sorter_elements['sorter-random'] ) { ?>
						<a class="btn btn-default btn-sm" data-sortby="random">Random</a>
						<?php } ?>
						<?php if( $sorter_elements['sorter-original-order'] ) { ?>
						<a class="btn btn-default btn-sm" data-sortby="original-order">Original Order</a>
						<?php } ?>
						<?php if( $sorter_elements['sorter-shuffle'] ) { ?>
						<a class="btn btn-default btn-sm" data-sortby="shuffle">Shuffle</a>
						<?php } ?>
					</div>
				</div>
				<!-- End Portfolio Sorter -->