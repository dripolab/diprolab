	<!-- Footer -->
	<?php
	/**
	* medicale_mascot_before_footer hook.
	*
	*/
	do_action( 'medicale_mascot_before_footer' );
	?>
	<script type="text/javascript">
	$(document).ready(function(){

		function replaceTexts(){
			if ($('#wpcf7-f2378-p111-o1 form.wpcf7-form .row .col-sm-6 label')) {
				$('#wpcf7-f2378-p111-o1 form.wpcf7-form .row .col-sm-6:nth-child(1) label').text("Nombre *");
				$('#wpcf7-f2378-p111-o1 form.wpcf7-form .row .col-sm-6:nth-child(3) label').text("Asunto");
				$('#wpcf7-f2378-p111-o1 form.wpcf7-form .row .col-sm-6:nth-child(4) label').text("Telefono");
				$('#wpcf7-f2378-p111-o1 form.wpcf7-form .row .col-sm-12 label').text("Mensaje");
				$('#wpcf7-f2378-p111-o1 form.wpcf7-form .row .col-sm-12 input').attr("value","Enviar");
			}

			if($('.woocommerce div.product .product_meta > span.posted_in')){
				var link = $('.woocommerce div.product .product_meta > span.posted_in').children().attr('href');
				var text = $('.woocommerce div.product .product_meta > span.posted_in').children().text();

				$('.woocommerce div.product .product_meta > span.posted_in').text('Categoria: ');
				$('.woocommerce div.product .product_meta > span.posted_in').append('<a href="'+link+'" rel="tag">'+text+'</a>');
				$('.single_add_to_cart_button.button.alt.btn.btn-dark.btn-theme-colored.btn-flat').text('Añadir al Carrito');
				$('.woocommerce div.product .woocommerce-tabs .nav-tabs li:nth-child(1) a').text('Descripción');
				$('.woocommerce div.product .woocommerce-tabs .nav-tabs li:nth-child(2) a').text('Información Adicional');
				$('.woocommerce div.product .woocommerce-tabs .nav-tabs li:nth-child(3) a').text('Comentarios');
				$('.woocommerce-Tabs-panel.woocommerce-Tabs-panel--description.panel.entry-content.wc-tab.tab-pane h4').text('Descripción');
				$('.text-holder.text-holder-bottom .btn.btn-dark.btn-sm.btn-theme-colored.button.product_type_variable.add_to_cart_button').text('Ver Opciones');
				$('.col-md-9.main-content-area.md-pull-right .woocommerce-result-count').text('Mostrando Todos Los Resultados');
				$('.woocommerce-product-search button').text('Buscar');
				$('.widget_shopping_cart_content .woocommerce-mini-cart__empty-message').text('No se encontraron productos en su carretilla');
				$('#woocommerce-product-search-field-0').attr('placeholder','Busqueda de Productos');

				console.log("Check Producto Detalle");
				bandera = true;
			}

			if ($('.woocommerce-message.alert.alert-info.alert-dismissible .button.wc-forward')) {
				var Button = $('.woocommerce-message.alert.alert-info.alert-dismissible').children("a");
				$('.woocommerce-message.alert.alert-info.alert-dismissible .button.wc-forward').text("Ver Carretilla");
				$('.woocommerce-message.alert.alert-info.alert-dismissible').text("Se añadio a su carretilla");
				$('.woocommerce-message.alert.alert-info.alert-dismissible').append(Button);
			}

			if ($('.woocommerce-message.alert.alert-info.alert-dismissible')) {
				var ButtonText = $('.woocommerce-message.alert.alert-info.alert-dismissible').children();
				$('.woocommerce-message.alert.alert-info.alert-dismissible').text("Carretilla Actualizada");
				$('.woocommerce-message.alert.alert-info.alert-dismissible').append(ButtonText);
			}
			$('.woocommerce .cart-empty').text("Información Enviada con éxito");
			$('.woocommerce .return-to-shop .button.wc-backward.btn.btn-dark.btn-theme-colored').text("Regresa a Nuestros Productos");
			console.log("Check Producto Detalle 123123123123");

		}

		var checkTexts = setInterval(replaceTexts(), 10);


	});

	</script>
	<style media="screen">

	</style>
	<footer id="footer" class="footer <?php echo $footer_classes;?>">
		<div class="footer-bottom extra">
			<div class="container">
				<span class="phoneNumber">Teléfono: (502)2434 7649 – (502)2433 9049</span>
				<span class="adress">Dirección: 5 Calle 31 – 49 Z. 11, Utatlán – Guatemala</span>
			</div>
		</div>
<style media="screen">
.footer-bottom.extra .container{padding-bottom: 0!important;padding-top: 15px!important;}
.footer-bottom .container{padding-top: 0!important;}
	.phoneNumber, .adress{
		display: block;
		width: 50%;
		float: left;
	}
	.adress{text-align: right;}
</style>

	<?php
		if( $footer_bottom_visibility ) {
		medicale_mascot_get_footer_bottom();
		}
	?>

	<?php
		/**
		* medicale_mascot_footer_end hook.
		*
		*/
		do_action( 'medicale_mascot_footer_end' );
	?>
	</footer>
	<?php
	/**
	* medicale_mascot_after_footer hook.
	*
	*/
	do_action( 'medicale_mascot_after_footer' );
	?>
