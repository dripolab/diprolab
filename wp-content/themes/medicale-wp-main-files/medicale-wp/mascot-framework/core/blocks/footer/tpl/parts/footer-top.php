	<?php
		/**
		* medicale_mascot_before_footer_top hook.
		*
		*/
		do_action( 'medicale_mascot_before_footer_top' );
	?>
	<div class="footer-top <?php echo $footer_top_text_alignment; ?> <?php echo $footer_top_classes;?>" style="<?php echo $footer_bgcolor; ?> <?php echo $footer_bgimg; ?>">
		<div class="<?php echo $footer_top_container_class; ?>" style="<?php echo $footer_top_padding_top; ?> <?php echo $footer_top_padding_bottom; ?>">
			<div class="row <?php echo $footer_top_column_separator_enabled; ?>">
			<?php
				/**
				* medicale_mascot_footer_top_start hook.
				*
				*/
				do_action( 'medicale_mascot_footer_top_start' );
			?>
			<?php
				medicale_mascot_get_footer_top_columns_layout();
			?>
			<?php
				/**
				* medicale_mascot_footer_top_end hook.
				*
				*/
				do_action( 'medicale_mascot_footer_top_end' );
			?>
			</div>
		</div>
	</div>
	<?php
		/**
		* medicale_mascot_after_footer_top hook.
		*
		*/
		do_action( 'medicale_mascot_after_footer_top' );
	?>