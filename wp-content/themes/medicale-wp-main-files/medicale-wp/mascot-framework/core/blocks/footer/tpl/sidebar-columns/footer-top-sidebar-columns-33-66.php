
		<div class="col-md-4 <?php echo $footer_top_column_class;?>">
			<?php if ( is_active_sidebar( 'footer-sidebar-top-column-1' ) ) : ?>
				<?php dynamic_sidebar( 'footer-sidebar-top-column-1' ); ?>
			<?php endif; ?>
		</div>
		<div class="col-md-8 <?php echo $footer_top_column_class;?>">
			<?php if ( is_active_sidebar( 'footer-sidebar-top-column-2' ) ) : ?>
				<?php dynamic_sidebar( 'footer-sidebar-top-column-2' ); ?>
			<?php endif; ?>
		</div>