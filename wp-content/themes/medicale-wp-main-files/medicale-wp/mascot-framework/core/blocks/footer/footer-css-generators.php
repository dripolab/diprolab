<?php

if (!function_exists('medicale_mascot_footer_bg')) {
	/**
	 * Generate CSS codes for Widget Footer Background
	 */
	function medicale_mascot_footer_bg() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'footer-settings-footer-top-bg';
		$declaration = array();
		$selector = array(
			'footer#footer'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		$declaration = medicale_mascot_redux_option_field_background( $var_name );
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_footer_bg');
}



/*Footer Top Starts*/
if (!function_exists('medicale_mascot_footer_top_padding_top_bottom')) {
	/**
	 * Generate CSS codes for footer top Padding Top & Bottom
	 */
	function medicale_mascot_footer_top_padding_top_bottom() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'footer-settings-footer-top-padding-top-bottom';
		$declaration = array();
		$selector = array(
			'footer#footer .footer-top .container',
			'footer#footer .footer-top .container-fluid'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		//added padding into the container.
		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name]['padding-top'] != "" ) {
			$declaration['padding-top'] = $medicale_mascot_redux_theme_opt[$var_name]['padding-top'];
		}
		if( $medicale_mascot_redux_theme_opt[$var_name]['padding-bottom'] != "" ) {
			$declaration['padding-bottom'] = $medicale_mascot_redux_theme_opt[$var_name]['padding-bottom'];
		}
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_footer_top_padding_top_bottom');
}

if (!function_exists('medicale_mascot_footer_top_widget_title_typography')) {
	/**
	 * Generate CSS codes for Footer Top Widget Title Typography
	 */
	function medicale_mascot_footer_top_widget_title_typography() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'footer-settings-footer-top-widget-title-typography';
		$declaration = array();
		$selector = array(
			'footer#footer .footer-top .widget .widget-title',
			'footer#footer.footer-inverted .footer-top .widget .widget-title'
		);

		$declaration = medicale_mascot_redux_option_field_typography( $var_name );
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_footer_top_widget_title_typography');
}

if (!function_exists('medicale_mascot_footer_top_widget_text_typography')) {
	/**
	 * Generate CSS codes for Footer Top Widget Text Typography
	 */
	function medicale_mascot_footer_top_widget_text_typography() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'footer-settings-footer-top-widget-text-typography';
		$declaration = array();
		$selector = array(
			'footer#footer .footer-top .widget',
			'footer#footer.footer-inverted .footer-top .widget'
		);

		$declaration = medicale_mascot_redux_option_field_typography( $var_name );
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_footer_top_widget_text_typography');
}

if (!function_exists('medicale_mascot_footer_top_widget_link_typography')) {
	/**
	 * Generate CSS codes for Footer Top Widget Link Typography
	 */
	function medicale_mascot_footer_top_widget_link_typography() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'footer-settings-footer-top-widget-link-typography';
		$declaration = array();
		$selector = array(
			'footer#footer .footer-top .widget a',
			'footer#footer.footer-inverted .footer-top .widget a'
		);

		$declaration = medicale_mascot_redux_option_field_typography( $var_name );
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_footer_top_widget_link_typography');
}

if (!function_exists('medicale_mascot_footer_top_widget_link_hover_color')) {
	/**
	 * Generate CSS codes for Footer Top Widget Link Hover Color
	 */
	function medicale_mascot_footer_top_widget_link_hover_color() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'footer-settings-footer-top-widget-link-hover-color';
		$declaration = array();
		$selector = array(
			'footer#footer .footer-top .widget a:hover',
			'footer#footer.footer-inverted .footer-top .widget a:hover'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		$declaration['color'] = $medicale_mascot_redux_theme_opt[$var_name];
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_footer_top_widget_link_hover_color');
}


/*Footer Bottom Starts*/
if (!function_exists('medicale_mascot_footer_bottom_padding_top_bottom')) {
	/**
	 * Generate CSS codes for footer bottom Padding Top & Bottom
	 */
	function medicale_mascot_footer_bottom_padding_top_bottom() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'footer-settings-footer-bottom-padding-top-bottom';
		$declaration = array();
		$selector = array(
			'footer#footer .footer-bottom .container',
			'footer#footer .footer-bottom .container-fluid'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		//added padding into the container.
		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		if( $medicale_mascot_redux_theme_opt[$var_name]['padding-top'] != "" ) {
			$declaration['padding-top'] = $medicale_mascot_redux_theme_opt[$var_name]['padding-top'];
		}
		if( $medicale_mascot_redux_theme_opt[$var_name]['padding-bottom'] != "" ) {
			$declaration['padding-bottom'] = $medicale_mascot_redux_theme_opt[$var_name]['padding-bottom'];
		}
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_footer_bottom_padding_top_bottom');
}


if (!function_exists('medicale_mascot_footer_bottom_background_color')) {
	/**
	 * Generate CSS codes for Footer Bottom Background Color
	 */
	function medicale_mascot_footer_bottom_background_color() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'footer-settings-footer-bottom-bgcolor';
		$declaration = array();
		$selector = array(
			'footer#footer .footer-bottom'
		);

		//if empty then return
		if( !array_key_exists( $var_name, $medicale_mascot_redux_theme_opt ) ) {
			return;
		}

		if( $medicale_mascot_redux_theme_opt[$var_name] == '' ) {
			return;
		}
		
		$declaration['background-color'] = $medicale_mascot_redux_theme_opt[$var_name];
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_footer_bottom_background_color');
}


if (!function_exists('medicale_mascot_footer_bottom_widget_text_typography')) {
	/**
	 * Generate CSS codes for Footer Bottom Widget Text Typography
	 */
	function medicale_mascot_footer_bottom_widget_text_typography() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'footer-settings-footer-bottom-widget-text-typography';
		$declaration = array();
		$selector = array(
			'footer#footer .footer-bottom'
		);

		$declaration = medicale_mascot_redux_option_field_typography( $var_name );
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_footer_bottom_widget_text_typography');
}

if (!function_exists('medicale_mascot_footer_bottom_widget_link_typography')) {
	/**
	 * Generate CSS codes for Footer Bottom Widget Link Typography
	 */
	function medicale_mascot_footer_bottom_widget_link_typography() {
		global $medicale_mascot_redux_theme_opt;
		$var_name = 'footer-settings-footer-bottom-widget-link-typography';
		$declaration = array();
		$selector = array(
			'footer#footer .footer-bottom a'
		);

		$declaration = medicale_mascot_redux_option_field_typography( $var_name );
		echo medicale_mascot_dynamic_css_generator($selector, $declaration);
	}
	add_action('medicale_mascot_dynamic_css_generator_action', 'medicale_mascot_footer_bottom_widget_link_typography');
}