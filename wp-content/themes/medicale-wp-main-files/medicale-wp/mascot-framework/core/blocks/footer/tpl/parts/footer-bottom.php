	<?php
		/**
		* medicale_mascot_before_footer_bottom hook.
		*
		*/
		do_action( 'medicale_mascot_before_footer_bottom' );
	?>
	<div class="footer-bottom" style="<?php echo $footer_bottom_bgcolor; ?>">
		<div class="<?php echo $footer_bottom_container_class; ?>" style="<?php echo $footer_bottom_padding_top; ?> <?php echo $footer_bottom_padding_bottom; ?>">
		<div class="row">
		<?php
			/**
			* medicale_mascot_footer_bottom_start hook.
			*
			*/
			do_action( 'medicale_mascot_footer_bottom_start' );
		?>
		<?php
			medicale_mascot_get_footer_bottom_columns_layout();
		?>
		<?php
			/**
			* medicale_mascot_footer_bottom_end hook.
			*
			*/
			do_action( 'medicale_mascot_footer_bottom_end' );
		?>

		</div>
		</div>
	</div>
	<?php
		/**
		* medicale_mascot_after_footer_bottom hook.
		*
		*/
		do_action( 'medicale_mascot_after_footer_bottom' );
	?>
