<?php


if(!function_exists('medicale_mascot_get_footer_parts')) {
	/**
	 * Function that Renders Footer HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_footer_parts() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();
		$footer_classes_array = array();
		$params['footer_classes'] = '';


		//Footer Visibility
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_visibility", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['footer_visibility'] = $temp_meta_value;
		} else {
			$params['footer_visibility'] = medicale_mascot_get_redux_option( 'footer-settings-footer-visibility', true );
		}

		if( !$params['footer_visibility'] ) {
			return;
		}


		//Fixed Footer Bottom Effect
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_fixed_footer_bottom", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			if( $temp_meta_value ) {
				$footer_classes_array[] = 'fixed-footer';
			}
		} else if( medicale_mascot_get_redux_option( 'footer-settings-fixed-footer-bottom' ) ) {
			$footer_classes_array[] = 'fixed-footer';
		}

		
		//Footer Text Color
		$footer_classes_array[] = medicale_mascot_get_redux_option( 'footer-settings-footer-text-color' );




		//Footer Top Visibility
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_show_footer_top", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['footer_top_visibility'] = $temp_meta_value;
		} else {
			$params['footer_top_visibility'] = medicale_mascot_get_redux_option( 'footer-settings-show-footer-top', true );
		}

		//Footer Bottom Visibility
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_show_footer_bottom", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['footer_bottom_visibility'] = $temp_meta_value;
		} else {
			$params['footer_bottom_visibility'] = medicale_mascot_get_redux_option( 'footer-settings-show-footer-bottom', true );
		}


		//make array into string
		if( is_array( $footer_classes_array ) && count( $footer_classes_array ) ) {
			$params['footer_classes'] = esc_attr(implode(' ', $footer_classes_array));
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'footer-parts', null, 'footer/tpl', $params );
		
		return $html;
	}
}

if (!function_exists('medicale_mascot_get_footer_top')) {
	/**
	 * Return footer top HTML
	 */
	function medicale_mascot_get_footer_top() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();
		$footer_top_classes_array = array();
		$params['footer_top_classes'] = '';
		
		//Footer Container
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_top_container", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['footer_top_container_class'] = $temp_meta_value;
		} else {
			$params['footer_top_container_class'] = medicale_mascot_get_redux_option( 'footer-settings-footer-top-container', 'container' );
		}


		//Footer Background Type
		//check if meta value is provided for this page or then get it from theme options
		$params['footer_bgcolor'] = '';
		$params['footer_bgimg'] = '';
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_top_bg_type", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['footer_bg_type'] = $temp_meta_value;

			if( $params['footer_bg_type'] == 'bg-color' ) {

				//Background Color
				$params['footer_bgcolor'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_top_bgcolor", '', $current_page_id );
				if( ! medicale_mascot_metabox_opt_val_is_empty( $params['footer_bgcolor'] ) ) {
					$params['footer_bgcolor'] = 'background-color: ' . $params['footer_bgcolor'] . '; ';
				}

			} else if ( $params['footer_bg_type'] == 'bg-img' ) {

				//Background Image
				$params['footer_bgimg'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_top_bgimg", '', $current_page_id );
				if( ! medicale_mascot_metabox_opt_val_is_empty( $params['footer_bgimg'] ) ) {
					$params['footer_bgimg'] = medicale_mascot_metabox_get_image_advanced_field_url( $params['footer_bgimg'] );
					$params['footer_bgimg'] = 'background-image: url(' . $params['footer_bgimg'] . '); ';
				}

			}
		}


		//Footer Background Parallax Effect
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_top_bg_parallax_effect", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			if( $temp_meta_value ) {
				$footer_top_classes_array[] = 'parallax';
			}
		} else if( medicale_mascot_get_redux_option( 'footer-settings-footer-top-bg-parallax-effect' ) ) {
			$footer_top_classes_array[] = 'parallax';
		}


		//Footer Background Overlay Status
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_top_bg_layer_overlay_status", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['layer_overlay_status']['status'] = $temp_meta_value;
			$params['layer_overlay_status']['from'] = 'post-meta';
		} else {
			$params['layer_overlay_status']['status'] = medicale_mascot_get_redux_option( 'footer-settings-footer-top-bg-layer-overlay-status' );
			$params['layer_overlay_status']['from'] = 'theme-options';
		}

		if( $params['layer_overlay_status']['status'] ) {
			//Overlay Color
			//check if meta value is provided for this page or then get it from theme options
			$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_top_bg_layer_overlay_color", '', $current_page_id );
			if( $params['layer_overlay_status']['from'] == 'post-meta' && !medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit") {
				$params['layer_overlay_color'] = $temp_meta_value;
			} else {
				$params['layer_overlay_color'] = medicale_mascot_get_redux_option( 'footer-settings-footer-top-bg-layer-overlay-color' );
			}
			
			//final layer overlay class
			if( $params['layer_overlay_status']['from'] == 'post-meta' ) {
				$footer_top_classes_array[] = 'layer-overlay overlay-'. $params['layer_overlay_color'] .'-'.rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_top_bg_layer_overlay_opacity", '', $current_page_id );
			} else if ( $params['layer_overlay_status']['from'] == 'theme-options' ) {
				$footer_top_classes_array[] = 'layer-overlay overlay-'. $params['layer_overlay_color'] .'-'.medicale_mascot_get_redux_option( 'footer-settings-footer-top-bg-layer-overlay' );
			}
		}




		//if Footer Top Column Separator enabled then add equal height class
		$params['footer_top_column_separator_enabled'] = '';
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_top_column_separator", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			if( $temp_meta_value ) {
				$params['footer_top_column_separator_enabled'] = 'equal-height';
			}
		} else if( medicale_mascot_get_redux_option( 'footer-settings-show-footer-top-column-separator' ) ) {
			$params['footer_top_column_separator_enabled'] = 'equal-height';
		}


		//Footer Top Columns Text Alignment
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_top_text_align", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['footer_top_text_alignment'] = $temp_meta_value;
		} else {
			$params['footer_top_text_alignment'] = esc_attr( medicale_mascot_get_redux_option( 'footer-settings-footer-top-text-align' ) );
		}


		
		//Footer Top Padding Top(px)
		$params['footer_top_padding_top'] = '';
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_top_padding_top", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( trim( $temp_meta_value ) ) && $temp_meta_value > 0 ) {
			$params['footer_top_padding_top'] = 'padding-top: ' . $temp_meta_value . 'px; ';
		}

		//Footer Top Padding Bottom(px)
		$params['footer_top_padding_bottom'] = '';
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_top_padding_bottom", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( trim( $temp_meta_value ) ) && $temp_meta_value > 0 ) {
			$params['footer_top_padding_bottom'] = 'padding-bottom: ' . $temp_meta_value . 'px; ';
		}




		//make array into string
		if( is_array( $footer_top_classes_array ) && count( $footer_top_classes_array ) ) {
			$params['footer_top_classes'] = esc_attr(implode(' ', $footer_top_classes_array));
		}
		

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'footer-top', null, 'footer/tpl/parts', $params );
		
		return $html;
	}
}

if (!function_exists('medicale_mascot_get_footer_top_columns_layout')) {
	/**
	 * Return footer top Columns Layout HTML
	 */
	function medicale_mascot_get_footer_top_columns_layout() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();
		$footer_top_column_class_array = array();
		$params['footer_top_column_class'] = '';
		
		//Footer Top Columns Layout
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_top_columns_layout", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['footer_top_columns_layout'] = $temp_meta_value;
		} else {
			$params['footer_top_columns_layout'] = medicale_mascot_get_redux_option( 'footer-settings-footer-top-columns-layout', '25-25-25-25' );
		}


		//if Footer Top Column Separator enabled
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_top_column_separator", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			if( $temp_meta_value ) {
				$footer_top_column_class_array[] = 'footer-top-column-separator';
			}
		} else if( medicale_mascot_get_redux_option( 'footer-settings-show-footer-top-column-separator' ) ) {
			$footer_top_column_class_array[] = 'footer-top-column-separator';
		}


		if( is_array( $footer_top_column_class_array ) && count( $footer_top_column_class_array ) ) {
			$params['footer_top_column_class'] = esc_attr(implode(' ', $footer_top_column_class_array));
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'footer-top-sidebar-columns', $params['footer_top_columns_layout'], 'footer/tpl/sidebar-columns', $params );
		
		return $html;
	}
}

if (!function_exists('medicale_mascot_get_footer_bottom')) {
	/**
	 * Return footer bottom HTML
	 */
	function medicale_mascot_get_footer_bottom() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();
		
		//Footer Container
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_bottom_container", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['footer_bottom_container_class'] = $temp_meta_value;
		} else {
			$params['footer_bottom_container_class'] = medicale_mascot_get_redux_option( 'footer-settings-footer-bottom-container', 'container' );
		}


		
		//Footer Bottom Padding Top(px)
		$params['footer_bottom_padding_top'] = '';
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_bottom_padding_top", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( trim( $temp_meta_value ) ) && $temp_meta_value > 0 ) {
			$params['footer_bottom_padding_top'] = 'padding-top: ' . $temp_meta_value . 'px; ';
		}

		//Footer Bottom Padding Bottom(px)
		$params['footer_bottom_padding_bottom'] = '';
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_bottom_padding_bottom", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( trim( $temp_meta_value ) ) && $temp_meta_value > 0 ) {
			$params['footer_bottom_padding_bottom'] = 'padding-bottom: ' . $temp_meta_value . 'px; ';
		}

		//Background Color
		$params['footer_bottom_bgcolor'] = '';
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_bottom_bgcolor", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "" ) {
			$params['footer_bottom_bgcolor'] = 'background-color: ' . $params['footer_bottom_bgcolor'] . '; ';
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'footer-bottom', null, 'footer/tpl/parts', $params );
		
		return $html;
	}
}

if (!function_exists('medicale_mascot_get_footer_bottom_columns_layout')) {
	/**
	 * Return footer bottom Columns Layout HTML
	 */
	function medicale_mascot_get_footer_bottom_columns_layout() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();


		//Footer Top Columns Layout
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_bottom_columns_layout", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['footer_bottom_columns_layout'] = $temp_meta_value;
		} else {
			$params['footer_bottom_columns_layout'] = medicale_mascot_get_redux_option( 'footer-settings-footer-bottom-columns-layout', '66-33' );
		}



		//Footer Bottom Column1 Text Alignment
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_bottom_column1_text_align", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['footer_bottom_column1_text_alignment'] = $temp_meta_value . ' sm-text-center';
		} else {
			$params['footer_bottom_column1_text_alignment'] = esc_attr( medicale_mascot_get_redux_option( 'footer-settings-footer-bottom-column1-text-align' ) ) . ' sm-text-center';
		}

		//Footer Bottom Column2 Text Alignment
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_bottom_column2_text_align", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['footer_bottom_column2_text_alignment'] = $temp_meta_value . ' sm-text-center';
		} else {
			$params['footer_bottom_column2_text_alignment'] = esc_attr( medicale_mascot_get_redux_option( 'footer-settings-footer-bottom-column2-text-align' ) ) . ' sm-text-center';
		}

		//Footer Bottom Column3 Text Alignment
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_bottom_column3_text_align", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['footer_bottom_column3_text_alignment'] = $temp_meta_value . ' sm-text-center';
		} else {
			$params['footer_bottom_column3_text_alignment'] = esc_attr( medicale_mascot_get_redux_option( 'footer-settings-footer-bottom-column3-text-align' ) ) . ' sm-text-center';
		}

		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'footer-bottom-sidebar-columns', $params['footer_bottom_columns_layout'], 'footer/tpl/sidebar-columns', $params );
		
		return $html;
	}
}


if (!function_exists('medicale_mascot_get_footer_bottom_column1_content')) {
	/**
	 * Return Footer Bottom Column1 Content HTML
	 */
	function medicale_mascot_get_footer_bottom_column1_content() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();
		$html = '';


		//Footer Bottom Columns 1 - Content Type
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_bottom_column1_content", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value[0] != "inherit" ) {
			$params['footer_bottom_column1_content_type'] = $temp_meta_value;
			$params['footer_bottom_column1_content_type_from'] = 'metabox';

		} else {
			$params['footer_bottom_column1_content_type'] = medicale_mascot_get_redux_option( 'footer-settings-footer-bottom-column1-content' );
			$params['footer_bottom_column1_content_type_from'] = 'redux';
		}

		//foreach content type
		if( !medicale_mascot_metabox_opt_val_is_empty( $params['footer_bottom_column1_content_type'] ) ){
		foreach ( $params['footer_bottom_column1_content_type'] as $each_content_type ) {

			switch ( $each_content_type ) {
				case 'copyright-text':
					//Copyright Text
					if( $params['footer_bottom_column1_content_type_from'] == "metabox" ) {
						$params['copyright_text'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_bottom_column1_copyright_text", '', $current_page_id );
					} else {
						$params['copyright_text'] = medicale_mascot_get_redux_option( 'footer-settings-footer-bottom-column1-copyright-text' );
					}
					break;
					
				case 'footer-nav':
					//Footer Nav
					$params['footer_nav_col_number'] = esc_html__( "Columns 1", 'medicale-wp' );
					$params['footer_nav'] = 'column1-footer-nav';
					break;
					
				case 'social-links':
					//Enabled social links
					$params['social_links'] = medicale_mascot_get_redux_option( 'social-links-ordering', false, 'Enabled' );
					break;
				
				default:
					# code...
					break;
			}

			//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
			$html = medicale_mascot_get_blocks_template_part( $each_content_type, null, 'footer/tpl/content', $params );

		}//end foreach content type
		}

		return $html;
	}
}


if (!function_exists('medicale_mascot_get_footer_bottom_column2_content')) {
	/**
	 * Return Footer Bottom Column2 Content HTML
	 */
	function medicale_mascot_get_footer_bottom_column2_content() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();
		$html = '';


		//Footer Bottom Columns 1 - Content Type
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_bottom_column2_content", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value[0] != "inherit" ) {
			$params['footer_bottom_column2_content_type'] = $temp_meta_value;
			$params['footer_bottom_column2_content_type_from'] = 'metabox';
		} else {
			$params['footer_bottom_column2_content_type'] = medicale_mascot_get_redux_option( 'footer-settings-footer-bottom-column2-content' );
			$params['footer_bottom_column2_content_type_from'] = 'redux';
		}


		//foreach content type
		if( !medicale_mascot_metabox_opt_val_is_empty( $params['footer_bottom_column2_content_type'] ) ){
		foreach ( $params['footer_bottom_column2_content_type'] as $each_content_type ) {
		
			switch ( $each_content_type ) {
				case 'copyright-text':
					//Copyright Text
					if( $params['footer_bottom_column2_content_type_from'] == "metabox" ) {
						$params['copyright_text'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_bottom_column2_copyright_text", '', $current_page_id );
					} else {
						$params['copyright_text'] = medicale_mascot_get_redux_option( 'footer-settings-footer-bottom-column2-copyright-text' );
					}
					break;
					
				case 'footer-nav':
					//Footer Nav
					$params['footer_nav_col_number'] = esc_html__( "Columns 2", 'medicale-wp' );
					$params['footer_nav'] = 'column2-footer-nav';
					break;
					
				case 'social-links':
					//Enabled social links
					$params['social_links'] = medicale_mascot_get_redux_option( 'social-links-ordering', false, 'Enabled' );
					break;
				
				default:
					# code...
					break;
			}

			//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
			$html = medicale_mascot_get_blocks_template_part( $each_content_type, null, 'footer/tpl/content', $params );

		}//end foreach content type
		}

		return $html;
	}
}


if (!function_exists('medicale_mascot_get_footer_bottom_column3_content')) {
	/**
	 * Return Footer Bottom Column3 Content HTML
	 */
	function medicale_mascot_get_footer_bottom_column3_content() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();
		$html = '';


		//Footer Bottom Columns 1 - Content Type
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_bottom_column3_content", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value[0] != "inherit" ) {
			$params['footer_bottom_column3_content_type'] = $temp_meta_value;
			$params['footer_bottom_column3_content_type_from'] = 'metabox';
		} else {
			$params['footer_bottom_column3_content_type'] = medicale_mascot_get_redux_option( 'footer-settings-footer-bottom-column3-content' );
			$params['footer_bottom_column3_content_type_from'] = 'redux';
		}


		//foreach content type
		foreach ( $params['footer_bottom_column3_content_type'] as $each_content_type ) {
		
			switch ( $each_content_type ) {
				case 'copyright-text':
					//Copyright Text
					if( $params['footer_bottom_column3_content_type_from'] == "metabox" ) {
						$params['copyright_text'] = rwmb_meta( 'medicale_mascot_' . "page_metabox_footer_bottom_column3_copyright_text", '', $current_page_id );
					} else {
						$params['copyright_text'] = medicale_mascot_get_redux_option( 'footer-settings-footer-bottom-column3-copyright-text' );
					}
					break;
					
				case 'footer-nav':
					//Footer Nav
					$params['footer_nav_col_number'] = esc_html__( "Columns 3", 'medicale-wp' );
					$params['footer_nav'] = 'column3-footer-nav';
					break;
					
				case 'social-links':
					//Enabled social links
					$params['social_links'] = medicale_mascot_get_redux_option( 'social-links-ordering', false, 'Enabled' );
					break;
				
				default:
					# code...
					break;
			}

			//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
			$html = medicale_mascot_get_blocks_template_part( $each_content_type, null, 'footer/tpl/content', $params );

		}//end foreach content type
		
		return $html;
	}
}


if (!function_exists('medicale_mascot_register_footer_sidebar')) {
	/**
	 * Register Footer Sidebar
	 */
	function medicale_mascot_register_footer_sidebar() {
		// Footer Top Sidebar Column 1
		register_sidebar( array(
			'name' => esc_html__( 'Footer Top Column 1', 'medicale-wp' ),
			'id' => 'footer-sidebar-top-column-1',
			'description'	=> esc_html__( 'Widgets in this area will be shown on Footer Top Column 1. Drag and drop your widgets here.', 'medicale-wp' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		) );

		// Footer Top Sidebar Column 2
		register_sidebar( array(
			'name' => esc_html__( 'Footer Top Column 2', 'medicale-wp' ),
			'id' => 'footer-sidebar-top-column-2',
			'description'	=> esc_html__( 'Widgets in this area will be shown on Footer Top Column 2. Drag and drop your widgets here.', 'medicale-wp' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		) );

		// Footer Top Sidebar Column 3
		register_sidebar( array(
			'name' => esc_html__( 'Footer Top Column 3', 'medicale-wp' ),
			'id' => 'footer-sidebar-top-column-3',
			'description'	=> esc_html__( 'Widgets in this area will be shown on Footer Top Column 3. Drag and drop your widgets here.', 'medicale-wp' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		) );

		// Footer Top Sidebar Column 4
		register_sidebar( array(
			'name' => esc_html__( 'Footer Top Column 4', 'medicale-wp' ),
			'id' => 'footer-sidebar-top-column-4',
			'description'	=> esc_html__( 'Widgets in this area will be shown on Footer Top Column 4. Drag and drop your widgets here.', 'medicale-wp' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		) );
	}
	add_action( 'widgets_init', 'medicale_mascot_register_footer_sidebar', 1000 );
}