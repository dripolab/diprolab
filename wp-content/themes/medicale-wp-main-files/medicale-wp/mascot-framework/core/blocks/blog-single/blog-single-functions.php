<?php


if(!function_exists('medicale_mascot_get_blog_single')) {
	/**
	 * Function that Renders Blog Single HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_blog_single( $container_type = 'container' ) {
		$params = array();


		$params['container_type'] = $container_type;

		if( medicale_mascot_get_redux_option( 'blog-single-post-settings-fullwidth' ) ) {
			$params['container_type'] = 'container-fluid';
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'blog-single-parts', null, 'blog-single/tpl', $params );
		
		return $html;
	}
}

if (!function_exists('medicale_mascot_get_blog_single_sidebar_layout')) {
	/**
	 * Return Blog Single Sidebar Layout HTML
	 */
	function medicale_mascot_get_blog_single_sidebar_layout() {
		$params = array();

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'blog-single', medicale_mascot_get_redux_option( 'blog-single-post-settings-sidebar-layout', 'sidebar-right-25' ), 'blog-single/tpl/sidebar-columns', $params );
		
		return $html;
	}
}

if (!function_exists('medicale_mascot_get_blog_single_all')) {
	/**
	 * Return Blog Single All
	 */
	function medicale_mascot_get_blog_single_all() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();

		medicale_mascot_get_blog_single_post_format( get_post_format() );

		if( medicale_mascot_get_redux_option( 'blog-single-post-settings-show-tags', true ) ) {
			medicale_mascot_get_blog_single_tags();
		}

		if( medicale_mascot_get_redux_option( 'blog-single-post-settings-show-share' ) ) {
			medicale_mascot_get_social_share_links();
		}

		if( medicale_mascot_get_redux_option( 'blog-single-post-settings-show-next-pre-post-link', true ) ) {
			medicale_mascot_get_blog_single_next_pre_post_link();
		}

		if( !empty( trim( get_the_content() ) ) && 'post' == get_post_type() && medicale_mascot_get_redux_option( 'blog-single-post-settings-author-info-box', true ) ) {
			medicale_mascot_get_blog_single_author_info_box();
		}

		if( medicale_mascot_get_redux_option( 'blog-single-post-settings-show-related-posts', true ) ) {
			$posts_count = medicale_mascot_get_redux_option( 'blog-single-post-settings-show-related-posts-count', 3 );
			medicale_mascot_get_blog_single_related_posts( $current_page_id, $posts_count );
		}

		if( medicale_mascot_get_redux_option( 'blog-single-post-settings-show-comments', true ) ) {
			medicale_mascot_show_comments();
		}
	}
}

if (!function_exists('medicale_mascot_get_blog_single_post_format')) {
	/**
	 * Return Blog Single Post Format HTML
	 */
	function medicale_mascot_get_blog_single_post_format( $post_format = '' ) {
		global $supported_post_formats;
		$params = array();

		if( !in_array( $post_format, $supported_post_formats ) ) {
			$post_format = 'standard';
		}
		$params['post_format'] = $post_format;

		$params['show_post_featured_image'] = medicale_mascot_get_redux_option( 'blog-single-post-settings-show-post-featured-image', true );
		
		$params['enable_drop_caps'] = medicale_mascot_get_redux_option( 'blog-single-post-settings-enable-drop-caps' );
		if( $params['enable_drop_caps'] ) {
			$params['enable_drop_caps'] = 'drop-caps';
		} else {
			$params['enable_drop_caps'] = '';
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'post-format', $post_format, 'blog-single/tpl/post-format', $params );
		
		return $html;
	}
}

if(!function_exists('medicale_mascot_get_single_post_title')) {
	/**
	 * Function that Renders Single Post Title HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_single_post_title() {
		if( ! medicale_mascot_get_redux_option( 'page-title-settings-show-title', true ) ) {
			return medicale_mascot_get_post_title();
		} else if ( ! medicale_mascot_get_redux_option( 'page-title-settings-enable-page-title', true ) ) {
			return medicale_mascot_get_post_title();
		}
	}
}

if(!function_exists('medicale_mascot_get_blog_single_post_thumbnail')) {
	/**
	 * Function that Renders Thumbnail HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_blog_single_post_thumbnail( $post_format = '' ) {
		global $supported_post_formats;
		$params = array();
		
		//format
		$format = $post_format ? : 'standard';
		
		if( !in_array( $post_format, $supported_post_formats ) ) {
			$post_format = 'standard';
		}
		$params['post_format'] = $post_format;

		$params['featured_image_height'] = medicale_mascot_get_redux_option( 'blog-single-post-settings-featured-image-height', 600 );

		if ( ( $params['post_format'] == "standard" || $params['post_format'] == "image" ) && !has_post_thumbnail() ) {
			return;
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'post-thumb', $post_format, 'blog-single/tpl/parts', $params );
		return $html;
	}
}

if ( ! function_exists( 'medicale_mascot_blog_single_post_meta' ) ) {
	/**
	 * Print HTML with meta information for the current post-date/time and author.
	 *
	 */
	function medicale_mascot_blog_single_post_meta() {
	?>
	<ul class="entry-meta list-inline">
	<?php
		if( medicale_mascot_get_redux_option( 'blog-single-post-settings-post-meta', false, 'show-post-by-author' ) ) {
	?>
			<li><i class="fa fa-user-o"></i> <?php medicale_mascot_posted_by();?></li>
	<?php
		} if( medicale_mascot_get_redux_option( 'blog-single-post-settings-post-meta', true, 'show-post-date' ) ) {
	?>
			<li><i class="fa fa-calendar-o"></i> <?php medicale_mascot_posted_on();?></li>
	<?php
		} if( has_category() && medicale_mascot_get_redux_option( 'blog-single-post-settings-post-meta', true, 'show-post-category' ) ) {
	?>
			<li><i class="fa fa-folder-o"></i> <?php medicale_mascot_post_category();?></li>
	<?php
		} if( comments_open() && medicale_mascot_get_redux_option( 'blog-single-post-settings-post-meta', false, 'show-post-comments-count' ) ) {
	?>
			<li><i class="fa fa-commenting-o"></i> <?php medicale_mascot_get_comments_number(); ?></li>
	<?php
		} if( medicale_mascot_get_redux_option( 'blog-single-post-settings-post-meta', false, 'show-post-like-button' ) ) {
	?>
			<li><?php echo medicale_mascot_sl_get_simple_likes_button( get_the_ID() ); ?></li>
	<?php
		}
	?>
	</ul>
	<?php
	}
}


if ( ! function_exists( 'medicale_mascot_get_blog_single_tags' ) ) {
	/**
	 * Return Blog Single Post Tags
	 *
	 */
	function medicale_mascot_get_blog_single_tags() {
		$params = array();

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'post-tags', null, 'blog-single/tpl/parts', $params );
		
		return $html;
	}
}

if ( ! function_exists( 'medicale_mascot_get_blog_single_author_info_box' ) ) {
	/**
	 * Return Blog Single Post Author Info
	 *
	 */
	function medicale_mascot_get_blog_single_author_info_box() {
		$params = array();

		$params['show_social_icons'] = medicale_mascot_get_redux_option( 'blog-single-post-settings-author-info-box-show-social-icons' );
		$params['show_author_email'] = medicale_mascot_get_redux_option( 'blog-single-post-settings-author-info-box-show-author-email' );

		//social icons
		if( $params['show_social_icons'] ) {
			$author_ID = get_the_author_meta( 'ID' );
			$social_icons_list = array();
			$social_icons = array(
				'facebook',
				'twitter',
				'linkedin',
				'googleplus',
				'instagram',
				'pinterest',
				'tumblr'
			);
			
			foreach( $social_icons as $each_social_icon ){
				$social_link = get_user_meta( $author_ID, 'medicale_mascot_' . $each_social_icon, true );
				if( $social_link != '' ) {
					$social_icons_list[$each_social_icon] = array(
						'url' => $social_link, 
						'class' => 'fa fa-'.$each_social_icon, 
					);
				}
			}
			$params['social_icons_list'] = $social_icons_list;
		}
		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'post-author-info', null, 'blog-single/tpl/parts', $params );
		
		return $html;
	}
}

if ( ! function_exists( 'medicale_mascot_get_blog_single_next_pre_post_link' ) ) {
	/**
	 * Return Blog Single Next Previous Post Link
	 *
	 */
	function medicale_mascot_get_blog_single_next_pre_post_link() {
		$params = array();

		$params['next_pre_link_within_same_cat'] = medicale_mascot_get_redux_option( 'blog-single-post-settings-show-next-pre-post-link-within-same-cat' );

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'post-next-pre-post-link', null, 'blog-single/tpl/parts', $params );
		
		return $html;
	}
}

if ( ! function_exists( 'medicale_mascot_get_blog_single_related_posts' ) ) {
	/**
	 * Return Blog Single Related Posts
	 *
	 */
	function medicale_mascot_get_blog_single_related_posts( $post_id, $related_count, $args = array() ) {
		
		$args = wp_parse_args( (array) $args, array(
			'orderby' => 'rand',
			'return'  => 'query', // Valid values are: 'query' (WP_Query object), 'array' (the arguments array)
		) );

		$related_args = array(
			'post_type'		=> get_post_type( $post_id ),
			'posts_per_page' => $related_count,
			'post_status'	=> 'publish',
			'post__not_in'   => array( $post_id ),
			'orderby'		=> $args['orderby'],
			'tax_query'		=> array()
		);

		$post		= get_post( $post_id );
		$taxonomies = get_object_taxonomies( $post, 'names' );

		foreach ( $taxonomies as $taxonomy ) {
			$terms = get_the_terms( $post_id, $taxonomy );
			if ( empty( $terms ) ) {
				continue;
			}
			$term_list					= wp_list_pluck( $terms, 'slug' );
			$related_args['tax_query'][] = array(
				'taxonomy' => $taxonomy,
				'field'	=> 'slug',
				'terms'	=> $term_list
			);
		}

		if ( count( $related_args['tax_query'] ) > 1 ) {
			$related_args['tax_query']['relation'] = 'OR';
		}

		if ( $args['return'] == 'query' ) {
			$params['related_posts_query_result'] = new WP_Query( $related_args );
		} else {
			$params['related_posts_query_result'] = $related_args;
		}

		//related posts
		$params['related_posts_carousel'] = medicale_mascot_get_redux_option( 'blog-single-post-settings-show-related-posts-carousel' );

		$posts_carousel = '';
		if( $params['related_posts_carousel'] ) {
			$posts_carousel = 'carousel';
		}
		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'post-related-posts', $posts_carousel, 'blog-single/tpl/parts', $params );
		
		return $html;
	}
}


if ( ! function_exists( 'medicale_mascot_get_blog_single_custom_password_form' ) ) {
	/**
	 * Customizing And Styling The Password Protected Form
	 *
	 */
	function medicale_mascot_get_blog_single_custom_password_form() {
		global $post;
		$label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
		$o = '<form class="post-password-form" action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post">
		<p>' . esc_html__( "This content is password protected. To view it please enter your password below:", 'medicale-wp' ) . '<p>
		<label for="' . $label . '">' . esc_html__( "Password:", 'medicale-wp' ) . ' </label><input class="form-control" name="post_password" id="' . $label . '" type="password" size="20" maxlength="20" /><input class="btn btn-dark btn-theme-colored" type="submit" name="Submit" value="' . esc_attr__( "Submit", 'medicale-wp' ) . '" />
		</form>
		';
		return $o;
	}
	add_filter( 'the_password_form', 'medicale_mascot_get_blog_single_custom_password_form' );
}


if ( ! function_exists( 'medicale_mascot_single_move_comment_field_to_bottom' ) ) {
	/**
	 * Moving the Comment Text Field to Bottom
	 *
	 */
	function medicale_mascot_single_move_comment_field_to_bottom( $fields ) {
		$comment_field = $fields['comment'];
		unset( $fields['comment'] );
		$fields['comment'] = $comment_field;
		return $fields;
	}
	add_filter( 'comment_form_fields', 'medicale_mascot_single_move_comment_field_to_bottom' );
}