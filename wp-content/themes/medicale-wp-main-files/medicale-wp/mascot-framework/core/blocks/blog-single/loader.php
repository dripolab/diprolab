<?php
require_once MASCOT_FRAMEWORK_DIR . '/core/blocks/blog-single/blog-single-metabox.php';
require_once MASCOT_FRAMEWORK_DIR . '/core/blocks/blog-single/blog-single-css-generators.php';
require_once MASCOT_FRAMEWORK_DIR . '/core/blocks/blog-single/blog-single-functions.php';