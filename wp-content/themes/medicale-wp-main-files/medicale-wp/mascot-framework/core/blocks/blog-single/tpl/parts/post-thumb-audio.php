<?php
	$mp3_link = rwmb_meta( 'medicale_mascot_' . 'settings_audio_mp3_link' );
	$ogg_link = rwmb_meta( 'medicale_mascot_' . 'settings_audio_ogg_link' );
	$soundcloud_link = rwmb_meta( 'medicale_mascot_' . 'settings_audio_soundcloud_link' );

?>
<div class="post-thumb thumb">
	<?php medicale_mascot_get_blocks_template_part( 'thumb', null, 'blog-single/tpl/parts', $params ); ?>
	<?php
		if( $soundcloud_link ):
			echo wp_oembed_get( $soundcloud_link, array('width'=>1140) );
		endif;
	?>
	<?php if( $mp3_link && $ogg_link ): ?>
	<audio id="audio-player-post-<?php the_ID(); ?>" class="media-element-audio-player" style="width: 100%" controls="controls">
		<source src="<?php echo $mp3_link;?>" type="audio/mp3" autoplay="true">
		<source src="<?php echo $ogg_link;?>" type="audio/ogg" autoplay="true">
		Your browser does not support the audio element.
	</audio>
	<?php endif; ?>
</div>