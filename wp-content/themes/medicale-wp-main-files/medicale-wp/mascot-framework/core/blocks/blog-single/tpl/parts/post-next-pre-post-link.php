<div class="row pre-next-post-link">
	<div class="col-xs-6 p-0">
		<div class="pre-post-link">
		<?php
			$pre_title = '<h4 class="title">Previous Post</h4>';
			if( $next_pre_link_within_same_cat ) {
				if( !empty( get_previous_post( true ) ) ){
					previous_post_link('%link',"<i class='fa fa-long-arrow-left'></i>", true );
					echo '<div class="content">';
					echo $pre_title;
					previous_post_link('%link', '%title', true );
					echo "</div>";
				}
			} else {
				if( !empty( get_previous_post() ) ){
					previous_post_link('%link',"<i class='fa fa-long-arrow-left'></i>" );
					echo '<div class="content">';
					echo $pre_title;
					previous_post_link('%link', '%title' );
					echo "</div>";
				}
			}
		?>
		</div>
	</div>
	<div class="col-xs-6 p-0 text-right">
		<div class="next-post-link">
		<?php
			$pre_title = '<h4 class="title">Next Post</h4>';
			if( $next_pre_link_within_same_cat ) {
				if( !empty( get_next_post( true ) ) ){
					echo '<div class="content">';
					echo $pre_title;
					next_post_link('%link', '%title', true );
					echo "</div>";
					next_post_link('%link',"<i class='fa fa-long-arrow-right'></i>", true );
				}
			} else {
				if( !empty( get_next_post() ) ){
					echo '<div class="content">';
					echo $pre_title;
					next_post_link('%link', '%title' );
					echo "</div>";
					next_post_link('%link',"<i class='fa fa-long-arrow-right'></i>" );
				}
			}
		?>
		</div>
	</div>
</div>