<div class="post-thumb thumb">
	<div class="owl-carousel owl-theme owl-carousel-1col owl-dots-center-bottom" data-nav="true" data-dots="true">
		<?php
		if( has_post_thumbnail( get_the_ID() ) ) {
		?>
		<?php medicale_mascot_get_blocks_template_part( 'thumb', null, 'blog-single/tpl/parts', $params ); ?>
		<?php
		}

		$gallery_images = rwmb_meta( 'medicale_mascot_' . 'settings_gallery_images', 'size=full' );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $gallery_images ) ) {
			foreach ( $gallery_images as $each_gallery_image ) {
				//create resized image
				$resized_image = medicale_mascot_matthewruddy_image_resize( $each_gallery_image['full_url'], '1200', '700', true );
		?>
			<div class="box-hover-effect">
				<div class="effect-wrapper">
					<div class="thumb">
						<img src="<?php echo $resized_image['url'];?>" alt="<?php the_title();?>" class="img-responsive">
					</div>
					<div class="overlay-shade"></div>
					<div class="icons-holder icons-holder-middle">
						<div class="icons-holder-inner">
							<div class="styled-icons icon-sm icon-dark">
								<a href="<?php echo $each_gallery_image['full_url'] ?>" data-rel="prettyPhoto[blog-single-featured-image-prettyphoto-<?php the_ID(); ?>]" title="<?php echo get_the_title(); ?>"><i class="fa fa-picture-o"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
			}
		}
		?>
	</div>
</div>