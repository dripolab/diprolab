<?php
	$quote_quote = rwmb_meta( 'medicale_mascot_' . 'settings_quote_quote' );
	$quote_author = rwmb_meta( 'medicale_mascot_' . 'settings_quote_author' );
?>
<div class="post-excerpt">
	<blockquote class="highlighted-quote bg-theme-colored">
		<p><?php echo $quote_quote; ?></p>
		<footer><cite title="Source Title"><?php echo $quote_author; ?></cite></footer>
	</blockquote>
</div>