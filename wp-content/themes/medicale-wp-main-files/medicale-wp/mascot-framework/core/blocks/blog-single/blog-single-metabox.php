<?php
add_action( 'rwmb_meta_boxes', 'medicale_mascot_blog_single_register_user_meta_boxes' );
function medicale_mascot_blog_single_register_user_meta_boxes( $meta_boxes ) {
	$meta_boxes[] = array(
		'title' => 'Contact Info',
		'type'  => 'user', // Specifically for user
		'fields' => array(
			array(
				'name' => esc_html__( 'Mobile phone', 'medicale-wp' ),
				'id'   => 'medicale_mascot_' . 'mobile',
				'type' => 'text',
			),
			array(
				'name' => esc_html__( 'Work phone', 'medicale-wp' ),
				'id'   => 'medicale_mascot_' . 'work',
				'type' => 'text',
			),
			array(
				'name' => esc_html__( 'Address', 'medicale-wp' ),
				'id'   => 'medicale_mascot_' . 'address',
				'type' => 'textarea',
			),
		),
	);
	$meta_boxes[] = array(
		'title' => 'Social Networks',
		'type'  => 'user', // Specifically for user
		'fields' => array(
			array(
				'name' => esc_html__( 'Facebook', 'medicale-wp' ),
				'id'   => 'medicale_mascot_' . 'facebook',
				'type' => 'text',
			),
			array(
				'name' => esc_html__( 'Twitter', 'medicale-wp' ),
				'id'   => 'medicale_mascot_' . 'twitter',
				'type' => 'text',
			),
			array(
				'name' => esc_html__( 'Linkedin', 'medicale-wp' ),
				'id'   => 'medicale_mascot_' . 'linkedin',
				'type' => 'text',
			),
			array(
				'name' => esc_html__( 'Google Plus', 'medicale-wp' ),
				'id'   => 'medicale_mascot_' . 'googleplus',
				'type' => 'text',
			),
			array(
				'name' => esc_html__( 'Instagram', 'medicale-wp' ),
				'id'   => 'medicale_mascot_' . 'instagram',
				'type' => 'text',
			),
			array(
				'name' => esc_html__( 'Pinterest', 'medicale-wp' ),
				'id'   => 'medicale_mascot_' . 'pinterest',
				'type' => 'text',
			),
			array(
				'name' => esc_html__( 'Tumblr', 'medicale-wp' ),
				'id'   => 'medicale_mascot_' . 'tumblr',
				'type' => 'text',
			),
		),
	);
	return $meta_boxes;
}