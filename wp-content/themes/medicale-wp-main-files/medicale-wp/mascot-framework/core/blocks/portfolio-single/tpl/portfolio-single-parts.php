<?php
	/**
	* medicale_mascot_before_portfolio_single_section hook.
	*
	*/
	do_action( 'medicale_mascot_before_portfolio_single_section' );
?>
<section class="portfolio-single-section">
	<div class="<?php echo $container_type;?>">
	<?php
		/**
		* medicale_mascot_portfolio_single_container_start hook.
		*
		*/
		do_action( 'medicale_mascot_portfolio_single_container_start' );
	?>
	<?php
	if ( have_posts() ) :
		// Start the Loop.
		while ( have_posts() ) : the_post();
		medicale_mascot_get_portfolio_single_sidebar_layout();
		endwhile;
	else :
		// If no content, include the "No posts found" template.
		echo "No posts found!";
	endif;
	?>
	<?php
		/**
		* medicale_mascot_portfolio_single_container_end hook.
		*
		*/
		do_action( 'medicale_mascot_portfolio_single_container_end' );
	?>
	</div>
</section>
<?php
	/**
	* medicale_mascot_after_portfolio_single_section hook.
	*
	*/
	do_action( 'medicale_mascot_after_portfolio_single_section' );
?>