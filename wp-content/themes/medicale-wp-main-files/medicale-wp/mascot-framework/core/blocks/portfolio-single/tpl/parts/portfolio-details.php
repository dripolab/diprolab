
		<h4 class="title"><?php echo get_the_title(); ?></h4>
		<div class="portfolio-content">
			<?php do_action( 'medicale_mascot_portfolio_single_content_start' ); ?>
			<?php the_content();?>
			<?php medicale_mascot_get_post_wp_link_pages(); ?>
			<?php do_action( 'medicale_mascot_portfolio_single_content_end' ); ?>
		</div>
		<?php do_action( 'medicale_mascot_portfolio_single_postmeta_start' ); ?>
		<?php medicale_mascot_portfolio_single_post_meta(); ?>
		<div class="mb-30"></div>
		<?php
		if( $show_share ) {
			medicale_mascot_get_social_share_links();
		}
		?>
		<div class="clearfix"></div>
		<?php medicale_mascot_get_portfolio_single_launch_project_link(); ?>
		<?php do_action( 'medicale_mascot_portfolio_single_postmeta_end' ); ?>