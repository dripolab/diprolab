<div class="row">
  <div class="col-md-12 main-content-area">
	<div id="post-<?php the_ID(); ?>" class="portfolio-single <?php echo $portfolio_details_type;?>">
		<?php do_action( 'medicale_mascot_portfolio_single_main_content_area_start' ); ?>
		<?php
		medicale_mascot_get_portfolio_single_all();
		?>
		<?php do_action( 'medicale_mascot_portfolio_single_main_content_area_end' ); ?>
	</div>
  </div>
</div>