<div class="launch-project-link">
	<a class="btn btn-gray btn-theme-colored" <?php if( $project_link_target ) echo 'target="_blank"'; ?> href="<?php echo $project_link_url; ?>"><?php echo $project_link_title; ?></a>
</div>