<?php
$gallery_layout_mode = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-portfolio-type-big-image-gallery-layout-mode' );
$items_per_row = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-portfolio-type-big-image-gallery-items-per-row' );
?>
<div class="portfolio-container">
	<div class="portfolio-images">
	<!-- portfolio Masonry -->
	<div class="gallery-isotope grid-<?php echo $items_per_row; ?> <?php echo $gallery_layout_mode; ?> clearfix">
		<?php if( $gallery_layout_mode == 'masonry' ) { ?>
		<div class="gallery-item gallery-item-sizer"></div>
		<?php } ?>
		<?php
		$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
		$full_image_url = $full_image_url[0];
		?>
		<div class="gallery-item box-hover-effect">
		<div class="effect-wrapper">
			<div class="thumb">
			<img src="<?php echo $full_image_url;?>" alt="<?php echo get_the_title(); ?>">
			</div>
			<div class="overlay-shade"></div>
			<div class="icons-holder icons-holder-middle">
			<div class="icons-holder-inner">
				<div class="styled-icons icon-sm icon-dark">
				<a href="<?php echo $full_image_url ?>" data-rel="prettyPhoto[portfolio-details-gallery-prettyphoto]" title="<?php echo get_the_title(); ?>"><i class="fa fa-picture-o"></i></a>
				</div>
			</div>
			</div>
		</div>
		</div>
		<?php $gallery_images = rwmb_meta( 'medicale_mascot_' . "portfolio_metabox_portfolio_gallery_images", 'size=full' ); ?>
		<?php
		if ( !empty( $gallery_images ) ) {
			foreach ( $gallery_images as $each_gallery_image ) {
			?>
			<div class="gallery-item box-hover-effect">
			<div class="effect-wrapper">
				<div class="thumb">
				<img src="<?php echo $each_gallery_image['url'];?>" alt="<?php echo get_the_title(); ?>">
				</div>
				<div class="overlay-shade"></div>
				<div class="icons-holder icons-holder-middle">
				<div class="icons-holder-inner">
					<div class="styled-icons icon-sm icon-dark">
					<a href="<?php echo $each_gallery_image['url'] ?>" data-rel="prettyPhoto[portfolio-details-gallery-prettyphoto]" title="<?php echo get_the_title(); ?>"><i class="fa fa-picture-o"></i></a>
					</div>
				</div>
				</div>
			</div>
			</div>
			<?php
			}
		}
		?>
	</div>
	</div>
	<div class="portfolio-details">
	<?php medicale_mascot_get_portfolio_single_details( 'columns' ); ?>
	</div>
</div>