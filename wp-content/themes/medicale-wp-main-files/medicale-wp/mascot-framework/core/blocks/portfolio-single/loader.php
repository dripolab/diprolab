<?php
require_once MASCOT_FRAMEWORK_DIR . '/core/blocks/portfolio-single/portfolio-single-metabox.php';
require_once MASCOT_FRAMEWORK_DIR . '/core/blocks/portfolio-single/portfolio-single-css-generators.php';
require_once MASCOT_FRAMEWORK_DIR . '/core/blocks/portfolio-single/portfolio-single-functions.php';