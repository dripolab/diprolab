<?php
use MASCOTCORE\CPT\Portfolio\CPT_Portfolio;

add_filter( 'rwmb_meta_boxes', 'medicale_mascot_portfolio_metaboxes' );

function medicale_mascot_portfolio_metaboxes( $meta_boxes ) {
	if( !class_exists('MASCOTCORE\CPT\Portfolio\CPT_Portfolio') ) {
		return $meta_boxes;
	}

	//Post Type: Portfolio
	$portfolio_cpt_class = CPT_Portfolio::Instance();

	// Meta Box Settings for this Page
	$meta_boxes[] = array(
		'title'		=> esc_html__( 'Portfolio Settings', 'medicale-wp' ),
		'post_types' => $portfolio_cpt_class->ptKey,
		'priority'   => 'high',

		// List of tabs, in one of the following formats:
		// 1) key => label
		// 2) key => array( 'label' => Tab label, 'icon' => Tab icon )
		'tabs'		=> array(
			'gallery_images' => array(
				'label' => esc_html__( 'Gallery Images', 'medicale-wp' ),
				'icon'  => 'dashicons-screenoptions', // Dashicon
			),
			'layout' => array(
				'label' => esc_html__( 'Layout', 'medicale-wp' ),
				'icon'  => 'dashicons-screenoptions', // Dashicon
			),
			'checklist' => array(
				'label' => esc_html__( 'Checklist', 'medicale-wp' ),
				'icon'  => 'dashicons-screenoptions', // Dashicon
			),
			'project_link' => array(
				'label' => esc_html__( 'Project Link', 'medicale-wp' ),
				'icon'  => 'dashicons-screenoptions', // Dashicon
			),
			'other' => array(
				'label' => esc_html__( 'Other Settings', 'medicale-wp' ),
				'icon'  => 'dashicons-screenoptions', // Dashicon
			),
		),

		// Tab style: 'default', 'box' or 'left'. Optional
		'tab_style' => 'left',
		
		// Show meta box wrapper around tabs? true (default) or false. Optional
		'tab_wrapper' => true,

		'fields'	=> array(

			//gallery_images tab starts
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Portfolio Gallery Images', 'medicale-wp' ),
				'desc' => esc_html__( 'Changes of the following settings will be effective only for this page.', 'medicale-wp' ),
				'tab'  => 'gallery_images',
			),
			array(
				'id'   => 'medicale_mascot_' . 'portfolio_metabox_portfolio_gallery_images',
				'name' => esc_html__( 'Gallery Images', 'medicale-wp' ),
				'desc' => esc_html__( 'Choose your portfolio images.', 'medicale-wp' ),
				'type' => 'image_advanced',
				'tab'  => 'gallery_images',
			),
			//gallery_images tab ends

			//layout tab starts
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Portfolio Layout Type', 'medicale-wp' ),
				'desc' => esc_html__( 'Changes of the following settings will be effective only for this page.', 'medicale-wp' ),
				'tab'  => 'layout',
			),
			array(
				'name'		=> esc_html__( 'Fullwidth?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'portfolio_metabox_fullwidth',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Make the page fullwidth or not..', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'		=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'		=> esc_html__( 'No', 'medicale-wp' ),
				),
				'tab'		=> 'layout',
			),
			array(
				'name'	=> esc_html__( 'Portfolio Details Type', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'portfolio_metabox_portfolio_details_type',
				'type'	=> 'image_select',
				'options' => array(
					'inherit'				=> MASCOT_ADMIN_ASSETS_URI . '/images/portfolio-single/type/inherit.png',
					'small-image'			=> MASCOT_ADMIN_ASSETS_URI . '/images/portfolio-single/type/small-image.png',
					'small-image-slider'	=> MASCOT_ADMIN_ASSETS_URI . '/images/portfolio-single/type/small-image-slider.png',
					'big-image'			=> MASCOT_ADMIN_ASSETS_URI . '/images/portfolio-single/type/big-image.png',
					'big-image-slider'		=> MASCOT_ADMIN_ASSETS_URI . '/images/portfolio-single/type/big-image-slider.png',
					'small-image-gallery'   => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio-single/type/small-image-gallery.png',
					'big-image-gallery'	 => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio-single/type/big-image-gallery.png',
				),
				'std'	 => 'inherit',
				'tab'	 => 'layout',
			),
			//layout tab ends

			//checklist tab starts
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Checklist Custom Fields', 'medicale-wp' ),
				'desc' => esc_html__( 'Changes of the following settings will be effective only for this page.', 'medicale-wp' ),
				'tab'  => 'checklist',
			),
			array(
				'id'	 => 'medicale_mascot_' . 'portfolio_metabox_checklist',
				// Group field
				'type'   => 'group',
				// Clone whole group?
				'clone'  => true,
				// Drag and drop clones to reorder them?
				'sort_clone' => true,
				// Sub-fields
				'fields' => array(
					array(
						'name' => esc_html__( 'Checklist Title', 'medicale-wp' ),
						'id'   => 'medicale_mascot_' . 'portfolio_metabox_checklist_title',
						'type' => 'text',
						'desc' => esc_html__( 'Title to describe the checklist items. Example: Skills.', 'medicale-wp' ),
					),
					array(
						'name' => esc_html__( 'Checklist Details', 'medicale-wp' ),
						'id'   => 'medicale_mascot_' . 'portfolio_metabox_checklist_details',
						'type' => 'textarea',
						'desc' => esc_html__( 'Details of the checklist. Example: HTML, CSS & WordPress.', 'medicale-wp' ),
					),
				),
				'tab'  => 'checklist',
			),
			//checklist tab ends

			//project_link tab starts
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Project Link', 'medicale-wp' ),
				'desc' => esc_html__( 'Changes of the following settings will be effective only for this page.', 'medicale-wp' ),
				'tab'  => 'project_link',
			),
			array(
				'name'		=> esc_html__( 'Project Link Title', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'portfolio_metabox_project_link_title',
				'desc'		=> esc_html__( 'The custom project text that will link.', 'medicale-wp' ),
				'type'		=> 'text',
				'tab'		=> 'project_link',
			),
			array(
				'name'		=> esc_html__( 'Project URL', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'portfolio_metabox_project_link_url',
				'desc'		=> esc_html__( 'The URL the project text links to.', 'medicale-wp' ),
				'type'		=> 'text',
				'tab'		=> 'project_link',
			),
			array(
				'name'		=> esc_html__( 'Project Link Target', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'portfolio_metabox_project_link_target',
				'desc'		=> esc_html__( 'Open in new window.', 'medicale-wp' ),
				'type'		=> 'checkbox',
				'tab'		=> 'project_link',
			),
			//project_link tab ends

			//other tab starts
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Portfolio Meta', 'medicale-wp' ),
				'desc' => esc_html__( 'Changes of the following settings will be effective only for this page.', 'medicale-wp' ),
				'tab'  => 'other',
			),
			array(
				'name'		=> esc_html__( 'Portfolio Meta', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'portfolio_metabox_portfolio_meta',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Enable/Disabling this option will show/hide each Portfolio Meta on your Portfolio Details Page', 'medicale-wp' ),
				'options'   => array(
					'inherit'								=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'show-post-by-author'					=> esc_html__( 'Show By Author', 'medicale-wp' ),
					'show-post-date'						=> esc_html__( 'Show Date', 'medicale-wp' ),
					'show-post-category'					=> esc_html__( 'Show Category', 'medicale-wp' ),
					'show-post-tag'						=> esc_html__( 'Show Tag', 'medicale-wp' ),
					'show-post-checklist-custom-fields'	 => esc_html__( 'Show Checklist Custom Fields', 'medicale-wp' ),
				),
				'multiple'  => true,
				'std'		=> array( 
					'inherit'
				),
				'tab'		=> 'other',
			),
			array(
				'name'		=> esc_html__( 'Show Share?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'portfolio_metabox_show_share',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Enable/Disabling this option will show/hide share options on your page.', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'		=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'		=> esc_html__( 'No', 'medicale-wp' ),
				),
				'tab'		=> 'other',
			),
			array(
				'name'		=> esc_html__( 'Show Next/Previous Single Post Navigation Link', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'portfolio_metabox_show_next_pre_post_link',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Enable/Disabling this option will show/hide link for Next & Previous Posts.', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'		=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'		=> esc_html__( 'No', 'medicale-wp' ),
				),
				'tab'		=> 'other',
			),
			array(
				'name'		=> esc_html__( 'Show Related Portfolio Items', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'portfolio_metabox_show_related_posts',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Enable/Disabling this option will show/hide Related Posts List/Carousel on your page.', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'		=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'		=> esc_html__( 'No', 'medicale-wp' ),
				),
				'tab'		=> 'other',
			),
			array(
				'name'		=> esc_html__( 'Show Comments', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'portfolio_metabox_show_comments',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Enable/Disabling this option will show/hide Comments on your page.', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'		=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'		=> esc_html__( 'No', 'medicale-wp' ),
				),
				'tab'		=> 'other',
			),
			//other tab ends
		),
	);

	return $meta_boxes;
}