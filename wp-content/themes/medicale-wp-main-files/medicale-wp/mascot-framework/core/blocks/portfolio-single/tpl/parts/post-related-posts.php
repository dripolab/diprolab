
		<div class="related-posts">
			<h4 class="title"><?php esc_html_e('Related Projects', 'medicale-wp' ); ?></h4>
			<?php if ( $related_posts_query_result->have_posts() ): ?>
			<div class="row">
				<?php
					// Start the Loop.
					while ( $related_posts_query_result->have_posts() ): $related_posts_query_result->the_post(); 
					?>
					<div class="col-md-4">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="entry-header">
								<a href="<?php esc_url( get_permalink() );?>"><?php medicale_mascot_get_post_thumbnail( get_post_format() ); ?></a>
							</div>
							<div class="entry-content">
								<?php the_title( '<h5 class="entry-title"><a href="' . esc_url( get_permalink() ) . '">', '</a></h5>' ); ?>
								<div class="entry-meta"><?php medicale_mascot_posted_on(); ?></div>
								<div class="post-excerpt">
									<?php medicale_mascot_related_posts_get_excerpt(); ?>
								</div>
								<div class="clearfix"></div>
							</div>
						</article>
					</div>
					<?php
					endwhile;
				?>
			</div>
			<?php endif; wp_reset_postdata();?>
		</div>