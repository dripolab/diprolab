<div class="row">
	<div class="col-sm-4 sidebar-area sidebar-left">
	<?php get_sidebar( 'left' ); ?>
	</div>
	<div class="col-sm-8 main-content-area">
		<div id="post-<?php the_ID(); ?>" class="portfolio-single <?php echo $portfolio_details_type;?>">
			<?php do_action( 'medicale_mascot_portfolio_single_main_content_area_start' ); ?>
			<?php
			medicale_mascot_get_portfolio_single_all();
			?>
			<?php do_action( 'medicale_mascot_portfolio_single_main_content_area_end' ); ?>
		</div>
	</div>
</div>
