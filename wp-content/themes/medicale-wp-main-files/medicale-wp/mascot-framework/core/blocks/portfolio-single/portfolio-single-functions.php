<?php


if(!function_exists('medicale_mascot_get_portfolio_single')) {
	/**
	 * Function that Renders Portfolio Single HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_portfolio_single( $container_type = 'container' ) {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();

		$params['container_type'] = $container_type;

		//Page Fullwidth
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "portfolio_metabox_fullwidth", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['container_type'] = $temp_meta_value;
		} else {
			$params['container_type'] = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-fullwidth' );
		}

		if( $params['container_type'] ) {
			$params['container_type'] = 'container-fluid';
		} else {
			$params['container_type'] = 'container';
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'portfolio-single-parts', null, 'portfolio-single/tpl', $params );
		
		return $html;
	}
}

if (!function_exists('medicale_mascot_get_portfolio_single_sidebar_layout')) {
	/**
	 * Return Portfolio Single Sidebar Layout HTML
	 */
	function medicale_mascot_get_portfolio_single_sidebar_layout() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();
		
		//Portfolio Details Type
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "portfolio_metabox_portfolio_details_type", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['portfolio_details_type'] = $temp_meta_value;
		} else {
			$params['portfolio_details_type'] = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-select-portfolio-details-type' );
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'portfolio-single', medicale_mascot_get_redux_option( 'portfolio-single-page-settings-sidebar-layout' ), 'portfolio-single/tpl/sidebar-columns', $params );
		
		return $html;
	}
}

if (!function_exists('medicale_mascot_get_portfolio_single_all')) {
	/**
	 * Return Portfolio Single All
	 */
	function medicale_mascot_get_portfolio_single_all() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();

		medicale_mascot_get_portfolio_single_type();

		//Show Next/Previous Single Post Navigation Link
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "portfolio_metabox_show_next_pre_post_link", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['show_next_pre_post_link'] = $temp_meta_value;
		} else {
			$params['show_next_pre_post_link'] = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-show-next-pre-post-link' );
		}

		if( $params['show_next_pre_post_link'] ) {
			medicale_mascot_get_portfolio_single_next_pre_post_link();
		}




		//Show Related Portfolio Items
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "portfolio_metabox_show_related_posts", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['show_related_posts'] = $temp_meta_value;
		} else {
			$params['show_related_posts'] = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-show-related-posts' );
		}

		if( $params['show_related_posts'] ) {
			$posts_count = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-show-related-posts-count' );
			if( empty( $posts_count ) ) {
				$posts_count = 3;
			}
			medicale_mascot_get_portfolio_single_related_posts( $current_page_id, $posts_count );
		}

		//Show Comments
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "portfolio_metabox_show_comments", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['show_comments'] = $temp_meta_value;
		} else {
			$params['show_comments'] = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-show-comments' );
		}
		if( $params['show_comments'] ) {
			medicale_mascot_show_comments();
		}
	}
}

if (!function_exists('medicale_mascot_get_portfolio_single_type')) {
	/**
	 * Return Portfolio Single Type HTML
	 */
	function medicale_mascot_get_portfolio_single_type() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();

		//Portfolio Details Type
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "portfolio_metabox_portfolio_details_type", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['portfolio_details_type'] = $temp_meta_value;
		} else {
			$params['portfolio_details_type'] = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-select-portfolio-details-type' );
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'portfolio-details', $params['portfolio_details_type'], 'portfolio-single/tpl/type', $params );
		
		return $html;
	}
}

if(!function_exists('medicale_mascot_get_portfolio_single_post_thumbnail')) {
	/**
	 * Function that Renders Post Thumbnail HTML Codes
	 * @return HTML
	 */
	function medicale_mascot_get_portfolio_single_post_thumbnail() {
		$params = array();

		if ( !has_post_thumbnail() ) {
			return;
		}
		
		if( !medicale_mascot_get_redux_option( 'portfolio-single-page-settings-show-post-featured-image' ) ) {
			return;
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'post-thumb', null, 'portfolio-single/tpl/parts', $params );
		return $html;
	}
}

if(!function_exists('medicale_mascot_get_portfolio_single_details')) {
	/**
	 * Function that Renders Portfolio Details Codes
	 * @return HTML
	 */
	function medicale_mascot_get_portfolio_single_details( $columns = null ) {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();

		//Show Share
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "portfolio_metabox_show_share", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value != "inherit" ) {
			$params['show_share'] = $temp_meta_value;
		} else {
			$params['show_share'] = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-show-share' );
		}
		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'portfolio-details', $columns, 'portfolio-single/tpl/parts', $params );
		return $html;
	}
}


if ( ! function_exists( 'medicale_mascot_portfolio_single_post_meta' ) ) {
	/**
	 * Print HTML with meta information for the current post-date/time and author.
	 *
	 */
	function medicale_mascot_portfolio_single_post_meta() {
		$current_page_id = medicale_mascot_get_page_id();


		//Show Share
		//check if meta value is provided for this page or then get it from theme options
		$temp_meta_value = rwmb_meta( 'medicale_mascot_' . "portfolio_metabox_portfolio_meta", '', $current_page_id );
		if( ! medicale_mascot_metabox_opt_val_is_empty( $temp_meta_value ) && $temp_meta_value[0] != "inherit" ) {
			$params['portfolio_meta'] = array();
			foreach ( $temp_meta_value as $key => $value ) {
				$params['portfolio_meta'][ $value ] = 1;
			}
		} else {
			$params['portfolio_meta'] = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-portfolio-meta' );
		}

	?>
	<div class="portfolio-meta">
	<?php
		if( isset( $params['portfolio_meta']['show-post-by-author']  ) && $params['portfolio_meta']['show-post-by-author'] ) {
	?>
		<div class="each-meta">
			<h6 class="title">Author</h6>
			<p class="info"><?php echo get_the_author();?></p>
		</div>
	<?php
		} if( isset( $params['portfolio_meta']['show-post-date']  ) && $params['portfolio_meta']['show-post-date'] ) {
	?>
		<div class="each-meta">
			<h6 class="title">Date</h6>
			<p class="info"><?php medicale_mascot_posted_on();?></p>
		</div>
	<?php
		} if( isset( $params['portfolio_meta']['show-post-category']  ) && $params['portfolio_meta']['show-post-category'] ) {
	?>
		<div class="each-meta">
			<h6 class="title">Category</h6>
			<p class="info"><?php echo medicale_mascot_get_portfolio_category_taxonomy_terms();?></p>
		</div>
	<?php
		} if( isset( $params['portfolio_meta']['show-post-tag']  ) && $params['portfolio_meta']['show-post-tag'] ) {
	?>
		<div class="each-meta">
			<h6 class="title">Tags</h6>
			<p class="info"><?php echo medicale_mascot_get_portfolio_tag_taxonomy_terms();?></p>
		</div>
	<?php
		} if( isset( $params['portfolio_meta']['show-post-checklist-custom-fields']  ) && $params['portfolio_meta']['show-post-checklist-custom-fields'] ) {

		$checklist = rwmb_meta( 'medicale_mascot_' . "portfolio_metabox_checklist", '', $current_page_id );
		foreach ( $checklist as $each_checklist ) :
	?>
		<div class="each-meta">
			<h6 class="title"><?php echo $each_checklist[ 'medicale_mascot_' . "portfolio_metabox_checklist_title" ]; ?></h6>
			<p class="info"><?php echo $each_checklist[ 'medicale_mascot_' . "portfolio_metabox_checklist_details" ]; ?></p>
		</div>
	<?php
		endforeach;
		}
	?>
	</div>
	<?php
	}
}

if ( ! function_exists( 'medicale_mascot_get_portfolio_single_next_pre_post_link' ) ) {
	/**
	 * Return Portfolio Single Next Previous Post Link
	 *
	 */
	function medicale_mascot_get_portfolio_single_next_pre_post_link() {
		$params = array();

		$params['next_pre_link_within_same_cat'] = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-show-next-pre-post-link-within-same-cat' );

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'post-next-pre-post-link', null, 'portfolio-single/tpl/parts', $params );
		
		return $html;
	}
}

if ( ! function_exists( 'medicale_mascot_get_portfolio_single_related_posts' ) ) {
	/**
	 * Return Portfolio Single Related Posts
	 *
	 */
	function medicale_mascot_get_portfolio_single_related_posts( $post_id, $related_count, $args = array() ) {

		$args = wp_parse_args( (array) $args, array(
			'orderby' => 'rand',
			'return'  => 'query', // Valid values are: 'query' (WP_Query object), 'array' (the arguments array)
		) );

		$related_args = array(
			'post_type'		=> get_post_type( $post_id ),
			'posts_per_page' => $related_count,
			'post_status'	=> 'publish',
			'post__not_in'   => array( $post_id ),
			'orderby'		=> $args['orderby'],
			'tax_query'		=> array()
		);

		$post		= get_post( $post_id );
		$taxonomies = get_object_taxonomies( $post, 'names' );

		foreach ( $taxonomies as $taxonomy ) {
			$terms = get_the_terms( $post_id, $taxonomy );
			if ( empty( $terms ) ) {
				continue;
			}
			$term_list					= wp_list_pluck( $terms, 'slug' );
			$related_args['tax_query'][] = array(
				'taxonomy' => $taxonomy,
				'field'	=> 'slug',
				'terms'	=> $term_list
			);
		}

		if ( count( $related_args['tax_query'] ) > 1 ) {
			$related_args['tax_query']['relation'] = 'OR';
		}

		if ( $args['return'] == 'query' ) {
			$params['related_posts_query_result'] = new WP_Query( $related_args );
		} else {
			$params['related_posts_query_result'] = $related_args;
		}

		//related posts
		$params['related_posts_carousel'] = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-show-related-posts-carousel' );

		$posts_carousel = '';
		if( $params['related_posts_carousel'] ) {
			$posts_carousel = 'carousel';
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'post-related-posts', $posts_carousel, 'portfolio-single/tpl/parts', $params );
		
		return $html;
	}
}


if ( ! function_exists( 'medicale_mascot_get_portfolio_single_launch_project_link' ) ) {
	/**
	 * Return Portfolio Single Launch Project Link
	 *
	 */
	function medicale_mascot_get_portfolio_single_launch_project_link() {
		$current_page_id = medicale_mascot_get_page_id();
		$params = array();

		$params['project_link_title'] = rwmb_meta( 'medicale_mascot_' . "portfolio_metabox_project_link_title", '', $current_page_id );

		if( trim($params['project_link_title']) == "" ) {
			return;
		}

		$params['project_link_url'] = rwmb_meta( 'medicale_mascot_' . "portfolio_metabox_project_link_url", '', $current_page_id );
		$params['project_link_target'] = rwmb_meta( 'medicale_mascot_' . "portfolio_metabox_project_link_target", '', $current_page_id );

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_blocks_template_part( 'launch-project-link', null, 'portfolio-single/tpl/parts', $params );
		
		return $html;
	}
}