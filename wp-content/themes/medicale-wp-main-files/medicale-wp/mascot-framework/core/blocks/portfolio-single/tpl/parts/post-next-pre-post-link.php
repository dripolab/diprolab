<div class="row pre-next-post-link">
	<div class="col-xs-4 p-0">
		<div class="pre-post-link">
		<?php
			$pre_title = '<h5 class="title">Previous Project</h5>';
			if( $next_pre_link_within_same_cat ) {
				if( !empty( get_previous_post( true ) ) ){
					previous_post_link('%link',"<i class='fa fa-long-arrow-left'></i>", true );
					echo '<div class="content">';
					echo $pre_title;
					previous_post_link('%link', '%title', true );
					echo "</div>";
				}
			} else {
				if( !empty( get_previous_post() ) ){
					previous_post_link('%link',"<i class='fa fa-long-arrow-left'></i>" );
					echo '<div class="content">';
					echo $pre_title;
					previous_post_link('%link', '%title' );
					echo "</div>";
				}
			}
		?>
		</div>
	</div>
	<div class="col-xs-4 p-0 text-center">
		<a href="<?php echo get_post_type_archive_link( 'portfolio' ); ?>" class="back-to-portfolio"><i class="fa fa-th-large"></i></a>
	</div>
	<div class="col-xs-4 p-0 text-right">
		<div class="next-post-link">
		<?php
			$pre_title = '<h5 class="title">Next Project</h5>';
			if( $next_pre_link_within_same_cat ) {
				if( !empty( get_next_post( true ) ) ){
					echo '<div class="content">';
					echo $pre_title;
					next_post_link('%link', '%title', true );
					echo "</div>";
					next_post_link('%link',"<i class='fa fa-long-arrow-right'></i>", true );
				}
			} else {
				if( !empty( get_next_post() ) ){
					echo '<div class="content">';
					echo $pre_title;
					next_post_link('%link', '%title' );
					echo "</div>";
					next_post_link('%link',"<i class='fa fa-long-arrow-right'></i>" );
				}
			}
		?>
		</div>
	</div>
</div>