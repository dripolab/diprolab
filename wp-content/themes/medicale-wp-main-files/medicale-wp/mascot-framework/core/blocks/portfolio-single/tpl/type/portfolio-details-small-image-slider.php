<?php
$description_alignment = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-portfolio-type-small-image-slider-description-alignment' );
$description_width = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-portfolio-type-small-image-slider-description-width' );
$description_sticky = medicale_mascot_get_redux_option( 'portfolio-single-page-settings-portfolio-type-small-image-slider-description-sticky' );
?>
<div class="portfolio-container <?php if( $description_sticky ) echo "portfolio-sticky-side-text"; ?>">
	<div class="row">
	<div class="col-sm-<?php echo $description_width; ?> <?php if( $description_alignment == 'right' ) echo "md-pull-right"; ?> portfolio-details-parent">
		<div class="portfolio-details">
		<?php medicale_mascot_get_portfolio_single_details(); ?>
		</div>
	</div>
	<div class="col-sm-<?php echo 12-$description_width; ?>">
		<div class="portfolio-images">
		<div class="owl-carousel owl-theme owl-carousel-1col" data-nav="true" data-dots="true">
		<?php
			$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
			$full_image_url = $full_image_url[0];
		?>
			<img src="<?php echo $full_image_url;?>" alt="<?php echo get_the_title(); ?>">
		<?php

			$gallery_images = rwmb_meta( 'medicale_mascot_' . "portfolio_metabox_portfolio_gallery_images", 'size=full' );
		?>
		<?php
		if ( !empty( $gallery_images ) ) {
			foreach ( $gallery_images as $each_gallery_image ) {
			?>
			<img src="<?php echo $each_gallery_image['url'];?>" alt="<?php echo get_the_title(); ?>">
			<?php
			}
		}
		?>
		</div>
		</div>
	</div>
	</div>
</div>