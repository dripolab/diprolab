<div class="portfolio-container">
	<div class="portfolio-images">
		<?php
			$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
			$full_image_url = $full_image_url[0];
		?>
		<img src="<?php echo $full_image_url;?>" alt="<?php echo get_the_title(); ?>">
		<?php $gallery_images = rwmb_meta( 'medicale_mascot_' . "portfolio_metabox_portfolio_gallery_images", 'size=full' ); ?>
		<?php
			if ( !empty( $gallery_images ) ) {
				foreach ( $gallery_images as $each_gallery_image ) {
				?>
				<img src="<?php echo $each_gallery_image['url'];?>" alt="<?php echo get_the_title(); ?>">
				<?php
				}
			}
		?>
	</div>
	<div class="portfolio-details">
		<?php medicale_mascot_get_portfolio_single_details( 'columns' ); ?>
	</div>
</div>