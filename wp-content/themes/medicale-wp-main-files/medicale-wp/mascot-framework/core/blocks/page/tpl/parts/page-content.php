<div id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<div class="page-content">
			<?php
				/**
				* medicale_mascot_before_page_content hook.
				*
				*/
				do_action( 'medicale_mascot_before_page_content' );
			?>
			<?php
				the_content();
			?>
			<?php
				/**
				* medicale_mascot_after_page_content hook.
				*
				*/
				do_action( 'medicale_mascot_after_page_content' );
			?>
		</div>
	</div>
</div>
<?php
	if( $page_show_comments ) {
	medicale_mascot_show_comments();
	}
?>
