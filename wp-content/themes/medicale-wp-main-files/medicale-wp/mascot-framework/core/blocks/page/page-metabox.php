<?php
add_filter( 'rwmb_meta_boxes', 'medicale_mascot_page_metaboxes' );

/**
 * Register meta boxes
 *
 * @param array $meta_boxes
 *
 * @return array
 */
function medicale_mascot_page_metaboxes( $meta_boxes ) {
	//list active sidebars
	$active_sidebar_list = array();
	$active_sidebar_list[ 'inherit' ] = esc_html__( 'Inherit from Theme Options', 'medicale-wp' );
	/** @global array $wp_registered_sidebars */
	global $wp_registered_sidebars;
	foreach ( $wp_registered_sidebars as $key => $value ) {
		$active_sidebar_list[ $key ] = $value['name'];
	}

	//list menus
	$active_menu_list = array();
	$nav_menus = wp_get_nav_menus();
	if ( ! empty ( $nav_menus ) ) {
		foreach ( $nav_menus as $item ) {
			$active_menu_list[ $item->term_id ] = $item->name;
		}
		//foreach
	}

	//get primary thme location menu item
	$theme_locations = get_nav_menu_locations();
	$primary_nav_menu_name = 'none';
	if( array_key_exists('primary', $theme_locations) && !empty($theme_locations['primary']) )  {
		$primary_nav_menu_obj = get_term( $theme_locations['primary'], 'nav_menu' );
		$primary_nav_menu_name = $primary_nav_menu_obj->name;
	}

	//ALL custom post types
	//$post_types = get_post_types();

	//Get a List of All Revolution Slider Aliases
	$list_rev_sliders = array();
	if ( class_exists( 'RevSlider' ) ) {
		$list_rev_sliders[0] = esc_html__( 'Select a Slider', 'medicale-wp' );
		$rev_slider = new RevSlider();
		$all_rev_sliders = $rev_slider->getAllSliderAliases();
		foreach ( $all_rev_sliders as $key => $value ) {
			$list_rev_sliders[$value] = $value;
		}
	}


	//Get a List of All Layer Slider Aliases
	$list_layer_sliders = array();
	if ( class_exists( 'LS_Sliders' ) ) {
		$list_layer_sliders[0] = esc_html__( 'Select a Slider', 'medicale-wp' );
		$LS_Sliders_list = LS_Sliders::find();
		foreach ( $LS_Sliders_list as $each_slide ) {
			$list_layer_sliders[ $each_slide['id'] ] = $each_slide['name'];
		}
	}


	// Background Patterns Reader
	$sample_patterns_path = MASCOT_ADMIN_ASSETS_DIR . '/images/pattern/';
	$sample_patterns_url  = MASCOT_ADMIN_ASSETS_URI . '/images/pattern/';
	$sample_patterns      = array();
	
	if ( is_dir( $sample_patterns_path ) ) {

		if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
			$sample_patterns = array();

			while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

				if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
					$name              = explode( '.', $sample_patterns_file );
					$name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
					$sample_patterns[] = $sample_patterns_url . $sample_patterns_file;
				}
			}
		}
	}



	$text_align_array = array(
		'inherit'			=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
		'text-left flip'	=> esc_html__( 'Left', 'medicale-wp' ),
		'text-center'		=> esc_html__( 'Center', 'medicale-wp' ),
		'text-right flip'	=> esc_html__( 'Right', 'medicale-wp' ),
	);

	// Page Sidebar
	$meta_boxes[] = array(
		'id'		=> 'page_sidebar',
		'title'		=> esc_html__( 'Page Sidebar', 'medicale-wp' ),
		'post_types' => array( 'post', 'page', 'portfolio' ),
		'context'	=> 'side',
		'priority'   => 'low',
		'fields'	 => array(
			array(
				'name'	=> esc_html__( 'Sidebar Layout', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_sidebar_layout',
				'type'	=> 'image_select',
				'options' => array(
					'inherit'				=> MASCOT_ADMIN_ASSETS_URI . '/images/sidebar/inherit.png',
					'sidebar-right-25'		=> MASCOT_ADMIN_ASSETS_URI . '/images/sidebar/sidebar-right-25.png',
					'sidebar-right-33'		=> MASCOT_ADMIN_ASSETS_URI . '/images/sidebar/sidebar-right-33.png',
					'no-sidebar'			=> MASCOT_ADMIN_ASSETS_URI . '/images/sidebar/no-sidebar.png',
					'sidebar-left-25'		=> MASCOT_ADMIN_ASSETS_URI . '/images/sidebar/sidebar-left-25.png',
					'sidebar-left-33'		=> MASCOT_ADMIN_ASSETS_URI . '/images/sidebar/sidebar-left-33.png',
					'both-sidebar-25-50-25' => MASCOT_ADMIN_ASSETS_URI . '/images/sidebar/both-sidebar-25-50-25.png',
				),
				'std'	 => 'inherit',
			),
			array(
				'name'	=> esc_html__( 'Pick Sidebar Default', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_sidebar_default',
				'type'	=> 'select',
				'options' => $active_sidebar_list,
			),
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Sidebar 2 Settings', 'medicale-wp' ),
				'desc'		=> esc_html__( 'Sidebar 2 will only be used if "Sidebar Both Side" is selected.', 'medicale-wp' ),
			),
			array(
				'name'		=> esc_html__( 'Pick Sidebar 2', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_sidebar_two',
				'type'		=> 'select',
				'options'   => $active_sidebar_list,
			),
			array(
				'name'	=> esc_html__( 'Sidebar 2 Position', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_sidebar_two_position',
				'type'	=> 'select',
				'desc' => esc_html__( 'Controls the position of sidebar 2. In that case, sidebar 1 will be shown on opposite side.', 'medicale-wp' ),
				'options' => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'left'		=> esc_html__( 'Left', 'medicale-wp' ),
					'right'	 	=> esc_html__( 'Right', 'medicale-wp' )
				),
			),
		)
	);


	// Meta Box Settings for this Page
	$meta_boxes[] = array(
		'title'	 => esc_html__( 'Page Settings', 'medicale-wp' ),
		'post_types' => array( 'post', 'page', 'portfolio' ),
		'priority'   => 'high',

		// List of tabs, in one of the following formats:
		// 1) key => label
		// 2) key => array( 'label' => Tab label, 'icon' => Tab icon )
		'tabs'		=> array(
			'slider' => array(
				'label' => esc_html__( 'Slider Settings', 'medicale-wp' ),
				'icon'  => 'dashicons-update', // Dashicon
			),
			'general' => array(
				'label' => esc_html__( 'General Settings', 'medicale-wp' ),
				'icon'  => 'dashicons-admin-home', // Dashicon
			),


			'header'  => array(
				'label' => esc_html__( 'Header', 'medicale-wp' ),
				'icon'  => 'dashicons-arrow-up-alt', // Dashicon
			),
			'header-layout'  => array(
				'label' => esc_html__( 'Header Layout', 'medicale-wp' ),
				'icon'  => 'dashicons-arrow-up-alt', // Dashicon
			),
			'header-top'  => array(
				'label' => esc_html__( 'Header Top Row', 'medicale-wp' ),
				'icon'  => 'dashicons-arrow-up-alt', // Dashicon
			),
			'header-mid'  => array(
				'label' => esc_html__( 'Header Middle Row', 'medicale-wp' ),
				'icon'  => 'dashicons-arrow-up-alt', // Dashicon
			),
			'header-navigation-layout'  => array(
				'label' => esc_html__( 'Header Navigation Row', 'medicale-wp' ),
				'icon'  => 'dashicons-arrow-up-alt', // Dashicon
			),


			'logo' => array(
				'label' => esc_html__( 'Logo', 'medicale-wp' ),
				'icon'  => 'dashicons-palmtree', // Dashicon
			),
			'page-title'		=> array(
				'label' => esc_html__( 'Page Title', 'medicale-wp' ),
				'icon'  => 'dashicons-archive', // Dashicon
			),
			'layout-setings'	=> array(
				'label' => esc_html__( 'Layout Settings', 'medicale-wp' ),
				'icon'  => 'dashicons-editor-table', // Dashicon
			),
			'footer-top-callout'	=> array(
				'label' => esc_html__( 'Footer Call Out', 'medicale-wp' ),
				'icon'  => 'dashicons-editor-insertmore', // Dashicon
			),
			

			'footer'	=> array(
				'label' => esc_html__( 'Footer Settings', 'medicale-wp' ),
				'icon'  => 'dashicons-arrow-down-alt', // Dashicon
			),
			'footer-top'	=> array(
				'label' => esc_html__( 'Footer Top', 'medicale-wp' ),
				'icon'  => 'dashicons-arrow-down-alt', // Dashicon
			),
			'footer-bottom'	=> array(
				'label' => esc_html__( 'Footer Bottom Row', 'medicale-wp' ),
				'icon'  => 'dashicons-arrow-down-alt', // Dashicon
			),
		),

		// Tab style: 'default', 'box' or 'left'. Optional
		'tab_style' => 'left',
		
		// Show meta box wrapper around tabs? true (default) or false. Optional
		'tab_wrapper' => true,

		'fields'	=> array(

			//slider tab starts
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Slider Settings', 'medicale-wp' ),
				'desc' => esc_html__( 'Changes of the following settings will be effective only for this page.', 'medicale-wp' ),
				'tab'  => 'slider',
			),
			array(
				'name'		=> esc_html__( 'Slider Type', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_slider_select_slider_type',
				'type'		=> 'select',
				'desc' => esc_html__( 'Select the type of slider you want to display.', 'medicale-wp' ),
				'options'   => array(
					'no-slider'			=> esc_html__( 'No Slider', 'medicale-wp' ),
					'rev-slider'		=> esc_html__( 'Slider Revolution', 'medicale-wp' ),
					'layer-slider'		=> esc_html__( 'Layer Slider', 'medicale-wp' ),
				),
				'std'		=> 'no-slider',
				'tab'		=> 'slider',
			),
			array(
				'name'		=> esc_html__( 'Choose Revolution Slider', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_slider_select_rev_slider',
				'type'		=> 'select',
				'desc' => esc_html__( 'Select the name(alias) of the revolution slider you want to display.', 'medicale-wp' ),
				'options'   => $list_rev_sliders,
				'visible'   => array( 'medicale_mascot_' . 'page_metabox_slider_select_slider_type', '=', 'rev-slider' ),
				'tab'		=> 'slider',
			),
			array(
				'name'		=> esc_html__( 'Choose Layer Slider', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_slider_select_layer_slider',
				'type'		=> 'select',
				'desc' => esc_html__( 'Select the name(alias) of the revolution slider you want to display.', 'medicale-wp' ),
				'options'   => $list_layer_sliders,
				'visible'   => array( 'medicale_mascot_' . 'page_metabox_slider_select_slider_type', '=', 'layer-slider' ),
				'tab'		=> 'slider',
			),
			array(
				'name'		=> esc_html__( 'Slider Position', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_slider_position',
				'type'		=> 'select',
				'desc' => esc_html__( 'Choose position of the slider you want to display. You can put it below or above the header.', 'medicale-wp' ),
				'options'   => array(
					'default'		=> esc_html__( 'Default', 'medicale-wp' ),
					'below-header'	=> esc_html__( 'Below Header', 'medicale-wp' ),
					'above-header'	=> esc_html__( 'Above Header', 'medicale-wp' ),
				),
				'std'		=> 'default',
				'visible'   => array( 'medicale_mascot_' . 'page_metabox_slider_select_slider_type', '!=', 'no-slider' ),
				'tab'		=> 'slider',
			),
			//slider tab ends






			//general tab starts
			array(
				'type' => 'heading',
				'name' => esc_html__( 'General Settings', 'medicale-wp' ),
				'desc' => esc_html__( 'Changes of the following settings will be effective only for this page.', 'medicale-wp' ),
				'tab'  => 'general',
			),
			array(
				'name'		=> esc_html__( 'Hide Featured Image', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_hide_featured_image',
				'type'		=> 'checkbox',
				'desc'		=> esc_html__( 'Enable/Disabling this option will show/hide Featured Image in blog page.', 'medicale-wp' ),
				'tab'		=> 'general',
			),
			array(
				'name'		=> esc_html__( 'Show Comments', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_general_show_comments',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				'tab'		=> 'general',
			),
			array(
				'name'		=> esc_html__( 'Smooth Scroll', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_general_smooth_scroll',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'yes'		=> esc_html__( 'On', 'medicale-wp' ),
					'no'		=> esc_html__( 'Off', 'medicale-wp' ),
				),
				'tab'		=> 'general',
			),
			array(
				'name'		=> esc_html__( 'Smooth Page Transitions', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_general_smooth_page_transition',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'yes'		=> esc_html__( 'On', 'medicale-wp' ),
					'no'		=> esc_html__( 'Off', 'medicale-wp' ),
				),
				'tab'		=> 'general',
			),
			array(
				'name'		=> esc_html__( 'Enable Page Preloader', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_general_enable_page_preloader',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'yes'		=> esc_html__( 'On', 'medicale-wp' ),
					'no'		=> esc_html__( 'Off', 'medicale-wp' ),
				),
				'tab'		=> 'general',
			),
			//general tab ends



			
			//Header tab starts
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Header', 'medicale-wp' ),
				'desc' => esc_html__( 'Changes of the following settings will be effective only for this page.', 'medicale-wp' ),
				'tab'  => 'header',
			),
			array(
				'name'		=> esc_html__( 'Header Visibility', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_visibility',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Show or hide header only for this page.', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'   		=> esc_html__( 'Show', 'medicale-wp' ),
					'0' 		=> esc_html__( 'Hide', 'medicale-wp' ),
				),
				'tab'		=> 'header',
			),




			// DIVIDER
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Header Layout', 'medicale-wp' ),
				//'visible' => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'  => 'header-layout',
			),
			array(
				'name'		=> esc_html__( 'Header Layout Type', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_layout_type',
				'type'		=> 'select',
				'options'   => array(
					'inherit'						=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'header-1-2col'					=> esc_html__( '2 Rows', 'medicale-wp' ),
					'header-2-3col'					=> esc_html__( '3 Rows', 'medicale-wp' ),
					'header-3-logo-center'			=> esc_html__( '3 Rows Logo Center', 'medicale-wp' ),
					'header-4-logo-menu-center'	 	=> esc_html__( '3 Rows Logo + Menu Center', 'medicale-wp' ),
					'header-5-mobile-nav'			=> esc_html__( 'Mobile Nav', 'medicale-wp' ),
					'header-6-side-panel-nav'		=> esc_html__( 'Side Panel Nav', 'medicale-wp' ),
					'header-7-vertical-nav'			=> esc_html__( 'Vertical Nav', 'medicale-wp' ),
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'		=> 'header-layout',
			),
			array(
				'name'		=> esc_html__( 'Header Container', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_layout_type_container',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   		=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'container' 		=> esc_html__( 'Container', 'medicale-wp' ),
					'container-fluid' 	=> esc_html__( 'Container Fluid', 'medicale-wp' )
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'		=> 'header-layout',
			),
			/*array(
				'name'		=> esc_html__( 'Header Behaviour~', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_layout_behaviour',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   	=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'header-fixed'  => esc_html__( 'Fixed on Scroll', 'medicale-wp' ),
					'header-sticky' => esc_html__( 'Sticky on Scroll', 'medicale-wp' ),
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'		=> 'header',
			),*/




			// DIVIDER
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Header Top Row', 'medicale-wp' ),
				//'visible' => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'  => 'header-top',
			),
			
			//Header Top
			array(
				'name'		=> esc_html__( 'Show Header Top', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_show_header_top',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Enabling this option will show Header Top section.', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'   		=> esc_html__( 'Yes', 'medicale-wp' ),
					'0' 		=> esc_html__( 'No', 'medicale-wp' ),
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Header Top Columns Layout', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_columns_layout',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'25-75'   	=> esc_html__( '1/4 + 3/4', 'medicale-wp' ),
					'33-66' 	=> esc_html__( '1/3 + 2/3', 'medicale-wp' ),
					'50-50'   	=> esc_html__( '1/2 + 1/2', 'medicale-wp' ),
					'66-33'   	=> esc_html__( '2/3 + 1/3', 'medicale-wp' ),
					'75-25'   	=> esc_html__( '3/4 + 1/4', 'medicale-wp' ),
					'100'   	=> esc_html__( '1/1', 'medicale-wp' ),
				),
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_show_header_top', '!=', '0' )
				),*/
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Use Theme Color in Background?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_bgcolor_use_themecolor',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'   		=> esc_html__( 'Yes', 'medicale-wp' ),
					'0' 		=> esc_html__( 'No - Instead use custom bg color', 'medicale-wp' ),
				),
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_show_header_top', '!=', '0' )
				),*/
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Custom Background Color', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_custom_bgcolor',
				'type'		=> 'color',
				'desc'		=> esc_html__( 'Pick a custom background color for Header Top', 'medicale-wp' ),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_bgcolor_use_themecolor', '=', '0' )
				),
				'tab'		=> 'header-top',
			),




			// heading Columns 1
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Column 1 - Header Top Left Widget', 'medicale-wp' ),
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_show_header_top', '!=', '0' )
				),*/
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Column 1 (Left Widget) - Content Types', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column1_content',
				'type'		=> 'select',
				'desc'		=> sprintf( esc_html__( '> You can choose multiple. %3$s> In case of %1$s"Social Links"%2$s, you will find it\'s settings at %1$sTheme Options > Social Links%2$s. %3$s> In case of %1$s"Header Top Navigation"%2$s, please create menu from %1$sAppearance > Menus%2$s and set Theme Location %1$s"Column 1 - Header Top Navigation"%2$s', 'medicale-wp' ), '<strong>', '</strong>', '<br>'),
				'options'   => array(
					'inherit'			=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'contact-info'		=> esc_html__( 'Contact Info', 'medicale-wp' ),
					'custom-text'		=> esc_html__( 'Custom Text', 'medicale-wp' ),
					'custom-button'	 	=> esc_html__( 'Custom Button', 'medicale-wp' ),
					'header-top-nav'	=> esc_html__( 'Header Top Navigation', 'medicale-wp' ),
					'social-links'		=> esc_html__( 'Social Links', 'medicale-wp' ),
					'search-box'		=> esc_html__( 'Search Box', 'medicale-wp' ),
					'login-register'	=> esc_html__( 'Login/Register', 'medicale-wp' ),
					'wpml-languages'	=> esc_html__( 'WPML Languages', 'medicale-wp' ),
					'checkout-button'   => esc_html__( 'Checkout Button', 'medicale-wp' ),
				),
				'multiple'  => true,
				'std'		=> array( 
					'inherit'
				),
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_show_header_top', '!=', '0' )
				),*/
				'tab'		=> 'header-top',
			),



			array(
				'type' => 'divider',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column1_content', 'in', 'contact-info' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'	=> esc_html__( 'Column 1 - Contact Info Text', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column1_contact_info',
				'type'	=> 'text_list',
				'clone'   => false,
				// Options: array of Placeholder => Label for text boxes
				// Number of options are not limited
				'options' => array(
					'phone'			=> sprintf( esc_html__( 'Phone %1$s', 'medicale-wp' ), '<br>'),
					'email'			=> sprintf( esc_html__( 'Email %1$s', 'medicale-wp' ), '<br>'),
					'address'		=> sprintf( esc_html__( 'Address %1$s', 'medicale-wp' ), '<br>'),
					'opening-hours' => esc_html__( 'Opening Hours', 'medicale-wp' ),
				),
				'std'	 => array( 
					esc_html__( 'Questions? Call us at: +123 4567 8901', 'medicale-wp' ),
					esc_html__( 'info@yourdomain.com', 'medicale-wp' ),
					esc_html__( 'Envato HQ 121 King Street, Melbourne', 'medicale-wp' ),
					esc_html__( 'Mon-Fri 09:00-17:00', 'medicale-wp' ),
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column1_content', 'in', 'contact-info' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'	=> esc_html__( 'Column 1 - Contact Info Visibility', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column1_contact_info_checkbox',
				'type'	=> 'checkbox_list',
				'desc'	=> esc_html__( 'Please choose which fields you want to display.', 'medicale-wp' ),
				// Array of 'value' => 'Label' pairs for radio options.
				// Note: the 'value' is stored in meta field, not the 'Label'
				'options' => array(
					'phone'			=> 'Phone',
					'email'			=> 'Email',
					'address'		=> 'Address',
					'opening-hours' => 'Opening Hours',
				),
				'std'	 => array( 
					'phone',
					'email'
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column1_content', 'in', 'contact-info' )
				),
				'tab'		=> 'header-top',
			),


			array(
				'type' => 'divider',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column1_content', 'in', 'custom-text' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Column 1 - Custom Text', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column1_custom_text',
				'type'		=> 'textarea',
				'desc'		=> esc_html__( 'Enter your custom text. Custom HTML tags are allowed (wp_kses).', 'medicale-wp' ),
				'std'		=> sprintf( esc_html__( 'Custom %1$sheader top%2$s text goes here!', 'medicale-wp' ), '<strong>', '</strong>'),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column1_content', 'in', 'custom-text' )
				),
				'tab'		=> 'header-top',
			),


			array(
				'type' => 'divider',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column1_content', 'in', 'custom-button' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'	=> esc_html__( 'Column 1 - Custom Button', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column1_custom_button',
				'type'	=> 'text_list',
				'desc'		=> esc_html__( 'Show a custom button in the header top.', 'medicale-wp' ),
				'clone'   => false,
				// Options: array of Placeholder => Label for text boxes
				// Number of options are not limited
				'options' => array(
					'Button Title'			=> sprintf( esc_html__( 'Button Title %1$s', 'medicale-wp' ), '<br>'),
					'Button Link'			=> sprintf( esc_html__( 'Button Link %1$s', 'medicale-wp' ), '<br>'),
				),
				'std'	 => array( 
					esc_html__( 'Custom Button', 'medicale-wp' ),
					esc_html__( '#', 'medicale-wp' ),
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column1_content', 'in', 'custom-button' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Column 1 - Custom Button Design Style', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column1_custom_button_design_style',
				'type'		=> 'select',
				'options'   => array(
					'btn-default'   => esc_html__( 'Button Defaults', 'medicale-wp' ),
					'btn-border'	=> esc_html__( 'Button Border', 'medicale-wp' ),
					'btn-dark'		=> esc_html__( 'Button Dark', 'medicale-wp' ),
					'btn-gray'		=> esc_html__( 'Button Gray', 'medicale-wp' ),
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column1_content', 'in', 'custom-button' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Column 1 - Custom Button Make Theme Colored?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column1_custom_button_theme_colored',
				'type'		=> 'checkbox',
				'desc'		=> esc_html__( 'To make the button theme colored, please check it.', 'medicale-wp' ),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column1_content', 'in', 'custom-button' )
				),
				'tab'		=> 'header-top',
			),


			array(
				'type' => 'divider',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column1_content', 'in', 'social-links' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Column 1 - Social Links Color', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column1_social_links_color',
				'type'		=> 'select',
				'options'   => array(
					'icon-dark'	 	=> esc_html__( 'Dark', 'medicale-wp' ),
					''				=> esc_html__( 'Default', 'medicale-wp' ),
					'icon-gray'	 	=> esc_html__( 'Gray', 'medicale-wp' ),
				),
				'std'		=> '',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column1_content', 'in', 'social-links' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Column 1 - Social Links Icon Style', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column1_social_links_icon_style',
				'type'		=> 'select',
				'options'   => array(
					'icon-rounded'   	=> esc_html__( 'Rounded', 'medicale-wp' ),
					''					=> esc_html__( 'Default', 'medicale-wp' ),
					'icon-circled'   	=> esc_html__( 'Circled', 'medicale-wp' ),
				),
				'std'		=> '',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column1_content', 'in', 'social-links' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Column 1 - Social Links Make Icon Area Bordered?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column1_social_links_icon_border_style',
				'type'		=> 'checkbox',
				'desc'		=> esc_html__( 'To make the social icons area bordered, please check it.', 'medicale-wp' ),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column1_content', 'in', 'social-links' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Column 1 - Social Links Make Theme Colored?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column1_social_links_icon_theme_colored',
				'type'		=> 'checkbox',
				'desc'		=> esc_html__( 'To make the social links theme colored, please check it.', 'medicale-wp' ),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column1_content', 'in', 'social-links' )
				),
				'tab'		=> 'header-top',
			),


			array(
				'type' => 'divider',
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_show_header_top', '!=', '0' )
				),*/
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Column 1 - Text Alignment', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column1_text_align',
				'type'		=> 'select',
				'options'   => $text_align_array,
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_show_header_top', '!=', '0' )
				),*/
				'tab'		=> 'header-top',
			),




			// heading Columns 2
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Column 2 - Header Top Right Widget', 'medicale-wp' ),
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_show_header_top', '!=', '0' )
				),*/
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Column 2 (Right Widget) - Content Types', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column2_content',
				'type'		=> 'select',
				'desc'		=> sprintf( esc_html__( '> You can choose multiple. %3$s> In case of %1$s"Social Links"%2$s, you will find it\'s settings at %1$sTheme Options > Social Links%2$s. %3$s> In case of %1$s"Header Top Navigation"%2$s, please create menu from %1$sAppearance > Menus%2$s and set Theme Location %1$s"Column 2 - Header Top Navigation"%2$s', 'medicale-wp' ), '<strong>', '</strong>', '<br>'),
				'options'   => array(
					'inherit'			=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'contact-info'		=> esc_html__( 'Contact Info', 'medicale-wp' ),
					'custom-text'		=> esc_html__( 'Custom Text', 'medicale-wp' ),
					'custom-button'	 	=> esc_html__( 'Custom Button', 'medicale-wp' ),
					'header-top-nav'	=> esc_html__( 'Header Top Navigation', 'medicale-wp' ),
					'social-links'		=> esc_html__( 'Social Links', 'medicale-wp' ),
					'search-box'		=> esc_html__( 'Search Box', 'medicale-wp' ),
					'login-register'	=> esc_html__( 'Login/Register', 'medicale-wp' ),
					'wpml-languages'	=> esc_html__( 'WPML Languages', 'medicale-wp' ),
					'checkout-button'   => esc_html__( 'Checkout Button', 'medicale-wp' ),
				),
				'multiple'  => true,
				'std'		=> array( 
					'inherit'
				),
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_show_header_top', '!=', '0' )
				),*/
				'tab'		=> 'header-top',
			),



			array(
				'type' => 'divider',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column2_content', 'in', 'contact-info' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'	=> esc_html__( 'Column 2 - Contact Info Text', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column2_contact_info',
				'type'	=> 'text_list',
				'clone'   => false,
				// Options: array of Placeholder => Label for text boxes
				// Number of options are not limited
				'options' => array(
					'phone'			=> sprintf( esc_html__( 'Phone %1$s', 'medicale-wp' ), '<br>'),
					'email'			=> sprintf( esc_html__( 'Email %1$s', 'medicale-wp' ), '<br>'),
					'address'		=> sprintf( esc_html__( 'Address %1$s', 'medicale-wp' ), '<br>'),
					'opening-hours' => esc_html__( 'Opening Hours', 'medicale-wp' ),
				),
				'std'	 => array( 
					esc_html__( 'Questions? Call us at: +123 4567 8901', 'medicale-wp' ),
					esc_html__( 'info@yourdomain.com', 'medicale-wp' ),
					esc_html__( 'Envato HQ 121 King Street, Melbourne', 'medicale-wp' ),
					esc_html__( 'Mon-Fri 09:00-17:00', 'medicale-wp' ),
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column2_content', 'in', 'contact-info' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'	=> esc_html__( 'Column 2 - Contact Info Visibility', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column2_contact_info_checkbox',
				'type'	=> 'checkbox_list',
				'desc'	=> esc_html__( 'Please choose which fields you want to display.', 'medicale-wp' ),
				// Array of 'value' => 'Label' pairs for radio options.
				// Note: the 'value' is stored in meta field, not the 'Label'
				'options' => array(
					'phone'			=> 'Phone',
					'email'			=> 'Email',
					'address'		=> 'Address',
					'opening-hours' => 'Opening Hours',
				),
				'std'	 => array( 
					'phone',
					'email'
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column2_content', 'in', 'contact-info' )
				),
				'tab'		=> 'header-top',
			),


			array(
				'type' => 'divider',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column2_content', 'in', 'custom-text' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Column 2 - Custom Text', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column2_custom_text',
				'type'		=> 'textarea',
				'desc'		=> esc_html__( 'Enter your custom text. Custom HTML tags are allowed (wp_kses).', 'medicale-wp' ),
				'std'		=> sprintf( esc_html__( 'Custom %1$sheader top%2$s text goes here!', 'medicale-wp' ), '<strong>', '</strong>'),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column2_content', 'in', 'custom-text' )
				),
				'tab'		=> 'header-top',
			),


			array(
				'type' => 'divider',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column2_content', 'in', 'custom-button' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'	=> esc_html__( 'Column 2 - Custom Button', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column2_custom_button',
				'type'	=> 'text_list',
				'desc'		=> esc_html__( 'Show a custom button in the header top.', 'medicale-wp' ),
				'clone'   => false,
				// Options: array of Placeholder => Label for text boxes
				// Number of options are not limited
				'options' => array(
					'Button Title'			=> sprintf( esc_html__( 'Button Title %1$s', 'medicale-wp' ), '<br>'),
					'Button Link'			=> sprintf( esc_html__( 'Button Link %1$s', 'medicale-wp' ), '<br>'),
				),
				'std'	 => array( 
					esc_html__( 'Custom Button', 'medicale-wp' ),
					esc_html__( '#', 'medicale-wp' ),
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column2_content', 'in', 'custom-button' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Column 2 - Custom Button Design Style', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column2_custom_button_design_style',
				'type'		=> 'select',
				'options'   => array(
					'btn-default'   => esc_html__( 'Button Defaults', 'medicale-wp' ),
					'btn-border'	=> esc_html__( 'Button Border', 'medicale-wp' ),
					'btn-dark'		=> esc_html__( 'Button Dark', 'medicale-wp' ),
					'btn-gray'		=> esc_html__( 'Button Gray', 'medicale-wp' ),
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column2_content', 'in', 'custom-button' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Column 2 - Custom Button Make Theme Colored?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column2_custom_button_theme_colored',
				'type'		=> 'checkbox',
				'desc'		=> esc_html__( 'To make the button theme colored, please check it.', 'medicale-wp' ),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column2_content', 'in', 'custom-button' )
				),
				'tab'		=> 'header-top',
			),


			array(
				'type' => 'divider',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column2_content', 'in', 'social-links' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Column 2 - Social Links Color', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column2_social_links_color',
				'type'		=> 'select',
				'options'   => array(
					'icon-dark'	 => esc_html__( 'Dark', 'medicale-wp' ),
					''			 => esc_html__( 'Default', 'medicale-wp' ),
					'icon-gray'	 => esc_html__( 'Gray', 'medicale-wp' ),
				),
				'std'		=> '',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column2_content', 'in', 'social-links' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Column 2 - Social Links Icon Style', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column2_social_links_icon_style',
				'type'		=> 'select',
				'options'   => array(
					'icon-rounded'   => esc_html__( 'Rounded', 'medicale-wp' ),
					''				 => esc_html__( 'Default', 'medicale-wp' ),
					'icon-circled'   => esc_html__( 'Circled', 'medicale-wp' ),
				),
				'std'		=> '',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column2_content', 'in', 'social-links' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Column 2 - Social Links Make Icon Area Bordered?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column2_social_links_icon_border_style',
				'type'		=> 'checkbox',
				'desc'		=> esc_html__( 'To make the social icons area bordered, please check it.', 'medicale-wp' ),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column2_content', 'in', 'social-links' )
				),
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Column 2 - Social Links Make Theme Colored?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column2_social_links_icon_theme_colored',
				'type'		=> 'checkbox',
				'desc'		=> esc_html__( 'To make the social links theme colored, please check it.', 'medicale-wp' ),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_top_column2_content', 'in', 'social-links' )
				),
				'tab'		=> 'header-top',
			),


			array(
				'type' => 'divider',
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_show_header_top', '!=', '0' )
				),*/
				'tab'		=> 'header-top',
			),
			array(
				'name'		=> esc_html__( 'Column 2 - Text Alignment', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_column2_text_align',
				'type'		=> 'select',
				'options'   => $text_align_array,
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_show_header_top', '!=', '0' )
				),*/
				'tab'		=> 'header-top',
			),



			// heading Columns 3
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Column 3: Header Middle Row Right Widget', 'medicale-wp' ),
				'desc'		=> esc_html__( 'Here we have treated Column 3 as the header middle row right widget. Header Middle Row features are only available for Second "Header Layout Type"! So please choose it from here "Header Layout Type" or from Header Layout tab of Theme Options.', 'medicale-wp' ),
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_show_header_top', '!=', '0' )
				),*/
				'tab'		=> 'header-mid',
			),
			array(
				'name'		=> esc_html__( 'Column 3 - Content Types', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_mid_column3_content',
				'type'		=> 'select',
				'desc'		=> sprintf( esc_html__( '> You can choose multiple. %3$s> In case of %1$s"Social Links"%2$s, you will find it\'s settings at %1$sTheme Options > Social Links%2$s.', 'medicale-wp' ), '<strong>', '</strong>', '<br>'),
				'options'   => array(
					'inherit'					=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'column3-contact-info'		=> esc_html__( 'Contact Info', 'medicale-wp' ),
					'column3-custom-text'		=> esc_html__( 'Custom Text', 'medicale-wp' ),
					'column3-custom-button'	 	=> esc_html__( 'Custom Button', 'medicale-wp' ),
					'column3-social-links'		=> esc_html__( 'Social Links', 'medicale-wp' ),
					'column3-search-box'		=> esc_html__( 'Search Box', 'medicale-wp' ),
				),
				'multiple'  => true,
				'std'		=> array( 
					'inherit'
				),
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_show_header_top', '!=', '0' )
				),*/
				'tab'		=> 'header-mid',
			),

			array(
				'type' => 'divider',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_mid_column3_content', 'in', 'column3-contact-info' )
				),
				'tab'		=> 'header-mid',
			),
			array(
				'name'		=> esc_html__( 'Column 3 - Contact Info - Icon Box Style', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_mid_column3_contact_info_iconbox_style',
				'type'		=> 'select',
				'options'   => array(
					'inherit'			=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'big-font-icon'	 	=> esc_html__( 'Big Font Icon', 'medicale-wp' ),
					'small-font-icon'   => esc_html__( 'Small Font Icon', 'medicale-wp' ),
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_mid_column3_content', 'in', 'column3-contact-info' )
				),
				'tab'		=> 'header-mid',
			),
			array(
				'name'	=> esc_html__( 'Column 3 - Contact Info Visibility', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_mid_column3_contact_info_checkbox',
				'type'	=> 'checkbox_list',
				'desc'	=> esc_html__( 'Please choose which fields you want to display.', 'medicale-wp' ),
				// Array of 'value' => 'Label' pairs for radio options.
				// Note: the 'value' is stored in meta field, not the 'Label'
				'options' => array(
					'phone'			=> 'Phone',
					'email'			=> 'Email',
					'address'		=> 'Address',
					'opening-hours' => 'Opening Hours',
				),
				'std'	 => array( 
					'phone',
					'email'
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_mid_column3_content', 'in', 'column3-contact-info' )
				),
				'tab'		=> 'header-mid',
			),
			array(
				'name'	=> esc_html__( 'Column 3 - Contact Info - Phone', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_mid_column3_contact_info_phone',
				'type'	=> 'text_list',
				'clone'   => false,
				// Options: array of Placeholder => Label for text boxes
				// Number of options are not limited
				'options' => array(
					'title'			=> sprintf( esc_html__( 'Title%1$s', 'medicale-wp' ), '<br>'),
					'subtitle'		=> sprintf( esc_html__( 'Subtitle%1$s', 'medicale-wp' ), '<br>'),
				),
				'std'	 => array( 
					esc_html__( 'Call us at', 'medicale-wp' ),
					esc_html__( '+123 4567 8901', 'medicale-wp' ),
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_mid_column3_contact_info_checkbox', 'in', 'phone' )
				),
				'tab'		=> 'header-mid',
			),
			array(
				'name'	=> esc_html__( 'Column 3 - Contact Info - Email', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_mid_column3_contact_info_email',
				'type'	=> 'text_list',
				'clone'   => false,
				// Options: array of Placeholder => Label for text boxes
				// Number of options are not limited
				'options' => array(
					'title'			=> sprintf( esc_html__( 'Title%1$s', 'medicale-wp' ), '<br>'),
					'subtitle'		=> sprintf( esc_html__( 'Subtitle%1$s', 'medicale-wp' ), '<br>'),
				),
				'std'	 => array( 
					esc_html__( 'Email us', 'medicale-wp' ),
					esc_html__( 'info@yourdomain.com', 'medicale-wp' ),
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_mid_column3_contact_info_checkbox', 'in', 'email' )
				),
				'tab'		=> 'header-mid',
			),
			array(
				'name'	=> esc_html__( 'Column 3 - Contact Info - Address', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_mid_column3_contact_info_address',
				'type'	=> 'text_list',
				'clone'   => false,
				// Options: array of Placeholder => Label for text boxes
				// Number of options are not limited
				'options' => array(
					'title'			=> sprintf( esc_html__( 'Title%1$s', 'medicale-wp' ), '<br>'),
					'subtitle'		=> sprintf( esc_html__( 'Subtitle%1$s', 'medicale-wp' ), '<br>'),
				),
				'std'	 => array( 
					esc_html__( 'Envato HQ', 'medicale-wp' ),
					esc_html__( '121 King Street, Melbourne', 'medicale-wp' ),
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_mid_column3_contact_info_checkbox', 'in', 'address' )
				),
				'tab'		=> 'header-mid',
			),
			array(
				'name'	=> esc_html__( 'Column 3 - Contact Info - Opening Hours', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_mid_column3_contact_info_opening_hours',
				'type'	=> 'text_list',
				'clone'   => false,
				// Options: array of Placeholder => Label for text boxes
				// Number of options are not limited
				'options' => array(
					'title'			=> sprintf( esc_html__( 'Title%1$s', 'medicale-wp' ), '<br>'),
					'subtitle'		=> sprintf( esc_html__( 'Subtitle%1$s', 'medicale-wp' ), '<br>'),
				),
				'std'	 => array( 
					esc_html__( '09:00 - 17:00', 'medicale-wp' ),
					esc_html__( 'Monday - Friday', 'medicale-wp' ),
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_mid_column3_contact_info_checkbox', 'in', 'opening-hours' )
				),
				'tab'		=> 'header-mid',
			),


			array(
				'type' => 'divider',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_mid_column3_content', 'in', 'column3-custom-text' )
				),
				'tab'		=> 'header-mid',
			),
			array(
				'name'		=> esc_html__( 'Column 3 - Custom Text', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_mid_column3_custom_text',
				'type'		=> 'textarea',
				'desc'		=> esc_html__( 'Enter your custom text. Custom HTML tags are allowed (wp_kses).', 'medicale-wp' ),
				'std'		=> sprintf( esc_html__( 'Custom %1$sheader top%2$s text goes here!', 'medicale-wp' ), '<strong>', '</strong>'),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_mid_column3_content', 'in', 'column3-custom-text' )
				),
				'tab'		=> 'header-mid',
			),


			array(
				'type' => 'divider',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_mid_column3_content', 'in', 'column3-custom-button' )
				),
				'tab'		=> 'header-mid',
			),
			array(
				'name'	=> esc_html__( 'Column 3 - Custom Button', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_mid_column3_custom_button',
				'type'	=> 'text_list',
				'desc'		=> esc_html__( 'Show a custom button in the header top.', 'medicale-wp' ),
				'clone'   => false,
				// Options: array of Placeholder => Label for text boxes
				// Number of options are not limited
				'options' => array(
					'Button Title'			=> sprintf( esc_html__( 'Button Title %1$s', 'medicale-wp' ), '<br>'),
					'Button Link'			=> sprintf( esc_html__( 'Button Link %1$s', 'medicale-wp' ), '<br>'),
				),
				'std'	 => array( 
					esc_html__( 'Custom Button', 'medicale-wp' ),
					esc_html__( '#', 'medicale-wp' ),
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_mid_column3_content', 'in', 'column3-custom-button' )
				),
				'tab'		=> 'header-mid',
			),
			array(
				'name'		=> esc_html__( 'Column 3 - Custom Button Design Style', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_mid_column3_custom_button_design_style',
				'type'		=> 'select',
				'options'   => array(
					'btn-default'   => esc_html__( 'Button Defaults', 'medicale-wp' ),
					'btn-border'	=> esc_html__( 'Button Border', 'medicale-wp' ),
					'btn-dark'		=> esc_html__( 'Button Dark', 'medicale-wp' ),
					'btn-gray'		=> esc_html__( 'Button Gray', 'medicale-wp' ),
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_mid_column3_content', 'in', 'column3-custom-button' )
				),
				'tab'		=> 'header-mid',
			),
			array(
				'name'		=> esc_html__( 'Column 3 - Custom Button Make Theme Colored?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_mid_column3_custom_button_theme_colored',
				'type'		=> 'checkbox',
				'desc'		=> esc_html__( 'To make the button theme colored, please check it.', 'medicale-wp' ),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_mid_column3_content', 'in', 'column3-custom-button' )
				),
				'tab'		=> 'header-mid',
			),


			array(
				'type' => 'divider',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_mid_column3_content', 'in', 'column3-social-links' )
				),
				'tab'		=> 'header-mid',
			),
			array(
				'name'		=> esc_html__( 'Column 3 - Social Links Color', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_mid_column3_social_links_color',
				'type'		=> 'select',
				'options'   => array(
					'icon-dark'	 => esc_html__( 'Dark', 'medicale-wp' ),
					''			 => esc_html__( 'Default', 'medicale-wp' ),
					'icon-gray'	 => esc_html__( 'Gray', 'medicale-wp' ),
				),
				'std'		=> '',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_mid_column3_content', 'in', 'column3-social-links' )
				),
				'tab'		=> 'header-mid',
			),
			array(
				'name'		=> esc_html__( 'Column 3 - Social Links Icon Style', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_mid_column3_social_links_icon_style',
				'type'		=> 'select',
				'options'   => array(
					'icon-rounded'   => esc_html__( 'Rounded', 'medicale-wp' ),
					''				 => esc_html__( 'Default', 'medicale-wp' ),
					'icon-circled'   => esc_html__( 'Circled', 'medicale-wp' ),
				),
				'std'		=> '',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_mid_column3_content', 'in', 'column3-social-links' )
				),
				'tab'		=> 'header-mid',
			),
			array(
				'name'		=> esc_html__( 'Column 3 - Social Links Make Icon Area Bordered?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_mid_column3_social_links_icon_border_style',
				'type'		=> 'checkbox',
				'desc'		=> esc_html__( 'To make the social icons area bordered, please check it.', 'medicale-wp' ),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_mid_column3_content', 'in', 'column3-social-links' )
				),
				'tab'		=> 'header-mid',
			),
			array(
				'name'		=> esc_html__( 'Column 3 - Social Links Make Theme Colored?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_mid_column3_social_links_icon_theme_colored',
				'type'		=> 'checkbox',
				'desc'		=> esc_html__( 'To make the social links theme colored, please check it.', 'medicale-wp' ),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_header_mid_column3_content', 'in', 'column3-social-links' )
				),
				'tab'		=> 'header-mid',
			),


			// DIVIDER
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Header Navigation Row', 'medicale-wp' ),
				//'visible' => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'  => 'header-navigation-layout',
			),
			array(
				'name'		=> esc_html__( 'Show Header Navigation Row', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_show_header_nav_row',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Enabling/Disabling this option will show/hide Whole Header Navigation Row section.', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'		=> 'header-navigation-layout',
			),
			array(
				'name'		=> esc_html__( 'Use Theme Color in Background?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_nav_row_bgcolor_use_themecolor',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Use theme color or custom bg color in Header Navigation Row', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No - Instead use custom bg color', 'medicale-wp' ),
				),
				//'visible' => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'		=> 'header-navigation-layout',
			),
			array(
				'name'		=> esc_html__( 'Custom Background Color', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_nav_row_custom_bgcolor',
				'type'		=> 'color',
				'desc'		=> esc_html__( 'Pick a custom background color for Header Navigation Row', 'medicale-wp' ),
				'visible' => array( 'medicale_mascot_' . 'page_metabox_header_nav_row_bgcolor_use_themecolor', '=', '0' ),
				'tab'		=> 'header-navigation-layout',
			),
			array(
				'name'		=> esc_html__( 'Navigation Link and Cart/Search/Side Push Icon Color', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_nav_row_link_n_icon_color',
				'type'		=> 'color',
				'desc'		=> esc_html__( 'Pick a custom color for link and icons on Header Navigation Row', 'medicale-wp' ),
				//'visible' => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'		=> 'header-navigation-layout',
			),
			array(
				'name'		=> esc_html__( 'Show Cart Icon', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_nav_row_show_menu_cart_icon',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Add Cart Icon on the right hand side of the menu. WooCommerce plugin needs to be installed.', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				//'visible' => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'		=> 'header-navigation-layout',
			),
			array(
				'name'		=> esc_html__( 'Show Search Icon', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_nav_row_show_menu_search_icon',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Add Search Icon on the right hand side of the menu.', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				//'visible' => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'		=> 'header-navigation-layout',
			),
			array(
				'name'		=> esc_html__( 'Show Side Push Panel', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_nav_row_show_side_push_panel',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Add Side Push Icon on the right hand side of the menu to Enable/Disable Side Push Panel section. You can easily add your widgets to this section from Appearance > Widgets (Side Push Panel Sidebar).', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				//'visible' => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'		=> 'header-navigation-layout',
			),


			//custom button
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Custom Button', 'medicale-wp' ),
				//'visible' => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'  => 'header-navigation-layout',
			),
			array(
				'name'		=> esc_html__( 'Show Custom Button', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_nav_row_show_custom_button',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Add Custom Button on the right hand side of the Header Navigation Row', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				//'visible' => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'		=> 'header-navigation-layout',
			),
			array(
				'name'	=> esc_html__( 'Custom Button Info', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_nav_custom_button_info',
				'type'	=> 'text_list',
				'desc'		=> esc_html__( 'Show a custom button in the Header Navigation Row.', 'medicale-wp' ),
				'clone'   => false,
				// Options: array of Placeholder => Label for text boxes
				// Number of options are not limited
				'options' => array(
					'Button Title'			=> sprintf( esc_html__( 'Button Title %1$s', 'medicale-wp' ), '<br>'),
					'Button Link'			=> sprintf( esc_html__( 'Button Link %1$s', 'medicale-wp' ), '<br>'),
				),
				'std'	 => array( 
					esc_html__( 'Custom Button', 'medicale-wp' ),
					esc_html__( '#', 'medicale-wp' ),
				),
				//'visible' => array( 'medicale_mascot_' . 'page_metabox_header_nav_row_show_custom_button', '=', '1' ),
				'tab'		=> 'header-navigation-layout',
			),
			array(
				'name'		=> esc_html__( 'Button Design Style', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_nav_custom_button_design_style',
				'type'		=> 'select',
				'options'   => array(
					'btn-default'   => esc_html__( 'Button Defaults', 'medicale-wp' ),
					'btn-border'	=> esc_html__( 'Button Border', 'medicale-wp' ),
					'btn-dark'		=> esc_html__( 'Button Dark', 'medicale-wp' ),
					'btn-gray'		=> esc_html__( 'Button Gray', 'medicale-wp' ),
				),
				//'visible' => array( 'medicale_mascot_' . 'page_metabox_header_nav_row_show_custom_button', '=', '1' ),
				'tab'		=> 'header-navigation-layout',
			),
			array(
				'name'		=> esc_html__( 'Make Button Theme Colored?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_nav_custom_button_theme_colored',
				'type'		=> 'checkbox',
				'desc'		=> esc_html__( 'To make the button theme colored, please check it.', 'medicale-wp' ),
				//'visible' => array( 'medicale_mascot_' . 'page_metabox_header_nav_row_show_custom_button', '=', '1' ),
				'tab'		=> 'header-navigation-layout',
			),




			array(
				'type' => 'heading',
				'name' => esc_html__( 'Header Navigation Menu', 'medicale-wp' ),
				//'visible' => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'  => 'header-navigation-layout',
			),

			array(
				'name'		=> esc_html__( 'Primary Navigation Menu', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_navigation_custom_primary_nav_menu',
				'type'		=> 'taxonomy',
				'desc'		=> sprintf( esc_html__( 'Select which menu you want to display as primary navigation on this page. Currently set to %1$s%2$s%3$s.', 'medicale-wp' ), '<a target="_blank" href="' . esc_url( admin_url( 'nav-menus.php?action=locations' ) ) . '">', $primary_nav_menu_name, '</a>' ),
				'taxonomy'  => 'nav_menu',
				'field_type'=> 'select',
				'query_args'=> array(),
				'tab'  => 'header-navigation-layout',
			),
			array(
				'name'		=> esc_html__( 'Enable One Page Nav Smooth Scrolling Effect', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_navigation_enable_one_page_nav_scrolling_effect',
				'type'		=> 'checkbox',
				'desc'		=> esc_html__( 'Check this box in order to enable one page navigation smooth scrollling effect.', 'medicale-wp' ),
				'tab'		=> 'header-navigation-layout',
			),

			array(
				'type' => 'heading',
				'name' => esc_html__( 'Header Navigation Skin Styling', 'medicale-wp' ),
				//'visible' => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'  => 'header-navigation-layout',
			),
			array(
				'name'		=> esc_html__( 'Navigation Skin', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_navigation_skin',
				'type'		=> 'select',
				'options'   => array(
					'inherit'			=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'default'			=> esc_html__( 'default', 'medicale-wp' ),
					'border-bottom'	 	=> esc_html__( 'border-bottom', 'medicale-wp' ),
					'border-boxed'		=> esc_html__( 'border-boxed', 'medicale-wp' ),
					'border-left'		=> esc_html__( 'border-left', 'medicale-wp' ),
					'border-top'		=> esc_html__( 'border-top', 'medicale-wp' ),
					'border-top-bottom' => esc_html__( 'border-top-bottom', 'medicale-wp' ),
					'bottom-trace'		=> esc_html__( 'bottom-trace', 'medicale-wp' ),
					'boxed'				=> esc_html__( 'boxed', 'medicale-wp' ),
					'colored'			=> esc_html__( 'colored', 'medicale-wp' ),
					'dark'				=> esc_html__( 'dark', 'medicale-wp' ),
					'gradient'			=> esc_html__( 'gradient', 'medicale-wp' ),
					'rounded-boxed'	 	=> esc_html__( 'rounded-boxed', 'medicale-wp' ),
					'shadow'			=> esc_html__( 'shadow', 'medicale-wp' ),
					'strip'				=> esc_html__( 'strip', 'medicale-wp' ),
					'subcolored'		=> esc_html__( 'subcolored', 'medicale-wp' ),
					'top-bottom-boxed'  => esc_html__( 'top-bottom-boxed', 'medicale-wp' ),
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'		=> 'header-navigation-layout',
			),
			array(
				'name'		=> esc_html__( 'Navigation Color Scheme', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_navigation_color_scheme',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'default'   => esc_html__( 'Default', 'medicale-wp' ),
					'blue'		=> esc_html__( 'Blue', 'medicale-wp' ),
					'green'	 	=> esc_html__( 'Green', 'medicale-wp' ),
					'orange'	=> esc_html__( 'Orange', 'medicale-wp' ),
					'pink'		=> esc_html__( 'Pink', 'medicale-wp' ),
					'purple'	=> esc_html__( 'Purple', 'medicale-wp' ),
					'red'		=> esc_html__( 'Red', 'medicale-wp' ),
					'yellow'	=> esc_html__( 'Yellow', 'medicale-wp' ),
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'		=> 'header-navigation-layout',
			),
			array(
				'name'		=> esc_html__( 'Navigation Primary Effect', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_navigation_primary_effect',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'fade'  	=> esc_html__( 'Fade', 'medicale-wp' ),
					'slide' 	=> esc_html__( 'Slide', 'medicale-wp' )
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'		=> 'header-navigation-layout',
			),
			array(
				'name'		=> esc_html__( 'Navigation CSS3 Animation', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_header_top_navigation_css3_animation',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'none'		=> esc_html__( 'None', 'medicale-wp' ),
					'zoom-in'   => esc_html__( 'Zoom In', 'medicale-wp' ),
					'zoom-out'  => esc_html__( 'Zoom Out', 'medicale-wp' ),
					'drop-up'   => esc_html__( 'Drop Up', 'medicale-wp' ),
					'drop-left' => esc_html__( 'Drop Left', 'medicale-wp' ),
					'swing'	 	=> esc_html__( 'Swing', 'medicale-wp' ),
					'flip'		=> esc_html__( 'Flip', 'medicale-wp' ),
					'roll-in'   => esc_html__( 'Roll In', 'medicale-wp' ),
					'stretch'   => esc_html__( 'Stretch', 'medicale-wp' ),
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_header_visibility', '!=', '0' ),
				'tab'		=> 'header-navigation-layout',
			),
			//Header tab ends



			//Logo tab starts
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Logo Settings', 'medicale-wp' ),
				'desc' => esc_html__( 'Changes of the following settings will be effective only for this page.', 'medicale-wp' ),
				'tab'  => 'logo',
			),
			array(
				'name'		=> esc_html__( 'Alternative Site Brand', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_logo_site_brand',
				'desc'		=> esc_html__( 'Enter the text that will be appeared as logo.', 'medicale-wp' ),
				'type'		=> 'text',
				'tab'		=> 'logo',
			),

			// DIVIDER
			array(
				'type'		=> 'heading',
				'name'		=> esc_html__( 'Logo', 'medicale-wp' ),
				'tab'  => 'logo',
			),
			array(
				'name'		=> esc_html__( 'Use logo in replace of text?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_use_logo',
				'type'		=> 'select',
				'options'   => array(
					'inherit' 	=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				'tab'		=> 'logo',
			),
			array(
				'name'		=> esc_html__( 'Logo (Default)', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_logo_default',
				'type'		=> 'image_advanced',
				'max_file_uploads' => 1,
				'visible'   => array( 'medicale_mascot_' . 'page_metabox_use_logo', '!=', '0' ),
				'tab'		=> 'logo',
			),
			array(
				'name'		=> esc_html__( 'Logo (Default) Retina', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_logo_default_2x',
				'type'		=> 'image_advanced',
				'max_file_uploads' => 1,
				'visible'   => array( 'medicale_mascot_' . 'page_metabox_use_logo', '!=', '0' ),
				'tab'		=> 'logo',
			),

			// DIVIDER
			array(
				'type'		=> 'heading',
				'name'		=> esc_html__( 'Switchable logo', 'medicale-wp' ),
				'visible'   => array( 'medicale_mascot_' . 'page_metabox_use_logo', '!=', '0' ),
				'tab'		=> 'logo',
			),
			array(
				'name'		=> esc_html__( 'Switchable logo(Light/Dark)?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_use_switchable_logo',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				'visible'   => array( 'medicale_mascot_' . 'page_metabox_use_logo', '!=', '0' ),
				'tab'		=> 'logo',
			),
			array(
				'name'		=> esc_html__( 'Logo (Light)', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_logo_light',
				'type'		=> 'image_advanced',
				'max_file_uploads' => 1,
				'visible'   => array( 'medicale_mascot_' . 'page_metabox_use_switchable_logo', '!=', '0' ),
				'tab'		=> 'logo',
			),
			array(
				'name'		=> esc_html__( 'Logo (Light) Retina', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_logo_light_2x',
				'type'		=> 'image_advanced',
				'max_file_uploads' => 1,
				'visible'   => array( 'medicale_mascot_' . 'page_metabox_use_switchable_logo', '!=', '0' ),
				'tab'		=> 'logo',
			),
			array(
				'name'		=> esc_html__( 'Logo (Dark)', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_logo_dark',
				'type'		=> 'image_advanced',
				'max_file_uploads' => 1,
				'visible'   => array( 'medicale_mascot_' . 'page_metabox_use_switchable_logo', '!=', '0' ),
				'tab'		=> 'logo',
			),
			array(
				'name'		=> esc_html__( 'Logo (Dark) Retina', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_logo_dark_2x',
				'type'		=> 'image_advanced',
				'max_file_uploads' => 1,
				'visible'   => array( 'medicale_mascot_' . 'page_metabox_use_switchable_logo', '!=', '0' ),
				'tab'		=> 'logo',
			),

			// DIVIDER
			array(
				'type'		=> 'heading',
				'name'		=> esc_html__( 'Logo height', 'medicale-wp' ),
				'visible'   => array( 'medicale_mascot_' . 'page_metabox_use_logo', '!=', '0' ),
				'tab'		=> 'logo',
			),
			array(
				'name'		=> esc_html__( 'Maximum logo height(px)', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_logo_maximum_height',
				'type'		=> 'slider',
				'desc'		=> esc_html__( 'Enter maximum logo height in px.', 'medicale-wp' ),
				'suffix' => esc_html__( 'px', 'medicale-wp' ),
				'js_options' => array(
					'min'  => 20,
					'max'  => 150,
					'step' => 1,
				),
				// Default value
				'std'		=> 40,
				'visible'   => array( 'medicale_mascot_' . 'page_metabox_use_logo', '!=', '0' ),
				'tab'		=> 'logo',
			),
			//Logo tab ends



			//Page Title tab starts
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Page Title', 'medicale-wp' ),
				'desc' => esc_html__( 'Changes of the following settings will be effective only for this page.', 'medicale-wp' ),
				'tab'  => 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Enable Page Title', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_enable_page_title',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				'tab'		=> 'page-title',
			),


			// DIVIDER
			array(
				'type'		=> 'heading',
				'name'		=> esc_html__( 'Title & Subtitle', 'medicale-wp' ),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Page Title Type', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_page_title_type',
				'type'		=> 'select',
				'options'   => array(
					'page-title'   		=> esc_html__( 'Show This Page Title', 'medicale-wp' ),
					'custom-title'		=> esc_html__( 'Enter Custom Title', 'medicale-wp' ),
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Custom Title Text', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_custom_page_title_text',
				'desc'		=> esc_html__( 'Enter the text that will be appeared as page title.', 'medicale-wp' ),
				'type'		=> 'text',
				'visible'   => array( 
					//array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_page_title_type', '=', 'custom-title' )
				),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Subtitle Text', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_page_sub_title_text',
				'desc'		=> esc_html__( 'Enter the text that will be appeared as subtitle.', 'medicale-wp' ),
				'type'		=> 'text',
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),


			// DIVIDER
			array(
				'type'		=> 'heading',
				'name'		=> esc_html__( 'Page Title Layout', 'medicale-wp' ),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Choose Page Title Layout', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_layout',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'standard'  => esc_html__( 'Standard', 'medicale-wp' ),
					'split'	 	=> esc_html__( 'Split', 'medicale-wp' ),
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Page Title Container', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_container',
				'type'		=> 'select',
				'options'   => array(
					'inherit'			=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'container'			=> esc_html__( 'Container', 'medicale-wp' ),
					'container-fluid'   => esc_html__( 'Container Fluid', 'medicale-wp' )
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Page Title Text Alignment', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_text_align',
				'type'		=> 'select',
				'options'   => $text_align_array,
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Default Text Color', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_default_text_color',
				'type'		=> 'select',
				'options'   => array(
					'inherit'		=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'text-light' 	=> esc_html__( 'Light Text', 'medicale-wp' ),
					'text-dark'  	=> esc_html__( 'Dark Text', 'medicale-wp' ),
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Page Title Height', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_area_height',
				'type'		=> 'select',
				'options'   => array(
					'inherit'				=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'padding-default'		=> esc_html__( 'Default', 'medicale-wp' ),
					'padding-extra-small'   => esc_html__( 'Extra Small', 'medicale-wp' ),
					'padding-small'			=> esc_html__( 'Small', 'medicale-wp' ),
					'padding-medium'		=> esc_html__( 'Medium', 'medicale-wp' ),
					'padding-large'			=> esc_html__( 'Large', 'medicale-wp' ),
					'padding-extra-large'   => esc_html__( 'Extra Large', 'medicale-wp' ),
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Show Title', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_area_show_title',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Show Breadcrumbs', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_area_show_breadcrumbs',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),


			// DIVIDER
			array(
				'type'		=> 'heading',
				'name'		=> esc_html__( 'Page Title Background', 'medicale-wp' ),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Page Title Background Type', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_area_bg_type',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'bg-color'  => esc_html__( 'Background Color', 'medicale-wp' ),
					'bg-img'	=> esc_html__( 'Background Image', 'medicale-wp' ),
					'bg-video'	=> esc_html__( 'Background Video', 'medicale-wp' ),
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Background Color', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_area_bgcolor',
				'type'		=> 'color',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_title_area_bg_type', '=', 'bg-color' )
				),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Background Image', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_area_bgimg',
				'type'		=> 'image_advanced',
				'max_file_uploads' => 1,
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_title_area_bg_type', '=', 'bg-img' )
				),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Page Title Background Parallax Effect', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_area_bg_parallax_effect',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_title_area_bg_type', '=', 'bg-img' )
				),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Add Background Video', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_area_bg_video_status',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_title_area_bg_type', '=', 'bg-video' )
				),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Video Type', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_area_bg_video_type',
				'type'		=> 'select',
				'options'   => array(
					'youtube'		=> esc_html__( 'Youtube', 'medicale-wp' ),
					'self-hosted'   => esc_html__( 'Self Hosted Video', 'medicale-wp' )
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_title_area_bg_video_status', '=', '1' )
				),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Youtube Video ID', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_area_bg_video_youtube_id',
				'desc'		=> esc_html__( 'Only put video ID not the whole URL. Example: E5ln4uR4TwQ', 'medicale-wp' ),
				'type'		=> 'text',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_title_area_bg_video_type', '=', 'youtube' )
				),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Video Poster', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_area_bg_video_self_hosted_video_poster',
				'type'		=> 'image_advanced',
				'max_file_uploads' => 1,
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_title_area_bg_video_type', '=', 'self-hosted' )
				),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'MP4 Video', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_area_bg_video_self_hosted_mp4_video_url',
				'type'		=> 'file_advanced',
				'max_file_uploads' => 1,
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_title_area_bg_video_type', '=', 'self-hosted' )
				),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'WEBM Video', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_area_bg_video_self_hosted_webm_video_url',
				'type'		=> 'file_advanced',
				'max_file_uploads' => 1,
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_title_area_bg_video_type', '=', 'self-hosted' )
				),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'OGV Video', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_area_bg_video_self_hosted_ogv_video_url',
				'type'		=> 'file_advanced',
				'max_file_uploads' => 1,
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_title_area_bg_video_type', '=', 'self-hosted' )
				),
				'tab'		=> 'page-title',
			),



			// DIVIDER
			array(
				'type'		=> 'heading',
				'name'		=> esc_html__( 'Background Overlay', 'medicale-wp' ),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Add Page Title Background Overlay?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_area_bg_layer_overlay_status',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Overlay Opacity', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_area_bg_layer_overlay_opacity',
				'type'		=> 'slider',
				'desc'		=> esc_html__( 'Overlay on background image on Page Title.', 'medicale-wp' ),
				'js_options' => array(
					'min'  => 1,
					'max'  => 9,
					'step' => 1,
				),
				// Default value
				'std'		=> 7,
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_title_area_bg_layer_overlay_status', '=', '1' )
				),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Overlay Color', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_area_bg_layer_overlay_color',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'dark'  	=> esc_html__( 'Dark', 'medicale-wp' ),
					'white' 	=> esc_html__( 'White', 'medicale-wp' )
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_title_area_bg_layer_overlay_status', '=', '1' )
				),
				'tab'		=> 'page-title',
			),



			// DIVIDER
			array(
				'type'		=> 'heading',
				'name'		=> esc_html__( 'Animation Effect', 'medicale-wp' ),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Title Animation Effect', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_animation_effect',
				'type'		=> 'select_advanced',
				'options'   => medicale_mascot_animate_css_animation_list(),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Subtitle Animation Effect', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_subtitle_animation_effect',
				'type'		=> 'select_advanced',
				'options'   => medicale_mascot_animate_css_animation_list(),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),

			// DIVIDER
			array(
				'type'		=> 'heading',
				'name'		=> esc_html__( 'Typography', 'medicale-wp' ),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Title Tag', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_tag',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'h1'		=> 'h1',
					'h2'		=> 'h2',
					'h3'		=> 'h3',
					'h4'		=> 'h4',
					'h5'		=> 'h5',
					'h6'		=> 'h6',
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Title Color', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_title_color',
				'type'		=> 'color',
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Subtitle Tag', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_subtitle_tag',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'h1'		=> 'h1',
					'h2'		=> 'h2',
					'h3'		=> 'h3',
					'h4'		=> 'h4',
					'h5'		=> 'h5',
					'h6'		=> 'h6',
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			array(
				'name'		=> esc_html__( 'Subtitle Color', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_subtitle_color',
				'type'		=> 'color',
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_enable_page_title', '!=', '0' ),
				'tab'		=> 'page-title',
			),
			//Page Title tab ends





			//Layout tab starts
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Layout Settings', 'medicale-wp' ),
				'desc' => esc_html__( 'Changes of the following settings will be effective only for this page.', 'medicale-wp' ),
				'tab'  => 'layout-setings',
			),
			array(
				'name'		=> esc_html__( 'Page Layout', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_page_layout',
				'type'		=> 'select',
				'options'   => array(
					'inherit'		=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'boxed'			=> esc_html__( 'Boxed', 'medicale-wp' ),
					'stretched'	 	=> esc_html__( 'Stretched', 'medicale-wp' )
				),
				'tab'		=> 'layout-setings',
			),

			array(
				'type'		=> 'heading',
				'name'		=> esc_html__( 'Boxed Layout Settings', 'medicale-wp' ),
				'visible'   => array( 'medicale_mascot_' . 'page_metabox_page_layout', '!=', 'stretched' ),
				'tab'		=> 'layout-setings',
			),
			array(
				'name'		=> esc_html__( 'Padding Top(px)', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_boxed_layout_padding_top',
				'desc'		=> esc_html__( 'Please put only integer value. Because the unit \'px\' will be automatically added at the end of the value.', 'medicale-wp' ),
				'type'		=> 'number',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_page_layout', '!=', 'stretched' ),
					array( 'medicale_mascot_' . 'page_metabox_boxed_layout_type', '!=', 'attach-top-bottom' )
				),
				'tab'		=> 'layout-setings',
			),
			array(
				'name'		=> esc_html__( 'Padding Bottom(px)', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_boxed_layout_padding_bottom',
				'desc'		=> esc_html__( 'Please put only integer value. Because the unit \'px\' will be automatically added at the end of the value.', 'medicale-wp' ),
				'type'		=> 'number',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_page_layout', '!=', 'stretched' ),
				),
				'tab'		=> 'layout-setings',
			),
			array(
				'name'		=> esc_html__( 'Container Shadow?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_boxed_layout_container_shadow',
				'desc'		=> esc_html__( 'Add shadow around the container.', 'medicale-wp' ),
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				'visible'   => array( 'medicale_mascot_' . 'page_metabox_page_layout', '!=', 'stretched' ),
				'tab'		=> 'layout-setings',
			),


			array(
				'name'		=> esc_html__( 'Background Type', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_boxed_layout_bg_type',
				'desc'		=> esc_html__( 'You can use patterns, image or solid color as a background.', 'medicale-wp' ),
				'type'		=> 'select',
				'options'   => array(
					'inherit'		=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'bg-color'	 	=> esc_html__( 'Solid Color', 'medicale-wp' ),
					'bg-pattern'	=> esc_html__( 'Patterns from Theme Library', 'medicale-wp' ),
					'bg-image'	 	=> esc_html__( 'Upload Own Image', 'medicale-wp' ),
				),
				'visible'   => array( 'medicale_mascot_' . 'page_metabox_page_layout', '!=', 'stretched' ),
				'tab'		=> 'layout-setings',
			),
			array(
				'name'		=> esc_html__( 'Background Color', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_boxed_layout_bg_type_color',
				'type'		=> 'color',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_boxed_layout_bg_type', '=', 'bg-color' )
				),
				'tab'		=> 'layout-setings',
			),
			array(
				'name'		=> esc_html__( 'Background Pattern from Theme Library', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_boxed_layout_bg_type_pattern',
				'type'		=> 'image_select',
				// Array of 'value' => 'Image Source' pairs
				'options'   => $sample_patterns,
				'std'		=> $sample_patterns[key($sample_patterns)],
				// Allow to select multiple values? Default is false
				// 'multiple' => true,
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_boxed_layout_bg_type', '=', 'bg-pattern' )
				),
				'tab'		=> 'layout-setings',
			),
			array(
				'name'		=> esc_html__( 'Background Image', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_boxed_layout_bg_type_img',
				'type'		=> 'image_advanced',
				'max_file_uploads' => 1,
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_boxed_layout_bg_type', '=', 'bg-image' )
				),
				'tab'		=> 'layout-setings',
			),


			array(
				'type'		=> 'heading',
				'name'		=> esc_html__( 'Content Width Setting', 'medicale-wp' ),
				'tab'		=> 'layout-setings',
			),
			array(
				'name'		=> esc_html__( 'Content Width', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_content_width',
				'desc'		=> esc_html__( 'Select content width. You can use any width by using custom CSS.', 'medicale-wp' ),
				'type'		=> 'select',
				'options'   => array(
					'inherit'				=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'container-970px'	 	=> esc_html__( '970px', 'medicale-wp' ),
					'container-default'   	=> esc_html__( '1170px (Bootstrap Default)', 'medicale-wp' ),
					'container-100pr'	 	=> esc_html__( 'Fullwidth 100%', 'medicale-wp' )
				),
				'tab'		=> 'layout-setings',
			),
			//Layout tab ends



			//footer-top-callout tab starts
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Footer Top Callout Settings', 'medicale-wp' ),
				'desc' => esc_html__( 'Changes of the following settings will be effective only for this page.', 'medicale-wp' ),
				'tab'		=> 'footer-top-callout',
			),
			array(
				'name'		=> esc_html__( 'Footer Top Callout Visibility', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_top_callout_visibility',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Show or hide Footer Top Callout only for this page.', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'		=> esc_html__( 'Show', 'medicale-wp' ),
					'0'		=> esc_html__( 'Hide', 'medicale-wp' ),
				),
				'tab'		=> 'footer-top-callout',
			),



			//footer tab starts
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Footer Settings', 'medicale-wp' ),
				'desc' => esc_html__( 'Changes of the following settings will be effective only for this page.', 'medicale-wp' ),
				'tab'		=> 'footer',
			),
			array(
				'name'		=> esc_html__( 'Footer Visibility', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_visibility',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Show or hide footer only for this page.', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Show', 'medicale-wp' ),
					'0'			=> esc_html__( 'Hide', 'medicale-wp' ),
				),
				'tab'		=> 'footer',
			),
			array(
				'name'		=> esc_html__( 'Fixed Footer Bottom Effect', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_fixed_footer_bottom',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Enabling this option will make Footer gradually appear on scroll. This is popular for OnePage Websites.', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
				'tab'		=> 'footer',
			),
			array(
				'name'		=> esc_html__( 'Footer Text Color~', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_text_color',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Select footer text color. Inverted will turn font color to black. Inverted is suitable for white background.', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'default'   => esc_html__( 'White (Default)', 'medicale-wp' ),
					'inverted'  => esc_html__( 'Inverted', 'medicale-wp' )
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
				'tab'		=> 'footer',
			),




			// heading
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Footer Top Settings', 'medicale-wp' ),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
				'tab'		=> 'footer-top',
			),
			array(
				'name'		=> esc_html__( 'Show Footer Top', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_show_footer_top',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Enabling this option will show Footer Top section.', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
				'tab'		=> 'footer-top',
			),
			array(
				'name'		=> esc_html__( 'Choose Footer Top Columns Layout', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_top_columns_layout',
				'type'		=> 'select',
				'options'   => array(
					'inherit'		=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'25-25-25-25'   => esc_html__( '1/4 + 1/4 + 1/4 + 1/4', 'medicale-wp' ),
					'33-33-33'		=> esc_html__( '1/3 + 1/3 + 1/3', 'medicale-wp' ),
					'25-50-25'		=> esc_html__( '1/4 + 1/2 + 1/4', 'medicale-wp' ),
					'25-25-50'		=> esc_html__( '1/4 + 1/4 + 1/2', 'medicale-wp' ),
					'50-25-25'		=> esc_html__( '1/2 + 1/4 + 1/4', 'medicale-wp' ),
					'50-50'			=> esc_html__( '1/2 + 1/2', 'medicale-wp' ),
					'66-33'			=> esc_html__( '2/3 + 1/3', 'medicale-wp' ),
					'33-66'			=> esc_html__( '1/3 + 2/3', 'medicale-wp' ),
					'25-75'			=> esc_html__( '1/4 + 3/4', 'medicale-wp' ),
					'75-25'			=> esc_html__( '3/4 + 1/4', 'medicale-wp' ),
					'100'			=> esc_html__( '1/1', 'medicale-wp' ),
				),
				// Allow to select multiple values? Default is false
				// 'multiple' => true,
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_show_footer_top', '!=', 'no' )
				),*/
				'tab'		=> 'footer-top',
			),
			array(
				'name'		=> esc_html__( 'Footer Top Container', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_top_container',
				'type'		=> 'select',
				'options'   => array(
					'inherit'			=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'container'			=> esc_html__( 'Container', 'medicale-wp' ),
					'container-fluid'   => esc_html__( 'Container Fluid', 'medicale-wp' )
				),
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_show_footer_top', '!=', 'no' )
				),*/
				'tab'		=> 'footer-top',
			),




			// heading
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Footer Top Background Settings', 'medicale-wp' ),
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_show_footer_top', '!=', 'no' )
				),*/
				'tab'		=> 'footer-top',
			),
			array(
				'name'		=> esc_html__( 'Footer Background Type', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_top_bg_type',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'bg-color'  => esc_html__( 'Background Color', 'medicale-wp' ),
					'bg-img'	=> esc_html__( 'Background Image', 'medicale-wp' ),
				),
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_show_footer_top', '!=', 'no' )
				),*/
				'tab'		=> 'footer-top',
			),
			array(
				'name'		=> esc_html__( 'Background Color', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_top_bgcolor',
				'type'		=> 'color',
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_top_bg_type', '=', 'bg-color' )
				),
				'tab'		=> 'footer-top',
			),
			array(
				'name'		=> esc_html__( 'Background Image', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_top_bgimg',
				'type'		=> 'image_advanced',
				'max_file_uploads' => 1,
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_top_bg_type', '=', 'bg-img' )
				),
				'tab'		=> 'footer-top',
			),
			array(
				'name'		=> esc_html__( 'Background Parallax Effect', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_top_bg_parallax_effect',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_top_bg_type', '=', 'bg-img' )
				),
				'tab'		=> 'footer-top',
			),
			

			// heading
			array(
				'type'		=> 'heading',
				'name'		=> esc_html__( 'Footer Top Background Overlay', 'medicale-wp' ),
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
				),*/
				'tab'		=> 'footer-top',
			),
			array(
				'name'		=> esc_html__( 'Add Background Overlay?', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_top_bg_layer_overlay_status',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
				),*/
				'tab'		=> 'footer-top',
			),
			array(
				'name'		=> esc_html__( 'Background Overlay Opacity', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_top_bg_layer_overlay_opacity',
				'type'		=> 'slider',
				'desc'		=> esc_html__( 'Overlay on background image on Footer.', 'medicale-wp' ),
				'js_options' => array(
					'min'  => 1,
					'max'  => 9,
					'step' => 1,
				),
				// Default value
				'std'		=> 7,
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_top_bg_layer_overlay_status', '=', '1' )
				),
				'tab'		=> 'footer-top',
			),
			array(
				'name'		=> esc_html__( 'Background Overlay Color', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_top_bg_layer_overlay_color',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'dark'  	=> esc_html__( 'Dark', 'medicale-wp' ),
					'white' 	=> esc_html__( 'White', 'medicale-wp' )
				),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_top_bg_layer_overlay_status', '=', '1' )
				),
				'tab'		=> 'footer-top',
			),




			// heading
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Footer Top Other Settings', 'medicale-wp' ),
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_show_footer_top', '!=', 'no' )
				),*/
				'tab'		=> 'footer-top',
			),

			array(
				'name'		=> esc_html__( 'Footer Column Separator', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_top_column_separator',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Display vertical line separator between widget columns.', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_show_footer_top', '!=', 'no' )
				),*/
				'tab'		=> 'footer-top',
			),
			array(
				'name'		=> esc_html__( 'Footer Top Columns Text Alignment', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_top_text_align',
				'type'		=> 'select',
				'options'   => $text_align_array,
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_show_footer_top', '!=', 'no' )
				),*/
				'tab'		=> 'footer-top',
			),
			array(
				'name'		=> esc_html__( 'Padding Top(px)', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_top_padding_top',
				'desc'		=> esc_html__( 'Please put only integer value. Because the unit \'px\' will be automatically added at the end of the value.', 'medicale-wp' ),
				'type'		=> 'number',
				'min'		=> 0,
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_show_footer_top', '!=', 'no' )
				),*/
				'tab'		=> 'footer-top',
			),
			array(
				'name'		=> esc_html__( 'Padding Bottom(px)', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_top_padding_bottom',
				'desc'		=> esc_html__( 'Please put only integer value. Because the unit \'px\' will be automatically added at the end of the value.', 'medicale-wp' ),
				'type'		=> 'number',
				'min'		=> 0,
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_show_footer_top', '!=', 'no' )
				),*/
				'tab'		=> 'footer-top',
			),





			// heading
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Footer Bottom Settings', 'medicale-wp' ),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
				'tab'		=> 'footer-bottom',
			),
			array(
				'name'		=> esc_html__( 'Show Footer Bottom', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_show_footer_bottom',
				'type'		=> 'select',
				'desc'		=> esc_html__( 'Enabling this option will show Footer Bottom section.', 'medicale-wp' ),
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'1'			=> esc_html__( 'Yes', 'medicale-wp' ),
					'0'			=> esc_html__( 'No', 'medicale-wp' ),
				),
				//'visible'   => array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
				'tab'		=> 'footer-bottom',
			),
			array(
				'name'		=> esc_html__( 'Footer Bottom Container', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_bottom_container',
				'type'		=> 'select',
				'options'   => array(
					'inherit'			=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'container'			=> esc_html__( 'Container', 'medicale-wp' ),
					'container-fluid'   => esc_html__( 'Container Fluid', 'medicale-wp' )
				),
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_show_footer_bottom', '!=', '0' )
				),*/
				'tab'		=> 'footer-bottom',
			),
			array(
				'name'		=> esc_html__( 'Footer Bottom Columns Layout', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout',
				'type'		=> 'select',
				'options'   => array(
					'inherit'   => esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'33-33-33'  => esc_html__( '1/3 + 1/3 + 1/3', 'medicale-wp' ),
					'25-50-25'  => esc_html__( '1/4 + 1/2 + 1/4', 'medicale-wp' ),
					'25-25-50'  => esc_html__( '1/4 + 1/4 + 1/2', 'medicale-wp' ),
					'50-25-25'  => esc_html__( '1/2 + 1/4 + 1/4', 'medicale-wp' ),
					'50-50'	 	=> esc_html__( '1/2 + 1/2', 'medicale-wp' ),
					'66-33'	 	=> esc_html__( '2/3 + 1/3', 'medicale-wp' ),
					'33-66'	 	=> esc_html__( '1/3 + 2/3', 'medicale-wp' ),
					'25-75'	 	=> esc_html__( '1/4 + 3/4', 'medicale-wp' ),
					'75-25'	 	=> esc_html__( '3/4 + 1/4', 'medicale-wp' ),
					'100'		=> esc_html__( '1/1', 'medicale-wp' ),
				),
				// Allow to select multiple values? Default is false
				// 'multiple' => true,
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_show_footer_bottom', '!=', '0' )
				),*/
				'tab'		=> 'footer-bottom',
			),


			// Footer Bottom Columns 1
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Footer Bottom Columns 1', 'medicale-wp' ),
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_show_footer_bottom', '!=', '0' )
				),*/
				'tab'		=> 'footer-bottom',
			),
			array(
				'name'		=> esc_html__( 'Columns 1 - Content Type', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_bottom_column1_content',
				'type'		=> 'select',
				'desc'		=> sprintf( esc_html__( '> You can choose multiple. %3$s> In case of %1$s"Social Links"%2$s, you will find it\'s settings at %1$sTheme Options > Social Links%2$s. %3$s> In case of %1$s"Footer Bottom Navigation"%2$s, please create menu from %1$sAppearance > Menus%2$s and set Theme Location %1$s"Columns 1 - Footer Bottom Navigation"%2$s', 'medicale-wp' ), '<strong>', '</strong>', '<br>'),
				'options'   => array(
					'inherit'			=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'copyright-text'  	=> esc_html__( 'Copyright Text', 'medicale-wp' ),
					'footer-nav'		=> esc_html__( 'Footer Bottom Navigation', 'medicale-wp' ),
					'social-links'		=> esc_html__( 'Social Links', 'medicale-wp' ),
				),
				'multiple'  => true,
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_show_footer_bottom', '!=', '0' )
				),*/
				'tab'		=> 'footer-bottom',
			),
			array(
				'name'		=> esc_html__( 'Columns 1 - Copyright Text', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_bottom_column1_copyright_text',
				'desc'		=> esc_html__( 'Enter your custom copyright text. HTML tags are allowed. Use [current-year] shortcode to show current year.', 'medicale-wp' ),
				'type'		=> 'textarea',
				'std'		=> sprintf( esc_html__( '&copy; Copyright [current-year] - All Rights Reserved - By %1$sKodeSolution%2$s', 'medicale-wp' ), '<a target="_blank" href="' . esc_url( '#' ) . '">', '</a>' ),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_column1_content', '=', 'copyright-text' )
				),
				'tab'		=> 'footer-bottom',
			),
			array(
				'name'		=> esc_html__( 'Column 1 - Text Alignment', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_bottom_column1_text_align',
				'type'		=> 'select',
				'options'   => $text_align_array,
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_show_footer_bottom', '!=', '0' )
				),*/
				'tab'		=> 'footer-bottom',
			),





			// Footer Bottom Columns 2
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Footer Bottom Columns 2', 'medicale-wp' ),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '100' ),
				),
				'tab'		=> 'footer-bottom',
			),
			array(
				'name'		=> esc_html__( 'Columns 2 - Content Type', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_bottom_column2_content',
				'type'		=> 'select',
				'desc'		=> sprintf( esc_html__( '> You can choose multiple. %3$s> In case of %1$s"Social Links"%2$s, you will find it\'s settings at %1$sTheme Options > Social Links%2$s. %3$s> In case of %1$s"Footer Bottom Navigation"%2$s, please create menu from %1$sAppearance > Menus%2$s and set Theme Location %1$s"Columns 2 - Footer Bottom Navigation"%2$s', 'medicale-wp' ), '<strong>', '</strong>', '<br>'),
				'options'   => array(
					'inherit'			=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'copyright-text'  	=> esc_html__( 'Copyright Text', 'medicale-wp' ),
					'footer-nav'		=> esc_html__( 'Footer Bottom Navigation', 'medicale-wp' ),
					'social-links'		=> esc_html__( 'Social Links', 'medicale-wp' ),
				),
				'multiple'  => true,
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '100' ),
				),
				'tab'		=> 'footer-bottom',
			),
			array(
				'name'		=> esc_html__( 'Columns 2 - Copyright Text', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_bottom_column2_copyright_text',
				'desc'		=> esc_html__( 'Enter your custom copyright text. HTML tags are allowed. Use [current-year] shortcode to show current year.', 'medicale-wp' ),
				'type'		=> 'textarea',
				'std'		=> sprintf( esc_html__( '&copy; Copyright [current-year] - All Rights Reserved - By %1$sKodeSolution%2$s', 'medicale-wp' ), '<a target="_blank" href="' . esc_url( '#' ) . '">', '</a>' ),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_column2_content', '=', 'copyright-text' )
				),
				'tab'		=> 'footer-bottom',
			),
			array(
				'name'		=> esc_html__( 'Column 2 - Text Alignment', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_bottom_column2_text_align',
				'type'		=> 'select',
				'options'   => $text_align_array,
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '100' ),
				),
				'tab'		=> 'footer-bottom',
			),






			// Footer Bottom Columns 3
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Footer Bottom Columns 3', 'medicale-wp' ),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '50-50' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '66-33' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '33-66' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '25-75' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '75-25' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '100' ),
				),
				'tab'		=> 'footer-bottom',
			),
			array(
				'name'		=> esc_html__( 'Columns 3 - Content Type', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_bottom_column3_content',
				'type'		=> 'select',
				'desc'		=> sprintf( esc_html__( '> You can choose multiple. %3$s> In case of %1$s"Social Links"%2$s, you will find it\'s settings at %1$sTheme Options > Social Links%2$s. %3$s> In case of %1$s"Footer Bottom Navigation"%2$s, please create menu from %1$sAppearance > Menus%2$s and set Theme Location %1$s"Columns 3 - Footer Bottom Navigation"%2$s', 'medicale-wp' ), '<strong>', '</strong>', '<br>'),
				'options'   => array(
					'inherit'			=> esc_html__( 'Inherit from Theme Options', 'medicale-wp' ),
					'copyright-text'  	=> esc_html__( 'Copyright Text', 'medicale-wp' ),
					'footer-nav'		=> esc_html__( 'Footer Bottom Navigation', 'medicale-wp' ),
					'social-links'		=> esc_html__( 'Social Links', 'medicale-wp' ),
				),
				'multiple'  => true,
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '50-50' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '66-33' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '33-66' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '25-75' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '75-25' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '100' ),
				),
				'tab'		=> 'footer-bottom',
			),
			array(
				'name'		=> esc_html__( 'Columns 3 - Copyright Text', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_bottom_column3_copyright_text',
				'desc'		=> esc_html__( 'Enter your custom copyright text. HTML tags are allowed. Use [current-year] shortcode to show current year.', 'medicale-wp' ),
				'type'		=> 'textarea',
				'std'		=> sprintf( esc_html__( '&copy; Copyright [current-year] - All Rights Reserved - By %1$sKodeSolution%2$s', 'medicale-wp' ), '<a target="_blank" href="' . esc_url( '#' ) . '">', '</a>' ),
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_column3_content', '=', 'copyright-text' )
				),
				'tab'		=> 'footer-bottom',
			),
			array(
				'name'		=> esc_html__( 'Columns 3 - Text Alignment', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_bottom_column3_text_align',
				'type'		=> 'select',
				'options'   => $text_align_array,
				'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '50-50' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '66-33' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '33-66' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '25-75' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '75-25' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_bottom_columns_layout', '!=', '100' ),
				),
				'tab'		=> 'footer-bottom',
			),








			// Footer Bottom Other Settings
			array(
				'type' => 'heading',
				'name' => esc_html__( 'Footer Bottom Other Settings', 'medicale-wp' ),
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_show_footer_bottom', '!=', '0' )
				),*/
				'tab'		=> 'footer-bottom',
			),
			array(
				'name'		=> esc_html__( 'Margin Top(px)', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_bottom_margin_top',
				'desc'		=> esc_html__( 'Please put only integer value. Because the unit \'px\' will be automatically added at the end of the value.', 'medicale-wp' ),
				'type'		=> 'number',
				'min'		=> 0,
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_show_footer_bottom', '!=', '0' )
				),*/
				'tab'		=> 'footer-bottom',
			),
			array(
				'name'		=> esc_html__( 'Margin Bottom(px)', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_bottom_margin_bottom',
				'desc'		=> esc_html__( 'Please put only integer value. Because the unit \'px\' will be automatically added at the end of the value.', 'medicale-wp' ),
				'type'		=> 'number',
				'min'		=> 0,
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_show_footer_bottom', '!=', '0' )
				),*/
				'tab'		=> 'footer-bottom',
			),
			array(
				'name'		=> esc_html__( 'Footer Bottom Background Color', 'medicale-wp' ),
				'id'		=> 'medicale_mascot_' . 'page_metabox_footer_bottom_bgcolor',
				'type'		=> 'color',
				/*'visible'   => array( 
					array( 'medicale_mascot_' . 'page_metabox_footer_visibility', '!=', '0' ),
					array( 'medicale_mascot_' . 'page_metabox_footer_show_footer_bottom', '!=', '0' )
				),*/
				'tab'		=> 'footer-bottom',
			),
			//footer tab ends

		),
	);


	return $meta_boxes;
}
