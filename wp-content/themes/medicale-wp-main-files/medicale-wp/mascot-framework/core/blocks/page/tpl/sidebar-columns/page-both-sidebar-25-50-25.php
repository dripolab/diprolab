<div class="row">
	<div class="col-md-3 sidebar-area sidebar-left">
	<?php get_sidebar( 'left' ); ?>
	</div>
	<div class="col-md-6 main-content-area">
	<?php do_action( 'medicale_mascot_page_main_content_area_start' ); ?>
	<?php
		medicale_mascot_get_page_content();
	?>
	<?php do_action( 'medicale_mascot_page_main_content_area_end' ); ?>
	</div>
	<div class="col-md-3 sidebar-area sidebar-right">
	<?php get_sidebar( 'right' ); ?>
	</div>
</div>