<?php
	/**
	* medicale_mascot_before_page_section hook.
	*
	*/
	do_action( 'medicale_mascot_before_page_section' );
?>
<section>
	<div class="<?php echo $container_type; ?>">
		<?php
			/**
			* medicale_mascot_page_container_start hook.
			*
			*/
			do_action( 'medicale_mascot_page_container_start' );
		?>

		<?php
			if ( have_posts() ) :
			// Start the Loop.
			while ( have_posts() ) : the_post();
				medicale_mascot_get_page_sidebar_layout( $page_layout );
			endwhile;
			endif;
		?>

		<?php
			/**
			* medicale_mascot_page_container_end hook.
			*
			*/
			do_action( 'medicale_mascot_page_container_end' );
		?>
	</div>
</section> 
<?php
	/**
	* medicale_mascot_after_page_section hook.
	*
	*/
	do_action( 'medicale_mascot_after_page_section' );
?>