<?php	
/*
*
*	Core Actions
*	---------------------------------------
*	Mascot Framework v1.0
* 	Copyright ThemeMascot 2017 - http://www.thememascot.com
*
*/


if(!function_exists('medicale_mascot_action_widgets_init')) {
	/**
	 * Init Widgets
	 */
	function medicale_mascot_action_widgets_init() {
	}
}


if(!function_exists('medicale_mascot_action_wp_head')) {
	/**
	 * Head Action
	 */
	function medicale_mascot_action_wp_head() {
		medicale_mascot_wp_head_responsive_viewport();
		medicale_mascot_wp_head_favicon();
		medicale_mascot_wp_head_apple_touch_icons();
	}
}


if(!function_exists('medicale_mascot_wp_head_responsive_viewport')) {
	/**
	 * Enable Responsive
	 */
	function medicale_mascot_wp_head_responsive_viewport() {
		if( medicale_mascot_get_redux_option( 'general-settings-enable-responsive', true ) ) { ?>
			<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<?php } else { ?>
			<meta name="viewport" content="width=1140, user-scalable=yes">
		<?php }
	}
}


if(!function_exists('medicale_mascot_wp_head_favicon')) {
	/**
	 * Add Favicon
	 */
	function medicale_mascot_wp_head_favicon() {
		if( medicale_mascot_get_redux_option( 'general-settings-favicon', false, 'url' ) ) { ?>
			<link href="<?php echo medicale_mascot_get_redux_option( 'general-settings-favicon', false, 'url' ); ?>" rel="shortcut icon">
		<?php } else { ?>
			<link href="<?php echo MASCOT_ASSETS_URI;?>/images/logo/favicon.png" rel="shortcut icon" type="image/png">
		<?php }
	}
}


if(!function_exists('medicale_mascot_wp_head_apple_touch_icons')) {
	/**
	 * Add Apple Touch Icons 144x144, 114x114, 72x72, 32x32
	 */
	function medicale_mascot_wp_head_apple_touch_icons() {
		//apple-touch-icon
		if( medicale_mascot_get_redux_option( 'general-settings-apple-touch-32', false, 'url' ) ) { ?>
			<link href="<?php echo medicale_mascot_get_redux_option( 'general-settings-apple-touch-32', false, 'url' ); ?>" rel="apple-touch-icon">
		<?php } else { ?>
			<link href="images/apple-touch-icon.png" rel="apple-touch-icon">
		<?php }
		
		//apple-touch-icon-72x72
		if( medicale_mascot_get_redux_option( 'general-settings-apple-touch-72', false, 'url' ) ) { ?>
			<link href="<?php echo medicale_mascot_get_redux_option( 'general-settings-apple-touch-72', false, 'url' ); ?>" rel="apple-touch-icon" sizes="72x72">
		<?php } else { ?>
			<link href="images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
		<?php }
		
		//apple-touch-icon-114x114
		if( medicale_mascot_get_redux_option( 'general-settings-apple-touch-114', false, 'url' ) ) { ?>
			<link href="<?php echo medicale_mascot_get_redux_option( 'general-settings-apple-touch-114', false, 'url' ); ?>" rel="apple-touch-icon" sizes="114x114">
		<?php } else { ?>
			<link href="images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
		<?php }
		
		//apple-touch-icon-144x144
		if( medicale_mascot_get_redux_option( 'general-settings-apple-touch-144', false, 'url' ) ) { ?>
			<link href="<?php echo medicale_mascot_get_redux_option( 'general-settings-apple-touch-144', false, 'url' ); ?>" rel="apple-touch-icon" sizes="144x144">
		<?php } else { ?>
			<link href="images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">
		<?php }
	}
}


if(!function_exists('medicale_mascot_action_wp_head_at_the_end')) {
	/**
	 * Head Action put code at the end
	 */
	function medicale_mascot_action_wp_head_at_the_end() {
		medicale_mascot_action_wp_head_at_the_end_ie8_support();
		medicale_mascot_wp_header_custom_html_js();
	}
}


if(!function_exists('medicale_mascot_action_wp_head_at_the_end_ie8_support')) {
	/**
	 * IE8 support of HTML5 elements
	 */
	function medicale_mascot_action_wp_head_at_the_end_ie8_support() {
	?>
	<!-- IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<?php
	}
}


if(!function_exists('medicale_mascot_wp_header_custom_html_js')) {
	/**
	 * Custom HTML/JS Code (in Footer)
	 */
	function medicale_mascot_wp_header_custom_html_js() {
		if( medicale_mascot_get_redux_option( 'custom-codes-custom-html-script-header' ) ) {
			echo "\n";
			echo medicale_mascot_get_redux_option( 'custom-codes-custom-html-script-header' );
			echo "\n";
		}
	}
}


if(!function_exists('medicale_mascot_action_wp_footer')) {
	/**
	 * Footer Action
	 */
	function medicale_mascot_action_wp_footer() {
		medicale_mascot_wp_footer_enable_backtotop();
		medicale_mascot_wp_footer_custom_html_js();
	}
}


if(!function_exists('medicale_mascot_wp_footer_enable_backtotop')) {
	/**
	 * Enable Back To Top
	 */
	function medicale_mascot_wp_footer_enable_backtotop() {
		if( medicale_mascot_get_redux_option( 'general-settings-enable-backtotop' ) ) { ?>
			<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
		<?php }
	}
}


if(!function_exists('medicale_mascot_wp_footer_custom_html_js')) {
	/**
	 * Custom HTML/JS Code (in Footer)
	 */
	function medicale_mascot_wp_footer_custom_html_js() {
		if( medicale_mascot_get_redux_option( 'custom-codes-custom-html-script-footer' ) ) {
			echo "\n";
			echo medicale_mascot_get_redux_option( 'custom-codes-custom-html-script-footer' );
			echo "\n";
		}
	}
}


if(!function_exists('medicale_mascot_action_theme_admin_login_custom_logo')) {
	/**
	 * Add Admin Login Custom Logo
	 */
	function medicale_mascot_action_theme_admin_login_custom_logo() { 
		$admin_login_logo = medicale_mascot_get_redux_option( 'logo-settings-admin-login-logo' );
		if( !medicale_mascot_variable_val_is_empty( $admin_login_logo ) ) {
	?>
		<style type="text/css">
			#login h1 a, .login h1 a {
				background-image: url(<?php echo $admin_login_logo['url']; ?>);
			height: 50px;
			width: 250px;
			background-size: 250px 50px;
			background-repeat: no-repeat;
			}
		</style>
	<?php 
		}
	}
	add_action( 'login_enqueue_scripts', 'medicale_mascot_action_theme_admin_login_custom_logo' );
}


if (!function_exists( 'medicale_mascot_require_core_plugin_message')) {
	/**
	 * Prints a mesasge in the admin if user hides TGMPA plugin activation message
	 */
	function medicale_mascot_require_core_plugin_message() {
		if ( get_user_meta( get_current_user_id(), 'tgmpa_dismissed_notice_tgmpa', true ) && !medicale_mascot_core_plugin_installed() ) {
		$class = 'notice notice-error';
		$message = esc_html__( '&diams;&diams;&diams; For proper theme functioning, the "<strong>Mascot Core</strong>" plugin is must required. Please ', 'medicale-wp' );
		$message .= '<a href="' . esc_url( admin_url( 'themes.php?page=tgmpa-install-plugins' ) ) . '">' . esc_html__( 'install', 'medicale-wp' ) . '</a>';
		$message .= esc_html__( ' and activate this plugin.', 'medicale-wp' );
		printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), $message ); 
		}
	}
	add_action( 'admin_notices', 'medicale_mascot_require_core_plugin_message' );
}


if(!function_exists('medicale_mascot_add_theme_page')) {
	/**
	 * Add Theme Page
	 */
	function medicale_mascot_add_theme_page() {
		add_theme_page('Mascot About', 'Mascot - About', 'edit_theme_options', 'mascot-about', 'medicale_mascot_theme_page_about');
		add_theme_page('Support & Help', 'Mascot - Help', 'edit_theme_options', 'mascot-docs', 'medicale_mascot_theme_page_docs');
		add_theme_page('FAQ', 'Mascot - FAQ', 'edit_theme_options', 'mascot-faq', 'medicale_mascot_theme_page_faq');
		add_theme_page('System Status', 'Mascot - System Status', 'edit_theme_options', 'mascot-system-status', 'medicale_mascot_theme_page_system_status');
	}
	add_action('admin_menu', 'medicale_mascot_add_theme_page');
}

if(!function_exists('medicale_mascot_theme_page_about')) {
	function medicale_mascot_theme_page_about() {
		echo medicale_mascot_get_template_part( 'admin/admin-tpl/mascot-about' );
	}
}

if(!function_exists('medicale_mascot_theme_page_docs')) {
	function medicale_mascot_theme_page_docs() {
		echo medicale_mascot_get_template_part( 'admin/admin-tpl/mascot-docs' );
	}
}

if(!function_exists('medicale_mascot_theme_page_faq')) {
	function medicale_mascot_theme_page_faq() {
		echo medicale_mascot_get_template_part( 'admin/admin-tpl/mascot-faq' );
	}
}

if(!function_exists('medicale_mascot_theme_page_system_status')) {
	function medicale_mascot_theme_page_system_status() {
		echo medicale_mascot_get_template_part( 'admin/admin-tpl/mascot-system-status' );
	}
}


if(!function_exists('medicale_mascot_theme_is_installed_hook')) {
	/**
	 * Theme Activated Hook
	 */
	function medicale_mascot_theme_is_installed_hook() {
		$mascot_installed_var = 'mascot_theme_is_installed';
		if ( false == get_option( $mascot_installed_var, false ) ) {		
			update_option( $mascot_installed_var, true );
			wp_redirect( admin_url( 'admin.php?page=mascot-about&theme-activated=true' ) );
			die();
		} 
		delete_option( $mascot_installed_var );
	}
	add_action( 'after_switch_theme', 'medicale_mascot_theme_is_installed_hook' );
}
