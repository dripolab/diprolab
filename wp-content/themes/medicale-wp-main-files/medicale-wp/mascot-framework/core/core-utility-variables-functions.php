<?php

if(!function_exists('medicale_mascot_animate_css_animation_list')) {
	/**
	 * animate.css animation list https://daneden.github.io/animate.css/
	 */
	function medicale_mascot_animate_css_animation_list() {
		$animate_css_animation_list = array(
			'' => '',
			'fadeIn' => 'fadeIn',
			'fadeInDown' => 'fadeInDown',
			'fadeInDownBig' => 'fadeInDownBig',
			'fadeInLeft' => 'fadeInLeft',
			'fadeInLeftBig' => 'fadeInLeftBig',
			'fadeInRight' => 'fadeInRight',
			'fadeInRightBig' => 'fadeInRightBig',
			'fadeInUp' => 'fadeInUp',
			'fadeInUpBig' => 'fadeInUpBig',
			'fadeOut' => 'fadeOut',
			'fadeOutDown' => 'fadeOutDown',
			'fadeOutDownBig' => 'fadeOutDownBig',
			'fadeOutLeft' => 'fadeOutLeft',
			'fadeOutLeftBig' => 'fadeOutLeftBig',
			'fadeOutRight' => 'fadeOutRight',
			'fadeOutRightBig' => 'fadeOutRightBig',
			'fadeOutUp' => 'fadeOutUp',
			'fadeOutUpBig' => 'fadeOutUpBig',
			'bounce' => 'bounce',
			'flash' => 'flash',
			'pulse' => 'pulse',
			'rubberBand' => 'rubberBand',
			'shake' => 'shake',
			'swing' => 'swing',
			'tada' => 'tada',
			'wobble' => 'wobble',
			'jello' => 'jello',
			'bounceIn' => 'bounceIn',
			'bounceInDown' => 'bounceInDown',
			'bounceInLeft' => 'bounceInLeft',
			'bounceInRight' => 'bounceInRight',
			'bounceInUp' => 'bounceInUp',
			'bounceOut' => 'bounceOut',
			'bounceOutDown' => 'bounceOutDown',
			'bounceOutLeft' => 'bounceOutLeft',
			'bounceOutRight' => 'bounceOutRight',
			'bounceOutUp' => 'bounceOutUp',
			'flip' => 'flip',
			'flipInX' => 'flipInX',
			'flipInY' => 'flipInY',
			'flipOutX' => 'flipOutX',
			'flipOutY' => 'flipOutY',
			'lightSpeedIn' => 'lightSpeedIn',
			'lightSpeedOut' => 'lightSpeedOut',
			'rotateIn' => 'rotateIn',
			'rotateInDownLeft' => 'rotateInDownLeft',
			'rotateInDownRight' => 'rotateInDownRight',
			'rotateInUpLeft' => 'rotateInUpLeft',
			'rotateInUpRight' => 'rotateInUpRight',
			'rotateOut' => 'rotateOut',
			'rotateOutDownLeft' => 'rotateOutDownLeft',
			'rotateOutDownRight' => 'rotateOutDownRight',
			'rotateOutUpLeft' => 'rotateOutUpLeft',
			'rotateOutUpRight' => 'rotateOutUpRight',
			'slideInUp' => 'slideInUp',
			'slideInDown' => 'slideInDown',
			'slideInLeft' => 'slideInLeft',
			'slideInRight' => 'slideInRight',
			'slideOutUp' => 'slideOutUp',
			'slideOutDown' => 'slideOutDown',
			'slideOutLeft' => 'slideOutLeft',
			'slideOutRight' => 'slideOutRight',
			'zoomIn' => 'zoomIn',
			'zoomInDown' => 'zoomInDown',
			'zoomInLeft' => 'zoomInLeft',
			'zoomInRight' => 'zoomInRight',
			'zoomInUp' => 'zoomInUp',
			'zoomOut' => 'zoomOut',
			'zoomOutDown' => 'zoomOutDown',
			'zoomOutLeft' => 'zoomOutLeft',
			'zoomOutRight' => 'zoomOutRight',
			'zoomOutUp' => 'zoomOutUp',
			'hinge' => 'hinge',
			'rollIn' => 'rollIn',
			'rollOut' => 'rollOut'
		);
		return $animate_css_animation_list;
	}
}

if(!function_exists('medicale_mascot_wp_admin_dashicons_list')) {
	/**
	 * WordPress admin Dashicons https://developer.wordpress.org/resource/dashicons
	 */
	function medicale_mascot_wp_admin_dashicons_list() {
		$animate_css_animation_list = array(
			'' => '',
			'dashicons-mascot' => 'dashicons-mascot',
			'dashicons-admin-appearance' => 'dashicons-admin-appearance',
			'dashicons-admin-collapse' => 'dashicons-admin-collapse',
			'dashicons-admin-comments' => 'dashicons-admin-comments',
			'dashicons-admin-customizer' => 'dashicons-admin-customizer',
			'dashicons-admin-generic' => 'dashicons-admin-generic',
			'dashicons-admin-home' => 'dashicons-admin-home',
			'dashicons-admin-links' => 'dashicons-admin-links',
			'dashicons-admin-media' => 'dashicons-admin-media',
			'dashicons-admin-multisite' => 'dashicons-admin-multisite',
			'dashicons-admin-network' => 'dashicons-admin-network',
			'dashicons-admin-page' => 'dashicons-admin-page',
			'dashicons-admin-plugins' => 'dashicons-admin-plugins',
			'dashicons-admin-post' => 'dashicons-admin-post',
			'dashicons-admin-settings' => 'dashicons-admin-settings',
			'dashicons-admin-site' => 'dashicons-admin-site',
			'dashicons-admin-tools' => 'dashicons-admin-tools',
			'dashicons-admin-users' => 'dashicons-admin-users',
			'dashicons-album' => 'dashicons-album',
			'dashicons-align-center' => 'dashicons-align-center',
			'dashicons-align-full-width' => 'dashicons-align-full-width',
			'dashicons-align-left' => 'dashicons-align-left',
			'dashicons-align-none' => 'dashicons-align-none',
			'dashicons-align-right' => 'dashicons-align-right',
			'dashicons-align-wide' => 'dashicons-align-wide',
			'dashicons-analytics' => 'dashicons-analytics',
			'dashicons-archive' => 'dashicons-archive',
			'dashicons-arrow-down-alt' => 'dashicons-arrow-down-alt',
			'dashicons-arrow-down-alt2' => 'dashicons-arrow-down-alt2',
			'dashicons-arrow-down' => 'dashicons-arrow-down',
			'dashicons-arrow-left-alt' => 'dashicons-arrow-left-alt',
			'dashicons-arrow-left-alt2' => 'dashicons-arrow-left-alt2',
			'dashicons-arrow-left' => 'dashicons-arrow-left',
			'dashicons-arrow-right-alt' => 'dashicons-arrow-right-alt',
			'dashicons-arrow-right-alt2' => 'dashicons-arrow-right-alt2',
			'dashicons-arrow-right' => 'dashicons-arrow-right',
			'dashicons-arrow-up-alt' => 'dashicons-arrow-up-alt',
			'dashicons-arrow-up-alt2' => 'dashicons-arrow-up-alt2',
			'dashicons-arrow-up' => 'dashicons-arrow-up',
			'dashicons-art' => 'dashicons-art',
			'dashicons-awards' => 'dashicons-awards',
			'dashicons-backup' => 'dashicons-backup',
			'dashicons-book-alt' => 'dashicons-book-alt',
			'dashicons-book' => 'dashicons-book',
			'dashicons-building' => 'dashicons-building',
			'dashicons-businessman' => 'dashicons-businessman',
			'dashicons-button' => 'dashicons-button',
			'dashicons-calendar-alt' => 'dashicons-calendar-alt',
			'dashicons-calendar' => 'dashicons-calendar',
			'dashicons-camera' => 'dashicons-camera',
			'dashicons-carrot' => 'dashicons-carrot',
			'dashicons-cart' => 'dashicons-cart',
			'dashicons-category' => 'dashicons-category',
			'dashicons-chart-area' => 'dashicons-chart-area',
			'dashicons-chart-bar' => 'dashicons-chart-bar',
			'dashicons-chart-line' => 'dashicons-chart-line',
			'dashicons-chart-pie' => 'dashicons-chart-pie',
			'dashicons-clipboard' => 'dashicons-clipboard',
			'dashicons-clock' => 'dashicons-clock',
			'dashicons-cloud' => 'dashicons-cloud',
			'dashicons-controls-back' => 'dashicons-controls-back',
			'dashicons-controls-forward' => 'dashicons-controls-forward',
			'dashicons-controls-pause' => 'dashicons-controls-pause',
			'dashicons-controls-play' => 'dashicons-controls-play',
			'dashicons-controls-repeat' => 'dashicons-controls-repeat',
			'dashicons-controls-skipback' => 'dashicons-controls-skipback',
			'dashicons-controls-skipforward' => 'dashicons-controls-skipforward',
			'dashicons-controls-volumeoff' => 'dashicons-controls-volumeoff',
			'dashicons-controls-volumeon' => 'dashicons-controls-volumeon',
			'dashicons-dashboard' => 'dashicons-dashboard',
			'dashicons-desktop' => 'dashicons-desktop',
			'dashicons-dismiss' => 'dashicons-dismiss',
			'dashicons-download' => 'dashicons-download',
			'dashicons-edit' => 'dashicons-edit',
			'dashicons-editor-aligncenter' => 'dashicons-editor-aligncenter',
			'dashicons-editor-alignleft' => 'dashicons-editor-alignleft',
			'dashicons-editor-alignright' => 'dashicons-editor-alignright',
			'dashicons-editor-bold' => 'dashicons-editor-bold',
			'dashicons-editor-break' => 'dashicons-editor-break',
			'dashicons-editor-code' => 'dashicons-editor-code',
			'dashicons-editor-contract' => 'dashicons-editor-contract',
			'dashicons-editor-customchar' => 'dashicons-editor-customchar',
			'dashicons-editor-expand' => 'dashicons-editor-expand',
			'dashicons-editor-help' => 'dashicons-editor-help',
			'dashicons-editor-indent' => 'dashicons-editor-indent',
			'dashicons-editor-insertmore' => 'dashicons-editor-insertmore',
			'dashicons-editor-italic' => 'dashicons-editor-italic',
			'dashicons-editor-justify' => 'dashicons-editor-justify',
			'dashicons-editor-kitchensink' => 'dashicons-editor-kitchensink',
			'dashicons-editor-ol' => 'dashicons-editor-ol',
			'dashicons-editor-outdent' => 'dashicons-editor-outdent',
			'dashicons-editor-paragraph' => 'dashicons-editor-paragraph',
			'dashicons-editor-paste-text' => 'dashicons-editor-paste-text',
			'dashicons-editor-paste-word' => 'dashicons-editor-paste-word',
			'dashicons-editor-quote' => 'dashicons-editor-quote',
			'dashicons-editor-removeformatting' => 'dashicons-editor-removeformatting',
			'dashicons-editor-rtl' => 'dashicons-editor-rtl',
			'dashicons-editor-spellcheck' => 'dashicons-editor-spellcheck',
			'dashicons-editor-strikethrough' => 'dashicons-editor-strikethrough',
			'dashicons-editor-table' => 'dashicons-editor-table',
			'dashicons-editor-textcolor' => 'dashicons-editor-textcolor',
			'dashicons-editor-ul' => 'dashicons-editor-ul',
			'dashicons-editor-underline' => 'dashicons-editor-underline',
			'dashicons-editor-unlink' => 'dashicons-editor-unlink',
			'dashicons-editor-video' => 'dashicons-editor-video',
			'dashicons-ellipsis' => 'dashicons-ellipsis',
			'dashicons-email-alt' => 'dashicons-email-alt',
			'dashicons-email-alt2' => 'dashicons-email-alt2',
			'dashicons-email' => 'dashicons-email',
			'dashicons-exerpt-view' => 'dashicons-exerpt-view',
			'dashicons-external' => 'dashicons-external',
			'dashicons-facebook-alt' => 'dashicons-facebook-alt',
			'dashicons-facebook' => 'dashicons-facebook',
			'dashicons-feedback' => 'dashicons-feedback',
			'dashicons-filter' => 'dashicons-filter',
			'dashicons-flag' => 'dashicons-flag',
			'dashicons-format-aside' => 'dashicons-format-aside',
			'dashicons-format-audio' => 'dashicons-format-audio',
			'dashicons-format-chat' => 'dashicons-format-chat',
			'dashicons-format-gallery' => 'dashicons-format-gallery',
			'dashicons-format-image' => 'dashicons-format-image',
			'dashicons-format-quote' => 'dashicons-format-quote',
			'dashicons-format-status' => 'dashicons-format-status',
			'dashicons-format-video' => 'dashicons-format-video',
			'dashicons-forms' => 'dashicons-forms',
			'dashicons-googleplus' => 'dashicons-googleplus',
			'dashicons-grid-view' => 'dashicons-grid-view',
			'dashicons-groups' => 'dashicons-groups',
			'dashicons-hammer' => 'dashicons-hammer',
			'dashicons-heading' => 'dashicons-heading',
			'dashicons-heart' => 'dashicons-heart',
			'dashicons-hidden' => 'dashicons-hidden',
			'dashicons-id-alt' => 'dashicons-id-alt',
			'dashicons-id' => 'dashicons-id',
			'dashicons-image-crop' => 'dashicons-image-crop',
			'dashicons-image-filter' => 'dashicons-image-filter',
			'dashicons-image-flip-horizontal' => 'dashicons-image-flip-horizontal',
			'dashicons-image-flip-vertical' => 'dashicons-image-flip-vertical',
			'dashicons-image-rotate-left' => 'dashicons-image-rotate-left',
			'dashicons-image-rotate-right' => 'dashicons-image-rotate-right',
			'dashicons-image-rotate' => 'dashicons-image-rotate',
			'dashicons-images-alt' => 'dashicons-images-alt',
			'dashicons-images-alt2' => 'dashicons-images-alt2',
			'dashicons-index-card' => 'dashicons-index-card',
			'dashicons-info' => 'dashicons-info',
			'dashicons-insert' => 'dashicons-insert',
			'dashicons-laptop' => 'dashicons-laptop',
			'dashicons-layout' => 'dashicons-layout',
			'dashicons-leftright' => 'dashicons-leftright',
			'dashicons-lightbulb' => 'dashicons-lightbulb',
			'dashicons-list-view' => 'dashicons-list-view',
			'dashicons-location-alt' => 'dashicons-location-alt',
			'dashicons-location' => 'dashicons-location',
			'dashicons-lock' => 'dashicons-lock',
			'dashicons-marker' => 'dashicons-marker',
			'dashicons-media-archive' => 'dashicons-media-archive',
			'dashicons-media-audio' => 'dashicons-media-audio',
			'dashicons-media-code' => 'dashicons-media-code',
			'dashicons-media-default' => 'dashicons-media-default',
			'dashicons-media-document' => 'dashicons-media-document',
			'dashicons-media-interactive' => 'dashicons-media-interactive',
			'dashicons-media-spreadsheet' => 'dashicons-media-spreadsheet',
			'dashicons-media-text' => 'dashicons-media-text',
			'dashicons-media-video' => 'dashicons-media-video',
			'dashicons-megaphone' => 'dashicons-megaphone',
			'dashicons-menu-alt' => 'dashicons-menu-alt',
			'dashicons-menu' => 'dashicons-menu',
			'dashicons-microphone' => 'dashicons-microphone',
			'dashicons-migrate' => 'dashicons-migrate',
			'dashicons-minus' => 'dashicons-minus',
			'dashicons-money' => 'dashicons-money',
			'dashicons-move' => 'dashicons-move',
			'dashicons-nametag' => 'dashicons-nametag',
			'dashicons-networking' => 'dashicons-networking',
			'dashicons-no-alt' => 'dashicons-no-alt',
			'dashicons-no' => 'dashicons-no',
			'dashicons-palmtree' => 'dashicons-palmtree',
			'dashicons-paperclip' => 'dashicons-paperclip',
			'dashicons-performance' => 'dashicons-performance',
			'dashicons-phone' => 'dashicons-phone',
			'dashicons-playlist-audio' => 'dashicons-playlist-audio',
			'dashicons-playlist-video' => 'dashicons-playlist-video',
			'dashicons-plus-alt' => 'dashicons-plus-alt',
			'dashicons-plus-light' => 'dashicons-plus-light',
			'dashicons-plus' => 'dashicons-plus',
			'dashicons-portfolio' => 'dashicons-portfolio',
			'dashicons-post-status' => 'dashicons-post-status',
			'dashicons-pressthis' => 'dashicons-pressthis',
			'dashicons-products' => 'dashicons-products',
			'dashicons-randomize' => 'dashicons-randomize',
			'dashicons-redo' => 'dashicons-redo',
			'dashicons-rss' => 'dashicons-rss',
			'dashicons-saved' => 'dashicons-saved',
			'dashicons-schedule' => 'dashicons-schedule',
			'dashicons-screenoptions' => 'dashicons-screenoptions',
			'dashicons-search' => 'dashicons-search',
			'dashicons-share-alt' => 'dashicons-share-alt',
			'dashicons-share-alt2' => 'dashicons-share-alt2',
			'dashicons-share' => 'dashicons-share',
			'dashicons-shield-alt' => 'dashicons-shield-alt',
			'dashicons-shield' => 'dashicons-shield',
			'dashicons-slides' => 'dashicons-slides',
			'dashicons-smartphone' => 'dashicons-smartphone',
			'dashicons-smiley' => 'dashicons-smiley',
			'dashicons-sort' => 'dashicons-sort',
			'dashicons-sos' => 'dashicons-sos',
			'dashicons-star-empty' => 'dashicons-star-empty',
			'dashicons-star-filled' => 'dashicons-star-filled',
			'dashicons-star-half' => 'dashicons-star-half',
			'dashicons-sticky' => 'dashicons-sticky',
			'dashicons-store' => 'dashicons-store',
			'dashicons-tablet' => 'dashicons-tablet',
			'dashicons-tag' => 'dashicons-tag',
			'dashicons-tagcloud' => 'dashicons-tagcloud',
			'dashicons-testimonial' => 'dashicons-testimonial',
			'dashicons-text' => 'dashicons-text',
			'dashicons-thumbs-down' => 'dashicons-thumbs-down',
			'dashicons-thumbs-up' => 'dashicons-thumbs-up',
			'dashicons-tickets-alt' => 'dashicons-tickets-alt',
			'dashicons-tickets' => 'dashicons-tickets',
			'dashicons-translation' => 'dashicons-translation',
			'dashicons-trash' => 'dashicons-trash',
			'dashicons-twitter' => 'dashicons-twitter',
			'dashicons-undo' => 'dashicons-undo',
			'dashicons-universal-access-alt' => 'dashicons-universal-access-alt',
			'dashicons-universal-access' => 'dashicons-universal-access',
			'dashicons-unlock' => 'dashicons-unlock',
			'dashicons-update' => 'dashicons-update',
			'dashicons-upload' => 'dashicons-upload',
			'dashicons-vault' => 'dashicons-vault',
			'dashicons-video-alt' => 'dashicons-video-alt',
			'dashicons-video-alt2' => 'dashicons-video-alt2',
			'dashicons-video-alt3' => 'dashicons-video-alt3',
			'dashicons-visibility' => 'dashicons-visibility',
			'dashicons-warning' => 'dashicons-warning',
			'dashicons-welcome-add-page' => 'dashicons-welcome-add-page',
			'dashicons-welcome-comments' => 'dashicons-welcome-comments',
			'dashicons-welcome-learn-more' => 'dashicons-welcome-learn-more',
			'dashicons-welcome-view-site' => 'dashicons-welcome-view-site',
			'dashicons-welcome-widgets-menus' => 'dashicons-welcome-widgets-menus',
			'dashicons-welcome-write-blog' => 'dashicons-welcome-write-blog',
			'dashicons-wordpress-alt' => 'dashicons-wordpress-alt',
			'dashicons-wordpress' => 'dashicons-wordpress',
			'dashicons-yes' => 'dashicons-yes'
		);
		return $animate_css_animation_list;
	}
}

if(!function_exists('medicale_mascot_orderby_parameters_list')) {
	/**
	 * Orderby Parameters list
	 */
	function medicale_mascot_orderby_parameters_list() {
		$orderby_parameters_list = array(
			esc_html__( 'Date', 'medicale-wp' ) 				=> 'date',
			esc_html__( 'Post Name', 'medicale-wp' ) 			=> 'name',
			esc_html__( 'Random Order', 'medicale-wp' ) 		=> 'rand',
			esc_html__( 'Last Modified Date', 'medicale-wp' ) 	=> 'modified',
			esc_html__( 'Author', 'medicale-wp' ) 				=> 'author',
			esc_html__( 'Title', 'medicale-wp' ) 				=> 'title',
			esc_html__( 'ID', 'medicale-wp' ) 					=> 'ID',
			esc_html__( 'Post/Page Parent ID', 'medicale-wp' ) 	=> 'parent',
			esc_html__( 'Number of Comments', 'medicale-wp' ) 	=> 'comment_count',
			esc_html__( 'Page Order', 'medicale-wp' ) 			=> 'menu_order'
		);
		return $orderby_parameters_list;
	}
}

if(!function_exists('medicale_mascot_list_hover_effects')) {
	/**
	 * Hover Effect list
	 */
	function medicale_mascot_list_hover_effects() {
		$hover_effects_list = array(
			esc_html__( 'Default', 'medicale-wp' )			=> '',
			esc_html__( 'Effect London', 'medicale-wp' )	=> 'effect-london',
			esc_html__( 'Effect Rome', 'medicale-wp' )		=> 'effect-rome',
			esc_html__( 'Effect Paris', 'medicale-wp' )		=> 'effect-paris',
			esc_html__( 'Effect Barlin', 'medicale-wp' )	=> 'effect-barlin'
		);
		return $hover_effects_list;
	}
}

if(!function_exists('medicale_mascot_portfolio_gutter_list')) {
	/**
	 * Portfolio Gutter list
	 */
	function medicale_mascot_portfolio_gutter_list() {
		$gutter_list = array(
			esc_html__( 'Default', 'medicale-wp' )  	=>  'gutter',
			esc_html__( 'No Gutter', 'medicale-wp' ) =>  '',
			'2px'  		=>  'gutter-small',
			'30px'  	=>  'gutter-30',
			'40px'  	=>  'gutter-40',
			'50px'  	=>  'gutter-50',
			'60px'  	=>  'gutter-60',
		);
		return $gutter_list;
	}
}

if(!function_exists('medicale_mascot_different_size_list')) {
	/**
	 * Size list
	 */
	function medicale_mascot_different_size_list() {
		$size_list = array(
			''  	=>  '',
			'xl'  	=>  'xl',
			'lg'  	=>  'lg',
			'md'  	=>  'md',
			'sm'  	=>  'sm',
			'xs'  	=>  'xs',
		);
		return $size_list;
	}
}

if(!function_exists('medicale_mascot_text_alignment_list')) {
	/**
	 * Text Alignment List
	 */
	function medicale_mascot_text_alignment_list() {
		$alignment_list = array(
			esc_html__( 'Text Center', 'medicale-wp' ) 	=> 'text-center',
			esc_html__( 'Text Right', 'medicale-wp' ) 	=> 'text-right flip',
			esc_html__( 'Text Left', 'medicale-wp' ) 	=> 'text-left flip'
		);
		return $alignment_list;
	}
}

if(!function_exists('medicale_mascot_redux_text_alignment_list')) {
	/**
	 * Text Alignment List - Redux
	 */
	function medicale_mascot_redux_text_alignment_list() {
		$alignment_list = array(
			'text-left flip'	 	=> esc_html__( 'Left', 'medicale-wp' ),
			'text-center'   		=> esc_html__( 'Center', 'medicale-wp' ),
			'text-right flip'		=> esc_html__( 'Right', 'medicale-wp' ),
		);
		return $alignment_list;
	}
}

if(!function_exists('medicale_mascot_heading_tag_list')) {
	/**
	 * Heading Tag List
	 */
	function medicale_mascot_heading_tag_list() {
		$heading_tag_list = array(
			''  	=>  '',
			'h2' => 'h2',
			'h3' => 'h3',
			'h4' => 'h4',
			'h5' => 'h5',
			'h6' => 'h6',
		);
		return $heading_tag_list;
	}
}

if(!function_exists('medicale_mascot_heading_tag_list_all')) {
	/**
	 * Heading Tag List
	 */
	function medicale_mascot_heading_tag_list_all() {
		$heading_tag_list_all = array(
			'h1' => 'h1',
			'h2' => 'h2',
			'h3' => 'h3',
			'h4' => 'h4',
			'h5' => 'h5',
			'h6' => 'h6',
		);
		return $heading_tag_list_all;
	}
}

if(!function_exists('medicale_mascot_open_link_in')) {
	/**
	 * Open Link In
	 */
	function medicale_mascot_open_link_in() {
		$open_link_in = array(
			esc_html__( 'New Window', 'medicale-wp' )   => '_blank',
			esc_html__( 'Same Window', 'medicale-wp' )  => '_self'
		);
		return $open_link_in;
	}
}

if(!function_exists('medicale_mascot_vc_font_style_list')) {
	/**
	 * Font Style List
	 */
	function medicale_mascot_vc_font_style_list() {
		$font_style_list = array(
			esc_html__( 'Normal', 'medicale-wp' )   => '',
			esc_html__( 'Italic', 'medicale-wp' )  	=> 'italic'
		);
		return $font_style_list;
	}
}

if(!function_exists('medicale_mascot_vc_font_weight_list')) {
	/**
	 * Font weight List
	 */
	function medicale_mascot_vc_font_weight_list() {
		$font_weight_list = array(
			esc_html__( 'Default', 'medicale-wp' )   => '',
			'100'   => '100',
			'200'   => '200',
			'300'   => '300',
			'400'   => '400',
			'500'   => '500',
			'600'   => '600',
			'700'   => '700',
			'800'   => '800',
		);
		return $font_weight_list;
	}
}

if(!function_exists('medicale_mascot_vc_text_transform_list')) {
	/**
	 * Text Transform List
	 */
	function medicale_mascot_vc_text_transform_list() {
		$text_transform_list = array(
			esc_html__( 'Default', 'medicale-wp' )   	=> '',
			esc_html__( 'None', 'medicale-wp' )  		=> 'none',
			esc_html__( 'Capitalize', 'medicale-wp' )  	=> 'capitalize',
			esc_html__( 'Uppercase', 'medicale-wp' )  	=> 'uppercase',
			esc_html__( 'Lowercase', 'medicale-wp' )  	=> 'lowercase',
			esc_html__( 'Initial', 'medicale-wp' )  	=> 'initial',
			esc_html__( 'Inherit', 'medicale-wp' )  	=> 'inherit'
		);
		return $text_transform_list;
	}
}

if(!function_exists('medicale_mascot_get_post_all_categories_array')) {
	/**
	 * Category List of Blog Posts as an Array
	 */
	function medicale_mascot_get_post_all_categories_array() {
		$categories = get_categories( array(
			'orderby' => 'name',
			'order'   => 'ASC'
		) );
		$cats = array();
		$cats[''] = 'All';
		foreach($categories as $cat){
			$cats[$cat->term_id] = $cat->name;
		}
		return $cats;
	}
}

if(!function_exists('medicale_mascot_get_page_list_array')) {
	/**
	 * Category List of Pages as an Array
	 */
	function medicale_mascot_get_page_list_array() {
		$all_pages = get_pages();
		$pages = array();
		foreach($all_pages as $each_page){
			$pages[$each_page->ID] = $each_page->post_title;
		}
		return $pages;
	}
}

if ( ! function_exists( 'medicale_mascot_metabox_get_list_of_predefined_theme_color_css_files' ) ) {
	/**
	 * Get list of Predefined Theme Color css files
	 */
	function medicale_mascot_metabox_get_list_of_predefined_theme_color_css_files() {
		$predefined_theme_colors = array();

		if( $handle = opendir( MASCOT_TEMPLATE_DIR . '/assets/css/colors/' ) ) {
			while( false !== ($entry = readdir($handle)) ) {
				if( $entry != "." && $entry != ".." ) {
					$predefined_theme_colors[$entry] = $entry;
				}
			}
			closedir($handle);
		}
		return $predefined_theme_colors;
	}
}


if ( ! function_exists( 'medicale_mascot_get_available_image_sizes' ) ) {
	/**
	 * Get information about available image sizes
	 */
	function medicale_mascot_get_available_image_sizes( $size = '' ) {
	
		global $_wp_additional_image_sizes;
	
		$sizes = array();
		$get_intermediate_image_sizes = get_intermediate_image_sizes();
	
		// Create the full array with sizes and crop info
		foreach( $get_intermediate_image_sizes as $_size ) {
			if ( in_array( $_size, array( 'thumbnail', 'medium', 'large' ) ) ) {
				$sizes[ $_size ]['width'] = get_option( $_size . '_size_w' );
				$sizes[ $_size ]['height'] = get_option( $_size . '_size_h' );
				$sizes[ $_size ]['crop'] = (bool) get_option( $_size . '_crop' );
			} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
				$sizes[ $_size ] = array( 
					'width' => $_wp_additional_image_sizes[ $_size ]['width'],
					'height' => $_wp_additional_image_sizes[ $_size ]['height'],
					'crop' =>  $_wp_additional_image_sizes[ $_size ]['crop']
				);
			}
		}
	
		// Get only 1 size if found
		if ( $size ) {
			if( isset( $sizes[ $size ] ) ) {
				return $sizes[ $size ];
			} else {
				return false;
			}
		}
		return $sizes;
	}
}


if ( ! function_exists( 'medicale_mascot_get_available_image_sizes_for_vc' ) ) {
	/**
	 * Get information about available image sizes for VC
	 */
	function medicale_mascot_get_available_image_sizes_for_vc() {
		$size = array();
		$available_image_sizes = medicale_mascot_get_available_image_sizes();
	
		// Create the full array with sizes and crop info
		foreach( $available_image_sizes as $key => $value ) {
			$sizes[ $value['width'] . 'x' . $value['height'] . ( ($value['crop'] == 1) ? ' - cropped' : '') ] = $key;
		}
		return $sizes;
	}
}