<?php

/* Loads all shortcodes located in shortcodes folder
================================================== */
if( !function_exists('medicale_mascot_load_all_shortcodes') ) {
	function medicale_mascot_load_all_shortcodes() {
		foreach( glob( MASCOT_FRAMEWORK_DIR.'/core/shortcodes/parts/*/loader.php' ) as $each_sc_loader ) {
			require_once $each_sc_loader;
		}
	}
	add_action('medicale_mascot_before_custom_action', 'medicale_mascot_load_all_shortcodes');
}
