<div class="progress-item <?php echo $custom_css_class;?>">
	<div class="progress-title">
		<<?php echo $title_tag;?>><?php echo $title;?></<?php echo $title_tag;?>>
	</div>
	<div class="progress" <?php echo $progress_inline_css;?>>
		<div class="progress-bar" <?php echo $progress_color;?> data-percent="<?php echo $percentage_value;?>" data-unit="<?php echo $unit_symbol;?>"></div>
	</div>
</div>