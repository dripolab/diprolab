
		<div class="section-title <?php echo $text_alignment;?> <?php echo $custom_css_class;?>">
			<div class="row">
			<div class="col-md-12">
				<<?php echo $title_tag;?> class="title <?php if( $title_wow_animation != '' ) { echo $title_wow_class;}?>" <?php if( $title_wow_animation != '' ) { echo $title_wow_duration.' '.$title_wow_delay;}?> <?php echo $inline_css;?>><?php echo $title_text;?></<?php echo $title_tag;?>>
				<div class="title-icon">
				<img class="mb-10" src="<?php echo MASCOT_ASSETS_URI; ?>/images/title-icon.png" alt="">
				</div>
				<p class="subtitle <?php if( $subtitle_wow_animation != '' ) { echo $subtitle_wow_class;}?>" <?php if( $subtitle_wow_animation != '' ) { echo $subtitle_wow_duration.' '.$subtitle_wow_delay;}?>><?php echo $subtitle_paragraph;?></p>
			</div>
			</div>
		</div>