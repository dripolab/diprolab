<?php

$days = array(
	'monday' 	=> esc_html__('Monday', 'medicale-wp'),
	'tuesday' 	=> esc_html__('Tuesday', 'medicale-wp'), 
	'wednesday' => esc_html__('Wednesday', 'medicale-wp'),
	'thursday' 	=> esc_html__('Thursday', 'medicale-wp'), 
	'friday' 	=> esc_html__('Friday', 'medicale-wp'), 
	'saturday' 	=> esc_html__('Saturday', 'medicale-wp'),
	'sunday' 	=> esc_html__('Sunday', 'medicale-wp'),
);
?>
<ul class="opening-hours <?php echo $border_color?>">
<?php 
  // set date variables
  $current_day = strtolower(date('l'));

  foreach( $days as $key => $day ) {
	$day_time = 'day_'.$key;
?>
  <li class="clearfix <?php if( $current_day == $key ) echo "active"; ?>">
	<span><?php echo $day; ?></span>
	<div class="value"><?php echo $$day_time ?></div>
  </li>
<?php 
  }
?>
</ul>