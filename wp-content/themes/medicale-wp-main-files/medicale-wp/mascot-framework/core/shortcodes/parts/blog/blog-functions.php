<?php

if (!function_exists('medicale_mascot_shortcode_get_blog_post_format')) {
	/**
	 * Return Shortcode Blog Post Format HTML
	 */
	function medicale_mascot_shortcode_get_blog_post_format( $post_format = '', $params = array() ) {

		$format = $post_format ? : 'standard';
		$params['post_format'] = $format;

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters)
		$html = medicale_mascot_get_shortcode_template_part( 'post-format', $format, 'blog/tpl/post-format', $params, true );
		return $html;
	}
}