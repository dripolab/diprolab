<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <div class="entry-header">
	<?php medicale_mascot_get_post_thumbnail( $post_format ); ?>
  </div>
  <div class="entry-content">
	<?php medicale_mascot_post_meta(); ?>
	<?php medicale_mascot_get_post_title(); ?>
	<div class="post-excerpt">
		<?php medicale_mascot_get_excerpt(); ?>
	</div>
	<div class="post-btn-readmore">
		<a href="<?php the_permalink(); ?>" class="btn-read-more"><?php esc_html_e( 'Read more', 'medicale-wp' ); ?></a>
	</div>
	<div class="clearfix"></div>
  </div>
</article>