<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <?php if ( $show_featured_image == 'true' ) : ?>
  <div class="entry-header">
	<?php medicale_mascot_get_post_thumbnail( $post_format ); ?>
  </div>
  <?php endif; ?>
  
  <div class="entry-content">
	<?php if ( $show_post_meta == 'true' ) : ?>
	<?php medicale_mascot_post_meta(); ?>
	<?php endif; ?>

	<?php if ( $show_title == 'true' ) : ?>
	<?php medicale_mascot_get_post_title(); ?>
	<?php endif; ?>

	<?php if ( $show_description == 'true' ) : ?>
	<div class="post-excerpt">
		<?php medicale_mascot_get_excerpt(); ?>
	</div>
	<?php endif; ?>

	<?php if ( $show_read_more_button == 'true' ) : ?>
	<?php medicale_mascot_get_shortcode_template_part( 'button', null, 'blog/tpl/post-format', $params, false );?>
	<?php endif; ?>

	<div class="clearfix"></div>
  </div>
</article>