<?php

if(!function_exists('mascot_core_sc_section_title_vc_map_modifier')) {
	/**
	 * Shortcodes vc_map modifier
	 */
	function mascot_core_sc_section_title_vc_map_modifier( $shortcode_base ) {
		//vc_remove_param( $shortcode_base, "display_type" );
		//vc_add_param( $shortcode_base, $attributes );
		//vc_add_params( $shortcode_base, $attributes );
	}
}



if(!function_exists('mascot_core_sc_section_title_render')) {
	/**
	 * Renders shortcodes HTML
	 */
	function mascot_core_sc_section_title_render( $attr, $content = null ) {

		$args = array(
			'custom_css_class' => '',
			'design_style' => 'style1',
			'text_alignment' => 'text-center',
			'title_text' => '',
			'subtitle_paragraph' => '',

			'title_tag' => 'h2',
			'title_text_color' => '',
			'title_font_size' => '',
			'title_line_height' => '',
			'title_letter_spacing' => '',
			'title_font_style' => '',
			'title_font_weight' => '',
			'title_text_transform' => '',

			'title_wow_animation' => '',
			'title_wow_animation_duration' => '',
			'title_wow_animation_delay' => '',

			'subtitle_wow_animation' => '',
			'subtitle_wow_animation_duration' => '',
			'subtitle_wow_animation_delay' => '',
		);

		$params = shortcode_atts($args, $attr);

		$params['inline_css'] = medicale_mascot_get_inline_css( mascot_core_sc_section_title_css( $params ) );

		//wow animation title
		if( $params['title_wow_animation'] != '' ) {
			$params['title_wow_class'] = 'wow ' . $params['title_wow_animation'];
			$params['title_wow_duration'] = medicale_mascot_get_inline_attributes( $params['title_wow_animation_duration'], 'data-wow-duration' );
			$params['title_wow_delay'] = medicale_mascot_get_inline_attributes( $params['title_wow_animation_delay'], 'data-wow-delay' );
		}
		//wow animation subtitle
		if( $params['subtitle_wow_animation'] != '' ) {
			$params['subtitle_wow_class'] = 'wow ' . $params['subtitle_wow_animation'];
			$params['subtitle_wow_duration'] = medicale_mascot_get_inline_attributes( $params['subtitle_wow_animation_duration'], 'data-wow-duration' );
			$params['subtitle_wow_delay'] = medicale_mascot_get_inline_attributes( $params['subtitle_wow_animation_delay'], 'data-wow-delay' );
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, shortcode_ob_start)
		$html = medicale_mascot_get_shortcode_template_part( 'section-title-' . $params['design_style'], null, 'section-title/tpl', $params, true );
		
		return $html;
	}
}

if(!function_exists('mascot_core_sc_section_title_css')) {
	/**
	 * Get Title Styles
	 */
	function mascot_core_sc_section_title_css( $params ) {
		$css_array = array();

		if( $params['title_text_color'] != '' ) {
			$css_array[] = 'color: '.$params['title_text_color'];
		}
		if( $params['title_font_size'] != '' ) {
			$font_size = medicale_mascot_remove_suffix( $params['title_font_size'], 'px');
			$css_array[] = 'font-size: '.$font_size.'px';
		}
		if( $params['title_line_height'] != '' ) {
			$font_size = medicale_mascot_remove_suffix( $params['title_line_height'], 'px');
			$css_array[] = 'line-height: '.$font_size.'px';
		}
		if( $params['title_letter_spacing'] != '' ) {
			$font_size = medicale_mascot_remove_suffix( $params['title_letter_spacing'], 'px');
			$css_array[] = 'letter-spacing: '.$font_size.'px';
		}
		if( $params['title_font_style'] != '' ) {
			$css_array[] = 'font-style: '.$params['title_font_style'];
		}
		if( $params['title_font_weight'] != '' ) {
			$css_array[] = 'font-weight: '.$params['title_font_weight'];
		}
		if( $params['title_text_transform'] != '' ) {
			$css_array[] = 'text-transform: '.$params['title_text_transform'];
		}

		return implode( '; ', $css_array );
	}
}