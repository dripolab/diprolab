<?php
if( $columns == 1 ) {
  $col_layout = 12;
  $col_layout_class = "col-md-12";
  $equal_height_class = "";
} else if( $columns == 2 ) {
  $col_layout = 6;
  $col_layout_class = "col-sm-6 col-md-6";
  $equal_height_class = "equal-height";
} else if( $columns == 3 ) {
  $col_layout = 4;
  $col_layout_class = "col-sm-6 col-md-4";
  $equal_height_class = "equal-height";
} else {
  $col_layout = 3;
  $col_layout_class = "col-sm-6 col-md-3";
  $equal_height_class = "equal-height";
}
?>

<?php if ( $the_query->have_posts() ) : ?>
  <div class="row <?php echo $custom_css_class;?> <?php echo $equal_height_class;?>">
	<!-- the loop -->
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<div class="<?php echo $col_layout_class;?>">
		<?php
			echo medicale_mascot_shortcode_get_blog_post_format( get_post_format(), $params_array );
		?>
		</div>
	<?php endwhile; ?>
	<!-- end of the loop -->
  </div>
  <?php wp_reset_postdata(); ?>

<?php else : ?>
  <p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'medicale-wp' ); ?></p>
<?php endif; ?>