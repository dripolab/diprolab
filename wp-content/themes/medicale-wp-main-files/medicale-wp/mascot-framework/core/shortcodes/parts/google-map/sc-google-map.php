<?php

if(!function_exists('mascot_core_sc_google_map_vc_map_modifier')) {
	/**
	 * Shortcodes vc_map modifier
	 */
	function mascot_core_sc_google_map_vc_map_modifier( $shortcode_base ) {
		//vc_remove_param( $shortcode_base, "display_type" );
		//vc_add_param( $shortcode_base, $attributes );
		//vc_add_params( $shortcode_base, $attributes );
	}
}



if(!function_exists('mascot_core_sc_google_map_render')) {
	/**
	 * Renders shortcodes HTML
	 */
	function mascot_core_sc_google_map_render( $attr, $content = null ) {
		
		$args = array(
			'custom_css_class' => '',
			'google_map_style' => 'greyscale1',
			'lat' => '-37.817314',
			'long' => '144.955431',
			'zoom' => '10',
			'height' => '300',
			'marker_url' => '',
			'marker_text_title' => 'Envato',
			'marker_text_desc' => 'The world\'s leading marketplace and community for creative assets and creative people.', 
		);

		$params = shortcode_atts($args, $attr);

		//Enque Google Map Scripts
		wp_enqueue_script( array( 'google-map-api', 'google-map-init' ) );

		//marker url
		if( !empty( $params['marker_url'] ) ) {
			$image  = wp_get_attachment_image_src($params['marker_url'], 'large');
			$params['marker_url'] = $image[0];
		} else {
			$params['marker_url'] = MASCOT_ASSETS_URI . '/images/map-marker1.png';
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, shortcode_ob_start)
		$html = medicale_mascot_get_shortcode_template_part( 'google-map', null, 'google-map/tpl', $params, true );
		
		return $html;
	}
}