<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <div class="entry-content">
  	<?php medicale_mascot_get_post_thumbnail( $post_format ); ?>
	<div class="clearfix"></div>
  </div>
</article>