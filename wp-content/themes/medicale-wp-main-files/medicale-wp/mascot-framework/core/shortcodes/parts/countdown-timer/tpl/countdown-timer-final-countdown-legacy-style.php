<?php
  $random_number = wp_rand( 111111, 999999 );
?>
<div class="final-countdown final-countdown-legacy-style">
  <span id="final-countdown-clock-<?php echo $random_number.'-'.get_the_ID();?>" data-future-date="<?php echo $countdown_future_date_time;?>" data-showtime="<?php echo $show_time;?>"></span>
</div>