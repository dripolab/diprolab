
	<div class="funfact text-center <?php echo $custom_css_class;?>">
		<?php medicale_mascot_get_shortcode_template_part( 'icon-type', $icon_type, 'funfact-counter/tpl', $params, false );?>
		<<?php echo $counter_tag;?> class="animate-number" data-value="<?php echo $counter_range;?>" <?php echo $animation_duration;?> <?php echo $counter_inline_css;?>>0</<?php echo $counter_tag;?>>
		<<?php echo $title_tag;?> class="title" <?php echo $title_inline_css;?>><?php echo $title;?></<?php echo $title_tag;?>>
	</div>