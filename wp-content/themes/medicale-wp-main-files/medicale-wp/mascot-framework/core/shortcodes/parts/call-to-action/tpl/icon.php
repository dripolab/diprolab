
			<div class="<?php echo $icon_alignment; ?>">
				<a href="<?php echo $icon_link; ?>" target="<?php echo $icon_target; ?>">
					<i class="<?php echo ${$icon_pack};?>" style="<?php if ( $icon_font_size != "" ) echo 'font-size: ' . $icon_font_size . ';'; ?><?php if ( $icon_color != "" ) echo 'color: ' . $icon_color . ';'; ?>"></i>
				</a>
			</div>