
	<div class="post-btn-readmore">
		<a href="<?php the_permalink(); ?>" class="btn <?php echo $design_style;?> <?php echo $button_size;?> <?php if( $btn_block ) { echo 'btn-block'; }?> <?php if( $threed_effect ) { echo 'btn-3d'; }?> <?php if( $btn_theme_colored ) { echo 'btn-theme-colored'; }?>"><?php esc_html_e( 'Read more', 'medicale-wp' ); ?></a>
	</div>