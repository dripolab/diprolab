<?php

if(!function_exists('mascot_core_sc_social_list_vc_map_modifier')) {
	/**
	 * Shortcodes vc_map modifier
	 */
	function mascot_core_sc_social_list_vc_map_modifier( $shortcode_base ) {
		//vc_remove_param( $shortcode_base, "display_type" );
		//vc_add_param( $shortcode_base, $attributes );
		//vc_add_params( $shortcode_base, $attributes );
	}
}



if(!function_exists('mascot_core_sc_social_list_render')) {
	/**
	 * Renders shortcodes HTML
	 */
	function mascot_core_sc_social_list_render( $attr, $content = null ) {
		
		$args = array(
			'content_in_grid' => '',
			'grid_size' => '',
			'visual_style' => '',

			'show_button' => '',
			'button_0_position' => '',
			'button_0_size' => '',
			'button_0_text' => 'Read More',
			'button_0_link' => '#',
			'button_0_target' => '',

			'icon_pack' => '',
			'icon_link' => '',
			'icon_target' => '',
			'icon_placement' => '',
			'icon_font_size' => '30px',
			'icon_skin' => '',

			'box_padding' => '30px',
			'text_font_size' => '',
			'space_below_text' => '',
			'button_position' => '',
			
			'button_1_size' => '',
			'button_1_text' => 'Read More',
			'button_1_link' => '#',
			'button_1_target' => '',

			'button_2_size' => '',
			'button_2_text' => 'Read More',
			'button_2_link' => '#',
			'button_2_target' => '',
			
			'content' => ''
		);

		$params = shortcode_atts($args, $attr);

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, shortcode_ob_start)
		$html = medicale_mascot_get_shortcode_template_part( 'social-list', $params['visual_style'], 'social-list/tpl', $params, true );
		
		return $html;
	}
}