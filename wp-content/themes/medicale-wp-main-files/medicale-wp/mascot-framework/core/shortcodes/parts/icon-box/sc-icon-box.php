<?php

if(!function_exists('mascot_core_sc_icon_box_vc_map_modifier')) {
	/**
	 * Shortcodes vc_map modifier
	 */
	function mascot_core_sc_icon_box_vc_map_modifier( $shortcode_base ) {
		//vc_remove_param( $shortcode_base, "display_type" );
		//vc_add_param( $shortcode_base, $attributes );
		//vc_add_params( $shortcode_base, $attributes );
	}
}




if(!function_exists('mascot_core_sc_icon_box_render')) {
	/**
	 * Renders shortcodes HTML
	 */
	function mascot_core_sc_icon_box_render( $attr, $content = null ) {

		$args = array(
			'custom_css_class' => '',
			'title' => 'Sample Title',
			'icon' => 'fa fa-diamond',
			'link_icon_title' => false,
			'link_url' => '',
			'content' => $content,
			'text_alignment' => '',
			'title_tag' => 'h5',
			'icon_position' => 'icon-top',
			'icon_size' => '',
			'icon_color' => '',
			'icon_style' => '',
			'icon_border_style' => '',
			'icon_theme_colored' => false, 
			'show_read_more_button' => false, 
		);

		$params = shortcode_atts($args, $attr);
		
		//link url
		$href = vc_build_link( $params['link_url'] );
		$target = '';
		if(isset($href['target']) && $href['target'] != ''){
			$target = 'target="'.trim($href['target']).'"';
		}
		$params['target'] = $target;
		$params['url'] = $href['url'];

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, shortcode_ob_start)
		$html = medicale_mascot_get_shortcode_template_part( 'icon-box-' . $params['icon_position'], null, 'icon-box/tpl', $params, true );
		
		return $html;
	}
}