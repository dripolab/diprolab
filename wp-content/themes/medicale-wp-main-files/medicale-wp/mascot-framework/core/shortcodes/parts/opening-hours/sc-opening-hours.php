<?php

if(!function_exists('mascot_core_sc_opening_hours_vc_map_modifier')) {
	/**
	 * Shortcodes vc_map modifier
	 */
	function mascot_core_sc_opening_hours_vc_map_modifier( $shortcode_base ) {
		//vc_remove_param( $shortcode_base, "display_type" );
		//vc_add_param( $shortcode_base, $attributes );
		//vc_add_params( $shortcode_base, $attributes );
	}
}



if(!function_exists('mascot_core_sc_opening_hours_render')) {
	/**
	 * Renders shortcodes HTML
	 */
	function mascot_core_sc_opening_hours_render( $attr, $content = null ) {

		$args = array(
			'custom_css_class' => '',
			'border_color' => '',

			'day_monday' => '',
			'day_tuesday' => '',
			'day_wednesday' => '',
			'day_thursday' => '',
			'day_friday' => '',
			'day_saturday' => '',
			'day_sunday' => '',

		);

		$params = shortcode_atts($args, $attr);

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, shortcode_ob_start)
		$html = medicale_mascot_get_shortcode_template_part( 'opening-hours', null, 'opening-hours/tpl', $params, true );
		
		return $html;
	}
}