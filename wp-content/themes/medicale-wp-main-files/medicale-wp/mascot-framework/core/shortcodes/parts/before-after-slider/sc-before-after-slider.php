<?php

if(!function_exists('mascot_core_sc_before_after_slider_vc_map_modifier')) {
	/**
	 * Shortcodes vc_map modifier
	 */
	function mascot_core_sc_before_after_slider_vc_map_modifier( $shortcode_base ) {
		//vc_remove_param( $shortcode_base, "display_type" );
		//vc_add_param( $shortcode_base, $attributes );
		//vc_add_params( $shortcode_base, $attributes );
	}
}



if(!function_exists('mascot_core_sc_before_after_slider_render')) {
	/**
	 * Renders shortcodes HTML
	 */
	function mascot_core_sc_before_after_slider_render( $attr, $content = null ) {

		$args = array(
			'custom_css_class' => '',

			'before_image' => '',
			'after_image' => '',
			'orientation' => 'horizontal',
			'before_label' => 'Before',
			'after_label' => 'After',
			'default_offset_pct' => '0.5',
			'no_overlay' => 'true',
		);

		$params = shortcode_atts($args, $attr);

		wp_enqueue_style( array( 'twentytwenty' ) );
		wp_enqueue_script( array( 'twentytwenty-event-move', 'twentytwenty' ) );
		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, shortcode_ob_start)
		$html = medicale_mascot_get_shortcode_template_part( 'before-after-slider', null, 'before-after-slider/tpl', $params, true );
		
		return $html;
	}
}