<div class="row">
	<div class="call-to-action">

		<?php if( $content_in_grid == "no" ) { ?>
		<div class="col-md-12">
		<?php } elseif ( $content_in_grid == "yes" ) { ?>

			<?php medicale_mascot_get_shortcode_template_part( 'grid-left', null, 'call-to-action/tpl', $params, false ); ?>

		<?php } //end $content_in_grid == "yes" ?>

			<?php medicale_mascot_get_shortcode_template_part( 'content', null, 'call-to-action/tpl', $params, false ); ?>
		</div>




		<?php if( $content_in_grid == "no" ) { ?>
		<div class="col-md-12">
		<?php } elseif ( $content_in_grid == "yes" ) { ?>

			<?php medicale_mascot_get_shortcode_template_part( 'grid-right', null, 'call-to-action/tpl', $params, false ); ?>

		<?php } //end $content_in_grid == "yes" ?>
			<?php if( $show_button == "yes" ) { ?>
			<div class="<?php echo $button_0_position; ?>">
				<a class="btn cta-btn <?php echo $button_0_design_style; ?><?php if ( $button_0_btn_theme_colored ) echo ' btn-theme-colored'; ?> <?php echo $button_0_size; ?>" href="<?php echo $button_0_link; ?>" target="<?php echo $button_0_target; ?>"><?php echo $button_0_text; ?></a>
			</div>
			<?php } ?>
		</div>
	</div>
</div>