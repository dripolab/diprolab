<?php if ( $the_query->have_posts() ) : ?>
  <!-- Blog Masonry -->
  <div id="grid" class="gallery-isotope grid-<?php echo $columns;?> masonry gutter-30 clearfix <?php echo $custom_css_class;?>">  
	<div class="gallery-item gallery-item-sizer"></div>
	<!-- the loop -->
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<div class="gallery-item">
		<?php
			echo medicale_mascot_shortcode_get_blog_post_format( get_post_format(), $params_array );
		?>
		</div>
	<?php endwhile; ?>
	<!-- end of the loop -->
  </div>
  <?php wp_reset_postdata(); ?>

<?php else : ?>
  <p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'medicale-wp' ); ?></p>
<?php endif; ?>