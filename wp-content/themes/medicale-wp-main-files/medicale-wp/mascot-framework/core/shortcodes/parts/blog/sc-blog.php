<?php

if(!function_exists('mascot_core_sc_blog_vc_map_modifier')) {
	/**
	 * Shortcodes vc_map modifier
	 */
	function mascot_core_sc_blog_vc_map_modifier( $shortcode_base ) {
		//vc_remove_param( $shortcode_base, "display_type" );
		//vc_add_param( $shortcode_base, $attributes );
		//vc_add_params( $shortcode_base, $attributes );
	}
}



if(!function_exists('mascot_core_sc_blog_render')) {
	/**
	 * Renders shortcodes HTML
	 */
	function mascot_core_sc_blog_render( $attr, $content = null ) {
		
		$args = array(
			'custom_css_class' => '',
			'display_type' => '',
			'columns' => '4',

			'show_navigation' => 'true',
			'show_bullets' => 'true',
			'animation_speed' => '4000',

			'total_items' => '-1',
			'category' => '',
			'order_by' => 'date',
			'order' => 'DESC',

			'show_title' => 'true',
			'show_description'	=> 'true',
			'show_featured_image' => 'true',
			'show_post_meta' => 'true',
			'show_read_more_button' => 'true',

			'design_style' => 'default',
			'button_size' => '',
			'btn_theme_colored' => '',
			'btn_block' => false,
			'threed_effect' => false,
		);
		$params = shortcode_atts($args, $attr);
		$params['params_array'] = $params;
		
		//query args
		$args = array(
			'orderby' => $params['order_by'],
			'order' => $params['order'],
			'posts_per_page' => $params['total_items'],
		);

		//if category selected
		if( $params['category'] ) {
			$args['category_name'] = $params['category'];
		}

		$the_query = new \WP_Query( $args );
		$params['the_query'] = $the_query;

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, shortcode_ob_start)
		$html = medicale_mascot_get_shortcode_template_part( 'blog', $params['display_type'], 'blog/tpl/type', $params, true );
		
		return $html;
	}
}