<?php

if(!function_exists('mascot_core_sc_countdown_timer_vc_map_modifier')) {
	/**
	 * Shortcodes vc_map modifier
	 */
	function mascot_core_sc_countdown_timer_vc_map_modifier( $shortcode_base ) {
		//vc_remove_param( $shortcode_base, "display_type" );
		//vc_add_param( $shortcode_base, $attributes );
		//vc_add_params( $shortcode_base, $attributes );
	}
}



if(!function_exists('mascot_core_sc_countdown_timer_render')) {
	/**
	 * Renders shortcodes HTML
	 */
	function mascot_core_sc_countdown_timer_render( $attr, $content = null ) {
		
		$args = array(
			'custom_css_class' => '',
			'design_style' => 'final-countdown-coupon',
			'countdown_future_date_time' => date("Y/m/d H:i:s", strtotime("+5 week")),
			'show_time' => true,
		);

		$params = shortcode_atts($args, $attr);

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, shortcode_ob_start)
		$html = medicale_mascot_get_shortcode_template_part( 'countdown-timer-' . $params['design_style'], null, 'countdown-timer/tpl', $params, true );
		
		return $html;
	}
}