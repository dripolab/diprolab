<?php

if(!function_exists('mascot_core_sc_progress_bar_vc_map_modifier')) {
	/**
	 * Shortcodes vc_map modifier
	 */
	function mascot_core_sc_progress_bar_vc_map_modifier( $shortcode_base ) {
		//vc_remove_param( $shortcode_base, "display_type" );
		//vc_add_param( $shortcode_base, $attributes );
		//vc_add_params( $shortcode_base, $attributes );
	}
}



if(!function_exists('mascot_core_sc_progress_bar_render')) {
	/**
	 * Renders shortcodes HTML
	 */
	function mascot_core_sc_progress_bar_render( $attr, $content = null ) {
		
		$args = array(
			'custom_css_class' => '',
			'design_style' => 'style1',
			'title' => 'Wordpress',
			'percentage_value' => '85',
			'unit_symbol' => '%',
			'title_tag' => 'h6',
			'progress_color' => '',
			'background_color' => '', 
		);

		$params = shortcode_atts($args, $attr);

		$params['progress_inline_css'] = medicale_mascot_get_inline_css( 'background-color: '.$params['background_color'] );
		$params['progress_color'] = medicale_mascot_get_inline_attributes( $params['progress_color'], 'data-barcolor' );

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, shortcode_ob_start)
		$html = medicale_mascot_get_shortcode_template_part( 'progress-bar-' . $params['design_style'], null, 'progress-bar/tpl', $params, true );
		
		return $html;
	}
}