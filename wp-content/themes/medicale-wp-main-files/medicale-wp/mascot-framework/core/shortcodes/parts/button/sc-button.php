<?php

if(!function_exists('mascot_core_sc_button_vc_map_modifier')) {
	/**
	 * Shortcodes vc_map modifier
	 */
	function mascot_core_sc_button_vc_map_modifier( $shortcode_base ) {
		//vc_remove_param( $shortcode_base, "display_type" );
		//vc_add_param( $shortcode_base, $attributes );
		//vc_add_params( $shortcode_base, $attributes );
	}
}




if(!function_exists('mascot_core_sc_button_render')) {
	/**
	 * Renders shortcodes HTML
	 */
	function mascot_core_sc_button_render( $attr, $content = null ) {

		$args = array(
			'custom_css_class' => '',
			'button_text' => '',
			'link_url' => '#',
			'design_style' => 'default',
			'button_size' => '',
			'btn_theme_colored' => '',
			'btn_block' => false,
			'threed_effect' => false,
			'button_alignment' => '',
			'button_wow_animation' => '',
			'button_wow_animation_duration' => '',

			'add_icon' => false,
			'button_icon_left' => '',
			'button_icon_right' => '', 
		);

		$params = shortcode_atts($args, $attr);
		
		//link url
		$href = vc_build_link( $params['link_url'] );
		$target = '';
		if(isset($href['target']) && $href['target'] != ''){
			$target = 'target="'.trim($href['target']).'"';
		}
		$params['target'] = $target;
		$params['url'] = $href['url'];

		//wow animation
		if( $params['button_wow_animation'] != '' ) {
			$params['button_wow_class'] = 'wow ' . $params['button_wow_animation'];
			$params['button_wow_duration'] = medicale_mascot_get_inline_attributes( $params['button_wow_animation_duration'], 'data-wow-duration' );
		}

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, shortcode_ob_start)
		$html = medicale_mascot_get_shortcode_template_part( 'button', null, 'button/tpl', $params, true );
		
		return $html;
	}
}