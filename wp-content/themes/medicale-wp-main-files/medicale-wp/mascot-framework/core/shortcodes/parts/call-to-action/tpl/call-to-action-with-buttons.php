<div class="row">
	<div class="call-to-action">

		<?php if( $content_in_grid == "no" ) { ?>
		<div class="col-md-12">
		<?php } elseif ( $content_in_grid == "yes" ) { ?>
			<?php medicale_mascot_get_shortcode_template_part( 'grid-left', null, 'call-to-action/tpl', $params, false ); ?>
		<?php } //end $content_in_grid == "yes" ?>
			<?php medicale_mascot_get_shortcode_template_part( 'content', null, 'call-to-action/tpl', $params, false ); ?>
		</div>


		<?php if( $content_in_grid == "no" ) { ?>
		<div class="col-md-12">
		<?php } elseif ( $content_in_grid == "yes" ) { ?>
			<?php medicale_mascot_get_shortcode_template_part( 'grid-right', null, 'call-to-action/tpl', $params, false ); ?>
		<?php } //end $content_in_grid == "yes" ?>
			<div class="<?php echo $button_position; ?>">
				<a class="btn cta-btn <?php echo $button_1_design_style; ?><?php if ( $button_1_btn_theme_colored ) echo ' btn-theme-colored'; ?> <?php echo $button_1_size; ?>" href="<?php echo $button_1_link; ?>" target="<?php echo $button_1_target; ?>"><?php echo $button_1_text; ?></a>
				<a class="btn cta-btn <?php echo $button_2_design_style; ?><?php if ( $button_2_btn_theme_colored ) echo ' btn-theme-colored'; ?> <?php echo $button_2_size; ?>" href="<?php echo $button_2_link; ?>" target="<?php echo $button_2_target; ?>"><?php echo $button_2_text; ?></a>
			</div>
		</div>
	</div>
</div>