<?php

if(!function_exists('mascot_core_sc_call_to_action_vc_map_modifier')) {
	/**
	 * Shortcodes vc_map modifier
	 */
	function mascot_core_sc_call_to_action_vc_map_modifier( $shortcode_base ) {
		//vc_remove_param( $shortcode_base, "display_type" );
		//vc_add_param( $shortcode_base, $attributes );
		//vc_add_params( $shortcode_base, $attributes );
	}
}



if(!function_exists('mascot_core_sc_call_to_action_render')) {
	/**
	 * Renders shortcodes HTML
	 */
	function mascot_core_sc_call_to_action_render( $attr, $content = null ) {
		if( !array_key_exists('icon_pack', $attr) ) {
			$attr['icon_pack'] = 'null';
		}
		
		$args = array(
			'content_in_grid' => '',
			'grid_size' => '',
			'visual_style' => '',

			'show_button' => '',
			'button_0_size' => '',
			'button_0_text' => 'Read More',
			'button_0_link' => '#',
			'button_0_target' => '',

			'button_0_design_style' => '',
			'button_0_size' => '',
			'button_0_btn_theme_colored' => '',
			'button_0_position' => '',

			'icon_pack' => '',
			$attr['icon_pack'] => '',
			'icon_link' => '',
			'icon_target' => '',
			'icon_placement' => '',
			'icon_alignment' => '',
			'icon_font_size' => '30px',
			'icon_color' => '',

			
			'button_1_size' => '',
			'button_1_text' => 'Read More',
			'button_1_link' => '#',
			'button_1_target' => '',

			'button_2_size' => '',
			'button_2_text' => 'Read More',
			'button_2_link' => '#',
			'button_2_target' => '',

			'button_1_design_style' => '',
			'button_1_size' => '',
			'button_1_btn_theme_colored' => '',
			'button_2_design_style' => '',
			'button_2_size' => '',
			'button_2_btn_theme_colored' => '',
			'button_position' => '',
			
			'content' => ''
		);

		$params = shortcode_atts($args, $attr);
		$params['content'] = $content;
		
		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, shortcode_ob_start)
		$html = medicale_mascot_get_shortcode_template_part( 'call-to-action', $params['visual_style'], 'call-to-action/tpl', $params, true );
		
		return $html;
	}
}