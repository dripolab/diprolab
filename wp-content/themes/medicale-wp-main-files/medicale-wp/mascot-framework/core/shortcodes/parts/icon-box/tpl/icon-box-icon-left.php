
<div class="icon-box icon-left <?php echo $text_alignment;?> <?php if( $icon_theme_colored ) { echo 'iconbox-theme-colored'; }?>  <?php echo $custom_css_class;?>">
  <a class="icon <?php echo $icon_size;?> <?php echo $icon_color;?> <?php if( $icon_border_style ) { echo 'icon-bordered'; }?> <?php echo $icon_style;?> pull-left flip" <?php echo $target;?> href="<?php echo $url;?>">
	<i class="<?php echo $icon;?>"></i>
  </a>
  <<?php echo $title_tag;?> class="icon-box-title"><a <?php echo $target;?> href="<?php echo $url;?>"><?php echo $title;?></a></<?php echo $title_tag;?>>
  <div class="content"><?php echo do_shortcode($content);?></div>
  <?php if( $show_read_more_button ) { ?>
  <a <?php echo $target;?> href="<?php echo $url;?>">Read more</a>
  <?php }?>
</div>