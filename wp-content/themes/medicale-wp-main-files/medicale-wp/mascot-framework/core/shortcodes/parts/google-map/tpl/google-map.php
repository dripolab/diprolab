<?php
  $random_number = wp_rand( 1111, 9999 );
?>

<!-- Google Map HTML Codes -->
<div class="<?php echo $custom_css_class;?>">
  <div 
	data-popupstring-id="#popupstring-<?php echo $random_number;?>"
	class="map-canvas autoload-map"
	data-mapstyle="<?php echo $google_map_style;?>"
	data-height="<?php echo $height;?>"
	data-latlng="<?php echo $lat;?>,<?php echo $long;?>"
	data-title="<?php echo $marker_text_title;?>"
	data-zoom="<?php echo $zoom;?>"
	data-marker="<?php echo $marker_url;?>">
  </div>
  <div class="map-popupstring hidden" id="popupstring-<?php echo $random_number;?>">
	<div class="text-center">
		<h3><?php echo $marker_text_title;?></h3>
		<p><?php echo $marker_text_desc;?></p>
	</div>
  </div>
</div>