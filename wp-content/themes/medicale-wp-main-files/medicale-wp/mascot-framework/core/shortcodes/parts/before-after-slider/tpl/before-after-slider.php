<?php $random_number = wp_rand( 111111, 999999 ); ?>
<div id="twentytwenty-slider-<?php echo $random_number;?>" class="twentytwenty-container" data-orientation="<?php echo $orientation; ?>" data-offset-percent="<?php echo $default_offset_pct; ?>" data-no-overlay="<?php echo $no_overlay; ?>" data-before-label="<?php echo $before_label; ?>" data-after-label="<?php echo $after_label; ?>">
  <?php if( isset( $before_image ) && !empty( $before_image ) ): ?>
  <img src="<?php $image = wp_get_attachment_image_src( $before_image, 'large'); echo $image[0];?>" alt="">
  <?php endif; ?>

  <?php if( isset( $after_image ) && !empty( $after_image ) ): ?>
  <img src="<?php $image = wp_get_attachment_image_src( $after_image, 'large'); echo $image[0];?>" alt="">
  <?php endif; ?>
</div>