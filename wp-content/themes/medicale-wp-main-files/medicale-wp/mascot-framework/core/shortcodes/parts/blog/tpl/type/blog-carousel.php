
<?php if ( $the_query->have_posts() ) : ?>
  <div class="<?php echo $custom_css_class;?>">
	<div class="owl-carousel owl-theme owl-carousel-<?php echo $columns;?>col" <?php if ( $show_navigation == 'true' ) : ?> data-nav="<?php echo $show_navigation;?>"<?php endif; ?> <?php if ( $show_bullets == 'true' ) : ?> data-dots="<?php echo $show_bullets;?>"<?php endif; ?> <?php if ( $animation_speed != '' ) : ?> data-duration="<?php echo $animation_speed;?>"<?php endif; ?>>
		<!-- the loop -->
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<div class="item">
		<?php
			echo medicale_mascot_shortcode_get_blog_post_format( get_post_format(), $params_array );
		?>
		</div>
		<?php endwhile; ?>
		<!-- end of the loop -->
	</div>
  </div>
  <?php wp_reset_postdata(); ?>

<?php else : ?>
  <p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'medicale-wp' ); ?></p>
<?php endif; ?>