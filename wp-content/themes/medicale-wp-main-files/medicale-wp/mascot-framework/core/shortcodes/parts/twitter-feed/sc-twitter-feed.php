<?php

if(!function_exists('mascot_twitter_feed_sc_twitter_feed_vc_map_modifier')) {
	/**
	 * Shortcodes vc_map modifier
	 */
	function mascot_twitter_feed_sc_twitter_feed_vc_map_modifier( $shortcode_base ) {
		//vc_remove_param( $shortcode_base, "display_type" );
		//vc_add_param( $shortcode_base, $attributes );
		//vc_add_params( $shortcode_base, $attributes );
	}
}



if(!function_exists('mascot_twitter_feed_sc_twitter_feed_render')) {
	/**
	 * Renders shortcodes HTML
	 */
	function mascot_twitter_feed_sc_twitter_feed_render( $attr, $content = null ) {
		
		$args = array(
			'custom_css_class' => '',
			'feed_type' => 'carousel',
			'userid' => 'Envato',
			'count' => '3',
			'text_alignment' => 'text-center',

			'show_navigation' => 'false',
			'show_bullets' => 'true',
			'animation_speed' => '4000',
			
			'animate_out' => 'fadeOut',
			'animate_in' => 'fadeIn',

		);

		$params = shortcode_atts($args, $attr);

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, shortcode_ob_start)
		$html = medicale_mascot_get_shortcode_template_part( 'twitter-feed-' . $params['feed_type'], null, 'twitter-feed/tpl', $params, true );
		
		return $html;
	}
}