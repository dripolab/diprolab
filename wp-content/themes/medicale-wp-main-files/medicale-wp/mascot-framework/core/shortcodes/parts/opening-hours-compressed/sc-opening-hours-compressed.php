<?php

if(!function_exists('mascot_core_sc_opening_hours_compressed_vc_map_modifier')) {
	/**
	 * Shortcodes vc_map modifier
	 */
	function mascot_core_sc_opening_hours_compressed_vc_map_modifier( $shortcode_base ) {
		//vc_remove_param( $shortcode_base, "display_type" );
		//vc_add_param( $shortcode_base, $attributes );
		//vc_add_params( $shortcode_base, $attributes );
	}
}



if(!function_exists('mascot_core_sc_opening_hours_compressed_render')) {
	/**
	 * Renders shortcodes HTML
	 */
	function mascot_core_sc_opening_hours_compressed_render( $attr, $content = null ) {

		$args = array(
			'custom_css_class' => '',
			'border_color' => '',

			'day_1' => '',
			'day_1_time' => '',

			'day_2' => '',
			'day_2_time' => '',

			'day_3' => '',
			'day_3_time' => '',

			'day_4' => '',
			'day_4_time' => '',

			'day_5' => '',
			'day_5_time' => '',

			'day_6' => '',
			'day_6_time' => '',

			'day_7' => '',
			'day_7_time' => '',

		);

		$params = shortcode_atts($args, $attr);

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, shortcode_ob_start)
		$html = medicale_mascot_get_shortcode_template_part( 'opening-hours-compressed', null, 'opening-hours-compressed/tpl', $params, true );
		
		return $html;
	}
}