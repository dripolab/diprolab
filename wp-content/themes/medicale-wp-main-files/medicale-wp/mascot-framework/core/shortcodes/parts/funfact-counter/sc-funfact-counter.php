<?php

if(!function_exists('mascot_core_sc_funfact_counter_vc_map_modifier')) {
	/**
	 * Shortcodes vc_map modifier
	 */
	function mascot_core_sc_funfact_counter_vc_map_modifier( $shortcode_base ) {
		//vc_remove_param( $shortcode_base, "display_type" );
		//vc_add_param( $shortcode_base, $attributes );
		//vc_add_params( $shortcode_base, $attributes );
	}
}


if(!function_exists('mascot_core_sc_funfact_counter_render')) {
	/**
	 * Renders shortcodes HTML
	 */
	function mascot_core_sc_funfact_counter_render( $attr, $content = null ) {
		if( !array_key_exists('icon_pack', $attr) ) {
			$attr['icon_pack'] = 'null';
		}

		$args = array(
			'custom_css_class' => '',
			'design_style' => 'style1',
			'title' => '',
			'counter_range' => '1250',
			'counter_duration' => '1500',

			'icon_type' => 'font-icon',
			'icon_pack' => '',
			$attr['icon_pack'] => '',
			'image_icon' => '',
			'icon_color' => '',

			'counter_tag' => 'h2',
			'counter_text_color' => '',
			'counter_font_size' => '',
			'counter_line_height' => '',
			'counter_letter_spacing' => '',
			'counter_font_style' => '',
			'counter_font_weight' => '',

			'title_tag' => 'h4',
			'title_text_color' => '',
			'title_font_size' => '',
			'title_line_height' => '',
			'title_letter_spacing' => '',
			'title_font_style' => '',
			'title_font_weight' => '',
			'title_text_transform' => '',
		);

		$params = shortcode_atts($args, $attr);

		$params['title_inline_css'] = medicale_mascot_get_inline_css( mascot_core_sc_funfact_counter_title_css( $params ) );
		$params['counter_inline_css'] = medicale_mascot_get_inline_css( mascot_core_sc_section_title_counter_css( $params ) );

		$params['icon_color_inline_css'] = '';
		if( $params['icon_color'] != '' ) {
			$params['icon_color_inline_css'] = medicale_mascot_get_inline_css( 'color: '.$params['icon_color'] );
		}
		$params['animation_duration'] = medicale_mascot_get_inline_attributes( $params['counter_duration'], 'data-animation-duration' );

		//Produce HTML version by using the parameters (filename, variation, folder name, parameters, shortcode_ob_start)
		$html = medicale_mascot_get_shortcode_template_part( 'funfact-counter', $params['design_style'], 'funfact-counter/tpl', $params, true );
		
		return $html;
	}
}

if(!function_exists('mascot_core_sc_funfact_counter_title_css')) {
	/**
	 * Get Title Styles
	 */
	function mascot_core_sc_funfact_counter_title_css( $params ) {
		$css_array = array();

		if( $params['title_text_color'] != '' ) {
			$css_array[] = 'color: '.$params['title_text_color'];
		}
		if( $params['title_font_size'] != '' ) {
			$font_size = medicale_mascot_remove_suffix( $params['title_font_size'], 'px');
			$css_array[] = 'font-size: '.$font_size.'px';
		}
		if( $params['title_line_height'] != '' ) {
			$font_size = medicale_mascot_remove_suffix( $params['title_line_height'], 'px');
			$css_array[] = 'line-height: '.$font_size.'px';
		}
		if( $params['title_letter_spacing'] != '' ) {
			$font_size = medicale_mascot_remove_suffix( $params['title_letter_spacing'], 'px');
			$css_array[] = 'letter-spacing: '.$font_size.'px';
		}
		if( $params['title_font_style'] != '' ) {
			$css_array[] = 'font-style: '.$params['title_font_style'];
		}
		if( $params['title_font_weight'] != '' ) {
			$css_array[] = 'font-weight: '.$params['title_font_weight'];
		}
		if( $params['title_text_transform'] != '' ) {
			$css_array[] = 'text-transform: '.$params['title_text_transform'];
		}

		return implode( '; ', $css_array );
	}
}

if(!function_exists('mascot_core_sc_section_title_counter_css')) {
	/**
	 * Get Counter Styles
	 */
	function mascot_core_sc_section_title_counter_css( $params ) {
		$css_array = array();

		if( $params['counter_text_color'] != '' ) {
			$css_array[] = 'color: '.$params['counter_text_color'];
		}
		if( $params['counter_font_size'] != '' ) {
			$font_size = medicale_mascot_remove_suffix( $params['counter_font_size'], 'px');
			$css_array[] = 'font-size: '.$font_size.'px';
		}
		if( $params['counter_line_height'] != '' ) {
			$font_size = medicale_mascot_remove_suffix( $params['counter_line_height'], 'px');
			$css_array[] = 'line-height: '.$font_size.'px';
		}
		if( $params['counter_letter_spacing'] != '' ) {
			$font_size = medicale_mascot_remove_suffix( $params['counter_letter_spacing'], 'px');
			$css_array[] = 'letter-spacing: '.$font_size.'px';
		}
		if( $params['counter_font_style'] != '' ) {
			$css_array[] = 'font-style: '.$params['counter_font_style'];
		}
		if( $params['counter_font_weight'] != '' ) {
			$css_array[] = 'font-weight: '.$params['counter_font_weight'];
		}

		return implode( '; ', $css_array );
	}
}