<div class="row">
	<div class="call-to-action">

		<?php if( $icon_placement == 'left' ) : ?>

			<?php if( $content_in_grid == "no" ) { ?>
			<div class="col-md-12">
			<?php } elseif ( $content_in_grid == "yes" ) { ?>

			<?php medicale_mascot_get_shortcode_template_part( 'grid-right', null, 'call-to-action/tpl', $params, false ); ?>

			<?php } //end $content_in_grid == "yes" ?>
				<?php medicale_mascot_get_shortcode_template_part( 'icon', null, 'call-to-action/tpl', $params, false ); ?>
			</div>
			

			<?php if( $content_in_grid == "no" ) { ?>
			<div class="col-md-12">
			<?php } elseif ( $content_in_grid == "yes" ) { ?>

			<?php medicale_mascot_get_shortcode_template_part( 'grid-left', null, 'call-to-action/tpl', $params, false ); ?>

			<?php } //end $content_in_grid == "yes" ?>

				<?php medicale_mascot_get_shortcode_template_part( 'content', null, 'call-to-action/tpl', $params, false ); ?>
			</div>

		<?php else: ?>

			<?php if( $content_in_grid == "no" ) { ?>
			<div class="col-md-12">
			<?php } elseif ( $content_in_grid == "yes" ) { ?>

			<?php medicale_mascot_get_shortcode_template_part( 'grid-left', null, 'call-to-action/tpl', $params, false ); ?>

			<?php } //end $content_in_grid == "yes" ?>

				<?php medicale_mascot_get_shortcode_template_part( 'content', null, 'call-to-action/tpl', $params, false ); ?>
			</div>

			<?php if( $content_in_grid == "no" ) { ?>
			<div class="col-md-12">
			<?php } elseif ( $content_in_grid == "yes" ) { ?>

			<?php medicale_mascot_get_shortcode_template_part( 'grid-right', null, 'call-to-action/tpl', $params, false ); ?>

			<?php } //end $content_in_grid == "yes" ?>
				<?php medicale_mascot_get_shortcode_template_part( 'icon', null, 'call-to-action/tpl', $params, false ); ?>
			</div>

		<?php endif ?>

	</div>
</div>