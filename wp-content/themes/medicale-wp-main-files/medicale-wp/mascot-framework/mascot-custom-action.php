<?php

// Custom Action for this theme
add_action('after_setup_theme', 'medicale_mascot_custom_action_init', 0);

function medicale_mascot_custom_action_init() {

	do_action('medicale_mascot_before_custom_action');

	do_action('medicale_mascot_custom_action');

	do_action('medicale_mascot_after_custom_action');
}