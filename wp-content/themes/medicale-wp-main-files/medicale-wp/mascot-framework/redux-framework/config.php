<?php
	/**
	 * ReduxFramework Sample Config File
	 * For full documentation, please visit: http://docs.reduxframework.com/
	 */

	if ( ! class_exists( 'Redux' ) ) {
		return;
	}

	// This is your option name where all the Redux data is stored.
	$opt_name = "medicale_mascot_redux_theme_opt";

	// This line is only for altering the demo. Can be easily removed.
	$opt_name = apply_filters( 'medicale_mascot_redux_theme_opt/opt_name', $opt_name );

	$site_url = site_url();

	//custom action hook for this template:
	add_action ('redux/options/medicale_mascot_redux_theme_opt/saved', 'medicale_mascot_generate_css_for_custom_theme_color_from_less');
	add_action ('redux/options/medicale_mascot_redux_theme_opt/saved', 'medicale_mascot_generate_dynamic_css');

	/*
	 *
	 * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
	 *
	 */

	// Background Patterns Reader
	$sample_patterns_path = MASCOT_ADMIN_ASSETS_DIR . '/images/pattern/';
	$sample_patterns_url  = MASCOT_ADMIN_ASSETS_URI . '/images/pattern/';
	$sample_patterns      = array();
	
	if ( is_dir( $sample_patterns_path ) ) {

		if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
			$sample_patterns = array();

			while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

				if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
					$name              = explode( '.', $sample_patterns_file );
					$name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
					$sample_patterns[ $sample_patterns_url . $sample_patterns_file ] = array(
						'alt' => $name,
						'img' => $sample_patterns_url . $sample_patterns_file
					);
				}
			}
		}
	}


	/*
	 *
	 * ---> START SECTIONS
	 *
	 */

	/*

		As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


	 */


	// -> START General Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'General', 'medicale-wp' ),
		'id'     => 'general-settings',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-admin-home',
		'fields' => array(
			array(
				'id'       => 'general-settings-favicon',
				'type'     => 'media',
				'url'      => false,
				'title'    => esc_html__( 'Favicon', 'medicale-wp' ),
				'compiler' => 'true',
				//'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
				'desc'     => esc_html__( 'Basic media uploader with disabled URL input field.', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Upload a 32px x 32px png/gif image that will represent your website favicon.', 'medicale-wp' ),
				'default'	=> array( 'url' => MASCOT_ASSETS_URI . '/images/logo/favicon.png' ),
			),
			array(
				'id'       => 'general-settings-apple-touch-144',
				'type'     => 'media',
				'url'      => false,
				'title'    => esc_html__( 'Apple Touch 144x144 Icon', 'medicale-wp' ),
				'compiler' => 'true',
				//'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
				'desc'     => esc_html__( 'Basic media uploader with disabled URL input field.', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Upload a 144px x 144px png image that will be your website bookmark on retina iOS devices.', 'medicale-wp' ),
				'default'	=> array( 'url' => MASCOT_ASSETS_URI . '/images/logo/apple-touch-icon-144x144.png' ),
			),
			array(
				'id'       => 'general-settings-apple-touch-114',
				'type'     => 'media',
				'url'      => false,
				'title'    => esc_html__( 'Apple Touch 114x114 Icon', 'medicale-wp' ),
				'compiler' => 'true',
				//'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
				'desc'     => esc_html__( 'Basic media uploader with disabled URL input field.', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Upload a 114px x 114px png image that will be your website bookmark on retina iOS devices.', 'medicale-wp' ),
				'default'	=> array( 'url' => MASCOT_ASSETS_URI . '/images/logo/apple-touch-icon-114x114.png' ),
			),
			array(
				'id'       => 'general-settings-apple-touch-72',
				'type'     => 'media',
				'url'      => false,
				'title'    => esc_html__( 'Apple Touch 72x72 Icon', 'medicale-wp' ),
				'compiler' => 'true',
				//'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
				'desc'     => esc_html__( 'Basic media uploader with disabled URL input field.', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Upload a 72px x 72px Png image that will be your website bookmark on non-retina iOS devices.', 'medicale-wp' ),
				'default'	=> array( 'url' => MASCOT_ASSETS_URI . '/images/logo/apple-touch-icon-72x72.png' ),
			),
			array(
				'id'       => 'general-settings-apple-touch-32',
				'type'     => 'media',
				'url'      => false,
				'title'    => esc_html__( 'Apple Touch 32x32 Icon', 'medicale-wp' ),
				'compiler' => 'true',
				//'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
				'desc'     => esc_html__( 'Basic media uploader with disabled URL input field.', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Upload a 32px x 32px png image that will be your website bookmark on non-retina iOS devices.', 'medicale-wp' ),
				'default'	=> array( 'url' => MASCOT_ASSETS_URI . '/images/logo/apple-touch-icon.png' ),
			),
			array(
				'id'       => 'general-settings-enable-responsive',
				'type'     => 'switch',
				'title'    => esc_html__( 'Enable Responsive', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disable the responsive behaviour of the theme', 'medicale-wp' ),
				'default'	=> true,
			),
			/*array(
				'id'       => 'general-settings-enable-rtl',
				'type'     => 'switch',
				'title'    => esc_html__( 'Enable RTL~', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Please enable this option if you are using wordpress for RTL languages.', 'medicale-wp' ),
				'default'	=> false,
			),*/
			array(
				'id'       => 'general-settings-enable-backtotop',
				'type'     => 'switch',
				'title'    => esc_html__( 'Enable Back To Top', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable Back to Top button that appears in the bottom right corner of the screen.', 'medicale-wp' ),
				'default'	=> true,
			),

			/*array(
				'id'       => 'general-settings-smooth-scroll',
				'type'     => 'switch',
				'title'    => esc_html__( 'Smooth Scroll=======', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enabling this option will perform a smooth scrolling effect on every page (except on Mac and touch devices).', 'medicale-wp' ),
				'default'	=> false,
			),
			array(
				'id'       => 'general-settings-smooth-page-transition',
				'type'     => 'switch',
				'title'    => esc_html__( 'Smooth Page Transitions=======', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enabling this option will perform a smooth transition between pages when clicking on links.', 'medicale-wp' ),
				'default'	=> false,
			),*/
			//section H3 Starts
			array(
				'id'       => 'general-settings-enable-page-preloader-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Page Preloader Settings', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disable Page Preloader.', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'       => 'general-settings-enable-page-preloader',
				'type'     => 'switch',
				'title'    => esc_html__( 'Enable Page Preloader', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enabling this option will show page preloader.', 'medicale-wp' ),
				'default'	=> false,
			),
			array(
				'id'        => 'general-settings-page-preloader-type',
				'type'      => 'button_set',
				'compiler'  => true,
				'title'     => esc_html__( 'Page Preloader Type', 'medicale-wp' ),
				'subtitle'  => esc_html__( 'Select preloader type', 'medicale-wp' ),
				'options'   => array(
					'css-preloader'    => esc_html__( 'CSS Preloader', 'medicale-wp' ),
					'gif-preloader'    => esc_html__( 'Gif Icon Preloader', 'medicale-wp' )
				),
				'default'   => 'css-preloader',
				'required' => array( 'general-settings-enable-page-preloader', '=', '1' ),
			),
			array(
				'id'        => 'general-settings-page-preloader-type-css',
				'type'     => 'select',
				'title'    => esc_html__( 'CSS Preloader', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose Predefined CSS Preloader.', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'preloader-bubblingG'           => esc_html__( 'Bubbling', 'medicale-wp' ),
					'preloader-circle-loading-wrapper' => esc_html__( 'Circle Loading', 'medicale-wp' ),
					'preloader-coffee'              => esc_html__( 'Coffee', 'medicale-wp' ),
					'preloader-battery'             => esc_html__( 'Battery', 'medicale-wp' ),
					'preloader-dot-circle-rotator'  => esc_html__( 'Dot Circle Rotator', 'medicale-wp' ),
					'preloader-dot-loading'         => esc_html__( 'Dot Loading', 'medicale-wp' ),
					'preloader-double-torus'        => esc_html__( 'Double Torus', 'medicale-wp' ),
					'preloader-equalizer'           => esc_html__( 'Equalizer', 'medicale-wp' ),
					'preloader-floating-circles'    => esc_html__( 'Floating Circles', 'medicale-wp' ),
					'preloader-fountainTextG'       => esc_html__( 'Fountain Text', 'medicale-wp' ),
					'preloader-jackhammer'          => esc_html__( 'Jackhammer', 'medicale-wp' ),
					'preloader-loading-wrapper'     => esc_html__( 'Loading', 'medicale-wp' ),
					'preloader-orbit-loading'       => esc_html__( 'Orbit Loading', 'medicale-wp' ),
					'preloader-speeding-wheel'      => esc_html__( 'Speeding Wheel', 'medicale-wp' ),
					'preloader-square-swapping'     => esc_html__( 'Square Swapping', 'medicale-wp' ),
					'preloader-tube-tunnel'         => esc_html__( 'Tube Tunnel', 'medicale-wp' ),
					'preloader-whirlpool'           => esc_html__( 'Whirlpool', 'medicale-wp' ),
				),
				'default'	=> 'preloader-dot-loading',
				'required' => array( 'general-settings-page-preloader-type', '=', 'css-preloader' ),
			),
			array(
				'id'        => 'general-settings-page-preloader-type-gif',
				'type'     => 'select',
				'title'    => esc_html__( 'Gif Icon Preloader', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose Predefined Gif Icon Preloader.', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					MASCOT_ADMIN_ASSETS_URI . '/images/preloaders/1.gif'  =>  'preloader-1',
					MASCOT_ADMIN_ASSETS_URI . '/images/preloaders/2.gif'  =>  'preloader-2',
					MASCOT_ADMIN_ASSETS_URI . '/images/preloaders/3.gif'  =>  'preloader-3',
					MASCOT_ADMIN_ASSETS_URI . '/images/preloaders/4.gif'  =>  'preloader-4',
					MASCOT_ADMIN_ASSETS_URI . '/images/preloaders/5.gif'  =>  'preloader-5',
					MASCOT_ADMIN_ASSETS_URI . '/images/preloaders/6.gif'  =>  'preloader-6',
					MASCOT_ADMIN_ASSETS_URI . '/images/preloaders/7.gif'  =>  'preloader-7',
					MASCOT_ADMIN_ASSETS_URI . '/images/preloaders/8.gif'  =>  'preloader-8',
					MASCOT_ADMIN_ASSETS_URI . '/images/preloaders/9.gif'  =>  'preloader-9',
					MASCOT_ADMIN_ASSETS_URI . '/images/preloaders/10.gif' =>  'preloader-10',
				),
				'default'	=> MASCOT_ADMIN_ASSETS_URI . '/images/preloaders/1.gif',
				'required' => array( 'general-settings-page-preloader-type', '=', 'gif-preloader' ),
			),
			array(
				'id'        => 'general-settings-page-preloader-show-disable-button',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Show Disable Preloader Button', 'medicale-wp' ),
				'subtitle'    => esc_html__( 'Show Disable Preloader Button at the corner of the screen.', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> '1',
				'required' => array( 'general-settings-enable-page-preloader', '=', '1' ),
			),
			array(
				'id'        => 'general-settings-page-preloader-show-disable-button-text',
				'type'     => 'text',
				'title'    => esc_html__( 'Disable Preloader Button Text', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter the text that will be appeared into the Disable Preloader Button.', 'medicale-wp' ),
				'desc'     => '',
				'default'    => esc_html__( 'Disable Preloader', 'medicale-wp' ),
				'required' => array( 'general-settings-page-preloader-show-disable-button', '=', '1' ),
			),
			array(
				'id'       => 'general-settings-enable-page-preloader-ends',
				'type'     => 'section',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
			),
		)
	) );


	// -> START Logo Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Logo', 'medicale-wp' ),
		'id'     => 'logo-settings',
		'desc'   => sprintf( esc_html__( 'If you want to upload SVG logo then please install this %1$ssvg plugin%2$s', 'medicale-wp' ), '<a target="_blank" href="' . esc_url( 'https://wordpress.org/plugins/svg-support/' ) . '">', '</a>' ),
		'icon'   => 'dashicons-before dashicons-palmtree',
		'fields' => array(
			array(
				'id'       => 'logo-settings-site-brand',
				'type'     => 'text',
				'title'    => esc_html__( 'Site Brand', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter the text that will be appeared as logo', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> $theme->get( 'Name' ),
			),

			array(
				'id'       => 'logo-settings-want-to-use-logo',
				'type'     => 'switch',
				'title'    => esc_html__( 'Use logo in replace of "Site Brand" Text?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'If you want to use logo then please enable it.', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),

			array(
				'id'       => 'logo-settings-switchable-logo',
				'type'     => 'switch',
				'title'    => esc_html__( 'Switchable logo(Light+Dark)?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'If you want to use switchable logo then please enable it.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 'logo-settings-want-to-use-logo', '=', '1' ),
			),

			array(
				'id'       => 'logo-settings-logo-default',
				'type'     => 'media',
				'url'      => false,
				'title'    => esc_html__( 'Logo (Default)', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Upload/choose your custom logo image', 'medicale-wp' ),
				'compiler' => 'true',
				//'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
				'desc'     => '',
				'default'	=> array( 'url' => MASCOT_ASSETS_URI . '/images/logo/logo-wide.png' ),
				'required' => array( 'logo-settings-switchable-logo', '=', '0' ),
			),

			array(
				'id'       => 'logo-settings-logo-default-2x',
				'type'     => 'media',
				'url'      => false,
				'title'    => esc_html__( 'Logo (Default) Retina', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select an image file for the retina version of the logo. It should be exactly 2x the size of the main logo.', 'medicale-wp' ),
				'compiler' => 'true',
				//'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
				'desc'     => '',
				'default'	=> array( 'url' => MASCOT_ASSETS_URI . '/images/logo/logo-wide@2x.png' ),
				'required' => array( 'logo-settings-switchable-logo', '=', '0' ),
			),

			array(
				'id'       => 'logo-settings-logo-light',
				'type'     => 'media',
				'url'      => false,
				'title'    => esc_html__( 'Logo (Light)', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Upload a logo for the light skin.', 'medicale-wp' ),
				'compiler' => 'true',
				//'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
				'desc'     => '',
				'default'	=> array( 'url' => MASCOT_ASSETS_URI . '/images/logo/logo-wide-white.png' ),
				'required' => array( 'logo-settings-switchable-logo', '=', '1' ),
			),

			array(
				'id'       => 'logo-settings-logo-light-2x',
				'type'     => 'media',
				'url'      => false,
				'title'    => esc_html__( 'Logo (Light) Retina', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select an image file for the retina version of the logo. It should be exactly 2x the size of the main logo.', 'medicale-wp' ),
				'compiler' => 'true',
				//'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
				'desc'     => '',
				'default'	=> array( 'url' => MASCOT_ASSETS_URI . '/images/logo/logo-wide-white@2x.png' ),
				'required' => array( 'logo-settings-switchable-logo', '=', '1' ),
			),

			array(
				'id'       => 'logo-settings-logo-dark',
				'type'     => 'media',
				'url'      => false,
				'title'    => esc_html__( 'Logo (Dark)', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Upload a logo for the dark skin.', 'medicale-wp' ),
				'compiler' => 'true',
				//'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
				'desc'     => '',
				'default'	=> array( 'url' => MASCOT_ASSETS_URI . '/images/logo/logo-wide.png' ),
				'required' => array( 'logo-settings-switchable-logo', '=', '1' ),
			),

			array(
				'id'       => 'logo-settings-logo-dark-2x',
				'type'     => 'media',
				'url'      => false,
				'title'    => esc_html__( 'Logo (Dark) Retina', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select an image file for the retina version of the logo. It should be exactly 2x the size of the main logo.', 'medicale-wp' ),
				'compiler' => 'true',
				//'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
				'desc'     => '',
				'default'	=> array( 'url' => MASCOT_ASSETS_URI . '/images/logo/logo-wide@2x.png' ),
				'required' => array( 'logo-settings-switchable-logo', '=', '1' ),
			),


			array(
				'id'            => 'logo-settings-maximum-logo-height',
				'type'          => 'slider',
				'title'         => esc_html__( 'Maximum logo height(px)', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Enter maximum logo height in px.', 'medicale-wp' ),
				'desc'          => '',
				'default'       => 40,
				'min'           => 20,
				'step'          => 1,
				'max'           => 150,
				'display_value' => 'text',
				'required' => array( 'logo-settings-want-to-use-logo', '=', '1' ),
			),

			array(
				'id'       => 'logo-settings-admin-login-logo',
				'type'     => 'media',
				'url'      => false,
				'title'    => esc_html__( 'WordPress Admin Login Logo', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Change the default wordpress login logo. Dimensions should be 250x50 px', 'medicale-wp' ),
				'compiler' => 'true',
				//'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
				'desc'     => '',
				'default'	=> array( 'url' => MASCOT_ASSETS_URI . '/images/logo/logo-wide.png' ),
			),

		)
	) );



	// -> START Header
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Header', 'medicale-wp' ),
		'id'     => 'header',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-arrow-up-alt',
	) );



	// -> START Header Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Header', 'medicale-wp' ),
		'id'     => 'header-settings',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-arrow-up-alt',
		'subsection' => true,
		'fields' => array(
			array(
				'id'       => 'header-settings-choose-header-visibility',
				'type'     => 'switch',
				'title'    => esc_html__( 'Header Visibility', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Show or hide header globally', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => 'Show',
				'off'      => 'Hide',
			),
		)
	) );



	// -> START Header Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Header Layout', 'medicale-wp' ),
		'id'     => 'header-layout',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-arrow-up-alt',
		'subsection' => true,
		'fields' => array(

			//Header Visibility Important
			array(
				'id'        => 'header-settings-header-layout-info-field-important',
				'type'      => 'info',
				'title'     => esc_html__( 'Important!', 'medicale-wp' ),
				'subtitle'  => sprintf( esc_html__( 'As you have chosen %1$sHeader Visibility%2$s to %1$sHide%2$s so there\'s nothing to show here!', 'medicale-wp' ), '<strong>', '</strong>'),
				'notice'    => false,
				'required' => array( 'header-settings-choose-header-visibility', '!=', '1' ),
			),

			array(
				'id'       => 'header-settings-choose-header-layout-type',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Choose Header Layout Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select the type of header you would like to use', 'medicale-wp' ),
				'desc'     => '',
				//Must provide key => value(array:title|img) pairs for radio options
				'options'	=> array(
					'header-1-2col' => array(
						'alt' => '2 Rows',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/header-layout/header-1-2col.jpg'
					),
					'header-2-3col' => array(
						'alt' => '3 Rows',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/header-layout/header-2-3col.jpg'
					),
					'header-3-logo-center' => array(
						'alt' => '3 Rows Logo Center',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/header-layout/header-3-logo-center.jpg'
					),
					'header-4-logo-menu-center' => array(
						'alt' => '3 Rows Logo + Menu Center',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/header-layout/header-4-logo-menu-center.jpg'
					),
					'header-5-mobile-nav' => array(
						'alt' => 'Mobile Nav',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/header-layout/header-5-mobile-nav.jpg'
					),
					'header-6-side-panel-nav' => array(
						'alt' => 'Side Panel Nav',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/header-layout/header-6-side-panel-nav.jpg'
					),
					'header-7-vertical-nav' => array(
						'alt' => 'Vertical Nav',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/header-layout/header-7-vertical-nav.jpg'
					),
				),
				'default'	=> 'header-1-2col',
				'required' => array( 'header-settings-choose-header-visibility', '=', '1' ),
			),
			array(
				'id'       => 'header-settings-header-layout-type-container',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Header Container', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Put Header content boxed or stretched fullwidth.', 'medicale-wp' ),
				'options'	=> array(
					'container' => esc_html__( 'Container', 'medicale-wp' ),
					'container-fluid' => esc_html__( 'Container Fluid', 'medicale-wp' )
				),
				'default' => 'container',
				'required' => array( 'header-settings-choose-header-visibility', '=', '1' ),
			),
			/*~*//*array(
				'id'       => 'header-settings-header-layout-type-behaviour',
				'type'     => 'select',
				'title'    => esc_html__( 'Header Behaviour', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select the behaviour of header when you scroll down to page', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'header-fixed'  => esc_html__( 'Fixed on Scroll', 'medicale-wp' ),
					'header-sticky' => esc_html__( 'Sticky on Scroll', 'medicale-wp' ),
				),
				'default'	=> 'header-fixed',
				'required' => array( 'header-settings-choose-header-visibility', '=', '1' ),
			),*/

		)
	) );


	// -> START Header Top Row
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Header Top Row', 'medicale-wp' ),
		'id'     => 'header-header-top',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-arrow-up-alt',
		'subsection' => true,
		'fields' => array(

			//Header Visibility Important
			array(
				'id'        => 'header-settings-header-top-info-field-important',
				'type'      => 'info',
				'title'     => esc_html__( 'Important!', 'medicale-wp' ),
				'subtitle'  => sprintf( esc_html__( 'As you have chosen %1$sHeader Visibility%2$s to %1$sHide%2$s so there\'s nothing to show here!', 'medicale-wp' ), '<strong>', '</strong>'),
				'notice'    => false,
				'required' => array( 'header-settings-choose-header-visibility', '!=', '1' ),
			),

			array(
				'id'       => 'header-settings-show-header-top',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Header Top', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enabling this option will show Header Top section', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 'header-settings-choose-header-visibility', '=', '1' ),
			),
			array(
				'id'       => 'header-settings-header-top-columns-layout',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Choose Header Top Columns Layout', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose layout of the Header Top area.', 'medicale-wp' ),
				'desc'     => '',
				//Must provide key => value(array:title|img) pairs for radio options
				'options'	=> array(
					'25-75' => array(
						'alt' => '1/4 + 3/4',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/header-top/h1.jpg'
					),
					'33-66' => array(
						'alt' => '1/3 + 2/3',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/header-top/h2.jpg'
					),
					'50-50' => array(
						'alt' => '1/2 + 1/2',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/header-top/h3.jpg'
					),
					'66-33' => array(
						'alt' => '2/3 + 1/3',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/header-top/h4.jpg'
					),
					'75-25' => array(
						'alt' => '3/4 + 1/4',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/header-top/h5.jpg'
					),
					'100' => array(
						'alt' => '1/1',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/header-top/h6.jpg'
					)
				),
				'default'	=> '66-33',
				'required' => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),









			//Header Top Column 1
			array(
				'id'        => 'header-settings-header-top-column1-info-field-column1',
				'type'      => 'info',
				'title'     => esc_html__( 'Header Top Column 1 - Left Widget', 'medicale-wp' ),
				'subtitle'  => esc_html__( 'Choose content to display on column 1 (Left Widget) of header top', 'medicale-wp' ),
				'notice'    => false,
				'required'  => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),
			array(
				'id'       => 'header-settings-header-top-column1-content',
				'type'     => 'select',
				'title'    => esc_html__( 'Column 1 (Left Widget) - Content Types', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose content type to display on column 1.', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( '> You can choose multiple and rearrange them by moving left or right! %3$s> In case of %1$s"Social Links"%2$s, you will find it\'s settings at %1$sTheme Options > Social Links%2$s. %3$s> In case of %1$s"Header Top Navigation"%2$s, Please create a new menu from %1$sAppearance > Menus%2$s and set Theme Location %1$s"Column 1 - Header Top Navigation"%2$s', 'medicale-wp' ), '<strong>', '</strong>', '<br>'),
				'multi'    => true,
				'sortable' => true,
				'options'	=> array(
					'contact-info'      => esc_html__( 'Contact Info', 'medicale-wp' ),
					'custom-text'       => esc_html__( 'Custom Text', 'medicale-wp' ),
					'custom-button'     => esc_html__( 'Custom Button', 'medicale-wp' ),
					'header-top-nav'    => esc_html__( 'Header Top Navigation', 'medicale-wp' ),
					'social-links'      => esc_html__( 'Social Links', 'medicale-wp' ),
					'search-box'        => esc_html__( 'Search Box', 'medicale-wp' ),
					'login-register'    => esc_html__( 'Login/Register', 'medicale-wp' ),
					'wpml-languages'    => esc_html__( 'WPML Languages', 'medicale-wp' ),
					'checkout-button'   => esc_html__( 'Checkout Button', 'medicale-wp' ),
				),
				'default'	=> array(
					'contact-info',
				),
				'required' => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),


			//Column 1 - Contact Info
			array(
				'id'       => 'header-settings-header-top-column1-contact-info-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Column 1 - Contact Info', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-top-column1-content', '=', 'contact-info' )
				)
			),
			array(
				'id'       => 'header-settings-header-top-column1-contact-info',
				'type'     => 'sortable',
				'title'    => esc_html__( 'Column 1 - Contact Info Text', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter your custom contact info.', 'medicale-wp' ),
				'desc'     => esc_html__( 'Reorder them by moving up or down!', 'medicale-wp' ),
				'label'    => true,
				'options'	=> array(
					'phone'           => esc_html__( 'Phone', 'medicale-wp' ),
					'email'           => esc_html__( 'Email', 'medicale-wp' ),
					'address'         => esc_html__( 'Address', 'medicale-wp' ),
					'opening-hours'   => esc_html__( 'Opening Hours', 'medicale-wp' ),
				),
				'default' => array(
					'phone'           => esc_html__( 'Questions? Call us at: +123 4567 8901', 'medicale-wp' ),
					'email'           => esc_html__( 'info@yourdomain.com', 'medicale-wp' ),
					'address'         => esc_html__( 'Envato HQ 121 King Street, Melbourne', 'medicale-wp' ),
					'opening-hours'   => esc_html__( 'Mon-Fri 09:00-17:00', 'medicale-wp' ),
				),
			),
			array(
				'id'       => 'header-settings-header-top-column1-contact-info-checkbox',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Column 1 - Contact Info Visibility', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Show/Hide each item individually.', 'medicale-wp' ),
				'desc'     => esc_html__( 'Please choose which fields you want to display.', 'medicale-wp' ),
				'label'    => true,
				'options'	=> array(
					'phone'           => esc_html__( 'Phone', 'medicale-wp' ),
					'email'           => esc_html__( 'Email', 'medicale-wp' ),
					'address'         => esc_html__( 'Address', 'medicale-wp' ),
					'opening-hours'   => esc_html__( 'Opening Hours', 'medicale-wp' ),
				),
				'default' => array(
					'phone'           => 1,
					'email'           => 1,
					'address'         => 0,
					'opening-hours'   => 0,
				),
			),
			array(
				'id'       => 'header-settings-header-top-column1-contact-info-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-top-column1-content', '=', 'contact-info' )
				)
			),



			//Column 1 - Custom Text
			array(
				'id'       => 'header-settings-header-top-column1-custom-text-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Column 1 - Custom Text', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-top-column1-content', '=', 'custom-text' )
				)
			),
			array(
				'id'       => 'header-settings-header-top-column1-custom-text',
				'type'     => 'textarea',
				'title'    => esc_html__( 'Column 1 - Custom Text', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter your custom text. Custom HTML tags are allowed (wp_kses).', 'medicale-wp' ),
				'rows'     => '3',
				'desc'     => '',
				'default'	=> sprintf( esc_html__( 'Custom %1$sheader top%1$s text goes here!', 'medicale-wp' ), '<br>'),
				'validate' => 'html_custom',
				'allowed_html' => array(
					'a' => array(
						'href' => array(),
						'title' => array()
					),
					'br' => array(),
					'em' => array(),
					'strong' => array()
				),
			),
			array(
				'id'       => 'header-settings-header-top-column1-custom-text-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-top-column1-content', '=', 'custom-text' )
				)
			),




			//Column 1 - Custom Button
			array(
				'id'       => 'header-settings-header-top-column1-custom-button-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Column 1 - Custom Button', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-top-column1-content', '=', 'custom-button' )
				)
			),
			array(
				'id'       => 'header-settings-header-top-column1-custom-button',
				'type'     => 'sortable',
				'title'    => esc_html__( 'Column 1 - Custom Button', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter your custom button info.', 'medicale-wp' ),
				'desc'     => esc_html__( 'Show a custom button in the header top.', 'medicale-wp' ),
				'label'    => true,
				'options'	=> array(
					'Button Title'  => '',
					'Button Link'   => '',
				),
				'default' => array(
					'Button Title'  => 'Custom Button',
					'Button Link'   => '#',
				),
			),
			array(
				'id'       => 'header-settings-header-top-column1-custom-button-design-style',
				'type'     => 'select',
				'title'    => esc_html__( 'Button Design Style', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'btn-default'   => esc_html__( 'Button Defaults', 'medicale-wp' ),
					'btn-border'    => esc_html__( 'Button Border', 'medicale-wp' ),
					'btn-dark'      => esc_html__( 'Button Dark', 'medicale-wp' ),
					'btn-gray'      => esc_html__( 'Button Gray', 'medicale-wp' ),
				),
				'default'	=> 'btn-gray',
			),
			array(
				'id'       => 'header-settings-header-top-column1-custom-button-theme-colored',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Make Button Theme Colored?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'To make the button theme colored, please check it.', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> '1',
			),
			array(
				'id'       => 'header-settings-header-top-column1-custom-button-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-top-column1-content', '=', 'custom-button' )
				)
			),



			//Column 1 - Social Links
			array(
				'id'       => 'header-settings-header-top-column1-social-links-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Column 1 - Social Links', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-top-column1-content', '=', 'social-links' )
				)
			),
			array(
				'id'       => 'header-settings-header-top-column1-social-links-color',
				'type'     => 'select',
				'title'    => esc_html__( 'Social Links Color', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'icon-dark'     => esc_html__( 'Dark', 'medicale-wp' ),
					''              => esc_html__( 'Default', 'medicale-wp' ),
					'icon-gray'     => esc_html__( 'Gray', 'medicale-wp' ),
				),
				'default'	=> '',
			),
			array(
				'id'       => 'header-settings-header-top-column1-social-links-icon-style',
				'type'     => 'select',
				'title'    => esc_html__( 'Social Links Icon Style', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'icon-rounded'   => esc_html__( 'Rounded', 'medicale-wp' ),
					''               => esc_html__( 'Default', 'medicale-wp' ),
					'icon-circled'   => esc_html__( 'Circled', 'medicale-wp' ),
				),
				'default'	=> '',
			),
			array(
				'id'       => 'header-settings-header-top-column1-social-links-icon-border-style',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Make Icon Area Bordered?', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> '0',
			),
			array(
				'id'       => 'header-settings-header-top-column1-social-links-theme-colored',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Make Social Links Theme Colored?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'To make the social links theme colored, please check it.', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> '0',
			),
			array(
				'id'       => 'header-settings-header-top-column1-social-links-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-top-column1-content', '=', 'social-links' )
				)
			),


			//Column 1 - Text Alignment
			array(
				'id'       => 'header-settings-header-top-column1-text-align-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Column 1 - Text Alignment', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),
			array(
				'id'       => 'header-settings-header-top-column1-text-align',
				'type'     => 'select',
				'title'    => esc_html__( 'Column 1 - Text Alignment', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Text Alignment in Header Top Column 1', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> medicale_mascot_redux_text_alignment_list(),
				'default'	=> 'text-left flip',
			),
			array(
				'id'       => 'header-settings-header-top-column1-text-align-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),








			//Header Top Column 2
			array(
				'id'        => 'header-settings-header-top-column2-info-field-column2',
				'type'      => 'info',
				'title'     => esc_html__( 'Header Top Column 2 - Right Widget', 'medicale-wp' ),
				'subtitle'  => esc_html__( 'Choose content to display on column 2 (Left Widget) of header top', 'medicale-wp' ),
				'notice'    => false,
				'required'  => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),
			array(
				'id'       => 'header-settings-header-top-column2-content',
				'type'     => 'select',
				'title'    => esc_html__( 'Column 2 (Right Widget) - Content Types', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose content type to display on column 2.', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( '> You can choose multiple and rearrange them by moving left or right! %3$s> In case of %1$s"Social Links"%2$s, you will find it\'s settings at %1$sTheme Options > Social Links%2$s. %3$s> In case of %1$s"Header Top Navigation"%2$s, Please create a new menu from %1$sAppearance > Menus%2$s and set Theme Location %1$s"Column 2 - Header Top Navigation"%2$s', 'medicale-wp' ), '<strong>', '</strong>', '<br>'),
				'multi'    => true,
				'sortable' => true,
				'options'	=> array(
					'contact-info'      => esc_html__( 'Contact Info', 'medicale-wp' ),
					'custom-text'       => esc_html__( 'Custom Text', 'medicale-wp' ),
					'custom-button'     => esc_html__( 'Custom Button', 'medicale-wp' ),
					'header-top-nav'    => esc_html__( 'Header Top Navigation', 'medicale-wp' ),
					'social-links'      => esc_html__( 'Social Links', 'medicale-wp' ),
					'search-box'        => esc_html__( 'Search Box', 'medicale-wp' ),
					'login-register'    => esc_html__( 'Login/Register', 'medicale-wp' ),
					'wpml-languages'    => esc_html__( 'WPML Languages', 'medicale-wp' ),
					'checkout-button'   => esc_html__( 'Checkout Button', 'medicale-wp' ),
				),
				'default'	=> array(
					'header-top-nav',
				),
				'required' => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),


			//Column 2 - Contact Info
			array(
				'id'       => 'header-settings-header-top-column2-contact-info-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Column 2 - Contact Info', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-top-column2-content', '=', 'contact-info' )
				)
			),
			array(
				'id'       => 'header-settings-header-top-column2-contact-info',
				'type'     => 'sortable',
				'title'    => esc_html__( 'Column 2 - Contact Info Text', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter your custom contact info.', 'medicale-wp' ),
				'desc'     => esc_html__( 'Reorder them by moving up or down!', 'medicale-wp' ),
				'label'    => true,
				'options'	=> array(
					'phone'           => esc_html__( 'Phone', 'medicale-wp' ),
					'email'           => esc_html__( 'Email', 'medicale-wp' ),
					'address'         => esc_html__( 'Address', 'medicale-wp' ),
					'opening-hours'   => esc_html__( 'Opening Hours', 'medicale-wp' ),
				),
				'default' => array(
					'phone'           => esc_html__( 'Questions? Call us at: +123 4567 8901', 'medicale-wp' ),
					'email'           => esc_html__( 'info@yourdomain.com', 'medicale-wp' ),
					'address'         => esc_html__( 'Envato HQ 121 King Street, Melbourne', 'medicale-wp' ),
					'opening-hours'   => esc_html__( 'Mon-Fri 09:00-17:00', 'medicale-wp' ),
				),
			),
			array(
				'id'       => 'header-settings-header-top-column2-contact-info-checkbox',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Column 2 - Contact Info Visibility', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Show/Hide each item individually.', 'medicale-wp' ),
				'desc'     => esc_html__( 'Please choose which fields you want to display.', 'medicale-wp' ),
				'label'    => true,
				'options'	=> array(
					'phone'           => esc_html__( 'Phone', 'medicale-wp' ),
					'email'           => esc_html__( 'Email', 'medicale-wp' ),
					'address'         => esc_html__( 'Address', 'medicale-wp' ),
					'opening-hours'   => esc_html__( 'Opening Hours', 'medicale-wp' ),
				),
				'default' => array(
					'phone'           => 1,
					'email'           => 1,
					'address'         => 0,
					'opening-hours'   => 0,
				),
			),
			array(
				'id'       => 'header-settings-header-top-column2-contact-info-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-top-column2-content', '=', 'contact-info' )
				)
			),



			//Column 2 - Custom Text
			array(
				'id'       => 'header-settings-header-top-column2-custom-text-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Column 2 - Custom Text', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-top-column2-content', '=', 'custom-text' )
				)
			),
			array(
				'id'       => 'header-settings-header-top-column2-custom-text',
				'type'     => 'textarea',
				'title'    => esc_html__( 'Column 2 - Custom Text', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter your custom text. Custom HTML tags are allowed (wp_kses).', 'medicale-wp' ),
				'rows'     => '3',
				'desc'     => '',
				'default'	=> sprintf( esc_html__( 'Custom %1$sheader top%2$s text goes here!', 'medicale-wp' ), '<strong>', '</strong>'),
				'validate' => 'html_custom',
				'allowed_html' => array(
					'a' => array(
						'href' => array(),
						'title' => array()
					),
					'br' => array(),
					'em' => array(),
					'strong' => array()
				),
			),
			array(
				'id'       => 'header-settings-header-top-column2-custom-text-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-top-column2-content', '=', 'custom-text' )
				)
			),




			//Column 2 - Custom Button
			array(
				'id'       => 'header-settings-header-top-column2-custom-button-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Column 2 - Custom Button', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-top-column2-content', '=', 'custom-button' )
				)
			),
			array(
				'id'       => 'header-settings-header-top-column2-custom-button',
				'type'     => 'sortable',
				'title'    => esc_html__( 'Column 2 - Custom Button', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter your custom button info.', 'medicale-wp' ),
				'desc'     => esc_html__( 'Show a custom button in the header top.', 'medicale-wp' ),
				'label'    => true,
				'options'	=> array(
					'Button Title'  => '',
					'Button Link'   => '',
				),
				'default' => array(
					'Button Title'  => 'Custom Button',
					'Button Link'   => '#',
				),
			),
			array(
				'id'       => 'header-settings-header-top-column2-custom-button-design-style',
				'type'     => 'select',
				'title'    => esc_html__( 'Button Design Style', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'btn-default'   => esc_html__( 'Button Defaults', 'medicale-wp' ),
					'btn-border'    => esc_html__( 'Button Border', 'medicale-wp' ),
					'btn-dark'      => esc_html__( 'Button Dark', 'medicale-wp' ),
					'btn-gray'      => esc_html__( 'Button Gray', 'medicale-wp' ),
				),
				'default'	=> 'btn-gray',
			),
			array(
				'id'       => 'header-settings-header-top-column2-custom-button-theme-colored',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Make Button Theme Colored?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'To make the button theme colored, please check it.', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> '1',
			),
			array(
				'id'       => 'header-settings-header-top-column2-custom-button-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-top-column2-content', '=', 'custom-button' )
				)
			),




			//Column 2 - Social Links
			array(
				'id'       => 'header-settings-header-top-column2-social-links-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Column 2 - Social Links', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-top-column2-content', '=', 'social-links' )
				)
			),
			array(
				'id'       => 'header-settings-header-top-column2-social-links-color',
				'type'     => 'select',
				'title'    => esc_html__( 'Social Links Color', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'icon-dark'     => esc_html__( 'Dark', 'medicale-wp' ),
					''              => esc_html__( 'Default', 'medicale-wp' ),
					'icon-gray'     => esc_html__( 'Gray', 'medicale-wp' ),
				),
				'default'	=> '',
			),
			array(
				'id'       => 'header-settings-header-top-column2-social-links-icon-style',
				'type'     => 'select',
				'title'    => esc_html__( 'Social Links Icon Style', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'icon-rounded'   => esc_html__( 'Rounded', 'medicale-wp' ),
					''               => esc_html__( 'Default', 'medicale-wp' ),
					'icon-circled'   => esc_html__( 'Circled', 'medicale-wp' ),
				),
				'default'	=> '',
			),
			array(
				'id'       => 'header-settings-header-top-column2-social-links-icon-border-style',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Make Icon Area Bordered?', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> '0',
			),
			array(
				'id'       => 'header-settings-header-top-column2-social-links-theme-colored',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Make Social Links Theme Colored?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'To make the social links theme colored, please check it.', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> '0',
			),
			array(
				'id'       => 'header-settings-header-top-column2-social-links-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-top-column2-content', '=', 'social-links' )
				)
			),


			//Column 2 - Text Alignment
			array(
				'id'       => 'header-settings-header-top-column2-text-align-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Column 2 - Text Alignment', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),
			array(
				'id'       => 'header-settings-header-top-column2-text-align',
				'type'     => 'select',
				'title'    => esc_html__( 'Column 2 - Text Alignment', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Text Alignment in Header Top Column 2', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> medicale_mascot_redux_text_alignment_list(),
				'default'	=> 'text-right flip',
			),
			array(
				'id'       => 'header-settings-header-top-column2-text-align-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),





			//Header Top Other Settings
			array(
				'id'        => 'header-settings-header-top-info-field-other-settings',
				'type'      => 'info',
				'title'     => esc_html__( 'Header Top Other Settings', 'medicale-wp' ),
				'notice'    => false,
				'required'  => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),
			array(
				'id'       => 'header-settings-header-top-padding-top-bottom',
				'type'     => 'spacing',
				// An array of CSS selectors to apply this font style to
				'mode'     => 'padding',
				// absolute, padding, margin, defaults to padding
				'all'      => false,
				// Have one field that applies to all
				'top'           => true,     // Disable the top
				'right'         => false,     // Disable the right
				'bottom'        => true,     // Disable the bottom
				'left'          => false,     // Disable the left
				'units'         => 'px',      // You can specify a unit value. Possible: px, em, %
				//'units_extended'=> 'true',    // Allow users to select any type of unit
				'display_units' => true,   // Set to false to hide the units if the units are specified
				'title'    => esc_html__( 'Padding Top & Bottom(px)', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Padding for top and bottom in px.', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( 'Default Padding Top is 20px and Bottom is 20px. %1$sPlease put only integer value. Because the unit \'px\' will be automatically added to the end of the value.', 'medicale-wp' ), '<br>'),
				'required' => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),
			array(
				'id'       => 'header-settings-header-top-bgcolor-use-themecolor',
				'type'     => 'switch',
				'title'    => esc_html__( 'Use Theme Color in Background?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Use theme color or custom bg color', 'medicale-wp' ),
				'default'  => 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),
			array(
				'id'       => 'header-settings-header-top-custom-bgcolor',
				'type'     => 'color',
				'title'    => esc_html__( 'Custom Background Color', 'medicale-wp' ),
				'subtitle' => '',
				'subtitle' => esc_html__( 'Pick a custom background color for Header Top.', 'medicale-wp' ),
				'transparent' => true,
				'required' => array( 
					array( 'header-settings-show-header-top', '=', '1' ),
					array( 'header-settings-header-top-bgcolor-use-themecolor', '=', '0' ),
				)
			),
			/*array(
				'id'       => 'header-settings-header-top-has-border',
				'type'     => 'switch',
				'title'    => esc_html__( 'Top Bar in Grid Border?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Put visible grid border in top bar.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),*/




			//Header Top Typography
			array(
				'id'        => 'header-settings-header-top-info-field-typography',
				'type'      => 'info',
				'title'     => esc_html__( 'Header Top Typography', 'medicale-wp' ),
				'notice'    => false,
				'required'  => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),
			array(
				'id'            => 'header-settings-header-top-widget-text-typography',
				'type'          => 'typography',
				'title'         => esc_html__( 'Widget Text Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
				'required'      => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),
			array(
				'id'            => 'header-settings-header-top-widget-link-typography',
				'type'          => 'typography',
				'title'         => esc_html__( 'Widget Link Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
				'required'      => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),
			array(
				'id'       => 'header-settings-header-top-widget-link-hover-color',
				'type'     => 'color',
				'title'    => esc_html__( 'Widget Link Hover/Active Color', 'medicale-wp' ),
				'subtitle' => '',
				'transparent' => false,
				'required' => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),

		)
	) );


	// -> START Header Middle Row
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Header Middle Row', 'medicale-wp' ),
		'id'     => 'header-header-mid',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-arrow-up-alt',
		'subsection' => true,
		'fields' => array(

			//Header Visibility Important
			array(
				'id'        => 'header-settings-header-mid-info-field-important',
				'type'      => 'info',
				'title'     => esc_html__( 'Important!', 'medicale-wp' ),
				'subtitle'  => sprintf( esc_html__( 'As you have chosen %1$sHeader Visibility%2$s to %1$sHide%2$s so there\'s nothing to show here!', 'medicale-wp' ), '<strong>', '</strong>'),
				'notice'    => false,
				'required' => array( 'header-settings-choose-header-visibility', '!=', '1' ),
			),

			//Header Visibility Important
			array(
				'id'        => 'header-settings-header-mid-info-field-only-visible-header-2',
				'type'      => 'info',
				'title'     => esc_html__( 'Important! Features not available.', 'medicale-wp' ),
				'subtitle'  => esc_html__( 'Header Middle Row features are only available for "Header Layout Type - 3 Rows"! So please choose it from Header Layout tab.', 'medicale-wp' ),
				'notice'    => false,
				'required'  => array( 
					array( 'header-settings-choose-header-layout-type', '!=', 'header-2-3col' )
				)
			),

			//Header Middle Row Column 3
			array(
				'id'        => 'header-settings-header-mid-column3-info-field-column3',
				'type'      => 'info',
				'title'     => esc_html__( 'Column 3: Header Middle Row Right Widget', 'medicale-wp' ),
				'subtitle'  => esc_html__( 'Here we have treated Column 3 as the header middle row right widget.', 'medicale-wp' ),
				'notice'    => false,
				'required'  => array( 
					array( 'header-settings-choose-header-layout-type', '=', 'header-2-3col' )
				)
			),
			array(
				'id'       => 'header-settings-header-mid-column3-content',
				'type'     => 'select',
				'title'    => esc_html__( 'Column 3 - Content Types', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose content type to display on column 3.', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( '> You can choose multiple and rearrange them by moving left or right! %3$s> In case of %1$s"Social Links"%2$s, you will find it\'s settings at %1$sTheme Options > Social Links%2$s.', 'medicale-wp' ), '<strong>', '</strong>', '<br>'),
				'multi'    => true,
				'sortable' => true,
				'options'	=> array(
					'column3-contact-info'      => esc_html__( 'Contact Info', 'medicale-wp' ),
					'column3-custom-text'       => esc_html__( 'Custom Text', 'medicale-wp' ),
					'column3-custom-button'     => esc_html__( 'Custom Button', 'medicale-wp' ),
					'column3-social-links'      => esc_html__( 'Social Links', 'medicale-wp' ),
					'column3-search-box'        => esc_html__( 'Search Box', 'medicale-wp' ),
				),
				'default'	=> array(
					'column3-contact-info',
				),
				'required' => array( 
					array( 'header-settings-choose-header-layout-type', '=', 'header-2-3col' )
				)
			),


			//Column 3 - Contact Info
			array(
				'id'       => 'header-settings-header-mid-column3-contact-info-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Column 3 - Contact Info', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-mid-column3-content', '=', 'column3-contact-info' )
				)
			),
			array(
				'id'       => 'header-settings-header-mid-column3-contact-info-iconbox-style',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Icon Box Style', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select icon box style.', 'medicale-wp' ),
				'options' => array(
					'big-font-icon'     => esc_html__( 'Big Font Icon', 'medicale-wp' ),
					'small-font-icon'   => esc_html__( 'Small Font Icon', 'medicale-wp' )
				),
				'default' => 'big-font-icon',
			),
			array(
				'id'       => 'header-settings-header-mid-column3-contact-info-checkbox',
				'type'     => 'sortable',
				'title'    => esc_html__( 'Contact Info Visibility and Ordering', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Show/Hide each item individually. Reorder them by moving up or down!', 'medicale-wp' ),
				'desc'     => esc_html__( 'Please choose which fields you want to display.', 'medicale-wp' ),
				'label'    => true,
				'mode'     => 'checkbox',
				'options'	=> array(
					'phone'           => esc_html__( 'Phone', 'medicale-wp' ),
					'email'           => esc_html__( 'Email', 'medicale-wp' ),
					'address'         => esc_html__( 'Address', 'medicale-wp' ),
					'opening-hours'   => esc_html__( 'Opening Hours', 'medicale-wp' ),
				),
				'default' => array(
					'phone'           => 1,
					'email'           => 1,
					'address'         => 1,
					'opening-hours'   => 0,
				),
			),
			array(
				'id'       => 'header-settings-header-mid-column3-contact-info-phone',
				'type'     => 'sortable',
				'title'    => esc_html__( 'Contact Info - Phone', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter your custom contact info.', 'medicale-wp' ),
				'desc'     => esc_html__( 'Reorder them by moving up or down!', 'medicale-wp' ),
				'label'    => true,
				'options'	=> array(
					'Title'           => esc_html__( 'Phone', 'medicale-wp' ),
					'Subtitle'        => esc_html__( 'Phone', 'medicale-wp' ),
				),
				'default' => array(
					'Title'           => esc_html__( 'Call us at', 'medicale-wp' ),
					'Subtitle'        => esc_html__( '+123 4567 8901', 'medicale-wp' ),
				),
			),
			array(
				'id'       => 'header-settings-header-mid-column3-contact-info-email',
				'type'     => 'sortable',
				'title'    => esc_html__( 'Contact Info - Email', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter your custom contact info.', 'medicale-wp' ),
				'desc'     => esc_html__( 'Reorder them by moving up or down!', 'medicale-wp' ),
				'label'    => true,
				'options'	=> array(
					'Title'           => esc_html__( 'Email', 'medicale-wp' ),
					'Subtitle'        => esc_html__( 'Email', 'medicale-wp' ),
				),
				'default' => array(
					'Title'           => esc_html__( 'Email us', 'medicale-wp' ),
					'Subtitle'        => esc_html__( 'info@yourdomain.com', 'medicale-wp' ),
				),
			),
			array(
				'id'       => 'header-settings-header-mid-column3-contact-info-address',
				'type'     => 'sortable',
				'title'    => esc_html__( 'Contact Info - Address', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter your custom contact info.', 'medicale-wp' ),
				'desc'     => esc_html__( 'Reorder them by moving up or down!', 'medicale-wp' ),
				'label'    => true,
				'options'	=> array(
					'Title'           => esc_html__( 'Address', 'medicale-wp' ),
					'Subtitle'        => esc_html__( 'Address', 'medicale-wp' ),
				),
				'default' => array(
					'Title'           => esc_html__( 'Envato HQ', 'medicale-wp' ),
					'Subtitle'        => esc_html__( '121 King Street, Melbourne', 'medicale-wp' ),
				),
			),
			array(
				'id'       => 'header-settings-header-mid-column3-contact-info-opening-hours',
				'type'     => 'sortable',
				'title'    => esc_html__( 'Contact Info - Opening Hours', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter your custom contact info.', 'medicale-wp' ),
				'desc'     => esc_html__( 'Reorder them by moving up or down!', 'medicale-wp' ),
				'label'    => true,
				'options'	=> array(
					'Title'           => esc_html__( 'Opening Hours', 'medicale-wp' ),
					'Subtitle'        => esc_html__( 'Opening Hours', 'medicale-wp' ),
				),
				'default' => array(
					'Title'           => esc_html__( '09:00 - 17:00', 'medicale-wp' ),
					'Subtitle'        => esc_html__( 'Monday - Friday', 'medicale-wp' ),
				),
			),
			array(
				'id'       => 'header-settings-header-mid-column3-contact-info-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-mid-column3-content', '=', 'column3-contact-info' )
				)
			),



			//Column 3 - Custom Text
			array(
				'id'       => 'header-settings-header-mid-column3-custom-text-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Column 3 - Custom Text', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-mid-column3-content', '=', 'column3-custom-text' )
				)
			),
			array(
				'id'       => 'header-settings-header-mid-column3-custom-text',
				'type'     => 'textarea',
				'title'    => esc_html__( 'Column 3 - Custom Text', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter your custom text. Custom HTML tags are allowed (wp_kses).', 'medicale-wp' ),
				'rows'     => '3',
				'desc'     => '',
				'default'	=> sprintf( esc_html__( 'Custom %1$sheader middle row%2$s text goes here!', 'medicale-wp' ), '<strong>', '</strong>'),
				'validate' => 'html_custom',
				'allowed_html' => array(
					'a' => array(
						'href' => array(),
						'title' => array()
					),
					'br' => array(),
					'em' => array(),
					'strong' => array()
				),
			),
			array(
				'id'       => 'header-settings-header-mid-column3-custom-text-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-mid-column3-content', '=', 'column3-custom-text' )
				)
			),




			//Column 3 - Custom Button
			array(
				'id'       => 'header-settings-header-mid-column3-custom-button-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Column 3 - Custom Button', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-mid-column3-content', '=', 'column3-custom-button' )
				)
			),
			array(
				'id'       => 'header-settings-header-mid-column3-custom-button',
				'type'     => 'sortable',
				'title'    => esc_html__( 'Column 3 - Custom Button', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter your custom button info.', 'medicale-wp' ),
				'desc'     => esc_html__( 'Show a custom button in the header middle row.', 'medicale-wp' ),
				'label'    => true,
				'options'	=> array(
					'Button Title'  => '',
					'Button Link'   => '',
				),
				'default' => array(
					'Button Title'  => 'Custom Button',
					'Button Link'   => '#',
				),
			),
			array(
				'id'       => 'header-settings-header-mid-column3-custom-button-design-style',
				'type'     => 'select',
				'title'    => esc_html__( 'Button Design Style', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'btn-default'   => esc_html__( 'Button Defaults', 'medicale-wp' ),
					'btn-border'    => esc_html__( 'Button Border', 'medicale-wp' ),
					'btn-dark'      => esc_html__( 'Button Dark', 'medicale-wp' ),
					'btn-gray'      => esc_html__( 'Button Gray', 'medicale-wp' ),
				),
				'default'	=> 'btn-gray',
			),
			array(
				'id'       => 'header-settings-header-mid-column3-custom-button-theme-colored',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Make Button Theme Colored?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'To make the button theme colored, please check it.', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> '1',
			),
			array(
				'id'       => 'header-settings-header-mid-column3-custom-button-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-mid-column3-content', '=', 'column3-custom-button' )
				)
			),


			//Column 3 - Social Links
			array(
				'id'       => 'header-settings-header-mid-column3-social-links-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Column 3 - Social Links', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-mid-column3-content', '=', 'column3-social-links' )
				)
			),
			array(
				'id'       => 'header-settings-header-mid-column3-social-links-color',
				'type'     => 'select',
				'title'    => esc_html__( 'Social Links Color', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'icon-dark'     => esc_html__( 'Dark', 'medicale-wp' ),
					''              => esc_html__( 'Default', 'medicale-wp' ),
					'icon-gray'     => esc_html__( 'Gray', 'medicale-wp' ),
				),
				'default'	=> '',
			),
			array(
				'id'       => 'header-settings-header-mid-column3-social-links-icon-style',
				'type'     => 'select',
				'title'    => esc_html__( 'Social Links Icon Style', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'icon-rounded'   => esc_html__( 'Rounded', 'medicale-wp' ),
					''               => esc_html__( 'Default', 'medicale-wp' ),
					'icon-circled'   => esc_html__( 'Circled', 'medicale-wp' ),
				),
				'default'	=> '',
			),
			array(
				'id'       => 'header-settings-header-mid-column3-social-links-icon-border-style',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Make Icon Area Bordered?', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> '0',
			),
			array(
				'id'       => 'header-settings-header-mid-column3-social-links-theme-colored',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Make Social Links Theme Colored?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'To make the social links theme colored, please check it.', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> '0',
			),
			array(
				'id'       => 'header-settings-header-mid-column3-social-links-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'header-settings-header-mid-column3-content', '=', 'column3-social-links' )
				)
			),

		)
	) );



	// -> START Header Navigation Row
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Header Navigation Row', 'medicale-wp' ),
		'id'     => 'header-navigation-layout',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-arrow-up-alt',
		'subsection' => true,
		'fields' => array(

			//Header Visibility Important
			array(
				'id'        => 'header-settings-header-navigation-info-field-important',
				'type'      => 'info',
				'title'     => esc_html__( 'Important!', 'medicale-wp' ),
				'subtitle'  => sprintf( esc_html__( 'As you have chosen %1$sHeader Visibility%2$s to %1$sHide%2$s so there\'s nothing to show here!', 'medicale-wp' ), '<strong>', '</strong>'),
				'notice'    => false,
				'required' => array( 'header-settings-choose-header-visibility', '!=', '1' ),
			),



			
			array(
				'id'       => 'header-settings-navigation-show-header-nav-row',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Header Navigation Row', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enabling/Disabling this option will show/hide Whole Header Navigation Row section.', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 'header-settings-choose-header-visibility', '=', '1' ),
			),

			array(
				'id'       => 'header-settings-navigation-bgcolor-use-themecolor',
				'type'     => 'switch',
				'title'    => esc_html__( 'Use Theme Color in Background?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Use theme color or custom bg color in Header Navigation Row', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 'header-settings-choose-header-visibility', '=', '1' ),
			),
			array(
				'id'       => 'header-settings-navigation-custom-bgcolor',
				'type'     => 'color',
				'title'    => esc_html__( 'Custom Background Color', 'medicale-wp' ),
				'subtitle' => '',
				'subtitle' => esc_html__( 'Pick a custom background color for Header Navigation Row.', 'medicale-wp' ),
				'transparent' => true,
				'required' => array( 'header-settings-navigation-bgcolor-use-themecolor', '=', '0' ),
			),
			array(
				'id'       => 'header-settings-navigation-vertical-nav-bgimg',
				'type'     => 'background',
				'title'    => esc_html__( 'Background Image for Vertical Nav', 'medicale-wp' ),
				'subtitle' => sprintf( esc_html__( 'Set background image for Header Layout Type %1$sVertical Nav%2$s.', 'medicale-wp' ), '<strong>', '</strong>'),
				'background-color' => false,
				'required' => array( 'header-settings-choose-header-layout-type', '=', 'header-7-vertical-nav' ),
			),
			array(
				'id'       => 'header-settings-navigation-custom-navigation-link-n-icon-color',
				'type'     => 'color',
				'title'    => esc_html__( 'Navigation Link and Cart/Search/Side Push Icon Color', 'medicale-wp' ),
				'subtitle' => '',
				'subtitle' => esc_html__( 'Pick a custom color for link and icons on Header Navigation Row.', 'medicale-wp' ),
				'transparent' => true,
				'required' => array( 'header-settings-choose-header-visibility', '=', '1' ),
			),



			array(
				'id'       => 'header-settings-navigation-show-menu-cart-icon',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Cart Icon', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Add Cart Icon on the right hand side of the menu. WooCommerce plugin needs to be installed.', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => 'Yes',
				'off'      => 'No',
				'required' => array( 'header-settings-choose-header-visibility', '=', '1' ),
			),
			array(
				'id'       => 'header-settings-navigation-show-menu-search-icon',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Search Icon', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Add Search Icon on the right hand side of the menu.', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => 'Yes',
				'off'      => 'No',
				'required' => array( 'header-settings-choose-header-visibility', '=', '1' ),
			),
			array(
				'id'       => 'header-settings-navigation-show-side-push-panel',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Side Push Panel', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Add Side Push Icon on the right hand side of the menu to Enable/Disable Side Push Panel section. You can easily add your widgets to this section from Appearance > Widgets (Side Push Panel Sidebar)', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => 'Yes',
				'off'      => 'No',
				'required' => array( 'header-settings-choose-header-visibility', '=', '1' ),
			),



			//Header Nav - Custom Button
			array(
				'id'        => 'header-settings-navigation-custom-button-info-field',
				'type'      => 'info',
				'title'     => esc_html__( 'Custom Button', 'medicale-wp' ),
				'subtitle'  => esc_html__( 'Add Custom Button on the right hand side of the Header Navigation Row', 'medicale-wp' ),
				'notice'    => false,
				'required'  => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),
			array(
				'id'       => 'header-settings-navigation-show-custom-button',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Custom Button', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Add Custom Button on the right hand side of the Header Navigation Row.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => 'Yes',
				'off'      => 'No',
				'required' => array( 'header-settings-choose-header-visibility', '=', '1' ),
			),
			array(
				'id'       => 'header-settings-navigation-custom-button-info',
				'type'     => 'sortable',
				'title'    => esc_html__( 'Custom Button Info', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter your custom button info.', 'medicale-wp' ),
				'desc'     => esc_html__( 'Show a custom button in the Header Navigation Row.', 'medicale-wp' ),
				'label'    => true,
				'options'	=> array(
					'Button Title'  => '',
					'Button Link'   => '',
				),
				'default' => array(
					'Button Title'  => 'Custom Button',
					'Button Link'   => '#',
				),
				'required'  => array( 
					array( 'header-settings-navigation-show-custom-button', '=', '1' )
				)
			),
			array(
				'id'       => 'header-settings-navigation-custom-button-design-style',
				'type'     => 'select',
				'title'    => esc_html__( 'Button Design Style', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'btn-default'   => esc_html__( 'Button Defaults', 'medicale-wp' ),
					'btn-border'    => esc_html__( 'Button Border', 'medicale-wp' ),
					'btn-dark'      => esc_html__( 'Button Dark', 'medicale-wp' ),
					'btn-gray'      => esc_html__( 'Button Gray', 'medicale-wp' ),
				),
				'default'	=> 'btn-gray',
				'required'  => array( 
					array( 'header-settings-navigation-show-custom-button', '=', '1' )
				)
			),
			array(
				'id'       => 'header-settings-navigation-custom-button-theme-colored',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Make Button Theme Colored?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'To make the button theme colored, please check it.', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> '1',
				'required'  => array( 
					array( 'header-settings-navigation-show-custom-button', '=', '1' )
				)
			),





			//Header Nav - Navigation Skin
			array(
				'id'        => 'header-settings-navigation-skin-info-field',
				'type'      => 'info',
				'title'     => esc_html__( 'Navigation Skin', 'medicale-wp' ),
				'subtitle'  => esc_html__( 'Select the skin of main menu you would like to use', 'medicale-wp' ),
				'notice'    => false,
				'required'  => array( 
					array( 'header-settings-show-header-top', '=', '1' )
				)
			),
			array(
				'id'       => 'header-settings-navigation-skin',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Navigation Skin', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select the skin of main menu you would like to use', 'medicale-wp' ),
				'desc'     => '',
				//Must provide key => value(array:title|img) pairs for radio options
				'options'	=> array(
					'default' => array(
						'alt' => 'default',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/skins/default.jpg'
					),
					'border-bottom' => array(
						'alt' => 'border-bottom',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/skins/border-bottom.jpg'
					),
					'border-boxed' => array(
						'alt' => 'border-boxed',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/skins/border-boxed.jpg'
					),
					'border-left' => array(
						'alt' => 'border-left',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/skins/border-left.jpg'
					),
					'border-top' => array(
						'alt' => 'border-top',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/skins/border-top.jpg'
					),
					'border-top-bottom' => array(
						'alt' => 'border-top-bottom',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/skins/border-top-bottom.jpg'
					),
					'bottom-trace' => array(
						'alt' => 'bottom-trace',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/skins/bottom-trace.jpg'
					),
					'boxed' => array(
						'alt' => 'boxed',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/skins/boxed.jpg'
					),
					'colored' => array(
						'alt' => 'colored',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/skins/colored.jpg'
					),
					'dark' => array(
						'alt' => 'dark',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/skins/dark.jpg'
					),
					'gradient' => array(
						'alt' => 'gradient',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/skins/gradient.jpg'
					),
					'rounded-boxed' => array(
						'alt' => 'rounded-boxed',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/skins/rounded-boxed.jpg'
					),
					'shadow' => array(
						'alt' => 'shadow',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/skins/shadow.jpg'
					),
					'strip' => array(
						'alt' => 'strip',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/skins/strip.jpg'
					),
					'subcolored' => array(
						'alt' => 'subcolored',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/skins/subcolored.jpg'
					),
					'top-bottom-boxed' => array(
						'alt' => 'top-bottom-boxed',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/skins/top-bottom-boxed.jpg'
					),
				),
				'default'	=> 'boxed',
				'required' => array( 'header-settings-choose-header-visibility', '=', '1' ),
			),
			array(
				'id'       => 'header-settings-navigation-color-scheme',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Color Scheme', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select the color scheme of main menu', 'medicale-wp' ),
				'desc'     => '',
				//Must provide key => value(array:title|img) pairs for radio options
				'options'	=> array(
					'default' => array(
						'alt' => esc_html__( 'Default', 'medicale-wp' ),
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/colors/default.jpg '
					),
					'blue' => array(
						'alt' => esc_html__( 'Blue', 'medicale-wp' ),
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/colors/blue.jpg '
					),
					'green' => array(
						'alt' => esc_html__( 'Green', 'medicale-wp' ),
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/colors/green.jpg '
					),
					'orange' => array(
						'alt' => esc_html__( 'Orange', 'medicale-wp' ),
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/colors/orange.jpg '
					),
					'pink' => array(
						'alt' => esc_html__( 'Pink', 'medicale-wp' ),
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/colors/pink.jpg '
					),
					'purple' => array(
						'alt' => esc_html__( 'Purple', 'medicale-wp' ),
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/colors/purple.jpg '
					),
					'red' => array(
						'alt' => esc_html__( 'Red', 'medicale-wp' ),
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/colors/red.jpg '
					),
					'yellow' => array(
						'alt' => esc_html__( 'Yellow', 'medicale-wp' ),
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/top-menu-style/colors/yellow.jpg '
					)
				),
				'default'	=> 'default',
				'required' => array( 'header-settings-choose-header-visibility', '=', '1' ),
			),

			array(
				'id'       => 'header-settings-navigation-primary-effect',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Primary Effect', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> array(
					'fade'  => esc_html__( 'Fade', 'medicale-wp' ),
					'slide' => esc_html__( 'Slide', 'medicale-wp' )
				),
				'default'	=> 'fade',
				'required' => array( 'header-settings-choose-header-visibility', '=', '1' ),
			),
			array(
				'id'       => 'header-settings-navigation-css3-animation',
				'type'     => 'button_set',
				'title'    => esc_html__( 'CSS3 Animation', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> array(
					'none'      => esc_html__( 'None', 'medicale-wp' ),
					'zoom-in'   => esc_html__( 'Zoom In', 'medicale-wp' ),
					'zoom-out'  => esc_html__( 'Zoom Out', 'medicale-wp' ),
					'drop-up'   => esc_html__( 'Drop Up', 'medicale-wp' ),
					'drop-left' => esc_html__( 'Drop Left', 'medicale-wp' ),
					'swing'     => esc_html__( 'Swing', 'medicale-wp' ),
					'flip'      => esc_html__( 'Flip', 'medicale-wp' ),
					'roll-in'   => esc_html__( 'Roll In', 'medicale-wp' ),
					'stretch'   => esc_html__( 'Stretch', 'medicale-wp' ),
				),
				'default'	=> 'none',
				'required' => array( 'header-settings-choose-header-visibility', '=', '1' ),
			),

			
		)
	) );



	// -> START Menu
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Menu', 'medicale-wp' ),
		'id'     => 'header-menu',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-menu',
	) );



	// -> START Header Menu Megamenu
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Megamenu', 'medicale-wp' ),
		'id'     => 'header-menu-megamenu',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-menu',
		'subsection' => true,
		'fields' => array(
			array(
				'id'       => 'header-menu-megamenu-enable-megamenu',
				'type'     => 'switch',
				'title'    => esc_html__( 'Enable Mega Menu', 'medicale-wp' ),
				'subtitle' => sprintf( esc_html__( 'Turn on to enable mega menu. After enabling mega menu, you will get a lot of options for mega menu at %1$sAppearance > Menus%2$s', 'medicale-wp' ), '<strong>', '</strong>'),
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 'header-settings-choose-header-visibility', '=', '1' ),
			),
		)
	) );



	// -> START Layout Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Layout Settings', 'medicale-wp' ),
		'id'     => 'layout-settings',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-editor-table',
		'fields' => array(
			array(
				'id'        => 'layout-settings-page-layout',
				'type'      => 'button_set',
				'compiler'  => true,
				'title'     => esc_html__( 'Page Layout', 'medicale-wp' ),
				'subtitle'  => esc_html__( 'Select primary page layout of your theme', 'medicale-wp' ),
				'options'   => array(
					'boxed'        => esc_html__( 'Boxed', 'medicale-wp' ),
					'stretched'    => esc_html__( 'Stretched', 'medicale-wp' )
				),
				'default'   => 'stretched',
			),


			//section H3 Starts
			array(
				'id'       => 'layout-settings-boxed-layout-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Boxed Layout Settings', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Define styles for Boxed Layout.', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 'layout-settings-page-layout', '=', 'boxed' ),
			),
			array(
				'id'             => 'layout-settings-boxed-layout-padding-top-bottom',
				'type'           => 'spacing',
				'mode'           => 'padding',
				'all'            => false,
				// Have one field that applies to all
				'top'            => true,     // Disable the top
				'right'          => false,     // Disable the right
				'bottom'         => true,     // Disable the bottom
				'left'           => false,     // Disable the left
				'units'          => 'px',
				'units_extended' => 'true',
				'display_units'  => true,   // Set to false to hide the units if the units are specified
				'title'          => esc_html__( 'Padding Top & Bottom(px)', 'medicale-wp' ),
				'subtitle'       => 'Top and bottom padding in px for boxed layout.',
				'desc'           => 'Controls the top and bottom padding of the boxed layout. Ex: 40px, 40px. Please put only integer value. Because the unit \'px\' will be automatically added to the end of the value.',
				'default'            => array(
					'padding-top'     => '40', 
					'padding-bottom'  => '40', 
					'units'          => 'px', 
				)
			),
			array(
				'id'       => 'layout-settings-boxed-layout-container-shadow',
				'type'     => 'switch',
				'title'    => esc_html__( 'Container Shadow?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Add shadow around the container.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => 'On',
				'off'      => 'Off',
			),
			array(
				'id'       => 'layout-settings-boxed-layout-bg-type',
				'type'     => 'radio',
				'title'    => esc_html__( 'Background Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'You can use patterns, image or solid color as a background.', 'medicale-wp' ),
				'options'	=> array(
					'bg-color'     => esc_html__( 'Solid Color', 'medicale-wp' ),
					'bg-patter'    => esc_html__( 'Patterns from Theme Library', 'medicale-wp' ),
					'bg-image'     => esc_html__( 'Upload Own Image', 'medicale-wp' ),
				),
				'default'	=> 'bg-color',
			),
			array(
				'id'       => 'layout-settings-boxed-layout-bg-type-bgcolor',
				'type'     => 'color',
				'title'    => esc_html__( 'Background Solid Color', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Pick a custom color for background (default: #444).', 'medicale-wp' ),
				'default'	=> '#444',
				'transparent' => true,
				'required' => array( 'layout-settings-boxed-layout-bg-type', '=', 'bg-color' ),
			),
			array(
				'id'       => 'layout-settings-boxed-layout-bg-type-pattern',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Choose Patterns from Theme Library', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select a patterns by clicking on it.', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> $sample_patterns,
				'default'	=> key($sample_patterns),
				'required' => array( 'layout-settings-boxed-layout-bg-type', '=', 'bg-patter' ),
			),
			array(
				'id'       => 'layout-settings-boxed-layout-bg-type-bgimg',
				'type'     => 'background',
				'title'    => esc_html__( 'Background Image', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Body background image.', 'medicale-wp' ),
				'background-color' => false,
				'required' => array( 'layout-settings-boxed-layout-bg-type', '=', 'bg-image' ),
			),
			array(
				'id'       => 'layout-settings-boxed-layout-ends',
				'type'     => 'section',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
			),



			array(
				'id'       => 'layout-settings-content-width',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Content Width', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select content width. You can use any width by using custom CSS', 'medicale-wp' ),
				'options' => array(
					'container-970px'     => esc_html__( '970px', 'medicale-wp' ),
					'container-default'   => esc_html__( '1170px (Bootstrap Default)', 'medicale-wp' ),
					'container-100pr'     => esc_html__( 'Fullwidth 100%', 'medicale-wp' )
				),
				'default' => 'container-default',
			),
		)
	) );



	// -> START Footer
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Footer Call Out', 'medicale-wp' ),
		'id'     => 'footer-top-area',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-editor-insertmore',
	) );


	// -> START Footer Top Call Out Area
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Footer Top Call Out Area', 'medicale-wp' ),
		'id'     => 'footer-top-call-out-area',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-editor-insertmore',
		'subsection' => true,
		'fields' => array(
			array(
				'id'       => 'footer-top-call-out-area-visibility',
				'type'     => 'switch',
				'title'    => esc_html__( 'Footer Top Callout', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Show or hide footer top callout area globally', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => 'Show',
				'off'      => 'Hide',
			),

			//section Callout Text
			array(
				'id'       => 'footer-top-call-out-area-text-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Callout Text', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'footer-top-call-out-area-visibility', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-top-call-out-area-text',
				'type'     => 'editor',
				'title'    => esc_html__( 'Callout Text', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter your custom content for your footer callout.', 'medicale-wp' ),
				'desc'     => '',
				'args'     => array(
					'textarea_rows'    => 5
				),
				'default'	=> esc_html__( 'This is sample footer callout text. From here you can add some relevant information about your company or product. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'medicale-wp' ),
			),
			array(
				'id'       => 'footer-top-call-out-area-text-color',
				'type'     => 'color',
				'title'    => esc_html__( 'Callout Area Text Color', 'medicale-wp' ),
				'subtitle' => '',
				'transparent' => false,
			),
			array(
				'id'            => 'footer-top-call-out-area-text-font-size',
				'type'          => 'slider',
				'title'         => esc_html__( 'Font Size(px)', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Default: 18px', 'medicale-wp' ),
				'default'       => 18,
				'min'           => 1,
				'step'          => 1,
				'max'           => 100,
				'display_value' => 'text',
			),
			array(
				'id'       => 'footer-top-call-out-area-text-align',
				'type'     => 'select',
				'title'    => esc_html__( 'Text Alignment', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> medicale_mascot_redux_text_alignment_list(),
				'default'	=> '',
			),
			array(
				'id'       => 'footer-top-call-out-area-text-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'footer-top-call-out-area-visibility', '=', '1' )
				)
			),


			//section Left Font Icon
			array(
				'id'       => 'footer-top-call-out-area-left-font-icon-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Left Font Icon', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'footer-top-call-out-area-visibility', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-top-call-out-area-left-font-icon',
				'type'     => 'text',
				'title'    => esc_html__( 'Font Icon', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( 'Example: "fa fa-diamond". Collect your own icon from here: %1$sFontAwesome%2$s.', 'medicale-wp' ), '<a target="_blank" href="' . esc_url( 'http://fontawesome.io/icons/' ) . '">', '</a>' ),
			),
			array(
				'id'       => 'footer-top-call-out-area-left-font-icon-position',
				'type'     => 'select',
				'title'    => esc_html__( 'Icon Position', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> array(
					'left' => 'Left',
					'top'  => 'Top',
				),
				'default'	=> 'left',
			),
			array(
				'id'            => 'footer-top-call-out-area-left-font-icon-fontsize',
				'type'          => 'slider',
				'title'         => esc_html__( 'Font Size(px)', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Default: 48px', 'medicale-wp' ),
				'default'       => 48,
				'min'           => 1,
				'step'          => 1,
				'max'           => 200,
				'display_value' => 'text',
			),
			array(
				'id'       => 'footer-top-call-out-area-left-font-icon-color',
				'type'     => 'color',
				'title'    => esc_html__( 'Icon Color', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Pick a custom color for Left Font Icon.', 'medicale-wp' ),
				'transparent' => false,
			),
			array(
				'id'       => 'footer-top-call-out-area-left-font-icon-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'footer-top-call-out-area-visibility', '=', '1' )
				)
			),




			//section Callout Button
			array(
				'id'       => 'footer-top-call-out-area-button-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Callout Button', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'footer-top-call-out-area-visibility', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-top-call-out-area-button-visibility',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Callout Button', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Show or hide callout button.', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => 'Show',
				'off'      => 'Hide',
			),
			array(
				'id'       => 'footer-top-call-out-area-button-position',
				'type'     => 'select',
				'title'    => esc_html__( 'Button Position', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> array(
					'button-right'     => 'Right',
					'button-bottom'    => 'Bottom',
				),
				'default'	=> 'button-right',
				'required' => array( 
					array( 'footer-top-call-out-area-button-visibility', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-top-call-out-area-button-text',
				'type'     => 'text',
				'title'    => esc_html__( 'Button Text', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter the text that will be appeared in callout button.', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> esc_html__( 'Get Started!', 'medicale-wp' ),
				'required' => array( 
					array( 'footer-top-call-out-area-button-visibility', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-top-call-out-area-button-link',
				'type'     => 'text',
				'title'    => esc_html__( 'Button Link', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter the URL for the callout button.', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> esc_html__( 'https://themeforest.net/', 'medicale-wp' ),
				'required' => array( 
					array( 'footer-top-call-out-area-button-visibility', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-top-call-out-area-button-link-open-in-window',
				'type'     => 'select',
				'title'    => esc_html__( 'Open Button Link in', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> array(
					'_blank' => 'New Tab',
					'_self'  => 'Same Tab',
				),
				'default'	=> '_blank',
				'required' => array( 
					array( 'footer-top-call-out-area-button-visibility', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-top-call-out-area-button-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'footer-top-call-out-area-visibility', '=', '1' )
				)
			),




			//section Other Settings
			array(
				'id'       => 'footer-top-call-out-area-other-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Other Settings', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'footer-top-call-out-area-visibility', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-top-call-out-area-bgcolor',
				'type'     => 'color',
				'title'    => esc_html__( 'Callout Area Background Color', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Pick a custom background color for callout area.', 'medicale-wp' ),
				'default'	=> '#f5f5f5',
				'transparent' => false,
			),
			array(
				'id'       => 'footer-top-call-out-area-border-top-color',
				'type'     => 'color',
				'title'    => esc_html__( 'Callout Area Border Top Color', 'medicale-wp' ),
				'subtitle' => '',
				'transparent' => false,
			),
			array(
				'id'       => 'footer-top-call-out-area-border-bottom-color',
				'type'     => 'color',
				'title'    => esc_html__( 'Callout Area Border Bottom Color', 'medicale-wp' ),
				'subtitle' => '',
				'transparent' => false,
			),
			array(
				'id'       => 'footer-top-call-out-area-other-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'footer-top-call-out-area-visibility', '=', '1' )
				)
			),
		)
	) );



	// -> START Footer
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Footer', 'medicale-wp' ),
		'id'     => 'footer',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-arrow-down-alt',
	) );

	// -> START Footer Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Footer Settings', 'medicale-wp' ),
		'id'     => 'footer-settings',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-arrow-down-alt2',
		'subsection' => true,
		'fields' => array(
			array(
				'id'       => 'footer-settings-footer-visibility',
				'type'     => 'switch',
				'title'    => esc_html__( 'Footer Visibility', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Show or hide footer globally', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => 'Show',
				'off'      => 'Hide',
			),
			array(
				'id'       => 'footer-settings-fixed-footer-bottom',
				'type'     => 'switch',
				'title'    => esc_html__( 'Fixed Footer Bottom Effect', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enabling this option will make Footer gradually appear on scroll. This is popular for OnePage Websites.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 'footer-settings-footer-visibility', '=', '1' ),
			),
			array(
				'id'       => 'footer-settings-footer-text-color',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Footer BG + Text Color', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select footer text color. Inverted will turn font color to black. Inverted is suitable for white background.', 'medicale-wp' ),
				'options'	=> array(
					'footer-black'      => esc_html__( 'Default (Dark BG + White Text )', 'medicale-wp' ),
					'footer-inverted'   => esc_html__( 'Inverted (White BG + Black Text )', 'medicale-wp' )
				),
				'default' => 'footer-black',
				'required' => array( 'footer-settings-footer-visibility', '=', '1' ),
			),
		)
	) );


	// -> START Footer Top
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Footer Top', 'medicale-wp' ),
		'id'     => 'footer-top',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-arrow-down-alt2',
		'subsection' => true,
		'fields' => array(
			//show footer top
			array(
				'id'       => 'footer-settings-show-footer-top',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Footer Top', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enabling this option will show Footer Top section', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 'footer-settings-footer-visibility', '=', '1' ),
			),
			array(
				'id'       => 'footer-settings-footer-top-columns-layout',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Choose Footer Top Columns Layout', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose layout of the Footer Top area.', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( 'These are footer sidebar widget areas. You can add/remove widgets from Appearance > Widgets. %3$s%1$sCorresponding Sidebars are:%2$s %3$s1. Footer Top Column 1 %3$s2. Footer Top Column 2 %3$s3. Footer Top Column 3 %3$s4. Footer Top Column 4 %3$s', 'medicale-wp' ), '<strong>', '</strong>', '<br>'),
				//Must provide key => value(array:title|img) pairs for radio options
				'options'	=> array(
					'25-25-25-25' => array(
						'alt' => '1/4 + 1/4 + 1/4 + 1/4',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f1.jpg'
					),
					'33-33-33' => array(
						'alt' => '1/3 + 1/3 + 1/3',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f2.jpg'
					),
					'25-50-25' => array(
						'alt' => '1/4 + 1/2 + 1/4',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f3.jpg'
					),
					'25-25-50' => array(
						'alt' => '1/4 + 1/4 + 1/2',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f4.jpg'
					),
					'50-25-25' => array(
						'alt' => '1/2 + 1/4 + 1/4',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f5.jpg'
					),
					'50-50' => array(
						'alt' => '1/2 + 1/2',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f6.jpg'
					),
					'66-33' => array(
						'alt' => '2/3 + 1/3',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f7.jpg'
					),
					'33-66' => array(
						'alt' => '1/3 + 2/3',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f8.jpg'
					),
					'25-75' => array(
						'alt' => '1/4 + 3/4',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f9.jpg'
					),
					'75-25' => array(
						'alt' => '3/4 + 1/4',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f10.jpg'
					),
					'100' => array(
						'alt' => '1/1',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f11.jpg'
					)
				),
				'default'	=> '25-25-25-25',
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-top', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-settings-footer-top-container',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Footer Top Container', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Put footer top content boxed or stretched fullwidth.', 'medicale-wp' ),
				'options'	=> array(
					'container' => esc_html__( 'Container', 'medicale-wp' ),
					'container-fluid' => esc_html__( 'Container Fluid', 'medicale-wp' )
				),
				'default' => 'container',
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-top', '=', '1' )
				)
			),


			
			array(
				'id'        => 'footer-settings-footer-top-background-settings',
				'type'      => 'info',
				'title'     => esc_html__( 'Background Settings', 'medicale-wp' ),
				'notice'    => false,
				'required'  => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-top', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-settings-footer-top-bg',
				'type'     => 'background',
				'title'    => esc_html__( 'Footer Top Background', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose footer top background image or color.', 'medicale-wp' ),
				'default'	=> array(
					'background-color'      => '',
					'background-repeat'     => 'no-repeat',
					'background-size'       => 'cover',
					'background-attachment' => '',
					'background-position'   => 'center center',
					'background-image'      => '',
				),
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-top', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-settings-footer-top-bg-parallax-effect',
				'type'     => 'switch',
				'title'    => esc_html__( 'Footer Top Background Parallax Effect', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Parallax effect only for background image of footer top.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-top', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-settings-footer-top-bg-layer-overlay-status',
				'type'     => 'switch',
				'title'    => esc_html__( 'Add Background Overlay', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-top', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-settings-footer-top-bg-layer-overlay',
				'type'          => 'slider',
				'title'         => esc_html__( 'Footer Top Background Overlay', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Overlay on background image on footer top.', 'medicale-wp' ),
				'desc'          => '',
				'default'       => 7,
				'min'           => 1,
				'step'          => 1,
				'max'           => 9,
				'display_value' => 'text',
				'required' => array( 'footer-settings-footer-top-bg-layer-overlay-status', '=', '1' ),
			),
			array(
				'id'       => 'footer-settings-footer-top-bg-layer-overlay-color',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Footer Top Background Overlay Color', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select Dark or White Overlay on background image.', 'medicale-wp' ),
				'options'	=> array(
					'dark'          => esc_html__( 'Dark', 'medicale-wp' ),
					'white'         => esc_html__( 'White', 'medicale-wp' ),
					'theme-colored' => esc_html__( 'Primary Theme Color', 'medicale-wp' )
				),
				'default' => 'dark',
				'required' => array( 'footer-settings-footer-top-bg-layer-overlay-status', '=', '1' ),
			),




			
			array(
				'id'        => 'footer-settings-footer-top-other-settings',
				'type'      => 'info',
				'title'     => esc_html__( 'Other Settings', 'medicale-wp' ),
				'notice'    => false,
				'required'  => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-top', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-settings-show-footer-top-column-separator',
				'type'     => 'switch',
				'title'    => esc_html__( 'Footer Column Separator', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Display vertical line separator between widget columns.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-top', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-settings-footer-top-text-align',
				'type'     => 'select',
				'title'    => esc_html__( 'Footer Top Columns Text Alignment', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Text Alignment in Footer Columns', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> medicale_mascot_redux_text_alignment_list(),
				'default'	=> '',
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-top', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-settings-footer-top-padding-top-bottom',
				'type'     => 'spacing',
				// An array of CSS selectors to apply this font style to
				'mode'     => 'padding',
				// absolute, padding, margin, defaults to padding
				'all'      => false,
				// Have one field that applies to all
				'top'           => true,     // Disable the top
				'right'         => false,     // Disable the right
				'bottom'        => true,     // Disable the bottom
				'left'          => false,     // Disable the left
				'units'         => 'px',      // You can specify a unit value. Possible: px, em, %
				//'units_extended'=> 'true',    // Allow users to select any type of unit
				'display_units' => true,   // Set to false to hide the units if the units are specified
				'title'    => esc_html__( 'Padding Top & Bottom(px)', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Padding for top and bottom in px.', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( 'Default Padding Top is 50px and Bottom is 50px. %1$sPlease put only integer value. Because the unit \'px\' will be automatically added to the end of the value.', 'medicale-wp' ), '<br>'),
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-top', '=', '1' )
				)
			),


			array(
				'id'        => 'footer-settings-footer-top-typography',
				'type'      => 'info',
				'title'     => esc_html__( 'Typography', 'medicale-wp' ),
				'notice'    => false,
				'required'  => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-top', '=', '1' )
				)
			),
			array(
				'id'            => 'footer-settings-footer-top-widget-title-typography',
				'type'          => 'typography',
				'title'         => esc_html__( 'Widget Title Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
				'required'      => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-top', '=', '1' )
				)
			),
			array(
				'id'            => 'footer-settings-footer-top-widget-text-typography',
				'type'          => 'typography',
				'title'         => esc_html__( 'Widget Text Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
				'required'      => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-top', '=', '1' )
				)
			),
			array(
				'id'            => 'footer-settings-footer-top-widget-link-typography',
				'type'          => 'typography',
				'title'         => esc_html__( 'Widget Link Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
				'required'      => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-top', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-settings-footer-top-widget-link-hover-color',
				'type'     => 'color',
				'title'    => esc_html__( 'Widget Link Hover/Active Color', 'medicale-wp' ),
				'subtitle' => '',
				'transparent' => false,
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-top', '=', '1' )
				)
			),
		)
	) );


	// -> START Footer Bottom
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Footer Bottom', 'medicale-wp' ),
		'id'     => 'footer-bottom',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-arrow-down-alt2',
		'subsection' => true,
		'fields' => array(
			//show footer bottom
			array(
				'id'       => 'footer-settings-show-footer-bottom',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Footer Bottom', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enabling this option will show Footer Bottom section', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 'footer-settings-footer-visibility', '=', '1' ),
			),
			array(
				'id'       => 'footer-settings-footer-bottom-container',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Footer Bottom Container', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Put footer bottom content boxed or stretched fullwidth.', 'medicale-wp' ),
				'options'	=> array(
					'container' => esc_html__( 'Container', 'medicale-wp' ),
					'container-fluid' => esc_html__( 'Container Fluid', 'medicale-wp' )
				),
				'default' => 'container',
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-bottom', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-settings-footer-bottom-columns-layout',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Choose Footer Bottom Columns Layout', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose layout of the Footer Bottom area.', 'medicale-wp' ),
				'desc'     => '',
				//Must provide key => value(array:title|img) pairs for radio options
				'options'	=> array(
					'100' => array(
						'alt' => '1/1',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f11.jpg'
					),
					'50-50' => array(
						'alt' => '1/2 + 1/2',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f6.jpg'
					),
					'66-33' => array(
						'alt' => '2/3 + 1/3',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f7.jpg'
					),
					'33-66' => array(
						'alt' => '1/3 + 2/3',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f8.jpg'
					),
					'75-25' => array(
						'alt' => '3/4 + 1/4',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f10.jpg'
					),
					'25-75' => array(
						'alt' => '1/4 + 3/4',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f9.jpg'
					),
					'50-25-25' => array(
						'alt' => '1/2 + 1/4 + 1/4',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f5.jpg'
					),
					'25-25-50' => array(
						'alt' => '1/4 + 1/4 + 1/2',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f4.jpg'
					),
					'25-50-25' => array(
						'alt' => '1/4 + 1/2 + 1/4',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f3.jpg'
					),
					'33-33-33' => array(
						'alt' => '1/3 + 1/3 + 1/3',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f2.jpg'
					),
				),
				'default'	=> '66-33',
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-bottom', '=', '1' )
				)
			),


			array(
				'id'        => 'footer-settings-footer-bottom-column1-info-field',
				'type'      => 'info',
				'title'     => esc_html__( 'Column 1', 'medicale-wp' ),
				'notice'    => false,
				'required'  => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-bottom', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-settings-footer-bottom-column1-content',
				'type'     => 'select',
				'title'    => esc_html__( 'Column 1 - Content Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose content type to display on column 1.', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( '> You can choose multiple and rearrange them by moving left or right! %3$s> In case of %1$s"Social Links"%2$s, you will find it\'s settings at %1$sTheme Options > Social Links%2$s. %3$s> In case of %1$s"Footer Bottom Navigation"%2$s, Please create a new menu from %1$sAppearance > Menus%2$s and set Theme Location %1$s"Columns 1 - Footer Bottom Navigation"%2$s', 'medicale-wp' ), '<strong>', '</strong>', '<br>'),
				'multi'    => true,
				'sortable' => true,
				'options'	=> array(
					'copyright-text'  => esc_html__( 'Copyright Text', 'medicale-wp' ),
					'footer-nav'      => esc_html__( 'Footer Bottom Navigation', 'medicale-wp' ),
					'social-links'    => esc_html__( 'Social Links', 'medicale-wp' ),
				),
				'default'	=> array(
					'copyright-text',
				),
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-bottom', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-settings-footer-bottom-column1-copyright-text',
				'type'     => 'textarea',
				'title'    => esc_html__( 'Column 1 - Copyright Text', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter your custom copyright text. Custom HTML tags are allowed (wp_kses). Use [current-year] shortcode to show current year.', 'medicale-wp' ),
				'rows'     => '3',
				'desc'     => '',
				'default'	=> sprintf( esc_html__( '&copy; Copyright [current-year] - All Rights Reserved - By %1$sKodeSolution%2$s', 'medicale-wp' ), '<a target="_blank" href="' . esc_url( '#' ) . '">', '</a>' ),
				'validate' => 'html_custom',
				'allowed_html' => array(
					'a' => array(
						'href' => array(),
						'title' => array()
					),
					'br' => array(),
					'em' => array(),
					'strong' => array()
				),
				'required' => array( 
					array( 'footer-settings-footer-bottom-column1-content', '=', 'copyright-text' )
				)
			),
			array(
				'id'       => 'footer-settings-footer-bottom-column1-text-align',
				'type'     => 'select',
				'title'    => esc_html__( 'Column 1 - Text Alignment', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Text Alignment in Footer Bottom Column 1', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> medicale_mascot_redux_text_alignment_list(),
				'default'	=> 'text-left flip',
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-bottom', '=', '1' )
				)
			),


			array(
				'id'         => 'footer-settings-footer-bottom-column2-info-field',
				'type'      => 'info',
				'title'     => esc_html__( 'Columns 2', 'medicale-wp' ),
				'notice'    => false,
				'required'  => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-bottom', '=', '1' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '100' )
				)
			),
			array(
				'id'       => 'footer-settings-footer-bottom-column2-content',
				'type'     => 'select',
				'title'    => esc_html__( 'Columns 2 - Content Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose content type to display on column 2.', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( '> You can choose multiple and rearrange them by moving left or right! %3$s> In case of %1$s"Social Links"%2$s, you will find it\'s settings at %1$sTheme Options > Social Links%2$s. %3$s> In case of %1$s"Footer Bottom Navigation"%2$s, Please create a new menu from %1$sAppearance > Menus%2$s and set Theme Location %1$s"Columns 2 - Footer Bottom Navigation"%2$s', 'medicale-wp' ), '<strong>', '</strong>', '<br>'),
				'multi'    => true,
				'sortable' => true,
				'options'	=> array(
					'copyright-text'  => esc_html__( 'Copyright Text', 'medicale-wp' ),
					'footer-nav'      => esc_html__( 'Footer Bottom Navigation', 'medicale-wp' ),
					'social-links'    => esc_html__( 'Social Links', 'medicale-wp' ),
				),
				'default'	=> array(
					'footer-nav',
				),
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-bottom', '=', '1' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '100' )
				)
			),
			array(
				'id'       => 'footer-settings-footer-bottom-column2-copyright-text',
				'type'     => 'textarea',
				'title'    => esc_html__( 'Columns 2 - Copyright Text', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter your custom copyright text. Custom HTML tags are allowed (wp_kses). Use [current-year] shortcode to show current year.', 'medicale-wp' ),
				'rows'     => '3',
				'desc'     => '',
				'default'	=> sprintf( esc_html__( '&copy; Copyright [current-year] - All Rights Reserved - By %1$sKodeSolution%2$s', 'medicale-wp' ), '<a target="_blank" href="' . esc_url( '#' ) . '">', '</a>' ),
				'validate' => 'html_custom',
				'allowed_html' => array(
					'a' => array(
						'href' => array(),
						'title' => array()
					),
					'br' => array(),
					'em' => array(),
					'strong' => array()
				),
				'required' => array( 
					array( 'footer-settings-footer-bottom-column2-content', '=', 'copyright-text' )
				)
			),
			array(
				'id'       => 'footer-settings-footer-bottom-column2-text-align',
				'type'     => 'select',
				'title'    => esc_html__( 'Column 2 - Text Alignment', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Text Alignment in Footer Bottom Column 1', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> medicale_mascot_redux_text_alignment_list(),
				'default'	=> 'text-right flip',
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-bottom', '=', '1' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '100' )
				)
			),


			array(
				'id'        => 'footer-settings-footer-bottom-column3-info-field',
				'type'      => 'info',
				'title'     => esc_html__( 'Columns 3', 'medicale-wp' ),
				'notice'    => false,
				'required'  => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-bottom', '=', '1' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '100' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '50-50' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '66-33' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '33-66' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '75-25' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '25-75' ),
				)
			),
			array(
				'id'       => 'footer-settings-footer-bottom-column3-content',
				'type'     => 'select',
				'title'    => esc_html__( 'Columns 3 - Content Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose content type to display on column 3.', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( '> You can choose multiple and rearrange them by moving left or right! %3$s> In case of %1$s"Social Links"%2$s, you will find it\'s settings at %1$sTheme Options > Social Links%2$s. %3$s> In case of %1$s"Footer Bottom Navigation"%2$s, Please create a new menu from %1$sAppearance > Menus%2$s and set Theme Location %1$s"Columns 3 - Footer Bottom Navigation"%2$s', 'medicale-wp' ), '<strong>', '</strong>', '<br>'),
				'multi'    => true,
				'sortable' => true,
				'options'	=> array(
					'copyright-text'  => esc_html__( 'Copyright Text', 'medicale-wp' ),
					'footer-nav'      => esc_html__( 'Footer Bottom Navigation', 'medicale-wp' ),
					'social-links'    => esc_html__( 'Social Links', 'medicale-wp' ),
				),
				'default'	=> array(
					'footer-nav',
				),
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-bottom', '=', '1' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '100' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '50-50' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '66-33' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '33-66' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '75-25' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '25-75' ),
				)
			),
			array(
				'id'       => 'footer-settings-footer-bottom-column3-copyright-text',
				'type'     => 'textarea',
				'title'    => esc_html__( 'Copyright Text', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter your custom copyright text. Custom HTML tags are allowed (wp_kses). Use [current-year] shortcode to show current year.', 'medicale-wp' ),
				'rows'     => '3',
				'desc'     => '',
				'default'  => sprintf( esc_html__( '&copy; Copyright [current-year] - All Rights Reserved - By %1$sKodeSolution%2$s', 'medicale-wp' ), '<a target="_blank" href="' . esc_url( '#' ) . '">', '</a>' ),
				'validate' => 'html_custom',
				'allowed_html' => array(
					'a' => array(
						'href' => array(),
						'title' => array()
					),
					'br' => array(),
					'em' => array(),
					'strong' => array()
				),
				'required' => array( 
					array( 'footer-settings-footer-bottom-column3-content', '=', 'copyright-text' )
				)
			),
			array(
				'id'       => 'footer-settings-footer-bottom-column3-text-align',
				'type'     => 'select',
				'title'    => esc_html__( 'Columns 3 - Text Alignment', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Text Alignment in Footer Bottom Column 1', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> medicale_mascot_redux_text_alignment_list(),
				'default'	=> 'text-right flip',
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-bottom', '=', '1' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '100' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '50-50' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '66-33' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '33-66' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '75-25' ),
					array( 'footer-settings-footer-bottom-columns-layout', '!=', '25-75' ),
				)
			),



			array(
				'id'        => 'footer-settings-footer-bottom-other-info-field',
				'type'      => 'info',
				'title'     => esc_html__( 'Other Settings', 'medicale-wp' ),
				'notice'    => false,
				'required'  => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-bottom', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-settings-footer-bottom-padding-top-bottom',
				'type'     => 'spacing',
				// An array of CSS selectors to apply this font style to
				'mode'     => 'padding',
				// absolute, padding, margin, defaults to padding
				'all'      => false,
				// Have one field that applies to all
				'top'           => true,     // Disable the top
				'right'         => false,     // Disable the right
				'bottom'        => true,     // Disable the bottom
				'left'          => false,     // Disable the left
				'units'         => 'px',      // You can specify a unit value. Possible: px, em, %
				//'units_extended'=> 'true',    // Allow users to select any type of unit
				'display_units' => true,   // Set to false to hide the units if the units are specified
				'title'    => esc_html__( 'Padding Top & Bottom(px)', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Padding top and bottom in px for footer bottom.', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( 'Default Padding Top is 15px and Bottom is 15px. %1$sPlease put only integer value. Because the unit \'px\' will be automatically added to the end of the value.', 'medicale-wp' ), '<br>'),
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-bottom', '=', '1' )
				)
			),
			array(
				'id'       => 'footer-settings-footer-bottom-bgcolor',
				'type'     => 'color',
				'title'    => esc_html__( 'Background Color', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Pick a custom background color for Footer Bottom (default: #333).', 'medicale-wp' ),
				'transparent' => true,
				'required' => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-bottom', '=', '1' )
				)
			),



			array(
				'id'        => 'footer-settings-footer-bottom-typography',
				'type'      => 'info',
				'title'     => esc_html__( 'Typography', 'medicale-wp' ),
				'notice'    => false,
				'required'  => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-bottom', '=', '1' )
				)
			),
			array(
				'id'            => 'footer-settings-footer-bottom-widget-text-typography',
				'type'          => 'typography',
				'title'         => esc_html__( 'Text Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
				'required'      => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-bottom', '=', '1' )
				)
			),
			array(
				'id'            => 'footer-settings-footer-bottom-widget-link-typography',
				'type'          => 'typography',
				'title'         => esc_html__( 'Link Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
				'required'      => array( 
					array( 'footer-settings-footer-visibility', '=', '1' ),
					array( 'footer-settings-show-footer-bottom', '=', '1' )
				)
			),
		)
	) );



	// -> START Typography
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Typography', 'medicale-wp' ),
		'id'     => 'typography',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-editor-textcolor',
	) );

	// -> START Body Typography
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Body Typography', 'medicale-wp' ),
		'id'     => 'primary-body-typography',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-editor-textcolor',
		'subsection' => true,
		'fields' => array(
			array(
				'id'            => 'typography-primary-body',
				'type'          => 'typography',
				'title'         => esc_html__( 'Primary Body Font', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Specify the body font properties.', 'medicale-wp' ),
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
			),
			array(
				'id'            => 'typography-primary-body-link-color',
				'type'          => 'color',
				'title'         => esc_html__( 'Primary Link Color', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Specify link color throughout the body.', 'medicale-wp' ),
				'transparent'   => false,
			),
		)
		
	) );

	// -> START Headings Typography
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Headings Typography', 'medicale-wp' ),
		'id'     => 'headings-typography',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-editor-textcolor',
		'subsection' => true,
		'fields' => array(
			//section H1 Starts
			array(
				'id'       => 'typography-h1-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Heading H1', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Define styles for heading H1.', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'            => 'typography-h1',
				'type'          => 'typography',
				'title'         => esc_html__( 'Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
			),
			array(
				'id'       => 'typography-h1-margin-top-bottom',
				'type'     => 'spacing',
				// An array of CSS selectors to apply this font style to
				'mode'     => 'margin',
				// absolute, padding, margin, defaults to padding
				'all'      => false,
				// Have one field that applies to all
				'top'      => true,     // Disable the top
				'right'    => false,     // Disable the right
				'bottom'   => true,     // Disable the bottom
				'left'     => false,     // Disable the left
				'units'    => 'px',      // You can specify a unit value. Possible: px, em, %
				//'units_extended'=> 'true',    // Allow users to select any type of unit
				'display_units' => true,   // Set to false to hide the units if the units are specified
				'title'    => esc_html__( 'Margin Top & Bottom', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
			),
			array(
				'id'     => 'typography-h1-section-ends',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),


			//section H2 Starts
			array(
				'id'       => 'typography-h2-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Heading H2', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Define styles for heading H2.', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'            => 'typography-h2',
				'type'          => 'typography',
				'title'         => esc_html__( 'Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
			),
			array(
				'id'       => 'typography-h2-margin-top-bottom',
				'type'     => 'spacing',
				// An array of CSS selectors to apply this font style to
				'mode'     => 'margin',
				// absolute, padding, margin, defaults to padding
				'all'      => false,
				// Have one field that applies to all
				'top'      => true,     // Disable the top
				'right'    => false,     // Disable the right
				'bottom'   => true,     // Disable the bottom
				'left'     => false,     // Disable the left
				'units'    => 'px',      // You can specify a unit value. Possible: px, em, %
				//'units_extended'=> 'true',    // Allow users to select any type of unit
				'display_units' => true,   // Set to false to hide the units if the units are specified
				'title'    => esc_html__( 'Margin Top & Bottom', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
			),
			array(
				'id'     => 'typography-h2-section-ends',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),


			//section H3 Starts
			array(
				'id'       => 'typography-h3-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Heading H3', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Define styles for heading H3.', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'            => 'typography-h3',
				'type'          => 'typography',
				'title'         => esc_html__( 'Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
			),
			array(
				'id'       => 'typography-h3-margin-top-bottom',
				'type'     => 'spacing',
				// An array of CSS selectors to apply this font style to
				'mode'     => 'margin',
				// absolute, padding, margin, defaults to padding
				'all'      => false,
				// Have one field that applies to all
				'top'      => true,     // Disable the top
				'right'    => false,     // Disable the right
				'bottom'   => true,     // Disable the bottom
				'left'     => false,     // Disable the left
				'units'    => 'px',      // You can specify a unit value. Possible: px, em, %
				//'units_extended'=> 'true',    // Allow users to select any type of unit
				'display_units' => true,   // Set to false to hide the units if the units are specified
				'title'    => esc_html__( 'Margin Top & Bottom', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
			),
			array(
				'id'     => 'typography-h3-section-ends',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),


			//section H4 Starts
			array(
				'id'       => 'typography-h4-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Heading H4', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Define styles for heading H4.', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'            => 'typography-h4',
				'type'          => 'typography',
				'title'         => esc_html__( 'Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
			),
			array(
				'id'       => 'typography-h4-margin-top-bottom',
				'type'     => 'spacing',
				// An array of CSS selectors to apply this font style to
				'mode'     => 'margin',
				// absolute, padding, margin, defaults to padding
				'all'      => false,
				// Have one field that applies to all
				'top'      => true,     // Disable the top
				'right'    => false,     // Disable the right
				'bottom'   => true,     // Disable the bottom
				'left'     => false,     // Disable the left
				'units'    => 'px',      // You can specify a unit value. Possible: px, em, %
				//'units_extended'=> 'true',    // Allow users to select any type of unit
				'display_units' => true,   // Set to false to hide the units if the units are specified
				'title'    => esc_html__( 'Margin Top & Bottom', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
			),
			array(
				'id'     => 'typography-h4-section-ends',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),


			//section H5 Starts
			array(
				'id'       => 'typography-h5-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Heading H5', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Define styles for heading H5.', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'            => 'typography-h5',
				'type'          => 'typography',
				'title'         => esc_html__( 'Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
			),
			array(
				'id'       => 'typography-h5-margin-top-bottom',
				'type'     => 'spacing',
				// An array of CSS selectors to apply this font style to
				'mode'     => 'margin',
				// absolute, padding, margin, defaults to padding
				'all'      => false,
				// Have one field that applies to all
				'top'      => true,     // Disable the top
				'right'    => false,     // Disable the right
				'bottom'   => true,     // Disable the bottom
				'left'     => false,     // Disable the left
				'units'    => 'px',      // You can specify a unit value. Possible: px, em, %
				//'units_extended'=> 'true',    // Allow users to select any type of unit
				'display_units' => true,   // Set to false to hide the units if the units are specified
				'title'    => esc_html__( 'Margin Top & Bottom', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
			),
			array(
				'id'     => 'typography-h5-section-ends',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),


			//section H6 Starts
			array(
				'id'       => 'typography-h6-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Heading H6', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Define styles for heading H6.', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'            => 'typography-h6',
				'type'          => 'typography',
				'title'         => esc_html__( 'Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
			),
			array(
				'id'       => 'typography-h6-margin-top-bottom',
				'type'     => 'spacing',
				// An array of CSS selectors to apply this font style to
				'mode'     => 'margin',
				// absolute, padding, margin, defaults to padding
				'all'      => false,
				// Have one field that applies to all
				'top'      => true,     // Disable the top
				'right'    => false,     // Disable the right
				'bottom'   => true,     // Disable the bottom
				'left'     => false,     // Disable the left
				'units'    => 'px',      // You can specify a unit value. Possible: px, em, %
				//'units_extended'=> 'true',    // Allow users to select any type of unit
				'display_units' => true,   // Set to false to hide the units if the units are specified
				'title'    => esc_html__( 'Margin Top & Bottom', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
			),
			array(
				'id'     => 'typography-h6-section-ends',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),



		)
	) );



	// -> START Theme Color Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Theme Color Settings', 'medicale-wp' ),
		'id'     => 'theme-color-settings',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-art',
		'fields' => array(
			array(
				'id'       => 'theme-color-settings-theme-color-type',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Website Primary Theme Color', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select website primary theme color', 'medicale-wp' ),
				'options' => array(
					'predefined' => esc_html__( 'Predefined Theme Colors', 'medicale-wp' ),
					'custom'     => esc_html__( 'Custom Theme Color', 'medicale-wp' )
				),
				'default' => 'predefined',
			),
			array(
				'id'       => 'theme-color-settings-primary-theme-color',
				'type'     => 'select',
				'title'    => esc_html__( 'Predefined Theme Colors', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose one from these predefined theme colors', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> medicale_mascot_metabox_get_list_of_predefined_theme_color_css_files(),
				'default'	=> 'theme-skin-blue6.css',
				'required' => array( 'theme-color-settings-theme-color-type', '=', 'predefined' ),
			),
			array(
				'id'       => 'theme-color-settings-custom-theme-color',
				'type'     => 'color',
				'title'    => esc_html__( 'Custom Theme Color', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Pick a custom color for the theme.', 'medicale-wp' ),
				'default'	=> '#5d5d5d',
				'transparent'  => false,
				'required' => array( 'theme-color-settings-theme-color-type', '=', 'custom' ),
			),
		)
	) );



	// -> START Page Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Page', 'medicale-wp' ),
		'id'     => 'page-settings',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-media-default',
		'fields' => array(
			array(
				'id'       => 'page-settings-sidebar-layout',
				'type'     => 'select',
				'title'    => esc_html__( 'Page Sidebar Layout', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose a sidebar layout for pages', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'no-sidebar'            => esc_html__( 'No Sidebar', 'medicale-wp' ),
					'both-sidebar-25-50-25' => esc_html__( 'Sidebar Both Side (1/4 + 2/4 + 1/4)', 'medicale-wp' ),
					'sidebar-right-25'      => esc_html__( 'Sidebar Right 1/4', 'medicale-wp' ),
					'sidebar-right-33'      => esc_html__( 'Sidebar Right 1/3', 'medicale-wp' ),
					'sidebar-left-25'       => esc_html__( 'Sidebar Left 1/4', 'medicale-wp' ),
					'sidebar-left-33'       => esc_html__( 'Sidebar Left 1/3', 'medicale-wp' ),

				),
				'default'	=> 'no-sidebar',
			),
			array(
				'id'       => 'page-settings-sidebar-layout-sidebar-default',
				'type'     => 'select',
				'title'    => esc_html__( 'Page Default Sidebar', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose default sidebar that will be displayed on pages.', 'medicale-wp' ),
				'desc'     => '',
				'data'     => 'sidebars',
				'default'	=> 'page-sidebar',
				'required' => array( 'page-settings-sidebar-layout', '!=', 'no-sidebar' ),
			),
			array(
				'id'       => 'page-settings-sidebar-layout-sidebar-two',
				'type'     => 'select',
				'title'    => esc_html__( 'Page Sidebar 2', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose sidebar 2 that will be displayed on pages. Sidebar 2 will only be used if "Sidebar Both Side" is selected.', 'medicale-wp' ),
				'desc'     => '',
				'data'     => 'sidebars',
				'default'	=> 'page-sidebar-two',
				'required' => array( 'page-settings-sidebar-layout', '=', 'both-sidebar-25-50-25' ),
			),
			array(
				'id'       => 'page-settings-sidebar-layout-sidebar-two-position',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Page Sidebar 2 - Position', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Controls the position of sidebar 2. In that case, default sidebar will be shown on opposite side.', 'medicale-wp' ),
				'options'	=> array(
					'left'      => esc_html__( 'Left', 'medicale-wp' ),
					'right'     => esc_html__( 'Right', 'medicale-wp' )
				),
				'default'	=> 'right',
				'required' => array( 'page-settings-sidebar-layout', '=', 'both-sidebar-25-50-25' ),
			),
			array(
				'id'       => 'page-settings-show-comments',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Page Comments', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disable comments on all pages except the post pages. It is possible to disable them individually using page meta settings.', 'medicale-wp' ),
				'default'	=> true,
			),
		)
	) );




	// -> START Page Title Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Page Title', 'medicale-wp' ),
		'id'     => 'page-title-settings',
		'desc'   => esc_html__( 'Enable/Disable Page Title Area for posts and pages.', 'medicale-wp' ),
		'icon'   => 'dashicons-before dashicons-archive',
		'fields' => array(
			array(
				'id'       => 'page-title-settings-enable-page-title',
				'type'     => 'switch',
				'title'    => esc_html__( 'Enable Page Title Area', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'page-title-settings-title-layout',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Choose Page Title Layout', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				//Must provide key => value(array:title|img) pairs for radio options
				'options'	=> array(
					'standard' => array(
						'alt' => 'standard',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f11.jpg'
					),
					'split' => array(
						'alt' => 'split',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/footer/f7.jpg'
					),
				),
				'default'	=> 'split',
				'required' => array( 'page-title-settings-enable-page-title', '=', '1' ),
			),
			array(
				'id'       => 'page-title-settings-container',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Title Area Container', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Put Page Title content into boxed or stretched fullwidth.', 'medicale-wp' ),
				'options'	=> array(
					'container' => esc_html__( 'Container', 'medicale-wp' ),
					'container-fluid' => esc_html__( 'Container Fluid', 'medicale-wp' )
				),
				'default' => 'container',
				'required' => array( 'page-title-settings-enable-page-title', '=', '1' ),
			),
			array(
				'id'       => 'page-title-settings-show-title',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Title', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disable title on Page Title Area. It is possible to disable them individually using page meta settings.', 'medicale-wp' ),
				'default'	=> true,
				'required' => array( 'page-title-settings-enable-page-title', '=', '1' ),
			),
			array(
				'id'       => 'page-title-settings-show-breadcrumbs',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Breadcrumbs', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disable breadcrumbs on Page Title. It is possible to disable them individually using page meta settings.', 'medicale-wp' ),
				'default'	=> true,
				'required' => array( 'page-title-settings-enable-page-title', '=', '1' ),
			),
			array(
				'id'       => 'page-title-settings-height',
				'type'     => 'select',
				'title'    => esc_html__( 'Title Area Height', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'select2' => array( array( 'minimumResultsForSearch' => 'Infinity' ) ),
				'options'	=> array(
					'padding-default'       => esc_html__( 'Default', 'medicale-wp' ),
					'padding-extra-small'   => esc_html__( 'Extra Small', 'medicale-wp' ),
					'padding-small'         => esc_html__( 'Small', 'medicale-wp' ),
					'padding-medium'        => esc_html__( 'Medium', 'medicale-wp' ),
					'padding-large'         => esc_html__( 'Large', 'medicale-wp' ),
					'padding-extra-large'   => esc_html__( 'Extra Large', 'medicale-wp' ),
				),
				'default'	=> 'padding-medium',
				'required' => array( 'page-title-settings-enable-page-title', '=', '1' ),
			),
			array(
				'id'       => 'page-title-settings-text-color',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Default Text Color', 'medicale-wp' ),
				'desc'     => '',
				'subtitle' => esc_html__( 'Select default text color. Inverted will turn font color to black. Inverted is suitable for white background.', 'medicale-wp' ),
				'options'	=> array(
					'text-default'   => esc_html__( 'Default (Text White)', 'medicale-wp' ),
					'text-inverted'  => esc_html__( 'Inverted (Text Black)', 'medicale-wp' )
				),
				'default' => 'text-default',
				'required' => array( 'page-title-settings-enable-page-title', '=', '1' ),
			),
			array(
				'id'       => 'page-title-settings-text-align',
				'type'     => 'select',
				'title'    => esc_html__( 'Text Alignment', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Text Alignment of Page Title', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> medicale_mascot_redux_text_alignment_list(),
				'default'	=> 'text-left flip',
				'required' => array( 'page-title-settings-enable-page-title', '=', '1' ),
			),
			array(
				'id'       => 'page-title-settings-top-border-color',
				'type'     => 'color',
				'title'    => esc_html__( 'Title Area Top Border Color', 'medicale-wp' ),
				'subtitle' => '',
				'transparent' => false,
				'required' => array( 'page-title-settings-enable-page-title', '=', '1' ),
			),
			array(
				'id'       => 'page-title-settings-bottom-border-color',
				'type'     => 'color',
				'title'    => esc_html__( 'Title Area Bottom Border Color', 'medicale-wp' ),
				'subtitle' => '',
				'transparent' => false,
				'required' => array( 'page-title-settings-enable-page-title', '=', '1' ),
			),


			//Page Title background
			array(
				'id'       => 'page-title-settings-bg-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Page Title Background', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'page-title-settings-enable-page-title', '=', '1' )
				)
			),
			array(
				'id'       => 'page-title-settings-bg',
				'type'     => 'background',
				'title'    => esc_html__( 'Page Title Background', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose background image or color.', 'medicale-wp' ),
				'default'	=> array(
					'background-repeat'     => 'no-repeat',
					'background-size'       => 'cover',
					'background-attachment' => '',
					'background-position'   => 'center center',
					'background-image'      => MASCOT_ASSETS_URI . '/images/bg/bg1.jpg',
				),
				'required' => array( 
					array( 'page-title-settings-enable-page-title', '=', '1' )
				)
			),
			array(
				'id'       => 'page-title-settings-bg-parallax-effect',
				'type'     => 'switch',
				'title'    => esc_html__( 'Page Title Background Parallax Effect', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Parallax effect only for background image of Page Title.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 
					array( 'page-title-settings-enable-page-title', '=', '1' )
				)
			),
			array(
				'id'       => 'page-title-settings-bg-layer-overlay-status',
				'type'     => 'switch',
				'title'    => esc_html__( 'Add Page Title Background Overlay', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 
					array( 'page-title-settings-enable-page-title', '=', '1' )
				)
			),
			array(
				'id'       => 'page-title-settings-bg-layer-overlay',
				'type'          => 'slider',
				'title'         => esc_html__( 'Background Overlay Opacity', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Overlay on background image on Page Title.', 'medicale-wp' ),
				'desc'          => '',
				'default'       => 8,
				'min'           => 1,
				'step'          => 1,
				'max'           => 9,
				'display_value' => 'text',
				'required' => array( 
					array( 'page-title-settings-enable-page-title', '=', '1' ),
					array( 'page-title-settings-bg-layer-overlay-status', '=', '1' )
				)
			),
			array(
				'id'       => 'page-title-settings-bg-layer-overlay-color',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Background Overlay Color', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select Dark or White Overlay on background image.', 'medicale-wp' ),
				'options'	=> array(
					'dark'          => esc_html__( 'Dark', 'medicale-wp' ),
					'white'         => esc_html__( 'White', 'medicale-wp' ),
					'theme-colored' => esc_html__( 'Primary Theme Color', 'medicale-wp' )
				),
				'default' => 'dark',
				'required' => array( 
					array( 'page-title-settings-enable-page-title', '=', '1' ),
					array( 'page-title-settings-bg-layer-overlay-status', '=', '1' )
				)
			),

			//background video
			array(
				'id'       => 'page-title-settings-bg-video-status',
				'type'     => 'switch',
				'title'    => esc_html__( 'Add Background Video', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 
					array( 'page-title-settings-enable-page-title', '=', '1' )
				)
			),
			array(
				'id'       => 'page-title-settings-bg-video-type',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Video Type', 'medicale-wp' ),
				'subtitle' => '',
				'options' => array(
					'youtube' => esc_html__( 'Youtube', 'medicale-wp' ),
					'self-hosted' => esc_html__( 'Self Hosted Video', 'medicale-wp' )
				),
				'default' => 'youtube',
				'required' => array( 
					array( 'page-title-settings-enable-page-title', '=', '1' ),
					array( 'page-title-settings-bg-video-status', '=', '1' )
				)
			),
			array(
				'id'       => 'page-title-settings-bg-video-youtube-id',
				'type'     => 'text',
				'title'    => esc_html__( 'Youtube Video ID', 'medicale-wp' ),
				'subtitle'    => esc_html__( 'Only put video ID not the whole URL.', 'medicale-wp' ),
				'desc'     => '',
				'placeholder'    => esc_html__( 'Example: E5ln4uR4TwQ', 'medicale-wp' ),
				'default' => '',
				'required' => array( 
					array( 'page-title-settings-enable-page-title', '=', '1' ),
					array( 'page-title-settings-bg-video-status', '=', '1' ),
					array( 'page-title-settings-bg-video-type', '=', 'youtube' )
				)
			),
			array(
				'id'       => 'page-title-settings-bg-video-self-hosted-video-poster',
				'type'     => 'media',
				'title'    => esc_html__( 'Video Poster', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'url'      => true,
				'readonly' => false,
				'mode'     => false, // Can be set to false to allow any media type, or can also be set to any mime type.
				'default'	=> '',
				'required' => array( 
					array( 'page-title-settings-enable-page-title', '=', '1' ),
					array( 'page-title-settings-bg-video-status', '=', '1' ),
					array( 'page-title-settings-bg-video-type', '=', 'self-hosted' )
				)
			),
			array(
				'id'       => 'page-title-settings-bg-video-self-hosted-mp4-video-url',
				'type'     => 'media',
				'title'    => esc_html__( 'MP4 Video URL', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'url'      => true,
				'readonly' => false,
				'mode'     => 'mp4', // Can be set to false to allow any media type, or can also be set to any mime type.
				'default'	=> '',
				'required' => array( 
					array( 'page-title-settings-enable-page-title', '=', '1' ),
					array( 'page-title-settings-bg-video-status', '=', '1' ),
					array( 'page-title-settings-bg-video-type', '=', 'self-hosted' )
				)
			),
			array(
				'id'       => 'page-title-settings-bg-video-self-hosted-webm-video-url',
				'type'     => 'media',
				'title'    => esc_html__( 'WEBM Video URL', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'url'      => true,
				'readonly' => false,
				'mode'     => 'webm', // Can be set to false to allow any media type, or can also be set to any mime type.
				'default'	=> '',
				'required' => array( 
					array( 'page-title-settings-enable-page-title', '=', '1' ),
					array( 'page-title-settings-bg-video-status', '=', '1' ),
					array( 'page-title-settings-bg-video-type', '=', 'self-hosted' )
				)
			),
			array(
				'id'       => 'page-title-settings-bg-video-self-hosted-ogv-video-url',
				'type'     => 'media',
				'title'    => esc_html__( 'OGV Video URL', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'url'      => true,
				'readonly' => false,
				'mode'     => 'false', // Can be set to false to allow any media type, or can also be set to any mime type.
				'default'	=> '',
				'required' => array( 
					array( 'page-title-settings-enable-page-title', '=', '1' ),
					array( 'page-title-settings-bg-video-status', '=', '1' ),
					array( 'page-title-settings-bg-video-type', '=', 'self-hosted' )
				)
			),
			array(
				'id'       => 'page-title-settings-bg-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'page-title-settings-enable-page-title', '=', '1' ),
				)
			),



			//animation
			array(
				'id'       => 'page-title-settings-title-animation-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Animation Effect', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'page-title-settings-enable-page-title', '=', '1' )
				)
			),
			array(
				'id'       => 'page-title-settings-title-animation-effect',
				'type'     => 'select',
				'title'    => esc_html__( 'Title Animation Effect', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> medicale_mascot_animate_css_animation_list(),
				'default'	=> '',
				'required' => array( 'page-title-settings-enable-page-title', '=', '1' ),
			),
			array(
				'id'       => 'page-title-settings-title-animation-duration',
				'type'     => 'text',
				'title'    => esc_html__( 'Title Animation Duration', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => 'Change the animation duration. Example: 1500ms or 1.5s or 0.5s etc. Default 0.5s.',
				'placeholder' => esc_html__( '1.5s', 'medicale-wp' ),
				'default'	=> '',
				'required' => array( 'page-title-settings-enable-page-title', '=', '1' ),
			),
			array(
				'id'       => 'page-title-settings-subtitle-animation-effect',
				'type'     => 'select',
				'title'    => esc_html__( 'Sub Title Animation Effect', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> medicale_mascot_animate_css_animation_list(),
				'default'	=> '',
				'required' => array( 'page-title-settings-enable-page-title', '=', '1' ),
			),
			array(
				'id'       => 'page-title-settings-subtitle-animation-duration',
				'type'     => 'text',
				'title'    => esc_html__( 'Sub Title Animation Duration', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => 'Change the animation duration. Example: 1500ms or 1.5s or 0.5s etc. Default 0.5s.',
				'placeholder' => esc_html__( '1.5s', 'medicale-wp' ),
				'default'	=> '',
				'required' => array( 'page-title-settings-enable-page-title', '=', '1' ),
			),
			array(
				'id'       => 'page-title-settings-title-animation-ends',
				'type'     => 'section',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( 'page-title-settings-enable-page-title', '=', '1' )
				)
			),



			//section Typography Starts
			array(
				'id'       => 'page-title-settings-title-typography-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Typography', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Define text and styles for Title.', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 'page-title-settings-enable-page-title', '=', '1' ),
			),
			array(
				'id'       => 'page-title-settings-title-tag',
				'type'     => 'select',
				'title'    => esc_html__( 'Title Tag', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose title element tag', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> medicale_mascot_heading_tag_list_all(),
				'default'	=> 'h3',
			),
			array(
				'id'            => 'page-title-settings-title-typography',
				'type'          => 'typography',
				'title'         => esc_html__( 'Title Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
			),
			array(
				'id'       => 'page-title-settings-title-margin-top-bottom',
				'type'     => 'spacing',
				// An array of CSS selectors to apply this font style to
				'mode'     => 'margin',
				// absolute, padding, margin, defaults to padding
				'all'      => false,
				// Have one field that applies to all
				'top'      => true,     // Disable the top
				'right'    => false,     // Disable the right
				'bottom'   => true,     // Disable the bottom
				'left'     => false,     // Disable the left
				'units'    => 'px',      // You can specify a unit value. Possible: px, em, %
				//'units_extended'=> 'true',    // Allow users to select any type of unit
				'display_units' => true,   // Set to false to hide the units if the units are specified
				'title'    => esc_html__( 'Title Margin Top & Bottom', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
			),
			array(
				'id'       => 'page-title-settings-subtitle-tag',
				'type'     => 'select',
				'title'    => esc_html__( 'Subtitle Tag', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose subtitle element tag', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> medicale_mascot_heading_tag_list_all(),
				'default'	=> 'h6',
			),
			array(
				'id'            => 'page-title-settings-subtitle-typography',
				'type'          => 'typography',
				'title'         => esc_html__( 'Sub Title Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
			),
			array(
				'id'       => 'page-title-settings-subtitle-margin-top-bottom',
				'type'     => 'spacing',
				// An array of CSS selectors to apply this font style to
				'mode'     => 'margin',
				// absolute, padding, margin, defaults to padding
				'all'      => false,
				// Have one field that applies to all
				'top'      => true,     // Disable the top
				'right'    => false,     // Disable the right
				'bottom'   => true,     // Disable the bottom
				'left'     => false,     // Disable the left
				'units'    => 'px',      // You can specify a unit value. Possible: px, em, %
				//'units_extended'=> 'true',    // Allow users to select any type of unit
				'display_units' => true,   // Set to false to hide the units if the units are specified
				'title'    => esc_html__( 'Sub Title Margin Top & Bottom', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
			),
			array(
				'id'            => 'page-title-settings-breadcrumbs-typography',
				'type'          => 'typography',
				'title'         => esc_html__( 'Breadcrumbs Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
			),
			array(
				'id'       => 'page-title-settings-breadcrumbs-last-child-text-color',
				'type'     => 'color',
				'title'    => esc_html__( 'Breadcrumbs Last Child Text Color', 'medicale-wp' ),
				'subtitle' => '',
				'transparent' => false,
			),
			array(
				'id'       => 'page-title-settings-breadcrumbs-seperator-color',
				'type'     => 'color',
				'title'    => esc_html__( 'Breadcrumbs Seperator Color', 'medicale-wp' ),
				'subtitle' => '',
				'transparent' => false,
			),
			array(
				'id'       => 'page-title-settings-breadcrumbs-link-hover-color',
				'type'     => 'color',
				'title'    => esc_html__( 'Breadcrumbs Link Hover/Active Color', 'medicale-wp' ),
				'subtitle' => '',
				'transparent' => false,
			),
			array(
				'id'       => 'page-title-settings-title-typography-ends',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),
		)
	) );



	// -> START Blog Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Blog', 'medicale-wp' ),
		'id'     => 'blog-settings-parent',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-media-document',
	) );



	// -> START Blog Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Blog Archive Page', 'medicale-wp' ),
		'id'     => 'blog-settings',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'blog-settings-archive-page-layout',
				'type'     => 'select',
				'title'    => esc_html__( 'Default Blog Post Layout for Archive Pages', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose a default layout for archive pages', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'standard-1-col' => 'Standard 1 Column Default',
					'standard-1-col-classic' => 'Standard 1 Column Classic',
					'standard-2-col' => 'Standard 2 Columns',
					'standard-3-col' => 'Standard 3 Columns',
					'standard-4-col' => 'Standard 4 Columns',
					'masonry-2-col'  => 'Masonry 2 Columns',
					'masonry-3-col'  => 'Masonry 3 Columns',
					'masonry-4-col'  => 'Masonry 4 Columns',
				),
				'default'	=> 'standard-1-col',
			),
			array(
				'id'       => 'blog-settings-sidebar-layout',
				'type'     => 'select',
				'title'    => esc_html__( 'Sidebar Layout', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose a sidebar layout for pages', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'no-sidebar'            => esc_html__( 'No Sidebar', 'medicale-wp' ),
					'both-sidebar-25-50-25' => esc_html__( 'Sidebar Both Side (1/4 + 2/4 + 1/4)', 'medicale-wp' ),
					'sidebar-right-25'      => esc_html__( 'Sidebar Right 1/4', 'medicale-wp' ),
					'sidebar-right-33'      => esc_html__( 'Sidebar Right 1/3', 'medicale-wp' ),
					'sidebar-left-25'       => esc_html__( 'Sidebar Left 1/4', 'medicale-wp' ),
					'sidebar-left-33'       => esc_html__( 'Sidebar Left 1/3', 'medicale-wp' ),
				),
				'default'	=> 'sidebar-right-25',
			),


			array(
				'id'       => 'blog-settings-sidebar-layout-sidebar-default',
				'type'     => 'select',
				'title'    => esc_html__( 'Blog Default Sidebar', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose default sidebar that will be displayed on blog archive pages.', 'medicale-wp' ),
				'desc'     => '',
				'data'     => 'sidebars',
				'default'	=> 'default-sidebar',
				'required' => array( 'blog-settings-sidebar-layout', '!=', 'no-sidebar' ),
			),
			array(
				'id'       => 'blog-settings-sidebar-layout-sidebar-two',
				'type'     => 'select',
				'title'    => esc_html__( 'Blog Sidebar 2', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose sidebar 2 that will be displayed on blog archive pages. Sidebar 2 will only be used if "Sidebar Both Side" is selected.', 'medicale-wp' ),
				'desc'     => '',
				'data'     => 'sidebars',
				'default'	=> 'blog-secondary-sidebar',
				'required' => array( 'blog-settings-sidebar-layout', '=', 'both-sidebar-25-50-25' ),
			),
			array(
				'id'       => 'blog-settings-sidebar-layout-sidebar-two-position',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Blog Sidebar 2 - Position', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Controls the position of sidebar 2. In that case, default sidebar will be shown on opposite side.', 'medicale-wp' ),
				'options'	=> array(
					'left'      => esc_html__( 'Left', 'medicale-wp' ),
					'right'     => esc_html__( 'Right', 'medicale-wp' )
				),
				'default'	=> 'right',
				'required' => array( 'blog-settings-sidebar-layout', '=', 'both-sidebar-25-50-25' ),
			),


			array(
				'id'       => 'blog-settings-fullwidth',
				'type'     => 'switch',
				'title'    => esc_html__( 'Fullwidth?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Make the page fullwidth or not.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'            => 'blog-settings-excerpt-length',
				'type'          => 'slider',
				'title'         => esc_html__( 'Excerpt Length', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Number of words to display in excerpt.', 'medicale-wp' ),
				'desc'          => '',
				'default'       => 22,
				'min'           => 0,
				'step'          => 1,
				'max'           => 500,
				'display_value' => 'text',
			),
			array(
				'id'       => 'blog-settings-show-post-featured-image',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Featured Image', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide Featured Image in blog page.', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'blog-settings-post-meta',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Post Meta', 'medicale-wp' ),
				'subtitle'     => esc_html__( 'Enable/Disabling this option will show/hide each Post Meta on your page.', 'medicale-wp' ),
				'desc' => '',
				//Must provide key => value pairs for multi checkbox options
				'options'	=> array(
					'show-post-by-author'       => esc_html__( 'Show By Author', 'medicale-wp' ),
					'show-post-date'            => esc_html__( 'Show Date', 'medicale-wp' ),
					'show-post-category'        => esc_html__( 'Show Category', 'medicale-wp' ),
					'show-post-comments-count'  => esc_html__( 'Show Comments Count', 'medicale-wp' ),
					'show-post-tag'             => esc_html__( 'Show Tag', 'medicale-wp' ),
					'show-post-like-button'     => esc_html__( 'Show Like Button', 'medicale-wp' ),
				),
				//See how std has changed? you also don't need to specify opts that are 0.
				'default'	=> array(
					'show-post-by-author' => '1',
					'show-post-date' => '1',
					'show-post-category' => '1',
					'show-post-comments-count' => '0',
					'show-post-tag' => '0',
					'show-post-like-button' => '0'
				)
			),
			array(
				'id'       => 'blog-settings-show-pagination',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Pagination', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enabling this option will show comments on your page.', 'medicale-wp' ),
				'default'	=> true,
			),
		)
	) );



	// -> START Single Post Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Single Post', 'medicale-wp' ),
		'id'     => 'blog-single-post-settings',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'blog-single-post-settings-fullwidth',
				'type'     => 'switch',
				'title'    => esc_html__( 'Fullwidth?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Make the page fullwidth or not.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'blog-single-post-settings-sidebar-layout',
				'type'     => 'select',
				'title'    => esc_html__( 'Sidebar Layout', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose a sidebar layout for pages', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'no-sidebar'            => esc_html__( 'No Sidebar', 'medicale-wp' ),
					'both-sidebar-25-50-25' => esc_html__( 'Sidebar Both Side (1/4 + 2/4 + 1/4)', 'medicale-wp' ),
					'sidebar-right-25'      => esc_html__( 'Sidebar Right 1/4', 'medicale-wp' ),
					'sidebar-right-33'      => esc_html__( 'Sidebar Right 1/3', 'medicale-wp' ),
					'sidebar-left-25'       => esc_html__( 'Sidebar Left 1/4', 'medicale-wp' ),
					'sidebar-left-33'       => esc_html__( 'Sidebar Left 1/3', 'medicale-wp' ),
				),
				'default'	=> 'sidebar-right-25',
			),



			array(
				'id'       => 'blog-single-post-settings-sidebar-layout-sidebar-default',
				'type'     => 'select',
				'title'    => esc_html__( 'Default Sidebar', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose default sidebar that will be displayed on blog single pages.', 'medicale-wp' ),
				'desc'     => '',
				'data'     => 'sidebars',
				'default'	=> 'default-sidebar',
				'required' => array( 'blog-single-post-settings-sidebar-layout', '!=', 'no-sidebar' ),
			),
			array(
				'id'       => 'blog-single-post-settings-sidebar-layout-sidebar-two',
				'type'     => 'select',
				'title'    => esc_html__( 'Sidebar 2', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose sidebar 2 that will be displayed on blog single pages. Sidebar 2 will only be used if "Sidebar Both Side" is selected.', 'medicale-wp' ),
				'desc'     => '',
				'data'     => 'sidebars',
				'default'	=> 'blog-secondary-sidebar',
				'required' => array( 'blog-single-post-settings-sidebar-layout', '=', 'both-sidebar-25-50-25' ),
			),
			array(
				'id'       => 'blog-single-post-settings-sidebar-layout-sidebar-two-position',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Sidebar 2 - Position', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Controls the position of sidebar 2. In that case, default sidebar will be shown on opposite side.', 'medicale-wp' ),
				'options'	=> array(
					'left'      => esc_html__( 'Left', 'medicale-wp' ),
					'right'     => esc_html__( 'Right', 'medicale-wp' )
				),
				'default'	=> 'right',
				'required' => array( 'blog-single-post-settings-sidebar-layout', '=', 'both-sidebar-25-50-25' ),
			),


			
			array(
				'id'       => 'blog-single-post-settings-show-post-featured-image',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Featured Image', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide Featured Image in blog page.', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'            => 'blog-single-post-settings-featured-image-height',
				'type'          => 'slider',
				'title'         => esc_html__( 'Featured Image Height(px)', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Set height for featured image displayed on your blog single page.', 'medicale-wp' ),
				'desc'          => '',
				'default'       => 600,
				'min'           => 0,
				'step'          => 1,
				'max'           => 1200,
				'display_value' => 'text',
			),
			array(
				'id'       => 'blog-single-post-settings-enable-drop-caps',
				'type'     => 'switch',
				'title'    => esc_html__( 'Enable Drop Caps', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling Drop Caps for the first letter of post content.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'blog-single-post-settings-post-meta',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Post Meta', 'medicale-wp' ),
				'subtitle'     => esc_html__( 'Enable/Disabling this option will show/hide each Post Meta on your page.', 'medicale-wp' ),
				'desc' => '',
				//Must provide key => value pairs for multi checkbox options
				'options'	=> array(
					'show-post-by-author'       => esc_html__( 'Show By Author', 'medicale-wp' ),
					'show-post-date'            => esc_html__( 'Show Date', 'medicale-wp' ),
					'show-post-category'        => esc_html__( 'Show Category', 'medicale-wp' ),
					'show-post-comments-count'  => esc_html__( 'Show Comments Count', 'medicale-wp' ),
					'show-post-like-button'     => esc_html__( 'Show Like Button', 'medicale-wp' ),
				),
				//See how std has changed? you also don't need to specify opts that are 0.
				'default'	=> array(
					'show-post-by-author' => '1',
					'show-post-date' => '1',
					'show-post-category' => '1',
					'show-post-comments-count' => '1',
					'show-post-like-button' => '0'
				)
			),
			array(
				'id'       => 'blog-single-post-settings-show-tags',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Tags', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide tags on your page.', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'blog-single-post-settings-show-share',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Share', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide share options on your page.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),


			//section Next/Previous Navigation Link Starts
			array(
				'id'       => 'blog-single-post-settings-show-next-pre-post-link-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Next/Previous Navigation Link', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'       => 'blog-single-post-settings-show-next-pre-post-link',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Next/Previous Single Post Navigation Link', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide link for Next & Previous Posts.', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'blog-single-post-settings-show-next-pre-post-link-within-same-cat',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Navigation Link Within Same Category', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide link to the next/previous post within the same category as the current post.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 'blog-single-post-settings-show-next-pre-post-link', '=', '1' ),
			),
			array(
				'id'       => 'blog-single-post-settings-show-next-pre-post-link-section-ends',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),


			//section Author Info Box
			array(
				'id'       => 'blog-single-post-settings-author-info-box-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Author Info Box', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'       => 'blog-single-post-settings-author-info-box',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Author Info Box', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide Author Info Box on your page.', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'blog-single-post-settings-author-info-box-show-social-icons',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Social Icons', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 'blog-single-post-settings-author-info-box', '=', '1' ),
			),
			array(
				'id'       => 'blog-single-post-settings-author-info-box-show-author-email',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Author Email', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 'blog-single-post-settings-author-info-box', '=', '1' ),
			),
			array(
				'id'       => 'blog-single-post-settings-author-info-box-section-ends',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),




			//section Related Posts Starts
			array(
				'id'       => 'blog-single-post-settings-related-posts-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Related Posts', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide Related Posts List/Carousel on your page.', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'       => 'blog-single-post-settings-show-related-posts',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Related Posts', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'blog-single-post-settings-show-related-posts-carousel',
				'type'     => 'switch',
				'title'    => esc_html__( 'Carousel?', 'medicale-wp' ),
				'subtitle' => 'Make it carousel or grid',
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 'blog-single-post-settings-show-related-posts', '=', '1' ),
			),
			array(
				'id'       => 'blog-single-post-settings-show-related-posts-count',
				'type'     => 'text',
				'title'    => esc_html__( 'Number of Posts', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => esc_html__( 'Enter number of posts to display. Default 3', 'medicale-wp' ),
				'default'	=> '3',
				'required' => array( 'blog-single-post-settings-show-related-posts', '=', '1' ),
			),
			array(
				'id'            => 'blog-single-post-settings-show-related-posts-excerpt-length',
				'type'          => 'slider',
				'title'         => esc_html__( 'Excerpt Length', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Number of words to display in excerpt.', 'medicale-wp' ),
				'desc'          => '',
				'default'       => 20,
				'min'           => 0,
				'step'          => 1,
				'max'           => 200,
				'display_value' => 'text',
				'required' => array( 'blog-single-post-settings-show-related-posts', '=', '1' ),
			),
			array(
				'id'       => 'blog-single-post-settings-related-posts-section-ends',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),



			//section Show Comments Starts
			array(
				'id'       => 'blog-single-post-settings-comments-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Comments', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide Comments on your page.', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'       => 'blog-single-post-settings-show-comments',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Comments', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'blog-single-post-settings-comments-section-ends',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),

		)
	) );



	// -> START Single Post Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Other Settings', 'medicale-wp' ),
		'id'     => 'blog-other-settings',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'blog-other-settings-show-blog-title-description',
				'type'     => 'switch',
				'title'    => esc_html__( 'Custom Blog Title & Description', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Add title and description in title section of blog page', 'medicale-wp' ),
				'default'  => 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'blog-other-settings-blog-title-text',
				'type'     => 'text',
				'title'    => esc_html__( 'Blog Title Text', 'medicale-wp' ),
				'desc'     => '',
				'default'  => esc_html__( 'Blog', 'medicale-wp' ),
				'required' => array( 'blog-other-settings-show-blog-title-description', '=', '1' ),
			),
			array(
				'id'       => 'blog-other-settings-blog-description-text',
				'type'     => 'textarea',
				'title'    => esc_html__( 'Blog Description Text', 'medicale-wp' ),
				'desc'     => '',
				'default'  => '',
				'required' => array( 'blog-other-settings-show-blog-title-description', '=', '1' ),
			)
		)
	) );



	// -> START Portfolio Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Portfolio', 'medicale-wp' ),
		'id'     => 'portfolio-settings-parent',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-format-gallery',
	) );



	// -> START Portfolio Layout Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Portfolio Archive Layout', 'medicale-wp' ),
		'id'     => 'portfolio-layout-settings',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'portfolio-settings-sidebar-layout',
				'type'     => 'select',
				'title'    => esc_html__( 'Portfolio Sidebar Layout', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose a sidebar layout for portfolio pages', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'no-sidebar'            => esc_html__( 'No Sidebar', 'medicale-wp' ),
					'both-sidebar-25-50-25' => esc_html__( 'Sidebar Both Side (1/4 + 2/4 + 1/4)', 'medicale-wp' ),
					'sidebar-right-25'      => esc_html__( 'Sidebar Right 1/4', 'medicale-wp' ),
					'sidebar-right-33'      => esc_html__( 'Sidebar Right 1/3', 'medicale-wp' ),
					'sidebar-left-25'       => esc_html__( 'Sidebar Left 1/4', 'medicale-wp' ),
					'sidebar-left-33'       => esc_html__( 'Sidebar Left 1/3', 'medicale-wp' ),

				),
				'default'	=> 'no-sidebar',
			),
			array(
				'id'       => 'portfolio-settings-sidebar-layout-sidebar-default',
				'type'     => 'select',
				'title'    => esc_html__( 'Portfolio Default Sidebar', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose default sidebar that will be displayed on portfolio pages.', 'medicale-wp' ),
				'desc'     => '',
				'data'     => 'sidebars',
				'default'	=> 'page-sidebar',
				'required' => array( 'portfolio-settings-sidebar-layout', '!=', 'no-sidebar' ),
			),
			array(
				'id'       => 'portfolio-settings-sidebar-layout-sidebar-two',
				'type'     => 'select',
				'title'    => esc_html__( 'Portfolio Sidebar 2', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose sidebar 2 that will be displayed on portfolio pages. Sidebar 2 will only be used if "Sidebar Both Side" is selected.', 'medicale-wp' ),
				'desc'     => '',
				'data'     => 'sidebars',
				'default'	=> 'page-sidebar-two',
				'required' => array( 'portfolio-settings-sidebar-layout', '=', 'both-sidebar-25-50-25' ),
			),
			array(
				'id'       => 'portfolio-settings-sidebar-layout-sidebar-two-position',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Portfolio Sidebar 2 - Position', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Controls the position of sidebar 2. In that case, default sidebar will be shown on opposite side.', 'medicale-wp' ),
				'options'	=> array(
					'left'      => esc_html__( 'Left', 'medicale-wp' ),
					'right'     => esc_html__( 'Right', 'medicale-wp' )
				),
				'default'	=> 'right',
				'required' => array( 'portfolio-settings-sidebar-layout', '=', 'both-sidebar-25-50-25' ),
			),




			array(
				'id'       => 'portfolio-layout-settings-select-portfolio-type',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Portfolio Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select the type of portfolio you would like to display.', 'medicale-wp' ),
				'desc'     => '',
				//Must provide key => value(array:title|img) pairs for radio options
				'options'	=> array(
					'default' => array(
						'alt' => 'Default',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio/type/default.png'
					),
					'london' => array(
						'alt' => 'London',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio/type/london.png'
					),
					'rome' => array(
						'alt' => 'Rome',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio/type/rome.png'
					),
					'paris' => array(
						'alt' => 'Paris',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio/type/paris.png'
					),
					'barlin' => array(
						'alt' => 'Barlin',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio/type/barlin.png'
					),
					'simple' => array(
						'alt' => 'Simple',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio/type/simple.png'
					),
					'standard' => array(
						'alt' => 'Standard',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio/type/standard.png'
					),
				),
				'default'	=> 'standard'
			),

			array(
				'id'       => 'portfolio-layout-settings-select-portfolio-layout-mode',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Portfolio Layout Mode (FitRows Or Masonry)', 'medicale-wp' ),
				'subtitle' => esc_html__( 'You can position items with different layout modes. Select a layout mode you would like to use.', 'medicale-wp' ),
				'desc'     => '',
				//Must provide key => value(array:title|img) pairs for radio options
				'options'	=> array(
					'fitrows' => array(
						'alt' => 'Fit Rows',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio/layout-mode/fitrows.png'
					),
					'masonry' => array(
						'alt' => 'Masonry',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio/layout-mode/masonry.png'
					),
				),
				'default'	=> 'masonry'
			),


			array(
				'id'       => 'portfolio-layout-settings-items-per-row',
				'type'     => 'select',
				'title'    => esc_html__( 'Number of Portfolio Items Per Row', 'medicale-wp' ),
				'subtitle'    => esc_html__( 'Select your default column structure for your portfolio items.', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'1'  => '1 Item Per Row',
					'2'  => '2 Items Per Row',
					'3'  => '3 Items Per Row',
					'4'  => '4 Items Per Row',
					'5'  => '5 Items Per Row',
					'6'  => '6 Items Per Row',
					'7'  => '7 Items Per Row',
					'8'  => '8 Items Per Row',
					'9'  => '9 Items Per Row',
					'10' => '10 Items Per Row',
				),
				'default'	=> '3',
			),
			array(
				'id'            => 'portfolio-layout-settings-items-per-page',
				'type'          => 'slider',
				'title'         => esc_html__( 'Number of Portfolio Items Per Page', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Controls the number of items to display on portfolio archive pages. Set to -1 to display all. Set to 0 to use the number of posts from Settings > Reading.', 'medicale-wp' ),
				'desc'          => '',
				'default'       => 10,
				'min'           => -1,
				'step'          => 1,
				'max'           => 40,
				'display_value' => 'text',
			),
			array(
				'id'            => 'portfolio-layout-settings-gutter-size',
				'type'          => 'slider',
				'title'         => esc_html__( 'Portfolio Column Spacing (Gutter Size) px', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Controls column spacing or gutter size between items on portfolio archive pages.', 'medicale-wp' ),
				'desc'          => '',
				'default'       => 10,
				'min'           => 0,
				'step'          => 1,
				'max'           => 250,
				'display_value' => 'text',
			),
			array(
				'id'       => 'portfolio-layout-settings-thumbnail-orientation',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Thumbnail Orientation', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose thumbnail orientation on your Portfolio Archive Page.', 'medicale-wp' ),
				'options'	=> array(
					'landscape' => esc_html__( 'Landscape', 'medicale-wp' ),
					'portrait'  => esc_html__( 'Portrait', 'medicale-wp' )
				),
				'default' => 'landscape',
			),
			array(
				'id'       => 'portfolio-layout-settings-thumbnail-ratio',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Thumbnail Ratio', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose thumbnail ratio on your Portfolio Archive Page.', 'medicale-wp' ),
				'options'	=> array(
					'16:9'  => esc_html__( '16:9', 'medicale-wp' ),
					'4:3'   => esc_html__( '4:3', 'medicale-wp' ),
					'3:2'   => esc_html__( '3:2', 'medicale-wp' ),
					'1:1'   => esc_html__( '1:1', 'medicale-wp' )
				),
				'default' => '16:9',
			),
		)
	) );



	// -> START Portfolio Layout Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Portfolio Page Layout', 'medicale-wp' ),
		'id'     => 'portfolio-page-settings',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'portfolio-layout-settings-fullwidth',
				'type'     => 'switch',
				'title'    => esc_html__( 'Page Fullwidth?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Make the portfolio page fullwidth or not.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'portfolio-layout-settings-portfolio-items-add-animation-effect',
				'type'     => 'switch',
				'title'    => esc_html__( 'Add Reveal Animation Effect?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Want to add a reveal animation effect on portfolio items at first sight?', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'portfolio-layout-settings-portfolio-items-animation-effect',
				'type'     => 'select',
				'title'    => esc_html__( 'Choose Reveal Animation Effect', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose a reveal animation effect from the list for portfolio items.', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> medicale_mascot_animate_css_animation_list(),
				'default'	=> 'fadeInUp',
				'required' => array( 'portfolio-layout-settings-portfolio-items-add-animation-effect', '=', '1' ),
			),
			array(
				'id'       => 'portfolio-layout-settings-portfolio-filter-sorter-position',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Portfolio Filter & Sorter Position', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Put Filter in left or center of the container.', 'medicale-wp' ),
				'options'	=> medicale_mascot_redux_text_alignment_list(),
				'default' => 'text-left flip',
				'required' => array( 'portfolio-layout-settings-portfolio-filter', '=', '1' ),
			),





			
			array(
				'id'       => 'portfolio-layout-settings-portfolio-filter-settings',
				'type'      => 'info',
				'title'     => esc_html__( 'Portfolio Filter', 'medicale-wp' ),
				'notice'    => false,
			),
			array(
				'id'       => 'portfolio-layout-settings-portfolio-filter',
				'type'     => 'switch',
				'title'    => esc_html__( 'Filter on Portfolio Items', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'portfolio-layout-settings-portfolio-filter-style',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Filter Style', 'medicale-wp' ),
				'subtitle' => esc_html__( 'You can choose one from different predefined styles.', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'1'  => 'Plain Text',
					'2'  => 'Flat Box',
					'3'  => 'Button Set',
				),
				'default'	=> '1',
				'required' => array( 'portfolio-layout-settings-portfolio-filter', '=', '1' ),
			),




			array(
				'id'       => 'portfolio-layout-settings-portfolio-sorter-settings',
				'type'      => 'info',
				'title'     => esc_html__( 'Portfolio Sorter', 'medicale-wp' ),
				'notice'    => false,
			),
			array(
				'id'       => 'portfolio-layout-settings-portfolio-sorter',
				'type'     => 'switch',
				'title'    => esc_html__( 'Sorter on Portfolio Items', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'portfolio-layout-settings-portfolio-sorter-style',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Sorter Style', 'medicale-wp' ),
				'subtitle' => esc_html__( 'You can choose one from different predefined styles.', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'1'  => 'Button Set',
					'2'  => 'List',
				),
				'default'	=> '1',
				'required' => array( 'portfolio-layout-settings-portfolio-sorter', '=', '1' ),
			),
			array(
				'id'       => 'portfolio-layout-settings-portfolio-sorter-elements',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Sorter Elements', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide each sorter elements.', 'medicale-wp' ),
				'desc'     => '',
				//Must provide key => value pairs for multi checkbox options
				'options'	=> array(
					'sorter-date'               => esc_html__( 'Date', 'medicale-wp' ),
					'sorter-name'               => esc_html__( 'Name', 'medicale-wp' ),
					'sorter-random'             => esc_html__( 'Random', 'medicale-wp' ),
					'sorter-original-order'     => esc_html__( 'Original Order', 'medicale-wp' ),
					//'sorter-shuffle'            => esc_html__( 'Shuffle', 'medicale-wp' ),
				),
				//See how std has changed? you also don't need to specify opts that are 0.
				'default'	=> array(
					'sorter-date' => '1',
					'sorter-name' => '1',
					'sorter-random' => '1',
					'sorter-original-order' => '1',
					//'sorter-shuffle' => '1',
				),
				'required' => array( 'portfolio-layout-settings-portfolio-sorter', '=', '1' ),
			),






			array(
				'id'       => 'portfolio-layout-settings-lightbox-settings',
				'type'      => 'info',
				'title'     => esc_html__( 'Lightbox Settings', 'medicale-wp' ),
				'notice'    => false,
			),
			array(
				'id'       => 'portfolio-layout-settings-lightbox-type',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Lightbox Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'If set to "Linked to Featured Image" mode, lightbox previous and next arrows will continue through all portfolio items Featured Images shown in the list. Otherwise it will show PrettyPhoto Gallery for each portfolio item.', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'link-featured'    => 'Linked to Featured Image',
					'link-featured-prettyphoto'    => 'Linked to Featured Image + PrettyPhoto',
					'link-gallery'     => 'Linked to Portfolio Gallery + PrettyPhoto',
				),
				'default'	=> 'link-featured-prettyphoto',
			),





			
			array(
				'id'       => 'portfolio-layout-settings-display-content-settings',
				'type'      => 'info',
				'title'     => esc_html__( 'Display Content Settings', 'medicale-wp' ),
				'notice'    => false,
			),
			array(
				'id'       => 'portfolio-layout-settings-display-content-type',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Display Content Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Controls the content type to display between excerpt and full content for portfolio elements and portfolio archive pages.', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'full-excerpt' => 'Full Excerpt',
					'custom-excerpt'      => 'Excerpt Custom Length',
					'full-content' => 'Full Content',
				),
				'default'	=> 'full-excerpt',
			),
			array(
				'id'            => 'portfolio-layout-settings-excerpt-length',
				'type'          => 'slider',
				'title'         => esc_html__( 'Excerpt Length', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Number of words to display in excerpt for each portfolio item.', 'medicale-wp' ),
				'desc'          => '',
				'default'       => 20,
				'min'           => 0,
				'step'          => 1,
				'max'           => 300,
				'display_value' => 'text',
				'required' => array( 'portfolio-layout-settings-display-content-type', '=', 'custom-excerpt' ),
			),
			array(
				'id'       => 'portfolio-layout-settings-portfolio-showhide-options',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Portfolio Show/Hide Options', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide each portfolio option on your page.', 'medicale-wp' ),
				'desc'     => '',
				//Must provide key => value pairs for multi checkbox options
				'options'	=> array(
					'show-portfolio-title'          => esc_html__( 'Show Title', 'medicale-wp' ),
					'show-portfolio-content'        => esc_html__( 'Show Content/Excerpt', 'medicale-wp' ),
					'show-portfolio-category'       => esc_html__( 'Show Category', 'medicale-wp' ),
					'show-portfolio-tag'            => esc_html__( 'Show Tag', 'medicale-wp' ),
					'show-portfolio-date'           => esc_html__( 'Show Date', 'medicale-wp' ),
					'show-portfolio-comments-count' => esc_html__( 'Show Comments Count', 'medicale-wp' ),
					'show-portfolio-like-button'    => esc_html__( 'Show Like Button', 'medicale-wp' ),
				),
				//See how std has changed? you also don't need to specify opts that are 0.
				'default'	=> array(
					'show-portfolio-title' => '1',
					'show-portfolio-content' => '0',
					'show-portfolio-category' => '1',
					'show-portfolio-tag' => '0',
					'show-portfolio-date' => '0',
					'show-portfolio-comments-count' => '0',
					'show-portfolio-like-button' => '1',
				)
			),





			
			array(
				'id'       => 'portfolio-layout-settings-portfolio-pagination-settings',
				'type'      => 'info',
				'title'     => esc_html__( 'Pagination Settings', 'medicale-wp' ),
				'notice'    => false,
			),
			array(
				'id'       => 'portfolio-layout-settings-pagination-type',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Pagination Type', 'medicale-wp' ),/*~*/
				'subtitle' => esc_html__( 'Controls the pagination type for portfolio elements and portfolio archive pages.', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'normal-pagination' => 'Normal Pagination',
					'infinite-scroll' => 'Infinite Scroll',
					'load-more-button' => 'Load More Button',
				),
				'default'	=> 'normal-pagination',
			),
			array(
				'id'       => 'portfolio-layout-settings-text-align',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Pagination Text Alignment', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> medicale_mascot_redux_text_alignment_list(),
				'default'	=> 'text-left flip',
			),



		)
	) );



	// -> START Portfolio Single Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Portfolio Single', 'medicale-wp' ),
		'id'     => 'portfolio-single-page-settings',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'portfolio-single-page-settings-fullwidth',
				'type'     => 'switch',
				'title'    => esc_html__( 'Fullwidth?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Make the page fullwidth or not.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'portfolio-single-page-settings-sidebar-layout',
				'type'     => 'select',
				'title'    => esc_html__( 'Sidebar Layout', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose a sidebar layout for portfolio details pages', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'no-sidebar'            => esc_html__( 'No Sidebar', 'medicale-wp' ),
					'both-sidebar-25-50-25' => esc_html__( 'Sidebar Both Side (1/4 + 2/4 + 1/4)', 'medicale-wp' ),
					'sidebar-right-25'      => esc_html__( 'Sidebar Right 1/4', 'medicale-wp' ),
					'sidebar-right-33'      => esc_html__( 'Sidebar Right 1/3', 'medicale-wp' ),
					'sidebar-left-25'       => esc_html__( 'Sidebar Left 1/4', 'medicale-wp' ),
					'sidebar-left-33'       => esc_html__( 'Sidebar Left 1/3', 'medicale-wp' ),
				),
				'default'	=> 'no-sidebar',
			),



			array(
				'id'       => 'portfolio-single-page-settings-sidebar-layout-sidebar-default',
				'type'     => 'select',
				'title'    => esc_html__( 'Default Sidebar', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose default sidebar that will be displayed on portfolio single pages.', 'medicale-wp' ),
				'desc'     => '',
				'data'     => 'sidebars',
				'default'	=> 'page-sidebar',
				'required' => array( 'portfolio-single-page-settings-sidebar-layout', '!=', 'no-sidebar' ),
			),
			array(
				'id'       => 'portfolio-single-page-settings-sidebar-layout-sidebar-two',
				'type'     => 'select',
				'title'    => esc_html__( 'Sidebar 2', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose sidebar 2 that will be displayed on portfolio single pages. Sidebar 2 will only be used if "Sidebar Both Side" is selected.', 'medicale-wp' ),
				'desc'     => '',
				'data'     => 'sidebars',
				'default'	=> 'page-sidebar-two',
				'required' => array( 'portfolio-single-page-settings-sidebar-layout', '=', 'both-sidebar-25-50-25' ),
			),
			array(
				'id'       => 'portfolio-single-page-settings-sidebar-layout-sidebar-two-position',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Sidebar 2 - Position', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Controls the position of sidebar 2. In that case, default sidebar will be shown on opposite side.', 'medicale-wp' ),
				'options'	=> array(
					'left'      => esc_html__( 'Left', 'medicale-wp' ),
					'right'     => esc_html__( 'Right', 'medicale-wp' )
				),
				'default'	=> 'right',
				'required' => array( 'portfolio-single-page-settings-sidebar-layout', '=', 'both-sidebar-25-50-25' ),
			),


			array(
				'id'       => 'portfolio-single-page-settings-select-portfolio-details-type',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Portfolio Details Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select the type of portfolio details you would like to display.', 'medicale-wp' ),
				'desc'     => '',
				//Must provide key => value(array:title|img) pairs for radio options
				'options'	=> array(
					'small-image' => array(
						'alt' => esc_html__( 'Small Image', 'medicale-wp' ),
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio-single/type/small-image.png'
					),
					'small-image-slider' => array(
						'alt' => esc_html__( 'Small Image Slider', 'medicale-wp' ),
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio-single/type/small-image-slider.png'
					),
					'big-image' => array(
						'alt' => esc_html__( 'Big Image', 'medicale-wp' ),
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio-single/type/big-image.png'
					),
					'big-image-slider' => array(
						'alt' => esc_html__( 'Big Image Slider', 'medicale-wp' ),
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio-single/type/big-image-slider.png'
					),
					'small-image-gallery' => array(
						'alt' => esc_html__( 'Small Image Gallery', 'medicale-wp' ),
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio-single/type/small-image-gallery.png'
					),
					'big-image-gallery' => array(
						'alt' => esc_html__( 'Big Image Gallery', 'medicale-wp' ),
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio-single/type/big-image-gallery.png'
					),
				),
				'default'	=> 'small-image'
			),


			//Small Image
			array(
				'id'        => 'portfolio-single-page-settings-portfolio-type-small-image-settings',
				'type'      => 'info',
				'title'     => esc_html__( 'Small Image Settings', 'medicale-wp' ),
				'notice'    => false,
				'required' => array( 'portfolio-single-page-settings-select-portfolio-details-type', '=', 'small-image' ),
			),
			array(
				'id'        => 'portfolio-single-page-settings-portfolio-type-small-image-description-alignment',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Item Description Alignment', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose alignment of item description.', 'medicale-wp' ),
				'options'	=> array(
					'left'   => esc_html__( 'Left', 'medicale-wp' ),
					'right'  => esc_html__( 'Right', 'medicale-wp' )
				),
				'default' => 'left',
				'required' => array( 
					array( 'portfolio-single-page-settings-select-portfolio-details-type', '=', 'small-image' )
				)
			),
			array(
				'id'        => 'portfolio-single-page-settings-portfolio-type-small-image-description-width',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Item Description Width', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose width of item description.', 'medicale-wp' ),
				'options'	=> array(
					'6'     => esc_html__( 'Half (1/2)', 'medicale-wp' ),
					'4'     => esc_html__( 'One Third (1/3)', 'medicale-wp' ),
					'3'     => esc_html__( 'One Fourth (1/4)', 'medicale-wp' )
				),
				'default' => '6',
				'required' => array( 
					array( 'portfolio-single-page-settings-select-portfolio-details-type', '=', 'small-image' )
				)
			),
			array(
				'id'        => 'portfolio-single-page-settings-portfolio-type-small-image-description-sticky',
				'type'     => 'switch',
				'title'    => esc_html__( 'Make Description Sticky', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Make description container sticky when scrolling down the page.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 
					array( 'portfolio-single-page-settings-select-portfolio-details-type', '=', 'small-image' )
				)
			),




			//Small Image Slider
			array(
				'id'        => 'portfolio-single-page-settings-portfolio-type-small-image-slider-settings',
				'type'      => 'info',
				'title'     => esc_html__( 'Small Image Slider Settings', 'medicale-wp' ),
				'notice'    => false,
				'required' => array( 'portfolio-single-page-settings-select-portfolio-details-type', '=', 'small-image-slider' ),
			),
			array(
				'id'        => 'portfolio-single-page-settings-portfolio-type-small-image-slider-description-alignment',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Item Description Alignment', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose alignment of item description.', 'medicale-wp' ),
				'options'	=> array(
					'left'   => esc_html__( 'Left', 'medicale-wp' ),
					'right'  => esc_html__( 'Right', 'medicale-wp' )
				),
				'default' => 'left',
				'required' => array( 
					array( 'portfolio-single-page-settings-select-portfolio-details-type', '=', 'small-image-slider' )
				)
			),
			array(
				'id'        => 'portfolio-single-page-settings-portfolio-type-small-image-slider-description-width',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Item Description Width', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose width of item description.', 'medicale-wp' ),
				'options'	=> array(
					'6'     => esc_html__( 'Half (1/2)', 'medicale-wp' ),
					'4'     => esc_html__( 'One Third (1/3)', 'medicale-wp' ),
					'3'     => esc_html__( 'One Fourth (1/4)', 'medicale-wp' )
				),
				'default' => '6',
				'required' => array( 
					array( 'portfolio-single-page-settings-select-portfolio-details-type', '=', 'small-image-slider' )
				)
			),
			array(
				'id'        => 'portfolio-single-page-settings-portfolio-type-small-image-slider-description-sticky',
				'type'     => 'switch',
				'title'    => esc_html__( 'Make Description Sticky', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Make description container sticky when scrolling down the page.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 
					array( 'portfolio-single-page-settings-select-portfolio-details-type', '=', 'small-image-slider' )
				)
			),




			//Small Image Gallery
			array(
				'id'        => 'portfolio-single-page-settings-portfolio-type-small-image-gallery-settings',
				'type'      => 'info',
				'title'     => esc_html__( 'Small Image Gallery Settings', 'medicale-wp' ),
				'notice'    => false,
				'required' => array( 'portfolio-single-page-settings-select-portfolio-details-type', '=', 'small-image-gallery' ),
			),
			array(
				'id'        => 'portfolio-single-page-settings-portfolio-type-small-image-gallery-description-alignment',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Item Description Alignment', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose alignment of item description.', 'medicale-wp' ),
				'options'	=> array(
					'left'   => esc_html__( 'Left', 'medicale-wp' ),
					'right'  => esc_html__( 'Right', 'medicale-wp' )
				),
				'default' => 'left',
				'required' => array( 
					array( 'portfolio-single-page-settings-select-portfolio-details-type', '=', 'small-image-gallery' )
				)
			),
			array(
				'id'        => 'portfolio-single-page-settings-portfolio-type-small-image-gallery-description-width',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Item Description Width', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose width of item description.', 'medicale-wp' ),
				'options'	=> array(
					'6'     => esc_html__( 'Half (1/2)', 'medicale-wp' ),
					'4'     => esc_html__( 'One Third (1/3)', 'medicale-wp' ),
					'3'     => esc_html__( 'One Fourth (1/4)', 'medicale-wp' )
				),
				'default' => '6',
				'required' => array( 
					array( 'portfolio-single-page-settings-select-portfolio-details-type', '=', 'small-image-gallery' )
				)
			),
			array(
				'id'        => 'portfolio-single-page-settings-portfolio-type-small-image-gallery-description-sticky',
				'type'     => 'switch',
				'title'    => esc_html__( 'Make Description Sticky', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Make description container sticky when scrolling down the page.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 
					array( 'portfolio-single-page-settings-select-portfolio-details-type', '=', 'small-image-gallery' )
				)
			),
			array(
				'id'       => 'portfolio-single-page-settings-portfolio-type-small-image-gallery-layout-mode',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Gallery Layout Mode (FitRows Or Masonry)', 'medicale-wp' ),
				'subtitle' => esc_html__( 'You can position items with different layout modes. Select a layout mode you would like to use.', 'medicale-wp' ),
				'desc'     => '',
				//Must provide key => value(array:title|img) pairs for radio options
				'options'	=> array(
					'fitrows' => array(
						'alt' => 'Fit Rows',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio/layout-mode/fitrows.png'
					),
					'masonry' => array(
						'alt' => 'Masonry',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio/layout-mode/masonry.png'
					),
				),
				'default'	=> 'masonry',
				'required' => array( 
					array( 'portfolio-single-page-settings-select-portfolio-details-type', '=', 'small-image-gallery' )
				)
			),
			array(
				'id'        => 'portfolio-single-page-settings-portfolio-type-small-image-gallery-items-per-row',
				'type'     => 'select',
				'title'    => esc_html__( 'Number of Images Per Row', 'medicale-wp' ),
				'subtitle'    => esc_html__( 'Select your default column structure for your gallery items.', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'1'  => '1 Item Per Row',
					'2'  => '2 Items Per Row',
					'3'  => '3 Items Per Row',
					'4'  => '4 Items Per Row',
					'5'  => '5 Items Per Row',
					'6'  => '6 Items Per Row',
					'7'  => '7 Items Per Row',
					'8'  => '8 Items Per Row',
					'9'  => '9 Items Per Row',
					'10' => '10 Items Per Row',
				),
				'default'	=> '3',
				'required' => array( 
					array( 'portfolio-single-page-settings-select-portfolio-details-type', '=', 'small-image-gallery' )
				)
			),




			//Small Image Gallery
			array(
				'id'        => 'portfolio-single-page-settings-portfolio-type-big-image-gallery-settings',
				'type'      => 'info',
				'title'     => esc_html__( 'Big Image Gallery Settings', 'medicale-wp' ),
				'notice'    => false,
				'required' => array( 'portfolio-single-page-settings-select-portfolio-details-type', '=', 'big-image-gallery' ),
			),
			array(
				'id'       => 'portfolio-single-page-settings-portfolio-type-big-image-gallery-layout-mode',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Gallery Layout Mode (FitRows Or Masonry)', 'medicale-wp' ),
				'subtitle' => esc_html__( 'You can position items with different layout modes. Select a layout mode you would like to use.', 'medicale-wp' ),
				'desc'     => '',
				//Must provide key => value(array:title|img) pairs for radio options
				'options'	=> array(
					'fitrows' => array(
						'alt' => 'Fit Rows',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio/layout-mode/fitrows.png'
					),
					'masonry' => array(
						'alt' => 'Masonry',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/portfolio/layout-mode/masonry.png'
					),
				),
				'default'	=> 'masonry',
				'required' => array( 
					array( 'portfolio-single-page-settings-select-portfolio-details-type', '=', 'big-image-gallery' )
				)
			),
			array(
				'id'        => 'portfolio-single-page-settings-portfolio-type-big-image-gallery-items-per-row',
				'type'     => 'select',
				'title'    => esc_html__( 'Number of Images Per Row', 'medicale-wp' ),
				'subtitle'    => esc_html__( 'Select your default column structure for your gallery items.', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'1'  => '1 Item Per Row',
					'2'  => '2 Items Per Row',
					'3'  => '3 Items Per Row',
					'4'  => '4 Items Per Row',
					'5'  => '5 Items Per Row',
					'6'  => '6 Items Per Row',
					'7'  => '7 Items Per Row',
					'8'  => '8 Items Per Row',
					'9'  => '9 Items Per Row',
					'10' => '10 Items Per Row',
				),
				'default'	=> '3',
				'required' => array( 
					array( 'portfolio-single-page-settings-select-portfolio-details-type', '=', 'big-image-gallery' )
				)
			),






			array(
				'id'        => 'portfolio-single-page-settings-other-settings',
				'type'      => 'info',
				'title'     => esc_html__( 'Other Settings', 'medicale-wp' ),
				'notice'    => false,
			),
			array(
				'id'       => 'portfolio-single-page-settings-portfolio-meta',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Portfolio Meta', 'medicale-wp' ),
				'subtitle'     => esc_html__( 'Enable/Disabling this option will show/hide each Portfolio Meta on your Portfolio Details Page.', 'medicale-wp' ),
				'desc' => '',
				//Must provide key => value pairs for multi checkbox options
				'options'	=> array(
					'show-post-by-author'       => esc_html__( 'Show By Author', 'medicale-wp' ),
					'show-post-date'            => esc_html__( 'Show Date', 'medicale-wp' ),
					'show-post-category'        => esc_html__( 'Show Category', 'medicale-wp' ),
					'show-post-tag'             => esc_html__( 'Show Tag', 'medicale-wp' ),
					'show-post-checklist-custom-fields'   => esc_html__( 'Show Checklist Custom Fields', 'medicale-wp' ),
				),
				//See how std has changed? you also don't need to specify opts that are 0.
				'default'	=> array(
					'show-post-by-author' => '0',
					'show-post-date' => '1',
					'show-post-category' => '1',
					'show-post-tag' => '1',
					'show-post-checklist-custom-fields' => '1',
				)
			),
			array(
				'id'       => 'portfolio-single-page-settings-show-share',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Share', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide share options on your page.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),


			//section Next/Previous Navigation Link Starts
			array(
				'id'       => 'portfolio-single-page-settings-show-next-pre-post-link-section-starts',
				'type'     => 'info',
				'title'    => esc_html__( 'Next/Previous Navigation Link', 'medicale-wp' ),
				'notice'   => false,
			),
			array(
				'id'       => 'portfolio-single-page-settings-show-next-pre-post-link',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Next/Previous Single Post Navigation Link', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide link for Next & Previous Posts.', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'portfolio-single-page-settings-show-next-pre-post-link-within-same-cat',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Navigation Link Within Same Category', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide link to the next/previous post within the same category as the current post.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 'portfolio-single-page-settings-show-next-pre-post-link', '=', '1' ),
			),




			//section Related Posts Starts
			array(
				'id'       => 'portfolio-single-page-settings-related-posts-section-starts',
				'type'     => 'info',
				'title'    => esc_html__( 'Related Posts', 'medicale-wp' ),
				'notice'   => false,
			),
			array(
				'id'       => 'portfolio-single-page-settings-show-related-posts',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Related Portfolio Items', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide Related Posts List/Carousel on your page.', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'portfolio-single-page-settings-show-related-posts-carousel',
				'type'     => 'switch',
				'title'    => esc_html__( 'Carousel?', 'medicale-wp' ),
				'subtitle' => 'Make it carousel or grid',
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 'portfolio-single-page-settings-show-related-posts', '=', '1' ),
			),
			array(
				'id'       => 'portfolio-single-page-settings-show-related-posts-count',
				'type'     => 'text',
				'title'    => esc_html__( 'Number of Posts', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => esc_html__( 'Enter number of posts to display. Default 3', 'medicale-wp' ),
				'default'	=> '3',
				'required' => array( 'portfolio-single-page-settings-show-related-posts', '=', '1' ),
			),
			array(
				'id'            => 'portfolio-single-page-settings-show-related-posts-excerpt-length',
				'type'          => 'slider',
				'title'         => esc_html__( 'Excerpt Length', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Number of words to display in excerpt.', 'medicale-wp' ),
				'desc'          => '',
				'default'       => 20,
				'min'           => 0,
				'step'          => 1,
				'max'           => 200,
				'display_value' => 'text',
				'required' => array( 'portfolio-single-page-settings-show-related-posts', '=', '1' ),
			),



			//section Related Posts Starts
			array(
				'id'       => 'portfolio-single-page-settings-comments-section-starts',
				'type'     => 'info',
				'title'    => esc_html__( 'Comments', 'medicale-wp' ),
				'notice'   => false,
			),
			array(
				'id'       => 'portfolio-single-page-settings-show-comments',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Comments', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide Comments on your page.', 'medicale-wp' ),
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),

		)
	) );






	// -> START Custom Post Types Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Custom Post Types', 'medicale-wp' ),
		'id'     => 'cpt-settings-parent',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-carrot',
	) );



	// -> START Custom Post Types Clients Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( '1. CPT - Clients', 'medicale-wp' ),
		'id'     => 'cpt-settings-clients',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'cpt-settings-clients-enable',
				'type'     => 'switch',
				'title'    => esc_html__( 'Clients Post Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Toggle the clients custom post type on or off.', 'medicale-wp' ),
				'default'	=> 1,
			),
			array(
				'id'       => 'cpt-settings-clients-label',
				'type'     => 'text',
				'title'    => esc_html__( 'Clients Label', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Rename the Custom Post Type. ', 'medicale-wp' ),
				'default'	=> 'Clients',
				'required' => array( 'cpt-settings-clients-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-clients-admin-dashicon',
				'type'     => 'select',
				'title'    => esc_html__( 'Clients Admin Dashboard Icon', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> medicale_mascot_wp_admin_dashicons_list(),
				'default'	=> 'dashicons-mascot',
				'required' => array( 'cpt-settings-clients-enable', '=', '1' ),
			),
		)
	) );


	// -> START Custom Post Types Departments Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( '2. CPT - Departments', 'medicale-wp' ),
		'id'     => 'cpt-settings-departments',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'cpt-settings-departments-enable',
				'type'     => 'switch',
				'title'    => esc_html__( 'Departments Post Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Toggle the departments custom post type on or off.', 'medicale-wp' ),
				'default'	=> 1,
			),
			array(
				'id'       => 'cpt-settings-departments-label',
				'type'     => 'text',
				'title'    => esc_html__( 'Departments Label', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Rename the Custom Post Type. ', 'medicale-wp' ),
				'default'	=> 'Departments',
				'required' => array( 'cpt-settings-departments-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-departments-admin-dashicon',
				'type'     => 'select',
				'title'    => esc_html__( 'Departments Admin Dashboard Icon', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> medicale_mascot_wp_admin_dashicons_list(),
				'default'   => 'dashicons-mascot',
				'required' => array( 'cpt-settings-departments-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-departments-slug',
				'type'     => 'text',
				'title'    => esc_html__( 'Departments Slug', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Specify a custom slug for Departments Post Type. ', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( '%1$sNOTE: When you change this setting you need to flush rewrite rules.%2$s %3$s%4$sTo do so, goto Settings > Permalinks and simply click on "Save Changes" button.%2$s', 'medicale-wp' ), '<strong style="color: #777;">', '</strong>', '<br>', '<strong style="color: #999;">'),
				'default'	=> 'department-items',
				'required' => array( 'cpt-settings-departments-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-departments-cat-slug',
				'type'     => 'text',
				'title'    => esc_html__( 'Departments Category Slug', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Specify a custom slug for the Category of Departments Post Type. ', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( '%1$sNOTE: When you change this setting you need to flush rewrite rules.%2$s %3$s%4$sTo do so, goto Settings > Permalinks and simply click on "Save Changes" button.%2$s', 'medicale-wp' ), '<strong style="color: #777;">', '</strong>', '<br>', '<strong style="color: #999;">'),
				'default'	=> 'department-category',
				'required' => array( 'cpt-settings-departments-enable', '=', '1' ),
			),
		)
	) );


	// -> START Custom Post Types FAQ Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( '3. CPT - FAQ', 'medicale-wp' ),
		'id'     => 'cpt-settings-faq',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'cpt-settings-faq-enable',
				'type'     => 'switch',
				'title'    => esc_html__( 'FAQ Post Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Toggle the faq custom post type on or off.', 'medicale-wp' ),
				'default'	=> 1,
			),
			array(
				'id'       => 'cpt-settings-faq-label',
				'type'     => 'text',
				'title'    => esc_html__( 'FAQ Label', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Rename the Custom Post Type. ', 'medicale-wp' ),
				'default'	=> 'FAQ',
				'required' => array( 'cpt-settings-faq-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-faq-admin-dashicon',
				'type'     => 'select',
				'title'    => esc_html__( 'FAQ Admin Dashboard Icon', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> medicale_mascot_wp_admin_dashicons_list(),
				'default'   => 'dashicons-mascot',
				'required' => array( 'cpt-settings-faq-enable', '=', '1' ),
			),




			//section Related Posts Starts
			array(
				'id'       => 'cpt-settings-faq-related-posts-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Related Posts', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide Related Posts List/Carousel on your page. The full settings will be found at "Blog > Single Post" Theme Options', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'       => 'cpt-settings-faq-show-related-posts',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Related Posts', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'cpt-settings-faq-show-related-posts-count',
				'type'     => 'text',
				'title'    => esc_html__( 'Number of Posts', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => esc_html__( 'Enter number of posts to display. Default 3', 'medicale-wp' ),
				'default'	=> '3',
				'required' => array( 'cpt-settings-faq-show-related-posts', '=', '1' ),
			),



		)
	) );


	// -> START Custom Post Types Features Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( '4. CPT - Features', 'medicale-wp' ),
		'id'     => 'cpt-settings-features',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'cpt-settings-features-enable',
				'type'     => 'switch',
				'title'    => esc_html__( 'Features Post Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Toggle the features custom post type on or off.', 'medicale-wp' ),
				'default'	=> 1,
			),
			array(
				'id'       => 'cpt-settings-features-label',
				'type'     => 'text',
				'title'    => esc_html__( 'Features Label', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Rename the Custom Post Type. ', 'medicale-wp' ),
				'default'	=> 'Features',
				'required' => array( 'cpt-settings-features-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-features-admin-dashicon',
				'type'     => 'select',
				'title'    => esc_html__( 'Features Admin Dashboard Icon', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> medicale_mascot_wp_admin_dashicons_list(),
				'default'   => 'dashicons-mascot',
				'required' => array( 'cpt-settings-features-enable', '=', '1' ),
			),




			//section Related Posts Starts
			array(
				'id'       => 'cpt-settings-features-related-posts-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Related Posts', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide Related Posts List/Carousel on your page. The full settings will be found at "Blog > Single Post" Theme Options', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'       => 'cpt-settings-features-show-related-posts',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Related Posts', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'cpt-settings-features-show-related-posts-count',
				'type'     => 'text',
				'title'    => esc_html__( 'Number of Posts', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => esc_html__( 'Enter number of posts to display. Default 3', 'medicale-wp' ),
				'default'	=> '3',
				'required' => array( 'cpt-settings-features-show-related-posts', '=', '1' ),
			),
		)
	) );


	// -> START Custom Post Types Gallery Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( '5. CPT - Gallery', 'medicale-wp' ),
		'id'     => 'cpt-settings-gallery',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'cpt-settings-gallery-enable',
				'type'     => 'switch',
				'title'    => esc_html__( 'Gallery Post Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Toggle the gallery custom post type on or off.', 'medicale-wp' ),
				'default'	=> 1,
			),
			array(
				'id'       => 'cpt-settings-gallery-label',
				'type'     => 'text',
				'title'    => esc_html__( 'Gallery Label', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Rename the Custom Post Type. ', 'medicale-wp' ),
				'default'	=> 'Gallery',
				'required' => array( 'cpt-settings-gallery-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-gallery-admin-dashicon',
				'type'     => 'select',
				'title'    => esc_html__( 'Gallery Admin Dashboard Icon', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> medicale_mascot_wp_admin_dashicons_list(),
				'default'   => 'dashicons-mascot',
				'required' => array( 'cpt-settings-gallery-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-gallery-slug',
				'type'     => 'text',
				'title'    => esc_html__( 'Gallery Slug', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Specify a custom slug for Gallery Post Type. ', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( '%1$sNOTE: When you change this setting you need to flush rewrite rules.%2$s %3$s%4$sTo do so, goto Settings > Permalinks and simply click on "Save Changes" button.%2$s', 'medicale-wp' ), '<strong style="color: #777;">', '</strong>', '<br>', '<strong style="color: #999;">'),
				'default'	=> 'gallery',
				'required' => array( 'cpt-settings-gallery-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-gallery-cat-slug',
				'type'     => 'text',
				'title'    => esc_html__( 'Gallery Category Slug', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Specify a custom slug for the Category of Gallery Post Type. ', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( '%1$sNOTE: When you change this setting you need to flush rewrite rules.%2$s %3$s%4$sTo do so, goto Settings > Permalinks and simply click on "Save Changes" button.%2$s', 'medicale-wp' ), '<strong style="color: #777;">', '</strong>', '<br>', '<strong style="color: #999;">'),
				'default'	=> 'gallery-category',
				'required' => array( 'cpt-settings-gallery-enable', '=', '1' ),
			),
		)
	) );


	// -> START Custom Post Types Portfolio Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( '6. CPT - Portfolio', 'medicale-wp' ),
		'id'     => 'cpt-settings-portfolio',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'cpt-settings-portfolio-enable',
				'type'     => 'switch',
				'title'    => esc_html__( 'Portfolio Post Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Toggle the portfolio custom post type on or off.', 'medicale-wp' ),
				'default'	=> 1,
			),
			array(
				'id'       => 'cpt-settings-portfolio-label',
				'type'     => 'text',
				'title'    => esc_html__( 'Portfolio Label', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Rename the Custom Post Type. ', 'medicale-wp' ),
				'default'	=> 'Portfolio',
				'required' => array( 'cpt-settings-portfolio-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-portfolio-admin-dashicon',
				'type'     => 'select',
				'title'    => esc_html__( 'Portfolio Admin Dashboard Icon', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> medicale_mascot_wp_admin_dashicons_list(),
				'default'   => 'dashicons-mascot',
				'required' => array( 'cpt-settings-portfolio-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-portfolio-slug',
				'type'     => 'text',
				'title'    => esc_html__( 'Portfolio Slug', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Specify a custom slug for Portfolio Post Type. ', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( '%1$sNOTE: When you change this setting you need to flush rewrite rules.%2$s %3$s%4$sTo do so, goto Settings > Permalinks and simply click on "Save Changes" button.%2$s', 'medicale-wp' ), '<strong style="color: #777;">', '</strong>', '<br>', '<strong style="color: #999;">'),
				'default'	=> 'portfolio',
				'required' => array( 'cpt-settings-portfolio-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-portfolio-cat-slug',
				'type'     => 'text',
				'title'    => esc_html__( 'Portfolio Category Slug', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Specify a custom slug for the Category of Portfolio Post Type. ', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( '%1$sNOTE: When you change this setting you need to flush rewrite rules.%2$s %3$s%4$sTo do so, goto Settings > Permalinks and simply click on "Save Changes" button.%2$s', 'medicale-wp' ), '<strong style="color: #777;">', '</strong>', '<br>', '<strong style="color: #999;">'),
				'default'	=> 'portfolio-category',
				'required' => array( 'cpt-settings-portfolio-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-portfolio-tag-slug',
				'type'     => 'text',
				'title'    => esc_html__( 'Portfolio Tag Slug', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Specify a custom slug for the Tag of Portfolio Post Type. ', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( '%1$sNOTE: When you change this setting you need to flush rewrite rules.%2$s %3$s%4$sTo do so, goto Settings > Permalinks and simply click on "Save Changes" button.%2$s', 'medicale-wp' ), '<strong style="color: #777;">', '</strong>', '<br>', '<strong style="color: #999;">'),
				'default'	=> 'portfolio-tag',
				'required' => array( 'cpt-settings-portfolio-enable', '=', '1' ),
			),
		)
	) );


	// -> START Custom Post Types Pricing Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( '7. CPT - Pricing', 'medicale-wp' ),
		'id'     => 'cpt-settings-pricing',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'cpt-settings-pricing-enable',
				'type'     => 'switch',
				'title'    => esc_html__( 'Pricing Post Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Toggle the pricing custom post type on or off.', 'medicale-wp' ),
				'default'	=> 1,
			),
			array(
				'id'       => 'cpt-settings-pricing-label',
				'type'     => 'text',
				'title'    => esc_html__( 'Pricing Label', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Rename the Custom Post Type. ', 'medicale-wp' ),
				'default'	=> 'Pricing',
				'required' => array( 'cpt-settings-pricing-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-pricing-admin-dashicon',
				'type'     => 'select',
				'title'    => esc_html__( 'Pricing Admin Dashboard Icon', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> medicale_mascot_wp_admin_dashicons_list(),
				'default'   => 'dashicons-mascot',
				'required' => array( 'cpt-settings-pricing-enable', '=', '1' ),
			),
		)
	) );


	// -> START Custom Post Types Services Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( '8. CPT - Services', 'medicale-wp' ),
		'id'     => 'cpt-settings-services',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'cpt-settings-services-enable',
				'type'     => 'switch',
				'title'    => esc_html__( 'Services Post Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Toggle the services custom post type on or off.', 'medicale-wp' ),
				'default'	=> 1,
			),
			array(
				'id'       => 'cpt-settings-services-label',
				'type'     => 'text',
				'title'    => esc_html__( 'Services Label', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Rename the Custom Post Type. ', 'medicale-wp' ),
				'default'	=> 'Services',
				'required' => array( 'cpt-settings-services-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-services-admin-dashicon',
				'type'     => 'select',
				'title'    => esc_html__( 'Services Admin Dashboard Icon', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> medicale_mascot_wp_admin_dashicons_list(),
				'default'   => 'dashicons-mascot',
				'required' => array( 'cpt-settings-services-enable', '=', '1' ),
			),




			//section Related Posts Starts
			array(
				'id'       => 'cpt-settings-services-related-posts-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Related Posts', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enable/Disabling this option will show/hide Related Posts List/Carousel on your page. The full settings will be found at "Blog > Single Post" Theme Options', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'       => 'cpt-settings-services-show-related-posts',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Related Posts', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'cpt-settings-services-show-related-posts-count',
				'type'     => 'text',
				'title'    => esc_html__( 'Number of Posts', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => esc_html__( 'Enter number of posts to display. Default 3', 'medicale-wp' ),
				'default'	=> '3',
				'required' => array( 'cpt-settings-services-show-related-posts', '=', '1' ),
			),
		)
	) );


	// -> START Custom Post Types Staff Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( '9. CPT - Staff', 'medicale-wp' ),
		'id'     => 'cpt-settings-staff',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'cpt-settings-staff-enable',
				'type'     => 'switch',
				'title'    => esc_html__( 'Staff Post Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Toggle the staff custom post type on or off.', 'medicale-wp' ),
				'default'	=> 1,
			),
			array(
				'id'       => 'cpt-settings-staff-label',
				'type'     => 'text',
				'title'    => esc_html__( 'Staff Label', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Rename the Custom Post Type. ', 'medicale-wp' ),
				'default'	=> 'Staff',
				'required' => array( 'cpt-settings-staff-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-staff-admin-dashicon',
				'type'     => 'select',
				'title'    => esc_html__( 'Staff Admin Dashboard Icon', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> medicale_mascot_wp_admin_dashicons_list(),
				'default'   => 'dashicons-mascot',
				'required' => array( 'cpt-settings-staff-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-staff-slug',
				'type'     => 'text',
				'title'    => esc_html__( 'Staff Slug', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Specify a custom slug for Staff Post Type. ', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( '%1$sNOTE: When you change this setting you need to flush rewrite rules.%2$s %3$s%4$sTo do so, goto Settings > Permalinks and simply click on "Save Changes" button.%2$s', 'medicale-wp' ), '<strong style="color: #777;">', '</strong>', '<br>', '<strong style="color: #999;">'),
				'default'	=> 'staff',
				'required' => array( 'cpt-settings-staff-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-staff-cat-slug',
				'type'     => 'text',
				'title'    => esc_html__( 'Staff Category Slug', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Specify a custom slug for the Category of Staff Post Type. ', 'medicale-wp' ),
				'desc'     => sprintf( esc_html__( '%1$sNOTE: When you change this setting you need to flush rewrite rules.%2$s %3$s%4$sTo do so, goto Settings > Permalinks and simply click on "Save Changes" button.%2$s', 'medicale-wp' ), '<strong style="color: #777;">', '</strong>', '<br>', '<strong style="color: #999;">'),
				'default'	=> 'staff-category',
				'required' => array( 'cpt-settings-staff-enable', '=', '1' ),
			),
		)
	) );


	// -> START Custom Post Types Testimonials Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( '10. CPT - Testimonials', 'medicale-wp' ),
		'id'     => 'cpt-settings-testimonials',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'cpt-settings-testimonials-enable',
				'type'     => 'switch',
				'title'    => esc_html__( 'Testimonials Post Type', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Toggle the testimonials custom post type on or off.', 'medicale-wp' ),
				'default'	=> 1,
			),
			array(
				'id'       => 'cpt-settings-testimonials-label',
				'type'     => 'text',
				'title'    => esc_html__( 'Testimonials Label', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Rename the Custom Post Type. ', 'medicale-wp' ),
				'default'	=> 'Testimonials',
				'required' => array( 'cpt-settings-testimonials-enable', '=', '1' ),
			),
			array(
				'id'       => 'cpt-settings-testimonials-admin-dashicon',
				'type'     => 'select',
				'title'    => esc_html__( 'Testimonials Admin Dashboard Icon', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> medicale_mascot_wp_admin_dashicons_list(),
				'default'   => 'dashicons-mascot',
				'required' => array( 'cpt-settings-testimonials-enable', '=', '1' ),
			),
		)
	) );




	// -> START Shop Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Woocommerce Shop', 'medicale-wp' ),
		'id'     => 'shop-settings-parent',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-cart',
	) );



	// -> START Shop Archive Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Shop Archive/Category Layout', 'medicale-wp' ),
		'id'     => 'shop-archive-settings',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'shop-archive-settings-fullwidth',
				'type'     => 'switch',
				'title'    => esc_html__( 'Page Fullwidth?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Make the shop page fullwidth or not.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'shop-archive-settings-sidebar-position',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Sidebar Position', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Controls the position of shop sidebar.', 'medicale-wp' ),
				'options'	=> array(
					'left'          => esc_html__( 'Left', 'medicale-wp' ),
					'right'         => esc_html__( 'Right', 'medicale-wp' ),
					'no-sidebar'    => esc_html__( 'No Sidebar', 'medicale-wp' )
				),
				'default'	=> 'no-sidebar',
			),




			array(
				'id'       => 'shop-layout-settings-select-shop-catalog-layout',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Shop Catalog Layout', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select the type of layout you would like to display.', 'medicale-wp' ),
				'desc'     => '',
				//Must provide key => value(array:title|img) pairs for radio options
				'options'	=> array(
					'default' => array(
						'alt' => 'Default',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/shop/type/default.png'
					),
					'standard' => array(
						'alt' => 'Standard',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/shop/type/standard.png'
					),
				),
				'default'	=> 'default'
			),

			array(
				'id'       => 'shop-layout-settings-select-shop-layout-mode',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Shop Layout Mode (FitRows Or Masonry)', 'medicale-wp' ),
				'subtitle' => esc_html__( 'You can position items with different layout modes. Select a layout mode you would like to use.', 'medicale-wp' ),
				'desc'     => '',
				//Must provide key => value(array:title|img) pairs for radio options
				'options'	=> array(
					'fitrows' => array(
						'alt' => 'Fit Rows',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/shop/layout-mode/fitrows.png'
					),
					'masonry' => array(
						'alt' => 'Masonry',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/shop/layout-mode/masonry.png'
					),
				),
				'default'	=> 'masonry'
			),


			array(
				'id'       => 'shop-archive-settings-products-per-row',
				'type'     => 'select',
				'title'    => esc_html__( 'Number of Products Per Row', 'medicale-wp' ),
				'subtitle'    => esc_html__( 'Select your default column structure for your shop items.', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'1'  => '1 Item Per Row',
					'2'  => '2 Items Per Row',
					'3'  => '3 Items Per Row',
					'4'  => '4 Items Per Row',
					'5'  => '5 Items Per Row',
					'6'  => '6 Items Per Row',
					'7'  => '7 Items Per Row',
					'8'  => '8 Items Per Row',
					'9'  => '9 Items Per Row',
					'10' => '10 Items Per Row',
				),
				'default'	=> '4',
			),
			array(
				'id'            => 'shop-archive-settings-products-per-page',
				'type'          => 'slider',
				'title'         => esc_html__( 'Number of Products Per Page', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Controls the number of items to display on shop archive pages. Set to -1 to display all. Set to 0 to use the number of posts from Settings > Reading.', 'medicale-wp' ),
				'desc'          => '',
				'default'       => 10,
				'min'           => -1,
				'step'          => 1,
				'max'           => 40,
				'display_value' => 'text',
			),
			array(
				'id'            => 'shop-archive-settings-products-per-page-dropdown-options',
				'type'          => 'text',
				'title'         => esc_html__( 'WooCommerce Products Per Page Dropdown', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'List of options products per page to show into the select dropdown menu.', 'medicale-wp' ),
				'desc'         => esc_html__( 'Seperated by spaces', 'medicale-wp' ),
				'default'       => '8 16 32 64',
			),
			array(
				'id'            => 'shop-archive-settings-gutter-size',
				'type'          => 'slider',
				'title'         => esc_html__( 'Shop Column Spacing (Gutter Size) px', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Controls column spacing or gutter size between items on shop archive pages.', 'medicale-wp' ),
				'desc'          => '',
				'default'       => 20,
				'min'           => 0,
				'step'          => 1,
				'max'           => 250,
				'display_value' => 'text',
			),
			array(
				'id'       => 'shop-archive-settings-products-thumb-type',
				'type'     => 'select',
				'title'    => esc_html__( 'Product Thumbnail Type', 'medicale-wp' ),
				'subtitle'    => esc_html__( 'Select your preferred style for your WooCommmerce product thumbnail.', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'image-featured'  => 'Featured Image',
					'image-swap'  => 'Image Swap',
					'image-gallery'  => 'Gallery Images',
				),
				'default'	=> 'image-swap',
			),
		)
	) );



	// -> START Shop Single Product Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Shop Single Product', 'medicale-wp' ),
		'id'     => 'shop-single-product-settings',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'shop-single-product-settings-fullwidth',
				'type'     => 'switch',
				'title'    => esc_html__( 'Page Fullwidth?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Make the single product page fullwidth or not.', 'medicale-wp' ),
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'shop-single-product-settings-sidebar-position',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Sidebar Position', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Controls the sidebar position of shop single product page.', 'medicale-wp' ),
				'options'	=> array(
					'left'          => esc_html__( 'Left', 'medicale-wp' ),
					'right'         => esc_html__( 'Right', 'medicale-wp' ),
					'no-sidebar'    => esc_html__( 'No Sidebar', 'medicale-wp' )
				),
				'default'	=> 'no-sidebar',
			),



			array(
				'id'       => 'shop-single-product-settings-select-single-catalog-layout',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Product Details Layout', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select the type of layout you would like to display.', 'medicale-wp' ),
				'desc'     => '',
				//Must provide key => value(array:title|img) pairs for radio options
				'options'	=> array(
					'image-with-thumb' => array(
						'alt' => 'image-with-thumb',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/shop/single-layout/image-with-thumb.png'
					),
					'plain-image' => array(
						'alt' => 'plain-image',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/shop/single-layout/plain-image.png'
					),
					'sticky-side-text' => array(
						'alt' => 'sticky-side-text',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/shop/single-layout/sticky-side-text.png'
					),
				),
				'default'	=> 'image-with-thumb'
			),



			array(
				'id'       => 'shop-single-product-settings-product-images-column-width',
				'type'     => 'select',
				'title'    => esc_html__( 'Product Images Column Width', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'4'     => esc_html__( 'Small - 4/12', 'medicale-wp' ),
					'5'     => esc_html__( 'Medium - 5/12', 'medicale-wp' ),
					'6'     => esc_html__( 'Large - 6/12', 'medicale-wp' ),
					'8'     => esc_html__( 'Extra Large - 8/12', 'medicale-wp' ),
				),
				'default'	=> '6',
			),
			array(
				'id'       => 'shop-single-product-settings-product-images-align',
				'type'     => 'select',
				'title'    => esc_html__( 'Product Images Alignment', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'left'     => esc_html__( 'Left', 'medicale-wp' ),
					'right'    => esc_html__( 'Right', 'medicale-wp' ),
				),
				'default'	=> 'left',
			),

			array(
				'id'       => 'shop-single-product-settings-enable-gallery-slider',
				'type'     => 'switch',
				'title'    => esc_html__( 'Enable Image Gallery Slider Feature', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 'shop-single-product-settings-select-single-catalog-layout', '=', 'image-with-thumb' ),
			),
			array(
				'id'       => 'shop-single-product-settings-enable-gallery-lightbox',
				'type'     => 'switch',
				'title'    => esc_html__( 'Enable Image Gallery Lightbox Feature', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'shop-single-product-settings-enable-gallery-zoom',
				'type'     => 'switch',
				'title'    => esc_html__( 'Enable Image Gallery Zoom Feature', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),

			
			array(
				'id'       => 'shop-single-product-settings-related-products-per-row',
				'type'     => 'select',
				'title'    => esc_html__( 'Related Products Columns', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Set number of columns for related and upsells products only', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'1'  => '1 Item Per Row',
					'2'  => '2 Items Per Row',
					'3'  => '3 Items Per Row',
					'4'  => '4 Items Per Row',
					'5'  => '5 Items Per Row',
					'6'  => '6 Items Per Row',
					'7'  => '7 Items Per Row',
					'8'  => '8 Items Per Row',
					'9'  => '9 Items Per Row',
					'10' => '10 Items Per Row',
				),
				'default'	=> '4',
			),
			array(
				'id'            => 'shop-single-product-settings-related-products-count',
				'type'          => 'text',
				'title'         => esc_html__( 'Related Products Count', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Number of related products shown on single product page. Enter "0" to disable.', 'medicale-wp' ),
				'default'       => '8',
			),




			array(
				'id'       => 'shop-single-product-settings-title-tag',
				'type'     => 'select',
				'title'    => esc_html__( 'Single Product Title Tag', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> medicale_mascot_heading_tag_list_all(),
				'default'	=> 'h3',
			),
			array(
				'id'       => 'shop-single-product-settings-enable-product-meta',
				'type'     => 'switch',
				'title'    => esc_html__( 'Product Meta', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => 'shop-single-product-settings-enable-sharing',
				'type'     => 'switch',
				'title'    => esc_html__( 'Enable Sharing', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
		)
	) );



	// -> START WooCommerce Styling Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'WooCommerce Styling', 'medicale-wp' ),
		'id'     => 'woocommerce-styling-settings',
		'subsection'       => true,
		'desc'   => '',
		'fields' => array(
			array(
				'id'       => 'woocommerce-styling-product-price-color',
				'type'     => 'color',
				'title'    => esc_html__( 'Product Price Color', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select your custom color for product price.', 'medicale-wp' ),
				'subtitle' => '',
				'transparent' => false,
			),
			array(
				'id'       => 'woocommerce-styling-product-on-sale-tag-bg-color',
				'type'     => 'color',
				'title'    => esc_html__( 'On Sale Tag Background Color', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select your custom background color for on-sale tag.', 'medicale-wp' ),
				'subtitle' => '',
				'transparent' => true,
			),
		)
	) );



	// -> START Sidebar Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Sidebar', 'medicale-wp' ),
		'id'     => 'sidebar-settings',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-align-left',
		'fields' => array(
			array(
				'id'       => 'sidebar-settings-sidebar-padding',
				'type'     => 'spacing',
				// An array of CSS selectors to apply this font style to
				'mode'     => 'padding',
				// absolute, padding, margin, defaults to padding
				'all'      => false,
				// Have one field that applies to all
				'top'           => true,
				'right'         => true,
				'bottom'        => true,
				'left'          => true,
				'units'         => 'px',      // You can specify a unit value. Possible: px, em, %
				//'units_extended'=> 'true',    // Allow users to select any type of unit
				'display_units' => true,   // Set to false to hide the units if the units are specified
				'title'    => esc_html__( 'Sidebar Padding(px)', 'medicale-wp' ),
				'subtitle' => 'Controls the sidebar padding. Please put only integer value. Because the unit \'px\' will be automatically added to the end of the value.',
			),
			array(
				'id'       => 'sidebar-settings-sidebar-bg-color',
				'type'     => 'color',
				'title'    => esc_html__( 'Sidebar Background Color', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Controls the background color of sidebar.', 'medicale-wp' ),
				'subtitle' => '',
				'transparent' => false,
			),
			array(
				'id'       => 'sidebar-settings-sidebar-text-align',
				'type'     => 'select',
				'title'    => esc_html__( 'Sidebar Text Alignment', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'left'     => esc_html__( 'Left', 'medicale-wp' ),
					'center'   => esc_html__( 'Center', 'medicale-wp' ),
					'right'    => esc_html__( 'Right', 'medicale-wp' ),
				),
				'default'	=> '',
			),

			
			//section Related Items Starts
			array(
				'id'       => 'sidebar-settings-sidebar-title-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Sidebar Widget Title', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'       => 'sidebar-settings-sidebar-title-padding',
				'type'     => 'spacing',
				// An array of CSS selectors to apply this font style to
				'mode'     => 'padding',
				// absolute, padding, margin, defaults to padding
				'all'      => false,
				// Have one field that applies to all
				'top'           => true,
				'right'         => true,
				'bottom'        => true,
				'left'          => true,
				'units'         => 'px',      // You can specify a unit value. Possible: px, em, %
				//'units_extended'=> 'true',    // Allow users to select any type of unit
				'display_units' => true,   // Set to false to hide the units if the units are specified
				'title'    => esc_html__( 'Widget Title Padding(px)', 'medicale-wp' ),
				'subtitle' => 'Controls the sidebar widget title padding. Please put only integer value. Because the unit \'px\' will be automatically added to the end of the value.',
			),
			array(
				'id'       => 'sidebar-settings-sidebar-title-bg-color',
				'type'     => 'color',
				'title'    => esc_html__( 'Background Color', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Controls the background color of sidebar widget title box', 'medicale-wp' ),
				'subtitle' => '',
				'transparent' => false,
			),
			array(
				'id'       => 'sidebar-settings-sidebar-title-text-color',
				'type'     => 'color',
				'title'    => esc_html__( 'Text Color', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Controls the background color of sidebar widget title box', 'medicale-wp' ),
				'subtitle' => '',
				'transparent' => false,
			),
			array(
				'id'            => 'sidebar-settings-sidebar-title-font-size',
				'type'          => 'text',
				'title'         => esc_html__( 'Font Size(px)', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Please put only integer value. Because the unit \'px\' will be automatically added to the end of the value.', 'medicale-wp' ),
				'desc'          => '',
			),
			array(
				'id'       => 'sidebar-settings-sidebar-title-show-line-bottom',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Show Line Bottom', 'medicale-wp' ),
				'subtitle' => esc_html__( 'If you enable it then a thin line will be visible below the widget title.', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> '0',
			),
			array(
				'id'       => 'sidebar-settings-sidebar-title-line-bottom-color',
				'type'     => 'color',
				'title'    => esc_html__( 'Line Bottom Color', 'medicale-wp' ),
				'subtitle' => '',
				'transparent' => false,
				'required' => array( 'sidebar-settings-sidebar-title-show-line-bottom', '=', '1' ),
			),
			array(
				'id'     => 'sidebar-settings-sidebar-title-section-ends',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),
		)
	) );



	// -> START 404 Page Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( '404 Page', 'medicale-wp' ),
		'id'     => '404-page-settings',
		'desc'   => esc_html__( 'Title, content and background settings for 404 Error Page', 'medicale-wp' ),
		'icon'   => 'dashicons-before dashicons-editor-help',
		'fields' => array(
			array(
				'id'       => '404-page-settings-layout',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Choose Layout', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose one among different layouts.', 'medicale-wp' ),
				'desc'     => '',
				//Must provide key => value(array:title|img) pairs for radio options
				'options'	=> array(
					'simple' => array(
						'alt' => 'Simple',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/404/simple.jpg'
					),
					'split' => array(
						'alt' => 'Split',
						'img' => MASCOT_ADMIN_ASSETS_URI . '/images/404/split.jpg'
					),
				),
				'default'	=> 'simple',
			),
			array(
				'id'       => '404-page-settings-show-header',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Header', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => '404-page-settings-show-footer',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Footer', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			
			array(
				'id'       => '404-page-settings-text-align',
				'type'     => 'select',
				'title'    => esc_html__( 'Text Alignment', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Text Alignment of this page', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> medicale_mascot_redux_text_alignment_list(),
				'default'	=> 'text-center',
			),
			array(
				'id'       => '404-page-settings-show-back-to-home-button',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Back to Home Button', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => '404-page-settings-back-to-home-button-label',
				'type'     => 'text',
				'title'    => esc_html__( 'Back to Home Button Label', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => esc_html__( 'Default: "Back to Home"', 'medicale-wp' ),
				'default'	=> esc_html__( 'Back to Home', 'medicale-wp' ),
				'required' => array( 
					array( '404-page-settings-show-back-to-home-button', '=', '1' )
				)
			),
			array(
				'id'       => '404-page-settings-show-social-links',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Social Links', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),





			//section custom background
			array(
				'id'       => '404-page-settings-custom-background-section-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Custom Background', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Define background for 404 page.', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'       => '404-page-settings-custom-background-status',
				'type'     => 'switch',
				'title'    => esc_html__( 'Custom Background', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => '404-page-settings-bg',
				'type'     => 'background',
				'title'    => esc_html__( 'Background', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Choose background image or color.', 'medicale-wp' ),
				'required' => array( 
					array( '404-page-settings-custom-background-status', '=', '1' )
				)
			),
			array(
				'id'       => '404-page-settings-bg-layer-overlay-status',
				'type'     => 'switch',
				'title'    => esc_html__( 'Add Background Overlay', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 0,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
				'required' => array( 
					array( '404-page-settings-custom-background-status', '=', '1' )
				)
			),
			array(
				'id'       => '404-page-settings-bg-layer-overlay',
				'type'          => 'slider',
				'title'         => esc_html__( 'Background Overlay Opacity', 'medicale-wp' ),
				'subtitle'      => esc_html__( 'Overlay on background image on footer.', 'medicale-wp' ),
				'desc'          => '',
				'default'       => 7,
				'min'           => 1,
				'step'          => 1,
				'max'           => 9,
				'display_value' => 'text',
				'required' => array( 
					array( '404-page-settings-custom-background-status', '=', '1' ),
					array( '404-page-settings-bg-layer-overlay-status', '=', '1' )
				)
			),
			array(
				'id'       => '404-page-settings-bg-layer-overlay-color',
				'type'     => 'button_set',
				'compiler' =>true,
				'title'    => esc_html__( 'Background Overlay Color', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Select Dark or White Overlay on background image.', 'medicale-wp' ),
				'options'	=> array(
					'dark'          => esc_html__( 'Dark', 'medicale-wp' ),
					'white'         => esc_html__( 'White', 'medicale-wp' ),
					'theme-colored' => esc_html__( 'Primary Theme Color', 'medicale-wp' )
				),
				'default' => 'dark',
				'required' => array( 
					array( '404-page-settings-custom-background-status', '=', '1' ),
					array( '404-page-settings-bg-layer-overlay-status', '=', '1' )
				)
			),
			array(
				'id'       => '404-page-settings-custom-background-section-ends',
				'type'     => 'section',
				'title'    => '',
				'subtitle' => '',
				'indent'   => false, // Indent all options below until the next 'section' option is set.
				'required' => array( 
					array( '404-page-settings-custom-background-status', '=', '1' )
				)
			),





			//section Title Starts
			array(
				'id'       => '404-page-settings-title-typography-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Title', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Define text and styles for Title.', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'       => '404-page-settings-title',
				'type'     => 'text',
				'title'    => esc_html__( 'Title Text', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Set page title to show', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> esc_html__( '404!', 'medicale-wp' ),
			),
			array(
				'id'            => '404-page-settings-title-typography',
				'type'          => 'typography',
				'title'         => esc_html__( 'Title Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
			),
			array(
				'id'       => '404-page-settings-title-margin-top-bottom',
				'type'     => 'spacing',
				// An array of CSS selectors to apply this font style to
				'mode'     => 'margin',
				// absolute, padding, margin, defaults to padding
				'all'      => false,
				// Have one field that applies to all
				'top'      => true,     // Disable the top
				'right'    => false,     // Disable the right
				'bottom'   => true,     // Disable the bottom
				'left'     => false,     // Disable the left
				'units'    => 'px',      // You can specify a unit value. Possible: px, em, %
				//'units_extended'=> 'true',    // Allow users to select any type of unit
				'display_units' => true,   // Set to false to hide the units if the units are specified
				'title'    => esc_html__( 'Margin Top & Bottom', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
			),
			array(
				'id'       => '404-page-settings-title-typography-ends',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),





			//section Content Starts
			array(
				'id'       => '404-page-settings-content-typography-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Content', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Define text and styles for Content.', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'       => '404-page-settings-content',
				'type'     => 'editor',
				'title'    => esc_html__( 'Content Text', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Enter the content for 404 page which will be showed below title.', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> esc_html__( 'Sorry - The page you are looking for no longer exists. Perhaps you can return back to the site\'s homepage and see if you can find what you are looking for.', 'medicale-wp' ),
			),
			array(
				'id'            => '404-page-settings-content-typography',
				'type'          => 'typography',
				'title'         => esc_html__( 'Content Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
			),
			array(
				'id'       => '404-page-settings-content-margin-top-bottom',
				'type'     => 'spacing',
				// An array of CSS selectors to apply this font style to
				'mode'     => 'margin',
				// absolute, padding, margin, defaults to padding
				'all'      => false,
				// Have one field that applies to all
				'top'      => true,     // Disable the top
				'right'    => false,     // Disable the right
				'bottom'   => true,     // Disable the bottom
				'left'     => false,     // Disable the left
				'units'    => 'px',      // You can specify a unit value. Possible: px, em, %
				//'units_extended'=> 'true',    // Allow users to select any type of unit
				'display_units' => true,   // Set to false to hide the units if the units are specified
				'title'    => esc_html__( 'Margin Top & Bottom', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
			),
			array(
				'id'       => '404-page-settings-content-typography-ends',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),




			//section Helpful Links Starts
			array(
				'id'       => '404-page-settings-helpful-links-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Helpful Links', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Define text and styles for helpful links.', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'       => '404-page-settings-show-helpful-links',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Helpful Links', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => sprintf( esc_html__( 'Please create a new menu from %1$sAppearance > Menus%2$s and set Theme Location %1$s"Page 404 Helpful Links"%2$s', 'medicale-wp' ), '<strong>', '</strong>', '<br>'),
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => '404-page-settings-helpful-links-heading',
				'type'     => 'text',
				'title'    => esc_html__( 'Heading Text', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Set heading text to show', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> esc_html__( 'Helpful Links', 'medicale-wp' ),
				'required' => array( 
					array( '404-page-settings-show-helpful-links', '=', '1' )
				)
			),
			array(
				'id'            => '404-page-settings-helpful-links-heading-typography',
				'type'          => 'typography',
				'title'         => esc_html__( 'Heading Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
				'required' => array( 
					array( '404-page-settings-show-helpful-links', '=', '1' )
				)
			),
			array(
				'id'            => '404-page-settings-helpful-links-typography',
				'type'          => 'typography',
				'title'         => esc_html__( 'Links Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
				'required' => array( 
					array( '404-page-settings-show-helpful-links', '=', '1' )
				)
			),
			array(
				'id'       => '404-page-settings-helpful-links-ends',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),




			//section Search Box Starts
			array(
				'id'       => '404-page-settings-search-box-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Search Box', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Define text and styles for search box.', 'medicale-wp' ),
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'       => '404-page-settings-show-search-box',
				'type'     => 'switch',
				'title'    => esc_html__( 'Show Search Box', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),
			array(
				'id'       => '404-page-settings-search-box-heading',
				'type'     => 'text',
				'title'    => esc_html__( 'Heading Text', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Set heading text to show', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> esc_html__( 'Search Website', 'medicale-wp' ),
				'required' => array( 
					array( '404-page-settings-show-search-box', '=', '1' )
				)
			),
			array(
				'id'       => '404-page-settings-search-box-paragraph',
				'type'     => 'textarea',
				'title'    => esc_html__( 'Paragraph Text', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Set paragraph text to show', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> esc_html__( 'Please use the search box to find what you are looking for. Perhaps searching can help.', 'medicale-wp' ),
				'required' => array( 
					array( '404-page-settings-show-search-box', '=', '1' )
				)
			),
			array(
				'id'            => '404-page-settings-search-box-heading-typography',
				'type'          => 'typography',
				'title'         => esc_html__( 'Heading Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
				'required' => array( 
					array( '404-page-settings-show-search-box', '=', '1' )
				)
			),
			array(
				'id'            => '404-page-settings-search-box-paragraph-typography',
				'type'          => 'typography',
				'title'         => esc_html__( 'Paragraph Typography', 'medicale-wp' ),
				'subtitle'      => '',
				//'compiler'    => true,  // Use if you want to hook in your own CSS compiler
				'google'        => true,
				// Disable google fonts. Won't work if you haven't defined your google api key
				'font-backup'   => false,
				// Select a backup non-google font in addition to a google font
				'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'       => false, // Only appears if google is true and subsets not set to false
				'font-size'     => true,
				'line-height'   => true,
				'word-spacing'  => true,  // Defaults to false
				'letter-spacing'=> true,  // Defaults to false
				'text-transform'=> true,  // Defaults to false
				'color'         => true,
				'preview'       => true, // Disable the previewer
				'all_styles'    => true,
				'units'         => 'px',
				'required' => array( 
					array( '404-page-settings-show-search-box', '=', '1' )
				)
			),
			array(
				'id'       => '404-page-settings-search-box-ends',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),
		)
	) );


	if( medicale_mascot_core_plugin_installed() && function_exists( 'mascot_core_redux_opt_maintenance_section' ) ) {
		Redux::setSection( $opt_name, mascot_core_redux_opt_maintenance_section() );
	}


	// -> START Social Links Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Social Links', 'medicale-wp' ),
		'id'     => 'social-links',
		'desc'   => esc_html__( 'This is your official social links. Set the order of social links to be appeared in the header/footer section.', 'medicale-wp' ),
		'icon'   => 'dashicons-before dashicons-facebook-alt',
		'fields' => array(
			array(
				'id'       => 'social-links-ordering',
				'type'     => 'sorter',
				'title'    => esc_html__( 'Social Links Ordering', 'medicale-wp' ),
				'desc'     => '',
				'compiler' => 'true',
				'options'	=> array(
					'Enabled' => array(
						'twitter'     => 'Twitter',
						'facebook'    => 'Facebook',
						'google-plus'     => 'Google+',
						'linkedin'     => 'Linkedin',
						'tumblr'     => 'Tumblr',
						'vk'     => 'VK',
					),
					'Backup'  => array(
						'pinterest'     => 'Pinterest',
						'reddit'     => 'Reddit',
						'envelope'     => 'Email',
						'external-link'     => 'Custom Link',
					),
				),
			),
			array(
				'id'       => 'social-links-open-in-window',
				'type'     => 'select',
				'title'    => esc_html__( 'Open links in', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> array(
					'_blank' => 'New Tab',
					'_self'  => 'Same Tab',
				),
				'default'	=> '_blank',
			),

			//section Social URLs Starts
			array(
				'id'       => 'social-links-urls-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Social URLs', 'medicale-wp' ),
				'subtitle' => '',
				'indent'   => true, // Indent all options below until the next 'section' option is set.
			),
			array(
				'id'       => 'social-links-url-twitter',
				'type'     => 'text',
				'title'    => esc_html__( 'Twitter', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => 'Example: http://twitter.com/envato',
				'default'	=> '',
			),
			array(
				'id'       => 'social-links-url-facebook',
				'type'     => 'text',
				'title'    => esc_html__( 'Facebook', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> '',
			),
			array(
				'id'       => 'social-links-url-google-plus',
				'type'     => 'text',
				'title'    => esc_html__( 'Google+', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> '',
			),
			array(
				'id'       => 'social-links-url-linkedin',
				'type'     => 'text',
				'title'    => esc_html__( 'Linkedin', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> '',
			),
			array(
				'id'       => 'social-links-url-tumblr',
				'type'     => 'text',
				'title'    => esc_html__( 'Tumblr', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> '',
			),
			array(
				'id'       => 'social-links-url-vk',
				'type'     => 'text',
				'title'    => esc_html__( 'VK', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> '',
			),
			array(
				'id'       => 'social-links-url-pinterest',
				'type'     => 'text',
				'title'    => esc_html__( 'Pinterest', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> '',
			),
			array(
				'id'       => 'social-links-url-reddit',
				'type'     => 'text',
				'title'    => esc_html__( 'Reddit', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> '',
			),
			array(
				'id'       => 'social-links-url-envelope',
				'type'     => 'text',
				'title'    => esc_html__( 'Email', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> '',
			),
			array(
				'id'       => 'social-links-url-external-link',
				'type'     => 'text',
				'title'    => esc_html__( 'Custom Link', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> '',
			),
			array(
				'id'       => 'social-links-urls-ends',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),
		)
	) );



	// -> START Sharing Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Sharing Settings', 'medicale-wp' ),
		'id'     => 'sharing-settings',
		'desc'   => esc_html__( 'Enable/Disable social sharing buttons for posts, pages and portfolio single pages', 'medicale-wp' ),
		'icon'   => 'dashicons-before dashicons-share',
		'fields' => array(
			array(
				'id'       => 'sharing-settings-enable-sharing',
				'type'     => 'switch',
				'title'    => esc_html__( 'Enable Sharing', 'medicale-wp' ),
				'subtitle' => '',
				'default'	=> 1,
				'on'       => esc_html__( 'Yes', 'medicale-wp' ),
				'off'      => esc_html__( 'No', 'medicale-wp' ),
			),

			array(
				'id'       => 'sharing-settings-heading',
				'type'     => 'text',
				'title'    => esc_html__( 'Sharing Heading', 'medicale-wp' ),
				'subtitle' => esc_html__( 'Your custom text for the social sharing heading.', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> esc_html__( 'Share On:', 'medicale-wp' ),
				'required' => array( 'sharing-settings-enable-sharing', '=', '1' ),
			),
			array(
				'id'       => 'sharing-settings-icon-type',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Sharing Icon Type', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> array(
					'text'          => 'Text',
					'icon'          => 'Flat Icon',
					'icon-brand'    => 'Icon with Brand Color',
				),
				'default'	=> 'icon-brand',
				'required' => array( 'sharing-settings-enable-sharing', '=', '1' ),
			),

			//Buttons Type Icon
			array(
				'id'       => 'sharing-settings-social-links-color',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Sharing Icons Color', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'icon-dark'     => esc_html__( 'Dark', 'medicale-wp' ),
					''              => esc_html__( 'Default', 'medicale-wp' ),
					'icon-gray'     => esc_html__( 'Gray', 'medicale-wp' ),
				),
				'default'	=> '',
				'required' => array( 
					array( 'sharing-settings-icon-type', '=', 'icon' ),
				)
			),
			array(
				'id'       => 'sharing-settings-social-links-icon-style',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Sharing Icons Style', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'icon-rounded'   => esc_html__( 'Rounded', 'medicale-wp' ),
					''               => esc_html__( 'Default', 'medicale-wp' ),
					'icon-circled'   => esc_html__( 'Circled', 'medicale-wp' ),
				),
				'default'	=> '',
				'required' => array( 'sharing-settings-icon-type', '!=', 'text' ),
			),
			array(
				'id'       => 'sharing-settings-social-links-icon-size',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Sharing Icons Size', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'icon-sm'   => esc_html__( 'Small', 'medicale-wp' ),
					''          => esc_html__( 'Default', 'medicale-wp' ),
					'icon-md'   => esc_html__( 'Medium', 'medicale-wp' ),
					'icon-lg'   => esc_html__( 'Large', 'medicale-wp' ),
					'icon-xl'   => esc_html__( 'Extra Large', 'medicale-wp' ),
				),
				'default'	=> '',
				'required' => array( 'sharing-settings-icon-type', '!=', 'text' ),
			),
			array(
				'id'       => 'sharing-settings-social-links-icon-animation-effect',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Icons Animation Effect', 'medicale-wp' ),
				'desc'     => '',
				'options'	=> array(
					'styled-icons-effect-rollover'   => esc_html__( 'Roll Over', 'medicale-wp' ),
					''                               => esc_html__( 'Default', 'medicale-wp' ),
					'styled-icons-effect-rotate'     => esc_html__( 'Rotate', 'medicale-wp' ),
				),
				'default'	=> '',
				'required' => array( 'sharing-settings-icon-type', '!=', 'text' ),
			),
			array(
				'id'       => 'sharing-settings-social-links-icon-border-style',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Make Sharing Icon Area Bordered?', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> '0',
				'required' => array( 
					array( 'sharing-settings-social-links-color', '!=', 'brand-color' ),
				)
			),
			array(
				'id'       => 'sharing-settings-social-links-theme-colored',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Make Sharing Icons Theme Colored?', 'medicale-wp' ),
				'subtitle' => esc_html__( 'To make the sharing icons theme colored, please check it.', 'medicale-wp' ),
				'desc'     => '',
				'default'	=> '0',
				'required' => array( 
					array( 'sharing-settings-social-links-color', '!=', 'brand-color' ),
				)
			),





			/*array(
				'id'       => 'sharing-settings-show-social-share-on',
				'type'     => 'checkbox',
				'title'    => esc_html__( 'Show Social Share On', 'medicale-wp' ),
				'subtitle'     => '',
				'desc' => '',
				//Must provide key => value pairs for multi checkbox options
				'options'	=> array(
					'show-on-posts'     => esc_html__( 'Posts', 'medicale-wp' ),
					'show-on-pages'     => esc_html__( 'Pages', 'medicale-wp' ),
					'show-on-portfolio' => esc_html__( 'Portfolio', 'medicale-wp' ),
				),
				//See how std has changed? you also don't need to specify opts that are 0.
				'default'	=> array(
					'show-on-posts' => '1',
					'show-on-pages' => '1',
					'show-on-portfolio' => '1',
				),
				'required' => array( 'sharing-settings-enable-sharing', '=', '1' ),
			),*/
			array(
				'id'       => 'sharing-settings-networks',
				'type'     => 'sorter',
				'title'    => esc_html__( 'Seleted Social Networks', 'medicale-wp' ),
				'desc'     => '',
				'compiler' => 'true',
				'options'	=> array(
					'Enabled' => array(
						'twitter'    => 'Twitter',
						'facebook'   => 'Facebook',
						'googleplus' => 'Google+',
						'linkedin'   => 'Linkedin',
						'tumblr'     => 'Tumblr',
						'pinterest'  => 'Pinterest',
						'email'      => 'Email',
					),
					'Disabled'  => array(
						'vk'        => 'VK',
						'reddit'    => 'Reddit',
						'print'     => 'Print',
					),
				),
				'required' => array( 'sharing-settings-enable-sharing', '=', '1' ),
			),

			//section Social Network URLs Starts
			array(
				'id'       => 'sharing-settings-icon-tooltip-starts',
				'type'     => 'section',
				'title'    => esc_html__( 'Sharing Icon Tooltip', 'medicale-wp' ),
				'subtitle' => '',
				'indent'   => true, // Indent all options below until the next 'section' option is set.
				'required' => array( 'sharing-settings-enable-sharing', '=', '1' ),
			),

			array(
				'id'       => 'sharing-settings-tooltip-directions',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Tooltip Text Directions', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'options'	=> array(
					'top'    => 'Top',
					'right'  => 'Right',
					'bottom' => 'Bottom',
					'left'   => 'Left',
					'none'   => 'None',
				),
				'default'	=> 'top',
			),
			array(
				'id'       => 'sharing-settings-tooltip-twitter',
				'type'     => 'text',
				'title'    => esc_html__( 'Tooltip text for Twitter', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> esc_html__( 'Share on Twitter', 'medicale-wp' ),
			),
			array(
				'id'       => 'sharing-settings-tooltip-facebook',
				'type'     => 'text',
				'title'    => esc_html__( 'Tooltip text for Facebook', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> esc_html__( 'Share on Facebook', 'medicale-wp' ),
			),
			array(
				'id'       => 'sharing-settings-tooltip-googleplus',
				'type'     => 'text',
				'title'    => esc_html__( 'Tooltip text for Google+', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> esc_html__( 'Share on Google+', 'medicale-wp' ),
			),
			array(
				'id'       => 'sharing-settings-tooltip-linkedin',
				'type'     => 'text',
				'title'    => esc_html__( 'Tooltip text for LinkedIn', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> esc_html__( 'Share on LinkedIn', 'medicale-wp' ),
			),
			array(
				'id'       => 'sharing-settings-tooltip-tumblr',
				'type'     => 'text',
				'title'    => esc_html__( 'Tooltip text for Tumblr', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> esc_html__( 'Share on Tumblr', 'medicale-wp' ),
			),
			array(
				'id'       => 'sharing-settings-tooltip-email',
				'type'     => 'text',
				'title'    => esc_html__( 'Tooltip text for Email', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> esc_html__( 'Share on Email', 'medicale-wp' ),
			),
			array(
				'id'       => 'sharing-settings-tooltip-vk',
				'type'     => 'text',
				'title'    => esc_html__( 'Tooltip text for VKontakte', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> esc_html__( 'Share on VKontakte', 'medicale-wp' ),
			),
			array(
				'id'       => 'sharing-settings-tooltip-pinterest',
				'type'     => 'text',
				'title'    => esc_html__( 'Tooltip text for Pinterest', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> esc_html__( 'Share on Pinterest', 'medicale-wp' ),
			),
			array(
				'id'       => 'sharing-settings-tooltip-reddit',
				'type'     => 'text',
				'title'    => esc_html__( 'Tooltip text for Reddit', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> esc_html__( 'Share on Reddit', 'medicale-wp' ),
			),
			array(
				'id'       => 'sharing-settings-tooltip-print',
				'type'     => 'text',
				'title'    => esc_html__( 'Tooltip text for Print', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
				'default'	=> esc_html__( 'Print This Page', 'medicale-wp' ),
			),
			array(
				'id'       => 'sharing-settings-icon-tooltip-ends',
				'type'   => 'section',
				'indent' => false, // Indent all options below until the next 'section' option is set.
			),

		)
	) );



	// -> START Twitter API Settings
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'API Settings', 'medicale-wp' ),
		'id'     => 'theme-api-settings',
		'desc'  => esc_html__( 'Fill the following fields if you want to use these features', 'medicale-wp' ),
		'icon'   => 'dashicons-before dashicons-admin-network',
		'fields' => array(
			array(
				'id'        => 'theme-api-settings-gmaps',
				'type'      => 'info',
				'title'     => esc_html__( 'Google Maps API Settings', 'medicale-wp' ),
				'subtitle'  => esc_html__( 'Fill the following field if you want to use Google Maps', 'medicale-wp' ),
				'notice'    => false,
			),
			array(
				'id'       => 'theme-api-settings-gmaps-api-key',
				'type'     => 'text',
				'title'    => esc_html__( 'Google Maps API Key', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
			),


			array(
				'id'        => 'theme-api-settings-twitter',
				'type'      => 'info',
				'title'     => esc_html__( 'Twitter API Settings', 'medicale-wp' ),
				'subtitle'  => sprintf( esc_html__('Fill the following fields if you want to use Twitter Feed Widget. You can collect those keys by creating your own Twitter API from here %s', 'medicale-wp'), '<a target="_blank" class="text-white" href="' . esc_url( 'https://dev.twitter.com/apps' ) . '">', '</a>' ),
				'notice'    => false,
			),

			array(
				'id'       => 'theme-api-settings-twitter-api-key',
				'type'     => 'text',
				'title'    => esc_html__( 'Twitter API Key', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
			),
			array(
				'id'       => 'theme-api-settings-twitter-api-secret',
				'type'     => 'text',
				'title'    => esc_html__( 'Twitter API Secret', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
			),

			array(
				'id'       => 'theme-api-settings-twitter-api-access-token',
				'type'     => 'text',
				'title'    => esc_html__( 'Twitter Access Token', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
			),
			array(
				'id'       => 'theme-api-settings-twitter-api-access-token-secret',
				'type'     => 'text',
				'title'    => esc_html__( 'Twitter Access Token Secret', 'medicale-wp' ),
				'subtitle' => '',
				'desc'     => '',
			),
		)
	) );



	// -> START Custom HTML/JS Codes
	Redux::setSection( $opt_name, array(
		'title'  => esc_html__( 'Custom HTML/JS Codes', 'medicale-wp' ),
		'id'     => 'custom-codes',
		'desc'   => '',
		'icon'   => 'dashicons-before dashicons-editor-code',
		'fields' => array(
			array(
				'id'       => 'custom-codes-custom-html-script-header',
				'type'     => 'ace_editor',
				'title'    => esc_html__( 'Custom HTML/JS Code - in Header before &lt;/head&gt; tag', 'medicale-wp' ),
				'subtitle' => esc_html__( 'If you have any custom HTML or JS Code you would like to add in the header before &lt;/head&gt; tag of the site then please enter it here. Only accepts javascript code wrapped with &lt;script&gt; tags and valid HTML markup.', 'medicale-wp' ),
				'mode'     => 'javascript',
				'theme'    => 'chrome',
				'desc'     => '',
				'default'     => '',
			),
			array(
				'id'       => 'custom-codes-custom-html-script-footer',
				'type'     => 'ace_editor',
				'title'    => esc_html__( 'Custom HTML/JS Code - in Footer before &lt;/body&gt; tag', 'medicale-wp' ),
				'subtitle' => esc_html__( 'If you have any custom HTML or JS Code you would like to add in the footer before &lt;/body&gt; tag of the site then please enter it here. Only accepts javascript code wrapped with &lt;script&gt; tags and valid HTML markup.', 'medicale-wp' ),
				'mode'     => 'javascript',
				'theme'    => 'chrome',
				'desc'     => '',
				'default'     => '',
			)
		)
	) );
	
	
	/*
	 * <--- END SECTIONS
	 */

