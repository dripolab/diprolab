<?php
/**
 * Template Name: Full Width Page
 * 
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
get_header();
?>

	<?php medicale_mascot_get_title_area_parts(); ?>
	<?php medicale_mascot_get_page( 'container-fluid pt-0 pb-0', 'no-sidebar' ); ?>

<?php
get_footer();
