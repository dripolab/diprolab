<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the .main-content div and #wrapper
 *
 */
?>


	<?php medicale_mascot_get_footer_top_callout(); ?>


	<?php
		/**
		 * medicale_mascot_main_content_end hook.
		 *
		 */
		do_action( 'medicale_mascot_main_content_end' );
	?>
	</div>
	<!-- main-content end --> 
	<?php
		/**
		 * medicale_mascot_after_main_content hook.
		 *
		 */
		do_action( 'medicale_mascot_after_main_content' );
	?>


	<?php if( apply_filters('medicale_mascot_filter_show_footer', true) ): ?>
	<?php medicale_mascot_get_footer_parts(); ?>
	<?php endif; ?>


	<?php wp_footer(); ?>
	
	<?php
		/**
		 * medicale_mascot_wrapper_end hook.
		 *
		 */
		do_action( 'medicale_mascot_wrapper_end' );
	?>
</div>
<!-- wrapper end -->
<?php
	/**
	 * medicale_mascot_body_tag_end hook.
	 *
	 */
	do_action( 'medicale_mascot_body_tag_end' );
?>
</body>
</html>
