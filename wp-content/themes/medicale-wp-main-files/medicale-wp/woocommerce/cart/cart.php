<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>
<form action="/diprolab/contact/#wpcf7-f2378-p111-o1" method="post" class="wpcf7-form" novalidate="novalidate">
   <div style="display: none;">
      <input type="hidden" name="_wpcf7" value="2378">
      <input type="hidden" name="_wpcf7_version" value="5.0.1">
      <input type="hidden" name="_wpcf7_locale" value="en_US">
      <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f2378-p111-o1">
      <input type="hidden" name="_wpcf7_container_post" value="111">
   </div>
   <div class="row">
      <div class="col-sm-6">
         <div class="form-group">
            <label>Nombre <small>*</small></label><br>
            <span class="wpcf7-form-control-wrap your-name"><input id="name" type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false"></span>
         </div>
      </div>
      <div class="col-sm-6">
         <div class="form-group">
            <label>Email <small>*</small></label><br>
            <span class="wpcf7-form-control-wrap your-email"><input id="mail" type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" aria-required="true" aria-invalid="false"></span>
         </div>
      </div>
      <div class="col-sm-6">
         <div class="form-group">
            <label>Empresa o Institucion <small>*</small></label><br>
            <span class="wpcf7-form-control-wrap your-subject"><input id="adress" type="text" name="cellPhone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false"></span>
         </div>
      </div>
      <div class="col-sm-6">
         <div class="form-group">
            <label>Telefono Fijo</label><br>
            <span class="wpcf7-form-control-wrap Number"><input id="phone" type="text" name="staticPhone" value="" size="40" class="wpcf7-form-control wpcf7-text form-control" aria-invalid="false"></span>
         </div>
      </div>
   </div>
</form>
<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
	<?php do_action( 'woocommerce_before_cart_table' ); ?>
	<div class="table-responsive">
		<table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents table-striped" cellspacing="0">
			<thead>
				<tr>
					<th class="product-remove">&nbsp;</th>
					<th class="product-thumbnail">&nbsp;</th>
					<th class="product-name"><?php esc_html_e( 'Product', 'medicale-wp' ); ?>o</th>
					<!--<th class="product-price"><?php esc_html_e( 'Price', 'medicale-wp' ); ?></th>-->
					<th class="product-quantity"><?php //esc_html_e( 'Quantity', 'medicale-wp' ); ?> Cantidad</th>
						<!--<th class="product-subtotal"><?php esc_html_e( 'Total', 'medicale-wp' ); ?></th>-->
				</tr>
			</thead>
			<tbody>
				<?php do_action( 'woocommerce_before_cart_contents' ); ?>

				<?php
				foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
					$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
					$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

					if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
						$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
						?>
						<tr class="productItem woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

							<td class="product-remove">
								<?php
									echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
										'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
										esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
										esc_html__( 'Remove this item', 'medicale-wp' ),
										esc_attr( $product_id ),
										esc_attr( $_product->get_sku() )
									), $cart_item_key );

								?>
							</td>

							<td class="product-thumbnail">
								<?php
									$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

									if ( ! $product_permalink ) {
										echo $thumbnail;
									} else {
										printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail );
									}
								?>
							</td>

							<td class="product-name" data-title="<?php esc_attr_e( 'Product', 'medicale-wp' ); ?>">
								<?php

									if ( ! $product_permalink ) {
										echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;';
									} else {
										echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key );
									}

									// Meta data
									echo WC()->cart->get_item_data( $cart_item );

									// Backorder notification
									if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
										echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'medicale-wp' ) . '</p>';
									}
								?>
							</td>
<!--
							<td class="product-price" data-title="<?php esc_attr_e( 'Price', 'medicale-wp' ); ?>">
								<?php
									echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
								?>
							</td>
-->
							<td class="product-quantity" data-title="<?php esc_attr_e( 'Quantity', 'medicale-wp' ); ?>">
								<?php
									if ( $_product->is_sold_individually() ) {
										$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
									} else {
										$product_quantity = woocommerce_quantity_input( array(
											'input_name'  => "cart[{$cart_item_key}][qty]",
											'input_value' => $cart_item['quantity'],
											'max_value'   => $_product->get_max_purchase_quantity(),
											'min_value'   => '0',
										), $_product, false );
									}

									echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
								?>
							</td>
<!--
							<td class="product-subtotal" data-title="<?php esc_attr_e( 'Total', 'medicale-wp' ); ?>">
								<?php
									echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
								?>
							</td>
						-->   
						</tr>
						<?php
					}
				}
				?>

				<?php do_action( 'woocommerce_cart_contents' ); ?>

				<tr>
					<td colspan="6" class="actions">
<!--
						<?php if ( wc_coupons_enabled() ) { ?>
						<div class="form-inline">
							<div class="coupon pull-left flip form-group">
								<label for="coupon_code"><?php esc_html_e( 'Coupon:', 'medicale-wp' ); ?></label>
								<input type="text" name="coupon_code" class="input-text form-control mr-5" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'medicale-wp' ); ?>" />

								<input type="submit" class="button btn btn-dark btn-theme-colored" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'medicale-wp' ); ?>" />
								<?php do_action( 'woocommerce_cart_coupon' ); ?>
							</div>
						</div>
						<?php } ?>
-->
						<input type="submit" class="button btn btn-dark pull-right flip" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'medicale-wp' ); ?>" />

						<?php do_action( 'woocommerce_cart_actions' ); ?>

						<?php wp_nonce_field( 'woocommerce-cart' ); ?>
					</td>
				</tr>

				<?php do_action( 'woocommerce_after_cart_contents' ); ?>
			</tbody>
		</table>
	</div>
	<?php do_action( 'woocommerce_after_cart_table' ); ?>
</form>

<div class="cart-collaterals">
	<div class="row">
		<div class="col-sm-12">
			 <div class="form-group">
						<input id="send" type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit btn btn-flat btn-lg btn-dark btn-theme-colored" name="on"><span class="ajax-loader"></span>

			</div>
		</div>
	<?php
		/**
		 * woocommerce_cart_collaterals hook.
		 *
		 * @hooked woocommerce_cross_sell_display
		 * @hooked woocommerce_cart_totals - 10
		 */
	 	//do_action( 'woocommerce_cart_collaterals' );
	?>
	</div>
	<div class="messageMail">
		<p>Información Eviada</p>
		<span>Pronto estaremos en comunicación con usted</span>
		<p>¡Gracias por Preferirnos!</p>
	</div>
</div>

<?php do_action( 'woocommerce_after_cart' ); ?>
<style media="screen">
	.messageMail{
		background: rgba(255, 253, 253, 0.88);
		border: 1px solid #39f94c;
		padding: 30px;
		display: none;
		opacity: 0;
		position: absolute;
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
	}
		.messageMail p{
			display: block;
			font-size: 35px;
			color: #000;
			text-align: center;
			font-weight: bolder;
			border-bottom: 1px solid #efefef;
			margin: 15vh 20vw 10px 20vw;
		}
		.messageMail span{
			display: block;
			font-size: 18px;
			color: #000;
			text-align: center;
		}

</style>
<script type="text/javascript">
	$(document).ready(function(){
		var name;
		var data = {};
		var bandera = false;
			$('#send').on("click", function(){

					if ($('#name').val() == '' || $('#mail').val() == '' || $('#cellphone').val() == '' || $('#phone').val() == '') {
						$('span input').each(function(){
							if ($(this).val() == '' ) {
								$(this).css( "border", "1px solid #f00" );
							}else{
								$(this).css( "border", "none" );
							}
						});
					}else{
						var name ={'name' : $('#name').val(),
							'mail': $('#mail').val(),
							'adress' : $('#adress').val(),
							'phone':$('#phone').val(),
						};
						$('table tr.productItem').each(function(i){
								data['product'+(i+1)+''] = {'image' : $('.product-thumbnail a img', this).attr("src"), 'product_name' : $('.product-name a',  this).text(),	'product_quantity' : $('.product-quantity div input[type="number"]', this).val()};
								Object.assign(name,{"total": i + 1});
								console.log($('.product-remove a').attr("class"));
						});
							$('span input').css( "border", "none" );
							name['products'] = data;


							$.ajax({
								type: 'POST',
								url: '/cartMailData.php',
								data: {name:name},
								success: function(res){
									console.log('send Infomation', res);
									bandera = true;
								}
							});

							$('.messageMail').show().animate({ opacity: 1}, 1000);

							setInterval(function() {
								if ($('table tr.productItem').size() == 0) {
										console.log(bandera, "en if");
											setTimeout(function(){
												var url = window.location.origin;
											  window.location.href = url;
												clearInterval();
											}, 3000);
											$('.woocommerce .cart-empty').text("Información Enviada con éxito");
											$('.woocommerce .return-to-shop .button.wc-backward.btn.btn-dark.btn-theme-colored').text("Regresa a Nuestros Productos");
											$('.woocommerce-message.alert.alert-info.alert-dismissible').hide();
								}else if (bandera == true) {
									console.log(bandera);
									$('table tr.productItem .product-remove a').click();
								}

							}, 3000);
						}


			});


	});

</script>
