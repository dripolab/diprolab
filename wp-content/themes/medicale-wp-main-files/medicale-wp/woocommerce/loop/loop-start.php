<?php
/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

/* Note: This file has been modified by ThemeMascot */

?>

<?php 
	$shop_layout_mode = medicale_mascot_get_redux_option( 'shop-layout-settings-select-shop-layout-mode' );
	$products_per_row = medicale_mascot_get_redux_option( 'shop-archive-settings-products-per-row' );
	$holder_id = medicale_mascot_get_shop_isotope_holder_ID();

	$products_per_row = apply_filters( 'medicale_mascot_shop_gallery_isotope_products_per_row_filter', $products_per_row );
?>
<div class="clearfix"></div>
<!-- Products Masonry -->
<div id="<?php echo $holder_id ?>" class="gallery-isotope shop-archive <?php echo $shop_layout_mode; ?> grid-<?php echo $products_per_row; ?> products">
	<?php if( $shop_layout_mode == 'masonry' ) { ?>
	<div class="gallery-item gallery-item-sizer"></div>
	<?php } ?>