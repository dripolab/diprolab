<?php
/**
 * The template for displaying archive pages
 */

get_header(); ?>

<?php medicale_mascot_get_title_area_parts(); ?>
<?php medicale_mascot_get_blog(); ?>

<?php get_footer();
