-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 10, 2017 at 01:57 PM
-- Server version: 5.5.51-38.2
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kodesolu_wrdp4`
--

-- --------------------------------------------------------

--
-- Table structure for table `wpfp_event_hours`
--

CREATE TABLE IF NOT EXISTS `wpfp_event_hours` (
  `event_hours_id` bigint(20) unsigned NOT NULL,
  `event_id` bigint(20) NOT NULL,
  `weekday_id` bigint(20) NOT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL,
  `tooltip` text NOT NULL,
  `before_hour_text` text NOT NULL,
  `after_hour_text` text NOT NULL,
  `category` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wpfp_event_hours`
--

INSERT INTO `wpfp_event_hours` (`event_hours_id`, `event_id`, `weekday_id`, `start`, `end`, `tooltip`, `before_hour_text`, `after_hour_text`, `category`) VALUES
(4, 2808, 2132, '06:00:00', '07:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Morfie Alex\r\n', 'Room: 01', ''),
(15, 2806, 2132, '07:00:00', '10:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Morfie Alex ', 'Room: 05', ''),
(8, 2808, 1215, '07:00:00', '12:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Jerremy Martin', 'Room: 02', ''),
(35, 2797, 1215, '17:00:00', '20:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Fredrikh Jeson', 'Room: 04', ''),
(14, 2808, 1217, '13:00:00', '16:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Morfie Alex ', 'Room: 01', ''),
(16, 2806, 1213, '10:00:00', '12:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Jaka Alex', 'Room: 06', ''),
(19, 2806, 1216, '11:00:00', '14:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Tony Martin', 'Room: 08', ''),
(18, 2806, 1214, '06:00:00', '09:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Jerin Maria', 'Room: 06', ''),
(20, 2803, 2132, '12:00:00', '14:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Robert Williams', 'Room: 09', ''),
(21, 2803, 1213, '06:00:00', '08:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Sakib Doe', 'Room: 10', ''),
(22, 2803, 1214, '09:00:00', '12:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Jerremy Martin', 'Room: 01', ''),
(23, 2803, 1216, '06:00:00', '08:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Robert Williams', 'Room: 05', ''),
(24, 2803, 1215, '14:00:00', '16:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Tony Martin', 'Room: 01', ''),
(25, 2797, 2132, '10:00:00', '12:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Morfie Alex ', 'Room: 02', ''),
(26, 2797, 1213, '08:00:00', '10:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Jerin Maria', 'Room: 04', ''),
(27, 2797, 1214, '12:00:00', '15:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Jessica Alba', 'Room: 02', ''),
(28, 2797, 1216, '08:00:00', '11:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Sakib Doe', 'Room: 03', ''),
(29, 2790, 1213, '12:00:00', '16:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Tony Martin', 'Room: 05', ''),
(30, 2790, 1217, '06:00:00', '09:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Morfie Alex ', 'Room: 02', ''),
(31, 2790, 2132, '14:00:00', '17:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Devid Warner', 'Room: 06', ''),
(32, 2790, 1215, '06:00:00', '07:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Tojo Alax', 'Room: 03', ''),
(33, 2790, 1216, '15:00:00', '17:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Mark Doe', 'Room: 02', ''),
(34, 2808, 1213, '16:00:00', '19:00:00', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Dr. Jessica Alba', 'Room: 03', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wpfp_event_hours`
--
ALTER TABLE `wpfp_event_hours`
  ADD PRIMARY KEY (`event_hours_id`), ADD KEY `event_id` (`event_id`), ADD KEY `weekday_id` (`weekday_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wpfp_event_hours`
--
ALTER TABLE `wpfp_event_hours`
  MODIFY `event_hours_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
