<?php

//load lib
require_once $plugin_dir_path . 'lib/plugin-core-functions.php';
require_once $plugin_dir_path . 'lib/plugin-core-utility-variables-functions.php';


/* Sidebar Generator
================================================== */
if ( ! class_exists( 'Mascot_Core_Sidebar_Generator' ) ) {
require_once $plugin_dir_path . 'external-plugins/sidebar-generator.php';
}

/* Less_Parser
================================================== */
if ( ! class_exists( 'Less_Parser' ) ) {
require_once $plugin_dir_path . 'external-plugins/lessphp/less-plugin.php';
}

/* WordPress Post Like System
================================================== */
if (!function_exists('mascot_core_sl_get_simple_likes_button')) {
require_once $plugin_dir_path . 'external-plugins/wp-post-like-system/post-like.php';
}


/* REDUX OPTIONS FRAMEWORK
================================================== */
if ( ! class_exists( 'ReduxFramework' ) && file_exists( $plugin_dir_path . 'external-plugins/redux-framework/ReduxCore/framework.php' ) ) {
require_once $plugin_dir_path . 'external-plugins/redux-framework/ReduxCore/framework.php';
require_once $plugin_dir_path . 'external-plugins/redux-framework/init-redux-args.php';
}


/* META BOX FRAMEWORK
================================================== */
if ( ! defined( 'RWMB_VER' ) ) {
require_once( $plugin_dir_path . 'external-plugins/meta-box/loader-metabox.php' );
}