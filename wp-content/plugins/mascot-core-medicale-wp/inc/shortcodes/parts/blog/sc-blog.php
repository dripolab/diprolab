<?php
namespace MASCOTCORE\Shortcodes\Blog;

use MASCOTCORE\Shortcodes\Lib;

/**
 * class SC_Blog
 * @package MASCOTCORE\Shortcodes\Blog;
 */
class SC_Blog implements Lib\Mascot_Core_Interface_Shortcodes {
	
	/**
	 * @var shortcode_base
	 */
	private $shortcode_base;

	/**
	 * construct
	 */
	public function __construct() {
		$this->shortcode_base = 'tm_blog';
		add_action('vc_before_init', array($this, 'MapShortCodeVC'));
	}

	/**
	 * Returns shortcode_base for shortcode
	 */
	public function getShortCodeBase() {
		return $this->shortcode_base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 */
	public function MapShortCodeVC() {
		$group_content_options = 'Content Options';
		$group_carousel_options = 'Carousel Options';
		$group_query_options = 'Query Options';
		$group_button_options = 'Button Options';

		$categories_array = mascot_core_category_list_array_for_vc( 'category' );
		$orderby_parameters_list1 = mascot_core_orderby_parameters_list();
		$orderby_parameters_list2 = array(
		);
		$orderby_parameters_list = array_merge( $orderby_parameters_list2, $orderby_parameters_list1 );


		if(function_exists('vc_map')) {
			$vc_map = array(
				'name'		=> esc_html__( 'Blog', 'mascot-core' ),
				'base'		=>$this->shortcode_base,
				'category'	=> 'by TM',
				'icon'		=> 'mascot-vc-icons vc-icon-cta',
				'allowed_container_element' => 'vc_row',
				'params'	=>array(
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Custom CSS class", 'mascot-core' ),
						"param_name"	=> "custom_css_class",
						"description"	=> esc_html__( 'To style particular content element.', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Display Type", 'mascot-core' ),
						"param_name"	=> "display_type",
						"description"	=> "",
						'value'			=> array(
							'Grid'	  => 'grid',
							'Masonry'   => 'masonry',
							'Carousel'  => 'carousel'
						),
						'save_always'   => true,
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Columns Layout", 'mascot-core' ),
						"param_name"	=> "columns",
						"description"	=> esc_html__( 'Define Columns Layout for Grid/Carousel.', 'mascot-core' ),
						'value'			=> array(
							'1'  =>  '1',
							'2'  =>  '2',
							'3'  =>  '3',
							'4'  =>  '4',
						),
						'std'			=> 4,
						'admin_label'   => true,
					),



					//Carousel Options
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Navigation Arrow", 'mascot-core' ),
						"param_name"	=> "show_navigation",
						"description"	=> esc_html__( 'Show Left Right Navigation Arrow for Testimonial Carousel', 'mascot-core' ),
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'dependency'	=> array('element' => 'display_type', 'value' => 'carousel'),
						'group'			=> $group_carousel_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Bullets", 'mascot-core' ),
						"param_name"	=> "show_bullets",
						"description"	=> esc_html__( 'Show Bottom Bullets for Testimonial Carousel', 'mascot-core' ),
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'dependency'	=> array('element' => 'display_type', 'value' => 'carousel'),
						'group'			=> $group_carousel_options
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Animation Speed", 'mascot-core' ),
						"param_name"	=> "animation_speed",
						"description"	=> esc_html__( 'Speed of slide animation in milliseconds. Default value is 4000', 'mascot-core' ),
						'admin_label'   => true,
						'dependency'	=> array('element' => 'display_type', 'value' => 'carousel'),
						'group'			=> $group_carousel_options
					),


					//Query Options
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Number of Items to Query from Database", 'mascot-core' ),
						"param_name"	=> "total_items",
						"description"	=> esc_html__( 'How many posts do you wish to show? Default 4', 'mascot-core' ),
						'admin_label'   => true,
						'group'			=> $group_query_options
					),
					array(
						'type'			=> 'checkbox',
						"heading"		=> esc_html__( "Category", 'mascot-core' ),
						"param_name"	=> "category",
						"description"	=> esc_html__( 'Choose a category to pull from.', 'mascot-core' ),
						'value'			=> $categories_array,
						'admin_label'   => true,
						'group'			=> $group_query_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Order By", 'mascot-core' ),
						"param_name"	=> "order_by",
						"description"	=> esc_html__( 'Select how to sort retrieved posts.', 'mascot-core' ),
						'value'			=> $orderby_parameters_list,
						'std'			=> $orderby_parameters_list1[ 'Date' ],
						'admin_label'   => true,
						'group'			=> $group_query_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Order", 'mascot-core' ),
						"param_name"	=> "order",
						"description"	=> esc_html__( 'Descending or Ascending order.', 'mascot-core' ),
						'value'			=> array(
							'Descending'	=> 'DESC',
							'Ascending'	 => 'ASC',
						),
						'admin_label'   => true,
						'group'			=> $group_query_options
					),



					//Content Options
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Title", 'mascot-core' ),
						"param_name"	=> "show_title",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Description", 'mascot-core' ),
						"param_name"	=> "show_description",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Featured Image", 'mascot-core' ),
						"param_name"	=> "show_featured_image",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'checkbox',
						"heading"		=> esc_html__( "Show Post Meta", 'mascot-core' ),
						"param_name"	=> "show_post_meta",
						'description'   => '',
						'std'			=> 'true',
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'checkbox',
						"heading"		=> esc_html__( "Show Read More Button?", 'mascot-core' ),
						"param_name"	=> "show_read_more_button",
						'description'   => '',
						'std'			=> 'true',
						'group'			=> $group_content_options
					),



					//Button Options
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Design Style", 'mascot-core' ),
						"param_name"	=> "design_style",
						"description"	=> "",
						'value'			=> array(
							'Button with Icon' => 'btn-read-more',
							'Button Default' => 'btn-default',
							'Button Border'  => 'btn-border',
							'Button Dark'	=> 'btn-dark',
							'Button Gray'	=> 'btn-gray'
						),
						'save_always'   => true,
						'admin_label'   => true,
						'dependency'	=> array('element' => 'show_read_more_button', 'value' => 'true'),
						'group'			=> $group_button_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Button Size", 'mascot-core' ),
						"param_name"	=> "button_size",
						"description"	=> "",
						'value'			=> array(
							'Default'		=> '',
							'Extra Small'   => 'btn-xs',
							'Small'		 => 'btn-sm',
							'Large'		 => 'btn-lg',
							'Extra Large'   => 'btn-xl',
						),
						'save_always'   => true,
						'admin_label'   => true,
						'dependency'	=> array('element' => 'show_read_more_button', 'value' => 'true'),
						'group'			=> $group_button_options
					),
					array(
						'type'			=> 'checkbox',
						"heading"		=> esc_html__( "Make Button Theme Colored?", 'mascot-core' ),
						"param_name"	=> "btn_theme_colored",
						'description'   => '',
						'dependency'	=> array('element' => 'show_read_more_button', 'value' => 'true'),
						'group'			=> $group_button_options
					),
					array(
						'type'			=> 'checkbox',
						"heading"		=> esc_html__( "Make Fullwidth (Block Level Button)", 'mascot-core' ),
						"param_name"	=> "btn_block",
						'description'   => '',
						'dependency'	=> array('element' => 'show_read_more_button', 'value' => 'true'),
						'group'			=> $group_button_options
					),
					array(
						'type'			=> 'checkbox',
						"heading"		=> esc_html__( "3D Effect", 'mascot-core' ),
						"param_name"	=> "threed_effect",
						'description'   => '',
						'dependency'	=> array('element' => 'show_read_more_button', 'value' => 'true'),
						'group'			=> $group_button_options
					),

				)
			);
			vc_map( $vc_map );

			//Modify vc_map from themes
			if( function_exists('mascot_core_sc_blog_vc_map_modifier') ) {
				mascot_core_sc_blog_vc_map_modifier( $this->shortcode_base );
			}
		}
	}

	/**
	 * Renders shortcodes HTML
	 */
	public function render( $attr, $content = null ) {

		//Render Shortcode from themes
		if( function_exists('mascot_core_sc_blog_render') ) {
			return mascot_core_sc_blog_render( $attr, $content );
		}
		
	}
}