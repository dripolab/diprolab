<?php
namespace MASCOTCORE\Shortcodes\IconBox;

use MASCOTCORE\Shortcodes\Lib;

/**
 * class SC_Icon_Box
 * @package MASCOTCORE\Shortcodes\IconBox;
 */
class SC_Icon_Box implements Lib\Mascot_Core_Interface_Shortcodes {
	
	/**
	 * @var shortcode_base
	 */
	private $shortcode_base;

	/**
	 * construct
	 */
	public function __construct() {
		$this->shortcode_base = 'tm_icon_box';
		add_action('vc_before_init', array($this, 'MapShortCodeVC'));
	}

	/**
	 * Returns shortcode_base for shortcode
	 */
	public function getShortCodeBase() {
		return $this->shortcode_base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 */
	public function MapShortCodeVC() {
		if(function_exists('vc_map')) {
			$group_design_options = 'Design Options';

			//collect vc icon pack
			$vc_icon_pack_array = array();
			if( function_exists('medicale_mascot_get_vc_icon_pack_array') ) {
				$vc_icon_pack_array = medicale_mascot_get_vc_icon_pack_array();
			}

			$params_part1 = array(
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Custom CSS Class", 'mascot-core' ),
					"param_name"	=> "custom_css_class",
					"description"	=> esc_html__( 'To style particular content element.', 'mascot-core' ),
					'admin_label'   => true,
				),
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Title", 'mascot-core' ),
					"param_name"	=> "title",
					"description"	=> esc_html__( 'Default: Sample Title.', 'mascot-core' ),
					'admin_label'   => true,
				),
			);

			$merged_fields = array_merge($params_part1, $vc_icon_pack_array);


			$params_part2 = array(
				array(
					'type'			=> 'checkbox',
					"heading"		=> esc_html__( "Link Icon/Title?", 'mascot-core' ),
					"param_name"	=> "link_icon_title",
					'description'   => '',
				),
				array(
					'type'			=> 'vc_link',
					'heading'		=> esc_html__( 'URL (Link)', 'mascot-core' ),
					'param_name'	=> 'link_url',
					'description'   => esc_html__( 'Add link to button.', 'mascot-core' ),
					'dependency'	=> array('element' => 'link_icon_title', 'value' => 'true'),
				),
				array(
					'type'			=> 'textarea_html',
					"heading"		=> esc_html__( "Box Content", 'mascot-core' ),
					"param_name"	=> "content",
					'description'   => '',
					'value'			=> "Default text for Box Content. Lorem ipsum dolor sit amet, consectetur adipisicing elit.",
					'save_always'   => true,
				),


				//Design Options
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Text Alignment", 'mascot-core' ),
					"param_name"	=> "text_alignment",
					"description"	=> "",
					'value'			=> mascot_core_text_alignment_list(),
					'save_always'   => true,
					'admin_label'   => true,
					'group'			=> $group_design_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Title Tag", 'mascot-core' ),
					"param_name"	=> "title_tag",
					"description"	=> "",
					'value'			=> mascot_core_heading_tag_list(),
					'admin_label'   => true,
					'group'			=> $group_design_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Icon Position", 'mascot-core' ),
					"param_name"	=> "icon_position",
					"description"	=> "",
					'value'			=> array(
						'top'	 => 'icon-top',
						'Left'	=> 'icon-left'
					),
					'group'			=> $group_design_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Icon Size", 'mascot-core' ),
					"param_name"	=> "icon_size",
					"description"	=> "",
					'value'			=> array(
						'Default'		=> '',
						'Extra Small'   => 'icon-xs',
						'Small'		 => 'icon-sm',
						'Large'		 => 'icon-lg',
						'Extra Large'   => 'icon-xl',
					),
					'group'			=> $group_design_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Icon Color", 'mascot-core' ),
					"param_name"	=> "icon_color",
					"description"	=> "",
					'value'			=> array(
						'Default'   => '',
						'Gray'	  => 'icon-gray',
						'Dark'	  => 'icon-dark'
					),
					'group'			=> $group_design_options,
					'save_always'   => true,
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Icon Style", 'mascot-core' ),
					"param_name"	=> "icon_style",
					"description"	=> "",
					'value'			=> array(
						'Default'   => '',
						'Rounded'   => 'icon-rounded',
						'Circled'   => 'icon-circled',
					),
					'group'			=> $group_design_options
				),
				array(
					'type'			=> 'checkbox',
					"heading"		=> esc_html__( "Make Icon Area Bordered?", 'mascot-core' ),
					"param_name"	=> "icon_border_style",
					'description'   => '',
					'group'			=> $group_design_options
				),
				array(
					'type'			=> 'checkbox',
					"heading"		=> esc_html__( "Make Icon Theme Colored?", 'mascot-core' ),
					"param_name"	=> "icon_theme_colored",
					'description'   => '',
					'group'			=> $group_design_options
				),
				array(
					'type'			=> 'checkbox',
					"heading"		=> esc_html__( "Show Read More Button?", 'mascot-core' ),
					"param_name"	=> "show_read_more_button",
					'description'   => '',
					'group'			=> $group_design_options
				),
			);

			$merged_fields = array_merge($merged_fields, $params_part2);

			$vc_map = array(
				'name'		=> esc_html__( 'Icon Box', 'mascot-core' ),
				'base'		=>$this->shortcode_base,
				'category'	=> 'by TM',
				'icon'		=> 'mascot-vc-icons vc-icon-cta',
				'allowed_container_element' => 'vc_row',
				'params'	=> $merged_fields
			);
			vc_map( $vc_map );

			//Modify vc_map from themes
			if( function_exists('mascot_core_sc_icon_box_vc_map_modifier') ) {
				mascot_core_sc_icon_box_vc_map_modifier( $this->shortcode_base );
			}
		}
	}

	/**
	 * Renders shortcodes HTML
	 */
	public function render( $attr, $content = null ) {

		//Render Shortcode from themes
		if( function_exists('mascot_core_sc_icon_box_render') ) {
			return mascot_core_sc_icon_box_render( $attr, $content );
		}
		
	}
}