<?php
namespace MASCOTCORE\Shortcodes\ProgressBar;

use MASCOTCORE\Shortcodes\Lib;

/**
 * class SC_Progress_Bar
 * @package MASCOTCORE\Shortcodes\ProgressBar;
 */
class SC_Progress_Bar implements Lib\Mascot_Core_Interface_Shortcodes {
	
	/**
	 * @var shortcode_base
	 */
	private $shortcode_base;

	/**
	 * construct
	 */
	public function __construct() {
		$this->shortcode_base = 'tm_progress_bar';
		add_action('vc_before_init', array($this, 'MapShortCodeVC'));
	}

	/**
	 * Returns shortcode_base for shortcode
	 */
	public function getShortCodeBase() {
		return $this->shortcode_base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 */
	public function MapShortCodeVC() {
		if(function_exists('vc_map')) {
			$group_content_options = 'Content Options';

			$vc_map = array(
				'name'		=> esc_html__( 'Progress Bar', 'mascot-core' ),
				'base'		=>$this->shortcode_base,
				'category'	=> 'by TM',
				'icon'		=> 'mascot-vc-icons vc-icon-cta',
				'allowed_container_element' => 'vc_row',
				'params'	=>array(
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Custom CSS class", 'mascot-core' ),
						"param_name"	=> "custom_css_class",
						"description"	=> esc_html__( 'To style particular content element.', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Design Style", 'mascot-core' ),
						"param_name"	=> "design_style",
						"description"	=> "",
						'value'			=> array(
							'Style 1'   => 'style1',
							'Style 2'   => 'style2'
						),
						'save_always'   => true,
						'admin_label'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Title", 'mascot-core' ),
						"param_name"	=> "title",
						"description"	=> esc_html__( 'Add your Progress/Skill Title Text. Default: Wordpress', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Percentage Value", 'mascot-core' ),
						"param_name"	=> "percentage_value",
						"description"	=> esc_html__( 'Add a Percentage Value. Maximum 100. Default: 85', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Unit Symbol", 'mascot-core' ),
						"param_name"	=> "unit_symbol",
						"description"	=> esc_html__( 'Add an Unit Symbol. Default: %', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Title Tag", 'mascot-core' ),
						"param_name"	=> "title_tag",
						"description"	=> "",
						'value'			=> mascot_core_heading_tag_list(),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'colorpicker',
						"heading"		=> esc_html__( "Progress Color", 'mascot-core' ),
						"param_name"	=> "progress_color",
						"value"		 => '',
						"description"	=> esc_html__( 'Pick a color for progress bar. Leave empty for default value.', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'colorpicker',
						"heading"		=> esc_html__( "Background Color", 'mascot-core' ),
						"param_name"	=> "background_color",
						"value"		 => '',
						"description"	=> esc_html__( 'Pick a color for the background of progress bar. Leave empty for default value.', 'mascot-core' ),
						'admin_label'   => true,
					),

				)
			);
			vc_map( $vc_map );

			//Modify vc_map from themes
			if( function_exists('mascot_core_sc_progress_bar_vc_map_modifier') ) {
				mascot_core_sc_progress_bar_vc_map_modifier( $this->shortcode_base );
			}
		}
	}

	/**
	 * Renders shortcodes HTML
	 */
	public function render( $attr, $content = null ) {

		//Render Shortcode from themes
		if( function_exists('mascot_core_sc_progress_bar_render') ) {
			return mascot_core_sc_progress_bar_render( $attr, $content );
		}
		
	}
}