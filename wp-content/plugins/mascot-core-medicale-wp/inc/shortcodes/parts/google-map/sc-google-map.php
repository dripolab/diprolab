<?php
namespace MASCOTCORE\Shortcodes\GoogleMap;

use MASCOTCORE\Shortcodes\Lib;

/**
 * class SC_Google_Map
 * @package MASCOTCORE\Shortcodes\GoogleMap;
 */
class SC_Google_Map implements Lib\Mascot_Core_Interface_Shortcodes {
	
	/**
	 * @var shortcode_base
	 */
	private $shortcode_base;

	/**
	 * construct
	 */
	public function __construct() {
		$this->shortcode_base = 'tm_google_map';
		add_action('vc_before_init', array($this, 'MapShortCodeVC'));
	}

	/**
	 * Returns shortcode_base for shortcode
	 */
	public function getShortCodeBase() {
		return $this->shortcode_base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 */
	public function MapShortCodeVC() {
		if(function_exists('vc_map')) {
			$group_content_options = 'Content Options';

			$vc_map = array(
				'name'		=> esc_html__( 'Google Map', 'mascot-core' ),
				'base'		=>$this->shortcode_base,
				'category'	=> 'by TM',
				'icon'		=> 'mascot-vc-icons vc-icon-cta',
				'allowed_container_element' => 'vc_row',
				'params'	=>array(
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Custom CSS class", 'mascot-core' ),
						"param_name"	=> "custom_css_class",
						"description"	=> esc_html__( 'To style particular content element.', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Google Map Style", 'mascot-core' ),
						"param_name"	=> "google_map_style",
						"description"	=> "",
						'value'			=> array(
							esc_html__( 'Default', 'mascot-core' )		=>   'default',
							esc_html__( 'Style 1', 'mascot-core' )		=>   'style1',
							esc_html__( 'Style 2', 'mascot-core' )		=>   'style2',
							esc_html__( 'Style 3', 'mascot-core' )		=>   'style3',
							esc_html__( 'Style 4', 'mascot-core' )		=>   'style4',
							esc_html__( 'Style 5', 'mascot-core' )		=>   'style5',
							esc_html__( 'Style 6', 'mascot-core' )		=>   'style6',
							esc_html__( 'Style 7', 'mascot-core' )		=>   'style7',
							esc_html__( 'Style 8', 'mascot-core' )		=>   'style8',
							esc_html__( 'Style 9', 'mascot-core' )		=>   'style9',
							esc_html__( 'Dark', 'mascot-core' )		  =>   'dark',
							esc_html__( 'Greyscale 1', 'mascot-core' )   =>   'greyscale1',
							esc_html__( 'Greyscale 2', 'mascot-core' )   =>   'greyscale2'
						),
						'std'   => 'greyscale1',
						'save_always'   => true,
						'admin_label'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Latitude:", 'mascot-core' ),
						"param_name"	=> "lat",
						"description"	=> sprintf( esc_html__( 'Collect Your Latitude From %1$sHere%2$s', 'mascot-core' ), '<a target="_blank" href="' . esc_url( 'http://www.latlong.net' ) . '">', '</a>' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Longitude:", 'mascot-core' ),
						"param_name"	=> "long",
						"description"	=> sprintf( esc_html__( 'Collect Your Longitude From %1$sHere%2$s', 'mascot-core' ), '<a target="_blank" href="' . esc_url( 'http://www.latlong.net' ) . '">', '</a>' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Zoom Level:", 'mascot-core' ),
						"param_name"	=> "zoom",
						"description"	=> esc_html__( 'Zoom levels between 0 (the lowest zoom level, in which the entire world can be seen on one map) and 21+ (down to streets and individual buildings', 'mascot-core' ),
						'std'			=> esc_html__( '10', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Height of the Map Canvas:", 'mascot-core' ),
						"param_name"	=> "height",
						'description'   => esc_html__( 'Default: 300', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'attach_image',
						"heading"		=> esc_html__( "Custom Pin Marker URL:", 'mascot-core' ),
						"param_name"	=> "marker_url",
						"description"	=> "Default Pin Marker will be used if no custom Pin Marker is selected.",
						'admin_label'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Title Text for Marker Popup:", 'mascot-core' ),
						"param_name"	=> "marker_text_title",
						'description'   => esc_html__( 'Default: Envato', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Short Description for Marker Popup:", 'mascot-core' ),
						"param_name"	=> "marker_text_desc",
						'description'   => esc_html__( 'Default: The world\'s leading marketplace and community for creative assets and creative people.', 'mascot-core' ),
						'admin_label'   => true,
					),

				)
			);
			vc_map( $vc_map );

			//Modify vc_map from themes
			if( function_exists('mascot_core_sc_google_map_vc_map_modifier') ) {
				mascot_core_sc_google_map_vc_map_modifier( $this->shortcode_base );
			}
		}
	}

	/**
	 * Renders shortcodes HTML
	 */
	public function render( $attr, $content = null ) {

		//Render Shortcode from themes
		if( function_exists('mascot_core_sc_google_map_render') ) {
			return mascot_core_sc_google_map_render( $attr, $content );
		}
		
	}
}