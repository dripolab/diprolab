<?php
namespace MASCOTCORE\Shortcodes\SocialList;

use MASCOTCORE\Shortcodes\Lib;

/**
 * class SC_Social_List
 * @package MASCOTCORE\Shortcodes\SocialList;
 */
class SC_Social_List implements Lib\Mascot_Core_Interface_Shortcodes {
	
	/**
	 * @var shortcode_base
	 */
	private $shortcode_base;

	/**
	 * construct
	 */
	public function __construct() {
		$this->shortcode_base = 'tm_social_list';
		add_action('vc_before_init', array($this, 'MapShortCodeVC'));
	}

	/**
	 * Returns shortcode_base for shortcode
	 */
	public function getShortCodeBase() {
		return $this->shortcode_base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 */
	public function MapShortCodeVC() {
		if(function_exists('vc_map')) {
			$group_content_options = 'Content Options';

			$vc_map = array(
				'name'		=> esc_html__( 'Social List', 'mascot-core' ),
				'base'		=>$this->shortcode_base,
				'category'	=> 'by TM',
				'icon'		=> 'mascot-vc-icons vc-icon-cta',
				'allowed_container_element' => 'vc_row',
				'params'	=>array(
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Custom CSS class", 'mascot-core' ),
						"param_name"	=> "custom_css_class",
						"description"	=> esc_html__( 'To style particular content element.', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Icon Size", 'mascot-core' ),
						"param_name"	=> "icon_size",
						'desc'		  => '',
						'value'			=> array(
							esc_html__( 'Small', 'mascot-core' )		=>   'icon-sm',
							esc_html__( 'Default', 'mascot-core' )	 =>   'icon-md',
							esc_html__( 'Extra Small', 'mascot-core' ) =>   'icon-xs',
							esc_html__( 'Large', 'mascot-core' )		=>   'icon-lg',
							esc_html__( 'Extra Large', 'mascot-core' ) =>   'icon-xl',
						),
						'save_always'   => true,
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Icon Border Style", 'mascot-core' ),
						"param_name"	=> "border_style",
						'desc'		  => '',
						'value'			=> array(
							esc_html__( 'Rounded', 'mascot-core' )	 =>   'icon-rounded',
							esc_html__( 'Default', 'mascot-core' )	 =>   'icon-default',
							esc_html__( 'Bordered', 'mascot-core' )	=>   'icon-bordered',
							esc_html__( 'Circled', 'mascot-core' )	 =>   'icon-circled',
						),
						'save_always'   => true,
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Icon Color", 'mascot-core' ),
						"param_name"	=> "icon_color",
						'desc'		  => '',
						'value'			=> array(
							esc_html__( 'Dark', 'mascot-core' )		=>   'icon-dark',
							esc_html__( 'Default', 'mascot-core' )	 =>   'icon-default',
							esc_html__( 'Gray', 'mascot-core' )		=>   'icon-gray',
						),
						'save_always'   => true,
						'admin_label'   => true,
					),
					array(
						'type'			=> 'checkbox',
						"heading"		=> esc_html__( "Make Icon Theme Colored?", 'mascot-core' ),
						"param_name"	=> "icon_theme_colored",
						'desc'		  => '',
						'admin_label'   => true,
						'std'			=> true,
					),

				)
			);
			vc_map( $vc_map );

			//Modify vc_map from themes
			if( function_exists('mascot_core_sc_social_list_vc_map_modifier') ) {
				mascot_core_sc_social_list_vc_map_modifier( $this->shortcode_base );
			}
		}
	}

	/**
	 * Renders shortcodes HTML
	 */
	public function render( $attr, $content = null ) {

		//Render Shortcode from themes
		if( function_exists('mascot_core_sc_social_list_render') ) {
			return mascot_core_sc_social_list_render( $attr, $content );
		}
		
	}
}