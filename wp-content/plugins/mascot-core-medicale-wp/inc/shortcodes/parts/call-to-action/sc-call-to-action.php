<?php
namespace MASCOTCORE\Shortcodes\CallToAction;

use MASCOTCORE\Shortcodes\Lib;

/**
 * class SC_Call_To_Action
 * @package MASCOTCORE\Shortcodes\CallToAction;
 */
class SC_Call_To_Action implements Lib\Mascot_Core_Interface_Shortcodes {
	
	/**
	 * @var shortcode_base
	 */
	private $shortcode_base;

	/**
	 * construct
	 */
	public function __construct() {
		$this->shortcode_base = 'tm_call_to_action';
		add_action('vc_before_init', array($this, 'MapShortCodeVC'));
	}

	/**
	 * Returns shortcode_base for shortcode
	 */
	public function getShortCodeBase() {
		return $this->shortcode_base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 */
	public function MapShortCodeVC() {

		if(function_exists('vc_map')) {
			$group_button_options = 'Button Options';
			$group_icon_options = 'Icons Options';
			$group_design_options = 'Button Design Options';

			//collect vc icon pack
			$vc_icon_pack_array = array();
			if( function_exists('medicale_mascot_get_vc_icon_pack_array') ) {
				$vc_icon_pack_array = medicale_mascot_get_vc_icon_pack_array( array('element' => 'visual_style', 'value' => array('with-icons')), $group_icon_options );
			}

			$params_part1 = array(
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Content in grid", 'mascot-core' ),
					"param_name"	=> "content_in_grid",
					"description"	=> esc_html__( "Choose content in grid or not.", 'mascot-core' ),
					'value'			=> array(
						'Yes'		=> 'yes',
						'No'		=> 'no'
					),
					'save_always'   => true,
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Grid size", 'mascot-core' ),
					"param_name"	=> "grid_size",
					"description"	=> esc_html__( "Choose grid size.", 'mascot-core' ),
					'value'			=> array(
						'10:2'		=> '102',
						'9:3'		=> '93',
						'6:6'		=> '66',
						'8:4'		=> '84',
					),
					'save_always'   => true,
					'dependency'	=> array('element' => 'content_in_grid', 'value' => 'yes')
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Visual Style", 'mascot-core' ),
					"param_name"	=> "visual_style",
					'description'   => '',
					'value'			=> array(
						'Default'	=> 'default',
						'With Icon' => 'with-icons',
						'With Two Buttons Below' => 'with-buttons',
					),
					'admin_label'   => true,
					'save_always'   => true,
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Show Button", 'mascot-core' ),
					"param_name"	=> "show_button",
					'description'   => '',
					'value'			=> array(
						'Yes'		=> 'yes',
						'No'		=> 'no'
					),
					'admin_label'   => true,
					'save_always'   => true,
					'dependency' => array('element' => 'visual_style', 'value' => array('default'))
				),



				//Default Button options
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Button Text", 'mascot-core' ),
					"param_name"	=> "button_0_text",
					"description"	=> esc_html__( 'Default text is "Read More"', 'mascot-core' ),
					'admin_label'   => true,
					'dependency' => array('element' => 'show_button', 'value' => array('yes')),
					'group'			=> $group_button_options
				),
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Button Link", 'mascot-core' ),
					"param_name"	=> "button_0_link",
					'description'   => 'Link must starts with http:// or https://',
					'admin_label'   => true,
					'dependency' => array('element' => 'show_button', 'value' => array('yes')),
					'group'			=> $group_button_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Button Target", 'mascot-core' ),
					"param_name"	=> "button_0_target",
					'description'   => '',
					'value' => array(
						'' => '',
						'Self' => '_self',
						'New Tab (Blank)' => '_blank'
					),
					'save_always'   => true,
					'description'   => '',
					'dependency' => array('element' => 'show_button', 'value' => array('yes')),
					'group'			=> $group_button_options
				),

				//Button Design Options
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Button Design Style", 'mascot-core' ),
					"param_name"	=> "button_0_design_style",
					'description'   => '',
					'value' => array(
						'Button Defaults'   => 'btn-default',
						'Button Border'	 => 'btn-border',
						'Button Dark'		=> 'btn-dark',
						'Button Gray'		=> 'btn-gray'
					),
					'save_always'   => true,
					'dependency' => array('element' => 'show_button', 'value' => array('yes')),
					'group'			=> $group_design_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Button Size", 'mascot-core' ),
					"param_name"	=> "button_0_size",
					'description'   => '',
					'value' => array(
						'Button Defaults'		=> '',
						'Button Extra Small'	=> 'btn-xs',
						'Button Small'		  => 'btn-sm',
						'Button Large'		  => 'btn-lg',
						'Button Extra Large'	=> 'btn-xl'
					),
					'save_always'   => true,
					'dependency' => array('element' => 'show_button', 'value' => array('yes')),
					'group'			=> $group_design_options
				),
				array(
					'type'			=> 'checkbox',
					"heading"		=> esc_html__( "Make Button Theme Colored?", 'mascot-core' ),
					"param_name"	=> "button_0_btn_theme_colored",
					'description'   => '',
					'save_always'   => true,
					'dependency' => array('element' => 'show_button', 'value' => array('yes')),
					'group'			=> $group_design_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Button Position", 'mascot-core' ),
					"param_name"	=> "button_0_position",
					'description'   => '',
					'value' => array(
						'Right' => 'text-right',
						'Center' => 'text-center',
						'Left' => 'text-left'
					),
					'save_always'   => true,
					'dependency' => array('element' => 'show_button', 'value' => array('yes')),
					'group'			=> $group_design_options
				),
			);

			$merged_fields = array_merge($params_part1, $vc_icon_pack_array);


			$params_part2 = array(
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Icons Link", 'mascot-core' ),
					"param_name"	=> "icon_link",
					'description'   => '',
					'dependency' => array('element' => 'icon_pack', 'not_empty' => true),
					'group'			=> $group_icon_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Icons Target", 'mascot-core' ),
					"param_name"	=> "icon_target",
					'description'   => '',
					'value' => array(
						'' => '',
						'Self' => '_self',
						'New Tab (Blank)' => '_blank'
					),
					'save_always'   => true,
					'dependency' => array('element' => 'icon_link', 'not_empty' => true),
					'group'			=> $group_icon_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Icons Placement", 'mascot-core' ),
					"param_name"	=> "icon_placement",
					'description'   => '',
					'value' => array(
						'Right of Text' => 'right',
						'Left of Text' => 'left'
					),
					'save_always'   => true,
					'dependency'	=> array('element' => 'visual_style', 'value' => array('with-icons')),
					'group'			=> $group_icon_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Icons Alignment", 'mascot-core' ),
					"param_name"	=> "icon_alignment",
					'description'   => '',
					'value' => array(
						'Right' => 'text-right',
						'Center' => 'text-center',
						'Left' => 'text-left'
					),
					'save_always'   => true,
					'dependency' => array('element' => 'visual_style', 'value' => array('with-icons')),
					'group'			=> $group_icon_options
				),

				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Icon Font Size (px)", 'mascot-core' ),
					"param_name"	=> "icon_font_size",
					"description"	=> esc_html__( "Default font size 30px", 'mascot-core' ),
					'dependency'	=> array('element' => 'visual_style', 'value' => array('with-icons')),
					'group'			=> $group_icon_options
				),
				array(
					'type'			=> 'colorpicker',
					"heading"		=> esc_html__( "Icon Color", 'mascot-core' ),
					"param_name"	=> "icon_color",
					'description'   => '',
					'save_always'   => true,
					'dependency'	=> array('element' => 'visual_style', 'value' => array('with-icons')),
					'group'			=> $group_icon_options
				),


				//Two Button options

				//button 1
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Button 1 Text", 'mascot-core' ),
					"param_name"	=> "button_1_text",
					'admin_label'   => true,
					'description'   => 'Default text is "Read More"',
					'dependency'	=> array('element' => 'visual_style', 'value' => array('with-buttons')),
					'group'			=> $group_button_options
				),
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Button 1 Link", 'mascot-core' ),
					"param_name"	=> "button_1_link",
					'description'   => 'Link must starts with http:// or https://',
					'dependency'	=> array('element' => 'visual_style', 'value' => array('with-buttons')),
					'group'			=> $group_button_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Button 1 Target", 'mascot-core' ),
					"param_name"	=> "button_1_target",
					'description'   => '',
					'value' => array(
						'' => '',
						'Self' => '_self',
						'New Tab (Blank)' => '_blank'
					),
					'save_always'   => true,
					'dependency'	=> array('element' => 'visual_style', 'value' => array('with-buttons')),
					'group'			=> $group_button_options
				),

				//button 2
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Button 2 Text", 'mascot-core' ),
					"param_name"	=> "button_2_text",
					'admin_label'   => true,
					'description'   => 'Default text is "Read More"',
					'dependency'	=> array('element' => 'visual_style', 'value' => array('with-buttons')),
					'group'			=> $group_button_options
				),
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Button 2 Link", 'mascot-core' ),
					"param_name"	=> "button_2_link",
					'description'   => 'Link must starts with http:// or https://',
					'dependency'	=> array('element' => 'visual_style', 'value' => array('with-buttons')),
					'group'			=> $group_button_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Button 2 Target", 'mascot-core' ),
					"param_name"	=> "button_2_target",
					'description'   => '',
					'value' => array(
						'' => '',
						'Self' => '_self',
						'New Tab (Blank)' => '_blank'
					),
					'save_always'   => true,
					'dependency'	=> array('element' => 'visual_style', 'value' => array('with-buttons')),
					'group'			=> $group_button_options
				),


				//Button Design Options
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Button Design Style", 'mascot-core' ),
					"param_name"	=> "button_1_design_style",
					'description'   => '',
					'value' => array(
						'Button Defaults'   => 'btn-default',
						'Button Border'	 => 'btn-border',
						'Button Dark'		=> 'btn-dark',
						'Button Gray'		=> 'btn-gray'
					),
					'save_always'   => true,
					'dependency'	=> array('element' => 'visual_style', 'value' => array('with-buttons')),
					'group'			=> $group_design_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Button Size", 'mascot-core' ),
					"param_name"	=> "button_1_size",
					'description'   => '',
					'value' => array(
						'Button Defaults'		=> '',
						'Button Extra Small'	=> 'btn-xs',
						'Button Small'		  => 'btn-sm',
						'Button Large'		  => 'btn-lg',
						'Button Extra Large'	=> 'btn-xl'
					),
					'save_always'   => true,
					'dependency'	=> array('element' => 'visual_style', 'value' => array('with-buttons')),
					'group'			=> $group_design_options
				),
				array(
					'type'			=> 'checkbox',
					"heading"		=> esc_html__( "Make Button Theme Colored?", 'mascot-core' ),
					"param_name"	=> "button_1_btn_theme_colored",
					'description'   => '',
					'save_always'   => true,
					'dependency'	=> array('element' => 'visual_style', 'value' => array('with-buttons')),
					'group'			=> $group_design_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Button Design Style", 'mascot-core' ),
					"param_name"	=> "button_2_design_style",
					'description'   => '',
					'value' => array(
						'Button Defaults'   => 'btn-default',
						'Button Border'	 => 'btn-border',
						'Button Dark'		=> 'btn-dark',
						'Button Gray'		=> 'btn-gray'
					),
					'save_always'   => true,
					'dependency'	=> array('element' => 'visual_style', 'value' => array('with-buttons')),
					'group'			=> $group_design_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Button Size", 'mascot-core' ),
					"param_name"	=> "button_2_size",
					'description'   => '',
					'value' => array(
						'Button Defaults'		=> '',
						'Button Extra Small'	=> 'btn-xs',
						'Button Small'		  => 'btn-sm',
						'Button Large'		  => 'btn-lg',
						'Button Extra Large'	=> 'btn-xl'
					),
					'save_always'   => true,
					'dependency'	=> array('element' => 'visual_style', 'value' => array('with-buttons')),
					'group'			=> $group_design_options
				),
				array(
					'type'			=> 'checkbox',
					"heading"		=> esc_html__( "Make Button Theme Colored?", 'mascot-core' ),
					"param_name"	=> "button_2_btn_theme_colored",
					'description'   => '',
					'save_always'   => true,
					'dependency'	=> array('element' => 'visual_style', 'value' => array('with-buttons')),
					'group'			=> $group_design_options
				),


				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Button Position", 'mascot-core' ),
					"param_name"	=> "button_position",
					'description'   => '',
					'value' => array(
						'Right' => 'text-right',
						'Center' => 'text-center',
						'Left' => 'text-left'
					),
					'save_always'   => true,
					'dependency'	=> array('element' => 'visual_style', 'value' => array('with-buttons')),
					'group'			=>  $group_design_options
				),




				array(
					'type'			=> 'textarea_html',
					"heading"		=> esc_html__( "Content", 'mascot-core' ),
					"param_name"	=> "content",
					"description"	=> esc_html__( "Choose content in grid or not.", 'mascot-core' ),
					'admin_label' => true,
					'value' => "Default text for Call to action. Lorem ipsum dolor sit amet, consectetur adipisicing elit."
				),
			);

			$merged_fields = array_merge($merged_fields, $params_part2);

			$vc_map = array(
				'name'		=> esc_html__( 'Call To Action', 'mascot-core' ),
				'base'		=>$this->shortcode_base,
				'category'	=> 'by TM',
				'icon'		=> 'mascot-vc-icons vc-icon-cta',
				'allowed_container_element' => 'vc_row',
				'params'	=> $merged_fields
			);
			vc_map( $vc_map );

			//Modify vc_map from themes
			if( function_exists('mascot_core_sc_call_to_action_vc_map_modifier') ) {
				mascot_core_sc_call_to_action_vc_map_modifier( $this->shortcode_base );
			}
		}
	}

	/**
	 * Renders shortcodes HTML
	 */
	public function render( $attr, $content = null ) {

		//Render Shortcode from themes
		if( function_exists('mascot_core_sc_call_to_action_render') ) {
			return mascot_core_sc_call_to_action_render( $attr, $content );
		}
		
	}
}