<?php
namespace MASCOTCORE\Shortcodes\OpeningHours;

use MASCOTCORE\Shortcodes\Lib;

/**
 * class SC_Opening_Hours
 * @package MASCOTCORE\Shortcodes\OpeningHours;
 */
class SC_Opening_Hours implements Lib\Mascot_Core_Interface_Shortcodes {
	
	/**
	 * @var shortcode_base
	 */
	private $shortcode_base;

	/**
	 * construct
	 */
	public function __construct() {
		$this->shortcode_base = 'tm_opening_hours';
		add_action('vc_before_init', array($this, 'MapShortCodeVC'));
	}

	/**
	 * Returns shortcode_base for shortcode
	 */
	public function getShortCodeBase() {
		return $this->shortcode_base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 */
	public function MapShortCodeVC() {
		$group_wow_options = 'Wow Animation Options';
		$title_options = 'Title Options';

		if(function_exists('vc_map')) {
			$vc_map = array(
				'name'		=> esc_html__( 'Opening Hours', 'mascot-core' ),
				'base'		=>$this->shortcode_base,
				'category'	=> 'by TM',
				'icon'		=> 'mascot-vc-icons vc-icon-cta',
				'allowed_container_element' => 'vc_row',
				'params'	=>array(
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Custom CSS class", 'mascot-core' ),
						"param_name"	=> "custom_css_class",
						"description"	=> esc_html__( 'To style particular content element.', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Border Color", 'mascot-core' ),
						"param_name"	=> "border_color",
						'value'			=> array(
							'Border Light'	 => 'border-light',
							'Border Dark'	=> 'border-dark'
						),
						'admin_label'   => true,
						'save_always'   => true,
					),

					
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Monday", 'mascot-core' ),
						"param_name"	=> "day_monday",
						"description"	=> "",
						"value"		 => esc_html__( "9:00 - 17:00", 'mascot-core' ),
						'admin_label'   => true,
						'save_always'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Tuesday", 'mascot-core' ),
						"param_name"	=> "day_tuesday",
						"description"	=> "",
						"value"		 => esc_html__( "9:00 - 17:00", 'mascot-core' ),
						'admin_label'   => true,
						'save_always'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Wednesday", 'mascot-core' ),
						"param_name"	=> "day_wednesday",
						"description"	=> "",
						"value"		 => esc_html__( "9:00 - 17:00", 'mascot-core' ),
						'admin_label'   => true,
						'save_always'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Thursday", 'mascot-core' ),
						"param_name"	=> "day_thursday",
						"description"	=> "",
						"value"		 => esc_html__( "9:00 - 17:00", 'mascot-core' ),
						'admin_label'   => true,
						'save_always'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Friday", 'mascot-core' ),
						"param_name"	=> "day_friday",
						"description"	=> "",
						"value"		 => esc_html__( "9:30 - 16:00", 'mascot-core' ),
						'admin_label'   => true,
						'save_always'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Saturday", 'mascot-core' ),
						"param_name"	=> "day_saturday",
						"description"	=> "",
						"value"		 => esc_html__( "10:00 - 15:00", 'mascot-core' ),
						'admin_label'   => true,
						'save_always'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Sunday", 'mascot-core' ),
						"param_name"	=> "day_sunday",
						"description"	=> "",
						"value"		 => esc_html__( "Closed", 'mascot-core' ),
						'admin_label'   => true,
						'save_always'   => true,
					),

				)
			);
			vc_map( $vc_map );

			//Modify vc_map from themes
			if( function_exists('mascot_core_sc_opening_hours_vc_map_modifier') ) {
				mascot_core_sc_opening_hours_vc_map_modifier( $this->shortcode_base );
			}
		}
	}

	/**
	 * Renders shortcodes HTML
	 */
	public function render( $attr, $content = null ) {

		//Render Shortcode from themes
		if( function_exists('mascot_core_sc_opening_hours_render') ) {
			return mascot_core_sc_opening_hours_render( $attr, $content );
		}
		
	}
}