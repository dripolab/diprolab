<?php
namespace MASCOTCORE\Shortcodes\CountdownTimer;

use MASCOTCORE\Shortcodes\Lib;

/**
 * class SC_Countdown_Timer
 * @package MASCOTCORE\Shortcodes\CountdownTimer;
 */
class SC_Countdown_Timer implements Lib\Mascot_Core_Interface_Shortcodes {
	
	/**
	 * @var shortcode_base
	 */
	private $shortcode_base;

	/**
	 * construct
	 */
	public function __construct() {
		$this->shortcode_base = 'tm_countdown_timer';
		add_action('vc_before_init', array($this, 'MapShortCodeVC'));
	}

	/**
	 * Returns shortcode_base for shortcode
	 */
	public function getShortCodeBase() {
		return $this->shortcode_base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 */
	public function MapShortCodeVC() {
		if(function_exists('vc_map')) {
			$group_content_options = 'Content Options';

			$vc_map = array(
				'name'		=> esc_html__( 'Countdown Timer', 'mascot-core' ),
				'base'		=>$this->shortcode_base,
				'category'	=> 'by TM',
				'icon'		=> 'mascot-vc-icons vc-icon-cta',
				'allowed_container_element' => 'vc_row',
				'params'	=>array(
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Custom CSS class", 'mascot-core' ),
						"param_name"	=> "custom_css_class",
						"description"	=> esc_html__( 'To style particular content element.', 'mascot-core' ),
						'admin_label'	=> true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Future Date & Time (Format Y/m/d H:i:s)", 'mascot-core' ),
						"param_name"	=> "countdown_future_date_time",
						"std"			=> date("Y/m/d H:i:s", strtotime("+5 week")),
						"description"	=> sprintf( esc_html__( 'See documentation from %1$shere%2$s to format a local time/date.', 'mascot-core' ), '<a target="_blank" href="' . esc_url( 'http://www.php.net/manual/en/function.date.php' ) . '">', '</a>' ),
						'save_always'	=> true,
						'admin_label'	=> true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Design Style", 'mascot-core' ),
						"param_name"	=> "design_style",
						"description"	=> "",
						'value'			=> array(
							'Final Countdown - Advanced Coupon'	=> 'final-countdown-advanced-coupon',
							'Final Countdown - Basic Coupon'	=> 'final-countdown-basic-coupon',
							'Final Countdown - Legacy style'	=> 'final-countdown-legacy-style',
							'Final Countdown - Months + Days'	=> 'final-countdown-months-offsets',
							'Final Countdown - Weeks + Days'	=> 'final-countdown-weeks-offsets',
							'Final Countdown - Only Days'		=> 'final-countdown-days-offsets',
							'Final Countdown - Only Hours'		=> 'final-countdown-hours',
						),
						'save_always'	=> true,
						'admin_label'	=> true,
					),
					array(
						'type'			=> 'checkbox',
						"heading"		=> esc_html__( "Show Time?", 'mascot-core' ),
						"param_name"	=> "show_time",
						"description"	=> esc_html__( 'Show or Hide Time of the date', 'mascot-core' ),
						"std"			=> 'true',
					),

				)
			);
			vc_map( $vc_map );

			//Modify vc_map from themes
			if( function_exists('mascot_core_sc_countdown_timer_vc_map_modifier') ) {
				mascot_core_sc_countdown_timer_vc_map_modifier( $this->shortcode_base );
			}
		}
	}

	/**
	 * Renders shortcodes HTML
	 */
	public function render( $attr, $content = null ) {

		//Render Shortcode from themes
		if( function_exists('mascot_core_sc_countdown_timer_render') ) {
			return mascot_core_sc_countdown_timer_render( $attr, $content );
		}
		
	}
}