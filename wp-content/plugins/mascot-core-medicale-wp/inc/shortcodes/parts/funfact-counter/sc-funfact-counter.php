<?php
namespace MASCOTCORE\Shortcodes\FunFactCounter;

use MASCOTCORE\Shortcodes\Lib;

/**
 * class SC_FunFact_Counter
 * @package MASCOTCORE\Shortcodes\FunFact;
 */
class SC_FunFact_Counter implements Lib\Mascot_Core_Interface_Shortcodes {
	
	/**
	 * @var shortcode_base
	 */
	private $shortcode_base;

	/**
	 * construct
	 */
	public function __construct() {
		$this->shortcode_base = 'tm_funfact_counter';
		add_action('vc_before_init', array($this, 'MapShortCodeVC'));
	}

	/**
	 * Returns shortcode_base for shortcode
	 */
	public function getShortCodeBase() {
		return $this->shortcode_base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 */
	public function MapShortCodeVC() {
		if(function_exists('vc_map')) {
			$group_icon_options = 'Icon Options';
			$title_options = 'Title Options';
			$counter_value_options = 'Counter Options';

			//collect vc icon pack
			$vc_icon_pack_array = array();
			if( function_exists('medicale_mascot_get_vc_icon_pack_array') ) {
				$vc_icon_pack_array = medicale_mascot_get_vc_icon_pack_array( array('element' => 'icon_type', 'value' => array('font-icon')), $group_icon_options );
			}

			$params_part1 = array(
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Custom CSS class", 'mascot-core' ),
					"param_name"	=> "custom_css_class",
					"description"	=> esc_html__( 'To style particular content element.', 'mascot-core' ),
					'admin_label'   => true,
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Design Style", 'mascot-core' ),
					"param_name"	=> "design_style",
					"description"	=> "",
					'value'			=> array(
						'Style 1'   => 'style1',
						'Style 2'   => 'style2'
					),
					'save_always'   => true,
					'admin_label'   => true,
				),
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Title", 'mascot-core' ),
					"param_name"	=> "title",
					"description"	=> esc_html__( 'Eg: Projects Completed', 'mascot-core' ),
					'admin_label'   => true,
				),
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Counter Value", 'mascot-core' ),
					"param_name"	=> "counter_range",
					"description"	=> esc_html__( 'Enter number for counter without any special character. Default: 1250', 'mascot-core' ),
					'admin_label'   => true,
				),
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Counter Duration(ms)", 'mascot-core' ),
					"param_name"	=> "counter_duration",
					"description"	=> esc_html__( 'Default: 1500', 'mascot-core' ),
					'admin_label'   => true,
				),


				//Icon Options
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Icon Type", 'mascot-core' ),
					"param_name"	=> "icon_type",
					"description"	=> "Choose font icon or image.",
					'value'			=> array(
						'Font Icon'		=> 'font-icon',
						'JPG/PNG Image'   => 'image'
					),
					'save_always'   => true,
					'admin_label'   => true,
					'group'			=> $group_icon_options
				),
			);

			$merged_fields = array_merge($params_part1, $vc_icon_pack_array);


			$params_part2 = array(
				array(
					'type'			=> 'attach_image',
					"heading"		=> esc_html__( "Upload Image Icon", 'mascot-core' ),
					"param_name"	=> "image_icon",
					"description"	=> esc_html__( 'Upload the custom image icon.', 'mascot-core' ),
					'admin_label'   => true,
					'group'			=> $group_icon_options,
					'dependency'	=> array('element' => 'icon_type', 'value' => 'image'),
				),
				array(
					'type'			=> 'colorpicker',
					"heading"		=> esc_html__( "Icon Color", 'mascot-core' ),
					"param_name"	=> "icon_color",
					"value"		 => '',
					'admin_label'   => true,
					'group'			=> $group_icon_options,
					'dependency'	=> array('element' => 'icon_type', 'value' => 'font-icon'),
				),


				//Counter Value Options
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Counter Tag", 'mascot-core' ),
					"param_name"	=> "counter_tag",
					"description"	=> "",
					'value'			=> mascot_core_heading_tag_list(),
					'admin_label'   => true,
					'group'			=> $counter_value_options
				),
				array(
					'type'			=> 'colorpicker',
					"heading"		=> esc_html__( "Text Color", 'mascot-core' ),
					"param_name"	=> "counter_text_color",
					"value"		 => '',
					'group'			=> $counter_value_options
				),
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Font Size (px)", 'mascot-core' ),
					"param_name"	=> "counter_font_size",
					"description"	=> "",
					'group'			=> $counter_value_options
				),
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Line Height (px)", 'mascot-core' ),
					"param_name"	=> "counter_line_height",
					"description"	=> "",
					'group'			=> $counter_value_options
				),
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Letter Spacing (px)", 'mascot-core' ),
					"param_name"	=> "counter_letter_spacing",
					"description"	=> "",
					'group'			=> $counter_value_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Font Style", 'mascot-core' ),
					"param_name"	=> "counter_font_style",
					"description"	=> "",
					'value'			=> mascot_core_vc_font_style_list(),
					'group'			=> $counter_value_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Font Weight", 'mascot-core' ),
					"param_name"	=> "counter_font_weight",
					"description"	=> "",
					'value'			=> mascot_core_vc_font_weight_list(),
					'group'			=> $counter_value_options
				),


				//Title Options
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Title Tag", 'mascot-core' ),
					"param_name"	=> "title_tag",
					"description"	=> "",
					'value'			=> mascot_core_heading_tag_list(),
					'admin_label'   => true,
					'group'			=> $title_options
				),
				array(
					'type'			=> 'colorpicker',
					"heading"		=> esc_html__( "Text Color", 'mascot-core' ),
					"param_name"	=> "title_text_color",
					"value"		 => '',
					'group'			=> $title_options
				),
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Font Size (px)", 'mascot-core' ),
					"param_name"	=> "title_font_size",
					"description"	=> "",
					'group'			=> $title_options
				),
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Line Height (px)", 'mascot-core' ),
					"param_name"	=> "title_line_height",
					"description"	=> "",
					'group'			=> $title_options
				),
				array(
					'type'			=> 'textfield',
					"heading"		=> esc_html__( "Letter Spacing (px)", 'mascot-core' ),
					"param_name"	=> "title_letter_spacing",
					"description"	=> "",
					'group'			=> $title_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Font Style", 'mascot-core' ),
					"param_name"	=> "title_font_style",
					"description"	=> "",
					'value'			=> mascot_core_vc_font_style_list(),
					'group'			=> $title_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Font Weight", 'mascot-core' ),
					"param_name"	=> "title_font_weight",
					"description"	=> "",
					'value'			=> mascot_core_vc_font_weight_list(),
					'group'			=> $title_options
				),
				array(
					'type'			=> 'dropdown',
					"heading"		=> esc_html__( "Text Transform", 'mascot-core' ),
					"param_name"	=> "title_text_transform",
					"description"	=> "",
					'value'			=> mascot_core_vc_text_transform_list(),
					'group'			=> $title_options
				),
			);

			$merged_fields = array_merge($merged_fields, $params_part2);

			$vc_map = array(
				'name'		=> esc_html__( 'Fun Fact Counter', 'mascot-core' ),
				'base'		=>$this->shortcode_base,
				'category'	=> 'by TM',
				'icon'		=> 'mascot-vc-icons vc-icon-cta',
				'allowed_container_element' => 'vc_row',
				'params'	=> $merged_fields
			);
			vc_map( $vc_map );

			//Modify vc_map from themes
			if( function_exists('mascot_core_sc_funfact_counter_vc_map_modifier') ) {
				mascot_core_sc_funfact_counter_vc_map_modifier( $this->shortcode_base );
			}
		}
	}

	/**
	 * Renders shortcodes HTML
	 */
	public function render( $attr, $content = null ) {

		//Render Shortcode from themes
		if( function_exists('mascot_core_sc_funfact_counter_render') ) {
			return mascot_core_sc_funfact_counter_render( $attr, $content );
		}
		
	}
}