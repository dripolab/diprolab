<?php
namespace MASCOTCORE\Shortcodes\OpeningHoursCompressed;

use MASCOTCORE\Shortcodes\Lib;

/**
 * class SC_Opening_Hours_Compressed
 * @package MASCOTCORE\Shortcodes\OpeningHoursCompressed;
 */
class SC_Opening_Hours_Compressed implements Lib\Mascot_Core_Interface_Shortcodes {
	
	/**
	 * @var shortcode_base
	 */
	private $shortcode_base;

	/**
	 * construct
	 */
	public function __construct() {
		$this->shortcode_base = 'tm_opening_hours_compressed';
		add_action('vc_before_init', array($this, 'MapShortCodeVC'));
	}

	/**
	 * Returns shortcode_base for shortcode
	 */
	public function getShortCodeBase() {
		return $this->shortcode_base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 */
	public function MapShortCodeVC() {
		$group_wow_options = 'Wow Animation Options';
		$title_options = 'Title Options';

		if(function_exists('vc_map')) {
			$vc_map = array(
				'name'		=> esc_html__( 'Opening Hours Compressed', 'mascot-core' ),
				'base'		=>$this->shortcode_base,
				'category'	=> 'by TM',
				'icon'		=> 'mascot-vc-icons vc-icon-cta',
				'allowed_container_element' => 'vc_row',
				'params'	=>array(
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Custom CSS class", 'mascot-core' ),
						"param_name"	=> "custom_css_class",
						"description"	=> esc_html__( 'To style particular content element.', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Border Color", 'mascot-core' ),
						"param_name"	=> "border_color",
						'value'			=> array(
							'Border Light'	 => 'border-light',
							'Border Dark'	=> 'border-dark'
						),
						'admin_label'   => true,
						'save_always'   => true,
					),

					//day1
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Day 1:", 'mascot-core' ),
						"param_name"	=> "day_1",
						"description"	=> "",
						"value"		 => esc_html__( "Monday - Friday", 'mascot-core' ),
						'admin_label'   => true,
						'save_always'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Time for Day 1:", 'mascot-core' ),
						"param_name"	=> "day_1_time",
						"description"	=> "",
						"value"		 => esc_html__( "9:00 - 17:00", 'mascot-core' ),
						'admin_label'   => true,
						'save_always'   => true,
					),

					//day2
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Day 2:", 'mascot-core' ),
						"param_name"	=> "day_2",
						"description"	=> "",
						"value"		 => esc_html__( "Saturday", 'mascot-core' ),
						'admin_label'   => true,
						'save_always'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Time for Day 2:", 'mascot-core' ),
						"param_name"	=> "day_2_time",
						"description"	=> "",
						"value"		 => esc_html__( "9.00 - 16.00", 'mascot-core' ),
						'admin_label'   => true,
						'save_always'   => true,
					),

					//day3
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Day 3:", 'mascot-core' ),
						"param_name"	=> "day_3",
						"description"	=> "",
						"value"		 => esc_html__( "Sunday", 'mascot-core' ),
						'admin_label'   => true,
						'save_always'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Time for Day 3:", 'mascot-core' ),
						"param_name"	=> "day_3_time",
						"description"	=> "",
						"value"		 => esc_html__( "Closed", 'mascot-core' ),
						'admin_label'   => true,
						'save_always'   => true,
					),

					//day4
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Day 4:", 'mascot-core' ),
						"param_name"	=> "day_4",
						"description"	=> "",
						'admin_label'   => true,
						'save_always'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Time for Day 4:", 'mascot-core' ),
						"param_name"	=> "day_4_time",
						"description"	=> "",
						'admin_label'   => true,
						'save_always'   => true,
					),

					//day5
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Day 5:", 'mascot-core' ),
						"param_name"	=> "day_5",
						"description"	=> "",
						'admin_label'   => true,
						'save_always'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Time for Day 5:", 'mascot-core' ),
						"param_name"	=> "day_5_time",
						"description"	=> "",
						'admin_label'   => true,
						'save_always'   => true,
					),

					//day6
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Day 6:", 'mascot-core' ),
						"param_name"	=> "day_6",
						"description"	=> "",
						'admin_label'   => true,
						'save_always'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Time for Day 6:", 'mascot-core' ),
						"param_name"	=> "day_6_time",
						"description"	=> "",
						'admin_label'   => true,
						'save_always'   => true,
					),

					//day7
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Day 7:", 'mascot-core' ),
						"param_name"	=> "day_7",
						"description"	=> "",
						'admin_label'   => true,
						'save_always'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Time for Day 7:", 'mascot-core' ),
						"param_name"	=> "day_7_time",
						"description"	=> "",
						'admin_label'   => true,
						'save_always'   => true,
					),

				)
			);
			vc_map( $vc_map );

			//Modify vc_map from themes
			if( function_exists('mascot_core_sc_opening_hours_compressed_vc_map_modifier') ) {
				mascot_core_sc_opening_hours_compressed_vc_map_modifier( $this->shortcode_base );
			}
		}
	}

	/**
	 * Renders shortcodes HTML
	 */
	public function render( $attr, $content = null ) {

		//Render Shortcode from themes
		if( function_exists('mascot_core_sc_opening_hours_compressed_render') ) {
			return mascot_core_sc_opening_hours_compressed_render( $attr, $content );
		}
		
	}
}