<?php
namespace MASCOTCORE\Shortcodes;

use MASCOTCORE\Shortcodes\Lib;
use MASCOTCORE\Shortcodes\BeforeAfterSlider\SC_Before_After_Slider;
use MASCOTCORE\Shortcodes\Blog\SC_Blog;
use MASCOTCORE\Shortcodes\Button\SC_Button;
use MASCOTCORE\Shortcodes\CallToAction\SC_Call_To_Action;
use MASCOTCORE\Shortcodes\CountdownTimer\SC_Countdown_Timer;
use MASCOTCORE\Shortcodes\FunFactCounter\SC_FunFact_Counter;
use MASCOTCORE\Shortcodes\GoogleMap\SC_Google_Map;
use MASCOTCORE\Shortcodes\IconBox\SC_Icon_Box;
use MASCOTCORE\Shortcodes\OpeningHours\SC_Opening_Hours;
use MASCOTCORE\Shortcodes\OpeningHoursCompressed\SC_Opening_Hours_Compressed;
use MASCOTCORE\Shortcodes\ProgressBar\SC_Progress_Bar;
use MASCOTCORE\Shortcodes\SectionTitle\SC_Section_Title;
use MASCOTCORE\Shortcodes\SocialList\SC_Social_List;

/**
 * class Loader_Shortcodes
 * @package MASCOTCORE\Shortcodes;
 */
class Loader_Shortcodes {
	/**
	 * @var Singleton The reference to *Singleton* instance of this class
	 */
	private static $instance;

	/**
	 * @var array
	 */
	private $allShortCodes = array();
	
	/**
	 * Returns the *Singleton* instance of this class.
	 *
	 * @return Singleton The *Singleton* instance.
	 */
	public static function get_instance()
	{
		if (null === static::$instance) {
			static::$instance = new static();
		}
		
		return static::$instance;
	}

	/**
	 * Protected constructor to prevent creating a new instance of the
	 * *Singleton* via the `new` operator from outside of this class.
	 */
	protected function __construct()
	{
	}

	/**
	 * Private clone method to prevent cloning of the instance of the
	 * *Singleton* instance.
	 *
	 * @return void
	 */
	private function __clone()
	{
	}

	/**
	 * Private unserialize method to prevent unserializing of the *Singleton*
	 * instance.
	 *
	 * @return void
	 */
	private function __wakeup()
	{
	}


	/**
	 * Adds new SC to SC array
	 */
	private function addNewShortCode(Lib\Mascot_Core_Interface_Shortcodes $newShortCode) {
		if(!array_key_exists($newShortCode->getShortCodeBase(), $this->allShortCodes)) {
			$this->allShortCodes[$newShortCode->getShortCodeBase()] = $newShortCode;
		}
	}

	/**
	 * List of all SCs to register
	 */
	private function available_shortcodes() {
		$this->addNewShortCode(new SC_Before_After_Slider());
		$this->addNewShortCode(new SC_Blog());
		$this->addNewShortCode(new SC_Button());
		$this->addNewShortCode(new SC_Call_To_Action());
		$this->addNewShortCode(new SC_Countdown_Timer());
		$this->addNewShortCode(new SC_FunFact_Counter());
		$this->addNewShortCode(new SC_Google_Map());
		$this->addNewShortCode(new SC_Icon_Box());
		$this->addNewShortCode(new SC_Opening_Hours());
		$this->addNewShortCode(new SC_Opening_Hours_Compressed());
		$this->addNewShortCode(new SC_Progress_Bar());
		$this->addNewShortCode(new SC_Section_Title());
		$this->addNewShortCode(new SC_Social_List());
	}


	/**
	 * Calls available_shortcodes method, loops through each SC in array and add it through add_shortcode
	 */
	public function load() {
		$this->available_shortcodes();
		foreach ($this->allShortCodes as $eachShortCode) {
			add_shortcode($eachShortCode->getShortCodeBase(), array($eachShortCode, 'render'));
		}
	}
}

//Create instance of Loader_Shortcodes
$scloader = Loader_Shortcodes::get_instance();
$scloader->load();