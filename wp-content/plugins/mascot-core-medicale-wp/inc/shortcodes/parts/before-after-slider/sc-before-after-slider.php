<?php
namespace MASCOTCORE\Shortcodes\BeforeAfterSlider;

use MASCOTCORE\Shortcodes\Lib;

/**
 * class SC_Before_After_Slider
 * @package MASCOTCORE\Shortcodes\BeforeAfterSlider;
 */
class SC_Before_After_Slider implements Lib\Mascot_Core_Interface_Shortcodes {
	
	/**
	 * @var shortcode_base
	 */
	private $shortcode_base;

	/**
	 * construct
	 */
	public function __construct() {
		$this->shortcode_base = 'tm_before_after_slider';
		add_action('vc_before_init', array($this, 'MapShortCodeVC'));
	}

	/**
	 * Returns shortcode_base for shortcode
	 */
	public function getShortCodeBase() {
		return $this->shortcode_base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 */
	public function MapShortCodeVC() {
		$group_wow_options = 'Wow Animation Options';
		$title_options = 'Title Options';

		if(function_exists('vc_map')) {
			$vc_map = array(
				'name'		=> esc_html__( 'Before After Slider', 'mascot-core' ),
				'base'		=>$this->shortcode_base,
				'category'	=> 'by TM',
				'icon'		=> 'mascot-vc-icons vc-icon-cta',
				'allowed_container_element' => 'vc_row',
				'params'	=>array(
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Custom CSS class", 'mascot-core' ),
						"param_name"	=> "custom_css_class",
						"description"	=> esc_html__( 'To style particular content element.', 'mascot-core' ),
						'admin_label'   => true,
					),


					array(
						'type'			=> 'attach_image',
						"heading"		=> esc_html__( "Before Image", 'mascot-core' ),
						"param_name"	=> "before_image",
						"description"	=> esc_html__( 'Upload the before image.', 'mascot-core' ),
						'save_always'   => true,
						'admin_label'   => true,
					),

					array(
						'type'			=> 'attach_image',
						"heading"		=> esc_html__( "After Image", 'mascot-core' ),
						"param_name"	=> "after_image",
						"description"	=> esc_html__( 'Upload the after image. Before and After image should have same dimension.', 'mascot-core' ),
						'save_always'   => true,
						'admin_label'   => true,
					),

					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Orientation", 'mascot-core' ),
						"param_name"	=> "orientation",
						"description"	=> esc_html__( "Orientation of the before and after images ('horizontal' or 'vertical')", 'mascot-core' ),
						'value'			=> array(
							__( "Horizontal", 'mascot-core' )   => 'horizontal',
							__( "Vertical", 'mascot-core' )	 => 'vertical',
						),
						'save_always'   => true,
						'admin_label'   => true,
						'std'			=> 'horizontal',
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Custom Before Label", 'mascot-core' ),
						"param_name"	=> "before_label",
						"description"	=> esc_html__( "Default custom before label: 'Before'", 'mascot-core' ),
						'save_always'   => true,
						'admin_label'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Custom After Label", 'mascot-core' ),
						"param_name"	=> "after_label",
						"description"	=> esc_html__( "Default custom after label: 'After'", 'mascot-core' ),
						'save_always'   => true,
						'admin_label'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Offset Percentage", 'mascot-core' ),
						"param_name"	=> "default_offset_pct",
						"description"	=> esc_html__( "How much of the before image is visible when the page loads(value must be between 0 to 1)", 'mascot-core' ),
						'save_always'   => true,
						'admin_label'   => true,
						'std'			=> '0.5',
					),
					array(
						'type'			=> 'checkbox',
						"heading"		=> esc_html__( "No Overlay", 'mascot-core' ),
						"param_name"	=> "no_overlay",
						"description"	=> esc_html__( "Do not show the overlay with before and after", 'mascot-core' ),
						'save_always'   => true,
						'admin_label'   => true,
						'std'			=> true,
					),


				)
			);
			vc_map( $vc_map );

			//Modify vc_map from themes
			if( function_exists('mascot_core_sc_before_after_slider_vc_map_modifier') ) {
				mascot_core_sc_before_after_slider_vc_map_modifier( $this->shortcode_base );
			}
		}
	}

	/**
	 * Renders shortcodes HTML
	 */
	public function render( $attr, $content = null ) {

		//Render Shortcode from themes
		if( function_exists('mascot_core_sc_before_after_slider_render') ) {
			return mascot_core_sc_before_after_slider_render( $attr, $content );
		}
		
	}
}