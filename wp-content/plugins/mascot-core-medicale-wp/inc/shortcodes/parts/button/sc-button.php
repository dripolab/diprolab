<?php
namespace MASCOTCORE\Shortcodes\Button;

use MASCOTCORE\Shortcodes\Lib;

/**
 * class SC_Button
 * @package MASCOTCORE\Shortcodes\Button;
 */
class SC_Button implements Lib\Mascot_Core_Interface_Shortcodes {
	
	/**
	 * @var shortcode_base
	 */
	private $shortcode_base;

	/**
	 * construct
	 */
	public function __construct() {
		$this->shortcode_base = 'tm_button';
		add_action('vc_before_init', array($this, 'MapShortCodeVC'));
	}

	/**
	 * Returns shortcode_base for shortcode
	 */
	public function getShortCodeBase() {
		return $this->shortcode_base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 */
	public function MapShortCodeVC() {
		if(function_exists('vc_map')) {
			$group_icon_options = 'Icon Options';

			$vc_map = array(
				'name'		=> esc_html__( 'Button', 'mascot-core' ),
				'base'		=>$this->shortcode_base,
				'category'	=> 'by TM',
				'icon'		=> 'mascot-vc-icons vc-icon-cta',
				'allowed_container_element' => 'vc_row',
				'params'	=>array(
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Custom CSS class", 'mascot-core' ),
						"param_name"	=> "custom_css_class",
						"description"	=> esc_html__( 'To style particular content element.', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Button Text", 'mascot-core' ),
						"param_name"	=> "button_text",
						"description"	=> esc_html__( 'Ex: Read More', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'vc_link',
						'heading'		=> esc_html__( 'URL (Link)', 'mascot-core' ),
						'param_name'	=> 'link_url',
						'description'   => esc_html__( 'Add link to button.', 'mascot-core' ),
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Design Style", 'mascot-core' ),
						"param_name"	=> "design_style",
						"description"	=> "",
						'value'			=> array(
							'Button Default' => 'btn-default',
							'Button Border'  => 'btn-border',
							'Button Dark'	=> 'btn-dark',
							'Button Gray'	=> 'btn-gray'
						),
						'save_always'   => true,
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Button Size", 'mascot-core' ),
						"param_name"	=> "button_size",
						"description"	=> "",
						'value'			=> array(
							'Default'		=> '',
							'Extra Small'   => 'btn-xs',
							'Small'		 => 'btn-sm',
							'Large'		 => 'btn-lg',
							'Extra Large'   => 'btn-xl',
						),
						'save_always'   => true,
						'admin_label'   => true,
					),
					array(
						'type'			=> 'checkbox',
						"heading"		=> esc_html__( "Make Button Theme Colored?", 'mascot-core' ),
						"param_name"	=> "btn_theme_colored",
						'description'   => '',
					),
					array(
						'type'			=> 'checkbox',
						"heading"		=> esc_html__( "Make Fullwidth (Block Level Button)", 'mascot-core' ),
						"param_name"	=> "btn_block",
						'description'   => '',
					),
					array(
						'type'			=> 'checkbox',
						"heading"		=> esc_html__( "3D Effect", 'mascot-core' ),
						"param_name"	=> "threed_effect",
						'description'   => '',
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Button Alignment", 'mascot-core' ),
						"param_name"	=> "button_alignment",
						"description"	=> "",
						'value'			=> array(
							'Left'	  => 'text-left',
							'Center'	=> 'text-center',
							'Right'	 => 'text-right'
						),
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Wow Animation Type", 'mascot-core' ),
						"param_name"	=> "button_wow_animation",
						"description"	=> "",
						'value'			=> mascot_core_animate_css_animation_list(),
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Wow Animation Duration", 'mascot-core' ),
						"param_name"	=> "button_wow_animation_duration",
						"description"	=> esc_html__( 'Enter Animation Duration(eg: 2s).', 'mascot-core' ),
					),


					//Icon Options
					array(
						'type'			=> 'checkbox',
						"heading"		=> esc_html__( "Add icon?", 'mascot-core' ),
						"param_name"	=> "add_icon",
						'description'   => '',
						'group'			=> $group_icon_options
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Icon Left", 'mascot-core' ),
						"param_name"	=> "button_icon_left",
						"description"	=> esc_html__( 'Icon to the left of your button text. Eg: fa fa-arrow-right', 'mascot-core' ),
						'dependency'	=> array('element' => 'add_icon', 'value' => 'true'),
						'group'			=> $group_icon_options
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Icon Right", 'mascot-core' ),
						"param_name"	=> "button_icon_right",
						"description"	=> esc_html__( 'Icon to the right of your button text. Eg: fa fa-arrow-right', 'mascot-core' ),
						'dependency'	=> array('element' => 'add_icon', 'value' => 'true'),
						'group'			=> $group_icon_options
					),

				)
			);
			vc_map( $vc_map );

			//Modify vc_map from themes
			if( function_exists('mascot_core_sc_button_vc_map_modifier') ) {
				mascot_core_sc_button_vc_map_modifier( $this->shortcode_base );
			}
		}
	}

	/**
	 * Renders shortcodes HTML
	 */
	public function render( $attr, $content = null ) {

		//Render Shortcode from themes
		if( function_exists('mascot_core_sc_button_render') ) {
			return mascot_core_sc_button_render( $attr, $content );
		}
		
	}
}