<?php
namespace MASCOTCORE\Shortcodes\SectionTitle;

use MASCOTCORE\Shortcodes\Lib;

/**
 * class SC_Section_Title
 * @package MASCOTCORE\Shortcodes\SectionTitle;
 */
class SC_Section_Title implements Lib\Mascot_Core_Interface_Shortcodes {
	
	/**
	 * @var shortcode_base
	 */
	private $shortcode_base;

	/**
	 * construct
	 */
	public function __construct() {
		$this->shortcode_base = 'tm_section_title';
		add_action('vc_before_init', array($this, 'MapShortCodeVC'));
	}

	/**
	 * Returns shortcode_base for shortcode
	 */
	public function getShortCodeBase() {
		return $this->shortcode_base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 */
	public function MapShortCodeVC() {
		$group_wow_options = 'Wow Animation Options';
		$title_options = 'Title Options';

		if(function_exists('vc_map')) {
			$vc_map = array(
				'name'		=> esc_html__( 'Section Title', 'mascot-core' ),
				'base'		=>$this->shortcode_base,
				'category'	=> 'by TM',
				'icon'		=> 'mascot-vc-icons vc-icon-cta',
				'allowed_container_element' => 'vc_row',
				'params'	=>array(
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Custom CSS class", 'mascot-core' ),
						"param_name"	=> "custom_css_class",
						"description"	=> esc_html__( 'To style particular content element.', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Title Text", 'mascot-core' ),
						"param_name"	=> "title_text",
						"description"	=> "",
						'admin_label'   => true,
					),
					array(
						'type'			=> 'textarea',
						"heading"		=> esc_html__( "Sub Title (Paragraph)", 'mascot-core' ),
						"param_name"	=> "subtitle_paragraph",
						"description"	=> esc_html__( 'Paragraph that will be placed below the title.', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Design Style", 'mascot-core' ),
						"param_name"	=> "design_style",
						"description"	=> "",
						'value'			=> array(
							'Style 1'   => 'style1',
							'Style 2'   => 'style2',
							'Style 3'   => 'style3'
						),
						'save_always'   => true,
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Text Alignment", 'mascot-core' ),
						"param_name"	=> "text_alignment",
						"description"	=> "",
						'value'			=> mascot_core_text_alignment_list(),
						'save_always'   => true,
						'admin_label'   => true,
					),


					//Title Options
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Title Tag", 'mascot-core' ),
						"param_name"	=> "title_tag",
						"description"	=> "",
						'value'			=> mascot_core_heading_tag_list(),
						'admin_label'   => true,
						'group'			=> $title_options
					),
					array(
						'type'			=> 'colorpicker',
						"heading"		=> esc_html__( "Text Color", 'mascot-core' ),
						"param_name"	=> "title_text_color",
						"value"		 => '',
						'group'			=> $title_options
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Font Size (px)", 'mascot-core' ),
						"param_name"	=> "title_font_size",
						"description"	=> "",
						'group'			=> $title_options
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Line Height (px)", 'mascot-core' ),
						"param_name"	=> "title_line_height",
						"description"	=> "",
						'group'			=> $title_options
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Letter Spacing (px)", 'mascot-core' ),
						"param_name"	=> "title_letter_spacing",
						"description"	=> "",
						'group'			=> $title_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Font Style", 'mascot-core' ),
						"param_name"	=> "title_font_style",
						"description"	=> "",
						'value'			=> mascot_core_vc_font_style_list(),
						'group'			=> $title_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Font Weight", 'mascot-core' ),
						"param_name"	=> "title_font_weight",
						"description"	=> "",
						'value'			=> mascot_core_vc_font_weight_list(),
						'group'			=> $title_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Text Transform", 'mascot-core' ),
						"param_name"	=> "title_text_transform",
						"description"	=> "",
						'value'			=> mascot_core_vc_text_transform_list(),
						'group'			=> $title_options
					),

					//Wow Animation Options
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Title - Wow Animation Type", 'mascot-core' ),
						"param_name"	=> "title_wow_animation",
						"description"	=> "",
						'value'			=> mascot_core_animate_css_animation_list(),
						'group'			=> $group_wow_options
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Title - Wow Animation Duration", 'mascot-core' ),
						"param_name"	=> "title_wow_animation_duration",
						"description"	=> esc_html__( 'Enter Animation Duration(eg: 2s).', 'mascot-core' ),
						'group'			=> $group_wow_options
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Title - Wow Animation Delay", 'mascot-core' ),
						"param_name"	=> "title_wow_animation_delay",
						"description"	=> esc_html__( 'Enter Animation Delay(eg: 5s).', 'mascot-core' ),
						'group'			=> $group_wow_options
					),

					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Sub Title - Wow Animation Type", 'mascot-core' ),
						"param_name"	=> "subtitle_wow_animation",
						"description"	=> "",
						'value'			=> mascot_core_animate_css_animation_list(),
						'group'			=> $group_wow_options
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Sub Title - Wow Animation Duration", 'mascot-core' ),
						"param_name"	=> "subtitle_wow_animation_duration",
						"description"	=> esc_html__( 'Enter Animation Duration(eg: 2s).', 'mascot-core' ),
						'group'			=> $group_wow_options
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Sub Title - Wow Animation Delay", 'mascot-core' ),
						"param_name"	=> "subtitle_wow_animation_delay",
						"description"	=> esc_html__( 'Enter Animation Delay(eg: 5s).', 'mascot-core' ),
						'group'			=> $group_wow_options
					)

				)
			);
			vc_map( $vc_map );

			//Modify vc_map from themes
			if( function_exists('mascot_core_sc_section_title_vc_map_modifier') ) {
				mascot_core_sc_section_title_vc_map_modifier( $this->shortcode_base );
			}
		}
	}

	/**
	 * Renders shortcodes HTML
	 */
	public function render( $attr, $content = null ) {

		//Render Shortcode from themes
		if( function_exists('mascot_core_sc_section_title_render') ) {
			return mascot_core_sc_section_title_render( $attr, $content );
		}
		
	}
}