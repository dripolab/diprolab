<?php

//load lib
require_once $plugin_dir_path . 'inc/shortcodes/lib/interface-shortcodes.php';


/* Loads all shortcodes located in shortcodes folder
================================================== */
if( !function_exists('mascot_core_load_all_shortcodes') ) {
	function mascot_core_load_all_shortcodes() {
		global $plugin_dir_path;
		foreach( glob($plugin_dir_path.'inc/shortcodes/parts/*/loader.php') as $each_sc_loader ) {
			require_once $each_sc_loader;
		}
		require_once $plugin_dir_path . 'inc/shortcodes/parts/reg-shortcodes.php';
	}
	add_action('after_setup_theme', 'mascot_core_load_all_shortcodes');
}


/**
 * Shortcode current-year
 */
add_shortcode('current-year', 'mascot_core_shortcode_current_year');
function mascot_core_shortcode_current_year() {
	return date('Y');
}