<?php
namespace MASCOTCORE\Shortcodes\Lib;

/**
 * interface Mascot_Core_Interface_Shortcodes
 * @package MASCOTCORE\Shortcodes\Lib;
 */
interface Mascot_Core_Interface_Shortcodes {
	/**
	 * Returns shortcode_base for shortcode
	 */
	public function getShortCodeBase();

	/**
	 * Maps shortcode to Visual Composer
	 */
	public function MapShortCodeVC();

	/**
	 * Renders shortcodes HTML
	 */
	public function render($attr, $content = null);
}