<?php
namespace MASCOTCORE\CPT\Staff;

use MASCOTCORE\Lib;

/**
 * Singleton class
 * class CPT_Staff
 * @package MASCOTCORE\CPT\Staff;
 */
final class CPT_Staff implements Lib\Mascot_Core_Interface_PostType {
	
	/**
	 * @var string
	 */
	public 	$ptKey;
	public 	$ptKeyRewriteBase;
	public  $ptTaxKey;
	public  $ptTaxKeyRewriteBase;
	public  $social_list;
	private $ptMenuIcon;
	private $ptSingularName;
	private $ptPluralName;

	/**
	 * Call this method to get singleton
	 *
	 * @return CPT_Staff
	 */
	public static function Instance()
	{
		static $inst = null;
		if ($inst === null) {
			$inst = new CPT_Staff();
		}
		return $inst;
	}
	
	/**
	 * Private ctor so nobody else can instance it
	 *
	 */
	private function __construct() {
		$this->ptSingularName = esc_html__( 'Staff Item', 'mascot-core' );
		$this->ptPluralName = esc_html__( 'Staff', 'mascot-core' );
		$this->ptKey = 'staff-items';
		$this->ptKeyRewriteBase = $this->ptKey;
		$this->ptTaxKey = 'staff_category';
		$this->ptTaxKeyRewriteBase = str_replace( '_', '-', $this->ptTaxKey );
		$this->ptMenuIcon = 'dashicons-id';
		$this->social_list = $this->socialList();
		add_filter( 'manage_edit-'.$this->ptKey.'_columns', array($this, 'customColumnsSettings') ) ;
		add_filter( 'manage_'.$this->ptKey.'_posts_custom_column', array($this, 'customColumnsContent') ) ;
		add_filter( 'rwmb_meta_boxes', array($this, 'regMetaBoxes') ) ;
	}

	/**
	 * @return string
	 */
	public function getPTKey() {
		return $this->ptKey;
	}

	/**
	 * @return string
	 */
	public function getPTTaxKey() {
		return $this->ptTaxKey;
	}

	/**
	 * registers custom post type & Tax
	 */
	public function register() {
		if ( class_exists( 'ReduxFramework' ) ) {
			$cpt_enable = mascot_core_get_redux_option( 'cpt-settings-staff-enable' );
			if( ! $cpt_enable ) {
				return;
			}
			$this->ptPluralName = mascot_core_get_redux_option( 'cpt-settings-staff-label', esc_html__( 'Staff', 'mascot-core' ) );
			$this->ptMenuIcon = mascot_core_get_redux_option( 'cpt-settings-staff-admin-dashicon', $this->ptMenuIcon );
			$this->ptKeyRewriteBase = mascot_core_get_redux_option( 'cpt-settings-staff-slug', $this->ptKeyRewriteBase );

		}
		
		$this->registerCustomPostType();
		$this->registerCustomTax();
	}
	
	/**
	 * Regsiters custom post type
	 */
	private function registerCustomPostType() {

		$labels = array(
			'name'					=> $this->ptPluralName,
			'singular_name'			=> $this->ptPluralName . ' ' . esc_html__( 'Item', 'mascot-core' ),
			'menu_name'				=> $this->ptPluralName,
			'name_admin_bar'		=> $this->ptPluralName,
			'archives'				=> esc_html__( 'Item Archives', 'mascot-core' ),
			'attributes'			=> esc_html__( 'Item Attributes', 'mascot-core' ),
			'parent_item_colon'		=> esc_html__( 'Parent Item:', 'mascot-core' ),
			'all_items'				=> esc_html__( 'All Items', 'mascot-core' ),
			'add_new_item'			=> esc_html__( 'Add New Item', 'mascot-core' ),
			'add_new'				=> esc_html__( 'Add New', 'mascot-core' ),
			'new_item'				=> esc_html__( 'New Item', 'mascot-core' ),
			'edit_item'				=> esc_html__( 'Edit Item', 'mascot-core' ),
			'update_item'			=> esc_html__( 'Update Item', 'mascot-core' ),
			'view_item'				=> esc_html__( 'View Item', 'mascot-core' ),
			'view_items'			=> esc_html__( 'View Items', 'mascot-core' ),
			'search_items'			=> esc_html__( 'Search Item', 'mascot-core' ),
			'not_found'				=> esc_html__( 'Not found', 'mascot-core' ),
			'not_found_in_trash'	=> esc_html__( 'Not found in Trash', 'mascot-core' ),
			'featured_image'		=> esc_html__( 'Featured Image', 'mascot-core' ),
			'set_featured_image'	=> esc_html__( 'Set featured image', 'mascot-core' ),
			'remove_featured_image'	=> esc_html__( 'Remove featured image', 'mascot-core' ),
			'use_featured_image'	=> esc_html__( 'Use as featured image', 'mascot-core' ),
			'insert_into_item'		=> esc_html__( 'Insert into item', 'mascot-core' ),
			'uploaded_to_this_item'	=> esc_html__( 'Uploaded to this item', 'mascot-core' ),
			'items_list'			=> esc_html__( 'Items list', 'mascot-core' ),
			'items_list_navigation'	=> esc_html__( 'Items list navigation', 'mascot-core' ),
			'filter_items_list'		=> esc_html__( 'Filter items list', 'mascot-core' ),
		);

		$args = array(
			'label'						=> $this->ptSingularName,
			'description'				=> esc_html__( 'Post Type Description', 'mascot-core' ),
			'labels'					=> $labels,
			'supports'					=> array( 'title', 'thumbnail', 'editor', ),
			'taxonomies'				=> array( $this->ptTaxKey, 'post_tag' ),
			'hierarchical'				=> false,
			'public'					=> true,
			'show_ui'					=> true,
			'show_in_menu'				=> true,
			'menu_position'				=> 30,
			'menu_icon'					=> $this->ptMenuIcon,
			'show_in_admin_bar'			=> true,
			'show_in_nav_menus'			=> true,
			'can_export'				=> true,
			'has_archive'				=> true,
			'exclude_from_search'		=> false,
			'publicly_queryable'		=> true,
			'capability_type'			=> 'page',
			'rewrite'					=> array( 'slug' => $this->ptKeyRewriteBase, 'with_front' => false ),
		);
		register_post_type( $this->ptKey, $args );

	}
	
	/**
	 * Regsiters custom Taxonomy
	 */
	private function registerCustomTax() {

		$labels = array(
			'name'						=> _x( 'Staff Categories', 'Taxonomy General Name', 'mascot-core' ),
			'singular_name'				=> _x( 'Staff Category', 'Taxonomy Singular Name', 'mascot-core' ),
			'menu_name'					=> $this->ptPluralName . esc_html__( ' Categories', 'mascot-core' ),
			'all_items'					=> esc_html__( 'All ', 'mascot-core' ) . $this->ptSingularName . esc_html__( ' Categories', 'mascot-core' ),
			'parent_item'				=> esc_html__( 'Parent Item', 'mascot-core' ),
			'parent_item_colon'			=> esc_html__( 'Parent Item:', 'mascot-core' ),
			'new_item_name'				=> esc_html__( 'New ', 'mascot-core' ) . $this->ptSingularName . esc_html__( ' Category', 'mascot-core' ),
			'add_new_item'				=> esc_html__( 'Add ', 'mascot-core' ) . $this->ptSingularName . esc_html__( ' Category', 'mascot-core' ),
			'edit_item'					=> esc_html__( 'Edit ', 'mascot-core' ) . $this->ptSingularName . esc_html__( ' Category', 'mascot-core' ),
			'update_item'				=> esc_html__( 'Update ', 'mascot-core' ) . $this->ptSingularName . esc_html__( ' Category', 'mascot-core' ),
			'view_item'					=> esc_html__( 'View ', 'mascot-core' ) . $this->ptSingularName . esc_html__( ' Category', 'mascot-core' ),
			'separate_items_with_commas'=> esc_html__( 'Separate items with commas', 'mascot-core' ),
			'add_or_remove_items'		=> esc_html__( 'Add or remove items', 'mascot-core' ),
			'choose_from_most_used'		=> esc_html__( 'Choose from the most used', 'mascot-core' ),
			'popular_items'				=> esc_html__( 'Popular Items', 'mascot-core' ),
			'search_items'				=> esc_html__( 'Search Items', 'mascot-core' ),
			'not_found'					=> esc_html__( 'Not Found', 'mascot-core' ),
			'no_terms'					=> esc_html__( 'No items', 'mascot-core' ),
			'items_list'				=> esc_html__( 'Items list', 'mascot-core' ),
			'items_list_navigation'		=> esc_html__( 'Items list navigation', 'mascot-core' ),
		);
		$args = array(
			'labels'					=> $labels,
			'hierarchical'				=> true,
			'public'					=> true,
			'show_ui'					=> true,
			'show_admin_column'			=> true,
			'show_in_nav_menus'			=> true,
			'show_tagcloud'				=> true,
			'rewrite'					=> array( 'slug' => $this->ptTaxKeyRewriteBase, 'with_front' => false ),
		);
		register_taxonomy( $this->ptTaxKey, array( $this->ptKey ), $args );
	}
	
	/**
	 * Custom Columns Settings
	 */
	public function customColumnsSettings( $columns ) {

		$columns = array(
			'cb'			=> '<input type="checkbox" />',
			'title'			=> esc_html__( 'Title', 'mascot-core' ),
			'name'			=> esc_html__( 'Name', 'mascot-core' ),
			'thumbnail'		=> esc_html__( 'Thumbnail', 'mascot-core' ),
			'category'		=> esc_html__( 'Category', 'mascot-core' ),
			'date'			=> esc_html__( 'Date', 'mascot-core' ),
		);
		return $columns;
	}

	/**
	 * Custom Columns Content
	 */
	public function customColumnsContent( $columns ) {
		global $post;
		switch( $columns ) {
			case 'category' :
				$post_terms = array();
				$taxonomy 	= $this->ptTaxKey;
				$post_type 	= get_post_type( $post->ID );
				$terms 		= get_the_terms( $post->ID, $taxonomy );

				if ( ! empty( $terms ) ) {
					foreach ( $terms as $term ) {
						$post_terms[] = "<a href='edit.php?post_type={$post_type}&{$taxonomy}={$term->slug}'> " . esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, $taxonomy, 'edit' ) ) . "</a>";
					}
					echo join( ', ', $post_terms );
				}
				else echo '<i>No categories.</i>';
			break;

			case 'name' :
				echo rwmb_meta( 'medicale_mascot_' . "cpt_staff_name", '', $post->ID );
			break;

			case 'thumbnail' :
				echo get_the_post_thumbnail( $post->ID, array( 64, 64 ) );
			break;

			default :
			break;
		}
	}

	/**
	 * socialList
	 */
	public function socialList() {
		$social_list = array(
			'dribbble'  => esc_html__( 'Dribble', 'mascot-core' ),
			'facebook'  => esc_html__( 'Facebook', 'mascot-core' ),
			'flickr'  => esc_html__( 'Flickr', 'mascot-core' ),
			'google-plus'  => esc_html__( 'Google Plus', 'mascot-core' ),
			'instagram'  => esc_html__( 'Instagram', 'mascot-core' ),

			'linkedin'  => esc_html__( 'Linkedin', 'mascot-core' ),
			'pinterest'  => esc_html__( 'Pinterest', 'mascot-core' ),
			'rss'  => esc_html__( 'RSS', 'mascot-core' ),
			'skype'  => esc_html__( 'Skype', 'mascot-core' ),
			'tumblr'  => esc_html__( 'Tumblr', 'mascot-core' ),
			
			'twitter'  => esc_html__( 'Twitter', 'mascot-core' ),
			'vimeo-square'  => esc_html__( 'Vimeo', 'mascot-core' ),
			'vine'  => esc_html__( 'Vine', 'mascot-core' ),
			'wordpress'  => esc_html__( 'Wordpress', 'mascot-core' ),
			'youtube'  => esc_html__( 'Youtube', 'mascot-core' ),
		);
		return $social_list;
	}

	/**
	 * Registers Meta Boxes
	 */
	public function regMetaBoxes( $meta_boxes ) {
		//social list array
		$social_list_array = array();
		foreach ($this->social_list as $key => $value) {
			# code...
			if( $key == 'facebook' ) {
				$social_list_array[] = array(
					'id'	=> 'medicale_mascot_' . 'cpt_staff_social_'.$key,
					'name'		=> $value,
					'desc'	=> esc_html__( 'Example: https://www.facebook.com/envato/', 'mascot-core' ),
					'type'	=> 'text',
					'tab'	=> 'social-info',
				);

			} else {
				$social_list_array[] = array(
					'id'	=> 'medicale_mascot_' . 'cpt_staff_social_'.$key,
					'name'		=> $value,
					'type'	=> 'text',
					'tab'	=> 'social-info',
				);
			}
		}


		// Meta Box Settings for this Page
		$meta_boxes[] = array(
			'id'		 => 'staff_meta_box',
			'title'	  => esc_html__( 'Staff Meta Box', 'mascot-core' ),
			'post_types' => $this->ptKey,
			'priority'   => 'high',

			// List of tabs, in one of the following formats:
			// 1) key => label
			// 2) key => array( 'label' => Tab label, 'icon' => Tab icon )
			'tabs'	  => array(
				'general-info' => array(
					'label' => esc_html__( 'General Info', 'mascot-core' ),
					'icon'  => 'dashicons-update', // Dashicon
				),
				'working-hours' => array(
					'label' => esc_html__( 'Working Hours', 'mascot-core' ),
					'icon'  => 'dashicons-admin-home', // Dashicon
				),
				'social-info' => array(
					'label' => esc_html__( 'Social Info', 'mascot-core' ),
					'icon'  => 'dashicons-admin-home', // Dashicon
				),
			),

			// Tab style: 'default', 'box' or 'left'. Optional
			'tab_style' => 'left',
			
			// Show meta box wrapper around tabs? true (default) or false. Optional
			'tab_wrapper' => true,

			'fields'	 => array_merge(
				array(
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_name',
						'name'	=> esc_html__( 'Name', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'general-info',
					),
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_speciality',
						'name'	=> esc_html__( 'Speciality', 'mascot-core' ),
						'type'	=> 'wysiwyg',
						'options' => array(
							'media_buttons' => false,
							'textarea_rows' => '4',
						),
						'tab'	=> 'general-info',
					),
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_short_bio',
						'name'	=> esc_html__( 'Short Bio:', 'mascot-core' ),
						'type'	=> 'wysiwyg',
						'options' => array(
							'media_buttons' => false,
							'textarea_rows' => '4',
						),
						'tab'	=> 'general-info',
					),
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_address',
						'name'	=> esc_html__( 'Address:', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'general-info',
					),
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_phone',
						'name'	=> esc_html__( 'Phone:', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'general-info',
					),
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_fax',
						'name'	=> esc_html__( 'Fax:', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'general-info',
					),
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_email',
						'name'	=> esc_html__( 'Email:', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'general-info',
					),
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_appointment_url',
						'name'	=> esc_html__( 'Request an Appointment URL:', 'mascot-core' ),
						'std'	=> esc_html__( '#', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'general-info',
					),

					//opening hours
					//Day 1
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_opening_hours_day_1',
						'name'	=> esc_html__( 'Day 1:', 'mascot-core' ),
						'std'	=> esc_html__( 'Monday - Friday', 'mascot-core' ),
						'desc'  => esc_html__( 'Example: Monday - Friday', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'working-hours',
					),
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_opening_hours_day_1_time',
						'name'	=> esc_html__( 'Time for Day 1:', 'mascot-core' ),
						'std'	=> esc_html__( '8.00 - 17.00', 'mascot-core' ),
						'desc'  => esc_html__( 'Example: 8.00 - 17.00', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'working-hours',
					),
					//Day 2
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_opening_hours_day_2',
						'name'	=> esc_html__( 'Day 2:', 'mascot-core' ),
						'std'	=> esc_html__( 'Saturday', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'working-hours',
					),
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_opening_hours_day_2_time',
						'name'	=> esc_html__( 'Time for Day 2:', 'mascot-core' ),
						'std'	=> esc_html__( '9.00 - 16.00', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'working-hours',
					),
					//Day 3
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_opening_hours_day_3',
						'name'	=> esc_html__( 'Day 3:', 'mascot-core' ),
						'std'	=> esc_html__( 'Friday', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'working-hours',
					),
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_opening_hours_day_3_time',
						'name'	=> esc_html__( 'Time for Day 3:', 'mascot-core' ),
						'std'	=> esc_html__( '9.30 - 14.00', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'working-hours',
					),
					//Day 4
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_opening_hours_day_4',
						'name'	=> esc_html__( 'Day 4:', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'working-hours',
					),
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_opening_hours_day_4_time',
						'name'	=> esc_html__( 'Time for Day 4:', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'working-hours',
					),
					//Day 5
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_opening_hours_day_5',
						'name'	=> esc_html__( 'Day 5:', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'working-hours',
					),
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_opening_hours_day_5_time',
						'name'	=> esc_html__( 'Time for Day 5:', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'working-hours',
					),
					//Day 6
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_opening_hours_day_6',
						'name'	=> esc_html__( 'Day 6:', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'working-hours',
					),
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_opening_hours_day_6_time',
						'name'	=> esc_html__( 'Time for Day 6:', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'working-hours',
					),
					//Day 7
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_opening_hours_day_7',
						'name'	=> esc_html__( 'Day 7:', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'working-hours',
					),
					array(
						'id'	=> 'medicale_mascot_' . 'cpt_staff_opening_hours_day_7_time',
						'name'	=> esc_html__( 'Time for Day 7:', 'mascot-core' ),
						'type'	=> 'text',
						'tab'	=> 'working-hours',
					),
				),
				$social_list_array
			)
		);

		return $meta_boxes;
	}

}
