<?php
namespace MASCOTCORE\CPT\Services;

use MASCOTCORE\Lib;

/**
 * Singleton class
 * class CPT_Services
 * @package MASCOTCORE\CPT\Services;
 */
final class CPT_Services implements Lib\Mascot_Core_Interface_PostType {
	
	/**
	 * @var string
	 */
	public  $ptKey;
	public 	$ptKeyRewriteBase;
	public  $ptTaxKey;
	public  $ptTaxKeyRewriteBase;
	private $ptMenuIcon;
	private $ptSingularName;
	private $ptPluralName;

	/**
	 * Call this method to get singleton
	 *
	 * @return CPT_Services
	 */
	public static function Instance()
	{
		static $inst = null;
		if ($inst === null) {
			$inst = new CPT_Services();
		}
		return $inst;
	}
	
	/**
	 * Private ctor so nobody else can instance it
	 *
	 */
	private function __construct() {
		$this->ptSingularName = esc_html__( 'Services Item', 'mascot-core' );
		$this->ptPluralName = esc_html__( 'Services', 'mascot-core' );
		$this->ptKey = 'services';
		$this->ptKeyRewriteBase = $this->ptKey;
		$this->ptTaxKey = 'services_category';
		$this->ptTaxKeyRewriteBase = str_replace( '_', '-', $this->ptTaxKey );
		$this->ptMenuIcon = 'dashicons-hammer';
		add_filter( 'manage_edit-'.$this->ptKey.'_columns', array($this, 'customColumnsSettings') ) ;
		add_filter( 'manage_'.$this->ptKey.'_posts_custom_column', array($this, 'customColumnsContent') ) ;
		add_filter( 'rwmb_meta_boxes', array($this, 'regMetaBoxes') ) ;
	}

	/**
	 * @return string
	 */
	public function getPTKey() {
		return $this->ptKey;
	}

	/**
	 * @return string
	 */
	public function getPTTaxKey() {
		return $this->ptTaxKey;
	}

	/**
	 * registers custom post type & Tax
	 */
	public function register() {
		if ( class_exists( 'ReduxFramework' ) ) {
			$cpt_enable = mascot_core_get_redux_option( 'cpt-settings-services-enable' );
			if( ! $cpt_enable ) {
				return;
			}
			$this->ptPluralName = mascot_core_get_redux_option( 'cpt-settings-services-label', esc_html__( 'Services', 'mascot-core' ) );
			$this->ptMenuIcon = mascot_core_get_redux_option( 'cpt-settings-services-admin-dashicon', $this->ptMenuIcon );

		}
		
		$this->registerCustomPostType();
		$this->registerCustomTax();
	}
	
	/**
	 * Regsiters custom post type
	 */
	private function registerCustomPostType() {

		$labels = array(
			'name'					=> $this->ptPluralName,
			'singular_name'			=> $this->ptPluralName . ' ' . esc_html__( 'Item', 'mascot-core' ),
			'menu_name'				=> $this->ptPluralName,
			'name_admin_bar'		=> $this->ptPluralName,
			'archives'				=> esc_html__( 'Item Archives', 'mascot-core' ),
			'attributes'			=> esc_html__( 'Item Attributes', 'mascot-core' ),
			'parent_item_colon'		=> esc_html__( 'Parent Item:', 'mascot-core' ),
			'all_items'				=> esc_html__( 'All Items', 'mascot-core' ),
			'add_new_item'			=> esc_html__( 'Add New Item', 'mascot-core' ),
			'add_new'				=> esc_html__( 'Add New', 'mascot-core' ),
			'new_item'				=> esc_html__( 'New Item', 'mascot-core' ),
			'edit_item'				=> esc_html__( 'Edit Item', 'mascot-core' ),
			'update_item'			=> esc_html__( 'Update Item', 'mascot-core' ),
			'view_item'				=> esc_html__( 'View Item', 'mascot-core' ),
			'view_items'			=> esc_html__( 'View Items', 'mascot-core' ),
			'search_items'			=> esc_html__( 'Search Item', 'mascot-core' ),
			'not_found'				=> esc_html__( 'Not found', 'mascot-core' ),
			'not_found_in_trash'	=> esc_html__( 'Not found in Trash', 'mascot-core' ),
			'featured_image'		=> esc_html__( 'Featured Image', 'mascot-core' ),
			'set_featured_image'	=> esc_html__( 'Set featured image', 'mascot-core' ),
			'remove_featured_image'	=> esc_html__( 'Remove featured image', 'mascot-core' ),
			'use_featured_image'	=> esc_html__( 'Use as featured image', 'mascot-core' ),
			'insert_into_item'		=> esc_html__( 'Insert into item', 'mascot-core' ),
			'uploaded_to_this_item'	=> esc_html__( 'Uploaded to this item', 'mascot-core' ),
			'items_list'			=> esc_html__( 'Items list', 'mascot-core' ),
			'items_list_navigation'	=> esc_html__( 'Items list navigation', 'mascot-core' ),
			'filter_items_list'		=> esc_html__( 'Filter items list', 'mascot-core' ),
		);

		$args = array(
			'label'						=> $this->ptSingularName,
			'description'				=> esc_html__( 'Post Type Description', 'mascot-core' ),
			'labels'					=> $labels,
			'supports'					=> array( 'title', 'editor' ),
			'taxonomies'				=> array( $this->ptTaxKey, 'post_tag' ),
			'hierarchical'				=> false,
			'public'					=> true,
			'show_ui'					=> true,
			'show_in_menu'				=> true,
			'menu_position'				=> 30,
			'menu_icon'					=> $this->ptMenuIcon,
			'show_in_admin_bar'			=> true,
			'show_in_nav_menus'			=> true,
			'can_export'				=> true,
			'has_archive'				=> true,
			'exclude_from_search'		=> false,
			'publicly_queryable'		=> true,
			'capability_type'			=> 'page',
			'rewrite'					=> array( 'slug' => $this->ptKeyRewriteBase, 'with_front' => false ),
		);
		register_post_type( $this->ptKey, $args );

	}
	
	/**
	 * Regsiters custom Taxonomy
	 */
	private function registerCustomTax() {

		$labels = array(
			'name'						=> _x( 'Services Categories', 'Taxonomy General Name', 'mascot-core' ),
			'singular_name'				=> _x( 'Service Category', 'Taxonomy Singular Name', 'mascot-core' ),
			'menu_name'					=> $this->ptSingularName . esc_html__( ' Categories', 'mascot-core' ),
			'all_items'					=> esc_html__( 'All ', 'mascot-core' ) . $this->ptSingularName . esc_html__( ' Categories', 'mascot-core' ),
			'parent_item'				=> esc_html__( 'Parent Item', 'mascot-core' ),
			'parent_item_colon'			=> esc_html__( 'Parent Item:', 'mascot-core' ),
			'new_item_name'				=> esc_html__( 'New ', 'mascot-core' ) . $this->ptSingularName . esc_html__( ' Category', 'mascot-core' ),
			'add_new_item'				=> esc_html__( 'Add ', 'mascot-core' ) . $this->ptSingularName . esc_html__( ' Category', 'mascot-core' ),
			'edit_item'					=> esc_html__( 'Edit ', 'mascot-core' ) . $this->ptSingularName . esc_html__( ' Category', 'mascot-core' ),
			'update_item'				=> esc_html__( 'Update ', 'mascot-core' ) . $this->ptSingularName . esc_html__( ' Category', 'mascot-core' ),
			'view_item'					=> esc_html__( 'View ', 'mascot-core' ) . $this->ptSingularName . esc_html__( ' Category', 'mascot-core' ),
			'separate_items_with_commas'=> esc_html__( 'Separate items with commas', 'mascot-core' ),
			'add_or_remove_items'		=> esc_html__( 'Add or remove items', 'mascot-core' ),
			'choose_from_most_used'		=> esc_html__( 'Choose from the most used', 'mascot-core' ),
			'popular_items'				=> esc_html__( 'Popular Items', 'mascot-core' ),
			'search_items'				=> esc_html__( 'Search Items', 'mascot-core' ),
			'not_found'					=> esc_html__( 'Not Found', 'mascot-core' ),
			'no_terms'					=> esc_html__( 'No items', 'mascot-core' ),
			'items_list'				=> esc_html__( 'Items list', 'mascot-core' ),
			'items_list_navigation'		=> esc_html__( 'Items list navigation', 'mascot-core' ),
		);
		$args = array(
			'labels'					=> $labels,
			'hierarchical'				=> true,
			'public'					=> true,
			'show_ui'					=> true,
			'show_admin_column'			=> true,
			'show_in_nav_menus'			=> true,
			'show_tagcloud'				=> true,
			'rewrite'					=> array( 'slug' => $this->ptTaxKeyRewriteBase, 'with_front' => false ),
		);
		register_taxonomy( $this->ptTaxKey, array( $this->ptKey ), $args );
	}
	
	/**
	 * Custom Columns Settings
	 */
	public function customColumnsSettings( $columns ) {

		$columns = array(
			'cb'			=> '<input type="checkbox" />',
			'title'			=> esc_html__( 'Title', 'mascot-core' ),
			'thumbnail'		=> esc_html__( 'Thumbnail', 'mascot-core' ),
			'category'		=> esc_html__( 'Category', 'mascot-core' ),
			'date'			=> esc_html__( 'Date', 'mascot-core' ),
		);
		return $columns;
	}

	/**
	 * Custom Columns Content
	 */
	public function customColumnsContent( $columns ) {
		global $post;
		switch( $columns ) {
			case 'category' :
				$post_terms = array();
				$taxonomy 	= $this->ptTaxKey;
				$post_type 	= get_post_type( $post->ID );
				$terms 		= get_the_terms( $post->ID, $taxonomy );

				if ( ! empty( $terms ) ) {
					foreach ( $terms as $term ) {
						$post_terms[] = "<a href='edit.php?post_type={$post_type}&{$taxonomy}={$term->slug}'> " . esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, $taxonomy, 'edit' ) ) . "</a>";
					}
					echo join( ', ', $post_terms );
				}
				else echo '<i>No categories.</i>';
			break;

			case 'thumbnail' :
				$images = rwmb_meta( 'medicale_mascot_' . "cpt_services_image", 'size=thumbnail', $post->ID );
				if ( !empty( $images ) ) {
					foreach ( $images as $image ) {
						echo "<img src='{$image['url']}' alt='{$image['alt']}' />";
					}
				}
			break;

			default :
			break;
		}
	}

	/**
	 * Registers Meta Boxes
	 */
	public function regMetaBoxes( $meta_boxes ) {

		//font icon pack
		$fonticon_array = array();
		if( mascot_core_theme_installed() ) {
			$fonticon_array = medicale_mascot_get_metabox_icon_pack_array( 'medicale_mascot_' . 'cpt_service_iconpack', array( 'medicale_mascot_' . 'cpt_services_choose_image_or_icon', '=', 'icon' ));
		}

		$metabox_fields = array(
			array(
				'id'		=> 'medicale_mascot_' . 'cpt_services_choose_image_or_icon',
				'name'		=> esc_html__( 'Choose Image/Icon', 'mascot-core' ),
				'type'		=> 'select',
				'options' 	=> array(
					'image'  => esc_html__( 'Image', 'mascot-core' ),
					'icon'  => esc_html__( 'Icon', 'mascot-core' ),
				),
				'std'		=> 'image',
			),
			array(
				'id'		=> 'medicale_mascot_' . 'cpt_services_image',
				'name'		=> esc_html__( 'Choose Image', 'mascot-core' ),
				'type'		=> 'image_advanced',
				'max_file_uploads' => 1,
				'visible' 	=> array( 'medicale_mascot_' . 'cpt_services_choose_image_or_icon', '=', 'image' )
			),
			array(
				'id'		=> 'medicale_mascot_' . 'cpt_services_icon_font_size',
				'name'		=> esc_html__( 'Icon Font Size', 'mascot-core' ),
				'desc'		=> esc_html__( 'Example: 40px', 'mascot-core' ),
				'type'		=> 'text',
				'std'		=> '40px',
				'visible'	=> array( 'medicale_mascot_' . 'cpt_services_choose_image_or_icon', '=', 'icon' )
			),
			array(
				'id'		=> 'medicale_mascot_' . 'cpt_services_icon_color',
				'name'		=> esc_html__( 'Icon Font Color', 'mascot-core' ),
				'type'		=> 'color',
				'visible' 	=> array( 'medicale_mascot_' . 'cpt_services_choose_image_or_icon', '=', 'icon' )
			),
		);

		$merged_fields = array_merge($metabox_fields, $fonticon_array);

		$meta_boxes[] = array(
			'id'			=> 'services_meta_box',
			'title'			=> esc_html__( 'Services Meta Box', 'mascot-core' ),
			'post_types'	=> $this->ptKey,
			'priority'		=> 'high',
			'fields'		=> $merged_fields
		);
		return $meta_boxes;
	}

}