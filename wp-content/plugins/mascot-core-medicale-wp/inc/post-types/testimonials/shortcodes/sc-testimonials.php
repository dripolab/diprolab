<?php
namespace MASCOTCORE\CPT\Testimonials\Shortcodes;

use MASCOTCORE\Lib;
use MASCOTCORE\CPT\Testimonials\CPT_Testimonials;

/**
 * class SC_Testimonials
 * @package MASCOTCORE\CPT\Testimonials\Shortcodes;
 */
class SC_Testimonials implements Lib\Mascot_Core_Interface_PTShortcodes {
	
	/**
	 * @var string
	 */
	private $base;

	/**
	 * construct
	 */
	public function __construct() {
		if ( class_exists( 'ReduxFramework' ) ) {
			if( ! mascot_core_get_redux_option( 'cpt-settings-testimonials-enable' ) ) {
				return;
			}
		}
		$this->base = 'tmvc_testimonials';
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 *
	 * @see vc_map()
	 */
	public function vcMap() {
		if(function_exists('vc_map')) {
			$group_content_options = 'Content Options';
			$group_carousel_options = 'Carousel Options';
			$group_query_options = 'Query Options';
			$new_cpt_class = CPT_Testimonials::Instance();
			$categories_array = mascot_core_category_list_array_for_vc( $new_cpt_class->ptTaxKey );

			$orderby_parameters_list1 = mascot_core_orderby_parameters_list();
			$orderby_parameters_list2 = array(
				'Testimonial Author Name' => 'testimonial_author_name'
			);
			$orderby_parameters_list = array_merge( $orderby_parameters_list2, $orderby_parameters_list1 );


			$vc_map = array(
				'name'		=> esc_html__( 'Testimonials', 'mascot-core' ),
				'base'		=>$this->base,
				'category'	=> 'by TM',
				'icon'		=> 'mascot-vc-icons vc-icon-cta',
				'allowed_container_element' => 'vc_row',
				'params'	=>array(
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Custom CSS class", 'mascot-core' ),
						"param_name"	=> "custom_css_class",
						"description"	=> esc_html__( 'To style particular content element.', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Testimonials Type", 'mascot-core' ),
						"param_name"	=> "testimonials_type",
						"description"	=> "",
						'value'			=> array(
							'Grid'	  	=> 'grid',
							'Carousel'  => 'carousel'
						),
						'save_always'   => true,
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Columns Layout", 'mascot-core' ),
						"param_name"	=> "columns",
						"description"	=> esc_html__( 'Define Columns Layout for Grid/Carousel.', 'mascot-core' ),
						'value'			=> array(
							'1'  =>  '1',
							'2'  =>  '2',
							'3'  =>  '3',
							'4'  =>  '4',
						),
						'std'			=> 4,
						'admin_label'   => true,
					),



					//Carousel Options
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Navigation Arrow", 'mascot-core' ),
						"param_name"	=> "show_navigation",
						"description"	=> esc_html__( 'Show Left Right Navigation Arrow for Testimonial Carousel', 'mascot-core' ),
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'dependency'	=> array('element' => 'testimonials_type', 'value' => 'carousel'),
						'group'			=> $group_carousel_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Bullets", 'mascot-core' ),
						"param_name"	=> "show_bullets",
						"description"	=> esc_html__( 'Show Bottom Bullets for Testimonial Carousel', 'mascot-core' ),
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'dependency'	=> array('element' => 'testimonials_type', 'value' => 'carousel'),
						'group'			=> $group_carousel_options
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Animation Speed", 'mascot-core' ),
						"param_name"	=> "animation_speed",
						"description"	=> esc_html__( 'Speed of slide animation in milliseconds. Default value is 4000', 'mascot-core' ),
						'admin_label'   => true,
						'dependency'	=> array('element' => 'testimonials_type', 'value' => 'carousel'),
						'group'			=> $group_carousel_options
					),


					//Query Options
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Number of Items to Query from Database", 'mascot-core' ),
						"param_name"	=> "total_items",
						"description"	=> esc_html__( 'How many testimonials do you wish to show? Default 4', 'mascot-core' ),
						'admin_label'   => true,
						'group'			=> $group_query_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Category", 'mascot-core' ),
						"param_name"	=> "selected_category",
						"description"	=> esc_html__( 'Choose a category to pull testimonials from.', 'mascot-core' ),
						'value'			=> $categories_array,
						'admin_label'   => true,
						'group'			=> $group_query_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Order By", 'mascot-core' ),
						"param_name"	=> "order_by",
						"description"	=> esc_html__( 'Select how to sort retrieved posts.', 'mascot-core' ),
						'value'			=> $orderby_parameters_list,
						'std'			=> $orderby_parameters_list1[ 'Date' ],
						'admin_label'   => true,
						'group'			=> $group_query_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Order", 'mascot-core' ),
						"param_name"	=> "order",
						"description"	=> esc_html__( 'Descending or Ascending order.', 'mascot-core' ),
						'value'			=> array(
							'Descending'	=> 'DESC',
							'Ascending'	 => 'ASC',
						),
						'admin_label'   => true,
						'group'			=> $group_query_options
					),



					//Content Options
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Thumb", 'mascot-core' ),
						"param_name"	=> "show_thumb",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Testimonial Text", 'mascot-core' ),
						"param_name"	=> "show_testimonial_text",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Author Name", 'mascot-core' ),
						"param_name"	=> "show_author_name",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Job Position", 'mascot-core' ),
						"param_name"	=> "show_author_job_position",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Company", 'mascot-core' ),
						"param_name"	=> "show_author_company",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'group'			=> $group_content_options
					),

				)
			);
			vc_map( $vc_map );

			//Modify vc_map from themes
			if( function_exists('mascot_core_cpt_sc_testimonials_vc_map_modifier') ) {
				mascot_core_cpt_sc_testimonials_vc_map_modifier( $this->base );
			}
		}
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $attr array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render( $attr, $content = null ) {
		if ( class_exists( 'ReduxFramework' ) ) {
			if( ! mascot_core_get_redux_option( 'cpt-settings-testimonials-enable' ) ) {
				return;
			}
		}

		//Render Shortcode from themes
		if( function_exists('mascot_core_cpt_sc_testimonials_render') ) {
			return mascot_core_cpt_sc_testimonials_render( $attr, $content, CPT_Testimonials::Instance() );
		}
		
	}
}