<?php
namespace MASCOTCORE\CPT\Gallery\Shortcodes;

use MASCOTCORE\Lib;
use MASCOTCORE\CPT\Gallery\CPT_Gallery;

/**
 * class SC_Gallery
 * @package MASCOTCORE\CPT\Gallery\Shortcodes;
 */
class SC_Gallery implements Lib\Mascot_Core_Interface_PTShortcodes {
	
	/**
	 * @var string
	 */
	private $base;

	/**
	 * construct
	 */
	public function __construct() {
		if ( class_exists( 'ReduxFramework' ) ) {
			if( ! mascot_core_get_redux_option( 'cpt-settings-gallery-enable' ) ) {
				return;
			}
		}
		$this->base = 'tmvc_gallery';
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 *
	 * @see vc_map()
	 */
	public function vcMap() {
		if(function_exists('vc_map')) {
			$group_content_options = 'Content Options';
			$group_carousel_options = 'Carousel Options';
			$group_query_options = 'Query Options';
			$new_cpt_class = CPT_Gallery::Instance();
			$categories_array = mascot_core_category_list_array_for_vc( $new_cpt_class->ptTaxKey );

			$orderby_parameters_list1 = mascot_core_orderby_parameters_list();
			$orderby_parameters_list2 = array(
			);
			$orderby_parameters_list = array_merge( $orderby_parameters_list2, $orderby_parameters_list1 );


			$vc_map = array(
				'name'		=> esc_html__( 'Gallery', 'mascot-core' ),
				'base'		=>$this->base,
				'category'	=> 'by TM',
				'icon'		=> 'mascot-vc-icons vc-icon-cta',
				'allowed_container_element' => 'vc_row',
				'params'	=>array(
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Custom CSS class", 'mascot-core' ),
						"param_name"	=> "custom_css_class",
						"description"	=> esc_html__( 'To style particular content element.', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Gallery Type", 'mascot-core' ),
						"param_name"	=> "gallery_type",
						"description"	=> "",
						'value'			=> array(
							'Grid'	  => 'grid',
							'Carousel'  => 'carousel'
						),
						'save_always'   => true,
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Columns Layout", 'mascot-core' ),
						"param_name"	=> "columns",
						"description"	=> esc_html__( 'Define Columns Layout for Grid/Carousel.', 'mascot-core' ),
						'value'			=> array(
							'1'  =>  '1',
							'2'  =>  '2',
							'3'  =>  '3',
							'4'  =>  '4',
							'5'  =>  '5',
							'6'  =>  '6',
						),
						'std'			=> 4,
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Image Orientation", 'mascot-core' ),
						"param_name"	=> "gallery_image_orientation",
						"description"	=> "",
						'value'			=> array(
							'Landscape View' => 'landscape',
							'Portrait View'  => 'portrait'
						),
						'save_always'   => true,
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Image Aspect Ratio", 'mascot-core' ),
						"param_name"	=> "gallery_image_aspect_ratio",
						"description"	=> esc_html__( 'Define Aspect Ratio for Gallery Images.', 'mascot-core' ),
						'value'			=> array(
							'16:9'  =>  '16:9',
							'4:3'   =>  '4:3',
							'3:2'   =>  '3:2',
							'1:1'   =>  '1:1',
						),
						'std'			=> 4,
						'admin_label'   => true,
					),



					//Carousel Options
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Navigation Arrow", 'mascot-core' ),
						"param_name"	=> "show_navigation",
						"description"	=> esc_html__( 'Show Left Right Navigation Arrow for Testimonial Carousel', 'mascot-core' ),
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'dependency'	=> array('element' => 'gallery_type', 'value' => 'carousel'),
						'group'			=> $group_carousel_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Bullets", 'mascot-core' ),
						"param_name"	=> "show_bullets",
						"description"	=> esc_html__( 'Show Bottom Bullets for Testimonial Carousel', 'mascot-core' ),
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'dependency'	=> array('element' => 'gallery_type', 'value' => 'carousel'),
						'group'			=> $group_carousel_options
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Animation Speed", 'mascot-core' ),
						"param_name"	=> "animation_speed",
						"description"	=> esc_html__( 'Speed of slide animation in milliseconds. Default value is 4000', 'mascot-core' ),
						'admin_label'   => true,
						'dependency'	=> array('element' => 'gallery_type', 'value' => 'carousel'),
						'group'			=> $group_carousel_options
					),


					//Query Options
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Number of Items to Query from Database", 'mascot-core' ),
						"param_name"	=> "total_items",
						"description"	=> esc_html__( 'How many gallery items do you wish to show? Default 4', 'mascot-core' ),
						'admin_label'   => true,
						'group'			=> $group_query_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Category", 'mascot-core' ),
						"param_name"	=> "selected_category",
						"description"	=> esc_html__( 'Choose a category to pull gallery from.', 'mascot-core' ),
						'value'			=> $categories_array,
						'admin_label'   => true,
						'group'			=> $group_query_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Order By", 'mascot-core' ),
						"param_name"	=> "order_by",
						"description"	=> esc_html__( 'Select how to sort retrieved posts.', 'mascot-core' ),
						'value'			=> $orderby_parameters_list,
						'std'			=> $orderby_parameters_list1[ 'Date' ],
						'admin_label'   => true,
						'group'			=> $group_query_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Order", 'mascot-core' ),
						"param_name"	=> "order",
						"description"	=> esc_html__( 'Descending or Ascending order.', 'mascot-core' ),
						'value'			=> array(
							'Descending'	=> 'DESC',
							'Ascending'	 => 'ASC',
						),
						'admin_label'   => true,
						'group'			=> $group_query_options
					),



					//Content Options
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Gutter", 'mascot-core' ),
						"param_name"	=> "gutter",
						"description"	=> esc_html__( 'Define Columns Layout for Grid/Carousel.', 'mascot-core' ),
						'value'			=> mascot_core_portfolio_gutter_list(),
						'save_always'   => true,
						'admin_label'   => true,
						'dependency'	=> array('element' => 'gallery_type', 'value' => 'grid'),
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Hover Effect", 'mascot-core' ),
						"param_name"	=> "gallery_hover_effect",
						"description"	=> "",
						'value'			=> mascot_core_list_hover_effects(),
						'save_always'   => true,
						'admin_label'   => true,
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Overlay Color on Image", 'mascot-core' ),
						"param_name"	=> "gallery_overlay_color_on_image",
						"description"	=> "",
						'value'			=> array(
							'Black Ovedrlay' => '',
							'White Ovedrlay'  => 'shade-white'
						),
						'save_always'   => true,
						'admin_label'   => true,
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Add Gradient Effect on Image", 'mascot-core' ),
						"param_name"	=> "show_gradient_effect_on_image",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'std'			=> 'false',
						'save_always'   => true,
						'admin_label'   => true,
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Filter", 'mascot-core' ),
						"param_name"	=> "show_gallery_filter",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'std'			=> 'false',
						'admin_label'   => true,
						'dependency'	=> array('element' => 'gallery_type', 'value' => 'grid'),
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Title", 'mascot-core' ),
						"param_name"	=> "show_gallery_title",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Overlay Zoom/Link Icons", 'mascot-core' ),
						"param_name"	=> "show_gallery_overlay_zoomlink_icons",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Description", 'mascot-core' ),
						"param_name"	=> "show_gallery_description",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'std'			=> 'false',
						'admin_label'   => true,
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Date", 'mascot-core' ),
						"param_name"	=> "show_gallery_date",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'std'			=> 'false',
						'admin_label'   => true,
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Category", 'mascot-core' ),
						"param_name"	=> "show_selected_category",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'group'			=> $group_content_options
					),

				)
			);
			vc_map( $vc_map );

			//Modify vc_map from themes
			if( function_exists('mascot_core_cpt_sc_gallery_vc_map_modifier') ) {
				mascot_core_cpt_sc_gallery_vc_map_modifier( $this->base );
			}
		}
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $attr array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render( $attr, $content = null ) {
		if ( class_exists( 'ReduxFramework' ) ) {
			if( ! mascot_core_get_redux_option( 'cpt-settings-gallery-enable' ) ) {
				return;
			}
		}

		//Render Shortcode from themes
		if( function_exists('mascot_core_cpt_sc_gallery_render') ) {
			return mascot_core_cpt_sc_gallery_render( $attr, $content, CPT_Gallery::Instance() );
		}
		
	}
}