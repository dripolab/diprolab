<?php
namespace MASCOTCORE\CPT;

use MASCOTCORE\Lib;

/**
 * class Reg_Post_Type
 * @package MASCOTCORE\CPT;
 */
class Reg_Post_Type {
	/**
	 * @var Singleton The reference to *Singleton* instance of this class
	 */
	private static $instance;

	/**
	 * @var array
	 */
	private $allPostTypes = array();
	
	/**
	 * Returns the *Singleton* instance of this class.
	 *
	 * @return Singleton The *Singleton* instance.
	 */
	public static function get_instance()
	{
		if (null === static::$instance) {
			static::$instance = new static();
		}
		
		return static::$instance;
	}

	/**
	 * Protected constructor to prevent creating a new instance of the
	 * *Singleton* via the `new` operator from outside of this class.
	 */
	protected function __construct()
	{
	}

	/**
	 * Private clone method to prevent cloning of the instance of the
	 * *Singleton* instance.
	 *
	 * @return void
	 */
	private function __clone()
	{
	}

	/**
	 * Private unserialize method to prevent unserializing of the *Singleton*
	 * instance.
	 *
	 * @return void
	 */
	private function __wakeup()
	{
	}


	/**
	 * Adds new post type to post types array
	 */
	private function add_new_post_type(Lib\Mascot_Core_Interface_PostType $newPostType) {
		if(!array_key_exists($newPostType->getPTKey(), $this->allPostTypes)) {
			$this->allPostTypes[$newPostType->getPTKey()] = $newPostType;
		}
	}

	/**
	 * List of all post types to register
	 */
	private function all_post_types_to_reg() {
		$this->add_new_post_type(Clients\CPT_Clients::Instance());
		$this->add_new_post_type(Departments\CPT_Departments::Instance());
		$this->add_new_post_type(FAQ\CPT_FAQ::Instance());
		$this->add_new_post_type(Features\CPT_Features::Instance());
		$this->add_new_post_type(Gallery\CPT_Gallery::Instance());
		$this->add_new_post_type(Portfolio\CPT_Portfolio::Instance());
		$this->add_new_post_type(PricingTables\CPT_Pricing_Tables::Instance());
		$this->add_new_post_type(Services\CPT_Services::Instance());
		$this->add_new_post_type(Staff\CPT_Staff::Instance());
		$this->add_new_post_type(Testimonials\CPT_Testimonials::Instance());
	}


	/**
	 * Calls all_post_types_to_reg method, loops through each post type in array and calls register method
	 */
	public function register() {
		$this->all_post_types_to_reg();
		
		foreach ($this->allPostTypes as $eachPostType) {
			$eachPostType->register();
		}
	}
}