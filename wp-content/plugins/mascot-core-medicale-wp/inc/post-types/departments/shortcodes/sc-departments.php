<?php
namespace MASCOTCORE\CPT\Departments\Shortcodes;

use MASCOTCORE\Lib;
use MASCOTCORE\CPT\Departments\CPT_Departments;

/**
 * class SC_Departments
 * @package MASCOTCORE\CPT\Departments\Shortcodes;
 */
class SC_Departments implements Lib\Mascot_Core_Interface_PTShortcodes {
	
	/**
	 * @var string
	 */
	private $base;

	/**
	 * construct
	 */
	public function __construct() {
		if ( class_exists( 'ReduxFramework' ) ) {
			if( ! mascot_core_get_redux_option( 'cpt-settings-departments-enable' ) ) {
				return;
			}
		}
		$this->base = 'tmvc_departments';
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer
	 *
	 * @see vc_map()
	 */
	public function vcMap() {
		if(function_exists('vc_map')) {
			$group_content_options = 'Content Options';
			$group_carousel_options = 'Carousel Options';
			$group_query_options = 'Query Options';
			$new_cpt_class = CPT_Departments::Instance();
			$categories_array = mascot_core_category_list_array_for_vc( $new_cpt_class->ptTaxKey );

			$orderby_parameters_list1 = mascot_core_orderby_parameters_list();
			$orderby_parameters_list2 = array(
			);
			$orderby_parameters_list = array_merge( $orderby_parameters_list2, $orderby_parameters_list1 );


			$vc_map = array(
				'name'		=> esc_html__( 'Departments', 'mascot-core' ),
				'base'		=> $this->base,
				'category'	=> 'by TM',
				'icon'		=> 'mascot-vc-icons vc-icon-cta',
				'allowed_container_element' => 'vc_row',
				'params'	=> array(
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Custom CSS class", 'mascot-core' ),
						"param_name"	=> "custom_css_class",
						"description"	=> esc_html__( 'To style particular content element.', 'mascot-core' ),
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Departments Type", 'mascot-core' ),
						"param_name"	=> "departments_type",
						"description"	=> "",
						'value'			=> array(
							'Tab'		=> 'tab',
							'Grid'	  => 'grid',
							'Carousel'  => 'carousel'
						),
						'save_always'   => true,
						'admin_label'   => true,
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Columns Layout", 'mascot-core' ),
						"param_name"	=> "columns",
						"description"	=> esc_html__( 'Define Columns Layout for Grid/Carousel.', 'mascot-core' ),
						'value'			=> array(
							'1'  =>  '1',
							'2'  =>  '2',
							'3'  =>  '3',
							'4'  =>  '4'
						),
						'std'			=> 4,
						'admin_label'   => true,
						'dependency'	=> array('element' => 'departments_type', 'value' => array('grid', 'carousel') ),
					),



					//Carousel Options
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Navigation Arrow", 'mascot-core' ),
						"param_name"	=> "show_navigation",
						"description"	=> esc_html__( 'Show Left Right Navigation Arrow for Department Carousel', 'mascot-core' ),
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'dependency'	=> array('element' => 'departments_type', 'value' => 'carousel'),
						'group'			=> $group_carousel_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Bullets", 'mascot-core' ),
						"param_name"	=> "show_bullets",
						"description"	=> esc_html__( 'Show Bottom Bullets for Department Carousel', 'mascot-core' ),
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'dependency'	=> array('element' => 'departments_type', 'value' => 'carousel'),
						'group'			=> $group_carousel_options
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Animation Speed", 'mascot-core' ),
						"param_name"	=> "animation_speed",
						"description"	=> esc_html__( 'Speed of slide animation in milliseconds. Default value is 4000', 'mascot-core' ),
						'admin_label'   => true,
						'dependency'	=> array('element' => 'departments_type', 'value' => 'carousel'),
						'group'			=> $group_carousel_options
					),


					//Query Options
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Number of Items to Query from Database", 'mascot-core' ),
						"param_name"	=> "total_items",
						"description"	=> esc_html__( 'How many departments do you wish to show? Default 4', 'mascot-core' ),
						'admin_label'   => true,
						'group'			=> $group_query_options
					),
					array(
						'type'			=> 'checkbox',
						"heading"		=> esc_html__( "Category", 'mascot-core' ),
						"param_name"	=> "selected_category",
						"description"	=> esc_html__( 'Choose a category to pull departments from.', 'mascot-core' ),
						'value'			=> $categories_array,
						'admin_label'   => true,
						'group'			=> $group_query_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Order By", 'mascot-core' ),
						"param_name"	=> "order_by",
						"description"	=> esc_html__( 'Select how to sort retrieved posts.', 'mascot-core' ),
						'value'			=> $orderby_parameters_list,
						'std'			=> $orderby_parameters_list1[ 'Date' ],
						'admin_label'   => true,
						'group'			=> $group_query_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Order", 'mascot-core' ),
						"param_name"	=> "order",
						"description"	=> esc_html__( 'Descending or Ascending order.', 'mascot-core' ),
						'value'			=> array(
							'Descending'	=> 'DESC',
							'Ascending'	 => 'ASC',
						),
						'admin_label'   => true,
						'group'			=> $group_query_options
					),



					//Content Options
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Title", 'mascot-core' ),
						"param_name"	=> "show_department_title",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Icon", 'mascot-core' ),
						"param_name"	=> "show_department_icon",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'dependency'	=> array('element' => 'departments_type', 'value' => 'tab'),
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Thumb", 'mascot-core' ),
						"param_name"	=> "show_department_thumb",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Thumbnail Image Size", 'mascot-core' ),
						"param_name"	=> "feature_thumb_image_size",
						'description'   => '',
						'value'			=> mascot_core_get_available_image_sizes_for_vc(),
						'std'			=> 'post-thumbnail',
						'admin_label'   => true,
						'dependency'	=> array('element' => 'show_department_thumb', 'value' => 'true'),
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Description", 'mascot-core' ),
						"param_name"	=> "show_department_description",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'textfield',
						"heading"		=> esc_html__( "Description Excerpt Length", 'mascot-core' ),
						"param_name"	=> "department_description_excerpt_length",
						"description"	=> esc_html__( 'Number of words to display in excerpt.', 'mascot-core' ),
						'value'   => 15,
						'admin_label'   => true,
						'save_always'   => true,
						'dependency'	=> array('element' => 'departments_type', 'value' => array('grid', 'carousel')),
						'group'			=> $group_content_options
					),
					array(
						'type'			=> 'dropdown',
						"heading"		=> esc_html__( "Show Read More Button", 'mascot-core' ),
						"param_name"	=> "show_department_read_more_button",
						'description'   => '',
						'value'			=> array(
							'Yes'		=> 'true',
							'No'		=> 'false'
						),
						'admin_label'   => true,
						'group'			=> $group_content_options
					),

				)
			);
			vc_map( $vc_map );

			//Modify vc_map from themes
			if( function_exists('mascot_core_cpt_sc_departments_vc_map_modifier') ) {
				mascot_core_cpt_sc_departments_vc_map_modifier( $this->base );
			}
		}
	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $attr array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render( $attr, $content = null ) {
		if ( class_exists( 'ReduxFramework' ) ) {
			if( ! mascot_core_get_redux_option( 'cpt-settings-departments-enable' ) ) {
				return;
			}
		}

		//Render Shortcode from themes
		if( function_exists('mascot_core_cpt_sc_departments_render') ) {
			return mascot_core_cpt_sc_departments_render( $attr, $content, CPT_Departments::Instance() );
		}
		
	}
}