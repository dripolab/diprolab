<?php
namespace MASCOTCORE\CPT\FAQ;

use MASCOTCORE\Lib;

/**
 * Singleton class
 * class CPT_FAQ
 * @package MASCOTCORE\CPT\FAQ;
 */
final class CPT_FAQ implements Lib\Mascot_Core_Interface_PostType {
	
	/**
	 * @var string
	 */
	public 	$ptKey;
	public 	$ptKeyRewriteBase;
	public  $ptTaxKey;
	public  $ptTaxKeyRewriteBase;
	private $ptMenuIcon;
	private $ptSingularName;
	private $ptPluralName;

	/**
	 * Call this method to get singleton
	 *
	 * @return CPT_FAQ
	 */
	public static function Instance()
	{
		static $inst = null;
		if ($inst === null) {
			$inst = new CPT_FAQ();
		}
		return $inst;
	}
	
	/**
	 * Private ctor so nobody else can instance it
	 *
	 */
	private function __construct() {
		$this->ptSingularName = esc_html__( 'FAQ Item', 'mascot-core' );
		$this->ptPluralName = esc_html__( 'FAQ', 'mascot-core' );
		$this->ptKey = 'faq';
		$this->ptKeyRewriteBase = $this->ptKey;
		$this->ptTaxKey = 'faq_category';
		$this->ptTaxKeyRewriteBase = str_replace( '_', '-', $this->ptTaxKey );
		$this->ptMenuIcon = 'dashicons-editor-help';
	}

	/**
	 * @return string
	 */
	public function getPTKey() {
		return $this->ptKey;
	}

	/**
	 * @return string
	 */
	public function getPTTaxKey() {
		return $this->ptTaxKey;
	}

	/**
	 * registers custom post type & Tax
	 */
	public function register() {
		if ( class_exists( 'ReduxFramework' ) ) {
			$cpt_enable = mascot_core_get_redux_option( 'cpt-settings-faq-enable' );
			if( ! $cpt_enable ) {
				return;
			}
			$this->ptPluralName = mascot_core_get_redux_option( 'cpt-settings-faq-label', esc_html__( 'FAQ', 'mascot-core' ) );
			$this->ptMenuIcon = mascot_core_get_redux_option( 'cpt-settings-faq-admin-dashicon', $this->ptMenuIcon );

		}
		
		$this->registerCustomPostType();
		$this->registerCustomTax();
	}
	
	/**
	 * Regsiters custom post type
	 */
	private function registerCustomPostType() {

		$labels = array(
			'name'					=> $this->ptPluralName,
			'singular_name'			=> $this->ptPluralName . ' ' . esc_html__( 'Item', 'mascot-core' ),
			'menu_name'				=> $this->ptPluralName,
			'name_admin_bar'		=> $this->ptPluralName,
			'archives'				=> esc_html__( 'Item Archives', 'mascot-core' ),
			'attributes'			=> esc_html__( 'Item Attributes', 'mascot-core' ),
			'parent_item_colon'		=> esc_html__( 'Parent Item:', 'mascot-core' ),
			'all_items'				=> esc_html__( 'All Items', 'mascot-core' ),
			'add_new_item'			=> esc_html__( 'Add New Item', 'mascot-core' ),
			'add_new'				=> esc_html__( 'Add New', 'mascot-core' ),
			'new_item'				=> esc_html__( 'New Item', 'mascot-core' ),
			'edit_item'				=> esc_html__( 'Edit Item', 'mascot-core' ),
			'update_item'			=> esc_html__( 'Update Item', 'mascot-core' ),
			'view_item'				=> esc_html__( 'View Item', 'mascot-core' ),
			'view_items'			=> esc_html__( 'View Items', 'mascot-core' ),
			'search_items'			=> esc_html__( 'Search Item', 'mascot-core' ),
			'not_found'				=> esc_html__( 'Not found', 'mascot-core' ),
			'not_found_in_trash'	=> esc_html__( 'Not found in Trash', 'mascot-core' ),
			'featured_image'		=> esc_html__( 'Featured Image', 'mascot-core' ),
			'set_featured_image'	=> esc_html__( 'Set featured image', 'mascot-core' ),
			'remove_featured_image'	=> esc_html__( 'Remove featured image', 'mascot-core' ),
			'use_featured_image'	=> esc_html__( 'Use as featured image', 'mascot-core' ),
			'insert_into_item'		=> esc_html__( 'Insert into item', 'mascot-core' ),
			'uploaded_to_this_item'	=> esc_html__( 'Uploaded to this item', 'mascot-core' ),
			'items_list'			=> esc_html__( 'Items list', 'mascot-core' ),
			'items_list_navigation'	=> esc_html__( 'Items list navigation', 'mascot-core' ),
			'filter_items_list'		=> esc_html__( 'Filter items list', 'mascot-core' ),
		);

		$args = array(
			'label'						=> $this->ptSingularName,
			'description'				=> esc_html__( 'Post Type Description', 'mascot-core' ),
			'labels'					=> $labels,
			'supports'					=> array( 'title', 'editor', ),
			'taxonomies'				=> array( $this->ptTaxKey, 'post_tag' ),
			'hierarchical'				=> false,
			'public'					=> true,
			'show_ui'					=> true,
			'show_in_menu'				=> true,
			'menu_position'				=> 30,
			'menu_icon'					=> $this->ptMenuIcon,
			'show_in_admin_bar'			=> true,
			'show_in_nav_menus'			=> true,
			'can_export'				=> true,
			'has_archive'				=> true,
			'exclude_from_search'		=> false,
			'publicly_queryable'		=> true,
			'capability_type'			=> 'page',
			'rewrite'					=> array( 'slug' => $this->ptKeyRewriteBase, 'with_front' => false ),
		);
		register_post_type( $this->ptKey, $args );

	}
	
	/**
	 * Regsiters custom Taxonomy
	 */
	private function registerCustomTax() {

		$labels = array(
			'name'						=> _x( 'FAQ Items Categories', 'Taxonomy General Name', 'mascot-core' ),
			'singular_name'				=> _x( 'FAQ Item Category', 'Taxonomy Singular Name', 'mascot-core' ),
			'menu_name'					=> $this->ptPluralName . esc_html__( ' Categories', 'mascot-core' ),
			'all_items'					=> esc_html__( 'All ', 'mascot-core' ) . $this->ptSingularName . esc_html__( ' Categories', 'mascot-core' ),
			'parent_item'				=> esc_html__( 'Parent Item', 'mascot-core' ),
			'parent_item_colon'			=> esc_html__( 'Parent Item:', 'mascot-core' ),
			'new_item_name'				=> esc_html__( 'New ', 'mascot-core' ) . $this->ptSingularName . esc_html__( ' Category', 'mascot-core' ),
			'add_new_item'				=> esc_html__( 'Add ', 'mascot-core' ) . $this->ptSingularName . esc_html__( ' Category', 'mascot-core' ),
			'edit_item'					=> esc_html__( 'Edit ', 'mascot-core' ) . $this->ptSingularName . esc_html__( ' Category', 'mascot-core' ),
			'update_item'				=> esc_html__( 'Update ', 'mascot-core' ) . $this->ptSingularName . esc_html__( ' Category', 'mascot-core' ),
			'view_item'					=> esc_html__( 'View ', 'mascot-core' ) . $this->ptSingularName . esc_html__( ' Category', 'mascot-core' ),
			'separate_items_with_commas'=> esc_html__( 'Separate items with commas', 'mascot-core' ),
			'add_or_remove_items'		=> esc_html__( 'Add or remove items', 'mascot-core' ),
			'choose_from_most_used'		=> esc_html__( 'Choose from the most used', 'mascot-core' ),
			'popular_items'				=> esc_html__( 'Popular Items', 'mascot-core' ),
			'search_items'				=> esc_html__( 'Search Items', 'mascot-core' ),
			'not_found'					=> esc_html__( 'Not Found', 'mascot-core' ),
			'no_terms'					=> esc_html__( 'No items', 'mascot-core' ),
			'items_list'				=> esc_html__( 'Items list', 'mascot-core' ),
			'items_list_navigation'		=> esc_html__( 'Items list navigation', 'mascot-core' ),
		);
		$args = array(
			'labels'					=> $labels,
			'hierarchical'				=> true,
			'public'					=> true,
			'show_ui'					=> true,
			'show_admin_column'			=> true,
			'show_in_nav_menus'			=> true,
			'show_tagcloud'				=> true,
			'rewrite'					=> array( 'slug' => $this->ptTaxKeyRewriteBase, 'with_front' => false ),
		);
		register_taxonomy( $this->ptTaxKey, array( $this->ptKey ), $args );
	}

}