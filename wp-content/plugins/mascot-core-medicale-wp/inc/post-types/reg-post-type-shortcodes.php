<?php
namespace MASCOTCORE\CPT;

use MASCOTCORE\Lib;

/**
 * class Reg_PT_Short_Codes
 * @package MASCOTCORE\CPT;
 */
class Reg_PT_Short_Codes {
	/**
	 * @var Singleton The reference to *Singleton* instance of this class
	 */
	private static $instance;

	/**
	 * @var array
	 */
	private $allPTShortCodes = array();
	
	/**
	 * Returns the *Singleton* instance of this class.
	 *
	 * @return Singleton The *Singleton* instance.
	 */
	public static function get_instance()
	{
		if (null === static::$instance) {
			static::$instance = new static();
		}
		
		return static::$instance;
	}

	/**
	 * Protected constructor to prevent creating a new instance of the
	 * *Singleton* via the `new` operator from outside of this class.
	 */
	protected function __construct()
	{
	}

	/**
	 * Private clone method to prevent cloning of the instance of the
	 * *Singleton* instance.
	 *
	 * @return void
	 */
	private function __clone()
	{
	}

	/**
	 * Private unserialize method to prevent unserializing of the *Singleton*
	 * instance.
	 *
	 * @return void
	 */
	private function __wakeup()
	{
	}


	/**
	 * Adds new SC to SC array
	 */
	private function add_new_pt_shortcode(Lib\Mascot_Core_Interface_PTShortcodes $newPTShortCode) {
		if(!array_key_exists($newPTShortCode->getBase(), $this->allPTShortCodes)) {
			$this->allPTShortCodes[$newPTShortCode->getBase()] = $newPTShortCode;
		}
	}

	/**
	 * List of all SCs to register
	 */
	private function available_shortcodes() {
		$this->add_new_pt_shortcode(new Clients\Shortcodes\SC_Clients());
		$this->add_new_pt_shortcode(new Departments\Shortcodes\SC_Departments());
		$this->add_new_pt_shortcode(new FAQ\Shortcodes\SC_FAQ());
		$this->add_new_pt_shortcode(new Features\Shortcodes\SC_Features());
		$this->add_new_pt_shortcode(new Gallery\Shortcodes\SC_Gallery());
		$this->add_new_pt_shortcode(new PricingTables\Shortcodes\SC_Pricing_Tables());
		$this->add_new_pt_shortcode(new Portfolio\Shortcodes\SC_Portfolio());
		$this->add_new_pt_shortcode(new Staff\Shortcodes\SC_Staff());
		$this->add_new_pt_shortcode(new Services\Shortcodes\SC_Services());
		$this->add_new_pt_shortcode(new Testimonials\Shortcodes\SC_Testimonials());
	}

	/**
	 * Calls available_shortcodes method, loops through each SC in array and add it through add_shortcode
	 */
	public function load() {
		$this->available_shortcodes();
		
		foreach ($this->allPTShortCodes as $eachPTShortCode) {
			add_shortcode($eachPTShortCode->getBase(), array($eachPTShortCode, 'render'));
		}
	}
}