<?php

/* Loads all Custom Post Types located in post-types folder
================================================== */
require_once $plugin_dir_path . 'lib/interface-post-type.php';
require_once $plugin_dir_path . 'lib/interface-post-type-shortcodes.php';

foreach( glob( $plugin_dir_path . 'inc/post-types/*/loader.php' ) as $each_cpt_loader ) {
	require_once $each_cpt_loader;
}

//load shortcodes for custom-post-types
require_once $plugin_dir_path . 'inc/post-types/reg-post-type.php';
require_once $plugin_dir_path . 'inc/post-types/reg-post-type-shortcodes.php';

use MASCOTCORE\CPT;
use MASCOTCORE\Lib;

// activate custom post types
function mascot_core_reg_custom_post_types() {
    CPT\Reg_Post_Type::get_instance()->register();
}

// activate shortcodes for custom post types
function mascot_core_reg_cpt_shortcodes() {
	CPT\Reg_PT_Short_Codes::get_instance()->load();
}

// flush_rewrite_rules after activating cpt
if ( ! function_exists( 'mascot_core_myplugin_flush_rewrites' ) ) {
	function mascot_core_myplugin_flush_rewrites() {
		// call your CPT registration function here (it should also be hooked into 'init')
		mascot_core_reg_custom_post_types();
		//and flush the rules.
		flush_rewrite_rules();
	}
	register_activation_hook( __FILE__, 'mascot_core_myplugin_flush_rewrites' );
}

//init cpt
add_action('init', 'mascot_core_reg_custom_post_types', 4);

//init cpt sc
add_action('init', 'mascot_core_reg_cpt_shortcodes', 5);

//shortcode loader
require_once $plugin_dir_path . 'inc/shortcodes/shortcodes-loader.php';