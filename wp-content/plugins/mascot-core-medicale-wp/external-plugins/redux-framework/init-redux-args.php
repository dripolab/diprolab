<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "medicale_mascot_redux_theme_opt";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => __( 'Theme Options', 'mascot-core' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Theme Options', 'mascot-core' ),
        'page_title'           => __( 'Theme Options', 'mascot-core' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => 'AIzaSyBliGynKGbW5yf2YgZv_Dov38wUipnsiII',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => true,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => 'medicale_mascot_redux_theme_opt',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => __( 'Right', 'mascot-core' ),
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    /*$args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'http://docs.reduxframework.com/',
        'title' => __( 'Documentation', 'mascot-core' ),
    );

    $args['admin_bar_links'][] = array(
        //'id'    => 'redux-support',
        'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
        'title' => __( 'Support', 'mascot-core' ),
    );*/

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'mailto:thememascot@gmail.com',
        'title' => 'Send an email to ThemeMascot',
        'icon'  => 'el el-icon-envelope'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/thememascot',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'https://themeforest.net/user/thememascot',
        'title' => 'Contact with Author',
        'icon'  => 'el el-icon-link'
    );

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        //$args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'mascot-core' ), $v );
    } else {
        //$args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'mascot-core' );
    }

    // Add content after the form.
    $args['footer_text'] = '';

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'mascot-core' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'mascot-core' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'mascot-core' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'mascot-core' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'mascot-core' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */




    /*
     *
     * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
     *
     */

    /*
    *
    * --> Action hook examples
    *
    */

    // If Redux is running as a plugin, this will remove the demo notice and links
    //add_action( 'redux/loaded', 'remove_demo' );

    // Function to test the compiler hook and demo CSS output.
    // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
    //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

    // Change the arguments after they've been declared, but before the panel is created
    //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

    // Change the default value of a field after it's been set, but before it's been useds
    //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

    // Dynamically add a section. Can be also used to modify sections/fields
    //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    if ( ! function_exists( 'compiler_action' ) ) {
        function compiler_action( $options, $css, $changed_values ) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";
            //print_r($options); //Option values
            //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
        }
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $field['msg']    = 'your custom error message';
                $return['error'] = $field;
            }

            if ( $warning == true ) {
                $field['msg']      = 'your custom warning message';
                $return['warning'] = $field;
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use get_template_directory_uri() if you want to use any of the built in icons
     * */
    if ( ! function_exists( 'dynamic_section' ) ) {
        function dynamic_section( $sections ) {
            //$sections = array();
            $sections[] = array(
                'title'  => __( 'Section via hook', 'mascot-core' ),
                'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'mascot-core' ),
                'icon'   => 'el el-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    if ( ! function_exists( 'change_arguments' ) ) {
        function change_arguments( $args ) {
            //$args['dev_mode'] = true;

            return $args;
        }
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    if ( ! function_exists( 'change_defaults' ) ) {
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }
    }

    /**
     * Removes the demo link and the notice of integrated demo from the redux-framework plugin
     */
    if ( ! function_exists( 'remove_demo' ) ) {
        function remove_demo() {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                remove_filter( 'plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2 );

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
            }
        }
    }


    /* This section is removed due to "Resource caching" plugin territory issue */

    //maintenance section opt added into the theme
    function mascot_core_redux_opt_maintenance_section() {
        $site_url = site_url();
        $maintenance_site_url = '<a href="' . $site_url . '?view-maintenance-mode=true" target="_blank">click here</a>';
        // -> START Maintenance Mode Settings
        $maintenance_section =  array(
            'title'  => __( 'Maintenance Mode', 'medicale-wp' ),
            'id'     => 'maintenance-mode-settings',
            'desc'   => sprintf( __('Add a maintenance page to your website to let visitors know your site is down for maintenance. Only users with admin rights get full access to the site. And as an administrator, you will not be able to see the maintenance page. If you want to see it then %s', 'medicale-wp'), $maintenance_site_url ),
            'icon'   => 'dashicons-before dashicons-hammer',
            'fields' => array(

                array(
                    'id'       => 'maintenance-mode-settings-status',
                    'type'     => 'switch',
                    'title'    => __( 'Maintenance Mode', 'medicale-wp' ),
                    'subtitle' => '',
                    'default'  => 0,
                    'on'       => 'Enabled',
                    'off'      => 'Disabled',
                ),
                array(
                    'id'       => 'maintenance-mode-layout-ordering',
                    'type'     => 'sorter',
                    'title'    => __( 'Layout Ordering', 'medicale-wp' ),
                    'subtitle' => __( 'Reorder them by shifting up and down.', 'medicale-wp' ),
                    'desc'     => '',
                    'compiler' => 'true',
                    'options'  => array(
                        'ordering' => array(
                            'title'     => 'Title',
                            'content'    => 'Content',
                            'timer' => 'Countdown Timer',
                        ),
                    ),
                    'required' => array( 'maintenance-mode-settings-status', '=', '1' ),
                ),
                
                array(
                    'id'       => 'maintenance-mode-text-align',
                    'type'     => 'select',
                    'title'    => __( 'Text Alignment', 'medicale-wp' ),
                    'subtitle' => __( 'Text Alignment of this page', 'medicale-wp' ),
                    'desc'     => '',
                    'options'  => mascot_core_text_alignment_list(),
                    'default'  => 'text-center',
                    'required' => array( 'maintenance-mode-settings-status', '=', '1' ),
                ),
                array(
                    'id'       => 'maintenance-mode-settings-enable-social-links',
                    'type'     => 'switch',
                    'title'    => __( 'Enable Social Links', 'medicale-wp' ),
                    'subtitle' => '',
                    'default'  => 0,
                    'on'       => __( 'Yes', 'medicale-wp' ),
                    'off'      => __( 'No', 'medicale-wp' ),
                    'required' => array( 'maintenance-mode-settings-status', '=', '1' ),
                ),





                //Logo Starts
                array(
                    'id'       => 'maintenance-mode-settings-logo-status',
                    'type'     => 'switch',
                    'title'    => __( 'Show Logo', 'medicale-wp' ),
                    'subtitle' => '',
                    'default'  => 0,
                    'on'       => __( 'Yes', 'medicale-wp' ),
                    'off'      => __( 'No', 'medicale-wp' ),
                    'required' => array( 'maintenance-mode-settings-status', '=', '1' ),
                ),
                array(
                    'id'       => 'maintenance-mode-settings-logo',
                    'type'     => 'media',
                    'url'      => false,
                    'title'    => __( 'Logo', 'medicale-wp' ),
                    'compiler' => 'true',
                    //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                    'desc'     => __( 'Basic media uploader with disabled URL input field.', 'medicale-wp' ),
                    'subtitle' => __( 'Upload a 32px x 32px png/gif image that will represent your website favicon.', 'medicale-wp' ),
                    //'default'  => array( 'url' => MASCOT_ASSETS_URI . '/images/logo/logo-wide.png' ),
                    'required' => array( 
                        array( 'maintenance-mode-settings-logo-status', '=', '1' )
                    )
                ),
                array(
                    'id'       => 'maintenance-mode-settings-logo-max-width',
                    'type'    => 'spinner',
                    'title'   => __( 'Logo Maximum Width', 'medicale-wp' ),
                    'desc'    => __( 'Set maximum width for uploaded logo (in px)', 'medicale-wp' ),
                    'default' => '200',
                    'min'     => '20',
                    'step'    => '1',
                    'max'     => '1000',
                    'required' => array( 
                        array( 'maintenance-mode-settings-logo-status', '=', '1' )
                    )
                ),




                //section Title Starts
                array(
                    'id'       => 'maintenance-mode-settings-title-typography-starts',
                    'type'     => 'section',
                    'title'    => __( 'Title', 'medicale-wp' ),
                    'subtitle' => __( 'Define text and styles for Title.', 'medicale-wp' ),
                    'indent'   => true, // Indent all options below until the next 'section' option is set.
                    'required' => array( 'maintenance-mode-settings-status', '=', '1' ),
                ),
                array(
                    'id'       => 'maintenance-mode-settings-title',
                    'type'     => 'text',
                    'title'    => __( 'Title Text', 'medicale-wp' ),
                    'subtitle' => __( 'Set page title to show', 'medicale-wp' ),
                    'desc'     => '',
                    'default'  => __( 'Site Under Construction', 'medicale-wp' ),
                    'required' => array( 'maintenance-mode-settings-status', '=', '1' ),
                ),
                array(
                    'id'            => 'maintenance-mode-settings-title-typography',
                    'type'          => 'typography',
                    'title'         => __( 'Title Typography', 'medicale-wp' ),
                    'subtitle'      => '',
                    //'compiler'    => true,  // Use if you want to hook in your own CSS compiler
                    'google'        => true,
                    // Disable google fonts. Won't work if you haven't defined your google api key
                    'font-backup'   => false,
                    // Select a backup non-google font in addition to a google font
                    'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
                    'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
                    'subsets'       => false, // Only appears if google is true and subsets not set to false
                    'font-size'     => true,
                    'line-height'   => true,
                    'word-spacing'  => true,  // Defaults to false
                    'letter-spacing'=> true,  // Defaults to false
                    'text-transform'=> true,  // Defaults to false
                    'color'         => true,
                    'preview'       => true, // Disable the previewer
                    'all_styles'    => true,
                    'units'         => 'px',
                    'required'      => array( 'maintenance-mode-settings-status', '=', '1' ),
                ),
                array(
                    'id'       => 'maintenance-mode-settings-title-margin-top-bottom',
                    'type'     => 'spacing',
                    // An array of CSS selectors to apply this font style to
                    'mode'     => 'margin',
                    // absolute, padding, margin, defaults to padding
                    'all'      => false,
                    // Have one field that applies to all
                    'top'      => true,     // Disable the top
                    'right'    => false,     // Disable the right
                    'bottom'   => true,     // Disable the bottom
                    'left'     => false,     // Disable the left
                    'units'    => 'px',      // You can specify a unit value. Possible: px, em, %
                    //'units_extended'=> 'true',    // Allow users to select any type of unit
                    'display_units' => true,   // Set to false to hide the units if the units are specified
                    'title'    => __( 'Margin Top & Bottom', 'medicale-wp' ),
                    'subtitle' => '',
                    'desc'     => '',
                    'required' => array( 'maintenance-mode-settings-status', '=', '1' ),
                ),
                array(
                    'id'       => 'maintenance-mode-settings-title-typography-ends',
                    'type'   => 'section',
                    'indent' => false, // Indent all options below until the next 'section' option is set.
                    'required' => array( 'maintenance-mode-settings-status', '=', '1' ),
                ),






                //section Content Starts
                array(
                    'id'       => 'maintenance-mode-settings-content-typography-starts',
                    'type'     => 'section',
                    'title'    => __( 'Content', 'medicale-wp' ),
                    'subtitle' => __( 'Define text and styles for Content.', 'medicale-wp' ),
                    'indent'   => true, // Indent all options below until the next 'section' option is set.
                    'required' => array( 'maintenance-mode-settings-status', '=', '1' ),
                ),
                array(
                    'id'       => 'maintenance-mode-settings-content',
                    'type'     => 'editor',
                    'title'    => __( 'Content Text', 'medicale-wp' ),
                    'subtitle' => __( 'Enter the content for maintenance page which will be showed below logo and countdown timer if those are selected', 'medicale-wp' ),
                    'default'  => 'Sorry, server is down for maintenance. We are improving and fixing problems of our website. <br> We will be back very soon...',
                    'required' => array( 'maintenance-mode-settings-status', '=', '1' ),
                ),
                array(
                    'id'            => 'maintenance-mode-settings-content-typography',
                    'type'          => 'typography',
                    'title'         => __( 'Content Typography', 'medicale-wp' ),
                    'subtitle'      => '',
                    //'compiler'    => true,  // Use if you want to hook in your own CSS compiler
                    'google'        => true,
                    // Disable google fonts. Won't work if you haven't defined your google api key
                    'font-backup'   => false,
                    // Select a backup non-google font in addition to a google font
                    'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
                    'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
                    'subsets'       => false, // Only appears if google is true and subsets not set to false
                    'font-size'     => true,
                    'line-height'   => true,
                    'word-spacing'  => true,  // Defaults to false
                    'letter-spacing'=> true,  // Defaults to false
                    'text-transform'=> true,  // Defaults to false
                    'color'         => true,
                    'preview'       => true, // Disable the previewer
                    'all_styles'    => true,
                    'units'         => 'px',
                    'required'      => array( 'maintenance-mode-settings-status', '=', '1' ),
                ),
                array(
                    'id'       => 'maintenance-mode-settings-content-margin-top-bottom',
                    'type'     => 'spacing',
                    // An array of CSS selectors to apply this font style to
                    'mode'     => 'margin',
                    // absolute, padding, margin, defaults to padding
                    'all'      => false,
                    // Have one field that applies to all
                    'top'      => true,     // Disable the top
                    'right'    => false,     // Disable the right
                    'bottom'   => true,     // Disable the bottom
                    'left'     => false,     // Disable the left
                    'units'    => 'px',      // You can specify a unit value. Possible: px, em, %
                    //'units_extended'=> 'true',    // Allow users to select any type of unit
                    'display_units' => true,   // Set to false to hide the units if the units are specified
                    'title'    => __( 'Margin Top & Bottom', 'medicale-wp' ),
                    'subtitle' => '',
                    'desc'     => '',
                    'required' => array( 'maintenance-mode-settings-status', '=', '1' ),
                ),
                array(
                    'id'       => 'maintenance-mode-settings-content-typography-ends',
                    'type'   => 'section',
                    'indent' => false, // Indent all options below until the next 'section' option is set.
                    'required' => array( 'maintenance-mode-settings-status', '=', '1' ),
                ),




                //section custom background
                array(
                    'id'       => 'maintenance-mode-settings-custom-background-starts',
                    'type'     => 'section',
                    'title'    => __( 'Custom Background', 'medicale-wp' ),
                    'subtitle' => __( 'Define Custom Background for maintenance page.', 'medicale-wp' ),
                    'indent'   => true, // Indent all options below until the next 'section' option is set.
                    'required' => array( 'maintenance-mode-settings-status', '=', '1' ),
                ),
                array(
                    'id'       => 'maintenance-mode-settings-custom-background-status',
                    'type'     => 'switch',
                    'title'    => __( 'Custom Background', 'medicale-wp' ),
                    'subtitle' => '',
                    'default'  => 0,
                    'on'       => __( 'Yes', 'medicale-wp' ),
                    'off'      => __( 'No', 'medicale-wp' ),
                    'required' => array( 'maintenance-mode-settings-status', '=', '1' ),
                ),
                array(
                    'id'       => 'maintenance-mode-settings-bg',
                    'type'     => 'background',
                    'title'    => __( 'Background', 'medicale-wp' ),
                    'subtitle' => __( 'Choose background image or color.', 'medicale-wp' ),
                    'required' => array( 
                        array( 'maintenance-mode-settings-custom-background-status', '=', '1' )
                    )
                ),
                array(
                    'id'       => 'maintenance-mode-settings-bg-layer-overlay-status',
                    'type'     => 'switch',
                    'title'    => __( 'Add Background Overlay', 'medicale-wp' ),
                    'subtitle' => '',
                    'default'  => 0,
                    'on'       => __( 'Yes', 'medicale-wp' ),
                    'off'      => __( 'No', 'medicale-wp' ),
                    'required' => array( 
                        array( 'maintenance-mode-settings-custom-background-status', '=', '1' )
                    )
                ),
                array(
                    'id'       => 'maintenance-mode-settings-bg-layer-overlay',
                    'type'          => 'slider',
                    'title'         => __( 'Background Overlay Opacity', 'medicale-wp' ),
                    'subtitle'      => __( 'Overlay on background image on footer.', 'medicale-wp' ),
                    'desc'          => '',
                    'default'       => 7,
                    'min'           => 1,
                    'step'          => 1,
                    'max'           => 9,
                    'display_value' => 'text',
                    'required' => array( 
                        array( 'maintenance-mode-settings-bg-layer-overlay-status', '=', '1' )
                    )
                ),
                array(
                    'id'       => 'maintenance-mode-settings-bg-layer-overlay-color',
                    'type'     => 'button_set',
                    'compiler' =>true,
                    'title'    => __( 'Background Overlay Color', 'medicale-wp' ),
                    'subtitle' => __( 'Select Dark or White Overlay on background image.', 'medicale-wp' ),
                    'options'  => array(
                        'dark'          => __( 'Dark', 'medicale-wp' ),
                        'white'         => __( 'White', 'medicale-wp' ),
                        'theme-colored' => __( 'Primary Theme Color', 'medicale-wp' )
                    ),
                    'default' => 'dark',
                    'required' => array( 
                        array( 'maintenance-mode-settings-bg-layer-overlay-status', '=', '1' )
                    )
                ),
                array(
                    'id'       => 'maintenance-mode-settings-custom-background-status-section-ends',
                    'type'     => 'section',
                    'title'    => '',
                    'subtitle' => '',
                    'indent'   => false, // Indent all options below until the next 'section' option is set.
                ),




                //section Countdown Timer
                array(
                    'id'       => 'maintenance-mode-settings-countdown-timer-section-starts',
                    'type'     => 'section',
                    'title'    => __( 'Countdown Timer', 'medicale-wp' ),
                    'subtitle' => __( 'Define time and styles for Countdown Timer.', 'medicale-wp' ),
                    'indent'   => true, // Indent all options below until the next 'section' option is set.
                    'required' => array( 'maintenance-mode-settings-status', '=', '1' ),
                ),
                array(
                    'id'       => 'maintenance-mode-settings-countdown-timer-status',
                    'type'     => 'switch',
                    'title'    => __( 'Enable Countdown Timer', 'medicale-wp' ),
                    'subtitle' => '',
                    'default'  => 0,
                    'on'       => __( 'Yes', 'medicale-wp' ),
                    'off'      => __( 'No', 'medicale-wp' ),
                    'required' => array( 'maintenance-mode-settings-status', '=', '1' ),
                ),
                array(
                    'id'       => 'maintenance-mode-settings-countdown-timer-day',
                    'type'     => 'date',
                    'title'    => __( 'Launch Date', 'medicale-wp' ),
                    'subtitle' => __( 'Enter the date when your website will be launched. The countdown will count to that day.', 'medicale-wp' ),
                    'desc'     => '',
                    'default'  => date('m/d/Y', strtotime('+2 Years')),
                    'placeholder'     => 'Choose Date',
                    'required' => array( 
                        array( 'maintenance-mode-settings-countdown-timer-status', '=', '1' )
                    )
                ),
                array(
                    'id'            => 'maintenance-mode-settings-countdown-timer-hour',
                    'type'          => 'slider',
                    'title'         => __( 'Launch Hour', 'medicale-wp' ),
                    'subtitle'      => __( 'Choose launcing hour in between 0 to 23 (24 hour format).', 'medicale-wp' ),
                    'desc'          => '',
                    'default'       => 7,
                    'min'           => 0,
                    'step'          => 1,
                    'max'           => 23,
                    'display_value' => 'text',
                    'required' => array( 
                        array( 'maintenance-mode-settings-countdown-timer-status', '=', '1' )
                    )
                ),
                array(
                    'id'            => 'maintenance-mode-settings-countdown-timer-minute',
                    'type'          => 'slider',
                    'title'         => __( 'Launch Minute', 'medicale-wp' ),
                    'subtitle'      => __( 'Choose launcing Minute in between 0 to 59.', 'medicale-wp' ),
                    'desc'          => '',
                    'default'       => 30,
                    'min'           => 0,
                    'step'          => 1,
                    'max'           => 59,
                    'display_value' => 'text',
                    'required' => array( 
                        array( 'maintenance-mode-settings-countdown-timer-status', '=', '1' )
                    )
                ),
                array(
                    'id'       => 'maintenance-mode-settings-countdown-timer-margin-top-bottom',
                    'type'     => 'spacing',
                    // An array of CSS selectors to apply this font style to
                    'mode'     => 'margin',
                    // absolute, padding, margin, defaults to padding
                    'all'      => false,
                    // Have one field that applies to all
                    'top'      => true,     // Disable the top
                    'right'    => false,     // Disable the right
                    'bottom'   => true,     // Disable the bottom
                    'left'     => false,     // Disable the left
                    'units'    => 'px',      // You can specify a unit value. Possible: px, em, %
                    //'units_extended'=> 'true',    // Allow users to select any type of unit
                    'display_units' => true,   // Set to false to hide the units if the units are specified
                    'title'    => __( 'Margin Top & Bottom', 'medicale-wp' ),
                    'subtitle' => '',
                    'desc'     => '',
                    'required' => array( 
                        array( 'maintenance-mode-settings-countdown-timer-status', '=', '1' )
                    )
                ),
                array(
                    'id'       => 'maintenance-mode-settings-countdown-timer-style',
                    'type'     => 'button_set',
                    'title'    => __( 'Countdown Timer Style', 'medicale-wp' ),
                    'subtitle' => __( 'Choose one from different styles', 'medicale-wp' ),
                    'desc'     => '',
                    'options'  => array(
                        '1' => 'Style 1 - Final Countdown',
                        '2' => 'Style 2 - Flip Clock',
                        '3' => 'Style 3 - Classy Countdown',
                    ),
                    'default'  => '1',
                    'required' => array( 
                        array( 'maintenance-mode-settings-countdown-timer-status', '=', '1' )
                    )
                ),
                array(
                    'id'       => 'maintenance-mode-settings-countdown-timer-style1-format',
                    'type'     => 'textarea',
                    'title'    => __( 'Date Time Format', 'medicale-wp' ),
                    'subtitle' => __( 'Enter the date time format to display', 'medicale-wp' ),
                    'desc'     => '',
                    'default'  => '%D <span>Days</span> %H <span>Hours</span> %M <span>Minutes</span> %S <span>Seconds</span>',
                    'required' => array( 
                        array( 'maintenance-mode-settings-countdown-timer-style', '=', '1' )
                    )
                ),
                array(
                    'id'            => 'maintenance-mode-settings-countdown-timer-typography',
                    'type'          => 'typography',
                    'title'         => __( 'Countdown Timer Typography', 'medicale-wp' ),
                    'subtitle'      => '',
                    //'compiler'    => true,  // Use if you want to hook in your own CSS compiler
                    'google'        => true,
                    // Disable google fonts. Won't work if you haven't defined your google api key
                    'font-backup'   => false,
                    // Select a backup non-google font in addition to a google font
                    'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
                    'font-weight'   => true, // Includes font-style and weight. Can use font-style or font-weight to declare
                    'subsets'       => false, // Only appears if google is true and subsets not set to false
                    'font-size'     => true,
                    'line-height'   => true,
                    'word-spacing'  => true,  // Defaults to false
                    'letter-spacing'=> true,  // Defaults to false
                    'text-transform'=> true,  // Defaults to false
                    'color'         => true,
                    'preview'       => true, // Disable the previewer
                    'all_styles'    => true,
                    'units'         => 'px',
                    'required' => array( 
                        array( 'maintenance-mode-settings-countdown-timer-style', '=', '1' )
                    )
                ),
                array(
                    'id'       => 'maintenance-mode-settings-countdown-timer-section-ends',
                    'type'     => 'section',
                    'title'    => '',
                    'subtitle' => '',
                    'indent'   => false, // Indent all options below until the next 'section' option is set.
                ),
            )
        );
        return $maintenance_section;
    }

    if ( ! mascot_core_theme_installed() ) {
        Redux::setSection( $opt_name, mascot_core_redux_opt_maintenance_section() );
    }