<?php
/*
Plugin Name: Mascot Core - Medicale WP
Plugin URI:  https://themeforest.net/user/thememascot/portfolio
Description: Mascot Core Plugin for Mascot Framework. It includes all the required Custom Post Types and Shortcodes needed by Mascot Framework.
Version:     1.0
Author:      ThemeMascot
Author URI:  https://themeforest.net/user/thememascot/portfolio
Text Domain: mascot-core
Domain Path: /languages/
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define( 'MASCOT_CORE_VERSION', '1.0' );

global $plugin_dir_path, $plugin_dir_url;
$plugin_dir_path = plugin_dir_path( __FILE__ );
$plugin_dir_url = plugin_dir_url( __FILE__ );



if(!function_exists('mascot_core_enque_scripts')) {
	/**
	 * enque style
	 */
	function mascot_core_enque_scripts() {
		global $plugin_dir_url;
		wp_enqueue_style( 'mascot-core-custom-admin', $plugin_dir_url . 'assets/css/custom-admin.css' );
	}
}
add_action( 'admin_enqueue_scripts',	'mascot_core_enque_scripts' );



if(!function_exists('mascot_core_text_domain')) {
	/**
	* Loads plugin text domain for translation
	*/
	function mascot_core_text_domain() {
		load_plugin_textdomain( 'mascot-core', false, basename( dirname( __FILE__ ) ) . '/languages' );
	}
	add_action('plugins_loaded', 'mascot_core_text_domain');
}

if( !function_exists('mascot_core_theme_installed') ) {
	/**
	* Checks whether theme is installed or not
	* @return bool
	*/
	function mascot_core_theme_installed() {
		return defined('MASCOT_TEMPLATE_URI');
	}
}

require_once $plugin_dir_path . 'load-lib-ext-plugins.php';
require_once $plugin_dir_path . 'load-cpt-sc.php';
require_once $plugin_dir_path . 'load-other.php';