<?php

// Null Funcion
function mascot_core_null_function() {}

if(!function_exists('mascot_core_get_cpt_template_part')) {
	/**
	 * Load a cpt template part into a template
	 *
	 * @param string $slug The slug name for the generic template.
	 * @param string $name The name of the specialised template.
	 * @param string $folder The name of the specialised folder.
	 * @param array $params array of parameters to pass to the template.
	 * @param boolean $shortcode_ob_start only for shortcodes to get HTML string.
	 */
	function mascot_core_get_cpt_template_part( $slug, $name = null, $folder, $params = array(), $shortcode_ob_start ) {

		$template_path = 'inc/post-types/' . $folder . '/' . $slug;

		return mascot_core_get_template_part( $template_path, $name, $params, $shortcode_ob_start );

	}
}

if(!function_exists('mascot_core_get_inc_folder_template_part')) {
	/**
	 * Load a inc folder template part into a template
	 *
	 * @param string $slug The slug name for the generic template.
	 * @param string $name The name of the specialised template.
	 * @param string $folder The name of the specialised folder.
	 * @param array $params array of parameters to pass to the template.
	 * @param boolean $shortcode_ob_start only for shortcodes to get HTML string.
	 */
	function mascot_core_get_inc_folder_template_part( $slug, $name = null, $folder, $params = array() ) {

		$template_path = 'inc/' . $folder . '/' . $slug;

		return mascot_core_get_template_part( $template_path, $name, $params );

	}
}

if(!function_exists('mascot_core_get_template_part')) {
	/**
	 * Load a template part into a template
	 *
	 * @param string $template_path path of the specialised template.
	 * @param string $name The name of the specialised template.
	 * @param array $params array of parameters to pass to the template.
	 * @param boolean $shortcode_ob_start only for shortcodes to get HTML string.
	 */
	function mascot_core_get_template_part( $template_path, $name = null, $params = array(), $shortcode_ob_start = false ) {

		$output_html = '';

		if( is_array($params) && count($params) ) {
			extract($params);
		}

		$templates = array();
		$name = (string) $name;
		if ( '' !== $name )
			$templates[] = "{$template_path}-{$name}.php";

		$templates[] = "{$template_path}.php";

		$located = mascot_core_locate_template($templates);

		if($located) {
			if( $shortcode_ob_start ) {
				ob_start();
				include($located);
				$output_html = ob_get_clean();
			} else {
				include($located);
			}
		}

		return $output_html;
	}
}

if(!function_exists('mascot_core_locate_template')) {
	/**
	 * Retrieve the name of the highest priority template file that exists.
	 *
	 * Searches in the MASCOT_STYLESHEET_DIR before MASCOT_TEMPLATE_DIR
	 * so that themes which inherit from a parent theme can just overload one file.
	 *
	 * @param string|array $template_names Template file(s) to search for, in order.
	 * @return string The template filename if one is located.
	 */
	function mascot_core_locate_template($template_names) {
		global $plugin_dir_path;

		$located = '';
		foreach ( (array) $template_names as $template_name ) {
			if ( !$template_name ) {
				continue;
			}
			if ( file_exists($plugin_dir_path . '/' . $template_name)) {
				$located = $plugin_dir_path . '/' . $template_name;
				break;
			}
		}
		return $located;
	}
}


if(!function_exists('mascot_core_register_widgets_add_extra_widgets')) {
	/**
	 * The $widget_list parameter is an array of all widget lists from the mascot_core_register_widgets() function.
	 */
	function mascot_core_register_widgets_add_extra_widgets( $widget_list ) {
		$extra_widgets = array(
			'Medicale_Mascot_Widget_GalleryImages',
			'Medicale_Mascot_Widget_Portfolio',
			'Medicale_Mascot_Widget_Testimonials',
		);
	 
		// combine the two arrays
		$widget_list = array_merge( $extra_widgets, $widget_list );
	 
		return $widget_list;
	}
	add_filter('mascot_core_register_widgets_add_widgets', 'mascot_core_register_widgets_add_extra_widgets');
}


if ( ! function_exists( 'mascot_core_category_list_array_for_vc' ) ) {
	/**
	 * Return category list array for VC
	 */
	function mascot_core_category_list_array_for_vc( $taxonomy ) {
		$list_categories = array(
			esc_html__( 'All', 'mascot-core' ) => ''
		);
		$terms = get_terms( $taxonomy );

		if ( $terms && !is_wp_error( $terms ) ) :
			foreach ( $terms as $term ) {
				$list_categories[ $term->name ] = $term->slug;
			}
		endif;

		return $list_categories;
	}
}


if ( ! function_exists( 'mascot_core_get_redux_option' ) ) {
	/**
	 * Retuns Redux Theme Option
	 */
	function mascot_core_get_redux_option( $id, $fallback = false, $param = false ) {
		global $medicale_mascot_redux_theme_opt;

		if ( $fallback == false ) $fallback = '';

		$output = ( isset( $medicale_mascot_redux_theme_opt[$id] ) && $medicale_mascot_redux_theme_opt[$id] !== '' ) ? $medicale_mascot_redux_theme_opt[$id] : $fallback;

		if ( !empty( $medicale_mascot_redux_theme_opt[$id] ) && $param ) {
			$output = $medicale_mascot_redux_theme_opt[$id][$param];
		}
		return $output;
	}
}




/**
 * Admin Bar Menu Actions
 */
function mascot_core_admin_menu_actions() {
	if( mascot_core_theme_installed() ) {
		add_action( 'admin_bar_menu',  'mascot_core_admin_bar_menu', 50 );
	}
}
add_action('init', 'mascot_core_admin_menu_actions');

// Admin Bar Menu
function mascot_core_admin_bar_menu( $wp_admin_bar ) {
	$icon = '<i class="ab-icon dashicons-admin-generic"></i>';
	
	$wp_admin_bar->add_menu(array(
		'id'	  => 'mascot-options',
		'title'   => $icon . 'Mascot Help',
		'href'	=> admin_url( 'admin.php?page=mascot-about' )
	));
	
	$wp_admin_bar->add_menu(array(
		'parent'  => 'mascot-options',
		'id'	  => 'mascot-options-about',
		'title'   => 'About',
		'href'	=> admin_url( 'admin.php?page=mascot-about' )
	));

	$wp_admin_bar->add_menu(array(
		'parent'  => 'mascot-options',
		'id'	  => 'mascot-help',
		'title'   => 'Support & Help',
		'href'	=> admin_url( 'admin.php?page=mascot-docs' )
	));

	$wp_admin_bar->add_menu(array(
		'parent'  => 'mascot-options',
		'id'	  => 'mascot-faq',
		'title'   => 'FAQ',
		'href'	=> admin_url( 'admin.php?page=mascot-faq' )
	));
	
	$wp_admin_bar->add_menu(array(
		'parent'  => 'mascot-options',
		'id'	  => 'mascot-options-sub',
		'title'   => 'Theme Options',
		'href'	=> admin_url( 'admin.php?page=ThemeOptions' )
	));
	
	if ( class_exists( 'OCDI_Plugin' ) ) {
		$wp_admin_bar->add_menu(array(
			'parent'  => 'mascot-options',
			'id'	  => 'mascot-demo-content-importer',
			'title'   => 'One Click Demo Import',
			'href'	=> admin_url( 'themes.php?page=tm-one-click-demo-import' )
		));
	}

	$plugins = mascot_core_tgmpa_get_plugins_need_update();
	if ( count( $plugins ) ) {
		$wp_admin_bar->add_menu(array(
			'parent'  => 'mascot-options',
			'id'	  => 'mascot-update-plugins',
			'title'   => 'Update Required Plugins <span class="update-plugins"><span class="update-count">'.count( $plugins ).'</span></span>',
			'href'	=> admin_url( 'themes.php?page=tgmpa-install-plugins' )
		));
	}

	$wp_admin_bar->add_menu(array(
		'parent'  => 'mascot-options',
		'id'	  => 'mascot-system-status',
		'title'   => 'System Status',
		'href'	=> admin_url( 'admin.php?page=mascot-system-status' )
	));
	
	$wp_admin_bar->add_menu(array(
		'parent'  => 'mascot-options',
		'id'	  => 'mascot-themes',
		'title'   => 'Browse Our Themes',
		'href'	=> 'http://themeforest.net/user/ThemeMascot/portfolio?ref=ThemeMascot',
		'meta'	=> array( 'target' => '_blank' )
	));
}


function mascot_core_theme_admin_menu_system_status() {
	global $plugin_dir_path;
	include_once $plugin_dir_path . 'inc/admin-tpl/mascot-system-status.php';
}

function mascot_core_tgmpa_get_plugins_need_update() {
	$instance = call_user_func( array( get_class( $GLOBALS['tgmpa'] ), 'get_instance' ) );
	$plugins  = array(
		//'all'	  => array(), // Meaning: all plugins which still have open actions.
		//'install'  => array(),
		'update'   => array(),
		//'activate' => array(),
	);
	foreach ( $instance->plugins as $slug => $plugin ) {
		if ( $instance->is_plugin_active( $slug ) && false === $instance->does_plugin_have_update( $slug ) ) {
			// No need to display plugins if they are installed, up-to-date and active.
			continue;
		} else {
			$plugins['all'][ $slug ] = $plugin;
			if ( ! $instance->is_plugin_installed( $slug ) ) {
				$plugins['install'][ $slug ] = $plugin;
			} else {
				if ( false !== $instance->does_plugin_have_update( $slug ) ) {
					$plugins['update'][ $slug ] = $plugin;
				}
				if ( $instance->can_plugin_activate( $slug ) ) {
					$plugins['activate'][ $slug ] = $plugin;
				}
			}
		}
	}
	return $plugins['update'];
}

if(!function_exists('mascot_core_get_url_params')) {
	/**
	 * retrieve values of parameters passing through the URL
	 */
	function mascot_core_get_url_params( $param ) {
		return isset( $_GET[ $param ] ) ? $_GET[ $param ] : ( isset( $_REQUEST[ $param ] ) ? $_REQUEST[ $param ] : '' );
	}
}