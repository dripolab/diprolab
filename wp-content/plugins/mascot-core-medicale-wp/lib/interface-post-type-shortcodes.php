<?php
namespace MASCOTCORE\Lib;

/**
 * interface Mascot_Core_Interface_PTShortcodes
 * @package MASCOTCORE\Lib;
 */
interface Mascot_Core_Interface_PTShortcodes {
	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase();

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render($atts, $content = null);
}