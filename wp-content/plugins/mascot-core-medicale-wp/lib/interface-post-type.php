<?php
namespace MASCOTCORE\Lib;

/**
 * interface Mascot_Core_Interface_PostType
 * @package MASCOTCORE\Lib;
 */
interface Mascot_Core_Interface_PostType {
	/**
	 * Returns PT Key
	 * @return string
	 */
	public function getPTKey();

	/**
	 * It registers custom post type
	 */
	public function register();
}