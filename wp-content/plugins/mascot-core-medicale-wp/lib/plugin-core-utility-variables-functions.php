<?php

if(!function_exists('mascot_core_animate_css_animation_list')) {
	/**
	 * animate.css animation list https://daneden.github.io/animate.css/
	 */
	function mascot_core_animate_css_animation_list() {
		$animate_css_animation_list = array(
			'' => '',
			'fadeIn' => 'fadeIn',
			'fadeInDown' => 'fadeInDown',
			'fadeInDownBig' => 'fadeInDownBig',
			'fadeInLeft' => 'fadeInLeft',
			'fadeInLeftBig' => 'fadeInLeftBig',
			'fadeInRight' => 'fadeInRight',
			'fadeInRightBig' => 'fadeInRightBig',
			'fadeInUp' => 'fadeInUp',
			'fadeInUpBig' => 'fadeInUpBig',
			'fadeOut' => 'fadeOut',
			'fadeOutDown' => 'fadeOutDown',
			'fadeOutDownBig' => 'fadeOutDownBig',
			'fadeOutLeft' => 'fadeOutLeft',
			'fadeOutLeftBig' => 'fadeOutLeftBig',
			'fadeOutRight' => 'fadeOutRight',
			'fadeOutRightBig' => 'fadeOutRightBig',
			'fadeOutUp' => 'fadeOutUp',
			'fadeOutUpBig' => 'fadeOutUpBig',
			'bounce' => 'bounce',
			'flash' => 'flash',
			'pulse' => 'pulse',
			'rubberBand' => 'rubberBand',
			'shake' => 'shake',
			'swing' => 'swing',
			'tada' => 'tada',
			'wobble' => 'wobble',
			'jello' => 'jello',
			'bounceIn' => 'bounceIn',
			'bounceInDown' => 'bounceInDown',
			'bounceInLeft' => 'bounceInLeft',
			'bounceInRight' => 'bounceInRight',
			'bounceInUp' => 'bounceInUp',
			'bounceOut' => 'bounceOut',
			'bounceOutDown' => 'bounceOutDown',
			'bounceOutLeft' => 'bounceOutLeft',
			'bounceOutRight' => 'bounceOutRight',
			'bounceOutUp' => 'bounceOutUp',
			'flip' => 'flip',
			'flipInX' => 'flipInX',
			'flipInY' => 'flipInY',
			'flipOutX' => 'flipOutX',
			'flipOutY' => 'flipOutY',
			'lightSpeedIn' => 'lightSpeedIn',
			'lightSpeedOut' => 'lightSpeedOut',
			'rotateIn' => 'rotateIn',
			'rotateInDownLeft' => 'rotateInDownLeft',
			'rotateInDownRight' => 'rotateInDownRight',
			'rotateInUpLeft' => 'rotateInUpLeft',
			'rotateInUpRight' => 'rotateInUpRight',
			'rotateOut' => 'rotateOut',
			'rotateOutDownLeft' => 'rotateOutDownLeft',
			'rotateOutDownRight' => 'rotateOutDownRight',
			'rotateOutUpLeft' => 'rotateOutUpLeft',
			'rotateOutUpRight' => 'rotateOutUpRight',
			'slideInUp' => 'slideInUp',
			'slideInDown' => 'slideInDown',
			'slideInLeft' => 'slideInLeft',
			'slideInRight' => 'slideInRight',
			'slideOutUp' => 'slideOutUp',
			'slideOutDown' => 'slideOutDown',
			'slideOutLeft' => 'slideOutLeft',
			'slideOutRight' => 'slideOutRight',
			'zoomIn' => 'zoomIn',
			'zoomInDown' => 'zoomInDown',
			'zoomInLeft' => 'zoomInLeft',
			'zoomInRight' => 'zoomInRight',
			'zoomInUp' => 'zoomInUp',
			'zoomOut' => 'zoomOut',
			'zoomOutDown' => 'zoomOutDown',
			'zoomOutLeft' => 'zoomOutLeft',
			'zoomOutRight' => 'zoomOutRight',
			'zoomOutUp' => 'zoomOutUp',
			'hinge' => 'hinge',
			'rollIn' => 'rollIn',
			'rollOut' => 'rollOut'
		);
		return $animate_css_animation_list;
	}
}

if(!function_exists('mascot_core_orderby_parameters_list')) {
	/**
	 * Orderby Parameters list
	 */
	function mascot_core_orderby_parameters_list() {
		$orderby_parameters_list = array(
			esc_html__( 'Date', 'medicale-wp' ) 				=> 'date',
			esc_html__( 'Post Name', 'medicale-wp' ) 			=> 'name',
			esc_html__( 'Random Order', 'medicale-wp' ) 		=> 'rand',
			esc_html__( 'Last Modified Date', 'medicale-wp' ) 	=> 'modified',
			esc_html__( 'Author', 'medicale-wp' ) 				=> 'author',
			esc_html__( 'Title', 'medicale-wp' ) 				=> 'title',
			esc_html__( 'ID', 'medicale-wp' ) 					=> 'ID',
			esc_html__( 'Post/Page Parent ID', 'medicale-wp' ) 	=> 'parent',
			esc_html__( 'Number of Comments', 'medicale-wp' ) 	=> 'comment_count',
			esc_html__( 'Page Order', 'medicale-wp' ) 			=> 'menu_order'
		);
		return $orderby_parameters_list;
	}
}

if(!function_exists('mascot_core_list_hover_effects')) {
	/**
	 * Hover Effect list
	 */
	function mascot_core_list_hover_effects() {
		$hover_effects_list = array(
			esc_html__( 'Default', 'medicale-wp' )   		=> '',
			esc_html__( 'Effect London', 'medicale-wp' )	=> 'effect-london',
			esc_html__( 'Effect Rome', 'medicale-wp' )	  => 'effect-rome',
			esc_html__( 'Effect Paris', 'medicale-wp' )	 => 'effect-paris',
			esc_html__( 'Effect Barlin', 'medicale-wp' )	=> 'effect-barlin'
		);
		return $hover_effects_list;
	}
}

if(!function_exists('mascot_core_portfolio_gutter_list')) {
	/**
	 * Portfolio Gutter list
	 */
	function mascot_core_portfolio_gutter_list() {
		$gutter_list = array(
			esc_html__( 'Default', 'medicale-wp' )  	=>  'gutter',
			esc_html__( 'No Gutter', 'medicale-wp' ) =>  '',
			'2px'  		=>  'gutter-small',
			'30px'  	=>  'gutter-30',
			'40px'  	=>  'gutter-40',
			'50px'  	=>  'gutter-50',
			'60px'  	=>  'gutter-60',
		);
		return $gutter_list;
	}
}

if(!function_exists('mascot_core_different_size_list')) {
	/**
	 * Size list
	 */
	function mascot_core_different_size_list() {
		$size_list = array(
			''  	=>  '',
			'xl'  	=>  'xl',
			'lg'  	=>  'lg',
			'md'  	=>  'md',
			'sm'  	=>  'sm',
			'xs'  	=>  'xs',
		);
		return $size_list;
	}
}

if(!function_exists('mascot_core_text_alignment_list')) {
	/**
	 * Text Alignment List
	 */
	function mascot_core_text_alignment_list() {
		$alignment_list = array(
			esc_html__( 'Text Center', 'medicale-wp' ) 	=> 'text-center',
			esc_html__( 'Text Right', 'medicale-wp' ) 	=> 'text-right flip',
			esc_html__( 'Text Left', 'medicale-wp' ) 	=> 'text-left flip'
		);
		return $alignment_list;
	}
}

if(!function_exists('mascot_core_heading_tag_list')) {
	/**
	 * Heading Tag List
	 */
	function mascot_core_heading_tag_list() {
		$heading_tag_list = array(
			''  	=>  '',
			'h2' => 'h2',
			'h3' => 'h3',
			'h4' => 'h4',
			'h5' => 'h5',
			'h6' => 'h6',
		);
		return $heading_tag_list;
	}
}

if(!function_exists('mascot_core_heading_tag_list_all')) {
	/**
	 * Heading Tag List
	 */
	function mascot_core_heading_tag_list_all() {
		$heading_tag_list_all = array(
			'h1' => 'h1',
			'h2' => 'h2',
			'h3' => 'h3',
			'h4' => 'h4',
			'h5' => 'h5',
			'h6' => 'h6',
		);
		return $heading_tag_list_all;
	}
}

if(!function_exists('mascot_core_open_link_in')) {
	/**
	 * Open Link In
	 */
	function mascot_core_open_link_in() {
		$open_link_in = array(
			esc_html__( 'New Window', 'medicale-wp' )   => '_blank',
			esc_html__( 'Same Window', 'medicale-wp' )  => '_self'
		);
		return $open_link_in;
	}
}

if(!function_exists('mascot_core_vc_font_style_list')) {
	/**
	 * Font Style List
	 */
	function mascot_core_vc_font_style_list() {
		$font_style_list = array(
			esc_html__( 'Normal', 'medicale-wp' )   => '',
			esc_html__( 'Italic', 'medicale-wp' )  	=> 'italic'
		);
		return $font_style_list;
	}
}

if(!function_exists('mascot_core_vc_font_weight_list')) {
	/**
	 * Font weight List
	 */
	function mascot_core_vc_font_weight_list() {
		$font_weight_list = array(
			esc_html__( 'Default', 'medicale-wp' )   => '',
			'100'   => '100',
			'200'   => '200',
			'300'   => '300',
			'400'   => '400',
			'500'   => '500',
			'600'   => '600',
			'700'   => '700',
			'800'   => '800',
		);
		return $font_weight_list;
	}
}

if(!function_exists('mascot_core_vc_text_transform_list')) {
	/**
	 * Text Transform List
	 */
	function mascot_core_vc_text_transform_list() {
		$text_transform_list = array(
			esc_html__( 'Default', 'medicale-wp' )   	=> '',
			esc_html__( 'None', 'medicale-wp' )  		=> 'none',
			esc_html__( 'Capitalize', 'medicale-wp' )  	=> 'capitalize',
			esc_html__( 'Uppercase', 'medicale-wp' )  	=> 'uppercase',
			esc_html__( 'Lowercase', 'medicale-wp' )  	=> 'lowercase',
			esc_html__( 'Initial', 'medicale-wp' )  	=> 'initial',
			esc_html__( 'Inherit', 'medicale-wp' )  	=> 'inherit'
		);
		return $text_transform_list;
	}
}

if(!function_exists('mascot_core_get_post_all_categories_array')) {
	/**
	 * Category List of Blog Posts as an Array
	 */
	function mascot_core_get_post_all_categories_array() {
		$categories = get_categories( array(
			'orderby' => 'name',
			'order'   => 'ASC'
		) );
		$cats = array();
		$cats[''] = 'All';
		foreach($categories as $cat){
			$cats[$cat->term_id] = $cat->name;
		}
		return $cats;
	}
}

if(!function_exists('mascot_core_get_page_list_array')) {
	/**
	 * Category List of Pages as an Array
	 */
	function mascot_core_get_page_list_array() {
		$all_pages = get_pages();
		$pages = array();
		foreach($all_pages as $each_page){
			$pages[$each_page->ID] = $each_page->post_title;
		}
		return $pages;
	}
}

if ( ! function_exists( 'mascot_core_metabox_get_list_of_predefined_theme_color_css_files' ) ) {
	/**
	 * Get list of Predefined Theme Color css files
	 */
	function mascot_core_metabox_get_list_of_predefined_theme_color_css_files() {
		$predefined_theme_colors = array();

		if( $handle = opendir( MASCOT_TEMPLATE_DIR . '/assets/css/colors/' ) ) {
			while( false !== ($entry = readdir($handle)) ) {
				if( $entry != "." && $entry != ".." ) {
					$predefined_theme_colors[$entry] = $entry;
				}
			}
			closedir($handle);
		}
		return $predefined_theme_colors;
	}
}

if ( ! function_exists( 'mascot_core_category_list_array' ) ) {
	/**
	 * Return category list array
	 */
	function mascot_core_category_list_array( $taxonomy ) {
		$list_categories = array(
			'' => esc_html__( 'All', 'mascot-core' )
		);
		$terms = get_terms( $taxonomy );

		if ( $terms && !is_wp_error( $terms ) ) :
			foreach ( $terms as $term ) {
				$list_categories[ $term->slug ] = $term->name;
			}
		endif;

		return $list_categories;
	}
}


if ( ! function_exists( 'mascot_core_get_available_image_sizes_for_vc' ) ) {
	/**
	 * Get information about available image sizes for VC
	 */
	function mascot_core_get_available_image_sizes_for_vc() {
		if( mascot_core_theme_installed() ) {
			return medicale_mascot_get_available_image_sizes_for_vc();
		} else {
			return array();
		}
	}
}