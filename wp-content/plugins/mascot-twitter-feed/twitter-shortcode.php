<?php
class Mascot_Twitter_Feed_SCTwitterFeed {
	
    /**
     * @var shortcode_base
     */
    private $shortcode_base;

    /**
     * construct
     */
    public function __construct() {
        $this->shortcode_base = 'tm_twitter_feed';
        add_action('vc_before_init', array($this, 'MapShortCodeVC'));
    }

    /**
     * Returns shortcode_base for shortcode
     */
    public function getShortCodeBase() {
        return $this->shortcode_base;
    }

    /**
     * Maps shortcode to Visual Composer
     */
    public function MapShortCodeVC() {
        if(function_exists('vc_map')) {
            $group_content_options = 'Content Options';
            $group_carousel_options = 'Carousel Options';
            $group_animation_options = 'Animation Options';

            $vc_map = array(
                'name' => esc_html__( 'Twitter Feed', 'mascot-twitter-feed' ),
                'base' => $this->shortcode_base,
                'category' => 'by TM',
                'icon' => 'mascot-vc-icons vc-icon-cta',
                'allowed_container_element' => 'vc_row',
                'params' => array(
                    array(
                        'type'          => 'textfield',
                        "heading"       => esc_html__( "Custom CSS class", 'mascot-twitter-feed' ),
                        "param_name"    => "custom_css_class",
                        "description"   => esc_html__( 'To style particular content element.', 'mascot-twitter-feed' ),
                        'admin_label'   => true,
                    ),
                    array(
                        'type'          => 'dropdown',
                        "heading"       => esc_html__( "Twitter Feed Type", 'mascot-twitter-feed' ),
                        "param_name"    => "feed_type",
                        "description"   => esc_html__( 'Select a twitter feed type from here.', 'mascot-twitter-feed' ),
                        'value'         => array(
                            esc_html__( 'Carousel', 'mascot-twitter-feed' )   =>   'carousel',
                            esc_html__( 'List', 'mascot-twitter-feed' )       =>   'list',
                        ),
                        'save_always'   => true,
                        'admin_label'   => true,
                    ),
                    array(
                        'type'          => 'textfield',
                        "heading"       => esc_html__( "Twitter Username", 'mascot-twitter-feed' ),
                        "param_name"    => "userid",
                        "description"   => esc_html__( 'Put Your Twitter Username Here. Default: "Envato".', 'mascot-twitter-feed' ),
                        'admin_label'   => true,
                    ),
                    array(
                        'type'          => 'textfield',
                        "heading"       => esc_html__( "Number of Tweets", 'mascot-twitter-feed' ),
                        "param_name"    => "count",
                        "description"   => esc_html__( 'Number of tweets you want to display. Default: "3".', 'mascot-twitter-feed' ),
                        'admin_label'   => true,
                    ),
                    array(
                        'type'          => 'dropdown',
                        "heading"       => esc_html__( "Text Alignment", 'mascot-twitter-feed' ),
                        "param_name"    => "text_alignment",
                        "description"   => "",
                        'value'         => mascot_twitter_feed_text_alignment_list(),
                        'save_always'   => true,
                        'admin_label'   => true,
                    ),


                    //Carousel Options
                    array(
                        'type'          => 'dropdown',
                        "heading"       => esc_html__( "Show Navigation Arrow", 'mascot-twitter-feed' ),
                        "param_name"    => "show_navigation",
                        "description"   => esc_html__( 'Show Left Right Navigation Arrow for Twitter Carousel', 'mascot-twitter-feed' ),
                        'value'         => array(
                            'Yes'       => 'true',
                            'No'        => 'false'
                        ),
                        'std'           => 'false',
                        'admin_label'   => true,
                        'dependency'    => array('element' => 'feed_type', 'value' => 'carousel'),
                        'group'         => $group_carousel_options
                    ),
                    array(
                        'type'          => 'dropdown',
                        "heading"       => esc_html__( "Show Bullets", 'mascot-twitter-feed' ),
                        "param_name"    => "show_bullets",
                        "description"   => esc_html__( 'Show Bottom Bullets for Twitter Carousel', 'mascot-twitter-feed' ),
                        'value'         => array(
                            'Yes'       => 'true',
                            'No'        => 'false'
                        ),
                        'admin_label'   => true,
                        'dependency'    => array('element' => 'feed_type', 'value' => 'carousel'),
                        'group'         => $group_carousel_options
                    ),
                    array(
                        'type'          => 'textfield',
                        "heading"       => esc_html__( "Animation Speed", 'mascot-twitter-feed' ),
                        "param_name"    => "animation_speed",
                        "description"   => esc_html__( 'Speed of slide animation in milliseconds. Default value is 4000', 'mascot-twitter-feed' ),
                        'admin_label'   => true,
                        'dependency'    => array('element' => 'feed_type', 'value' => 'carousel'),
                        'group'         => $group_carousel_options
                    ),



                    //Animation Options
                    array(
                        'type'          => 'dropdown',
                        "heading"       => esc_html__( "animateOut Style", 'mascot-twitter-feed' ),
                        "param_name"    => "animate_out",
                        "description"   => esc_html__( 'Choose animateOut animation from animate.css library.', 'mascot-twitter-feed' ),
                        'value'         => mascot_twitter_feed_animate_css_animation_list(),
                        'save_always'   => true,
                        'admin_label'   => true,
                        'dependency'    => array('element' => 'feed_type', 'value' => 'carousel'),
                        'group'         => $group_animation_options
                    ),
                    array(
                        'type'          => 'dropdown',
                        "heading"       => esc_html__( "animateIn Style", 'mascot-twitter-feed' ),
                        "param_name"    => "animate_in",
                        "description"   => esc_html__( 'Choose animateIn animation from animate.css library.', 'mascot-twitter-feed' ),
                        'value'         => mascot_twitter_feed_animate_css_animation_list(),
                        'save_always'   => true,
                        'admin_label'   => true,
                        'dependency'    => array('element' => 'feed_type', 'value' => 'carousel'),
                        'group'         => $group_animation_options
                    ),

                )
            );
            vc_map( $vc_map );

            //Modify vc_map from themes
            if( function_exists('mascot_twitter_feed_sc_twitter_feed_vc_map_modifier') ) {
                mascot_twitter_feed_sc_twitter_feed_vc_map_modifier( $this->shortcode_base );
            }
        }
    }

    /**
     * Renders shortcodes HTML
     */
    public function render( $attr, $content = null ) {

        //Render Shortcode from themes
        if( function_exists('mascot_twitter_feed_sc_twitter_feed_render') ) {
            return mascot_twitter_feed_sc_twitter_feed_render( $attr, $content );
        }
        
    }
}

// activate shortcode
function mascot_twitter_feed_reg_twitter_shortcode() {
	$new_sctwitterfeed = new Mascot_Twitter_Feed_SCTwitterFeed();
	add_shortcode($new_sctwitterfeed->getShortCodeBase(), array($new_sctwitterfeed, 'render'));
}

//init sc
add_action('init', 'mascot_twitter_feed_reg_twitter_shortcode', 6);