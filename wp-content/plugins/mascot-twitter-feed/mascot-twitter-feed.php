<?php
/*
Plugin Name: Mascot Twitter Feed
Plugin URI:  https://themeforest.net/user/thememascot/portfolio
Description: This plugin adds twitter feed functionality to our themes.
Version:     1.0
Author:      ThemeMascot
Author URI:  https://themeforest.net/user/thememascot/portfolio
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define( 'MASCOT_TWITTER_FEED_VERSION', '1.0' );

$twitter_plugin_dir_path = plugin_dir_url( __FILE__ );
define( 'MASCOT_TWITTER_FEED_PLUGIN_DIR_PATH', $twitter_plugin_dir_path );

function mascot_twitter_feed_enqueue_script() {   
    wp_enqueue_script( 'tweetie', MASCOT_TWITTER_FEED_PLUGIN_DIR_PATH . 'twitter/js/tweetie.js', array('jquery'), '1.0' );
}
add_action('wp_enqueue_scripts', 'mascot_twitter_feed_enqueue_script');

add_action("wp_ajax_mascot_twitter_tweetie_api_action", "mascot_twitter_tweetie_api_action");
add_action("wp_ajax_nopriv_mascot_twitter_tweetie_api_action", "mascot_twitter_tweetie_api_action");
function mascot_twitter_tweetie_api_action(){
    global $wpdb;
    require_once("twitter/tweet.php");
    wp_die();
}

if(!function_exists('mascot_twitter_feed_text_domain')) {
	/**
	* Loads plugin text domain for translation
	*/
	function mascot_twitter_feed_text_domain() {
		load_plugin_textdomain( 'mascot-twitter-feed', false, basename( dirname( __FILE__ ) ) . '/languages' );
	}
	add_action('plugins_loaded', 'mascot_twitter_feed_text_domain');
}

require_once 'mascot-utility-functions.php';
require_once 'twitter-shortcode.php';