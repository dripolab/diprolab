<?php
    /**
     * Your Twitter App Info
     */
    
    $api_key = '';
    $api_secret = '';
    $api_access_token = '';
    $api_access_token_secret = '';

    if ( class_exists( 'ReduxFramework' ) && function_exists('medicale_mascot_get_redux_option') ) {
        if( !empty( medicale_mascot_get_redux_option( 'theme-api-settings-twitter-api-key' ) ) ) {
            $api_key = medicale_mascot_get_redux_option( 'theme-api-settings-twitter-api-key' );
        }
        if( !empty( medicale_mascot_get_redux_option( 'theme-api-settings-twitter-api-secret' ) ) ) {
            $api_secret = medicale_mascot_get_redux_option( 'theme-api-settings-twitter-api-secret' );
        }
        if( !empty( medicale_mascot_get_redux_option( 'theme-api-settings-twitter-api-access-token' ) ) ) {
            $api_access_token = medicale_mascot_get_redux_option( 'theme-api-settings-twitter-api-access-token' );
        }
        if( !empty( medicale_mascot_get_redux_option( 'theme-api-settings-twitter-api-access-token-secret' ) ) ) {
            $api_access_token_secret = medicale_mascot_get_redux_option( 'theme-api-settings-twitter-api-access-token-secret' );
        }
    }

    // Consumer Key
    define('CONSUMER_KEY', $api_key);
    define('CONSUMER_SECRET', $api_secret);

    // User Access Token
    define('ACCESS_TOKEN', $api_access_token);
    define('ACCESS_SECRET', $api_access_token_secret);
    
    // Cache Settings
    define('CACHE_ENABLED', false);
    define('CACHE_LIFETIME', 3600); // in seconds
    define('HASH_SALT', md5(dirname(__FILE__)));