<?php
require_once("twitteroauth/Config.php"); // Path to twitteroauth library
require_once("twitteroauth/JsonDecoder.php"); // Path to twitteroauth library
require_once("twitteroauth/Response.php"); // Path to twitteroauth library
require_once("twitteroauth/SignatureMethod.php"); // Path to twitteroauth library
require_once("twitteroauth/Consumer.php"); // Path to twitteroauth library
require_once("twitteroauth/HmacSha1.php"); // Path to twitteroauth library
require_once("twitteroauth/Token.php"); // Path to twitteroauth library
require_once("twitteroauth/Util.php"); // Path to twitteroauth library
require_once("twitteroauth/Request.php"); // Path to twitteroauth library
require_once("twitteroauth/TwitterOAuthException.php"); // Path to twitteroauth library
require_once("twitteroauth/twitteroauth.php"); // Path to twitteroauth library
require_once('config.php'); // Path to config file